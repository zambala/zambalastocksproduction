//
//  EquitiesCell2.m
//  testing
//
//  Created by zenwise technologies on 26/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "EquitiesCell2.h"
#import "WisdomPortfolioCell.h"
#import "AppDelegate.h"

@implementation EquitiesCell2
{
    AppDelegate * delegate;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    //NSLog(@"Eq leader advice details:%@",delegate.leaderAdviceDetails);
    //NSLog(@"Portfolio array:%@",self.portfolioArray);
    self.portfolioTblView.delegate=self;
    self.portfolioTblView.dataSource=self;
    
    
    
    
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//tableview delegates//

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    self.tableViewheightConstraint.constant=self.portfolioArray.count*30;
    
    return self.portfolioArray.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    WisdomPortfolioCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    //NSLog(@"Cell for portarray:%@",self.portfolioArray);
    
    
    if(self.portfolioArray.count>0)
    {
        NSString * companyName = [[self.portfolioArray objectAtIndex:indexPath.row] objectForKey:@"symbol"];
        NSString * allocationPercent = [[self.portfolioArray objectAtIndex:indexPath.row] objectForKey:@"percentage"];
        NSString * percent = @"%";
        
        NSString * finalString = [NSString stringWithFormat:@"%@(%@%@)",companyName,allocationPercent,percent];
        cell.companyNameLabel.text=finalString;
        
        NSString * quantity = [[self.portfolioArray objectAtIndex:indexPath.row]objectForKey:@"quantity"];
        
        cell.quantityLabelNew.text=quantity;
        NSString * LTP = [[self.portfolioArray objectAtIndex:indexPath.row]objectForKey:@"LTP"];
        cell.ltpLabel.text=LTP;
        NSString * per=@"%";
        
       
        NSString * changePercent=[NSString stringWithFormat:@"%@%@",[[self.portfolioArray objectAtIndex:indexPath.row] objectForKey:@"ChangePercent"],per];
        
        if([changePercent containsString:@"-"])
        {
           cell.changePercentLabel.text=changePercent;
            cell.changePercentLabel.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
        }
        else{
             cell.changePercentLabel.text=changePercent;
            cell.changePercentLabel.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
        }

        
        
    }
    
    return cell;
    
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    
    return 30;
    
}



@end
