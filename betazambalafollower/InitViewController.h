//
//  InitViewController.h
//  betazambalafollower
//
//  Created by Zenwise Technologies on 28/09/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InitViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *backView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityInd;

@end
