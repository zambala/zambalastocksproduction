//
//  NewOffersByMonksViewController.h
//  PremiumServices
//
//  Created by zenwise mac 2 on 7/13/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "ViewController.h"

@interface NewOffersByMonksViewController : ViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *premiumAdvicesView;
@property (weak, nonatomic) IBOutlet UIButton *subscribeButton;
@property (weak, nonatomic) IBOutlet UIView *signalsView;
@property (weak, nonatomic) IBOutlet UIView *dealerModeView;
- (IBAction)onCartButtonTap:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *offersTbleView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *offerTblViewHgt;

@property (weak, nonatomic) IBOutlet UIView *popUpView;

@property (weak, nonatomic) IBOutlet UIView *blurView;

- (IBAction)addToCartBtn:(id)sender;


- (IBAction)cancelBtn:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *serviceTblView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cartOutlet;
@property NSInteger itemCount;


- (IBAction)onContinueTap:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *continueBtn;


@end
