//
//  ViewController1.h
//  testing
//
//  Created by zenwise mac 2 on 11/14/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomePage.h"

@interface ViewController1 : UIViewController<UIWebViewDelegate>

@property NSURL * url;
@property NSString * myString;





@property (weak, nonatomic) IBOutlet UIWebView *webView;


@property NSURLSession * session;


@property NSMutableURLRequest * urlRequest;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property NSURLSessionDataTask * task1;

- (IBAction)reloadBtn:(id)sender;

@property NSMutableDictionary * responseDict;

@end
