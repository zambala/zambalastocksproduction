//
//  StockView.h
//  testing
//
//  Created by zenwise mac 2 on 11/24/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StockView : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *detailsTable;
@end
