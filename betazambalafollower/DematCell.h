//
//  DematCell.h
//  testing
//
//  Created by zenwise technologies on 23/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DematCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
@property (weak, nonatomic) IBOutlet UILabel *QTYLabel;
@property (weak, nonatomic) IBOutlet UILabel *LPTLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;
@property (strong, nonatomic) IBOutlet UILabel *exchaneLbl;
- (IBAction)tradeAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *tradeBtn;
@property (weak, nonatomic) IBOutlet UILabel *avgPriceLbl;
@property (weak, nonatomic) IBOutlet UILabel *avgPurchaseLbl;
@property (weak, nonatomic) IBOutlet UIButton *modifyBtn;
@property (weak, nonatomic) IBOutlet UIView *modifyView;

@property (weak, nonatomic) IBOutlet UIView *editBtn;
@end
