//
//  ExploreMonks.h
//  PremiumServices
//
//  Created by zenwise mac 2 on 12/30/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExploreMonks : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *expMonksTbl;
@property NSMutableDictionary *responseDict;
@property (strong, nonatomic) IBOutlet UIView *bgView;

@end
