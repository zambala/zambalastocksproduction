//
//  TwoAnsViewController.m
//  2FAScreens
//
//  Created by Zenwise Technologies on 27/09/17.
//  Copyright © 2017 Zenwise Technologies. All rights reserved.
//

#import "TwoAnsViewController.h"
#import "AppDelegate.h"
#import "TagEncode.h"
#import "MT.h"
#import "TabBar.h"

@interface TwoAnsViewController ()
{
    AppDelegate * delegate1;
    MT * sharedManager;
    NSDictionary * loginReplyDict;
    NSMutableArray * inputArray;
    
}

@end

@implementation TwoAnsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.ans1TF.delegate=self;
    self.ans2TF.delegate=self;
    sharedManager=[MT Mt1];
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    inputArray=[[NSMutableArray alloc]init];
    [self.loginButton addTarget:self action:@selector(SecurityQuestions) forControlEvents:UIControlEventTouchUpInside];
    
    self.activityInd.hidden=YES;
    
    for(int i=0;i<delegate1.questionArray.count;i++)
    {
        if(i==0)
        {
        if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:1]])
        {
            self.question1Label.text=@"What was your favourite place to visit as a child?";
            
        }
        
        else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:2]])
        {
            self.question1Label.text=@"What is the name of your favourite pet?";
            
        }
            
        else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:3]])
        {
            self.question1Label.text=@"Which is your favourite color?";
            
        }
            
        else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:4]])
        {
            self.question1Label.text=@"Which is your favourite web browser?";
            
        }
            
        else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:5]])
        {
            self.question1Label.text=@"In which city were you born?";
            
        }
            
        else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:6]])
        {
            self.question1Label.text=@"Which is your favourite movie?";
            
        }
            
            
        }
            
            if(i==1)
            {
                if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:1]])
                {
                    self.question2Label.text=@"What was your favourite place to visit as a child?";
                    
                }
                
                else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:2]])
                {
                    self.question2Label.text=@"What is the name of your favourite pet?";
                    
                }
                
                else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:3]])
                {
                    self.question2Label.text=@"Which is your favourite color?";
                    
                }
                
                else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:4]])
                {
                    self.question2Label.text=@"Which is your favourite web browser?";
                    
                }
                
                else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:5]])
                {
                    self.question2Label.text=@"In which city were you born?";
                    
                }
                
                else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:6]])
                {
                    self.question2Label.text=@"Which is your favourite movie?";
                    
                }
                
            
        }
        
        
    }
    
    // Do any additional setup after loading the view.
}
    


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.ans1TF resignFirstResponder];
    [self.ans2TF resignFirstResponder];
   
    return true;
}
-(void)SecurityQuestions
{
    @try
    {
    if(self.ans1TF.text.length!=0&&self.ans2TF.text.length!=0)
    {
    [self.scrollView setContentOffset:CGPointZero animated:YES];
    self.activityInd.hidden=NO;
    [self.activityInd startAnimating];
    NSMutableArray * answersArray = [[NSMutableArray alloc]initWithObjects:self.ans1TF.text,self.ans2TF.text,nil];
//    NSString * ansString = [answersArray componentsJoinedByString:@","];
//    //NSLog(@"ansString:%@",ansString);
//    //PreLogin
//    delegate1.mtCheck=true;
//    TagEncode * tag1=[[TagEncode alloc]init];
//
//    [tag1 TagData:sharedManager.shMsgCode shValueMethod:sharedManager.SecurityQueAndAns];
//    [tag1 TagData:sharedManager.stUserID stringMethod:delegate1.mtClientId];
//    [tag1 TagData:sharedManager.stPassword stringMethod:delegate1.mtPassword];
//    [tag1 TagData:sharedManager.stNewPassword stringMethod:@""];
//    [tag1 TagData:sharedManager.inVersion intMethod:2];
//
//
//
//    for (int i=0; i<1; i++) {
//        NSString * ansString1 = [NSString stringWithFormat:@"%@",[answersArray objectAtIndex:i]];
//        [tag1 TagData:sharedManager.stSingleSecurityAnswer stringMethod:ansString1];
//        [tag1 TagData:sharedManager.nlTagFooter];
//
//    }
//
//    [tag1 GetBuffer];
//    [self newMessage];
        
        //new//
        
        TagEncode * enocde=[[TagEncode alloc]init];
        loginReplyDict=[[NSDictionary alloc]init];
        enocde.inputRequestString=@"twosecurityans";
       if(inputArray.count>0)
       {
           [inputArray removeAllObjects];
       }
        
        inputArray=answersArray;
        [inputArray addObject:delegate1.mtClientId];
        [inputArray addObject:delegate1.mtPassword];
        [inputArray addObject:[NSString stringWithFormat:@"%@multitradeapi/securityanswers",delegate1.mtBaseUrl]];
        
        
        [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            
            loginReplyDict=dict;
            
            NSString * msg=[[[loginReplyDict objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"loginstatus"];
            delegate1.accessToken=[[[loginReplyDict objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"token"];
            
             if([msg containsString:@"Login Successful"])
             {
                  dispatch_async(dispatch_get_main_queue(), ^{
                      delegate1.userID=delegate1.mtClientId;
                      delegate1.loginActivityStr=@"CLIENT";
                      NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
                      delegate1.zenwiseToken=delegate1.accessToken;
                      [loggedInUser setObject:delegate1.accessToken forKey:@"acesstoken"];
                      [loggedInUser setObject:delegate1.userID forKey:@"userid"];
                      [loggedInUser setObject:delegate1.brokerNameStr forKey:@"brokername"];
                       [loggedInUser setObject:delegate1.loginActivityStr forKey:@"loginActivityStr"];
                      [loggedInUser synchronize];
                 TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
                 
                 [self presentViewController:tabPage animated:YES completion:nil];
                  });
             }else
             {
                 UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:msg preferredStyle:UIAlertControllerStyleAlert];
                 
                 [self presentViewController:alert animated:YES completion:^{
                     
                 }];
                 
                 UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                     
                 }];
                 
                 [alert addAction:okAction];
             }
            
        }];
            
    }
    else
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Please enter missing fields" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
