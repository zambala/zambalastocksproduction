//
//  BrokerCell.m
//  testing
//
//  Created by zenwise technologies on 28/02/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "BrokerCell.h"

@implementation BrokerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.brokerView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16] CGColor];
    self.brokerView.layer.shadowOffset = CGSizeMake(0.0f,2.0f);
    self.brokerView.layer.shadowOpacity = 0.6f;
    self.brokerView.layer.shadowRadius = 3.0;
    self.brokerView.layer.masksToBounds = NO;
    self.brokerView.layer.cornerRadius=10.0f;
//    self.brokerView.clipsToBounds=YES;
    self.brokerImgView.layer.cornerRadius=self.brokerImgView.frame.size.width/2;
      self.brokerImgView.clipsToBounds=YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
