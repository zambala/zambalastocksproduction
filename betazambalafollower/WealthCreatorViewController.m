//
//  WealthCreatorViewController.m
//  betazambalafollower
//
//  Created by Zenwise Technologies on 22/03/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import "WealthCreatorViewController.h"
#import "WealthCreatorCollectionViewCell.h"
#import "HMSegmentedControl.h"

@interface WealthCreatorViewController ()
{
     HMSegmentedControl * segmentedControl;
    NSMutableArray * imagesArray;
    NSMutableArray * companyArray;
    NSMutableArray * symbolsArray;
}

@end

@implementation WealthCreatorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"WEALTH CREATOR";
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    imagesArray = [[NSMutableArray alloc]init];
    companyArray = [[NSMutableArray alloc]init];
    symbolsArray = [[NSMutableArray alloc]init];
    [imagesArray addObject:@"HCL.JPG"];
    [imagesArray addObject:@"HDFC.JPG"];
    [imagesArray addObject:@"HIND.JPG"];
    [imagesArray addObject:@"IO.JPG"];
    [imagesArray addObject:@"ITC.JPG"];
    [imagesArray addObject:@"KOTAK.JPG"];
    [imagesArray addObject:@"MARUTI.JPG"];
    [imagesArray addObject:@"RELIANCE.JPG"];
    [imagesArray addObject:@"TCS.JPG"];
    
    [companyArray addObject:@"Company Name1"];
    [companyArray addObject:@"Company Name2"];
    [companyArray addObject:@"Company Name3"];
    [companyArray addObject:@"Company Name4"];
    [companyArray addObject:@"Company Name5"];
    [companyArray addObject:@"Company Name6"];
    [companyArray addObject:@"Company Name7"];
    [companyArray addObject:@"Company Name8"];
    [companyArray addObject:@"Company Name9"];
    
    [symbolsArray addObject:@"SYMBOL1"];
    [symbolsArray addObject:@"SYMBOL2"];
    [symbolsArray addObject:@"SYMBOL3"];
    [symbolsArray addObject:@"SYMBOL4"];
    [symbolsArray addObject:@"SYMBOL5"];
    [symbolsArray addObject:@"SYMBOL6"];
    [symbolsArray addObject:@"SYMBOL7"];
    [symbolsArray addObject:@"SYMBOL8"];
    [symbolsArray addObject:@"SYMBOL9"];
    
    
   
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"25 YR BIGGEST",@"10 YR CONSISTENT",@"5 YR BIGGEST",@"5 YR FASTEST"]];
    segmentedControl.frame = CGRectMake(0, 0, self.view.frame.size.width, 50);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    //    segmentedControl.verticalDividerEnabled = YES;
    //    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(251/255.0) green:(196/255.0) blue:(12/255.0) alpha:1];
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
    segmentedControl.backgroundColor=[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1];
    segmentedControl.tintColor=[UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:1];
    [self.view addSubview:segmentedControl];
    
    self.wealthCreatorCollectionView.delegate=self;
    self.wealthCreatorCollectionView.dataSource=self;
    [self.wealthCreatorCollectionView reloadData];
    
    // Do any additional setup after loading the view.
}

-(void)segmentedControlChangedValue
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    @try
    {
    return imagesArray.count;
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
        WealthCreatorCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WealthCreatorCollectionViewCell" forIndexPath:indexPath];
    cell.companyNameLabel.text = [NSString stringWithFormat:@"%@",[companyArray objectAtIndex:indexPath.row]];
    cell.logoImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[imagesArray objectAtIndex:indexPath.row]]];
    cell.symbolLabel.text = [NSString stringWithFormat:@"%@",[symbolsArray objectAtIndex:indexPath.row]];
    return cell;
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width/2 -10 , (collectionView.frame.size.height-100)/2);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
