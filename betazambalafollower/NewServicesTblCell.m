//
//  NewServicesTblCell.m
//  PremiumServices
//
//  Created by zenwise mac 2 on 7/19/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "NewServicesTblCell.h"

@implementation NewServicesTblCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    

    self.subscribeBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.subscribeBtn.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.subscribeBtn.layer.shadowOpacity = 1.0f;
    self.subscribeBtn.layer.shadowRadius = 1.0f;
    self.subscribeBtn.layer.cornerRadius=2.1f;
    self.subscribeBtn.layer.masksToBounds = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onInfiBtnTap:(id)sender {
}
@end
