//
//  TopPicks.h
//  PayPage
//
//  Created by zenwise mac 2 on 12/22/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "HVTableView.h"

@interface TopPicks : UIViewController<UITableViewDelegate, UITableViewDataSource,UIScrollViewDelegate>


@property (strong, nonatomic) IBOutlet UITableView *topPicksTbl;



@property (strong, nonatomic) IBOutlet UIImageView *profileImg;

@property(nonatomic, retain) NSMutableDictionary * topPicksResponseArray;
@property NSMutableDictionary * potentialGainersDictionary;
@property NSMutableDictionary * topPerformersArray;
@property Reachability * reach;
@property NSMutableArray * finalResponseArrayGainers;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, assign) CGFloat lastContentOffset;
@property (weak, nonatomic) IBOutlet UIView *topPerformersView;
@property (weak, nonatomic) IBOutlet UITextField *searchFld;
@property (weak, nonatomic) IBOutlet UIScrollView *topPerformsScrollView;
@property (weak, nonatomic) IBOutlet UIView *topPerforView;
@property (weak, nonatomic) IBOutlet UIButton *blueChipsButton;
@property (weak, nonatomic) IBOutlet UIButton *bearishButton;
@property (weak, nonatomic) IBOutlet UIButton *allResultsButton;
@property (weak, nonatomic) IBOutlet UIButton *bullishButton;
@property (weak, nonatomic) IBOutlet HVTableView *topPerformersTblView;
@property  NSString * searchStr;
- (IBAction)onInfoTap:(id)sender;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *infoButton;


@end
