//
//  FundTransferView.m
//  
//
//  Created by zenwise technologies on 19/12/16.
//
//

#import "FundTransferView.h"
#import "TabBar.h"
#import "AppDelegate.h"
#import "FundsViewController.h"
#import "UserDetailsView.h"
@interface FundTransferView ()
{
    AppDelegate * delegate1;
    NSMutableArray * segmentsArray;
    NSString * segmentString;
}

@end

@implementation FundTransferView

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    segmentsArray=[[NSMutableArray alloc]init];
    //NSLog(@"Test log");
   // [segmentsArray addObject:@"Equities"];
//    self.segmentPickerView.delegate=self;
//    self.segmentPickerView.dataSource=self;
//    [self.segmentPickerView reloadAllComponents];
    
   // [segmentsArray addObject:@"Commodities"];
    // Do any additional setup after loading the view.
//    self.fundTransferView1.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
//    self.fundTransferView1.layer.shadowOffset = CGSizeMake(0, 2.0f);
//    self.fundTransferView1.layer.shadowOpacity = 1.0f;
//    self.fundTransferView1.layer.shadowRadius = 1.0f;
//    self.fundTransferView1.layer.cornerRadius=2.1f;
//    self.fundTransferView1.layer.masksToBounds = YES;
    
       
//    self.payBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
//    self.payBtn.layer.shadowOffset = CGSizeMake(0, 2.0f);
//    self.payBtn.layer.shadowOpacity = 1.0f;
//    self.payBtn.layer.shadowRadius = 1.0f;
//    self.payBtn.layer.cornerRadius=2.1f;
//    self.payBtn.layer.masksToBounds = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    [self.netBankingBtn setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    [self.netBankingBtn setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    [self.upiBtn setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    [self.upiBtn setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    
    [self.bankNameTF setUserInteractionEnabled:NO];
  //  [self.bankAccountTF setUserInteractionEnabled:YES];
    self.doneButton.hidden=YES;
    
    self.segmentPickerView.hidden=YES;
    [self.netBankingBtn addTarget:self action:@selector(netBankingAction) forControlEvents:UIControlEventTouchUpInside];
    [self.doneButton addTarget:self action:@selector(onDoneButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self netBankingAction];
    if([delegate1.brokerNameStr isEqualToString:@"Zerodha"])
    {
        
    }else if([delegate1.brokerNameStr isEqualToString:@"Upstox"])
    {
        self.bankAccountTF.text = delegate1.upstoxUserBankAccount;
        self.bankNameTF.text = delegate1.upstoxUserBankName;
        if(segmentsArray.count>0)
        {
            [segmentsArray removeAllObjects];
        }
        [segmentsArray addObject:@"EQUITY"];
        [segmentsArray addObject:@"COMMODITIES"];
    }else
    {
        [self getDetails];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dismissKeyboard
{
    [self.bankNameTF resignFirstResponder];
    [self.bankAccountTF resignFirstResponder];
    [self.amoutTxtFld resignFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)showingMainView:(id)sender {
    
//    TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
//
//    [self presentViewController:tabPage animated:YES completion:nil];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
//    FundsViewController * funds = [self.storyboard instantiateViewControllerWithIdentifier:@"FundsViewController"];
//    [self presentViewController:funds animated:YES completion:nil];
}

- (IBAction)segmentBtn:(id)sender {
    
    CGPoint bottomOffset = CGPointMake(0, self.fundScrollView.contentSize.height - self.fundScrollView.bounds.size.height);
    [self.fundScrollView setContentOffset:bottomOffset animated:YES];
    self.doneButton.hidden=NO;
    self.segmentPickerView.hidden=NO;
    self.segmentPickerView.delegate=self;
    self.segmentPickerView.dataSource=self;
    [self.segmentPickerView reloadAllComponents];
   
}
- (IBAction)upiBtnAction:(id)sender {
    
   
    self.netBankingBtn.selected=NO;
    self.upiBtn.selected=YES;
    delegate1.paymentString=@"UPI";
    //    self.selectionViewHgt.constant=270;
    self.paymentViewHgt.constant=270;
    self.enterVpaLbl.hidden=NO;
    self.vpaFld.hidden=NO;
    self.destinationVpa.hidden=NO;
    self.targetVpaLbl.hidden=NO;
}
- (IBAction)makePayementBtn:(id)sender {
    @try
    {
    
    if(self.amoutTxtFld.text.length>0&&self.bankAccountTF.text.length>0&&self.bankNameTF.text.length>0)
    {
        if([delegate1.brokerNameStr isEqualToString:@"Upstox"])
        {
            UserDetailsView * view=[self.storyboard instantiateViewControllerWithIdentifier:@"UserDetailsView"];
            segmentString = [NSString stringWithFormat:@"%@",self.segmentBtnOutlet.titleLabel.text];
            view.segment = segmentString;
            view.txnAmount= self.amoutTxtFld.text;
            view.upiAlias = self.vpaFld.text;
            view.typeOfPayment=@"payin";
            view.bankName = self.bankNameTF.text;
            [self presentViewController:view animated:YES completion:nil];
        }else
        {
        
        //if(self.vpaFld.text.length>0&&[delegate1.paymentString isEqualToString:@"UPI"])
        //{
        UserDetailsView * view=[self.storyboard instantiateViewControllerWithIdentifier:@"UserDetailsView"];
        view.typeOfPayment=@"payin";
        view.upiAlias= self.vpaFld.text;
//        @property NSString * accountIFSC;
//        @property NSString * accountNo;
//        @property NSString * segment;
//        @property NSString * txnAmount;
//        @property NSString * typeOfPayment;
//        @property NSString * upiAlias;
        
        view.accountIFSC=[[[self.multiTradeResponseDictionary objectForKey:@"Banks"]objectAtIndex:0] objectForKey:@"IFSCCode"];
        view.accountNo = [[[self.multiTradeResponseDictionary objectForKey:@"Banks"]objectAtIndex:0] objectForKey:@"Account"];
            view.bankName = [[[self.multiTradeResponseDictionary objectForKey:@"Banks"] objectAtIndex:0] objectForKey:@"BankName"];
        segmentString = [NSString stringWithFormat:@"%@",self.segmentBtnOutlet.titleLabel.text];
        view.segment = segmentString;
        view.txnAmount= self.amoutTxtFld.text;
            view.upiAlias = self.vpaFld.text;
        [self presentViewController:view animated:YES completion:nil];
        }
        //[self.navigationController pushViewController:view animated:YES];
        //}else
        //{
          //  UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Please enter VPA." preferredStyle:UIAlertControllerStyleAlert];
            //UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                // Ok action example
           // }];
           // [alert addAction:okAction];
            
           // [self presentViewController:alert animated:YES completion:nil];
       // }
    }else
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Please fill the empty fields" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            // Ok action example
        }];
        [alert addAction:okAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
//    ViewController * view=[self.storyboard instantiateViewControllerWithIdentifier:@"view1"];
//    view.payInCheck=@"payin";
//    [self.navigationController pushViewController:view animated:YES];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

}

-(void)onDoneButtonTap
{
    self.doneButton.hidden=YES;
    [self.fundScrollView setContentOffset:CGPointZero animated:YES];
    self.segmentPickerView.hidden=YES;
}

-(void)netBankingAction
{
    self.netBankingBtn.selected=YES;
    self.upiBtn.selected=NO;
    delegate1.paymentString=@"NET";
    self.paymentViewHgt.constant=145;
    self.enterVpaLbl.hidden=YES;
    self.vpaFld.hidden=YES;
    self.destinationVpa.hidden=YES;
    self.targetVpaLbl.hidden=YES;
}

//picker view//

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
        return segmentsArray.count;
    
}
- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    NSString * title=segmentsArray[0];
    
    [self.segmentBtnOutlet setTitle:title forState:UIControlStateNormal];
    return segmentsArray[row];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
   // NSInteger selectRow = [self.segmentPickerView selectedRowInComponent:0];
    
    NSString * title=segmentsArray[row];
        
    [self.segmentBtnOutlet setTitle:title forState:UIControlStateNormal];
    //[[self view] endEditing:YES];
    
}
-(void)getDetails
{
    @try
    {
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                                };
    NSDictionary *parameters = @{ @"UserID": delegate1.userID };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://103.197.51.55:3375/api/PaymentGateway"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if(data !=nil)
                                                    {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        self.multiTradeResponseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        //NSLog(@"Multitrade ResponseDict:%@",self.multiTradeResponseDictionary);
                                                        if(segmentsArray.count>0)
                                                        {
                                                            [segmentsArray removeAllObjects];
                                                        }
                                                        
                                                        [segmentsArray addObjectsFromArray:[self.multiTradeResponseDictionary objectForKey:@"ProductType"]];
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            NSString * bankName = [NSString stringWithFormat:@"%@",[[[self.multiTradeResponseDictionary objectForKey:@"Banks"]objectAtIndex:0] objectForKey:@"BankName"]];
                                                            
                                                            self.bankNameTF.text = bankName;
                                                            NSString * bankAccount = [NSString stringWithFormat:@"%@",[[[self.multiTradeResponseDictionary objectForKey:@"Banks"]objectAtIndex:0] objectForKey:@"Account"]];
                                                            
                                                            self.bankAccountTF.text = bankAccount;
                                                            self.segmentPickerView.hidden=YES;
                                                            self.segmentPickerView.delegate=self;
                                                            self.segmentPickerView.dataSource=self;
                                                            [self.segmentPickerView reloadAllComponents];
                                                            
                                                            
                                                        });
                                                    }
                                                        
                                                    }
                                                        
                                                        else
                                                        {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                                
                                                                [self presentViewController:alert animated:YES completion:^{
                                                                    
                                                                }];
                                                                
                                                                
                                                                UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                    [self getDetails];
                                                                }];
                                                                
                                                                UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                    
                                                                }];
                                                                
                                                                
                                                                [alert addAction:retryAction];
                                                                [alert addAction:cancel];
                                                                
                                                            });
                                                            
                                                        }
                                                       
                                                    
                                                }];
    [dataTask resume];
    
}
@catch (NSException * e) {
    //NSLog(@"Exception: %@", e);
}
@finally {
    //NSLog(@"finally");
}


}


@end
