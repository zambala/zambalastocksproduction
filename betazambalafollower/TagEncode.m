                   //
//  TagEncode.m
//  betazambalafollower
//
//  Created by zenwise technologies on 29/05/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "TagEncode.h"
#import "MT.h"
#import "AppDelegate.h"
#import "BrokerViewNavigation.h"


@interface TagEncode ()
{
    short  shDefaultSize;
    
    short  shIndex_;
    
    
    int inBufferSize_;
    MT *sharedManager;
    NSMutableArray * btBuffer_;
    NSMutableArray * newBtBuffer_;
    NSMutableArray * btStringBuffer;
    AppDelegate * delegate1;
    NSUserDefaults *loggedInUser;
    uint8_t *bytesValues;
   
    

    
}

@end

@implementation TagEncode

- (void)viewDidLoad {
    [super viewDidLoad];
    
    

    
    
    
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)inputRequestMethod:(NSMutableArray *)inputArray withCompletionHandler:(void (^)(NSDictionary *))handler
{
    @try
    {
   
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSUserDefaults *loggedInUserNew = [NSUserDefaults standardUserDefaults];
//    NSString * guestToken=[loggedInUserNew objectForKey:@"guestaccess"];
    NSDictionary *headers;
//    if(guestToken==nil)
//    {
         headers = @{ @"content-type": @"application/json",
                                   @"cache-control": @"no-cache",
                                   @"Accept":@"application/json",
                                   @"deviceid":delegate1.currentDeviceId,
                                   };
//    }
//    else
//    {
//        headers = @{ @"content-type": @"application/json",
//                     @"cache-control": @"no-cache",
//                     @"Accept":@"application/json",
//                     @"deviceid":delegate1.currentDeviceId,
//                     @"authtoken":[loggedInUserNew objectForKey:@"guestaccess"]
//                     
//                     };
//    }
    
    NSString * urlString;
    NSString * brokerid=[delegate1.brokerInfoDict objectForKey:@"brokerid"];
    
 
    
    NSDictionary * params=[[NSDictionary alloc]init];
    @try {
        

        if([self.inputRequestString isEqualToString:@"prelogin"])
        {
            if([delegate1.brokerNameStr isEqualToString:@"Proficient"])
            {
                NSString * newPassword = [NSString stringWithFormat:@"%@",[inputArray objectAtIndex:3]];
             if(newPassword.length>0)
             {
                 if(delegate1.mtNewUserBool ==true)
                 {
             params=@{@"clientid":[inputArray objectAtIndex:0],
                      @"password":[inputArray objectAtIndex:1],
                      @"newpassword":[inputArray objectAtIndex:3],
                      @"brokerid":brokerid,
                      @"deviceid":delegate1.currentDeviceId1,
                      @"isforgotpassword":@(false)
                      };
                 }
                 else
                 {
                     
                         params=@{@"clientid":[inputArray objectAtIndex:0],
                                  @"password":[inputArray objectAtIndex:1],
                                  @"newpassword":[inputArray objectAtIndex:3],
                                  @"brokerid":brokerid,
                                  @"deviceid":delegate1.currentDeviceId1,
                                  @"isforgotpassword":@(true)
                                  };
                     
                 
                     
                     
                 }
                 delegate1.mtNewUserBool = false;
             }
                else
                {
                    
                    params=@{@"clientid":[inputArray objectAtIndex:0],
                             @"password":[inputArray objectAtIndex:1],
                             @"newpassword":[inputArray objectAtIndex:3],
                             @"brokerid":brokerid,
                             @"deviceid":delegate1.currentDeviceId1,
                             @"isforgotpassword":@(false)
                             };
                }
            }
            else
            {
                params=@{@"clientid":[inputArray objectAtIndex:0],
                         @"password":[inputArray objectAtIndex:1],
                         @"newpassword":[inputArray objectAtIndex:3],
                         @"brokerid":brokerid,
                         @"deviceid":delegate1.currentDeviceId1
                        
                         };
            }
            
            urlString=[inputArray objectAtIndex:2];
        }
        else if([self.inputRequestString isEqualToString:@"markasread"])
        {
            
            params=@{@"mobilenumber":[inputArray objectAtIndex:0],
                     @"token":[inputArray objectAtIndex:1]
                     
                     };
            
            urlString=[inputArray objectAtIndex:2];
        }
        else if([self.inputRequestString isEqualToString:@"generateOtp"])
        {
            
            params=@{@"mobilenumber":[inputArray objectAtIndex:0],

                    };
            
            urlString=[inputArray objectAtIndex:1];
        }
        else if([self.inputRequestString isEqualToString:@"forgotpassword"])
        {
            
            params=@{@"clientid":[inputArray objectAtIndex:0],
                     @"email":[inputArray objectAtIndex:1],
                  
                     };
            
            urlString=[inputArray objectAtIndex:2];
        }
        else if([self.inputRequestString isEqualToString:@"closebanner"])
        {
            
            if([[inputArray objectAtIndex:1] isEqualToString:@"0"])
            {
                params=@{@"mobilenumber":[inputArray objectAtIndex:0],
                         @"notificationid":[inputArray objectAtIndex:2],
                         @"isacted":@(false),
                         @"authtoken":[inputArray objectAtIndex:3],
                          @"clientid":[inputArray objectAtIndex:5]
                         };
            }
            else if([[inputArray objectAtIndex:1] isEqualToString:@"1"])
            {
                params=@{@"mobilenumber":[inputArray objectAtIndex:0],
                         @"notificationid":[inputArray objectAtIndex:2],
                         @"isacted":@(true),
                         @"authtoken":[inputArray objectAtIndex:3],
                           @"clientid":[inputArray objectAtIndex:5]
                         };
            }
           
            
            urlString=[inputArray objectAtIndex:4];
        }
        else if([self.inputRequestString isEqualToString:@"verifyemail"])
        {
//            [prefs objectForKey:@"devicetoken"]
             NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            params = @{ @"clientmemberid":[inputArray objectAtIndex:0],
                        @"referralcode":[inputArray objectAtIndex:1],
                        @"brokerid":[inputArray objectAtIndex:2],
                        @"email":[inputArray objectAtIndex:3],
                        @"firstname":[inputArray objectAtIndex:4],
                        @"lastname":@"",
                        @"countrycode":[inputArray objectAtIndex:5],
                        @"mobilenumber":[inputArray objectAtIndex:0],
                        @"notificationinfo": @{ @"deviceid": delegate1.currentDeviceId,@"devicetoken":@"wdf",
                                                @"devicetype":@1
                                                }
                        };
            
            urlString=[inputArray objectAtIndex:6];
        }
       
        else if([self.inputRequestString isEqualToString:@"updateuserrecord"])
        {
            
            params=@{
                     @"name":[inputArray objectAtIndex:0],
                     @"email":[inputArray objectAtIndex:1],
                     @"mobilenumber":[inputArray objectAtIndex:2],
                     @"brokername":[inputArray objectAtIndex:3],
                     @"brokerid":[inputArray objectAtIndex:4],
                     @"authtoken":[inputArray objectAtIndex:5]
                     };
            
            urlString=[inputArray objectAtIndex:6];
        }
        else if([self.inputRequestString isEqualToString:@"funds"])
        {
            
            params=@{@"clientid":[inputArray objectAtIndex:1],
                     @"token":[inputArray objectAtIndex:0],
                     @"brokerid":[delegate1.brokerInfoDict objectForKey:@"brokerid"]
                     };
            
            urlString=[inputArray objectAtIndex:2];
        }
        
        else if([self.inputRequestString isEqualToString:@"submitAnswer"])
        {
            
            params=@{@"userid":[inputArray objectAtIndex:0],
                     @"answer":[inputArray objectAtIndex:1],
                     @"deviceid":[inputArray objectAtIndex:2],
                     @"questionid":[inputArray objectAtIndex:3],
                     @"authtoken":[inputArray objectAtIndex:5]
                     };
            
            urlString=[inputArray objectAtIndex:4];
        }
        else if([self.inputRequestString isEqualToString:@"redeem"])
        {
            
            params=@{@"mobilenumber":[inputArray objectAtIndex:1],
                     @"email":[inputArray objectAtIndex:2],
                     @"name":[inputArray objectAtIndex:3],
                     @"clientid":[inputArray objectAtIndex:4]
                     };
            
            urlString=[inputArray objectAtIndex:0];
        }
//        @"devicetoken":[prefs objectForKey:@"devicetoken"]
        else if([self.inputRequestString isEqualToString:@"clientcreation"])
        {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString * brokerId=[NSString stringWithFormat:@"%@",[inputArray objectAtIndex:2]];
            if(brokerId.length>0)
            {
            params = @{ @"clientmemberid":[inputArray objectAtIndex:0],
                                          @"referralcode":[inputArray objectAtIndex:1],
                                          @"brokerid":[inputArray objectAtIndex:2],
                                          @"email":[inputArray objectAtIndex:3],
                                          @"firstname":[inputArray objectAtIndex:4],
                                          @"lastname":@"",
                                          @"countrycode":[inputArray objectAtIndex:5],
                                          @"mobilenumber":[inputArray objectAtIndex:7],
                                          @"notificationinfo": @{ @"deviceid": delegate1.currentDeviceId,@"devicetoken":@"wdf",
                                                                  @"devicetype":@1
                                                                  }
                                          };
            
            urlString=[inputArray objectAtIndex:6];
            }
            else
            {
                params = @{ @"clientmemberid":[inputArray objectAtIndex:0],
                            @"referralcode":[inputArray objectAtIndex:1],
                            @"brokerid":[inputArray objectAtIndex:2],
                            @"email":[inputArray objectAtIndex:3],
                            @"firstname":[inputArray objectAtIndex:4],
                            @"lastname":@"",
                            @"countrycode":[inputArray objectAtIndex:5],
                            @"mobilenumber":[inputArray objectAtIndex:7],
                            @"otp":[inputArray objectAtIndex:8],
                            @"notificationinfo": @{ @"deviceid": delegate1.currentDeviceId,@"devicetoken":@"abcd",
                                                    @"devicetype":@1
                                                    }
                            };
                
                urlString=[inputArray objectAtIndex:6];
            }
        }
        else if([self.inputRequestString isEqualToString:@"twosecurityans"])
        {
           
            NSMutableArray * localArray=[[NSMutableArray alloc]init];
            [localArray addObject:[inputArray objectAtIndex:0]];
             [localArray addObject:[inputArray objectAtIndex:1]];
            
            params=@{@"clientid":[inputArray objectAtIndex:2],
                     @"password":[inputArray objectAtIndex:3],
                     @"answers":localArray,
                     @"brokerid":brokerid,
                     @"deviceid":delegate1.currentDeviceId1
                     };
            
            urlString=[inputArray objectAtIndex:4];
            
           
        }
        else if([self.inputRequestString isEqualToString:@"sixques"])
        {
            
            NSMutableArray * localArray=[[NSMutableArray alloc]init];
            [localArray addObject:[inputArray objectAtIndex:0]];
            [localArray addObject:[inputArray objectAtIndex:1]];
             [localArray addObject:[inputArray objectAtIndex:2]];
             [localArray addObject:[inputArray objectAtIndex:3]];
             [localArray addObject:[inputArray objectAtIndex:4]];
             [localArray addObject:[inputArray objectAtIndex:5]];
           
            
            params=@{@"clientid":[inputArray objectAtIndex:6],
                     @"password":[inputArray objectAtIndex:7],
                     @"answers":localArray,
                     @"brokerid":brokerid,
                     @"deviceid":delegate1.currentDeviceId1
                     };
            
            urlString=[inputArray objectAtIndex:8];
            
            
        }
        
        
        else if([self.inputRequestString isEqualToString:@"trade"])
        {
            
           
            
            params=@{@"token":[inputArray objectAtIndex:0],
                    @"clientid":[inputArray objectAtIndex:1],
                    @"brokerid":brokerid
                     };
            
            urlString=[inputArray objectAtIndex:2];
            
            
        }
        
        else if([self.inputRequestString isEqualToString:@"cancelrequest"])
        {
            
            
            params=@{@"token":[inputArray objectAtIndex:0],
                     @"clientid":[inputArray objectAtIndex:1],
                     @"atinorderno":[inputArray objectAtIndex:2],
                     @"brokerid":brokerid
                     };
            
            urlString=[inputArray objectAtIndex:3];
            
            
        }
        
      
        else if([self.inputRequestString isEqualToString:@"order"])
        {
            if(inputArray.count==13)
            {
            
            

            params=@{@"token":[inputArray objectAtIndex:0],
                     @"clientid":[inputArray objectAtIndex:1],
                     @"exchange":[inputArray objectAtIndex:2],
                     @"securityid":[inputArray objectAtIndex:3],
                     @"orderqty":[inputArray objectAtIndex:4],
                     @"pendingqty":[inputArray objectAtIndex:5],
                     @"price":[inputArray objectAtIndex:6],
                     @"transactiontype":[inputArray objectAtIndex:7],
                     @"ordertype":[inputArray objectAtIndex:8],
                     @"triggerprice":[inputArray objectAtIndex:12],
                     @"brokerid":brokerid,
                     @"symbolname":[inputArray objectAtIndex:10],
                     @"inProductType":[inputArray objectAtIndex:11]
                     };
            }
            else
            {
                params=@{@"token":[inputArray objectAtIndex:0],
                         @"clientid":[inputArray objectAtIndex:1],
                         @"exchange":[inputArray objectAtIndex:2],
                         @"securityid":[inputArray objectAtIndex:3],
                         @"orderqty":[inputArray objectAtIndex:4],
                         @"pendingqty":[inputArray objectAtIndex:5],
                         @"price":[inputArray objectAtIndex:6],
                         @"transactiontype":[inputArray objectAtIndex:7],
                         @"ordertype":[inputArray objectAtIndex:8],
                         @"brokerid":brokerid,
                         @"symbolname":[inputArray objectAtIndex:10],
                         @"inProductType":[inputArray objectAtIndex:11]
                         
                         };
            }
            urlString=[inputArray objectAtIndex:9];
            
            
            
        }
        
         else if([self.inputRequestString isEqualToString:@"modifyorder"])
         {
            
             
        
        
        if(inputArray.count==10)
        {
            
            
            
            params=@{@"token":[inputArray objectAtIndex:0],
                     @"clientid":[inputArray objectAtIndex:1],
                     @"atinorderno":[inputArray objectAtIndex:2],
                     @"orderqty":[inputArray objectAtIndex:3],
                     @"pendingqty":[inputArray objectAtIndex:4],
                     @"price":[inputArray objectAtIndex:5],
                     @"transactiontype":[inputArray objectAtIndex:6],
                     @"ordertype":[inputArray objectAtIndex:7],
                     @"triggerprice":[inputArray objectAtIndex:9],
                     @"brokerid":brokerid
                     };
        }
        else
        {
            params=@{@"token":[inputArray objectAtIndex:0],
                     @"clientid":[inputArray objectAtIndex:1],
                     @"atinorderno":[inputArray objectAtIndex:2],
                     @"orderqty":[inputArray objectAtIndex:3],
                     @"pendingqty":[inputArray objectAtIndex:4],
                     @"price":[inputArray objectAtIndex:5],
                     @"transactiontype":[inputArray objectAtIndex:6],
                     @"ordertype":[inputArray objectAtIndex:7],
                     @"brokerid":brokerid
                     };
        }
        urlString=[inputArray objectAtIndex:8];
                
         }else if ([self.inputRequestString isEqualToString:@"init"])
         {
            NSUserDefaults *loggedInUserNew = [NSUserDefaults standardUserDefaults];
             urlString = [inputArray objectAtIndex:0];
             if([[loggedInUserNew objectForKey:@"loginActivityStr"] isEqualToString:@"OTP1"])
             {
                 params=@{@"deviceid"   :[inputArray objectAtIndex:1],
                          @"versioninfo":[inputArray objectAtIndex:2],
                          @"devicetype":[inputArray objectAtIndex:3],
                          @"authtoken":[inputArray objectAtIndex:4],
                          @"clientid":[inputArray objectAtIndex:5]
                          };
             }
             else
             {
                 if(delegate1.accessToken.length>0)
                 {
                     params=@{@"deviceid"   :[inputArray objectAtIndex:1],
                              @"versioninfo":[inputArray objectAtIndex:2],
                              @"devicetype":[inputArray objectAtIndex:3],
                              @"authtoken":delegate1.accessToken,
                               @"clientid":[inputArray objectAtIndex:5]
                              };
                 }else
                 {
                     params=@{@"deviceid"   :[inputArray objectAtIndex:1],
                              @"versioninfo":[inputArray objectAtIndex:2],
                              @"devicetype":[inputArray objectAtIndex:3],
                              @"authtoken":@"",
                               @"clientid":[inputArray objectAtIndex:5]
                              };
                 }
             }
            
         }else if ([self.inputRequestString isEqualToString:@"logout"])
         {
             params = @{@"token":[inputArray objectAtIndex:1],
                        @"clientid":[inputArray objectAtIndex:2],
                        @"deviceid":[inputArray objectAtIndex:3],
                        @"brokerid":[inputArray objectAtIndex:4]
                        };
             urlString = [inputArray objectAtIndex:0];
         }
        
         else if ([self.inputRequestString isEqualToString:@"resourcelist"])
         {
             params = @{
                       
                        };
             urlString = [inputArray objectAtIndex:0];
         }
         else if ([self.inputRequestString isEqualToString:@"guide"])
         {
             params = @{
                        };
             urlString = [inputArray objectAtIndex:0];
         }
        else if([self.inputRequestString isEqualToString:@"contestquestions"])
        {
            
            params=@{@"type":[inputArray objectAtIndex:0],
                     @"terms":[inputArray objectAtIndex:1],
                     
                     };
            
            urlString=[inputArray objectAtIndex:2];
        }
        else if([self.inputRequestString isEqualToString:@"usawealth"])
        {
            NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
            NSString*number= [loggedInUser stringForKey:@"profilemobilenumber"];
            params=@{@"clientid":[inputArray objectAtIndex:0],
                     @"mobilenumber":number
                     };
            
            urlString=[inputArray objectAtIndex:1];
        }
        
       
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    
    NSData * postData=[NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
   
    self.responseDict=[[NSDictionary alloc]init];
   
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:15.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    
                                                    if(data!=nil)
                                                    {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            
                                                            if([httpResponse statusCode]==403)
                                                            {
                                                                
                                                                NSMutableArray * array = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                
                                                                //NSLog(@"Success Output:%@",array);
                                                                NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
                                                                
                                                                [dict setValue:array forKey:@"data"];
                                                                if([self.inputRequestString isEqualToString:@"prelogin"])
                                                                {
                                                                    self.responseDict=[[NSDictionary alloc]init];
                                                                    
                                                                    self.responseDict=[dict mutableCopy];
                                                                    
                                                                    handler(self.responseDict);
                                                                }
                                                                loggedInUser = [NSUserDefaults standardUserDefaults];
                                                                loggedInUser=nil;
                                                              
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                
                                                            }
                                                            else if([httpResponse statusCode]==401||[httpResponse statusCode]==400)
                                                            {
                                                                
                                                                NSMutableArray * array = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                
                                                                //NSLog(@"Success Output:%@",array);
                                                                NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
                                                                
                                                                [dict setValue:array forKey:@"data"];
                                                                if([self.inputRequestString isEqualToString:@"prelogin"])
                                                                {
                                                                    self.responseDict=[[NSDictionary alloc]init];
                                                                    
                                                                    self.responseDict=[dict mutableCopy];
                                                                    
                                                                    handler(self.responseDict);
                                                                }
                                                                loggedInUser = [NSUserDefaults standardUserDefaults];
                                                                loggedInUser=nil;
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                
                                                            }
                                                            else if([httpResponse statusCode]==423)
                                                            {
                                                                
                                                                NSDictionary * dict= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                
                                                                //NSLog(@"Success Output:%@",array);
//                                                                NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
                                                                
                                                               
                                                                
                                                                
                                                                self.responseDict=[[NSDictionary alloc]init];
                                                                
                                                                self.responseDict=[dict mutableCopy];
                                                                
                                                                handler(self.responseDict);
                                                                
                                                            }
                                                            else
                                                            {
                                                           
                                                            NSMutableArray * array = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            
                                                                 //NSLog(@"Success Output:%@",array);
                                                            NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
                                                            
                                                            [dict setValue:array forKey:@"data"];
                                                            
                                                            
                                                            self.responseDict=[[NSDictionary alloc]init];
                                                            
                                                            self.responseDict=[dict mutableCopy];
                                                            
                                                            handler(self.responseDict);
                                                            }
                                                            
                                                        }
                                                        
                                            
                                                        
                                                        
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                         
                                                         
                                                            
                                                            
                                                        });
                                                        
                                                    }
                                                    
                                                    else
                                                    {
                                                        handler([NSDictionary new]);
                                                    }
                                                    
                                                    
                                                }];
    [dataTask resume];
    


    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)inputRequestMethod1:(NSMutableArray *)inputArray withCompletionHandler:(void (^)(NSDictionary *))handler
{
    @try
    {
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSString * urlString;
    
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"Accept":@"application/json",
                                  @"deviceid":delegate1.currentDeviceId
                               
                               };
      urlString=[inputArray objectAtIndex:0];
//
//    if([self.inputRequestString isEqualToString:@"brokerinfo"])
//    {
//        urlString=[inputArray objectAtIndex:0];
//    }
//    if([self.inputRequestString isEqualToString:@"walletpoints"])
//    {
//        urlString=[inputArray objectAtIndex:0];
//    }
//    if([self.inputRequestString isEqualToString:@"coupans"])
//    {
//        urlString=[inputArray objectAtIndex:0];
//    }
        if([self.inputRequestString isEqualToString:@"generateOtp"]||[self.inputRequestString isEqualToString:@"otpVerify"]||[self.inputRequestString isEqualToString:@"emailstatus"])
    {
       headers = @{ @"content-type": @"application/json",
                                   @"cache-control": @"no-cache",
                                   @"Accept":@"application/json"
                                   };
    }
    if([self.inputRequestString isEqualToString:@"upstoxrequest"])
    {
        NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate1.accessToken];
        headers = @{ @"x-api-key": delegate1.APIKey,
                               @"authorization": access,
                               };
    }
        NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
        
        NSString * number= [loggedInUser stringForKey:@"profilemobilenumber"];
    if([self.inputRequestString isEqualToString:@"getallkeys"])
    {
        headers = @{
                    @"deviceid":delegate1.currentDeviceId,
                    @"mobilenumber":number,
                    @"clientid":number
                    };
    }
    
    self.responseDict=[[NSDictionary alloc]init];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:15.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
   
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    
                                                    if(data!=nil)
                                                    {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            if([httpResponse statusCode]==403)
                                                            {
                                                                
                                                                NSMutableArray * array = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                
                                                                //NSLog(@"Success Output:%@",array);
                                                                NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
                                                                
                                                                [dict setValue:array forKey:@"data"];
                                                                if([self.inputRequestString isEqualToString:@"prelogin"])
                                                                {
                                                                    self.responseDict=[[NSDictionary alloc]init];
                                                                    
                                                                    self.responseDict=[dict mutableCopy];
                                                                    
                                                                    handler(self.responseDict);
                                                                }
                                                               
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                
                                                            }
                                                            else if([httpResponse statusCode]==401||[httpResponse statusCode]==400)
                                                            {
                                                                
                                                                NSMutableArray * array = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                
                                                                //NSLog(@"Success Output:%@",array);
                                                                NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
                                                                
                                                                [dict setValue:array forKey:@"data"];
                                                                if([self.inputRequestString isEqualToString:@"prelogin"])
                                                                {
                                                                    self.responseDict=[[NSDictionary alloc]init];
                                                                    
                                                                    self.responseDict=[dict mutableCopy];
                                                                    
                                                                    handler(self.responseDict);
                                                                }
                                                              
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                
                                                            }
                                                            else if([httpResponse statusCode]==200)
                                                            {
                                                            NSMutableArray * array = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            
                                                            NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
                                                            
                                                            [dict setValue:array forKey:@"data"];
                                                            
                                                            
                                                            self.responseDict=[[NSDictionary alloc]init];
                                                            
                                                            self.responseDict=[dict mutableCopy];
                                                            
                                                            handler(self.responseDict);
                                                            }
                                                        }
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            
                                                            
                                                            
                                                            
                                                        });
                                                        
                                                    }
                                                    else
                                                        
                                                    {
                                                        handler([NSDictionary new]);
                                                    }
                                                    
                                                    
                                                    
                                                }];
    [dataTask resume];
    
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    
    
    
    
    
}
-(void)bseBowInputRequestMethod:(NSMutableArray *)inputArray withCompletionHandler:(void (^)(NSDictionary *))handler
{
    @try
    {
        delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
        NSString * urlString;
        
        NSDictionary *headers = @{
                                   @"cache-control": @"no-cache",
                               
                                 
                                   };
        urlString=[inputArray objectAtIndex:0];
        
        
      
     
        
        self.responseDict=[[NSDictionary alloc]init];
        
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:15.0];
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        
        
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        
                                                        if(data!=nil)
                                                        {
                                                            if (error) {
                                                                //NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                //NSLog(@"%@", httpResponse);
                                                                if([httpResponse statusCode]==403)
                                                                {
                                                                    
                                                                    NSMutableArray * array = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                    
                                                                    //NSLog(@"Success Output:%@",array);
                                                                    NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
                                                                    
                                                                    [dict setValue:array forKey:@"data"];
                                                                    if([self.inputRequestString isEqualToString:@"prelogin"])
                                                                    {
                                                                        self.responseDict=[[NSDictionary alloc]init];
                                                                        
                                                                        self.responseDict=[dict mutableCopy];
                                                                        
                                                                        handler(self.responseDict);
                                                                    }
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                    
                                                                }
                                                                else if([httpResponse statusCode]==401||[httpResponse statusCode]==400)
                                                                {
                                                                    
                                                                    NSMutableArray * array = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                    
                                                                    //NSLog(@"Success Output:%@",array);
                                                                    NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
                                                                    
                                                                    [dict setValue:array forKey:@"data"];
                                                                    if([self.inputRequestString isEqualToString:@"prelogin"])
                                                                    {
                                                                        self.responseDict=[[NSDictionary alloc]init];
                                                                        
                                                                        self.responseDict=[dict mutableCopy];
                                                                        
                                                                        handler(self.responseDict);
                                                                    }
                                                                    
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                    
                                                                }
                                                                else if([httpResponse statusCode]==200)
                                                                {
                                                                  
                                                                   NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                                                   
                                                                   
                                                                    
                                                                   
                                                                    
                                                                    NSLog(@"%@",responseString);
                                                                  
                                                                    
                                                                  
                                                                    
                                                                    handler([self bseBowDecodeMethod:responseString]);
                                                                }
                                                            }
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                
                                                                
                                                                
                                                                
                                                            });
                                                            
                                                        }
                                                        else
                                                            
                                                        {
                                                            handler([NSDictionary new]);
                                                        }
                                                        
                                                        
                                                        
                                                    }];
        [dataTask resume];
        
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    
    
    
    
    
}
-(NSDictionary *)bseBowDecodeMethod:(NSString *)responseString
{
    NSArray * recordsArray = [[NSArray alloc]init];
    NSMutableDictionary * responseDict = [[NSMutableDictionary alloc]init];
    
   recordsArray = [responseString componentsSeparatedByString:@"~"];
    
    for(int i =0;i<recordsArray.count;i++)
    {
         NSArray * fieldArray =[[NSArray alloc]init];
         fieldArray = [[recordsArray objectAtIndex:i] componentsSeparatedByString:@"|"];
         [responseDict setObject:fieldArray forKey:[NSString stringWithFormat:@"Record%i",i+1]];
        
        
    }
    NSLog(@"%@",responseDict);
    return responseDict;
}

//-(void)screenNavigation:(NSDictionary *)inputDict{
//    
//    @try
//    {
//        if(delegate2.notificationInfo!=nil)
//        {
//            //NSLog(@"%@",delegate2.notificationInfo);
//            
//            NSString * destination=[NSString stringWithFormat:@"%@",[[delegate2.notificationInfo objectForKey:@"u"]objectForKey:@"origin"]];
//            if([destination isEqualToString:@"Dummy Positions"])
//            {
//                
//                delegate2.pushNotificationSection=@"Dummy Positions";
//                delegate2.notificationInfo=nil;
//                [self.tabBarController setSelectedIndex:3];
//            }
//            else if([destination isEqualToString:@"NOTIFICATIONS"])
//            {
//                
//                delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[[delegate2.notificationInfo objectForKey:@"u"]objectForKey:@"section"]];
//                delegate2.notificationInfo=nil;
//                if([delegate2.pushNotificationSection isEqualToString:@"INDIA_NOTIFICATIONS"])
//                {
//                    [self.tabBarController setSelectedIndex:1];
//                }
//                else if([delegate2.pushNotificationSection isEqualToString:@"USA_NOTIFICATIONS"])
//                {
//                    [self onUsaTap];
//                }
//                
//            }
//            else if([destination isEqualToString:@"ZAMBALA_GUIDE"])
//            {
//                
//                delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[[delegate2.notificationInfo objectForKey:@"u"]objectForKey:@"section"]];
//                delegate2.notificationInfo=nil;
//                if([delegate2.pushNotificationSection isEqualToString:@"INDIA_ZAMBALA_GUIDE"])
//                {
//                    [self.tabBarController setSelectedIndex:1];
//                }
//                else if([delegate2.pushNotificationSection isEqualToString:@"USA_ZAMBALA_GUIDE"])
//                {
//                    [self onUsaTap];
//                }
//                
//            }
//            else if([destination isEqualToString:@"INDIA_MARKET_WATCH"])
//            {
//                
//                delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[[delegate2.notificationInfo objectForKey:@"u"]objectForKey:@"section"]];
//                delegate2.notificationInfo=nil;
//                [self onWatchTap];
//                
//            }
//            else if([destination isEqualToString:@"US_MARKET_WATCH"])
//            {
//                
//                delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[[delegate2.notificationInfo objectForKey:@"u"]objectForKey:@"section"]];
//                delegate2.notificationInfo=nil;
//                [self onUsaTap];
//                
//            }
//            else if([destination isEqualToString:@"INDIA_TOP_PICKS"])
//            {
//                
//                delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[[delegate2.notificationInfo objectForKey:@"u"]objectForKey:@"section"]];
//                delegate2.notificationInfo=nil;
//                [self onTopClick];
//                
//            }
//            else if([destination isEqualToString:@"USA_TOP_PICKS"])
//            {
//                
//                delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[[delegate2.notificationInfo objectForKey:@"u"]objectForKey:@"section"]];
//                delegate2.notificationInfo=nil;
//                [self onUsaTap];
//                
//            }
//            
//            else if([destination isEqualToString:@"Equities"])
//            {
//                
//                delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[[delegate2.notificationInfo objectForKey:@"u"]objectForKey:@"section"]];
//                delegate2.notificationInfo=nil;
//                [self onEquityTap];
//            }
//            else if([destination isEqualToString:@"Derivatives"])
//            {
//                
//                delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[[delegate2.notificationInfo objectForKey:@"u"]objectForKey:@"section"]];
//                delegate2.notificationInfo=nil;
//                [self onDerivativeTap];
//            }
//            else if([destination isEqualToString:@"ZambalaTv"])
//            {
//                
//                delegate2.pushNotificationSection=@"ZambalaTv";
//                delegate2.notificationInfo=nil;
//                [self.tabBarController setSelectedIndex:4];
//            }
//            else if([destination isEqualToString:@"INDIA_OPEN_ACCOUNT"])
//            {
//                
//                delegate2.pushNotificationSection=@"OpenAccount";
//                delegate2.notificationInfo=nil;
//                [self onOpenTap];
//            }
//            else if([destination isEqualToString:@"USA_OPEN_ACCOUNT"])
//            {
//                
//                delegate2.pushNotificationSection=@"USAOpenAccount";
//                delegate2.notificationInfo=nil;
//                [self onUsaTap];
//            }
//            else if([destination isEqualToString:@"PlayStoreUpdate"])
//            {
//                
//                delegate2.pushNotificationSection=@"PlayStoreUpdate";
//                delegate2.notificationInfo=nil;
//                NSString *iTunesLink = @"https://itunes.apple.com/us/app/zambala-stocks-by-zenwise/id1281356740?mt=8";
//                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
//            }
//            else if([destination isEqualToString:@"Rewards"])
//            {
//                
//                delegate2.pushNotificationSection=@"Rewards";
//                delegate2.notificationInfo=nil;
//                WalletView * wallet=[self.storyboard instantiateViewControllerWithIdentifier:@"WalletView"];
//                
//                [self presentViewController:wallet animated:YES completion:nil];
//            }
//        }
//    }
//    @catch (NSException * e) {
//        //NSLog(@"Exception: %@", e);
//    }
//    @finally {
//        //NSLog(@"finally");
//    }
//    
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
