//
//  WealthTreeView.m
//  testing
//
//  Created by zenwise mac 2 on 11/23/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "WealthTreeView.h"
#import "AppDelegate.h"
#import "HMSegmentedControl.h"
#import "TagEncode.h"



@interface WealthTreeView ()
{
    
    
    AppDelegate * delegate1;
    NSString * referralCode;
    HMSegmentedControl * segmentedControl;
}
@end

@implementation WealthTreeView

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
     self.rewardsView.hidden=YES;
    self.downView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.downView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.downView.layer.shadowOpacity = 1.0f;
    self.downView.layer.shadowRadius = 1.0f;
    self.downView.layer.cornerRadius=1.0f;
    self.downView.layer.masksToBounds = NO;
    
//    self.inviteFriendsButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
//    self.inviteFriendsButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
//    self.inviteFriendsButton.layer.shadowOpacity = 1.0f;
//    self.inviteFriendsButton.layer.shadowRadius = 1.0f;
//    self.inviteFriendsButton.layer.cornerRadius=1.0f;
//    self.inviteFriendsButton.layer.masksToBounds = NO;
//
    [self.walletImgView setTintColor:[UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:1]];
//    self.walletImgView.tintColor=CFBridgingRelease([[UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:1]CGColor]);
    self.inviteFriendsButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.inviteFriendsButton.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.inviteFriendsButton.layer.shadowOpacity = 5.0f;
    self.inviteFriendsButton.layer.shadowRadius = 4.0f;
    self.inviteFriendsButton.layer.cornerRadius=4.1f;
    self.inviteFriendsButton.layer.masksToBounds = NO;
    
    
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"REFERRAL",@"REWARDS"]];
    segmentedControl.frame = CGRectMake(0, 88, self.view.frame.size.width, 40);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segmentedControl];
    [segmentedControl setSelectedSegmentIndex:0];
    [self getRefeeralCode];
    // Do any additional setup after loading the view.
    
    [self walletdetails];
   
}
-(void)walletdetails
{
    TagEncode * enocde=[[TagEncode alloc]init];
    enocde.inputRequestString=@"walletpoints";
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    [inputArray addObject:[NSString stringWithFormat:@"%@coupon/userwallet?clientid=%@&authtoken=%@",delegate1.baseUrl,delegate1.userID,delegate1.zenwiseToken]];
    
    [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
        
        //NSLog(@"%@",dict);
        NSArray * keysArray=[dict allKeys];
        
        
        if(keysArray.count>0)
        {
        NSString * status=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"status"]];
        if([status isEqualToString:@"success"])
        {
            NSString * points=[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"walletvalue"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.pointsLbl.text=points;
            });
            
        }
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                
                UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self walletdetails];
                }];
                
                UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                
                
                [alert addAction:retryAction];
                [alert addAction:cancel];
                
            });
            
        }
    }];
}

-(void)segmentedControlChangedValue
{
    if(segmentedControl.selectedSegmentIndex==0)
    {
        self.referalScroll.hidden=NO;
        self.rewardsView.hidden=YES;
    }
    else if(segmentedControl.selectedSegmentIndex==1)
    {
        self.referalScroll.hidden=YES;
        self.rewardsView.hidden=NO;
    }
}

-(void)getRefeeralCode
{

    @try
    {
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                                @"authtoken":delegate1.zenwiseToken,
                                @"deviceid":delegate1.currentDeviceId
                               };
    
    NSString * urlString=[NSString stringWithFormat:@"%@clientreferral/code?clientid=%@",delegate1.baseUrl,delegate1.userID];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if(data!=nil)
                                                    {
                                                    
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==403)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                            
                                                        }
                                                        else if([httpResponse statusCode]==401)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                           NSMutableDictionary *referralDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        referralCode=[NSString stringWithFormat:@"%@",[referralDict objectForKey:@"referralcode"]];
                                                               
                                                        }
                                                        
                                                    }
                                                    
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        self.referralCodeLbl.text=referralCode;
                                                        
                                                        
                                                    });
                                                    }
                                                    else
                                                    {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            
                                                            UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                [self getRefeeralCode];
                                                            }];
                                                            
                                                            UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                
                                                            }];
                                                            
                                                            
                                                            [alert addAction:retryAction];
                                                            [alert addAction:cancel];
                                                            
                                                        });
                                                        
                                                    }
                                                }];
    [dataTask resume];
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)shareBtnAction:(id)sender {
    @try
    {
    
    if(referralCode.length!=0)
    {
    
    NSString *textToShare =[NSString stringWithFormat:@"I would recommend you to download ZAMBALA STOCKS.Use my referral code %@ and get zambala credits",referralCode];
        
        NSURL *myWebsite = [NSURL URLWithString:@"https://itunes.apple.com/us/app/zambala-stocks-by-zenwise/id1281356740?ls=1&mt=8"];
   // NSURL *myWebsite = [NSURL URLWithString:[NSString stringWithFormat:@"https://i06-6.tlnk.io/serve?action=click&publisher_id=358381&site_id=137664&my_publisher=%@&my_keyword=%@",delegate1.brokerNameStr,referralCode]];
        UIImage * image=[UIImage imageNamed:@"success"];
    NSArray *objectsToShare = @[textToShare,myWebsite,image];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
        
    }
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

}

- (IBAction)backAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
