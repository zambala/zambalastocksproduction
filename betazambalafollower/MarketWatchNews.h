//
//  MarketWatchNews.h
//  betazambalafollower
//
//  Created by zenwise technologies on 16/03/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarketWatchNews : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *newsHeading;
@property (strong, nonatomic) IBOutlet UILabel *newsTime;

@end
