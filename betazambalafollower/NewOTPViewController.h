//
//  NewOTPViewController.h
//  OTP
//
//  Created by Zenwise Technologies on 23/07/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewOTPViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *tfOne;

@property (weak, nonatomic) IBOutlet UITextField *tfTwo;
@property (weak, nonatomic) IBOutlet UITextField *tfThree;
@property (weak, nonatomic) IBOutlet UITextField *tfFour;
@property (weak, nonatomic) IBOutlet UITextField *tfFive;
@property (weak, nonatomic) IBOutlet UITextField *tfSix;
@property (weak, nonatomic) IBOutlet UIButton *resendButton;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;

@property NSString * countryCode;
@property NSString * phoneNumber;
@property NSString * referralCode;
@property NSString * email;
@property NSString * fullName;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *updateButton;
@property NSString * checkString;

@end
