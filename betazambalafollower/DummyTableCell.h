//
//  DummyTableCell.h
//  testing
//
//  Created by zenwise technologies on 23/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DummyTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic) IBOutlet UILabel *LTPLabel;
@property (strong, nonatomic) IBOutlet UILabel *profitAndLossLabel;
@property (strong, nonatomic) IBOutlet UILabel *profitAndLossLabel1;
@property (strong, nonatomic) IBOutlet UILabel *boughtLabel;
@property (strong, nonatomic) IBOutlet UILabel *NSECashLabel;
@property (strong, nonatomic) IBOutlet UILabel *adviceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *premiumImageView;
@property (weak, nonatomic) IBOutlet UILabel *premiumLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLbl;

@end
