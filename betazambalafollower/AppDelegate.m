 //
//  AppDelegate.m
//  testing
//
//  Created by zenwise mac 2 on 11/14/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "AppDelegate.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
//@import Tune;
#import <HockeySDK/HockeySDK.h>
#import <Pushwoosh/PushNotificationManager.h>
#import <UserNotifications/UserNotifications.h>
//#import <TwitterKit/TwitterKit.h>
@import TwitterKit;
#import "TagEncode.h"
#import "TabBar.h"
#import "PortfolioView.h"
#import "PageView.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
@import Firebase;
@import Mixpanel;


#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)





@interface AppDelegate ()<CLLocationManagerDelegate,PushNotificationDelegate,UNUserNotificationCenterDelegate>
{
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLGeocoder * geocoder;
    UINavigationController * navigation;
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    _onboardcheck=true;
   //  self.baseUrl=@"https://stockserver.zenwise.net/api/";
//    self.baseUrl=@"https://stagemainstock.zenwise.net/api/";
   // self.baseUrl=@"http://192.168.15.58:8013/api/";
    
//    self.ltpServerURL = @"https://stageltpserver.zenwise.net";
//     self.ltpServerURL = @"http://192.168.15.223:8012";
    
//  self.mtBaseUrl=@"https://stageotserver.zenwise.net/api/";
//  self.mtBaseUrl=@"http://192.168.15.180:8014/api/";
    
    //firebase//
   
    [FIRApp configure];
    
    //FACEBOOK
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    //US Open Account
    self.addressDict = [[NSMutableDictionary alloc]init];
    self.finalChoiceTradeDict =[[NSMutableDictionary alloc]init];
    self.apiKey=@"A3F671B418E9F573E04F";
//    self.apiKey=@"PvyKTESA2t-NzGR1IjF74";
    self.producationURL=@"https://newacctapi.choicetrade.com/";
//    self.producationURL=@"http://newacctapi-dev.letsgotrade.com/";
    self.usLTPURL = @"https://stockserver.zenwise.net/api/usstockquotes/";
    
    [self CurrentLocationIdentifier];
    
    self.currentDeviceId1 =  [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
//    if (getenv("NSZombieEnabled"))
//        //NSLog(@"NSZombieEnabled!");
    
    NSDictionary* userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (userInfo != nil)
    {
       self.notificationInfo=userInfo;
    }
//    if( SYSTEM_VERSION_LESS_THAN( @"10.0" ) )
//    {
    
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound |    UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
//    }
//    else
//    {
//        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
//        [[UNUserNotificationCenter currentNotificationCenter] setDelegate:self];
//        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error)
//         {
//             if( !error )
//             {
//                 dispatch_async(dispatch_get_main_queue(), ^{
//
//                     [[UIApplication sharedApplication] registerForRemoteNotifications];  // required to get the app to do anything at all about push notifications
//                     //NSLog( @"Push registration success." );
//
//                         });
//
//             }
//             else
//             {
//                 //NSLog( @"Push registration FAILED" );
//                 //NSLog( @"ERROR: %@ - %@", error.localizedFailureReason, error.localizedDescription );
//                 //NSLog( @"SUGGESTIONS: %@ - %@", error.localizedRecoveryOptions, error.localizedRecoverySuggestion );
//             }
//         }];
//    }
//
    
    self.upStockCheck=true;

    self.searchToWatch=true;

    self.marketWatchBool=true;
     self.customWatchBool=true;
 



    
    //hockey//--
    
    [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:@"e558ecbde3ae4dbd8ac29377a01e7646"];
    [[BITHockeyManager sharedHockeyManager] startManager];
    [[BITHockeyManager sharedHockeyManager].authenticator authenticateInstallation];

    _profileImg=@"YES";

    _firstTimeCheck=YES;
//
//

//
//
//
    self.kiteVersion=@"3";
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];

    if(prefs !=nil)
    {



        self.homeInstrumentToken=[prefs objectForKey:@"instrumentToken"];

    }

    UIDevice *device = [UIDevice currentDevice];

    NSString  *currentDeviceId = [[device identifierForVendor]UUIDString];

    //NSLog(@"%@",currentDeviceId);



    _cartDict=[[NSMutableDictionary alloc]init];
    _cartArray=[[NSMutableArray alloc]init];
//

    [Mixpanel sharedInstanceWithToken:@"462706eaabc7c4931df9cc9ed4389e25"];
   // [Mixpanel sharedInstanceWithToken:@"a6fe8d6944ba1d830fcd42038c532b87"];
   

//
    self.mtNewUserBool = false;
    _launchCheck=true;
    _firstTime=true;
    self.test=true;
    self.test1=true;
    self.historyArray = [[NSMutableArray alloc]init];
    self.topPerfomersDetailArray = [[NSMutableArray alloc]init];
    self.knowledgeDetailsDict=[[NSDictionary alloc]init];
    self.urlArray=[[NSMutableArray alloc]init];
    self.optionLimitPriceArray=[[NSMutableArray alloc]init];
    self.leaderAdviceDetails=[[NSMutableArray alloc]init];
    self.sampleDepthLot=[[NSMutableArray alloc]init];
    self.depthLotSize=[[NSMutableArray alloc]init];
    self.imageArray=[[NSMutableArray alloc]init];
     self.optionOrderArray=[[NSMutableArray alloc]init];
    self.optionSymbolArray=[[NSMutableArray alloc]init];
     self.homeFilterCompanyName=[[NSMutableArray alloc]init];
    self.btnCountArray=[[NSMutableArray alloc]init];
    self.selectLeadersID = [[NSMutableArray alloc]initWithCapacity:3];
    self.detailNewsDict=[[NSMutableDictionary alloc]init];
     self.homeinstrumentName=[[NSMutableArray alloc]init];
     self.homeInstrumentToken=[[NSMutableArray alloc]init];
     self.homeLotDepth=[[NSMutableArray alloc]init];

    self.optionOrderTransactionArray=[[NSMutableArray alloc]init];
    self.leaderDetailArray=[[NSMutableArray alloc]init];
    
    self.lastPriceArray=[[NSMutableArray alloc]init];
    self.filteredCompanyName=[[NSMutableArray alloc]init];
   self.percentageChangeArray=[[NSMutableArray alloc]init];
    self.timeArray=[[NSMutableArray alloc]init];

    self.dict=[[NSMutableDictionary alloc]init];
    self.dict123=[[NSMutableDictionary alloc]init];
    self.cashDic=[[NSMutableDictionary alloc]init];
    self.filterResponseDictionary=[[NSMutableDictionary alloc]init];
    self.scripsMt=[[NSArray alloc]init];
    self.tmpValuesDict=[[NSMutableDictionary alloc]init];
     self.searchSymbolDict=[[NSMutableDictionary alloc]init];
    self.mixpanelUserRegistration=[[NSDictionary alloc]init];
    self.dict1=[[NSDictionary alloc]init];
    self.requestToken=[[NSString alloc]init];
    self.APIKey=[[NSString alloc]init];
    self.secret=[[NSString alloc]init];
    self.APIKey=@"xt2q1dpbwzu69n97";
    self.secret=@"3nsu0qnobudx44ztdlwkckp7v6l30vsa";
    self.NIFTYdict=[[NSMutableDictionary alloc]init];
    self.SENSEXdict=[[NSMutableDictionary alloc]init];
    self.instrumentNameArr=[[NSMutableArray alloc]init];
    self.instrumentToken=[[NSMutableArray alloc]init];
    self.filterMonksArray=[[NSMutableArray alloc]init];
    self.selectLeadersName = [[NSMutableArray alloc]init];
    self.allTradeOrderArray = [[NSMutableArray alloc]init];
    self.holdingRequestCheck=true;
    self.depthMt=false;
    self.homeMt=false;
    self.orderMt=false;
    self.holdingsMt=false;


   
    
    
    

//    multitrade//--
    
    self.btOutBuffer=[[NSMutableArray alloc]init];
    self.allOrderHistory=[[NSMutableArray alloc]init];
    self.allTradeArray=[[NSMutableArray alloc]init];
    self.mtCancelArray=[[NSMutableArray alloc]init];
    self.mtPendingArray=[[NSMutableArray alloc]init];
    self.mtCompletedArray=[[NSMutableArray alloc]init];
    self.exchangeToken=[[NSMutableArray alloc]init];
    self.homeExchangeToken=[[NSMutableArray alloc]init];
    self.marketWatch1=[[NSMutableArray alloc]init];
    self.mtpositionsArray=[[NSMutableArray alloc]init];
    self.homeNewCompanyArray=[[NSMutableArray alloc]init];
    self.expiryDateArray=[[NSMutableArray alloc]init];
    self.strikeArray=[[NSMutableArray alloc]init];
    self.homeStrkeArray=[[NSMutableArray alloc]init];
    self.optionArray=[[NSMutableArray alloc]init];
    self.homeOptionArray=[[NSMutableArray alloc]init];
    self.homeExpiryDate=[[NSMutableArray alloc]init];
    self.lotsizeArray=[[NSMutableArray alloc]init];
    self.homeLotSizeArray=[[NSMutableArray alloc]init];
    self.userMarketWatch=[[NSMutableArray alloc]init];
    self.localExchageToken=[[NSMutableArray alloc]init];
    self.tradeCompletedArray=[[NSMutableArray alloc]init];
    self.stdealerId=[[NSMutableArray alloc]init];
    self.mtHoldingArray=[[NSMutableArray alloc]init];
    self.mtIndexWatchArray = [[NSMutableArray alloc]init];
    self.mtFundsArray = [[NSMutableArray alloc]init];


        //crashlytics//---
    
    [Fabric with:@[[Crashlytics class]]];


    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:(6/255.0) green:(31/255.0) blue:(43/255.0) alpha:1]];

    [[UINavigationBar appearance] setTranslucent:NO];
    //tab bar//

    [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed:(6/255.0) green:(31/255.0) blue:(43/255.0) alpha:1]];
     
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];

    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
     [UIFont fontWithName:@"Helvetica" size:15.0], NSFontAttributeName,nil]];

    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:1]}];

   [navigation.navigationItem.leftBarButtonItem setTitle:@" "];


    [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:1]];


   // [self CurrentLocationIdentifier];


        
    UIDevice *device1 = [UIDevice currentDevice];
    self.currentDeviceId = [[device1 identifierForVendor]UUIDString];
        
        
    

  
    //twitter//
    
    NSMutableDictionary *twitterKeys;
    
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"Twitter" ofType:@"plist"];
    
    twitterKeys = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
    
    [[Twitter sharedInstance] startWithConsumerKey:[twitterKeys objectForKey:@"consumer_key"] consumerSecret:[twitterKeys objectForKey:@"consumer_secret"]];
    
    
    
    [[Twitter sharedInstance] startWithConsumerKey:@"E5b4foHqkKv9AT8I4eDTKCqt2" consumerSecret:@"205ANedDQktKbaNUGFK5Kj2q8TB1ow0PJZeS7PTa8nojrAOJPG"];
    
    
    
    self.equityTickerNameArray = [[NSMutableArray alloc]init];
    self.equityCompanyNameArray = [[NSMutableArray alloc]init];
    self.marketWatchLocalStoreDict= [[NSMutableDictionary alloc]init];
    self.expertIDArray = [[NSMutableArray alloc]init];
    self.mainOrdersArray = [[NSMutableArray alloc]init];
    self.dummyAccountURL = @"http://api-test.choicetrade.com/";
    self.realAccountURL=@"http://api-trade.choicetrade.com/";
//    self.realAccountURL=@"http://api-test.choicetrade.com/";
    
  
 
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
     self.searchToWatch=true;
   
   
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    if(self.notificationInfo)
    {
//         [[NSNotificationCenter defaultCenter] postNotificationName:@"upnifty" object:nil];
//       
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    TabBar *tab = [storyboard instantiateViewControllerWithIdentifier:@"tab"];
//    UIViewController *top = [UIApplication sharedApplication].keyWindow.rootViewController;
//    [top presentViewController:tab animated:YES completion: nil];
   
//    [self.window makeKeyAndVisible];
//        [self.window.rootViewController presentViewController:tab animated:YES completion:^{
//
//        }];
    }
//    [self presentViewController:view animated:YES completion:nil];
    
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    //facebook
    [FBSDKAppEvents activateApp];
    
    self.searchToWatch=true;
     [[NSNotificationCenter defaultCenter] postNotificationName:@"back" object:nil];
//    if(self.notificationInfo)
//            {
//              [self.window.rootViewController.tabBarController setSelectedIndex:3];
//  
//}
//    if(self.notificationInfo)
//    {
//        
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        TabBar *loginViewController = [storyboard instantiateViewControllerWithIdentifier:@"tab"];
//        [self.window makeKeyAndVisible];
//        [self.window.rootViewController presentViewController:loginViewController animated:YES completion:NULL];
//    }
    
   
//    if([self.brokerNameStr isEqualToString:@"Zerodha"]||[self.brokerNameStr isEqualToString:@"Upstox"])
//    {
//        
//    }
//    else
//    {
//         [[NSNotificationCenter defaultCenter] postNotificationName:@"login" object:nil];
//    }
    
    if(self.notificationInfo)
   {
//    self.tabBarController.selectedViewController=[self.tabBarController.viewControllers objectAtIndex:0];
//    [self.navigationController popToRootViewControllerAnimated:YES];
//    [navigationController2 popToRootViewControllerAnimated:YES];
//    [navigationController3 popToRootViewControllerAnimated:YES];
//    [navigationController4 popToRootViewControllerAnimated:YES];
//    [navigationController5 popToRootViewControllerAnimated:YES];
    
//    TabBar * tabView = [[TabBar alloc]initWithNibName:@"tab" bundle:nil];
//    [tabView viewDidAppear:YES];
       
        [[NSNotificationCenter defaultCenter] postNotificationName:@"pushNotification" object:nil userInfo:nil];
   }
    
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"upnifty" object:nil];
    

}



-(BOOL)resignFirstResponder

{
    
    [self resignFirstResponder];
    
    return YES;
    
    
}




- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    
    
    
    
    // after we have current coordinates, we use this method to fetch the information data of fetched coordinate
    [geocoder reverseGeocodeLocation:[locations lastObject] completionHandler:^(NSArray *placemarks, NSError *error) {
        CLPlacemark *placemark = [placemarks lastObject];
        
        NSString *street = placemark.thoroughfare;
        NSString *city = placemark.locality;
        NSString *posCode = placemark.postalCode;
        NSString *country = placemark.country;
        
        //NSLog(@"we live in %@", country);
        //NSLog(@"we live in %@", street);
        //NSLog(@"we live in %@", city);
        //NSLog(@"we live in %@", posCode);
        
        // stopping locationManager from fetching again
        [locationManager stopUpdatingLocation];
    }];
}


-(void)CurrentLocationIdentifier
    {
        //---- For getting current gps location
        CLGeocoder *ceo;
        
        locationManager = [[CLLocationManager alloc]init];
        locationManager.delegate = self;
        [locationManager startUpdatingLocation];
        
        //------
        
        ceo= [[CLGeocoder alloc]init];
        [locationManager requestWhenInUseAuthorization];
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [locationManager requestWhenInUseAuthorization];
        }
        CLLocationCoordinate2D coordinate;
        
        coordinate.latitude=locationManager.location.coordinate.latitude;
        coordinate.longitude=locationManager.location.coordinate.longitude;
      
        MKPointAnnotation *marker = [MKPointAnnotation new];
        marker.coordinate = coordinate;
        //NSLog(@"%f",coordinate.latitude);
       
        
        CLLocation *loc = [[CLLocation alloc]initWithLatitude:coordinate.latitude longitude:coordinate.longitude
                           ];
        [ceo reverseGeocodeLocation:loc
                  completionHandler:^(NSArray *placemarks, NSError *error) {
                      CLPlacemark *placemark = [placemarks objectAtIndex:0];
                      //NSLog(@"placemark %@",placemark);
                      //String to hold address
                      NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                      //NSLog(@"addressDictionary %@", placemark.addressDictionary);
                      
                      //NSLog(@"placemark %@",placemark.region);
                      //NSLog(@"placemark %@",placemark.country);  // Give Country Name
                      //NSLog(@"placemark %@",placemark.locality); // Extract the city name
                      //NSLog(@"location %@",placemark.name);
                      //NSLog(@"location %@",placemark.ocean);
                      //NSLog(@"location %@",placemark.postalCode);
                      //NSLog(@"location %@",placemark.subLocality);
                      
                      //NSLog(@"location %@",placemark.location);
                      //Print the location to console
                      //NSLog(@"I am currently at %@",locatedAt);
                      
                      self.region=[NSString stringWithFormat:@"%@",placemark.region];
                      
                      self.country=[NSString stringWithFormat:@"%@",placemark.country];

                      
                      self.locality=[NSString stringWithFormat:@"%@",placemark.locality];

                      
                      self.name=[NSString stringWithFormat:@"%@",placemark.name];

                      
                      self.ocean=[NSString stringWithFormat:@"%@",placemark.ocean];

                      
                      self.postalCode=[NSString stringWithFormat:@"%@",placemark.postalCode];

                      
                      self.subLocality=[NSString stringWithFormat:@"%@",placemark.subLocality];

                      
                      self.location=[NSString stringWithFormat:@"%@",placemark.location];

                      
                      self.locatedAt=[NSString stringWithFormat:@"%@",locatedAt];

                      [locationManager stopUpdatingLocation];
                  }
         
         ];
}

//tune//

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary *)options {
    // Pass the deep link URL to Tune
    // Take care of the redirect yourself here
   
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    // Pass the deep link URL to Tune
    // Take care of the redirect yourself here
        return YES;
}
- (BOOL)application:(UIApplication *)application continueUserActivity:(nonnull NSUserActivity *)userActivity restorationHandler:(nonnull void (^)(NSArray * _Nullable))restorationHandler {
   
    return YES;
}

- (void)tuneDidReceiveDeeplink:(NSString *)deeplink {
    //NSLog(@"TUNE.deeplink: %@", deeplink);
    // Handle deep link redirect here...
}

- (void)tuneDidFailDeeplinkWithError:(NSError *)error {
    //NSLog(@"TUNE.deeplink failed: %@", [error localizedDescription]);
}

//location//

///+ (void)setShouldAutoCollectDeviceLocation:(BOOL)autoCollect
//{
//    autoCollect=YES;
//}



// system push notifications callback, delegate to pushManager






- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.mixpanelDeviceToken=deviceToken;
        //NSLog(@"deviceToken: %@", deviceToken);
        self.token = [NSString stringWithFormat:@"%@", deviceToken];
        //Format token as you need:
        self.token = [self.token stringByReplacingOccurrencesOfString:@" " withString:@""];
        self.token = [self.token stringByReplacingOccurrencesOfString:@">" withString:@""];
        self.token = [self.token stringByReplacingOccurrencesOfString:@"<" withString:@""];
        
        //NSLog(@"Main device token:%@",self.token);
        
        
        self.helpShiftData = [self.token dataUsingEncoding:NSUTF8StringEncoding];
        
       
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        if(prefs !=nil)
        {
            
            // getting an NSString
            
            [prefs setObject:self.token forKey:@"devicetoken"];
            [prefs setObject:deviceToken forKey:@"devicetokenData"];
            [prefs synchronize];
            
        }
        
        
        
    });
    
   
    [[PushNotificationManager pushManager] handlePushRegistration:deviceToken];
    
    
    
    // This sends the deviceToken to Mixpanel
    
    
    
}




- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    //NSLog( @"Handle push from foreground" );
    // custom code to handle push while app is in the foreground
    //NSLog(@"%@", notification.request.content.userInfo);
}


- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler {
    //NSLog( @"Handle push from background or closed" );
    // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
    //NSLog(@"%@", response.notification.request.content.userInfo);

    return;
 
}
-(void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void
                                                                                                                               (^)(UIBackgroundFetchResult))completionHandler
{
    // iOS 10 will handle notifications through other methods
     self.notificationInfo=[[NSDictionary alloc]init];
//    if( SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO( @"10.0" ) )
//    {
//        //NSLog( @"iOS version >= 10. Let NotificationCenter handle this one." );
//        // set a member variable to tell the new delegate that this is background
//        self.notificationInfo=userInfo;
//        return;
//    }
    //NSLog( @"HANDLE PUSH, didReceiveRemoteNotification: %@", userInfo );
    
    // custom code to handle notification content
    
    if( [UIApplication sharedApplication].applicationState == UIApplicationStateInactive )
    {
        //NSLog( @"INACTIVE" );
         self.notificationInfo=userInfo;
        completionHandler( UIBackgroundFetchResultNewData );
    }
    else if( [UIApplication sharedApplication].applicationState == UIApplicationStateBackground )
    {
        //NSLog( @"BACKGROUND" );
         self.notificationInfo=userInfo;
        completionHandler( UIBackgroundFetchResultNewData );
    }
    else
    {
        //NSLog( @"FOREGROUND" );
         self.notificationInfo=userInfo;
         [[NSNotificationCenter defaultCenter] postNotificationName:@"pushNotification" object:nil userInfo:nil];
        completionHandler( UIBackgroundFetchResultNewData );
    }
}





@end
