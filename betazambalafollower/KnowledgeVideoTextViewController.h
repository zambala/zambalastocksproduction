//
//  KnowledgeVideoTextViewController.h
//  KnowledgeSection
//
//  Created by Zenwise Technologies on 16/06/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KnowledgeVideoTextViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *backButton;
- (IBAction)onPlayButtonTap:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *shortDescriptionLbl;
@property (weak, nonatomic) IBOutlet UIImageView *thumbNailImgView;

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@end
