//
//  FeedListView.h
//  testing
//
//  Created by zenwise mac 2 on 12/19/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedListView : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *feedListTbl;


@property (retain, nonatomic) NSMutableArray *descriptionArr;
@property (retain, nonatomic) NSMutableArray *detailArray;

@property(nonatomic,retain)NSMutableArray *imageArray;
@property (retain, nonatomic) NSMutableArray *timeArray;



@end
