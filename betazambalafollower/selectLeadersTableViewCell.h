//
//  selectLeadersTableViewCell.h
//  betazambalafollower
//
//  Created by zenwise technologies on 10/07/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface selectLeadersTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *leaderSelectButton;
- (IBAction)onLeaderSelectButtonTap:(id)sender;

@end
