//
//  HomePage.h
//  testing
//
//  Created by zenwise mac 2 on 11/15/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CommonCrypto/CommonHMAC.h>
#import "PSWebSocket.h"
#import "Reachability.h"
@import SocketIO;


/*!
 @author Sanka Revanth
 @class HomePage
 @discussion This class reffers to market watch screen.Here we can see live stream of market prices of symbols.
 @superclass SuperClass: UIViewController\n
 @classdesign    Designed using UIView,Button,BarButton,Label,UITableview.
 @coclass    AppDelegate
 
 */


@interface HomePage : UIViewController<UITableViewDelegate, UITableViewDataSource>
{

}
@property (weak, nonatomic) IBOutlet UIView *modifySubView;
@property (weak, nonatomic) IBOutlet UILabel *noWatchLbl;
@property (weak, nonatomic) IBOutlet UILabel *noWatchDesLbl;

- (IBAction)addBtn:(id)sender;/*!<add symbol button*/

@property (weak, nonatomic) IBOutlet UIView *modifyBgView;
- (IBAction)addSymbolBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *profitLossLbl;
@property (weak, nonatomic) IBOutlet UITextField *modifyAvgPriceLbl;
@property (weak, nonatomic) IBOutlet UIButton *modifySaveBtn;
- (IBAction)onModifySaveTap:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *modifyQtyLbl;
@property (weak, nonatomic) IBOutlet UILabel *modifyCompanyLbl;
@property (nonatomic, strong)  SocketManager* manager;
@property (nonatomic, strong)  SocketIOClient* socketio;
@property (nonatomic, strong)  NSMutableArray *allKeysList1;
@property (nonatomic, strong)  NSMutableArray *allKeysList1Custom;
@property (weak, nonatomic) IBOutlet UILabel *currentValueLbl;

@property (weak, nonatomic) IBOutlet UILabel *investmentValueLbl;

@property (nonatomic, strong) PSWebSocket *socket;/*!<kite websocket*/

@property (weak, nonatomic) IBOutlet UITableView *customPortfolioTblView;

@property (strong, nonatomic) IBOutlet UIView *ifEmptyView;/*!<empty view*/
@property (weak, nonatomic) IBOutlet UIView *customPortfolioView;


@property (weak, nonatomic) IBOutlet UITableView *marketTableView;/*!<tableview to show list of symbols*/

- (IBAction)editTableViewBtn:(id)sender;/*!<edit button*/

@property (weak, nonatomic) IBOutlet UIView *customPortfolioBottomView;


@property (strong, nonatomic) IBOutlet UIView *headerView;
@property Reachability * reach;

@property id message;
//@property SRWebSocket * webSocket;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property NSTimer * timer;

@end
