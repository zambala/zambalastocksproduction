//
//  KnowledgeHomeViewController.m
//  KnowledgeSection
//
//  Created by Zenwise Technologies on 13/06/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "KnowledgeHomeViewController.h"
#import "HMSegmentedControl.h"
#import "KnowlegdeHomeTableViewCell.h"
#import "KnowledgeHomeTextTableViewCell.h"
#import "KnowledgeResourcesTableViewCell.h"
#import "KnowledgeTextImageViewController.h"
#import "KnowledgeTextOnlyViewController.h"
#import "KnowledgeVideoTextViewController.h"
#import "TagEncode.h"
#import "wealthCreatorCell.h"
#import <Mixpanel/Mixpanel.h>
#import "YoutubePlayerview.h"
#import "AppDelegate.h"
#import "WealthCell.h"
#import "UIImageView+WebCache.h"
#import "YTPlayerView.h"


@interface KnowledgeHomeViewController ()<UIScrollViewDelegate,YTPlayerViewDelegate>
{
    HMSegmentedControl * segmentedControl;
    NSString * term;
    NSString * type;
    NSDictionary * wealthDict;
    NSDictionary * wealthCreatorDictionary;
    AppDelegate * delegate1;
    NSString * inputType;
    NSUserDefaults *loggedInUser;
    NSString * number;
      NSMutableDictionary * responseDict1;
    NSMutableArray *   scripDetailsArray;
    NSMutableDictionary *scripDetails;
    BOOL enableBiggestBool;
    YTPlayerView *playerView;
   
}

@end

@implementation KnowledgeHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
     enableBiggestBool=true;
   
    loggedInUser = [NSUserDefaults standardUserDefaults];
    playerView.delegate=self;
    number= [loggedInUser stringForKey:@"profilemobilenumber"];
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"learn_page"];
    [mixpanelMini track:@"zambala_guide_page"];
    NSMutableArray * localSegmentsArray = [[NSMutableArray alloc]init];
    [localSegmentsArray addObject:@"ZAMBALA GUIDE"];
    [localSegmentsArray addObject:@"RESOURCES"];
    [localSegmentsArray addObject:@"WEALTH CREATOR"];
    [self.backScrollView setScrollEnabled:true];
    [self.knowledgeHomeTableView setScrollEnabled:false];
     [self.wealthCretaorTblView setScrollEnabled:false];
    self.backScrollView.delegate=self;
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:localSegmentsArray];
    segmentedControl.frame = CGRectMake(0, 0, self.segmentedControlView.frame.size.width, 56);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(onSegmentTap) forControlEvents:UIControlEventValueChanged];
    [segmentedControl setBackgroundColor:[UIColor colorWithRed:(248/255.0) green:(248/255.0) blue:(248/255.0) alpha:1.0f]];
    segmentedControl.selectionIndicatorColor=[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f];
    segmentedControl.tintColor=[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f];
    NSDictionary *defaults = @{
                               NSFontAttributeName : [UIFont systemFontOfSize:14.6f],
                               NSForegroundColorAttributeName : [UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1],
                               };
    segmentedControl.titleTextAttributes=defaults;
    [segmentedControl setSelectedSegmentIndex:0];
    [self.segmentedControlView addSubview:segmentedControl];
    [self onSegmentTap];
    
   
    
 
    self.bgViewHeight.constant=self.view.frame.size.height+50;
    segmentedControl.enabled=false;
    [segmentedControl setSelectedSegmentIndex:0];
    [self onSegmentTap];
    // Do any additional setup after loading the view.
}

-(void)onSegmentTap
{
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    segmentedControl.enabled=false;
    if(segmentedControl.selectedSegmentIndex==0)
    {
        self.knowledgeHomeTableView.delegate=self;
        self.knowledgeHomeTableView.dataSource=self;
        inputType=@"guide";
        Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
        [mixpanelMini track:@"guide_page"];
        self.knowledgeHomeTableView.hidden=YES;
        self.wealthCretaorTblView.hidden=YES;
        [self getResourceList];
       
    }
    else if(segmentedControl.selectedSegmentIndex==1)
    {
        self.knowledgeHomeTableView.delegate=self;
        self.knowledgeHomeTableView.dataSource=self;
        
      
        inputType=@"resource";
        Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
        [mixpanelMini track:@"resources_page"];
        self.knowledgeHomeTableView.hidden=YES;
        self.wealthCretaorTblView.hidden=YES;
        [self getResourceList];
    }
    else if(segmentedControl.selectedSegmentIndex==2)
    {
       
        self.wealthCretaorTblView.delegate=self;
        self.wealthCretaorTblView.dataSource=self;
        if([delegate1.usCheck isEqualToString:@"USA"])
        {
        }else
        {
        [self getScripMethod];
        }
        Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
        [mixpanelMini track:@"wealth_creator_page"];
      
        self.knowledgeHomeTableView.hidden=YES;
        self.wealthCretaorTblView.hidden=NO;
         type=@"biggest";
          term=@"5";
//        [self onBiggestBtnTap:self.biggestBtn];
        [self getCompanyList];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    @try
    {
    if(tableView==self.wealthCretaorTblView)
    {
        if( [[[wealthCreatorDictionary objectForKey:@"data"] objectForKey:@"data"] count]>0)
        {
        return [[[wealthCreatorDictionary objectForKey:@"data"] objectForKey:@"data"] count] +1;
        }
    }
    else if(tableView==self.knowledgeHomeTableView)
    
    {
        if( [[[wealthDict objectForKey:@"data"] objectForKey:@"data"] count]>0)
        {
            return [[[wealthDict objectForKey:@"data"] objectForKey:@"data"] count];
        }
      
    }
    return 0;
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{@try
    {
    if(tableView==self.knowledgeHomeTableView)
    {
//    if(segmentedControl.selectedSegmentIndex==0)
//    {
//    if (indexPath.row==1)
//    {
//        KnowledgeHomeTextTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"KnowledgeHomeTextTableViewCell" forIndexPath:indexPath];
//        cell.backView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
//        cell.backView.layer.shadowOffset = CGSizeMake(0, 2.0f);
//        cell.backView.layer.shadowOpacity = 3.0f;
//        cell.backView.layer.shadowRadius = 3.0f;
//        cell.backView.layer.cornerRadius=10.0f;
//        cell.backView.layer.masksToBounds = NO;
//        return cell;
//    }
//    else
//    {
//        KnowlegdeHomeTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"KnowlegdeHomeTableViewCell" forIndexPath:indexPath];
//        if(indexPath.row==2)
//        {
//            cell.videoButton.hidden=YES;
//        }else
//        {
//            cell.videoButton.hidden=NO;
//        }
//        return cell;
//    }
//    }
    
        KnowledgeResourcesTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"KnowledgeResourcesTableViewCell" forIndexPath:indexPath];
        cell.backView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
        cell.backView.layer.shadowOffset = CGSizeMake(0, 2.0f);
        cell.backView.layer.shadowOpacity = 3.0f;
        cell.backView.layer.shadowRadius = 3.0f;
        cell.backView.layer.cornerRadius=10.0f;
        cell.backView.layer.masksToBounds = NO;
       
//        if(indexPath.row==0)
//        {
//            cell.playButton.hidden=NO;
//        }else
//        {
//            cell.playButton.hidden=YES;
//        }
          NSDictionary * localDict=[[NSDictionary alloc]initWithDictionary:[[[wealthDict objectForKey:@"data"]objectForKey:@"data"]objectAtIndex:indexPath.row]];
         cell.shortDescriptionLbl.text=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"heading"]];
        cell.thumbNailImgView.layer.cornerRadius=10.0f;
        cell.thumbNailImgView.clipsToBounds = YES;
        
//        UIBezierPath *maskPath1 = [UIBezierPath
//                                   bezierPathWithRoundedRect:cell.imageView.bounds
//                                   byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft)
//                                   cornerRadii:CGSizeMake(5, 5)
//                                   ];
//
//        CAShapeLayer *maskLayer1 = [CAShapeLayer layer];
//         
//        maskLayer1.frame = cell.thumbNailImgView.bounds;
//        maskLayer1.path = maskPath1.CGPath;
//        cell.thumbNailImgView.layer.mask = maskLayer1;
        
        int localint=[[localDict objectForKey:@"type"] intValue];
        if(localint==1)
        {
            cell.thumbNailImgView.image=[UIImage imageNamed:@"RESOURCES"];
            cell.playButton.hidden=YES;
        }
        else  if(localint==2)
        {
            NSString * img=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"url"]];
            if([img isEqualToString:@"<null>"]||[img isEqual:[NSNull null]])
            {
                 cell.thumbNailImgView.image=[UIImage imageNamed:@"RESOURCES"];
            }
            else
            {
                cell.thumbNailImgView.image=nil;
                
                NSURL *url;
                @try {
                    
                    url =  [NSURL URLWithString:img];
                } @catch (NSException *exception) {
                    
                    
                    
                } @finally {
                    
                }
                
                [cell.thumbNailImgView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"RESOURCES"]];
//                NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//                    if (data) {
//                        UIImage *image = [UIImage imageWithData:data];
//                        if (image) {
//                            dispatch_async(dispatch_get_main_queue(), ^{
//                                KnowledgeResourcesTableViewCell *cell = [self.knowledgeHomeTableView cellForRowAtIndexPath:indexPath];
//                                if (cell)
//                                    cell.thumbNailImgView.image = image;
//
//
//                            });
//                        }
//                    }
//                }];
//                [task resume];
            }
             cell.playButton.hidden=YES;
            
        }
        else  if(localint==3)
        {
            NSString * img=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"thumbnailurl"]];
            if([img isEqualToString:@"<null>"]||[img isEqual:[NSNull null]])
            {
                cell.thumbNailImgView.image=[UIImage imageNamed:@"RESOURCES"];
            }
            else
            {
                cell.thumbNailImgView.image=nil;
                
                NSURL *url;
                @try {
                    url =  [NSURL URLWithString:img];
                } @catch (NSException *exception) {
                    
                    
                    
                } @finally {
                    
                }
                
                 [cell.thumbNailImgView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"RESOURCES"]];
//                NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//                    if (data) {
//                        UIImage *image = [UIImage imageWithData:data];
//                        if (image) {
//                            dispatch_async(dispatch_get_main_queue(), ^{
//
//                                KnowledgeResourcesTableViewCell *cell = [self.knowledgeHomeTableView cellForRowAtIndexPath:indexPath];
//                                if (cell)
//                                    cell.thumbNailImgView.image = image;
//
//                            });
//                        }
//                    }
//                }];
//                [task resume];
            }
            cell.playButton.hidden=YES;
            
        }
        else  if(localint==7)
        {
            NSString * img=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"thumbnailurl"]];
            if([img isEqualToString:@"<null>"]||[img isEqual:[NSNull null]])
            {
                cell.thumbNailImgView.image=[UIImage imageNamed:@"RESOURCES"];
            }
            else
            {
                cell.thumbNailImgView.image=nil;
                
                NSURL *url;
                @try {
                    url =  [NSURL URLWithString:img];
                } @catch (NSException *exception) {
                    
                    
                    
                } @finally {
                    
                }
                
                 [cell.thumbNailImgView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"RESOURCES"]];
//                NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//                    if (data) {
//                        UIImage *image = [UIImage imageWithData:data];
//                        if (image) {
//                            dispatch_async(dispatch_get_main_queue(), ^{
//
//                                KnowledgeResourcesTableViewCell *cell = [self.knowledgeHomeTableView cellForRowAtIndexPath:indexPath];
//                                if (cell)
//                                    cell.thumbNailImgView.image = image;
//
//                            });
//                        }
//                    }
//                }];
//                [task resume];
            }
             cell.playButton.hidden=NO;
        }
        //140
        return cell;
    
        
    }
    else if(tableView==self.wealthCretaorTblView)
    {
        
            if(indexPath.row==0)
            {
                WealthCell * cell = [tableView dequeueReusableCellWithIdentifier:@"WealthCell" forIndexPath:indexPath];
                if([delegate1.usCheck isEqualToString:@"USA"])
                {
                    cell.biggestBtn.hidden=YES;
                     cell.fastestBtn.hidden=YES;
                     cell.consistentBtn.hidden=YES;
                     cell.fiveYearsBtn.hidden=YES;
                     cell.twentyFiveYearsBtn.hidden=YES;
                     cell.descriptionLbl.text=@"Note: Assumed initial investment amount: ₹10,000";
                }
                else
                {
                    cell.biggestBtn.hidden=NO;
                    cell.fastestBtn.hidden=NO;
                    cell.consistentBtn.hidden=NO;
                    cell.fiveYearsBtn.hidden=NO;
//                    cell.twentyFiveYearsBtn.hidden=YES;
                [cell.biggestBtn addTarget:self action:@selector(onBiggestBtnTap:) forControlEvents:UIControlEventTouchUpInside];
                [cell.fastestBtn addTarget:self action:@selector(onFatestBtnTap:) forControlEvents:UIControlEventTouchUpInside];
                [cell.consistentBtn addTarget:self action:@selector(onConsistentBtnTap:) forControlEvents:UIControlEventTouchUpInside];
                [cell.fiveYearsBtn addTarget:self action:@selector(onFiveYearsBtnTap:) forControlEvents:UIControlEventTouchUpInside];
                [cell.twentyFiveYearsBtn addTarget:self action:@selector(onTwentyFiveTap:) forControlEvents:UIControlEventTouchUpInside];
                if(enableBiggestBool==true)
                {
                    cell.fiveYearsBtn.imageView.image=[UIImage imageNamed:@"radioSelect"];
                    cell.twentyFiveYearsBtn.imageView.image=[UIImage imageNamed:@"radioUnselect"];
                    enableBiggestBool=false;
                    [self onBiggestBtnTap:cell.biggestBtn];
                }
                }
//
                return cell;
            
            }
            else
            {
            wealthCreatorCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
            if([delegate1.usCheck isEqualToString:@"USA"])
            {
                NSDictionary * localDict=[[[wealthCreatorDictionary objectForKey:@"data"]objectForKey:@"data"]objectAtIndex:indexPath.row-1];
                int add=[[localDict objectForKey:@"alreadyadded"]intValue];
                if(add==1)
                {
                    [cell.addBtn setImage:[UIImage imageNamed:@"addWealthTick"] forState:UIControlStateNormal];
                }else
                {
                    [cell.addBtn setImage:[UIImage imageNamed:@"addWealth"] forState:UIControlStateNormal];
                }
                cell.companyName.text=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"longname"]];
                cell.symbolLbl.text=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"symbol"]];
                cell.priceLbl.text=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"amountmade"]];
                cell.durationLbl.text=[NSString stringWithFormat:@"in %@ years",[localDict objectForKey:@"years"]];
                [cell.addBtn addTarget:self action:@selector(addBtnTap:) forControlEvents:UIControlEventTouchUpInside];
                
            }
            else
            {
                NSDictionary * localDict=[[[wealthCreatorDictionary objectForKey:@"data"]objectForKey:@"data"]objectAtIndex:indexPath.row-1];
                NSString * exchangeToken = [NSString stringWithFormat:@"%@",[[localDict objectForKey:@"data"]objectForKey:@"exchange_token"]];
                if([delegate1.localExchageToken containsObject:exchangeToken])
                {
                    [cell.addBtn setImage:[UIImage imageNamed:@"addWealthTick"] forState:UIControlStateNormal];
                }else
                {
                    [cell.addBtn setImage:[UIImage imageNamed:@"addWealth"] forState:UIControlStateNormal];
                }
                cell.companyName.text=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"companyname"]];
                cell.symbolLbl.text=[NSString stringWithFormat:@"%@",[[localDict objectForKey:@"data"]objectForKey:@"symbol"]];
                cell.priceLbl.text=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"amountmade"]];
                [cell.addBtn addTarget:self action:@selector(addBtnTap:) forControlEvents:UIControlEventTouchUpInside];
                
            }
          
            return cell;
            }
        
    }
  
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
 
}
-(void)addBtnTap:(UIButton*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.wealthCretaorTblView];
    NSIndexPath *indexPath = [self.wealthCretaorTblView indexPathForRowAtPoint:buttonPosition];
//     wealthCreatorCell * cell = [self.wealthCretaorTblView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    if([delegate1.usCheck isEqualToString:@"USA"])
    {
        NSString * symbol=[NSString stringWithFormat:@"%@",[[[[wealthCreatorDictionary objectForKey:@"data"]objectForKey:@"data"]objectAtIndex:indexPath.row-1] objectForKey:@"symbol"] ];
        [self sendToServer:symbol];
    }
    else
    {
    [delegate1.userMarketWatch addObject:[[[[wealthCreatorDictionary objectForKey:@"data"]objectForKey:@"data"]objectAtIndex:indexPath.row-1] objectForKey:@"data"] ];
//    [cell.addBtn setImage:[UIImage imageNamed:@"addWealthTick"] forState:UIControlStateNormal];
    [self instruments];
    }
}
-(void)sendToServer:(NSString *)symbol
{
    @try
    {
    NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
    NSString*number= [loggedInUser stringForKey:@"profilemobilenumber"];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"authtoken":delegate1.zenwiseToken,
                               @"deviceid":delegate1.currentDeviceId
                               };
    NSDictionary *parameters;
    if(delegate1.sessionID.length>0)
    {
        parameters =@{ @"clientid": delegate1.usaId,
                       @"action": @"add",
                       @"mobilenumber":number,
                       @"symbols": @[symbol] };
    }else
    {
        parameters =@{ @"clientid": delegate1.userID,
                       @"action": @"add",
                       @"mobilenumber":number,
                       @"symbols": @[symbol] };
    }
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSString * url = [NSString stringWithFormat:@"%@usstockinfo/marketwatch",delegate1.baseUrl];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@",httpResponse);
                                                        if([httpResponse statusCode]==403)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                            
                                                        }
                                                        else if([httpResponse statusCode]==401)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                            
                                                        }else
                                                        {
                                                           NSDictionary * addResponseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                if([[addResponseDict objectForKey:@"message"]isEqualToString:@"success"])
                                                                {
                                                                    delegate1.addCheck=@"added";
                                                                    [self getCompanyList];
                                                                    [self.wealthCretaorTblView reloadData];
                                                                }else
                                                                {
                                                                    //NSLog(@"Error while adding symbol");
                                                                }
                                                                
                                                            });
                                                            
                                                        }
                                                    }
                                                }];
    [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==self.knowledgeHomeTableView)
    {
        return 250;
    }
    else if(tableView==self.wealthCretaorTblView)
    {
        if(indexPath.row==0)
        {
            if([delegate1.usCheck isEqualToString:@"USA"])
            {
                return 120;
            }
            else
            {
            return 200;
            }
        }
        else
        {
        return 70;
        }
    }
    return 0;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
    if(tableView==self.knowledgeHomeTableView)
    {
    
       
//        if(indexPath.row==2)
//        { 
//            KnowledgeTextImageViewController * textImage = [self.storyboard instantiateViewControllerWithIdentifier:@"KnowledgeTextImageViewController"];
//            [self presentViewController:textImage animated:YES completion:nil];
//        }
//        if(indexPath.row==1)
//        {
//            KnowledgeTextOnlyViewController * text = [self.storyboard instantiateViewControllerWithIdentifier:@"KnowledgeTextOnlyViewController"];
//            [self presentViewController:text animated:YES completion:nil];
//        }
    if (segmentedControl.selectedSegmentIndex==1||segmentedControl.selectedSegmentIndex==0)
    {
//        if(indexPath.row==0)
//        {
//            KnowledgeVideoTextViewController * video = [self.storyboard instantiateViewControllerWithIdentifier:@"KnowledgeVideoTextViewController"];
//            [self presentViewController:video animated:YES completion:nil];
//        }else
//        {
//            KnowledgeTextImageViewController * textImage = [self.storyboard instantiateViewControllerWithIdentifier:@"KnowledgeTextImageViewController"];
//            [self presentViewController:textImage animated:YES completion:nil];
//        }
        
       
       
       
        delegate1.resourceDetails=[[NSDictionary alloc]init];
        delegate1.resourceDetails=[[[wealthDict objectForKey:@"data"]objectForKey:@"data"]objectAtIndex:indexPath.row];
        int localint=[[delegate1.resourceDetails objectForKey:@"type"] intValue];
        if(localint==1)
        {
            KnowledgeTextOnlyViewController * text = [self.storyboard instantiateViewControllerWithIdentifier:@"KnowledgeTextOnlyViewController"];
            [self.navigationController pushViewController:text animated:YES];
        }
        else if(localint==2)
        {
            KnowledgeTextImageViewController * textImage = [self.storyboard instantiateViewControllerWithIdentifier:@"KnowledgeTextImageViewController"];
            [self.navigationController pushViewController:textImage animated:YES];
        }
        else if(localint==7)
        {
            YoutubePlayerview * textImage = [self.storyboard instantiateViewControllerWithIdentifier:@"YoutubePlayerview"];
            [self.navigationController pushViewController:textImage animated:YES];
            delegate1.tutorialurl=[NSString stringWithFormat:@"%@",[delegate1.resourceDetails objectForKey:@"url"]];
//            playerView=[[YTPlayerView alloc]init];
//           playerView.frame = CGRectMake(20, 50, self.view.frame.size.width-50,200);
//            [self.backView addSubview:playerView];
//            if(delegate1.tutorialurl.length>0)
//            {
//                NSMutableDictionary *queryStringDictionary = [[NSMutableDictionary alloc] init];
//
//                NSArray *urlComponents1 = [delegate1.tutorialurl componentsSeparatedByString:@"?"];
//
//                NSString * paramStr=[NSString stringWithFormat:@"%@",[urlComponents1 objectAtIndex:1]];
//
//                NSArray *urlComponents = [paramStr componentsSeparatedByString:@"&"];
//
//                for (NSString *keyValuePair in urlComponents)
//                {
//                    NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
//                    NSString *key = [[pairComponents firstObject] stringByRemovingPercentEncoding];
//                    NSString *value = [[pairComponents lastObject] stringByRemovingPercentEncoding];
//
//                    [queryStringDictionary setObject:value forKey:key];
//                }
//                NSString * videoID= [queryStringDictionary objectForKey:@"v"];
//
//                [playerView loadWithVideoId:videoID];
//                [playerView playVideo];
//            }
        }
        else if(localint==3)
        {
            @try
            {
                NSString * url=[delegate1.resourceDetails objectForKey:@"url"];
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
                
            } @catch (NSException *exception) {
                //NSLog(@"Caught Exception");
            } @finally {
                //NSLog(@"Finally");
            }
        }
    }
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)onBiggestBtnTap:(id)sender {
     type=@"biggest";
    [sender setBackgroundColor:[UIColor colorWithRed:2/255 green:30/255 blue:41/255 alpha:1.0f]];
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.wealthCretaorTblView];
    NSIndexPath *indexPath = [self.wealthCretaorTblView indexPathForRowAtPoint:buttonPosition];
     WealthCell *cell = [self.wealthCretaorTblView cellForRowAtIndexPath:indexPath];
    [cell.fastestBtn setBackgroundColor:[UIColor whiteColor]];
    [cell.fastestBtn setTitleColor:[UIColor colorWithRed:2/255 green:30/255 blue:41/255 alpha:1.0f] forState:UIControlStateNormal];
    
    [cell.consistentBtn setBackgroundColor:[UIColor whiteColor]];
    [cell.consistentBtn setTitleColor:[UIColor colorWithRed:2/255 green:30/255 blue:41/255 alpha:1.0f] forState:UIControlStateNormal];
    cell.fiveYearsBtn.hidden=NO;
    cell.twentyFiveYearsBtn.hidden=NO;
    [cell.fiveYearsBtn setTitle:@" 5 Years" forState:UIControlStateNormal];
     [cell.twentyFiveYearsBtn setTitle:@" 25 Years" forState:UIControlStateNormal];
    [self onFiveYearsBtnTap:cell.fiveYearsBtn];
  
}
- (void)onFatestBtnTap:(id)sender {
    type=@"fastest";
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.wealthCretaorTblView];
    NSIndexPath *indexPath = [self.wealthCretaorTblView indexPathForRowAtPoint:buttonPosition];
    WealthCell *cell = [self.wealthCretaorTblView cellForRowAtIndexPath:indexPath];
    cell.fiveYearsBtn.hidden=NO;
    cell.twentyFiveYearsBtn.hidden=YES;
    [cell.fiveYearsBtn setTitle:@" 5 Years" forState:UIControlStateNormal];
   
    [sender setBackgroundColor:[UIColor colorWithRed:2/255 green:30/255 blue:41/255 alpha:1.0f]];
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [cell.biggestBtn setBackgroundColor:[UIColor whiteColor]];
    [cell.biggestBtn setTitleColor:[UIColor colorWithRed:2/255 green:30/255 blue:41/255 alpha:1.0f] forState:UIControlStateNormal];
    
    [cell.consistentBtn setBackgroundColor:[UIColor whiteColor]];
    [cell.consistentBtn setTitleColor:[UIColor colorWithRed:2/255 green:30/255 blue:41/255 alpha:1.0f] forState:UIControlStateNormal];
    [self onFiveYearsBtnTap:cell.fiveYearsBtn];
    
}
- (void)onConsistentBtnTap:(id)sender {
     type=@"consistent";
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.wealthCretaorTblView];
    NSIndexPath *indexPath = [self.wealthCretaorTblView indexPathForRowAtPoint:buttonPosition];
    WealthCell *cell = [self.wealthCretaorTblView cellForRowAtIndexPath:indexPath];
    cell.fiveYearsBtn.hidden=NO;
    cell.twentyFiveYearsBtn.hidden=YES;
 
    [cell.fiveYearsBtn setTitle:@" 10 Years" forState:UIControlStateNormal];
    [sender setBackgroundColor:[UIColor colorWithRed:2/255 green:30/255 blue:41/255 alpha:1.0f]];
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [cell.fastestBtn setBackgroundColor:[UIColor whiteColor]];
    [cell.fastestBtn setTitleColor:[UIColor colorWithRed:2/255 green:30/255 blue:41/255 alpha:1.0f] forState:UIControlStateNormal];
    
    [cell.biggestBtn setBackgroundColor:[UIColor whiteColor]];
    [cell.biggestBtn setTitleColor:[UIColor colorWithRed:2/255 green:30/255 blue:41/255 alpha:1.0f] forState:UIControlStateNormal];
    [self onFiveYearsBtnTap:cell.fiveYearsBtn];
  
}
- (void)onFiveYearsBtnTap:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.wealthCretaorTblView];
    NSIndexPath *indexPath = [self.wealthCretaorTblView indexPathForRowAtPoint:buttonPosition];
    WealthCell *cell = [self.wealthCretaorTblView cellForRowAtIndexPath:indexPath];
    if([type isEqualToString:@"consistent"])
    {
        term=@"10";
        cell.descriptionLbl.text=@"Note: The following Net Values are calculated assuming the default investment value is ₹10,000 invested 10 year(s) ago.";
    }
    else
    {
        term=@"5";
        if([type isEqualToString:@"fastest"])
        {
            cell.descriptionLbl.text=@"Note: The following Net Values are calculated assuming the default investment value is ₹10,000 invested 5 year(s) ago.";
        }else
        {
        cell.descriptionLbl.text=@"Note: The following Net Values are calculated assuming the default investment value is ₹10,000 invested 5 year(s) ago. You can change the duration below:";
        }
    }
    cell.fiveYearsBtn.imageView.image=[UIImage imageNamed:@"radioSelect"];
    cell.twentyFiveYearsBtn.imageView.image=[UIImage imageNamed:@"radioUnselect"];
//    [self.fiveYearsBtn setBackgroundImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateNormal];
//    [self.twentyFiveBtn setBackgroundImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
      [self getCompanyList];
}
- (void)onTwentyFiveTap:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.wealthCretaorTblView];
    NSIndexPath *indexPath = [self.wealthCretaorTblView indexPathForRowAtPoint:buttonPosition];
    WealthCell *cell = [self.wealthCretaorTblView cellForRowAtIndexPath:indexPath];
    term=@"25";
     cell.descriptionLbl.text=@"Note: The following Net Values are calculated assuming the default investment value is ₹10,000 invested 25 year(s) ago. You can change the duration below:";
//    [self.fiveYearsBtn setBackgroundImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
//    [self.twentyFiveBtn setBackgroundImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateNormal];
    cell.fiveYearsBtn.imageView.image=[UIImage imageNamed:@"radioUnselect"];
    cell.twentyFiveYearsBtn.imageView.image=[UIImage imageNamed:@"radioSelect"];
   
    [self getCompanyList];
}
-(void)getCompanyList
{
    @try
    {
    TagEncode * enocde=[[TagEncode alloc]init];
    
    
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
  
        if([delegate1.usCheck isEqualToString:@"USA"])
        {
            
             enocde.inputRequestString=@"usawealth";
            if(delegate1.sessionID.length>0)
            {
             [inputArray addObject:delegate1.usaId];
            }
            else
            {
                 [inputArray addObject:delegate1.userID];
            }
             [inputArray addObject:[NSString stringWithFormat:@"%@knowledgebase/uswealthcreator",delegate1.baseUrl]];
        }
        else
        {
             enocde.inputRequestString=@"contestquestions";
             [inputArray addObject:type];
             [inputArray addObject:term];
             [inputArray addObject:[NSString stringWithFormat:@"%@knowledgebase/wealthcreator",delegate1.baseUrl]];
        }
  
    
    [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        
        NSArray * keysArray=[dict allKeys];
      
       
        if(keysArray.count>0)
        {
            
              dispatch_async(dispatch_get_main_queue(), ^{
                  self.activityIndicator.hidden=YES;
                  [self.activityIndicator stopAnimating];
                  self->wealthCreatorDictionary=dict;
                  [self.wealthCretaorTblView reloadData];
                   segmentedControl.enabled=true;
              });
            // change to a readable time format and change to local time zone
        }
    }];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

}
-(void)getResourceList
{
    @try
    {
    TagEncode * enocde=[[TagEncode alloc]init];
    
   
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        if([inputType isEqualToString:@"guide"])
        {    enocde.inputRequestString=@"guide";
             [inputArray addObject:[NSString stringWithFormat:@"%@knowledgebase/wealthcreatorresources/?type=g",delegate1.baseUrl]];
        }
        else if([inputType isEqualToString:@"resource"])
        {
             enocde.inputRequestString=@"resourcelist";
            
            
           
            if([delegate1.usCheck isEqualToString:@"USA"])
            {
              [inputArray addObject:[NSString stringWithFormat:@"%@knowledgebase/wealthcreatorresources/?type=r&sectiontype=2",delegate1.baseUrl]];
            }
            else
            {
                [inputArray addObject:[NSString stringWithFormat:@"%@knowledgebase/wealthcreatorresources/?type=r&sectiontype=1",delegate1.baseUrl]];
            }
           
        }
   
    
    [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        NSArray * keysArray=[dict allKeys];
      
        if(keysArray.count>0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.activityIndicator.hidden=YES;
                [self.activityIndicator stopAnimating];
                self.knowledgeHomeTableView.hidden=NO;
                //self->wealthDict=dict;
                wealthDict = [[NSDictionary alloc]initWithDictionary:dict];
                [self.knowledgeHomeTableView reloadData];
                 segmentedControl.enabled=true;
                 self.knowledgeHomeTableView.hidden=NO;
                 self.wealthCreatorView.hidden=YES;
            });
            // change to a readable time format and change to local time zone
        }
    }];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

}
//-(void)getZambalaGuide
//{
//    @try
//    {
//        TagEncode * enocde=[[TagEncode alloc]init];
//
//        enocde.inputRequestString=@"getguide";
//        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
//        [inputArray addObject:[NSString stringWithFormat:@"%@knowledgebase/wealthcreatorresources/?type=r",delegate1.baseUrl]];
//
//        [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
//
//            NSArray * keysArray=[dict allKeys];
//            //NSLog(@"%@",dict);
//            if(keysArray.count>0)
//            {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    self.knowledgeHomeTableView.hidden=NO;
//                    self->wealthDict=dict;
//                    [self.knowledgeHomeTableView reloadData];
//                });
//                // change to a readable time format and change to local time zone
//            }
//        }];
//    }
//    @catch (NSException * e) {
//        //NSLog(@"Exception: %@", e);
//    }
//    @finally {
//        //NSLog(@"finally");
//    }
//
//}
-(void)viewDidAppear:(BOOL)animated
{
    if([delegate1.pushNotificationSection isEqualToString:@"USA_NOTIFICATIONS"]||[delegate1.pushNotificationSection isEqualToString:@"INDIA_NOTIFICATIONS"])
    {
        delegate1.pushNotificationSection=@"";
        [segmentedControl setSelectedSegmentIndex:1];
        [self onSegmentTap];
    }
   else if([delegate1.pushNotificationSection isEqualToString:@"INDIA_ZAMBALA_GUIDE"]||[delegate1.pushNotificationSection isEqualToString:@"USA_ZAMBALA_GUIDE"])
    {
        delegate1.pushNotificationSection=@"";
        [segmentedControl setSelectedSegmentIndex:0];
        [self onSegmentTap];
    }
   else if([delegate1.pushNotificationSection isEqualToString:@"US_WEALTH_CREATOR_NOTIFICATIONS"]||[delegate1.pushNotificationSection isEqualToString:@"INDIA_WEALTH_CREATOR_NOTIFICATIONS"])
   {
       delegate1.pushNotificationSection=@"";
       [segmentedControl setSelectedSegmentIndex:2];
       [self onSegmentTap];
   }
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
   
    if (self.backScrollView.contentOffset.y >= 148) {
        [self.backScrollView setContentOffset:CGPointMake(0,148) animated:NO];
//        if(segmentedControl.selectedSegmentIndex==0||segmentedControl.selectedSegmentIndex==1)
//        {
            [self.knowledgeHomeTableView setScrollEnabled:true];
//        }
//        else
//        {
            [self.wealthCretaorTblView setScrollEnabled:true];
//        }
    }
    else
    {
//        [self.backScrollView setContentOffset:CGPointMake(0,self.backView.frame.size.height) animated:NO];
//        if(segmentedControl.selectedSegmentIndex==0||segmentedControl.selectedSegmentIndex==1)
//        {
            [self.knowledgeHomeTableView setScrollEnabled:false];
//        }
//        else
//        {
            [self.wealthCretaorTblView setScrollEnabled:false];
//        }
    }
//    CGFloat y = self.backScrollView.contentOffset.y;
//    if(y>148)
//    {
//        [self.backScrollView setScrollEnabled:false];
//    }
    
//    CGRect headerFrame = self.headerView.frame;
//    headerFrame.origin.y = MAX(self.headerView.frame.origin.y, self.backScrollView.contentOffset.y);
//    _headerView.frame = headerFrame;
}

-(void)getScripMethod
{
    @try
    {
        
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"cache-control": @"no-cache",
                                   @"authtoken":delegate1.zenwiseToken,
                                   @"deviceid":delegate1.currentDeviceId
                                   };
        
        NSString * localUrl;
        
        
            localUrl=[NSString stringWithFormat:@"%@clienttrade/watch/%@?mobilenumber=%@",delegate1.baseUrl,delegate1.userID,number];
            
       
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:localUrl]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        
        
        
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(data!=nil)
                                                        {
                                                            if (error) {
                                                               
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                
                                                                
                                                                if([httpResponse statusCode]==403)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                    
                                                                }
                                                                else if([httpResponse statusCode]==401)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    
                                                                    
                                                                    responseDict1=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                    
                                                                
                                                                        
                                                                        delegate1.marketWatchBool=false;
                                                                    
                                                                        
                                                                        delegate1.localExchageToken=[[NSMutableArray alloc]init];
                                                                        delegate1.homeNewCompanyArray=[[NSMutableArray alloc]init];
                                                                        delegate1.userMarketWatch=[[NSMutableArray alloc]init];
                                                                        delegate1.homeInstrumentToken=[[NSMutableArray alloc]init];
                                                                        
                                                                        for(int i=0;i<[[responseDict1 objectForKey:@"data"] count];i++)
                                                                        {
                                                                            //                                                            [delegate2.localExchageToken addObject:[[[[responseDict1 objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"] objectForKey:@"exchange_token"]];
                                                                            //                                                            [delegate2.homeInstrumentToken addObject:[[[[responseDict1 objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"] objectForKey:@"instrument_token"]];
                                                                            //
                                                                            //                                                           [delegate2.homeNewCompanyArray addObject:[[[responseDict1 objectForKey:@"data"]objectAtIndex:i] objectForKey:@"name"]];
                                                                            //
                                                                            //                                                            [delegate2.userMarketWatch addObject:[[[responseDict1 objectForKey:@"data"]objectAtIndex:i] objectForKey:@"name"]];
                                                                            //
                                                                            
                                                                            [delegate1.localExchageToken addObject:[[[responseDict1 objectForKey:@"data"]objectAtIndex:i] objectForKey:@"exchange_token"]];
                                                                            [delegate1.homeInstrumentToken addObject:[[[responseDict1 objectForKey:@"data"]objectAtIndex:i] objectForKey:@"instrument_token"]];
                                                                            
                                                                            [delegate1.homeNewCompanyArray addObject:[[responseDict1 objectForKey:@"data"]objectAtIndex:i]];
                                                                            
                                                                            //                                                            if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
                                                                            //                                                            {
                                                                            //                                                                            delegate2.homeInstrumentToken=[[[delegate2.homeInstrumentToken reverseObjectEnumerator] allObjects] mutableCopy];
                                                                            //                                                            }
                                                                            
                                                                            [delegate1.userMarketWatch addObject:[[responseDict1 objectForKey:@"data"]objectAtIndex:i]];
                                                                        }
                                                                        
                                                                        Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
                                                                        
                                                                        [mixpanelMini identify:delegate1.userID];
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        NSMutableArray * scripArray=[[NSMutableArray alloc]init];
                                                                        
                                                                        for(int i=0;i<[[responseDict1 objectForKey:@"data"]count];i++)
                                                                        {
                                                                            NSString * string2=[[NSString alloc]init];
                                                                            string2=[NSString stringWithFormat:@"%@", [[[responseDict1 objectForKey:@"data"]objectAtIndex:i]objectForKey:@"symbol"]];
                                                                            [scripArray addObject:string2];
                                                                        }
                                                                        
                                                                        
                                                                        NSArray * scripArrayNew=[scripArray mutableCopy];
                                                                        
                                                                        [mixpanelMini.people set:@{@"watchlist":scripArrayNew}];
                                                                        
                                                                        
                                                                    }
                                                                    
                                                                    
                                                              
                                                                
                                                            }
                                                            
                                                            
                                                            
                                                            
                                                            delegate1.searchToWatch=false;
                                                            // [self tmpDictMethod];
                                                            
                                                            
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                [self.wealthCretaorTblView reloadData];
                                                              
                                                                
                                                            });
                                                        }
                                                        else
                                                        {
                                                            [self getScripMethod];
                                                        }
                                                    }];
        [dataTask resume];
        
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    
    
    
}
-(void)instruments
{
    @try {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        
        scripDetailsArray=[[NSMutableArray alloc]init];
        scripDetails=[[NSMutableDictionary alloc]init];
        //
        //
        
            if(delegate1.userMarketWatch.count>=0)
            {
                
                
                
                NSMutableArray * uniqueArray=[[NSMutableArray alloc]init];
                
                for(int i=0;i<delegate1.userMarketWatch.count;i++)
                {
                    
                    
                    
                    NSMutableDictionary* E1 = [delegate1.userMarketWatch objectAtIndex:i];
                    
                    NSString * string=[NSString stringWithFormat:@"%@",[E1 objectForKey:@"exchange_token"]];
                    BOOL hasDuplicate = [[uniqueArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"exchange_token == %@",string]] count] > 0;
                    
                    if (!hasDuplicate)
                    {
                        [uniqueArray addObject:E1];
                    
                        
                    }
                    
                    else
                        
                    {
                        
                        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Symbol already added in your list" preferredStyle:UIAlertControllerStyleAlert];
                        
                        [self presentViewController:alert animated:YES completion:^{
                            
                        }];
                        
                        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            
                        }];
                        
                        [alert addAction:okAction];
                    }
                    
                    
                }
                
                
                
                
                
                for(int i=0;i<uniqueArray.count;i++)
                {
                    
                    NSMutableDictionary * tmp_dict=[[NSMutableDictionary alloc]init];
                    //
                    tmp_dict=[uniqueArray objectAtIndex:i];
                    
                    
                    
                    
                    [scripDetailsArray insertObject:tmp_dict atIndex:i];
                    
                    //                NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:scripDetailsArray];
                    //                NSArray *arrayWithoutDuplicates = [scripDetailsArray array];
                    
                    //            }
                    
                    //             NSCharacterSet * set=[NSCharacterSet newlineCharacterSet];
                    //
                    //            NSString * string=@"";
                    //
                    //            NSString * localString=[NSString stringWithFormat:@"%@",scripDetailsArray];
                    //
                    //            NSString * localSring1=[localString stringByReplacingOccurrencesOfString:@"(" withString:@"["]
                    //            ;
                    //
                    //            NSString * localSring2=[localSring1 stringByReplacingOccurrencesOfString:@")" withString:@"]"];
                    //
                    //            NSString * localSring3 = [localSring2 stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                    //            NSString * localSring4 = [localSring3 stringByReplacingOccurrencesOfString:@" " withString:@""];
                    //
                    //
                    //
                    //            NSString * localSring5 = [[localSring4 componentsSeparatedByCharactersInSet:set]componentsJoinedByString:@""];
                    //
                    //
                    
                    
                    
                    
                    [scripDetails setObject:scripDetailsArray forKey:@"data"];
                    
                    
                    
                    
                    
                }
                
             
            }
            
       
    }
    
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    [self resultCheck];
    
    
    
    
    
}
-(void)resultCheck
{
    @try {
        
        
        
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"cache-control": @"no-cache",
                                   @"authtoken":delegate1.zenwiseToken,
                                   @"deviceid":delegate1.currentDeviceId
                                   };
        
        
        
        
        NSString * localUrl;
      
            localUrl=[NSString stringWithFormat:@"%@clienttrade/watch",delegate1.baseUrl];
       
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:localUrl]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        
        NSDictionary * parameters = @{
                                      @"clientid":delegate1.userID,
                                      @"watchinfo":scripDetails,
                                      @"mobilenumber":number
                                      
                                      };
        
        NSData * postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(data!=nil)
                                                        {
                                                            if (error) {
                                                                
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                
                                                                if([httpResponse statusCode]==403)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                    
                                                                }
                                                                else if([httpResponse statusCode]==401)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    
                                                                    
                                                                    
                                                                    NSMutableDictionary * responseDict=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                    
                                                                    
                                                                    
                                                                    
                                                                }
                                                                
                                                            }
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                [self getScripMethod];
                                                                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:@"Symbol added to you market watch" preferredStyle:UIAlertControllerStyleAlert];
                                                                
                                                                [self presentViewController:alert animated:YES completion:^{
                                                                    
                                                                  
                                                                }];
                                                                
                                                                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                    delegate1.marketWatchBool=true;
                                                                }];
                                                                
                                                                [alert addAction:okAction];
                                                            });
                                                        }
                                                        else
                                                        {
                                                            [self resultCheck];
                                                        }
                                                    }];
        [dataTask resume];
        
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    
    
}


@end
