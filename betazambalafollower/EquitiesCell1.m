//
//  EquitiesCell1.m
//  testing
//
//  Created by zenwise technologies on 26/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "EquitiesCell1.h"

@implementation EquitiesCell1

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.tickImageView.layer.cornerRadius=self.tickImageView.frame.size.width/2;
    self.tickImageView.clipsToBounds = YES;
    
  
    
    self.adviceButton.layer.cornerRadius=10.0f;
    self.adviceButton.layer.masksToBounds = NO;
    
    self.originalAdviceTypeButton.layer.cornerRadius=10.0f;
    self.originalAdviceTypeButton.layer.masksToBounds = NO;
    self.durationLbl.layer.cornerRadius=self.durationLbl.frame.size.width/2;
    self.durationLbl.clipsToBounds=YES;
    self.buySellButton.layer.cornerRadius=5.0f;
    self.buySellButton.layer.masksToBounds = NO;
//    self.buySellButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
//    self.buySellButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
//    self.buySellButton.layer.shadowOpacity = 3.0f;
//    self.buySellButton.layer.shadowRadius = 3.0f;
    self.adviceButton.layer.borderWidth = 1.2f;
    self.adviceButton.layer.borderColor = [[UIColor colorWithRed:(87.0)/255 green:(160.0)/255 blue:(55.0)/255 alpha:1.0] CGColor];
    
//    self.buySellButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
//    self.buySellButton.layer.shadowOffset = CGSizeMake(0, 5.0f);
//    self.buySellButton.layer.shadowOpacity = 5.0f;
//    self.buySellButton.layer.shadowRadius = 4.0f;
//    self.buySellButton.layer.cornerRadius=4.1f;
//    self.buySellButton.layer.masksToBounds = NO;
 
    
    self.originalAdviceView.layer.cornerRadius=10.0f;
    self.originalAdviceView.layer.borderWidth = 1.0f;
    self.originalAdviceView.layer.borderColor = [[UIColor colorWithRed:(99.0)/255 green:(99.0)/255 blue:(99.0)/255 alpha:1.0] CGColor];
    self.originalAdviceTypeButton.layer.borderWidth = 1.2f;
    

    self.leaderImg.layer.borderWidth = 0.5f;
    self.leaderImg.layer.borderColor = [[UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0] CGColor];
    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)leaderProfileButton:(id)sender {
}
@end
