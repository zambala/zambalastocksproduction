//
//  MarketMonksView.h
//  testing
//
//  Created by zenwise mac 2 on 11/23/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

/*!
 @author Sanka Revanth
 @class MarketMonksView
 @discussion In this class we have two section follow and who to follow.In follow section we have list of followed leaders and in who to follow we have list of leader who are available.
 @superclass SuperClass: UIViewController\n
 @classdesign    Designed using UIView,Button,BarButton,ImageView,Label,UITableview.
 @coclass    AppDelegate
 
 */


@interface MarketMonksView : UIViewController<UITableViewDataSource,UITableViewDelegate, UISearchDisplayDelegate, UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating>
{
    
       
  
    BOOL searchFlag, filterFlag, equityFlag, deriFlag, currencyFlag, commFlag, tradeFlag, shortTermFlag, longTermFlag;
    
    BOOL starOneFlag, starTwoFlag, starThreeFlag, starFourFlag, starFiveFlag;
    
    
    UIView *popUpView, *titleView;
    UIView *backgroundView;
    
    UILabel  *headingLbl, *segmentLbl, *tradeLbl, *ratingLbl;
    
    UIButton  *equitiesBtn, *derivativesBtn, *currencyBtn, *commoditiesBtn, *dayTradeBtn, *shortTermBtn, *longTermBtn, *equImgBtn, *deriImgBtn, *currImgBtn, *commImgBtn, *dayImgBtn, *shortImgBtn, *longImgBtn, *starOneImg, *starTwoImg, *starThreeImg, *starFourImg, *starFiveImg;

    UIButton *applyBtn, *closeBtn;
    
    UIButton   *starOneImg1, *starTwoImg1, *starThreeImg1, *starFourImg1, *starFiveImg1;
    
    BOOL starOneFlag1, starTwoFlag1, starThreeFlag1, starFourFlag1, starFiveFlag1;

    


}



@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (weak, nonatomic) IBOutlet UITableView *followerTableView;/*!<used to populate list of leaders*/
@property NSMutableArray *profileNameArray/*!<it stores name of leaders*/,*followersArray;/*!<it stores follower count*/
@property NSMutableArray *responseDictionary;/*!<unused*/
@property NSMutableArray *buttonArray;/*!<unused*/
@property NSMutableArray *followPlusArray;/*!<it stores all unfollowing leader list*/
@property NSMutableArray *buttonArrayy;/*!<it stores state(following or unfollowing leader)*/
@property Reachability * reach;/*!<it is used to check newtwork status*/

- (IBAction)searchAction:(id)sender;/*!<it is used to activate search*/
@property UISearchController * controller;

@property NSMutableArray * leaderDict;/*!<unused*/
@property NSMutableArray * userIdArray;/*!<it stores id of leaders*/
@property NSMutableArray * followDict;/*!<it stores all following leader list*/

//methods//

-(void)starOneImg1;
-(void)starTwoImg1;
-(void)starThreeImg1;
-(void)starFourImg1;
-(void)starFiveImg1;
-(void)segmentedControlChangedValue;
-(NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;
-(NSString*)remaningTime:(NSDate*)startDate endDate:(NSDate*)endDate;
-(void)yourButtonClicked:(UIButton*)sender;
-(void)updateSearchResultsForSearchController:(UISearchController *)searchController;
-(void)hideImage;
@end
