//
//  SearchSymbolView.m
//  testing
//
//  Created by zenwise technologies on 16/02/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "SearchSymbolView.h"
#import "AppDelegate.h"
#import "HomePage.h"
#import "PSWebSocket.h"
#import "SearchTableViewCell.h"
#import <UITextField_AutoSuggestion/UITextField+AutoSuggestion.h>
#import <Mixpanel/Mixpanel.h>

@interface SearchSymbolView ()<UITextFieldAutoSuggestionDataSource, UITextFieldDelegate>
{
    NSString * exchangeString;
    NSString * instrumentTypeStr;
    AppDelegate * delegate1;
    NSString * equityStr;
    NSUInteger objectAtIndex;
    NSMutableArray * expiry;
    NSMutableArray * strike;
    NSString * derivativeStr;
    NSString * check;
    NSString * optionStr;
    NSString * exchangeString1;
    NSMutableArray * filteredArray;
    BOOL * strikeCheck;
    NSMutableDictionary * detailsArray;
    NSString * expiryStr;
    NSString * segmentStr;
    NSString * titleStr;
    NSUInteger optionCheck;
    NSArray * segmentsArray;
    NSMutableArray * companyName;
}
@property (strong, nonatomic) NSTimer * searchTimer;

@end

#define Symbol_ID @"symbol_id"
#define WEEKS @[@"Monday", @"Tuesday", @"Wednesday", @"Thirsday", @"Friday", @"Saturday", @"Sunday"]


@implementation SearchSymbolView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //appdelegate//
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"add_market_watch_symbol_page"];
    segmentsArray=[[NSArray alloc]init];
    if([delegate1.loginActivityStr isEqualToString:@"OTP1"])
    {
        
        segmentsArray=[delegate1.brokerInfoDict objectForKey:@"guestsegment"];
    }
    
    else
    {
        segmentsArray=[delegate1.brokerInfoDict objectForKey:@"segment"];
    }
    self.sampleView.hidden=YES;;
    self.company=[[NSMutableArray alloc]init];
    self.symbols=[[NSMutableArray alloc]init];
    self.symbolTxt.delegate=self;
//    if([delegate1.brokerNameStr isEqualToString:@"Zerodha"]||[delegate1.brokerNameStr isEqualToString:@"Upstox"])
//    {
    
        
//    }
//
//    else
//    {
    self.equityBtn.enabled=NO;
    self.derivativeBtn.enabled=NO;
        self.currencyBtn.enabled=NO;
        self.commodityBtn.enabled=NO;
        
//    }
    
   if(segmentsArray.count>0)
   {
    if([segmentsArray containsObject:@1])
    {
        self.equityBtn.enabled=YES;
        
        
    }
    
    if([segmentsArray containsObject:@2])
    {
        self.derivativeBtn.enabled=YES;
        
    }
    if([segmentsArray containsObject:@3])
    {
       self.currencyBtn.enabled=YES;
        
    }
   
   }
    
    else
    {
        self.equityBtn.enabled=YES;
        self.derivativeBtn.enabled=YES;
        self.currencyBtn.enabled=YES;
         self.commodityBtn.enabled=NO;
        
    }
    
    self.responseArray=[[NSMutableArray alloc]init];
    self.pickerView.hidden=YES;
    self.expiryBtnOut.hidden=YES;
    self.strikeBtnOut.hidden=YES;
    filteredArray=[[NSMutableArray alloc]init];
    self.addSymbolButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.addSymbolButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.addSymbolButton.layer.shadowOpacity = 1.0f;
    self.addSymbolButton.layer.shadowRadius = 1.0f;
    self.addSymbolButton.layer.cornerRadius=2.1f;
    self.addSymbolButton.layer.masksToBounds = NO;
    
    self.responseArray1=[[NSMutableArray alloc]init];
     expiry=[[NSMutableArray alloc]init];
   strike=[[NSMutableArray alloc]init];
    
    self.futureBtn.enabled=NO;
     self.optionBtn.enabled=NO;
    self.expiryBtnOut.enabled=NO;
    

    
    [self.exchangeSegment addTarget:self action:@selector(selectedSegment) forControlEvents:UIControlEventValueChanged];
    
    [self setBIDRadioBtn];
    
    [self.equityBtn addTarget:self action:@selector(equityAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.derivativeBtn addTarget:self action:@selector(derivativeAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.currencyBtn addTarget:self action:@selector(currencyAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.commodityBtn addTarget:self action:@selector(commodityAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.futureBtn addTarget:self action:@selector(futureAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.optionBtn addTarget:self action:@selector(optionAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.CE addTarget:self action:@selector(ceAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.PEBtn addTarget:self action:@selector(peAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.scroll.contentSize=CGSizeMake(375, 800);
    
    instrumentTypeStr=@"EQ";
    exchangeString1=@"NSE";
    
    
    [self equityAction];
    
    self.symbolTxt.delegate=self;
    self.searchTableView.hidden=YES;
    
    self.symbolTxt.delegate = self;
    self.symbolTxt.autoSuggestionDataSource = self;
    self.symbolTxt.fieldIdentifier = Symbol_ID;
    [self.symbolTxt observeTextFieldChanges];
    
}
//-(void)textFieldDidChange :(UITextField *)textField{
//
//    // if a timer is already active, prevent it from firing
//    if (self.searchTimer != nil) {
//        [self.searchTimer invalidate];
//        self.searchTimer = nil;
//    }
//
//    // reschedule the search: in 1.0 second, call the searchForKeyword: method on the new textfield content
//    self.searchTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0
//                                                        target: self
//                                                      selector: @selector(searchForKeyword:)
//                                                      userInfo: self.symbolTxt.text
//                                                       repeats: NO];
//
//}

- (void) searchForKeyword:(NSTimer *)timer
{
    // retrieve the keyword from user info
    NSString *keyword = (NSString*)timer.userInfo;
    self.searchStr=keyword;
    [self searchServer:self.searchStr];
    [self loadWeekDays];
   // [self segmentedControlChangedValue];
    // perform your search (stubbed here using //NSLog)
    //NSLog(@"Searching for keyword %@", keyword);
}


- (void)loadWeekDays {
    // cancel previous requests
    @try {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(loadWeekDaysInBackground) object:self];
        [self.symbolTxt setLoading:false];
        
        // clear previous results
        [self.symbols removeAllObjects];
        [self.symbolTxt reloadContents];
        
        // start loading
        [self.symbolTxt setLoading:true];
        [self performSelector:@selector(loadWeekDaysInBackground) withObject:self afterDelay:2.0f];

        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    }

- (void)loadWeekDaysInBackground {
    // finish loading
    @try {
        [self.symbolTxt setLoading:false];
        
        
        [self.symbols addObjectsFromArray:self.company];
        
        [self.symbolTxt reloadContents];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}


//    [self.symbols addObjectsFromArray:WEEKS];




//segment method//

-(void)selectedSegment
{
    if(self.exchangeSegment.selectedSegmentIndex==0)
    {
        exchangeString=@"NSE";
        exchangeString1=@"NSE";
        self.commodityBtn.enabled=NO;
        self.equityBtn.enabled=YES;
        self.equityBtn.selected=YES;
        self.derivativeBtn.enabled=YES;
        self.currencyBtn.selected=NO;
        self.derivativeBtn.selected=NO;
        self.currencyBtn.selected=NO;
        [self equityAction];
         self.derivativeBtn.enabled=YES;
        
        if([delegate1.brokerNameStr isEqualToString:@"Zerodha"]||[delegate1.brokerNameStr isEqualToString:@"Upstox"]||[delegate1.loginActivityStr isEqualToString:@"OTP1"])
        {
            self.currencyBtn.enabled=YES;
            
        }
        
        else
        {
            self.currencyBtn.enabled=NO;
            
        }
    }
    else if (self.exchangeSegment.selectedSegmentIndex==1)
    {
        exchangeString=@"BSE";
         exchangeString1=@"BSE";
        self.commodityBtn.enabled=NO;
        self.equityBtn.enabled=YES;
        self.derivativeBtn.enabled=YES;
        self.currencyBtn.selected=NO;
        self.equityBtn.selected=YES;
        self.derivativeBtn.selected=NO;
        self.currencyBtn.selected=NO;
        [self equityAction];
        
        if([delegate1.brokerNameStr isEqualToString:@"Zerodha"]||[delegate1.brokerNameStr isEqualToString:@"Upstox"]||[delegate1.loginActivityStr isEqualToString:@"OTP1"])
        {
            self.currencyBtn.enabled=YES;
            
        }
        
        else
        {
            self.currencyBtn.enabled=NO;
            
        }
        
        
       
    }
    
    else if (self.exchangeSegment.selectedSegmentIndex==2)
    {
        exchangeString=@"MCX";
        exchangeString1=@"MCX";
       
        self.currencyBtn.selected=YES;
        self.equityBtn.enabled=NO;
        self.derivativeBtn.enabled=NO;
        [self currencyAction];
        self.PEBtn.selected=NO;
         equityStr=@"FUT";
        [self futureAction];
        
        if([delegate1.brokerNameStr isEqualToString:@"Zerodha"]||[delegate1.brokerNameStr isEqualToString:@"Upstox"]||[delegate1.loginActivityStr isEqualToString:@"OTP1"])
        {
            self.currencyBtn.enabled=YES;
             self.commodityBtn.enabled=YES;
            
        }
        
        else
        {
            self.currencyBtn.enabled=NO;
             self.commodityBtn.enabled=NO;
            
        }
       
        
    }
}



//RADIO BUTTON//

-(void)setBIDRadioBtn
{
    [self.equityBtn setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    [self.equityBtn setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    self.equityBtn.selected = NO;
    
    [self.derivativeBtn setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    [self.derivativeBtn setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    self.derivativeBtn.selected = NO;
    
    [self.currencyBtn setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    [self.currencyBtn setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    self.currencyBtn.selected = NO;
    
    [self.commodityBtn setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    [self.commodityBtn setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    self.commodityBtn.selected = NO;
    
    [self.futureBtn setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    [self.futureBtn setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    self.futureBtn.selected = NO;
    
    [self.optionBtn setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    [self.optionBtn setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    self.optionBtn.selected = NO;
    
    [self.CE setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    [self.CE setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    self.CE.selected = NO;
    
    [self.PEBtn setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    [self.PEBtn setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    self.PEBtn.selected = NO;
    //radio-sel
}

//button methods//

-(void)equityAction
{
    self.equityBtn.selected = YES;
    self.derivativeBtn.selected = NO;
    self.currencyBtn.selected = NO;
    self.commodityBtn.selected = NO;
    self.instrumentLbl.hidden=NO;
    self.instrumentHgt.constant=0;
    self.instrumentTxtHgt.constant=0;
    self.instrumentLblTop.constant=0;
    self.instrumentViewTop.constant=0;
    self.instrumentView.hidden=YES;
    self.optionLbl.hidden=YES;
    self.optionLblHgt.constant=0;
     self.optionViewHgt.constant=0;
    self.optionLblTop.constant=0;
    self.optionViewTop.constant=0;
    self.optionView.hidden=YES;
    self.expiryDateLbl.hidden=YES;
    self.expiryLblHgt.constant=0;
     self.expiryTxtHgt.constant=0;
    self.expiryDateLblTop.constant=0;
    self.expiryDateTFTop.constant=0;
    self.expiryDateTxt.hidden=YES;
    self.strikePrice.hidden=YES;
    self.strikeHgt.constant=0;
    self.strikeTxtHgt.constant=0;
    self.strikePricelblTop.constant=0;
    self.strikePriceTFTop.constant=0;
    self.strikePriceTxt.hidden=YES;
    self.expHeightContraint.constant=0;
    self.strikeHeightConstraint.constant=0;
    self.lineViewHeightConstraint.constant=0;
    self.imageViewHeightContraint.constant=0;
    self.spLineViewHeightConstraint.constant=0;
    self.spImageViewHeightConstraint.constant=0;
    self.TFHeightConstraint.constant=0;
    self.spTFHeightConstraint.constant=0;
    self.expiryDropDownImg.hidden=YES;
    self.strikeDropDownImg.hidden=YES;
    self.symbolTxt.text=@"";
    self.expiryDateTxt.text=@"";
    self.strikePriceTxt.text=@"";
    
    equityStr=@"EQ";
     derivativeStr=@" ";
    self.expiryBtnOut.hidden=YES;
    self.strikeBtnOut.hidden=YES;
    
    self.symbolTxt.enabled=YES;
    
    if(self.equityBtn.selected==YES&&self.exchangeSegment.selectedSegmentIndex==0)
    {
        exchangeString1=@"NSE";
        
    }
    
    else if(self.equityBtn.selected==YES&&self.exchangeSegment.selectedSegmentIndex==1)
    {
        exchangeString1=@"BSE";
        
    }
    //    else if(self.exchangeSegment.selectedSegmentIndex==2)
    //    {
    //        segmentStr=@"NFO-FUT";
    //    }
    

   

    
}

-(void)derivativeAction
{
    self.equityBtn.selected = NO;
    self.derivativeBtn.selected = YES;
    self.currencyBtn.selected = NO;
    self.commodityBtn.selected = NO;
    self.instrumentLbl.hidden=NO;
    self.instrumentView.hidden=NO;
    self.futureBtn.selected = YES;
    self.instrumentHgt.constant=14;
    self.instrumentTxtHgt.constant=63.2;
    self.instrumentLblTop.constant=24;
    self.instrumentViewTop.constant=10;
    self.optionLbl.hidden=YES;
    self.optionLblHgt.constant=0;
    self.optionViewHgt.constant=0;
    self.optionLblTop.constant=0;
    self.optionViewTop.constant=0;
    self.optionView.hidden=YES;
    self.expiryDateLbl.hidden=NO;
    self.expiryLblHgt.constant=14;
    self.expiryTxtHgt.constant=30;
    self.expiryDateLblTop.constant=24;
    self.expiryDateTFTop.constant=10;
    self.expiryDateTxt.hidden=NO;
   // self.strikePrice.hidden=YES;
    //self.strikeHgt.constant=0;
    //self.strikeTxtHgt.constant=0;
    //self.strikePricelblTop.constant=0;
    //self.strikePriceTFTop.constant=0;
    //self.strikePriceTxt.hidden=YES;
    self.expHeightContraint.constant=63.4;
    self.strikeHeightConstraint.constant=63.4;
    self.lineViewHeightConstraint.constant=1;
    self.spLineViewHeightConstraint.constant=1;
    self.imageViewHeightContraint.constant=30;
    self.spImageViewHeightConstraint.constant=30;
    self.TFHeightConstraint.constant=17;
    self.spTFHeightConstraint.constant=17;
    self.symbolTxt.text=@"";
    self.expiryDateTxt.text=@"";
    self.strikePriceTxt.text=@"";
    
    self.strikeView.hidden=YES;
    self.strikeDropDown.hidden=YES;
    
     self.strikePricelblTop.constant=14;
    self.futureBtn.selected=YES;
    
    self.strikePrice.hidden=NO;
    self.strikeHgt.constant=0;
    self.strikeTxtHgt.constant=0;
    self.strikePricelblTop.constant=0;
    self.strikePriceTFTop.constant=14;
    self.strikePriceTxt.hidden=YES;
    self.strikeHeightConstraint.constant=0;

//    if([exchangeString isEqualToString:@"BSE"])
//    {
//        exchangeString=@"BFO";
//    }
//    
//    else if([exchangeString isEqualToString:@"NSE"])
//    {
//        exchangeString=@"NFO";
//    }
    
    equityStr=@"DR";
//    derivativeStr=@"Dr";
    self.expiryBtnOut.hidden=NO;
    self.strikeBtnOut.hidden=YES;
    
    self.symbolTxt.enabled=YES;
    
    
    
     if(self.derivativeBtn.selected==YES&&self.exchangeSegment.selectedSegmentIndex==0)
    {
        exchangeString=@"NFO-FUT";
        exchangeString1=@"NFO";
    }
    
    else if(self.derivativeBtn.selected==YES&&self.exchangeSegment.selectedSegmentIndex==1)
    {
        exchangeString=@"BFO-FUT";
         exchangeString1=@"BFO";
    }
//    else if(self.exchangeSegment.selectedSegmentIndex==2)
//    {
//        segmentStr=@"NFO-FUT";
//    }
    


}

-(void)currencyAction
{
    self.optionView.hidden=YES;
    self.equityBtn.selected = NO;
    self.derivativeBtn.selected = NO;
    self.currencyBtn.selected = YES;
    self.commodityBtn.selected = NO;
    self.instrumentLbl.hidden=NO;
    self.instrumentView.hidden=NO;
    self.futureBtn.selected = YES;
    self.instrumentHgt.constant=14;
    self.instrumentTxtHgt.constant=63.2;
    self.instrumentLblTop.constant=24;
    self.instrumentViewTop.constant=10;
    self.optionLbl.hidden=YES;
    self.optionLblHgt.constant=0;
    self.optionViewHgt.constant=0;
    self.optionLblTop.constant=0;
    self.optionViewTop.constant=0;
    self.optionView.hidden=YES;
    self.expiryDateLbl.hidden=NO;
    self.expiryLblHgt.constant=14;
    self.expiryTxtHgt.constant=30;
    self.expiryDateLblTop.constant=24;
    self.expiryDateTFTop.constant=10;
    self.expiryDateTxt.hidden=NO;
    // self.strikePrice.hidden=YES;
    //self.strikeHgt.constant=0;
    //self.strikeTxtHgt.constant=0;
    //self.strikePricelblTop.constant=0;
    //self.strikePriceTFTop.constant=0;
    //self.strikePriceTxt.hidden=YES;
    self.expHeightContraint.constant=63.4;
    self.strikeHeightConstraint.constant=63.4;
    self.lineViewHeightConstraint.constant=1;
    self.spLineViewHeightConstraint.constant=1;
    self.imageViewHeightContraint.constant=30;
    self.spImageViewHeightConstraint.constant=30;
    self.TFHeightConstraint.constant=17;
    self.spTFHeightConstraint.constant=17;
   
    self.symbolTxt.text=@"";
    self.symbolTxt.enabled=YES;
    self.expiryDateTxt.text=@"";
    self.strikePriceTxt.text=@"";
    
    self.strikeView.hidden=YES;
    self.strikeDropDown.hidden=YES;
    
    self.strikePricelblTop.constant=14;
    
    self.expiryBtnOut.hidden=NO;
    self.strikeBtnOut.hidden=YES;
    
    
   exchangeString=@"CDS-FUT";
    
    derivativeStr=@"Dr";
    equityStr=@"CDS";
    
//    [self futureAction];
    
    


}

-(void)commodityAction
{
    self.equityBtn.selected = NO;
    self.derivativeBtn.selected = NO;
    self.currencyBtn.selected = NO;
    self.commodityBtn.selected = YES;
    self.instrumentLbl.hidden=NO;
    self.instrumentView.hidden=NO;
    self.futureBtn.selected = YES;
     equityStr=@"FUT";
    self.PEBtn.selected=NO;
     exchangeString=@"MCX";
    self.symbolTxt.text=@"";
    self.expiryDateTxt.text=@"";
    self.strikePriceTxt.text=@"";
    self.PEBtn.selected=NO;
    self.expiryBtnOut.hidden=NO;
    self.strikeBtnOut.hidden=YES;
    self.symbolTxt.enabled=YES;

}

-(void)futureAction
{
//    self.equityBtn.selected = NO;
//    self.derivativeBtn.selected = YES;
//    self.currencyBtn.selected = NO;
//    self.commodityBtn.selected = NO;
    @try
    {
    self.instrumentLbl.hidden=NO;
    self.instrumentView.hidden=NO;
    self.futureBtn.selected = YES;
    self.instrumentHgt.constant=14;
    self.instrumentTxtHgt.constant=63.2;
    self.instrumentLblTop.constant=24;
    self.instrumentViewTop.constant=10;
    self.optionLbl.hidden=YES;
    self.optionLblHgt.constant=0;
    self.optionViewHgt.constant=0;
    self.optionLblTop.constant=0;
    self.optionViewTop.constant=0;
    self.optionView.hidden=YES;
    self.expiryDateLbl.hidden=NO;
    self.expiryLblHgt.constant=14;
    self.expiryTxtHgt.constant=30;
    self.expiryDateLblTop.constant=24;
    self.expiryDateTFTop.constant=10;
    self.expiryDateTxt.hidden=NO;
    self.strikePrice.hidden=NO;
    self.strikeHgt.constant=0;
    self.strikeTxtHgt.constant=0;
    self.strikePricelblTop.constant=0;
    self.strikePriceTFTop.constant=14;
    self.strikePriceTxt.hidden=YES;
    self.optionBtn.selected = NO;
    self.strikeHeightConstraint.constant=0;
    self.spImageViewHeightConstraint.constant=0;
    self.spLineViewHeightConstraint.constant=0;
    
    self.strikeView.hidden=YES;
    self.strikeDropDown.hidden=YES;
    self.expiryBtnOut.hidden=NO;
    self.strikeBtnOut.hidden=YES;
   
    
    
    self.pickerView.hidden=YES;
    self.sampleView.hidden=YES;
    
    self.expiryDropDownImg.hidden=NO;
    self.strikeDropDownImg.hidden=YES;
    
    //NSLog(@"%@",exchangeString);
    
    if([equityStr isEqualToString:@"DR"]||[equityStr isEqualToString:@"CDS"])
    {
        
        if(self.currencyBtn.selected==YES)
        {
            exchangeString=@"CDS-FUT";
        }
        
        else if(self.derivativeBtn.selected==YES&&self.exchangeSegment.selectedSegmentIndex==0)
        {
             exchangeString=@"NFO-FUT";
        }
        
        else if(self.derivativeBtn.selected==YES&&self.exchangeSegment.selectedSegmentIndex==1)
        {
            exchangeString=@"BFO-FUT";
        }
        
        
        @try {
            NSDictionary *headers = @{ 
                                       @"cache-control": @"no-cache",
                                        @"authtoken":delegate1.zenwiseToken,
                                          @"deviceid":delegate1.currentDeviceId
                                       };
            
            NSString * urlStr=[NSString stringWithFormat:@"%@stock/fao/%@?segment=%@&orderby=ASC",delegate1.baseUrl,self.symbolTxt.text,exchangeString];
            
            //NSLog(@"url %@",urlStr);
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:10.0];
            [request setHTTPMethod:@"GET"];
            [request setAllHTTPHeaderFields:headers];
            
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            if (error) {
                                                                //NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                //NSLog(@"%@", httpResponse);
                                                                
                                                                //                                                            [self.company removeAllObjects];
                                                                if([httpResponse statusCode]==403)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                    
                                                                }
                                                                else if([httpResponse statusCode]==401)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    
                                                                [expiry removeAllObjects];
                                                                [strike removeAllObjects];
                                                                ;
                                                                
                                                                self.responseArray1=[[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] mutableCopy];
                                                                
                                                                [delegate1.searchSymbolDict setObject:self.responseArray1 forKey:@"details"];
                                                                
                                                                //NSLog(@"%@",self.responseArray1);
                                                                
                                                                if(self.responseArray1.count>0)
                                                                {
                                                                    
//                                                                    NSString * string=[NSString stringWithFormat:@"%@",self.responseArray];
                                                                    
                                                                    self.detailsDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSArray arrayWithArray:self.responseArray1],@"details", nil];
                                                                    
                                                                    for(int i=0;i<self.responseArray1.count;i++)
                                                                    {
                                                                        
                                                                        //                                                                [self.company addObject:[[self.responseArray1 objectAtIndex:i]objectForKey:@"symbol"]];
                                                                        
                                                                        NSString *myString = [[self.responseArray1 objectAtIndex:i]objectForKey:@"expiryseries"];
                                                                        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                                                                        dateFormatter.dateFormat = @"yyyy-MM-dd";
                                                                        NSDate *yourDate = [dateFormatter dateFromString:myString];
                                                                        dateFormatter.dateFormat = @"dd MMM yyyy";
                                                                        //NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
                                                                        
                                                                        NSString * finalDate=[[dateFormatter stringFromDate:yourDate] uppercaseString];
                                                                        
                                                                        [expiry addObject:finalDate];
                                                                        
                                                                        
                                                                        
                                                                        
                                                                    }
                                                                    
                                                                    [self.responseArray removeAllObjects];
                                                                    
                                                                    //NSLog(@"expiry%@",expiry);
                                                                }
                                                                }
                                                            }
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                //                                                          HomePage * home1=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
                                                                //
                                                                //                                                          [self.navigationController pushViewController:home1 animated:YES];
                                                                if(expiry.count>0)
                                                                {
                                                                    [self.expiryBtnOut setTitle:[expiry objectAtIndex:0] forState:UIControlStateNormal];
                                                                    
                                                                    
                                                                }
                                                            });
                                                        }];
            [dataTask resume];

            
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
        
        
        
    }
    

    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

}

-(void)optionAction
{
    
    self.futureBtn.selected = NO;
    self.optionBtn.selected = YES;
    self.instrumentLbl.hidden=NO;
    self.instrumentHgt.constant=14;
    self.instrumentTxtHgt.constant=63.2;
    self.instrumentLblTop.constant=24;
    self.instrumentViewTop.constant=10;
    self.instrumentView.hidden=NO;
    self.optionLbl.hidden=NO;
    self.optionLblHgt.constant=14;
    self.optionViewHgt.constant=63.2;
    self.optionLblTop.constant=24;
    self.optionViewTop.constant=10;
    self.optionView.hidden=NO;
    self.expiryDateLbl.hidden=NO;
    self.expiryLblHgt.constant=14;
    self.expiryTxtHgt.constant=30;
    self.expiryDateLblTop.constant=24;
    self.expiryDateTFTop.constant=10;
    self.expiryDateTxt.hidden=NO;
    self.strikePrice.hidden=NO;
    self.strikeHgt.constant=14;
    self.strikeTxtHgt.constant=30;
    self.strikePricelblTop.constant=14;
    self.strikePriceTFTop.constant=10;
    self.strikePriceTxt.hidden=NO;
    [self ceAction];
    self.expiryDropDownImg.hidden=NO;
    self.strikeDropDownImg.hidden=NO;
    
    self.strikeView.hidden=NO;
    self.strikeDropDown.hidden=NO;
    
    self.strikeHeightConstraint.constant=63.4;
    self.spImageViewHeightConstraint.constant=30;
    self.spLineViewHeightConstraint.constant=1;
    self.expiryBtnOut.hidden=NO;
    self.strikeBtnOut.hidden=NO;
    self.pickerView.hidden=YES;
    self.sampleView.hidden=YES;
    
    

}

-(void)ceAction
{
    @try
    {
    self.CE.selected = YES;
    self.PEBtn.selected = NO;
    instrumentTypeStr=@"CE";
    self.pickerView.hidden=YES;
    self.sampleView.hidden=YES;

    
    
    
    if([equityStr isEqualToString:@"DR"]||[equityStr isEqualToString:@"CDS"])
    {
        
        optionStr=@"CE";
        
        if(self.currencyBtn.selected==YES)
        {
            exchangeString=@"CDS-OPT";
        }
        
        else if(self.derivativeBtn.selected==YES&&self.exchangeSegment.selectedSegmentIndex==0)
        {
            exchangeString=@"NFO-OPT";
        }
        
        else if(self.derivativeBtn.selected==YES&&self.exchangeSegment.selectedSegmentIndex==1)
        {
            exchangeString=@"BFO-OPT";
        }

        @try {
            NSDictionary *headers = @{
                                       @"cache-control": @"no-cache",
                                        @"authtoken":delegate1.zenwiseToken,
                                          @"deviceid":delegate1.currentDeviceId
                                       };
           NSString * urlStr=[NSString stringWithFormat:@"%@stock/fao/%@?segment=%@&optiontype=%@&limit=20000&orderby=ASC",delegate1.baseUrl,self.symbolTxt.text,exchangeString,optionStr];
            
            //NSLog(@"url %@",urlStr);
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:10.0];
            [request setHTTPMethod:@"GET"];
            [request setAllHTTPHeaderFields:headers];
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            if (error) {
                                                                //NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                 //NSLog(@"%@", httpResponse);
                                                                
                                                                //                                                        [self.company removeAllObjects];
                                                                
                                                                //
                                                                if([httpResponse statusCode]==403)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                    
                                                                }
                                                                else if([httpResponse statusCode]==401)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                    
                                                                }
                                                                else
                                                                {
                                                                [expiry removeAllObjects];
                                                                if(strike.count>0)
                                                                {
                                                                [strike removeAllObjects];
                                                                }
                                                                
                                                                if(filteredArray.count>0)
                                                                {
                                                                    [filteredArray removeAllObjects];
                                                                }
                                                                
                                                                self.responseArray1=[[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] mutableCopy];
                                                               
                                                                //NSLog(@"%@",self.responseArray1);
                                                                
                                                                if(self.responseArray1.count>0)
                                                                {
                                                                    
                                                                    [delegate1.searchSymbolDict setObject:self.responseArray1 forKey:@"details"];
                                                                    
                                                                    //NSLog(@"%@",self.responseArray1);
                                                                    
                                                                    NSString * string=[NSString stringWithFormat:@"%@",self.responseArray];
                                                                    
                                                                    self.detailsDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSArray arrayWithArray:self.responseArray1],@"details", nil];
                                                                    
                                                                    
                                                                    
                                                                    //                                                            [self.company addObject:[[self.responseArray1 objectAtIndex:i]objectForKey:@"symbol"]];
                                                                    
                                                                    //                                                            [expiry addObject:[[self.responseArray1 objectAtIndex:i]objectForKey:@"expiry"]];
                                                                    if(expiry.count>0)
                                                                    {
                                                                        expiryStr=[expiry objectAtIndex:0];
                                                                    }
                                                                    expiry=[[NSMutableArray alloc]init];
                                                                    for(int i=0;i<_responseArray1.count;i++)
                                                                    {
                                                                        
                                                                        NSString * str=[[_responseArray1 objectAtIndex:i] objectForKey:@"expiryseries"];
                                                                        //NSLog(@"sdc %@",str);
                                                                        //            NSString *myString = [number stringValue];
                                                                        //             //NSLog(@"sdc %@",myString);
                                                                        
                                                                        NSString *myString = [[self.responseArray1 objectAtIndex:i]objectForKey:@"expiryseries"];
                                                                        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                                                                        dateFormatter.dateFormat = @"yyyy-MM-dd";
                                                                        NSDate *yourDate = [dateFormatter dateFromString:myString];
                                                                        dateFormatter.dateFormat = @"dd MMM yyyy";
                                                                        //NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
                                                                        
                                                                        NSString * finalDate=[[dateFormatter stringFromDate:yourDate] uppercaseString];
//                                                                        rtrt
//                                                                        if(expiry.count==0)
//                                                                        {
//                                                                               [expiry addObject:finalDate];
//                                                                        }
//
//                                                                        else
//                                                                        {
//                                                                            if([expiry containsObject:finalDate])
//                                                                            {
//
//                                                                            }
//                                                                            else
//                                                                            {
                                                                        [expiry addObject:finalDate];
//                                                                            }
//                                                                        }
                                                                        
                                                                        
                                                                        
                                                                     
                                                                        if(expiry.count>0)
                                                                        {
                                                                            expiryStr=[expiry objectAtIndex:0];
                                                                        }
                                                                        
                                                                        
                                                                        
                                                                        if([expiryStr isEqualToString:finalDate])
                                                                        {
                                                                            [strike addObject:[[self.responseArray1 objectAtIndex:i]objectForKey:@"strikeprice"]];
                                                                            //NSLog(@"sdc%@",strike);
                                                                            
                                                                            //NSLog(@"%@",[self.responseArray1 objectAtIndex:i]);
                                                                            
                                                                            
                                                                            [filteredArray addObject:[self.responseArray1 objectAtIndex:i]];
                                                                            //NSLog(@"%@",filteredArray);
                                                                            
                                                                        }
                                                                    }

                                                                    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:expiry];
                                                                    expiry = [[orderedSet array]mutableCopy];
                                                                    //NSLog(@"expiry%@",expiry);
                                                                }
                                                                }
                                                            }
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                //                                                          HomePage * home1=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
                                                                //
                                                                //                                                          [self.navigationController pushViewController:home1 animated:YES];
                                                                //                                                        [self.expiryBtnOut setTitle:[expiry objectAtIndex:0] forState:UIControlStateNormal];
                                                                if(strike.count>0)
                                                                {
                                                                    [self.strikeBtnOut setTitle:[strike objectAtIndex:0] forState:UIControlStateNormal];
                                                                    
                                                                    
                                                                }
                                                                
                                                                
                                                            });
                                                        }];
            [dataTask resume];

            
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
        
    }
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}

-(void)peAction
{
    @try
    {
    self.CE.selected = NO;
    self.PEBtn.selected = YES;
    self.pickerView.hidden=YES;
    self.sampleView.hidden=YES;

    
    
    if([equityStr isEqualToString:@"DR"]||[equityStr isEqualToString:@"CDS"])
    {
        
       optionStr=@"PE";
        
        if(self.currencyBtn.selected==YES)
        {
            exchangeString=@"CDS-OPT";
        }
        
        else if(self.derivativeBtn.selected==YES&&self.exchangeSegment.selectedSegmentIndex==0)
        {
            exchangeString=@"NFO-OPT";
        }
        
        else if(self.derivativeBtn.selected==YES&&self.exchangeSegment.selectedSegmentIndex==1)
        {
            exchangeString=@"BFO-OPT";
        }
        
        @try {
            NSDictionary *headers = @{
                                       @"cache-control": @"no-cache",
                                        @"authtoken":delegate1.zenwiseToken,
                                          @"deviceid":delegate1.currentDeviceId
                                       };
            
            NSString * urlStr=[NSString stringWithFormat:@"%@stock/fao/%@?segment=%@&optiontype=%@&limit=20000&orderby=ASC",delegate1.baseUrl,self.symbolTxt.text,exchangeString,optionStr];
            
            //NSLog(@"url %@",urlStr);
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:10.0];
            [request setHTTPMethod:@"GET"];
            [request setAllHTTPHeaderFields:headers];
            
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            if (error) {
                                                                //NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                //NSLog(@"%@", httpResponse);
                                                                
                                                                //                                                        [self.company removeAllObjects];
                                                                
                                                                //                                                        [expiry removeAllObjects];
                                                                if([httpResponse statusCode]==403)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                    
                                                                }
                                                                else if([httpResponse statusCode]==401)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    
                                                                if(strike.count>0)
                                                                {
                                                                    [strike removeAllObjects];
                                                                }
                                                                
                                                                if(filteredArray.count>0)
                                                                {
                                                                    [filteredArray removeAllObjects];
                                                                }
                                                                
                                                                ;
                                                                
                                                                self.responseArray1=[[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] mutableCopy];
                                                                //NSLog(@"%@",self.responseArray1);
                                                                
                                                                if(self.responseArray1.count>0)
                                                                {
                                                                    
                                                                    [delegate1.searchSymbolDict setObject:self.responseArray1 forKey:@"details"];
                                                                    
                                                                    //NSLog(@"%@",self.responseArray1);
                                                                    
                                                                    NSString * string=[NSString stringWithFormat:@"%@",self.responseArray];
                                                                    
                                                                    self.detailsDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSArray arrayWithArray:self.responseArray1],@"details", nil];
                                                                    
                                                                    //NSLog(@"%@",self.detailsDict);
                                                                    
                                                                    
                                                                    if(expiry.count>0)
                                                                    {
                                                                        expiryStr=[expiry objectAtIndex:0];
                                                                    }
                                                                    for(int i=0;i<_responseArray1.count;i++)
                                                                    {
                                                                        
                                                                        NSString * str=[[_responseArray1 objectAtIndex:i] objectForKey:@"expiryseries"];
                                                                        //NSLog(@"sdc %@",str);
                                                                        //            NSString *myString = [number stringValue];
                                                                        //             //NSLog(@"sdc %@",myString);
                                                                        
                                                                        NSString *myString = [[self.responseArray1 objectAtIndex:i]objectForKey:@"expiryseries"];
                                                                        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                                                                        dateFormatter.dateFormat = @"yyyy-MM-dd";
                                                                        NSDate *yourDate = [dateFormatter dateFromString:myString];
                                                                        dateFormatter.dateFormat = @"dd MMM yyyy";
                                                                        //NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
                                                                        
                                                                        NSString * finalDate=[[dateFormatter stringFromDate:yourDate] uppercaseString];
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        if([expiryStr isEqualToString:finalDate])
                                                                        {
                                                                            [strike addObject:[[self.responseArray1 objectAtIndex:i]objectForKey:@"strikeprice"]];
                                                                            //NSLog(@"sdc%@",strike);
                                                                            
                                                                            //NSLog(@"%@",[self.responseArray1 objectAtIndex:i]);
                                                                            
                                                                            
                                                                            [filteredArray addObject:[self.responseArray1 objectAtIndex:i]];
                                                                            //NSLog(@"%@",filteredArray);
                                                                            
                                                                        }
                                                                    }
                                                                    
                                                                    
                                                                }
                                                                }
                                                            }
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                //                                                          HomePage * home1=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
                                                                //
                                                                //                                                          [self.navigationController pushViewController:home1 animated:YES];
                                                                //                                                        [self.expiryBtnOut setTitle:[expiry objectAtIndex:0] forState:UIControlStateNormal];
                                                                if(strike.count>0)
                                                                {
                                                                    [self.strikeBtnOut setTitle:[strike objectAtIndex:0] forState:UIControlStateNormal];
                                                                    
                                                                    
                                                                }
                                                                
                                                                
                                                            });
                                                        }];
            [dataTask resume];
            
            
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }

        
           }
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)addSymbol:(id)sender {
    
    

    @try {
        if(self.symbolTxt.text.length>0)
        {
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        if(prefs !=nil)
        {
            
            // getting an NSString
            
            //        delegate1.instrumentToken=[prefs objectForKey:@"instrumentToken"];
            //        delegate1.instrumentNameArr=[prefs objectForKey:@"instrumentNameArr"];
            //        delegate1.sampleDepthLot=[prefs objectForKey:@"sampleDepthLot"];
            //        delegate1.exchange=[prefs objectForKey:@"exchange"];
            //        delegate1.filteredCompanyName=[prefs objectForKey:@"filteredCompanyName"];
            
            delegate1.instrumentToken = [NSMutableArray arrayWithArray:[prefs objectForKey:@"instrumentToken"]];
            delegate1.instrumentNameArr = [NSMutableArray arrayWithArray:[prefs objectForKey:@"instrumentNameArr"]];
            delegate1.sampleDepthLot = [NSMutableArray arrayWithArray:[prefs objectForKey:@"sampleDepthLot"]];
            delegate1.exchange = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"exchange"]];
            delegate1.filteredCompanyName = [NSMutableArray arrayWithArray:[prefs objectForKey:@"filteredCompanyName"]];
             delegate1.exchangeToken = [NSMutableArray arrayWithArray:[prefs objectForKey:@"exchange_token"]];
             delegate1.exchangeToken = [NSMutableArray arrayWithArray:[prefs objectForKey:@"exchangevalue"]];
            
            @try {
                delegate1.fincode = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"fincode"]];
                
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            @try {
                delegate1.lotsizeArray = [NSMutableArray arrayWithArray:[prefs objectForKey:@"lotsize"]];
                 delegate1.expiryDateArray = [NSMutableArray arrayWithArray:[prefs objectForKey:@"expiryseries"]];
                
            } @catch (NSException *exception) {
                
            } @finally {
                
            }

            
            @try {
                delegate1.strikeArray = [NSMutableArray arrayWithArray:[prefs objectForKey:@"strikeprice"]];
                delegate1.optionArray = [NSMutableArray arrayWithArray:[prefs objectForKey:@"optiontype"]];
                
            } @catch (NSException *exception) {
                
            } @finally {
                
            }

            
            
            
            //NSLog(@"%@",delegate1.instrumentToken);
            //NSLog(@"%@",delegate1.instrumentNameArr);
            //NSLog(@"%@",delegate1.sampleDepthLot);
            //NSLog(@"%@",delegate1.exchange);
            //NSLog(@"%@",delegate1.filteredCompanyName);
            
            
            
        }
        
        //NSLog(@"%@",delegate1.instrumentToken);
        
        //NSLog(@"%@",self.detailsDict);
        
        //NSString * detailsString=[[self.detailsDict objectForKey:@"details" ]objectAtIndex:objectAtIndex];
        
        // NSArray * detailsArray=@[detailsString];
        
        //NSLog(@"%lu",objectAtIndex);

            //NSLog(@"%@",filteredArray);
        
        if(filteredArray.count>0)
        {
            
            detailsArray = [filteredArray objectAtIndex:0];
            //NSLog(@"array %@",detailsArray);
            
            [filteredArray removeAllObjects];
        }
        else
        {
            
            //NSLog(@"%@",self.detailsDict);
            
            detailsArray = [[self.detailsDict objectForKey:@"details" ]objectAtIndex:objectAtIndex];
            //    //NSLog(@"array %@",[detailsArray objectAtIndex:0]);
            
        }
            
            
            [delegate1.userMarketWatch addObject:detailsArray];
        
        NSString * string=[detailsArray valueForKey:@"instrument_token"];
        
        
        int myInt = [string intValue];
        
        //NSLog(@"token%i",myInt);
        
        
        [delegate1.instrumentToken addObject:[NSNumber numberWithInt:myInt]];
        
        //    [delegate1.instrumentToken addObject:[NSNumber numberWithInteger:myInt]];
        
        NSArray *allKeys = [detailsArray allKeys];
        //NSLog(@"%d",[allKeys containsObject:@"lotsize"]);
        
        if([allKeys containsObject:@"lotsize"])
        {
            NSString * lot=[detailsArray valueForKey:@"lotsize"];
            [delegate1.sampleDepthLot addObject:lot];
        }
        
        else
        {
            
            [delegate1.sampleDepthLot addObject:@"NULL"];
        }
        
        delegate1.exchange= [detailsArray valueForKey:@"exchange"];
        
        [delegate1.instrumentNameArr addObject:[detailsArray valueForKey:@"symbol"]];
            [delegate1.exchangeToken addObject:[detailsArray valueForKey:@"exchange_token"]];
            [delegate1.mtExchange addObject:[detailsArray valueForKey:@"exchangevalue"]];
        //NSLog(@"array %@",delegate1.instrumentNameArr);
        
        [delegate1.filteredCompanyName addObject:detailsArray];
        
        //NSLog(@"array %@",delegate1.filteredCompanyName);
            
            @try {
                delegate1.fincode=[detailsArray valueForKey:@"fincode"];
                
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            @try {
                
            } @catch (NSException *exception) {
                
            } @finally {
                delegate1.expiryDateArray=[detailsArray valueForKey:@"expiryseries"];
                delegate1.lotsizeArray=[detailsArray valueForKey:@"lotsize"];
                
            }
            
            @try {
                delegate1.optionArray=[detailsArray valueForKey:@"optiontype"];
                
                delegate1.strikeArray=[detailsArray valueForKey:@"strikeprice"];
                
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
        
        
        
        
        
        // saving an NSString
        
        
        
        [prefs setObject:delegate1.filteredCompanyName forKey:@"filteredCompanyName"];
        [prefs setObject:delegate1.instrumentNameArr forKey:@"instrumentNameArr"];
        [prefs setObject:delegate1.sampleDepthLot forKey:@"sampleDepthLot"];
        [prefs setObject:delegate1.instrumentToken forKey:@"instrumentToken"];
        [prefs setObject:delegate1.exchange forKey:@"exchange"];
            [prefs setObject:delegate1.exchangeToken forKey:@"exchange_token"];
             [prefs setObject:delegate1.mtExchange forKey:@"exchangevalue"];
            
            
           

                 
            
           
            
            @try {
            
             [prefs setObject:delegate1.fincode forKey:@"fincode"];
            
                
            } @catch (NSException *exception) {
                
            } @finally {
                
            }

        
        
            @try {
                
            } @catch (NSException *exception) {
                
            } @finally {
                 [prefs setObject:delegate1.expiryDateArray forKey:@"expiryseries"];
               [prefs setObject:delegate1.lotsizeArray forKey:@"lotsize"];
                
            }
            
            @try {
                [prefs setObject:delegate1.optionArray forKey:@"optiontype"];
                [prefs setObject:delegate1.strikeArray forKey:@"strikeprice"];
                
            } @catch (NSException *exception) {
                
            } @finally {
                
            }

        
        
        
        
        [prefs synchronize];
        
        
            delegate1.searchToWatch=true;
        
        
          
        [self.navigationController popViewControllerAnimated:YES];
    }
        else
        {
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Please select a symbol." preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:^{
                
            }];
            
            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            [alert addAction:okAction];

        }
    }
    
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
   
    
    
}

//- (void)textFieldDidBeginEditing:(UITextField *)textField
//{
//    
//    if(textField==self.symbolTxt)
//    {
//        
//        if([equityStr isEqualToString:@"EQ"]&&[exchangeString isEqualToString:@"BSE"])
//        {
//           }
//}


- (UITableViewCell *)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
        static NSString *cellIdentifier = @"WeekAutoSuggestionCell";
     @try {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
//    NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@", text];
//    NSArray *weeks = [self.symbols filteredArrayUsingPredicate:filterPredictate];
         NSArray *weeks = self.symbols;
         if(companyName.count>0)
         {
         
          
            NSString * finalName=[NSString stringWithFormat:@"%@(%@)",weeks[indexPath.row],[companyName objectAtIndex:indexPath.row]];
             cell.textLabel.text = finalName;
         }
         else
         {
             NSString * finalName=[NSString stringWithFormat:@"%@",weeks[indexPath.row]];
             cell.textLabel.text = finalName;
         }
      
    
    return cell;
     }
    @catch (NSException *exception) {
        //NSLog(@"%@", exception.reason);
    }
}

- (NSInteger)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section forText:(NSString *)text {
    
//    NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@", text];
    
    @try {
//        NSInteger count = [self.symbols filteredArrayUsingPredicate:filterPredictate].count;
         NSInteger count = self.symbols.count;
         return count;
    }
    @catch (NSException *exception) {
        //NSLog(@"%@", exception.reason);
    }

    return 0;
    
}

- (void)autoSuggestionField:(UITextField *)field textChanged:(NSString *)text {
    if (self.searchTimer != nil) {
        [self.searchTimer invalidate];
        self.searchTimer = nil;
    }
    
    // reschedule the search: in 1.0 second, call the searchForKeyword: method on the new textfield content
    self.searchTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                        target: self
                                                      selector: @selector(searchForKeyword:)
                                                      userInfo: self.symbolTxt.text
                                                       repeats: NO];
   
}

- (CGFloat)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
    return 50;
}

- (void)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
//
    @try {
    [self.view endEditing:YES];
    
    //NSLog(@"Selected suggestion at index row - %ld", (long)indexPath.row);
    
    self.futureBtn.enabled=YES;
    self.optionBtn.enabled=YES;
     self.expiryBtnOut.enabled=YES;
    
//           NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@", text];
//        NSArray *weeks = [self.symbols filteredArrayUsingPredicate:filterPredictate];
         NSArray *weeks = self.symbols;
        
        self.symbolTxt.text = weeks[indexPath.row];
    
//    NSIndexPath *selectedIndexPath = [tableView indexPathForSelectedRow];
    if([equityStr isEqualToString:@"EQ"])
    {
        objectAtIndex = indexPath.row;
    }
    
    else
    {
        objectAtIndex =0;
    }
   
    
    //NSLog(@"%li",(long)objectAtIndex);
    
    
    
//    self.expiryDateTxt.text=[expiry objectAtIndex:objectAtIndex];
//    self.strikePriceTxt.text=[strike objectAtIndex:objectAtIndex];
    
   if([equityStr isEqualToString:@"DR"]||[equityStr isEqualToString:@"CDS"]||[exchangeString isEqualToString:@"MCX"])
    {
        self.futureBtn.selected=YES;
        [self futureAction];
        
        if(self.currencyBtn.selected==YES)
        {
            exchangeString=@"CDS-FUT";
        }
        
        else if(self.commodityBtn.selected==YES)
        {
            exchangeString=@"MCX";
        }
        
        else if(self.derivativeBtn.selected==YES&&self.exchangeSegment.selectedSegmentIndex==0)
        {
            exchangeString=@"NFO-FUT";
        }
        
        else if(self.derivativeBtn.selected==YES&&self.exchangeSegment.selectedSegmentIndex==1)
        {
            exchangeString=@"BFO-FUT";
        }
        @try {
            NSDictionary *headers = @{  @"authtoken":delegate1.zenwiseToken,
                                       @"cache-control": @"no-cache",
                                           @"deviceid":delegate1.currentDeviceId
                                       };
            
            NSString * urlStr=[NSString stringWithFormat:@"%@stock/fao/%@?segment=%@&orderby=ASC",delegate1.baseUrl,self.symbolTxt.text,exchangeString];
            
            //NSLog(@"url %@",urlStr);
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:10.0];
            [request setHTTPMethod:@"GET"];
            [request setAllHTTPHeaderFields:headers];
            
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            if (error) {
                                                                //NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                //NSLog(@"%@", httpResponse);
                                                                
                                                                //                                                            [self.company removeAllObjects];
                                                                if([httpResponse statusCode]==403)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                    
                                                                }
                                                                else if([httpResponse statusCode]==401)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    
                                                                [expiry removeAllObjects];
                                                                [strike removeAllObjects];
                                                                ;
                                                                
                                                                self.responseArray1=[[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] mutableCopy];
                                                                
                                                                [delegate1.searchSymbolDict setObject:self.responseArray1 forKey:@"details"];
                                                                
                                                                //NSLog(@"%@",self.responseArray1);
                                                                
                                                                if(self.responseArray1.count>0)
                                                                {
                                                                    
                                                                    NSString * string=[NSString stringWithFormat:@"%@",self.responseArray];
                                                                    
                                                                    self.detailsDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSArray arrayWithArray:self.responseArray1],@"details", nil];
                                                                    
                                                                    for(int i=0;i<self.responseArray1.count;i++)
                                                                    {
                                                                        
                                                                        //                                                                [self.company addObject:[[self.responseArray1 objectAtIndex:i]objectForKey:@"symbol"]];
                                                                        
                                                                        NSString *myString = [[self.responseArray1 objectAtIndex:i]objectForKey:@"expiryseries"];
                                                                        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                                                                        dateFormatter.dateFormat = @"yyyy-MM-dd";
                                                                        NSDate *yourDate = [dateFormatter dateFromString:myString];
                                                                        dateFormatter.dateFormat = @"dd MMM yyyy";
                                                                        //NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
                                                                        
                                                                        NSString * finalDate=[[dateFormatter stringFromDate:yourDate] uppercaseString];
                                                                        
                                                                        [expiry addObject:finalDate];
                                                                        
                                                                        
                                                                        
                                                                    }
                                                                    
                                                                    [self.responseArray removeAllObjects];
                                                                    
                                                                    //NSLog(@"expiry%@",expiry);
                                                                }
                                                                }
                                                            }
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                //                                                          HomePage * home1=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
                                                                //
                                                                //                                                          [self.navigationController pushViewController:home1 animated:YES];
                                                                if(expiry.count>0)
                                                                {
                                                                    [self.expiryBtnOut setTitle:[expiry objectAtIndex:0] forState:UIControlStateNormal];
                                                                    
                                                                    
                                                                }
                                                            });
                                                        }];
            [dataTask resume];
            

            
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
        
        
        
    }
    
    
    }
    @catch (NSException *exception) {
        //NSLog(@"%@", exception.reason);
    }

    
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return true;
}

-(void)searchServer:(NSString *)string
{
    @try
    {
       // self.searchStr = [self.symbolTxt.text stringByReplacingCharactersInRange:range withString:string];
        self.searchStr = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                               NULL,
                                                                                               (CFStringRef) string,
                                                                                               NULL,
                                                                                               (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                               kCFStringEncodingUTF8 ));
        [self.expiryBtnOut setTitle:@"" forState:UIControlStateNormal];
        
        //NSLog(@"%@",self.searchStr);
        
        
      
        CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef) self.searchStr, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
        
         self.searchStr=[NSString stringWithFormat:@"%@",newString];
        if(self.searchStr.length>=1)
        {
            
            //NSLog(@"%@",delegate1.accessToken);
            
            
            if([equityStr isEqualToString:@"EQ"])
            {
                @try {
                    NSDictionary *headers = @{
                                              @"cache-control": @"no-cache",
                                              @"authtoken":delegate1.zenwiseToken,
                                              @"deviceid":delegate1.currentDeviceId
                                              };
                    
                    NSString * urlStr=[NSString stringWithFormat:@"%@stock/searchsymbol/?searchsymbol=%@&exchange=%@&limit=20000",delegate1.baseUrl,self.searchStr,exchangeString1];
                    
                    
                    
                    //NSLog(@"url %@",urlStr);
                    
                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                       timeoutInterval:10.0];
                    [request setHTTPMethod:@"GET"];
                    [request setAllHTTPHeaderFields:headers];
                    
                    NSURLSession *session = [NSURLSession sharedSession];
                    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                    if (error) {
                                                                        //NSLog(@"%@", error);
                                                                    } else {
                                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                        //NSLog(@"%@", httpResponse);
                                                                        if([httpResponse statusCode]==403)
                                                                        {
                                                                            
                                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                            
                                                                        }
                                                                        else if([httpResponse statusCode]==401)
                                                                        {
                                                                            
                                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                            
                                                                        }
                                                                        else
                                                                        {
                                                                            if(self.company.count>0)
                                                                            {
                                                                            [self.company removeAllObjects];
                                                                            }
                                                                            
                                                                            companyName=[[NSMutableArray alloc]init];
                                                                            
                                                                            self.responseArray=[[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] mutableCopy];
                                                                            
                                                                            [delegate1.searchSymbolDict setObject:self.responseArray forKey:@"details"];
                                                                            
                                                                            //NSLog(@"%@",self.responseArray);
                                                                            
                                                                            //                                                            NSString * string=[NSString stringWithFormat:@"%@",self.responseArray];
                                                                            
                                                                            self.detailsDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSArray arrayWithArray:self.responseArray],@"details", nil];
                                                                            //NSLog(@"%@",self.detailsDict);
                                                                            if(self.responseArray.count>0)
                                                                            {
                                                                                
                                                                                for(int i=0;i<self.responseArray.count;i++)
                                                                                {
                                                                                    
                                                                                    [self.company addObject:[[self.responseArray objectAtIndex:i]objectForKey:@"symbol"]];
                                                                                    [companyName addObject:[[self.responseArray objectAtIndex:i]objectForKey:@"name"]];
                                                                                    
                                                                                    
                                                                                    
                                                                                }
                                                                                
                                                                                [self.responseArray removeAllObjects];
                                                                            }
                                                                        }
                                                                    }
                                                                    
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        
                                                                        //                                                          HomePage * home1=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
                                                                        //
                                                                        //                                                          [self.navigationController pushViewController:home1 animated:YES];
                                                                        
                                                                        
                                                                        
                                                                    });
                                                                }];
                    [dataTask resume];
                    
                    
                    
                }
                @catch (NSException * e) {
                    //NSLog(@"Exception: %@", e);
                }
                @finally {
                    //NSLog(@"finally");
                }
            }
            
            
            
            else if([equityStr isEqualToString:@"CDS"])
                
            {
                @try {
                    NSDictionary *headers = @{
                                              @"cache-control": @"no-cache",
                                              @"authtoken":delegate1.zenwiseToken,
                                              @"deviceid":delegate1.currentDeviceId
                                              };
                    
                    NSString * urlStr=[NSString stringWithFormat:@"%@stock/fao?exchange=CDS&symbol=%@&limit=20000",delegate1.baseUrl,self.searchStr];
                    
                    //NSLog(@"url %@",urlStr);
                    
                    
                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                       timeoutInterval:10.0];
                    [request setHTTPMethod:@"GET"];
                    [request setAllHTTPHeaderFields:headers];
                    
                    NSURLSession *session = [NSURLSession sharedSession];
                    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                    if (error) {
                                                                        //NSLog(@"%@", error);
                                                                    } else {
                                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                        //NSLog(@"%@", httpResponse);
                                                                        if([httpResponse statusCode]==403)
                                                                        {
                                                                            
                                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                            
                                                                        }
                                                                        else if([httpResponse statusCode]==401)
                                                                        {
                                                                            
                                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                            
                                                                        }
                                                                        else
                                                                        {
                                                                            
                                                                            [self.company removeAllObjects];
                                                                            
                                                                            
                                                                            companyName=[[NSMutableArray alloc]init];
                                                                            
                                                                            self.responseArray=[[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] mutableCopy];
                                                                            
                                                                            [delegate1.searchSymbolDict setObject:self.responseArray forKey:@"details"];
                                                                            
                                                                            //NSLog(@"%@",self.responseArray);
                                                                            
                                                                            //                                                            NSString * string=[NSString stringWithFormat:@"%@",self.responseArray];
                                                                            
                                                                            self.detailsDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSArray arrayWithArray:self.responseArray],@"details", nil];
                                                                            
                                                                            //NSLog(@"%@",self.detailsDict);
                                                                            if(self.responseArray.count>0)
                                                                            {
                                                                                
                                                                                for(int i=0;i<self.responseArray.count;i++)
                                                                                {
                                                                                    
                                                                                    [self.company addObject:[[self.responseArray objectAtIndex:i]objectForKey:@"symbol"]];
//                                                                                     [companyName addObject:[[self.responseArray objectAtIndex:i]objectForKey:@"name"]];
                                                                                    
                                                                                    
                                                                                    
                                                                                }
                                                                                
                                                                                [self.responseArray removeAllObjects];
                                                                            }
                                                                        }
                                                                    }
                                                                    
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        
                                                                        //                                                          HomePage * home1=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
                                                                        //
                                                                        //                                                          [self.navigationController pushViewController:home1 animated:YES];
                                                                        
                                                                        
                                                                        
                                                                    });
                                                                }];
                    [dataTask resume];
                }
                @catch (NSException * e) {
                    //NSLog(@"Exception: %@", e);
                }
                @finally {
                    //NSLog(@"finally");
                }
                
                
                
                
                
                
            }
            
            
            
            
            
            
            
            
            
            else if ([equityStr isEqualToString:@"DR"])
            {
                @try {
                    NSDictionary *headers = @{
                                              @"cache-control": @"no-cache",
                                              @"authtoken":delegate1.zenwiseToken,
                                              @"deviceid":delegate1.currentDeviceId
                                              };
                    
                    NSString * urlStr=[NSString stringWithFormat:@"%@stock/searchsymbolfao/?exchange=%@&searchsymbol=%@&segment=%@&limit=20000",delegate1.baseUrl,exchangeString1,self.searchStr,exchangeString];
                    
                    
                    
                    //NSLog(@"url %@",urlStr);
                    
                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                       timeoutInterval:10.0];
                    [request setHTTPMethod:@"GET"];
                    [request setAllHTTPHeaderFields:headers];
                    
                    NSURLSession *session = [NSURLSession sharedSession];
                    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                    if (error) {
                                                                        //NSLog(@"%@", error);
                                                                    } else {
                                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                        //NSLog(@"%@", httpResponse);
                                                                        if([httpResponse statusCode]==403)
                                                                        {
                                                                            
                                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                            
                                                                        }
                                                                        else if([httpResponse statusCode]==401)
                                                                        {
                                                                            
                                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                            
                                                                        }
                                                                        else if([httpResponse statusCode]==400)
                                                                        {
                                                                            
                                                                         
                                                                            
                                                                        }
                                                                        else
                                                                        {
                                                                            
                                                                            [self.company removeAllObjects];
                                                                            
                                                                            
                                                                            companyName=[[NSMutableArray alloc]init];
                                                                            
                                                                            self.responseArray=[[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] mutableCopy];
                                                                            
                                                                            [delegate1.searchSymbolDict setObject:self.responseArray forKey:@"details"];
                                                                            
                                                                            //NSLog(@"%@",self.responseArray);
                                                                            
                                                                            NSString * string=[NSString stringWithFormat:@"%@",self.responseArray];
                                                                            
                                                                            self.detailsDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSArray arrayWithArray:self.responseArray],@"details", nil];
                                                                            
                                                                            for(int i=0;i<self.responseArray.count;i++)
                                                                            {
                                                                                
                                                                                [self.company addObject:[[self.responseArray objectAtIndex:i]objectForKey:@"symbol"]];
                                                                                
                                                                                 [companyName addObject:[[self.responseArray objectAtIndex:i]objectForKey:@"name"]];
                                                                                
                                                                            }
                                                                            
                                                                            [self.responseArray removeAllObjects];
                                                                        }
                                                                    }
                                                                    
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        
                                                                        //                                                          HomePage * home1=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
                                                                        //
                                                                        //                                                          [self.navigationController pushViewController:home1 animated:YES];
                                                                        
                                                                        
                                                                        
                                                                    });
                                                                }];
                    [dataTask resume];
                    
                    
                }
                @catch (NSException * e) {
                    //NSLog(@"Exception: %@", e);
                }
                @finally {
                    //NSLog(@"finally");
                }
                
            }
            
            else if([exchangeString isEqualToString:@"MCX"])
            {
                @try {
                    NSDictionary *headers = @{
                                              @"cache-control": @"no-cache",
                                              @"authtoken":delegate1.zenwiseToken,
                                              @"deviceid":delegate1.currentDeviceId
                                              };
                    
                    NSString * urlStr=[NSString stringWithFormat:@"%@stock/fao?exchange=MCX&symbol=%@&limit=20000",delegate1.baseUrl,self.searchStr];
                    
                    
                    //NSLog(@"url %@",urlStr);
                    
                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                       timeoutInterval:10.0];
                    [request setHTTPMethod:@"GET"];
                    [request setAllHTTPHeaderFields:headers];
                    
                    NSURLSession *session = [NSURLSession sharedSession];
                    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                    if (error) {
                                                                        //NSLog(@"%@", error);
                                                                    } else {
                                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                        //NSLog(@"%@", httpResponse);
                                                                        if([httpResponse statusCode]==403)
                                                                        {
                                                                            
                                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                            
                                                                        }
                                                                        else if([httpResponse statusCode]==401)
                                                                        {
                                                                            
                                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                            
                                                                        }
                                                                        else
                                                                        {
                                                                            
                                                                            [self.company removeAllObjects];
                                                                            
                                                                            
                                                                            companyName=[[NSMutableArray alloc]init];
                                                                            
                                                                            self.responseArray=[[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] mutableCopy];
                                                                            
                                                                            [delegate1.searchSymbolDict setObject:self.responseArray forKey:@"details"];
                                                                            
                                                                            //NSLog(@"%@",self.responseArray);
                                                                            
                                                                            NSString * string=[NSString stringWithFormat:@"%@",self.responseArray];
                                                                            
                                                                            self.detailsDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSArray arrayWithArray:self.responseArray],@"details", nil];
                                                                            
                                                                            for(int i=0;i<self.responseArray.count;i++)
                                                                            {
                                                                                
                                                                                [self.company addObject:[[self.responseArray objectAtIndex:i]objectForKey:@"symbol"]];
                                                                                
                                                                                
//                                                                                 [companyName addObject:[[self.responseArray objectAtIndex:i]objectForKey:@"name"]];
                                                                            }
                                                                            
                                                                            [self.responseArray removeAllObjects];
                                                                        }
                                                                    }
                                                                    
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        
                                                                        //                                                          HomePage * home1=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
                                                                        //
                                                                        //                                                          [self.navigationController pushViewController:home1 animated:YES];
                                                                        
                                                                        
                                                                        
                                                                    });
                                                                }];
                    [dataTask resume];
                    
                    
                }
                @catch (NSException * e) {
                    //NSLog(@"Exception: %@", e);
                }
                @finally {
                    //NSLog(@"finally");
                }
                
            }
            
        }
        
    }
    @catch (NSException *exception) {
        //NSLog(@"%@", exception.reason);
    }
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
//    @try
//    {
//    self.searchStr = [self.symbolTxt.text stringByReplacingCharactersInRange:range withString:string];
//        self.searchStr = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
//                                                                                               NULL,
//                                                                                               (CFStringRef) self.searchStr,
//                                                                                               NULL,
//                                                                                               (CFStringRef)@"!*'();:@&=+$,/?%#[]",
//                                                                                               kCFStringEncodingUTF8 ));
//     [self.expiryBtnOut setTitle:@"" forState:UIControlStateNormal];
//
//    //NSLog(@"%@",self.searchStr);
//
//
//    if([self.searchStr containsString:@"&"])
//    {
//        self.searchStr=[self.searchStr stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
//
//    }
//
//    if(self.searchStr.length>=1)
//    {
//
//        //NSLog(@"%@",delegate1.accessToken);
//
//
//            if([equityStr isEqualToString:@"EQ"])
//            {
//                @try {
//                    NSDictionary *headers = @{
//                                               @"cache-control": @"no-cache",
//                                                @"authtoken":delegate1.zenwiseToken,
//                                                  @"deviceid":delegate1.currentDeviceId
//                                               };
//
//                    NSString * urlStr=[NSString stringWithFormat:@"%@stock/searchsymbol/?searchsymbol=%@&exchange=%@&limit=20000",delegate1.baseUrl,self.searchStr,exchangeString1];
//
//
//
//                    //NSLog(@"url %@",urlStr);
//
//                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
//                                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                                       timeoutInterval:10.0];
//                    [request setHTTPMethod:@"GET"];
//                    [request setAllHTTPHeaderFields:headers];
//
//                    NSURLSession *session = [NSURLSession sharedSession];
//                    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
//                                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                                                                    if (error) {
//                                                                        //NSLog(@"%@", error);
//                                                                    } else {
//                                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
//                                                                        //NSLog(@"%@", httpResponse);
//                                                                        if([httpResponse statusCode]==403)
//                                                                        {
//
//                                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
//
//                                                                        }
//                                                                        else if([httpResponse statusCode]==401)
//                                                                        {
//
//                                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
//
//                                                                        }
//                                                                        else
//                                                                        {
//
//                                                                        [self.company removeAllObjects];
//
//
//                                                                        ;
//
//                                                                        self.responseArray=[[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] mutableCopy];
//
//                                                                        [delegate1.searchSymbolDict setObject:self.responseArray forKey:@"details"];
//
//                                                                        //NSLog(@"%@",self.responseArray);
//
//                                                                        //                                                            NSString * string=[NSString stringWithFormat:@"%@",self.responseArray];
//
//                                                                        self.detailsDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSArray arrayWithArray:self.responseArray],@"details", nil];
//                                                                        //NSLog(@"%@",self.detailsDict);
//                                                                        if(self.responseArray.count>0)
//                                                                        {
//
//                                                                            for(int i=0;i<self.responseArray.count;i++)
//                                                                            {
//
//                                                                                [self.company addObject:[[self.responseArray objectAtIndex:i]objectForKey:@"symbol"]];
//
//
//
//                                                                            }
//
//                                                                            [self.responseArray removeAllObjects];
//                                                                        }
//                                                                        }
//                                                                    }
//
//                                                                    dispatch_async(dispatch_get_main_queue(), ^{
//
//                                                                        //                                                          HomePage * home1=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
//                                                                        //
//                                                                        //                                                          [self.navigationController pushViewController:home1 animated:YES];
//
//
//
//                                                                    });
//                                                                }];
//                    [dataTask resume];
//
//
//
//                }
//                @catch (NSException * e) {
//                    //NSLog(@"Exception: %@", e);
//                }
//                @finally {
//                    //NSLog(@"finally");
//                }
//                            }
//
//
//
//            else if([equityStr isEqualToString:@"CDS"])
//
//            {
//                @try {
//                    NSDictionary *headers = @{
//                                               @"cache-control": @"no-cache",
//                                                @"authtoken":delegate1.zenwiseToken,
//                                                  @"deviceid":delegate1.currentDeviceId
//                                               };
//
//                    NSString * urlStr=[NSString stringWithFormat:@"%@stock/fao?exchange=CDS&symbol=%@&limit=20000",delegate1.baseUrl,self.searchStr];
//
//                    //NSLog(@"url %@",urlStr);
//
//
//                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
//                                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                                       timeoutInterval:10.0];
//                    [request setHTTPMethod:@"GET"];
//                    [request setAllHTTPHeaderFields:headers];
//
//                    NSURLSession *session = [NSURLSession sharedSession];
//                    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
//                                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                                                                    if (error) {
//                                                                        //NSLog(@"%@", error);
//                                                                    } else {
//                                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
//                                                                        //NSLog(@"%@", httpResponse);
//                                                                        if([httpResponse statusCode]==403)
//                                                                        {
//
//                                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
//
//                                                                        }
//                                                                        else if([httpResponse statusCode]==401)
//                                                                        {
//
//                                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
//
//                                                                        }
//                                                                        else
//                                                                        {
//
//                                                                        [self.company removeAllObjects];
//
//
//                                                                        ;
//
//                                                                        self.responseArray=[[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] mutableCopy];
//
//                                                                        [delegate1.searchSymbolDict setObject:self.responseArray forKey:@"details"];
//
//                                                                        //NSLog(@"%@",self.responseArray);
//
//                                                                        //                                                            NSString * string=[NSString stringWithFormat:@"%@",self.responseArray];
//
//                                                                        self.detailsDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSArray arrayWithArray:self.responseArray],@"details", nil];
//
//                                                                        //NSLog(@"%@",self.detailsDict);
//                                                                        if(self.responseArray.count>0)
//                                                                        {
//
//                                                                            for(int i=0;i<self.responseArray.count;i++)
//                                                                            {
//
//                                                                                [self.company addObject:[[self.responseArray objectAtIndex:i]objectForKey:@"symbol"]];
//
//
//
//                                                                            }
//
//                                                                            [self.responseArray removeAllObjects];
//                                                                        }
//                                                                        }
//                                                                    }
//
//                                                                    dispatch_async(dispatch_get_main_queue(), ^{
//
//                                                                        //                                                          HomePage * home1=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
//                                                                        //
//                                                                        //                                                          [self.navigationController pushViewController:home1 animated:YES];
//
//
//
//                                                                    });
//                                                                }];
//                    [dataTask resume];
//                }
//                @catch (NSException * e) {
//                    //NSLog(@"Exception: %@", e);
//                }
//                @finally {
//                    //NSLog(@"finally");
//                }
//
//
//
//
//
//
//        }
//
//
//
//
//
//
//
//
//
//        else if ([equityStr isEqualToString:@"DR"])
//        {
//            @try {
//                NSDictionary *headers = @{
//                                           @"cache-control": @"no-cache",
//                                            @"authtoken":delegate1.zenwiseToken,
//                                              @"deviceid":delegate1.currentDeviceId
//                                           };
//
//                NSString * urlStr=[NSString stringWithFormat:@"%@stock/searchsymbolfao/?exchange=%@&searchsymbol=%@&segment=%@&limit=20000",delegate1.baseUrl,exchangeString1,self.searchStr,exchangeString];
//
//
//
//                //NSLog(@"url %@",urlStr);
//
//                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
//                                                                       cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                                   timeoutInterval:10.0];
//                [request setHTTPMethod:@"GET"];
//                [request setAllHTTPHeaderFields:headers];
//
//                NSURLSession *session = [NSURLSession sharedSession];
//                NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
//                                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                                                                if (error) {
//                                                                    //NSLog(@"%@", error);
//                                                                } else {
//                                                                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
//                                                                    //NSLog(@"%@", httpResponse);
//                                                                    if([httpResponse statusCode]==403)
//                                                                    {
//
//                                                                        [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
//
//                                                                    }
//                                                                    else if([httpResponse statusCode]==401)
//                                                                    {
//
//                                                                        [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
//
//                                                                    }
//                                                                    else
//                                                                    {
//
//                                                                    [self.company removeAllObjects];
//
//
//                                                                    ;
//
//                                                                    self.responseArray=[[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] mutableCopy];
//
//                                                                    [delegate1.searchSymbolDict setObject:self.responseArray forKey:@"details"];
//
//                                                                    //NSLog(@"%@",self.responseArray);
//
//                                                                    NSString * string=[NSString stringWithFormat:@"%@",self.responseArray];
//
//                                                                    self.detailsDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSArray arrayWithArray:self.responseArray],@"details", nil];
//
//                                                                    for(int i=0;i<self.responseArray.count;i++)
//                                                                    {
//
//                                                                        [self.company addObject:[[self.responseArray objectAtIndex:i]objectForKey:@"symbol"]];
//
//
//
//                                                                    }
//
//                                                                    [self.responseArray removeAllObjects];
//                                                                    }
//                                                                }
//
//                                                                dispatch_async(dispatch_get_main_queue(), ^{
//
//                                                                    //                                                          HomePage * home1=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
//                                                                    //
//                                                                    //                                                          [self.navigationController pushViewController:home1 animated:YES];
//
//
//
//                                                                });
//                                                            }];
//                [dataTask resume];
//
//
//            }
//            @catch (NSException * e) {
//                //NSLog(@"Exception: %@", e);
//            }
//            @finally {
//                //NSLog(@"finally");
//            }
//
//        }
//
//        else if([exchangeString isEqualToString:@"MCX"])
//        {
//            @try {
//                NSDictionary *headers = @{
//                                          @"cache-control": @"no-cache",
//                                           @"authtoken":delegate1.zenwiseToken,
//                                             @"deviceid":delegate1.currentDeviceId
//                                          };
//
//                  NSString * urlStr=[NSString stringWithFormat:@"%@stock/fao?exchange=MCX&symbol=%@&limit=20000",delegate1.baseUrl,self.searchStr];
//
//
//                //NSLog(@"url %@",urlStr);
//
//                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
//                                                                       cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                                   timeoutInterval:10.0];
//                [request setHTTPMethod:@"GET"];
//                [request setAllHTTPHeaderFields:headers];
//
//                NSURLSession *session = [NSURLSession sharedSession];
//                NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
//                                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                                                                if (error) {
//                                                                    //NSLog(@"%@", error);
//                                                                } else {
//                                                                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
//                                                                    //NSLog(@"%@", httpResponse);
//                                                                    if([httpResponse statusCode]==403)
//                                                                    {
//
//                                                                        [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
//
//                                                                    }
//                                                                    else if([httpResponse statusCode]==401)
//                                                                    {
//
//                                                                        [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
//
//                                                                    }
//                                                                    else
//                                                                    {
//
//                                                                    [self.company removeAllObjects];
//
//
//                                                                    ;
//
//                                                                    self.responseArray=[[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] mutableCopy];
//
//                                                                    [delegate1.searchSymbolDict setObject:self.responseArray forKey:@"details"];
//
//                                                                    //NSLog(@"%@",self.responseArray);
//
//                                                                    NSString * string=[NSString stringWithFormat:@"%@",self.responseArray];
//
//                                                                    self.detailsDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSArray arrayWithArray:self.responseArray],@"details", nil];
//
//                                                                    for(int i=0;i<self.responseArray.count;i++)
//                                                                    {
//
//                                                                        [self.company addObject:[[self.responseArray objectAtIndex:i]objectForKey:@"symbol"]];
//
//
//
//                                                                    }
//
//                                                                    [self.responseArray removeAllObjects];
//                                                                    }
//                                                                }
//
//                                                                dispatch_async(dispatch_get_main_queue(), ^{
//
//                                                                    //                                                          HomePage * home1=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
//                                                                    //
//                                                                    //                                                          [self.navigationController pushViewController:home1 animated:YES];
//
//
//
//                                                                });
//                                                            }];
//                [dataTask resume];
//
//
//            }
//            @catch (NSException * e) {
//                //NSLog(@"Exception: %@", e);
//            }
//            @finally {
//                //NSLog(@"finally");
//            }
//
//        }
//
//    }
//
//    return YES;
//
//    }
//    @catch (NSException *exception) {
//        //NSLog(@"%@", exception.reason);
//    }
    
    return YES;
}

//picker view//

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if([check isEqualToString:@"expiry"])
    {
        if(expiry.count>0)
        {
        return expiry.count;
        }
    }
    
    else if([check isEqualToString:@"strike"])
    {
        if(strike.count>0)
        {
        return strike.count;
        }
    }
    
 return 0;
    
}
- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    @try
    {
    //NSLog(@"%@",expiry);
    
    if([check isEqualToString:@"expiry"])
    {
        if(expiry.count>0)
        {
        return expiry[row];
        }
    }
    
    else if([check isEqualToString:@"strike"])
    {
        if(strike.count>0)
        {
        return strike[row];
        }
    }
    return @"0";
    
}
@catch (NSException * e) {
    //NSLog(@"Exception: %@", e);
}
@finally {
    //NSLog(@"finally");
}
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    @try
    {
    if([check isEqualToString:@"expiry"])
    {
    NSInteger selectRow = [self.pickerView selectedRowInComponent:0];
    
    objectAtIndex = selectRow;
        optionCheck=selectRow;
    }
    else if ([check isEqualToString:@"strike"])
    {
        
        NSInteger selectRow = [self.pickerView selectedRowInComponent:0];
        
        objectAtIndex = selectRow;

    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

}



- (IBAction)expiryBtn:(id)sender {
    
    self.pickerView.hidden=NO;
    self.sampleView.hidden=NO;
    
    check=@"expiry";
    
    self.pickerView.delegate=self;
    self.pickerView.dataSource=self;

}

- (IBAction)strikeBtn:(id)sender {
    
    self.pickerView.hidden=NO;
    self.sampleView.hidden=NO;
    
    check=@"strike";
    
    self.pickerView.delegate=self;
    self.pickerView.dataSource=self;
    
}
- (IBAction)doneBtnAction:(id)sender {
    
    @try {
        if([check isEqualToString:@"expiry"])
        {
            [self.expiryBtnOut setTitle:[expiry objectAtIndex:objectAtIndex] forState:UIControlStateNormal];
            self.pickerView.hidden=YES;
            self.sampleView.hidden=YES;
            //        [self.backgroundView setBackgroundColor:[UIColor whiteColor]];
            //        self.backgroundView.alpha=1;
            
            
            //NSLog(@"%@",_responseArray1);
            //NSLog(@"%lu",_responseArray1.count);
            
            titleStr=[expiry objectAtIndex:objectAtIndex];
            //NSLog(@"%@",titleStr);
            
            if(strike.count>0)
            {
                [strike removeAllObjects];
            }
            
            if(filteredArray.count>0)
            {
                [filteredArray removeAllObjects];
                
            }
            
            
            for(int i=0;i<_responseArray1.count;i++)
            {
                
                NSString * str=[[_responseArray1 objectAtIndex:i] objectForKey:@"expiryseries"];
                //NSLog(@"sdc %@",str);
                //            NSString *myString = [number stringValue];
                //             //NSLog(@"sdc %@",myString);
                
                NSString *myString = [[self.responseArray1 objectAtIndex:i]objectForKey:@"expiryseries"];
                NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                dateFormatter.dateFormat = @"yyyy-MM-dd";
                NSDate *yourDate = [dateFormatter dateFromString:myString];
                dateFormatter.dateFormat = @"dd MMM yyyy";
                //NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
                
                NSString * finalDate=[[dateFormatter stringFromDate:yourDate] uppercaseString];
                
                
                
                
                if([titleStr isEqualToString:finalDate])
                {
                    [strike addObject:[[self.responseArray1 objectAtIndex:i]objectForKey:@"strikeprice"]];
                    //NSLog(@"sdc%@",strike);
                    
                    //NSLog(@"%@",[self.responseArray1 objectAtIndex:i]);
                    
                    
                    [filteredArray addObject:[self.responseArray1 objectAtIndex:i]];
                    //NSLog(@"%@",filteredArray);
                    
                }
            }
            
            //NSLog(@"%@",strike);
            //NSLog(@"%@",filteredArray);
            
            
            
        }
        else if ([check isEqualToString:@"strike"])
        {
            [self.strikeBtnOut setTitle:[strike objectAtIndex:objectAtIndex] forState:UIControlStateNormal];
            
            
            
            
            titleStr=[expiry objectAtIndex:optionCheck];
            NSString  * strikePrice=[strike objectAtIndex:objectAtIndex];
            //NSLog(@"%@",titleStr);
            
            
            
            if(filteredArray.count>0)
            {
                [filteredArray removeAllObjects];
                
            }
            
            
            for(int i=0;i<_responseArray1.count;i++)
            {
                
                NSString * str=[[_responseArray1 objectAtIndex:i] objectForKey:@"expiryseries"];
                //NSLog(@"sdc %@",str);
                //            NSString *myString = [number stringValue];
                //             //NSLog(@"sdc %@",myString);
                
                NSString *myString = [[self.responseArray1 objectAtIndex:i]objectForKey:@"expiryseries"];
                NSString *strikeString = [[self.responseArray1 objectAtIndex:i]objectForKey:@"strikeprice"];
                NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                dateFormatter.dateFormat = @"yyyy-MM-dd";
                NSDate *yourDate = [dateFormatter dateFromString:myString];
                dateFormatter.dateFormat = @"dd MMM yyyy";
                //NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
                
                NSString * finalDate=[[dateFormatter stringFromDate:yourDate] uppercaseString];
                
                
                
                
                if([titleStr isEqualToString:finalDate]&&[strikePrice isEqualToString:strikeString])
                {
                    
                    
                    [filteredArray addObject:[self.responseArray1 objectAtIndex:i]];
                    //NSLog(@"%@",filteredArray);
                    
                }
            }
            
            self.pickerView.hidden=YES;
            self.sampleView.hidden=YES;
        }
        

        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    }

-(void)keyboardDidHide:(NSNotification *)notification
{
//    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
}
@end
