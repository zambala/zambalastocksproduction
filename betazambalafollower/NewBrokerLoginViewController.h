//
//  NewBrokerLoginViewController.h
//  NewUI
//
//  Created by Zenwise Technologies on 23/05/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewBrokerLoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *brokerImageView;
@property (weak, nonatomic) IBOutlet UILabel *brokerNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *clientIDTF;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
@property (weak, nonatomic) IBOutlet UIButton *resetPasswordTF;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UITextField *passwordNew;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *passwordNewHgt;
- (IBAction)resetAction:(id)sender;
- (IBAction)loginAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityInd;
- (IBAction)onForgotPasswordTap:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordBtn;

@end
