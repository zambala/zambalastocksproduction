//
//  PaymentSelectionView.m
//  PremiumServices
//
//  Created by zenwise mac 2 on 7/20/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "PaymentSelectionView.h"
#import "ViewController.h"
#import "AppDelegate.h"
#import <Mixpanel/Mixpanel.h>
//@import Tune;

@interface PaymentSelectionView ()
{
    AppDelegate * delegate1;
    NSArray * allKeys;
    NSMutableArray * eventItem;
}

@end

@implementation PaymentSelectionView

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"payment_page"];
    NSString * string=[NSString stringWithFormat:@"₹%@",delegate1.amountForPayment];
    
    self.grandTotal.text=string;
    self.amountPayable.text=string;
    
    self.navigationItem.title = @"Make Payment";
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    

    
    [self.cardBtn addTarget:self action:@selector(cardAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.redeemSelectionBtn addTarget:self action:@selector(redeemAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [self.redeemSelectionBtn setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    
    [self.redeemSelectionBtn setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    
    self.redeemSelectionBtn.selected=NO;
    
    
    
    self.redeemView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.redeemView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.redeemView.layer.shadowOpacity = 1.0f;
    self.redeemView.layer.shadowRadius = 1.0f;
    self.redeemView.layer.cornerRadius=2.1f;
    self.redeemView.layer.masksToBounds = NO;
    self.redeemView.hidden=YES;
    
    self.amountView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.amountView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.amountView.layer.shadowOpacity = 1.0f;
    self.amountView.layer.shadowRadius = 1.0f;
    self.amountView.layer.cornerRadius=6.2f;
    self.amountView.layer.masksToBounds = NO;


    
    
    
    [self.cardBtn setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    
    [self.cardBtn setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    
    [self cardAction];
    
    [self.netBankingBtn addTarget:self action:@selector(netBankingAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.netBankingBtn setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    
    [self.netBankingBtn setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    
    [self.upiBtn addTarget:self action:@selector(upiAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.upiBtn setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    
    [self.upiBtn setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    
    [self.makePaymentBtn addTarget:self action:@selector(paymentAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    // Do any additional setup after loading the view.
}

-(void)redeemAction
{
    if(self.redeemSelectionBtn.selected==NO)
    {
        self.redeemSelectionBtn.selected=YES;
    }
    
    else if(self.redeemSelectionBtn.selected==YES)
    {
        self.redeemSelectionBtn.selected=NO;
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)cardAction
{
    self.cardBtn.selected=YES;
    self.netBankingBtn.selected=NO;
    self.upiBtn.selected=NO;
    delegate1.paymentString=@"CARD";
     self.selectionViewHgt.constant=145;
    self.enterVPALbl.hidden=YES;
    self.upiInputTxt.hidden=YES;
    self.brokerUpi.hidden=YES;
    self.targetVPALbl.hidden=YES;
    
    
}

-(void)netBankingAction
{
    self.cardBtn.selected=NO;
    self.netBankingBtn.selected=YES;
    self.upiBtn.selected=NO;
    delegate1.paymentString=@"NET";
     self.selectionViewHgt.constant=145;
    self.enterVPALbl.hidden=YES;
    self.upiInputTxt.hidden=YES;
    self.brokerUpi.hidden=YES;
    self.targetVPALbl.hidden=YES;
}


-(void)upiAction
{
    self.cardBtn.selected=NO;
    self.netBankingBtn.selected=NO;
    self.upiBtn.selected=YES;
    delegate1.paymentString=@"UPI";
    self.selectionViewHgt.constant=270;
    self.enterVPALbl.hidden=NO;
    self.upiInputTxt.hidden=NO;
    self.brokerUpi.hidden=NO;
    self.targetVPALbl.hidden=NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)paymentAction
{
   
        @try
        {
            eventItem=[[NSMutableArray alloc]init];
            allKeys=[[NSArray alloc]init];
            
            allKeys=[delegate1.cartDict allKeys];
            
            
            
            if(delegate1.paymentString.length>0)
            {
                if([delegate1.paymentString isEqualToString:@"UPI"])
                {
                    if(self.upiInputTxt.text.length>0)
                    {
                    
                    delegate1.UPIDetails=self.upiInputTxt.text;
                    NSString *newStr = [self.amountPayable.text substringFromIndex:1];
                    delegate1.amountAfterRedeem=newStr;
                    ViewController * view=[self.storyboard instantiateViewControllerWithIdentifier:@"view1"];
                    
                    [self.navigationController pushViewController:view animated:YES];
                    
                    
                    //            else
                    //            {
                    //
                    //                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Please fill the empty fields" preferredStyle:UIAlertControllerStyleAlert];
                    //                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                    //                    // Ok action example
                    //                }];
                    //                [alert addAction:okAction];
                    //
                    //                [self presentViewController:alert animated:YES completion:nil];
                    //
                    //            }
                    }else
                    {
                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Please enter VPA to continue." preferredStyle:UIAlertControllerStyleAlert];
                            
                            [self presentViewController:alert animated:YES completion:^{
                                
                            }];
                            
                            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                
                            }];
                            
                            [alert addAction:okAction];
                    }
                }
                
                else
                {
                    NSString *newStr = [self.amountPayable.text substringFromIndex:1];
                    delegate1.amountAfterRedeem=newStr;
                    ViewController * view=[self.storyboard instantiateViewControllerWithIdentifier:@"view1"];
                    
                    [self.navigationController pushViewController:view animated:YES];
                    
                }
                
            }
            
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }

    
}

@end
