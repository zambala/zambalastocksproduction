//
//  NewMessages.m
//  zambala leader
//
//  Created by zenwise mac 2 on 3/15/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "NewMessages.h"
#import "HMSegmentedControl.h"
#import "NewNotificationTableViewCell.h"
#import "TabBar.h"
#import "NoMessagesCell.h"
#import "HelpshiftCampaigns.h"
#import "HelpshiftInbox.h"
#import "TagEncode.h"
#import "AppDelegate.h"

@interface NewMessages ()
{
    HMSegmentedControl *segmentedControl;
    NSString * number;
    AppDelegate * delegate;
    NSArray * messagesList;
    
}


@end

@implementation NewMessages

#define DefaultColor   [UIColor clearColor]
#define SelectedColor  [UIColor colorWithRed:(252/255.0) green:(98/255.0) blue:(98/255.0) alpha:1]
#define BorderltColor  [UIColor grayColor]

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.messagesTbl.delegate=self;
    self.messagesTbl.dataSource=self;
    NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
    
    number= [loggedInUser stringForKey:@"profilemobilenumber"];
    delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
   
//    
//    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"NOTIFICATIONS",@"OFFERS"]];
//    segmentedControl.frame = CGRectMake(0,88, self.view.frame.size.width, 46);
//    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
//    //    segmentedControl.verticalDividerEnabled = YES;
//    //    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(251/255.0) green:(196/255.0) blue:(12/255.0) alpha:1];
//    // segmentedControl.backgroundColor = [UIColor lightGrayColor];
//    [segmentedControl setTintColor:[UIColor whiteColor]];
//    
//    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
//    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
//    [self.view addSubview:segmentedControl];
    [self getMessages];

}

-(void)segmentedControlChangedValue
{
    [self.messagesTbl reloadData];
    
//    [HelpshiftCampaigns showInboxOnViewController:self withConfig:nil];
//
      HelpshiftInbox *inbox = [HelpshiftInbox sharedInstance];
    
//      NSArray * array=[inbox getAllInboxMessages];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(messagesList.count>0)
    {
    return messagesList.count;
    }
    else
    {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(messagesList.count>0)
    {
   
          NewNotificationTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
        NSString * messageStr = [NSString stringWithFormat:@"%@",[[messagesList objectAtIndex:indexPath.row]objectForKey:@"message"]];
        if(messageStr!=nil && ![messageStr isEqualToString:@"<null>"])
        {
            cell.messageLbl.text = messageStr;
        }
       
        
        
        
    
        NSString * timeStr = [NSString stringWithFormat:@"%@",[[messagesList objectAtIndex:indexPath.row]objectForKey:@"createdon"]];
        if(timeStr!=nil && ![timeStr isEqualToString:@"<null>"])
        {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
            NSDate *date = [dateFormatter dateFromString:timeStr]; // create date from string
            
            // change to a readable time format and change to local time zone
            [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            NSString *finalDate = [dateFormatter stringFromDate:date];
            cell.timeLbl.text = finalDate;
        }
//         NoMessagesCell * cell=[tableView dequeueReusableCellWithIdentifier:@"nomessages" forIndexPath:indexPath];
        
        NSString * actedStr = [NSString stringWithFormat:@"%@",[[messagesList objectAtIndex:indexPath.row]objectForKey:@"isacted"]];
        if(actedStr==nil && [actedStr isEqualToString:@"<null>"])
        {
//            cell.bulletView.hidden=NO;
            cell.bulletView.layer.borderColor = [DefaultColor CGColor];
            cell.bulletView.backgroundColor = SelectedColor;
            
            
        }
        else
            
        {
//              cell.bulletView.hidden=YES;
            cell.bulletView.layer.borderColor = [BorderltColor CGColor];
            cell.bulletView.backgroundColor = DefaultColor;
        }
        return cell;
            
            
    }
    else
    {
        NoMessagesCell * cell=[tableView dequeueReusableCellWithIdentifier:@"nomessages" forIndexPath:indexPath];
        return cell;
    }
    
  
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self onMessageTapAction:indexPath.row];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAction:(id)sender {
    
//    TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
//    
//    [self presentViewController:tabPage animated:YES completion:nil];
    
    [self dismissViewControllerAnimated:YES completion:nil];

}

- (IBAction)readAllBtnAction:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        
    
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Please choose an option." message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [self presentViewController:alert animated:YES completion:^{
        
    }];
    
    UIAlertAction * readOption=[UIAlertAction actionWithTitle:@"Mark all as Read" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
      
        [self markAllAction];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addAction:readOption];
    [alert addAction:cancel];
    });
}

- (IBAction)filterAction:(id)sender {
    
    if(segmentedControl.selectedSegmentIndex==0)
        
    {
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Filer by:" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
        
        
        
        [self presentViewController:alert animated:YES completion:^{
            
            
            
        }];
        
        
        
        UIAlertAction * All=[UIAlertAction actionWithTitle:@"All" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
            
            
            
            
            
        }];
        
        
        
        UIAlertAction * adviceClosed=[UIAlertAction actionWithTitle:@"Advice closed" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
            
            
            
            
            
            
            
        }];
        
        
        
        UIAlertAction * followedOrSubscribed=[UIAlertAction actionWithTitle:@"Followed or subscribed" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
            
            
            
        }];
        
        UIAlertAction * alerts = [UIAlertAction actionWithTitle:@"Alerts" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
            
        }];
        
        
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
            
            
        }];
        
        
        
        
        
        
        
        
        
        
        
        [alert addAction:All];
        
        [alert addAction:adviceClosed];
        
        [alert addAction:followedOrSubscribed];
        
        [alert addAction:alerts];
        
        [alert addAction:cancel];
        
        
        
    }else if (segmentedControl.selectedSegmentIndex==1)
        
    {
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Filter by:" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
        
        
        
        [self presentViewController:alert animated:YES completion:^{
            
            
            
        }];
        
        
        
        UIAlertAction * All=[UIAlertAction actionWithTitle:@"All" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
            
            
            
            
            
        }];
        
        
        
        //        UIAlertAction * adviceClosed=[UIAlertAction actionWithTitle:@"Advice closed" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        //
        
        //
        
        //
        
        //
        
        //        }];
        
        //
        
        //        UIAlertAction * followedOrSubscribed=[UIAlertAction actionWithTitle:@"Followed or subscribed" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        //
        
        //
        
        //        }];
        
        //        UIAlertAction * alerts = [UIAlertAction actionWithTitle:@"Alerts" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        //
        
        //        }];
        
        
        
        UIAlertAction * referrals = [UIAlertAction actionWithTitle:@"Referrals" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
            
        }];
        
        
        
        UIAlertAction * rewards = [UIAlertAction actionWithTitle:@"Rewards" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
            
        }];
        
        
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
            
            
        }];
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        [alert addAction:All];
        
        // [alert addAction:adviceClosed];
        
        //  [alert addAction:followedOrSubscribed];
        
        //  [alert addAction:alerts];
        
        [alert addAction:referrals];
        
        [alert addAction:rewards];
        
        [alert addAction:cancel];
        
        
        
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    if(messagesList.count>0)
    {
   
    return 79;
    }
    else
    {
        return 400;
    }
}

-(void)getMessages
   {
       @try
       {
           TagEncode * enocde=[[TagEncode alloc]init];
           enocde.inputRequestString=@"generateOtp";
           NSMutableArray * inputArray = [[NSMutableArray alloc]init];
           [inputArray addObject:[NSString stringWithFormat:@"%@notification/usernotification?mobilenumber=%@&limit=1000&offset=0",delegate.baseUrl,number]];
           [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
               messagesList =[[dict objectForKey:@"data"] objectForKey:@"data"];
               
               
               if(messagesList.count>0)
               {
              
                       dispatch_async(dispatch_get_main_queue(), ^{
                      
                           [self.messagesTbl reloadData];
                       });
    
               }
               else
               {
                   dispatch_async(dispatch_get_main_queue(), ^{
                       UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                       
                       [self presentViewController:alert animated:YES completion:^{
                           
                       }];
                       
                       
                       UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                           [self getMessages];
                       }];
                       
                       UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                           
                       }];
                       
                       
                       [alert addAction:retryAction];
                       [alert addAction:cancel];
                       
                   });
                   
               }
           }];
           
           
       }
       
       @catch (NSException * e) {
           //NSLog(@"Exception: %@", e);
       }
       @finally {
           //NSLog(@"finally");
       }
   }

-(void)onMessageTapAction:(NSUInteger)rowNumber
{
    @try
    {
        NSString * messageId = [NSString stringWithFormat:@"%@",[[messagesList objectAtIndex:rowNumber] objectForKey:@"id"]];
        if(messageId !=nil && ![messageId isEqualToString:@"<null>"])
        {
            TagEncode * enocde=[[TagEncode alloc]init];
            NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
            NSString * number= [loggedInUser stringForKey:@"profilemobilenumber"];
            enocde.inputRequestString=@"closebanner";
            NSMutableArray * inputArray=[[NSMutableArray alloc]init];
            [inputArray addObject:number];
            [inputArray addObject:@"1"];
            [inputArray addObject:messageId];
            [inputArray addObject:delegate.zenwiseToken];
            [inputArray addObject:[NSString stringWithFormat:@"%@notification/act",delegate.baseUrl]];
            [inputArray addObject:delegate.userID];
            
            [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
                NSLog(@"%@",dict);
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                });
            }];
        }
        NSMutableDictionary * inputDict = [[NSMutableDictionary alloc]init];
        [inputDict setObject:[[messagesList objectAtIndex:rowNumber] objectForKey:@"landingpage"] forKey:@"u"];
        if([inputDict objectForKey:@"u"]!=nil)
        {
            delegate.notificationInfo = inputDict;
            TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
            
            [self presentViewController:tabPage animated:YES completion:nil];
            
        }
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}

-(void)markAllAction
{
    
    @try
    {
        TagEncode * enocde=[[TagEncode alloc]init];
        enocde.inputRequestString=@"markasread";
        NSMutableArray * inputArray = [[NSMutableArray alloc]init];
        [inputArray addObject:number];
        [inputArray addObject:delegate.zenwiseToken];
        [inputArray addObject:[NSString stringWithFormat:@"%@notification/markread",delegate.baseUrl]];
        [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            NSString * message =[NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"] objectForKey:@"message"]];
            
            
            if([message isEqualToString:@"success"])
            {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self.messagesTbl reloadData];
                });
                
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    
                    UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                        [self markAllAction];
                    }];
                    
                    UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                    }];
                    
                    
                    [alert addAction:retryAction];
                    [alert addAction:cancel];
                    
                });
                
            }
        }];
        
        
    }
    
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
@end
