//
//  sample.h
//  testing
//
//  Created by zenwise technologies on 28/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface sample : UIViewController<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property NSString * checkString;
@property NSURL *url;
@property NSString * urlString;

@end
