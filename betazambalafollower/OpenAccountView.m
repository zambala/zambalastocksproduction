//
//  OpenAccountView.m
//  testing
//
//  Created by zenwise technologies on 28/02/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "OpenAccountView.h"
#import "BrokerCell.h"
#import "BrokerDetailPage.h"
#import "AppDelegate.h"
#import <Mixpanel/Mixpanel.h>
#import "TagEncode.h"

@interface OpenAccountView ()
{
    
    
    AppDelegate * delegate1;
    NSString * brokerName;
}

@end

@implementation OpenAccountView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   @try
    {
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.navigationItem.title = @"Open Account";
    
     [self.navigationController setNavigationBarHidden:NO animated:YES];
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"open_account_page"];
    
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                                @"authtoken":delegate1.zenwiseToken,
                                  @"deviceid":delegate1.currentDeviceId
                             };
    
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower/recommendedbrokers?clientid=%@",delegate1.baseUrl,delegate1.userID]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==403)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                            
                                                        }
                                                        else if([httpResponse statusCode]==401)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                        self.responseArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        //NSLog(@"Open account responseArray:%@",self.responseArray);

                                                        }

                                                        
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        self.brokerTbl.delegate=self;
                                                        self.brokerTbl.dataSource=self;
                                                        [self.brokerTbl reloadData];
                                                    });
                                                }];
    [dataTask resume];
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//tableview//

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [[self.responseArray objectForKey:@"results"] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    @try
    {
    
        BrokerCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
   
    
    cell.brokerTitle.text = [[[self.responseArray objectForKey:@"results"] objectAtIndex:indexPath.row] objectForKey:@"name"];
    cell.brokerDetail.text = [[[[self.responseArray objectForKey:@"results"] objectAtIndex:indexPath.row] objectForKey:@"about"] objectForKey:@"description"];
    cell.brokerImgView.image=nil;
    
    NSURL *url;
    @try {
        url =  [NSURL URLWithString:[[[[self.responseArray objectForKey:@"results"] objectAtIndex:indexPath.row] objectForKey:@"about"] objectForKey:@"logourl"]];
    } @catch (NSException *exception) {
        
        
        
    } @finally {
        
    }
    
    
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (cell)
                        cell.brokerImgView.image = image;
                });
            }
        }
    }];
    [task resume];
    return cell;
    } @catch (NSException *exception) {
        //NSLog(@"Caught Exception");
    } @finally {
        //NSLog(@"Finally");
    }
//

//    
    
 }

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     BrokerCell *cell = [self.brokerTbl cellForRowAtIndexPath:indexPath];
//    BrokerDetailPage * broker=[self.storyboard instantiateViewControllerWithIdentifier:@"BrokerDetailPage"];
//    [self.navigationController pushViewController:broker animated:YES];
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"open_account_web_view_page"];
    @try
    {
        brokerName = cell.brokerTitle.text;
    NSString * url=[[[[self.responseArray objectForKey:@"results"] objectAtIndex:indexPath.row] objectForKey:@"about"] objectForKey:@"openaccounturl"];
    if(url.length>0)
    {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
    else
    {
        [self updateUserRecord];
    }
        
    } @catch (NSException *exception) {
        //NSLog(@"Caught Exception");
    } @finally {
        //NSLog(@"Finally");
    }
}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//   
//}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)informationAction:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:@"For any queries related to the account, please reach out to the respective brokers." preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:^{
        
    }];
    
    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:okAction];
          });
    }

-(void)updateUserRecord
{
   
    @try
    {
        TagEncode * enocde=[[TagEncode alloc]init];
        NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
        enocde.inputRequestString=@"updateuserrecord";
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        
        
        [inputArray addObject:[loggedInUser stringForKey:@"profilename"]];
        [inputArray addObject:[loggedInUser stringForKey:@"profileemail"]];
        [inputArray addObject:[loggedInUser stringForKey:@"profilemobilenumber"]];
        [inputArray addObject:brokerName];
        [inputArray addObject:[NSString stringWithFormat:@"%@",[delegate1.brokerInfoDict objectForKey:@"brokerid"]]];
        [inputArray addObject:delegate1.zenwiseToken];
        [inputArray addObject:[NSString stringWithFormat:@"%@userprofile/openaccountoffline",delegate1.baseUrl]];

        [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            NSArray * keysArray=[dict allKeys];
            
            
            if(keysArray.count>0)
                
            {
                
                NSString * message=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"message"]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:message preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                    }];
                    
                    [alert addAction:okAction];
                });
           
                
            }
            else
            {
                [self updateUserRecord];
            }
            
        }];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
@end
