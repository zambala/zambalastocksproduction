//
//  IndexWatch.h
//  betazambalafollower
//
//  Created by Zenwise Technologies on 14/12/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IndexWatch : NSObject

@property short usAT;
@property int btDataType; // 0:UnCompress , 1:lzo_compress.zLib
@property short usSize;
@property short usMsgCode;
@property int inSeqID;
@property NSString*stExchange; //48
@property int btNull1;

@property NSString *stSymbol1; //48
@property int btNull2;

@property int inBroadcastTime;

@property NSString* stSymbol2; // 32
@property int btNull3;

@property double dbIndexPrice;
@property double dbPrevIndexClose;
@property double dbOpen;
@property double dbHigh;
@property double dbLow;
@property double dbLifeTimeHigh;
@property double dbLifeTimeLow;


+ (id)Mt6;

@end
