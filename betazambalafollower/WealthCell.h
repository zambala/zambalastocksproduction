//
//  WealthCell.h
//  betazambalafollower
//
//  Created by Zenwise Technologies on 01/08/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WealthCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *fastestBtn;
@property (weak, nonatomic) IBOutlet UIButton *consistentBtn;

@property (weak, nonatomic) IBOutlet UIButton *twentyFiveYearsBtn;
@property (weak, nonatomic) IBOutlet UIButton *fiveYearsBtn;

@property (weak, nonatomic) IBOutlet UIButton *biggestBtn;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLbl;
@end
