//
//  TagEncode.h
//  betazambalafollower
//
//  Created by zenwise technologies on 29/05/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TagEncode : UIViewController

-(void) CheckCapacity:(int) inLength;
-(void) TagKeyAppend:( short) shTag;
-(void) TagData:(short) shTag;
-(void) TagData:(short)shTag boolMethod:(BOOL)blValue;
-(void) TagData:(short) shTag  byteMethod:(int) btValue;
-(void) TagData:(short)shTag shValueMethod:(short) shValue;
-(void) TagData:(short) shTag intMethod:(int) inValue;
-(void) TagData:(short) shTag longMethod:(long) lnValue;
-(void) TagData:(short)shTag doubleMethod:(double) lnValue;
-(void)TagData:(short)shTag stringMethod:(NSString *) stValue;
-(void) TagData1:(NSMutableArray *) btBuffer;
-(void)GetBuffer;
-(int)GetSize;
-(void)inputRequestMethod:(NSMutableArray *)inputArray withCompletionHandler:(void (^)(NSDictionary *))handler;
-(void)inputRequestMethod1:(NSMutableArray *)inputArray withCompletionHandler:(void (^)(NSDictionary *))handler;
@property NSString * inputRequestString;
@property NSDictionary * responseDict;
-(void)screenNavigation:(NSDictionary *)inputDict;
-(void)bseBowInputRequestMethod:(NSMutableArray *)inputArray withCompletionHandler:(void (^)(NSDictionary *))handler;
@end
