//
//  WisdomGrdenView.h
//  testing
//
//  Created by zenwise mac 2 on 11/23/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+MJPopupViewController.h"
#import "Reachability.h"
#import "MQTTKit.h"
/*!
 @author Sanka Revanth
 @class WisdomGrdenView
 @discussion This class has different sections(EQUITIES,DERIVATIVES,CURRENCY,COMMODITIES) where the user can check the advices related to segment.
 @superclass SuperClass: UIViewController\n
  @classdesign    Designed using UIView,Button,BarButton,ImageView,Label,UITableview.
 @coclass    AppDelegate
 
 */

@interface WisdomGrdenView : UIViewController<UISearchBarDelegate, UISearchResultsUpdating, UISearchControllerDelegate,UITableViewDelegate,UITableViewDataSource>


- (IBAction)popUpBtn:(id)sender;/*!<information button*/

@property (strong, nonatomic) UISearchController *controller;
@property Reachability * reach;
@property (nonatomic, strong) MQTTClient *client;
@property (nonatomic, assign) CGFloat lastContentOffset;
@property (weak, nonatomic) IBOutlet UITableView *equitiesTableView;

@property (weak, nonatomic) IBOutlet UITableView *derivativesTableView;

@property NSMutableDictionary * responseArray;/*!<it stores data from zambala server*/
@property BOOL test;/*!<unused*/
@property int check;/*!<unused*/

@property (weak, nonatomic) IBOutlet UILabel *premiumLbl;/*!<premium service */

@property (weak, nonatomic) IBOutlet UIImageView *premiumImgView;/*!<premium service image */
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;


@end
