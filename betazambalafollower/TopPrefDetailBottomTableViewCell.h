//
//  TopPrefDetailBottomTableViewCell.h
//  betazambalafollower
//
//  Created by Zenwise Technologies on 22/06/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopPrefDetailBottomTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UIButton *leaderNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *publicImageView;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *ltpLabel;
@property (weak, nonatomic) IBOutlet UILabel *advicePandLLabel;
@property (weak, nonatomic) IBOutlet UILabel *actedByLabel;
@property (weak, nonatomic) IBOutlet UILabel *sharesTradedLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateAndTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *buySellButton;
@property (weak, nonatomic) IBOutlet UIButton *closingAdviceButton;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIImageView *tickImageView;
@property (weak, nonatomic) IBOutlet UILabel *bottomMessgeLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomAdvicePandL;
@property (weak, nonatomic) IBOutlet UILabel *bottomActedByLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomLTPLabel;
@property (weak, nonatomic) IBOutlet UIButton *openingAdviceButton;
@property (weak, nonatomic) IBOutlet UILabel *bottomSharesTradedButton;
@property (weak, nonatomic) IBOutlet UILabel *bottomDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *sourceLabel;

@property (weak, nonatomic) IBOutlet UILabel *sourceDataLabel;

@end
