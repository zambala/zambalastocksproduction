//
//  RewardsHistoryViewController.m
//  betazambalafollower
//
//  Created by Zenwise Technologies Private Limited on 22/08/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import "RewardsHistoryViewController.h"
#import "RewardsHistVoucherTableViewCell.h"
#import "RewardHistoryTableViewCell.h"
#import "HMSegmentedControl.h"
#import "AppDelegate.h"
#import "UIImageView+WebCache.h"
#import <QuartzCore/QuartzCore.h>
#import "UIViewController+MJPopupViewController.h"
#import "VoucherPopUpViewController.h"
#import "VoucherTermsPopupViewController.h"
@import Mixpanel;
@interface RewardsHistoryViewController ()
{
    HMSegmentedControl * segmentedControl;
    AppDelegate * delegate;
    NSString * companyNameList;
    NSString * companyName;
    NSString * logoURL;
    NSString * termsAndConditions;
}

@end

@implementation RewardsHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //NSLog(@"%@",delegate.historyArray);
    self.activityIndicator.hidden=YES;
    self.voucherTableView.hidden=YES;
    [self.segmentControlView setBackgroundColor:[UIColor colorWithRed:(248/255.0) green:(248/255.0) blue:(248/255.0) alpha:1]];
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"REDEEMED VOUCHERS",@"POINTS HISTORY"]];
    segmentedControl.frame = CGRectMake(0, 0, self.segmentControlView.frame.size.width, 40);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    //    segmentedControl.verticalDividerEnabled = YES;
    //    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(251/255.0) green:(196/255.0) blue:(12/255.0) alpha:1];
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
    [segmentedControl setBackgroundColor:[UIColor colorWithRed:(248/255.0) green:(248/255.0) blue:(248/255.0) alpha:1.0f]];
    segmentedControl.selectionIndicatorColor=[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f];
    segmentedControl.tintColor=[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f];
    NSDictionary *defaults = @{
                               NSFontAttributeName : [UIFont systemFontOfSize:14.6f],
                               NSForegroundColorAttributeName : [UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1],
                               };
    segmentedControl.titleTextAttributes=defaults;
    [self.segmentControlView addSubview:segmentedControl];
    
    [segmentedControl setSelectedSegmentIndex:0];
    [self segmentedControlChangedValue];
    [self.voucherOkButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.termsOkayButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.termsAndConditionsLabel addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.backButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

-(void)onButtonTap:(UIButton *)sender
{
    if (sender == self.backButton)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)segmentedControlChangedValue
{
    self.voucherTableView.hidden=YES;
    if(segmentedControl.selectedSegmentIndex==0)
    {
        Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
        [mixpanelMini track:@"reward_redeemed_voucher_history_page"];
        [self getVouchers];
    }else if (segmentedControl.selectedSegmentIndex==1)
    {
        Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
        [mixpanelMini track:@"reward_point_history_page"];
        self.voucherTableView.hidden=NO;
        [self.voucherTableView reloadData];
    }
}

-(void)getListVouchers
{
   @try
    {
        NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
        NSString*number= [loggedInUser stringForKey:@"profilemobilenumber"];
        
     
        NSDictionary *headers = @{ @"authtoken":delegate.zenwiseToken,
                                   @"deviceid":delegate.currentDeviceId,
                                   @"mobilenumber":number
                                   };
        
        NSString * url = [NSString stringWithFormat:@"%@voucher/list",delegate.baseUrl];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==200)
                                                        {
                                                            self.getListReponseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            if(self.getListReponseDictionary.count>0)
                                                            {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    self.activityIndicator.hidden=YES;
                                                                    [self.activityIndicator stopAnimating];
                                                                    self.voucherTableView.hidden=NO;
                                                                    self.voucherTableView.delegate = self;
                                                                    self.voucherTableView.dataSource = self;
                                                                    [self.voucherTableView reloadData];
                                                                });
                                                            }
                                                            
                                                        }else
                                                        {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                self.activityIndicator.hidden=NO;
                                                                [self.activityIndicator startAnimating];
                                                           
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Sorry, we are unable to get the vouchers at the moment." preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                
                                                            }];
                                                            UIAlertAction * tryAgain=[UIAlertAction actionWithTitle:@"Try again" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                [self getListVouchers];
                                                            }];
                                                            
                                                            [alert addAction:okAction];
                                                            [alert addAction:tryAgain];
                                                            });
                                                        }
                                                    }
                                                }];
    [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)getVouchers
{
    @try
    {
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
        
        NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
        NSString*number= [loggedInUser stringForKey:@"profilemobilenumber"];
        NSString * url = [NSString stringWithFormat:@"%@voucher/history/?tag=%@",delegate.baseUrl,number];
        
        
       
        NSDictionary *headers = @{ @"authtoken":delegate.zenwiseToken,
                                   @"deviceid":delegate.currentDeviceId,
                                   @"mobilenumber":number
                                   };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==200)
                                                        {
                                                            self.vouchersHistoryDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            if(self.vouchersHistoryDictionary.count>0)
                                                            {
                                                                [self getListVouchers];
                                                            }
                                                        }else
                                                        {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                self.activityIndicator.hidden=YES;
                                                                [self.activityIndicator stopAnimating];
                                                                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong. Try again!" preferredStyle:UIAlertControllerStyleAlert];
                                                                
                                                                [self presentViewController:alert animated:YES completion:^{
                                                                    
                                                                }];
                                                                
                                                                
                                                                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                    
                                                                    
                                                                }];
                                                                
                                                                UIAlertAction * tryAgain=[UIAlertAction actionWithTitle:@"Try again" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                            
                                                                    [self getVouchers];
                                                                    
                                                                }];
                                                                
                                                                
                                                                [alert addAction:okAction];
                                                                [alert addAction:tryAgain];
                                                            });
                                                        }
                                                    }
                                                }];
    [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
-(void)popUPUI
{
 
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    @try
    {
    if(segmentedControl.selectedSegmentIndex==0)
    {
       return [[self.vouchersHistoryDictionary objectForKey:@"data"] count];
    }else if (segmentedControl.selectedSegmentIndex==1)
    {
        return delegate.historyArray.count;
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @try
    {
    if(segmentedControl.selectedSegmentIndex==0)
    {
        RewardsHistVoucherTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"RewardsHistVoucherTableViewCell" forIndexPath:indexPath];
        cell.backview.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16] CGColor];
        cell.backview.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
        cell.backview.layer.shadowOpacity = 1.0f;
        cell.backview.layer.shadowRadius = 2.0f;
        cell.backview.layer.cornerRadius = 5.0f;
        cell.backview.layer.masksToBounds = NO;
        cell.showVoucherButton.layer.cornerRadius = 10.0f;
        NSString * companyNameHist = [NSString stringWithFormat:@"%@",[[[[self.vouchersHistoryDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"product"] objectForKey:@"product_name"]];
        for (int i=0; i<[[self.getListReponseDictionary objectForKey:@"data"] count]; i++) {
            companyNameList = [NSString stringWithFormat:@"%@",[[[self.getListReponseDictionary objectForKey:@"data"] objectAtIndex:i] objectForKey:@"brand_name"]];
            if([companyNameHist isEqualToString:companyNameList])
            {
                NSString * vaucherImage = [NSString stringWithFormat:@"%@",[[[self.getListReponseDictionary objectForKey:@"data"] objectAtIndex:i] objectForKey:@"logo_link"]];
                [cell.voucherImageView sd_setImageWithURL:[NSURL URLWithString:vaucherImage] placeholderImage:[UIImage imageNamed:@""]];
                companyNameList=@"";
            }
        }
        cell.voucherNameLabel.text = companyNameHist;
        NSString * voucherValue = [NSString stringWithFormat:@"%@",[[[[self.vouchersHistoryDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"product"] objectForKey:@"amount"]];
        cell.voucherValueLabel.text = voucherValue;
        NSString * referenceID = [NSString stringWithFormat:@"%@",[[[self.vouchersHistoryDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"id"]];
        cell.referenceIDLabel.text = referenceID;
         NSString * timeStamp = [NSString stringWithFormat:@"%@",[[[self.vouchersHistoryDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"created"]];
        double unixTimeStamp =[timeStamp doubleValue];
        NSTimeInterval _interval=unixTimeStamp;
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
        NSDateFormatter *formatter= [[NSDateFormatter alloc] init];
        [formatter setLocale:[NSLocale currentLocale]];
        [formatter setDateFormat:@"dd.MM.yyyy"];
        NSString *dateString = [formatter stringFromDate:date];
        cell.redeemDateLabel.text = [NSString stringWithFormat:@"Redeemed on: %@",dateString];
        [cell.showVoucherButton addTarget:self action:@selector(onShowVoucherTap:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }else if (segmentedControl.selectedSegmentIndex==1)
    {
        RewardHistoryTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"RewardHistoryTableViewCell" forIndexPath:indexPath];
        NSString * dateStr=[NSString stringWithFormat:@"%@",[[delegate.historyArray objectAtIndex:indexPath.row] objectForKey:@"createdon"]];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        NSDate *date = [dateFormatter dateFromString:dateStr]; // create date from string
        // change to a readable time format and change to local time zone
        [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        NSString *finalDate = [dateFormatter stringFromDate:date];
        cell.dateLabel.text=finalDate;
        NSString * descString = [NSString stringWithFormat:@"%@",[[delegate.historyArray objectAtIndex:indexPath.row] objectForKey:@"action"]];
        cell.descriptionLabel.text = descString;
        return cell;
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(segmentedControl.selectedSegmentIndex==0)
    {
        return 140;
    }else if (segmentedControl.selectedSegmentIndex==1)
    {
        return 80;
    }
    return 0;
}


-(void)onShowVoucherTap:(UIButton*)sender
{
    @try
    {
    VoucherPopUpViewController *detailViewController = [[VoucherPopUpViewController alloc] initWithNibName:@"VoucherPopUpViewController" bundle:nil];
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.voucherTableView];
    NSIndexPath *indexPath = [self.voucherTableView indexPathForRowAtPoint:buttonPosition];
    NSString * companyNameHist = [NSString stringWithFormat:@"%@",[[[[self.vouchersHistoryDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"product"] objectForKey:@"product_name"]];
    for (int i=0; i<[[self.getListReponseDictionary objectForKey:@"data"] count]; i++) {
        companyNameList = [NSString stringWithFormat:@"%@",[[[self.getListReponseDictionary objectForKey:@"data"] objectAtIndex:i] objectForKey:@"brand_name"]];
        if([companyNameHist isEqualToString:companyNameList])
        {
            termsAndConditions = [NSString stringWithFormat:@"%@",[[[self.getListReponseDictionary objectForKey:@"data"] objectAtIndex:i] objectForKey:@"terms"]];
        }
    }
        NSString * companyName = [NSString stringWithFormat:@"%@",[[[[self.vouchersHistoryDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"product"] objectForKey:@"product_name"]];
        detailViewController.voucherCompanyName = companyName;
        NSString * expiryDate = [NSString stringWithFormat:@"%@",[[[[self.vouchersHistoryDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"product"] objectForKey:@"validity_date"]];
    detailViewController.voucherExpiry = expiryDate;
        NSString *decodeString = [termsAndConditions stringByRemovingPercentEncoding];
        NSString *decodeString1 = [decodeString stringByReplacingOccurrencesOfString:@"+" withString:@" "];
        NSString * terms = [self convertHtmlPlainText:decodeString1];
    detailViewController.voucherTerms = terms;
    NSString * voucherCode = [NSString stringWithFormat:@"%@",[[[[self.vouchersHistoryDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"product"] objectForKey:@"code"]];
    detailViewController.voucherCode = voucherCode;
    [self presentPopupViewController:detailViewController animationType: MJPopupViewAnimationFade];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
-(NSString*)convertHtmlPlainText:(NSString*)HTMLString{
    NSData *HTMLData = [HTMLString dataUsingEncoding:NSUTF8StringEncoding];
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithData:HTMLData options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:NULL error:NULL];
    NSString *plainString = attrString.string;
    
    return plainString;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
