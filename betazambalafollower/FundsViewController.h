//
//  FundsViewController.h
//  Funds
//
//  Created by Zenwise Technologies on 27/11/17.
//  Copyright © 2017 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FundsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *fundsView;
@property (weak, nonatomic) IBOutlet UIButton *payButton;
@property (weak, nonatomic) IBOutlet UILabel *netCashAvailable;
@property (weak, nonatomic) IBOutlet UILabel *amountTodayLabel;
@property (weak, nonatomic) IBOutlet UILabel *accontCashLabel;
@property (weak, nonatomic) IBOutlet UILabel *cashTodayLabel;
@property NSMutableDictionary * zerodhaResponseDictionary;
@property NSMutableDictionary * upstoxResponseDicitionary;
@property (weak, nonatomic) IBOutlet UIButton *derivativesPayoutButton;
@property (weak, nonatomic) IBOutlet UIButton *payInButton;
@property (weak, nonatomic) IBOutlet UILabel *eqLabel1;
@property (weak, nonatomic) IBOutlet UILabel *eqLabel2;
@property (weak, nonatomic) IBOutlet UILabel *eqLabel3;
@property (weak, nonatomic) IBOutlet UILabel *eqLabel4;
@property (weak, nonatomic) IBOutlet UILabel *drLabel1;
@property (weak, nonatomic) IBOutlet UILabel *drLabel2;
@property (weak, nonatomic) IBOutlet UILabel *drLabel3;
@property (weak, nonatomic) IBOutlet UILabel *drLabel4;
@property (weak, nonatomic) IBOutlet UILabel *drCashAvailableLabel;
@property (weak, nonatomic) IBOutlet UILabel *drAmountTodayLabel;
@property (weak, nonatomic) IBOutlet UILabel *drAccountCashLabel;
@property (weak, nonatomic) IBOutlet UILabel *drUsedTodayLabel;

- (IBAction)backAction:(id)sender;

@end
