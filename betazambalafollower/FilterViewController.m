//
//  FilterViewController.m
//  ZambalaUSA
//
//  Created by guna on 19/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "FilterViewController.h"
#import "HCSStarRatingView.h"
#import "AppDelegate.h"
//#import "MarketMonksViewController.h"

@interface FilterViewController ()
{
    NSString * equitiesString;
    NSString * currencyString;
    NSString * devirativeString;
    NSString * commoditiesString;
    NSString * dayTradeString;
    NSString * shortTermString;
    NSString * longTermString;
    NSString *ratingValue;
    AppDelegate * delegate;
    NSString * stringStar;
    
}

@end

@implementation FilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Filter";

    self.navigationController.navigationBarHidden=NO;
//     self.navigationItem.leftBarButtonItem.title=@" ";
  
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *backBtnImage = [UIImage imageNamed:@"screenClose.png"]  ;
    [backBtn setBackgroundImage:backBtnImage forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(goback) forControlEvents:UIControlEventTouchUpInside];
    backBtn.frame = CGRectMake(3,12,36, 36);
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn] ;
    self.navigationItem.leftBarButtonItem = backButton;
    
    //NSLog(@"Rating view did load delegate:%@",delegate.rating);
    
    
    
    self.equityButtonOutlet.selected=YES;
    self.derivativeButtonOutlet.selected=YES;
    self.currencyButtonOutlet.selected=YES;
    self.commoditiesButtonOutlet.selected=YES;
    self.dayTradeButtonOutlet.selected=YES;
    self.shortTermButtonOutlet.selected=YES;
    self.longTermButtonOutlet.selected=YES;
    

    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    //NSLog(@"Rating view did load prefs:%d",[[prefs objectForKey:@"rating"] intValue]);
    
//    if(prefs !=nil)
//    {
//
//        // getting an NSString
//
//        @try {
//           stringStar=[prefs stringForKey:@"rating"];
//
//        } @catch (NSException *exception) {
//
//        } @finally {
//
//        }
//
//
//
//        NSString * equityStr=[prefs stringForKey:@"1"];
//
//        if([equityStr isEqualToString:@"EquitySelected"])
//        {
//            self.equityButtonOutlet.selected = YES;
//
//        }
//
//        else if([equityStr isEqualToString:@"EquityUnSelected"])
//        {
//            self.equityButtonOutlet.selected = NO;
//
//
//        }
//
//        NSString * derivativeStr=[prefs stringForKey:@"2"];
//
//        if([derivativeStr isEqualToString:@"derivativeSelected"])
//        {
//            self.derivativeButtonOutlet.selected = YES;
//
//        }
//
//        else if([derivativeStr isEqualToString:@"derivativeUnSelected"])
//        {
//            self.derivativeButtonOutlet.selected = NO;
//
//
//        }
//
//        NSString * currencyStr=[prefs stringForKey:@"3"];
//
//        if([currencyStr isEqualToString:@"CurrencySelected"])
//        {
//            self.currencyButtonOutlet.selected = YES;
//
//        }
//
//        else if([currencyStr isEqualToString:@"CurrencyUnSelected"])
//        {
//            self.currencyButtonOutlet.selected = NO;
//
//
//        }
//
//        NSString * commodityStr=[prefs stringForKey:@"4"];
//
//        if([commodityStr isEqualToString:@"CommoditySelected"])
//        {
//            self.commoditiesButtonOutlet.selected = YES;
//
//        }
//
//        else if([commodityStr isEqualToString:@"CommodityUnSelected"])
//        {
//            self.commoditiesButtonOutlet.selected = NO;
//
//
//        }
//
//        NSString * day=[prefs stringForKey:@"5"];
//
//        if([day isEqualToString:@"DaySelected"])
//        {
//            self.dayTradeButtonOutlet.selected = YES;
//
//        }
//
//        else if([day isEqualToString:@"DayUnSelected"])
//        {
//            self.dayTradeButtonOutlet.selected = NO;
//
//
//        }
//
//
//        NSString * shortTerm=[prefs stringForKey:@"6"];
//
//        if([shortTerm isEqualToString:@"ShortSelected"])
//        {
//            self.shortTermButtonOutlet.selected = YES;
//
//        }
//
//        else if([shortTerm isEqualToString:@"ShortUnSelected"])
//        {
//            self.shortTermButtonOutlet.selected = NO;
//
//
//        }
//
//        NSString * longTerm=[prefs stringForKey:@"7"];
//
//        if([longTerm isEqualToString:@"LongSelected"])
//        {
//            self.longTermButtonOutlet.selected = YES;
//
//        }
//
//        else if([longTerm isEqualToString:@"LongUnSelected"])
//        {
//            self.longTermButtonOutlet.selected = NO;
//
//
//        }
//
//
//
//    }
    
    

    
    delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.starRatingView.minimumValue=0;
    self.starRatingView.maximumValue=5;
    self.starRatingView.value = 0;
    self.starRatingView.allowsHalfStars=NO;
    self.starRatingView.allowsHalfStars=NO;
    self.starRatingView.emptyStarImage = [UIImage imageNamed:@"ratingstarEmpty"];
    self.starRatingView.halfStarImage = [UIImage imageNamed:@"starsmallHalf"]; // optional
    self.starRatingView.filledStarImage = [UIImage imageNamed:@"ratingstarFilled"];
    [self.starRatingView setUserInteractionEnabled:YES];
    [self.starRatingView addTarget:self action:@selector(didChangeValue:) forControlEvents:UIControlEventValueChanged];
    
    
    
//    UIImage *NavigationPortraitBackground= [UIImage imageNamed:@"screenClose.png"];
//    [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
    
    self.segmentView.layer.shadowOffset = CGSizeMake(0, 2);
    self.segmentView.layer.shadowOpacity =0.1;
    self.segmentView.layer.shadowRadius = 3.1;
    self.segmentView.clipsToBounds = NO;
    
    
    self.ratingView.layer.shadowOffset = CGSizeMake(0, 2);
    self.ratingView.layer.shadowOpacity = 0.1;
    self.ratingView.layer.shadowRadius = 3.1;
    self.ratingView.clipsToBounds = NO;
    
    self.tradeView.layer.shadowOffset = CGSizeMake(0, 2);
    self.tradeView.layer.shadowOpacity = 0.1;
    self.tradeView.layer.shadowRadius = 3.1;
    self.tradeView.clipsToBounds = NO;
    
    self.equityButtonOutlet.selected=NO;
     self.derivativeButtonOutlet.selected=NO;
     self.currencyButtonOutlet.selected=NO;
     self.commoditiesButtonOutlet.selected=NO;
     self.dayTradeButtonOutlet.selected=NO;
     self.shortTermButtonOutlet.selected=NO;
     self.longTermButtonOutlet.selected=NO;
    
    
    [self.equityButtonOutlet setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    [self.equityButtonOutlet setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    
    [self.derivativeButtonOutlet setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    [self.derivativeButtonOutlet setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    
    [self.currencyButtonOutlet setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    [self.currencyButtonOutlet setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    
    [self.commoditiesButtonOutlet setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    [self.commoditiesButtonOutlet setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    
    [self.dayTradeButtonOutlet setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    [self.dayTradeButtonOutlet setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    
    
    [self.longTermButtonOutlet setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    [self.longTermButtonOutlet setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    
    
    [self.shortTermButtonOutlet setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    [self.shortTermButtonOutlet setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    
    self.segmentDictionary= [[NSMutableDictionary alloc]init];
    self.tradeDictionary = [[NSMutableDictionary alloc]init];
    

    
    // Do any additional setup after loading the view.
}
- (void)goback
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backButtonAction:(id)sender {
//    MarketMonksViewController * marketVC=[self.storyboard instantiateViewControllerWithIdentifier:@"market"];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)currencyButtonAction:(id)sender {
    if(self.currencyButtonOutlet.selected==NO)
    {
        self.currencyButtonOutlet.selected=YES;
        self.crCheckFlag=false;
        [self.segmentDictionary setObject:[NSNumber numberWithInt:3] forKey:@"currency"];
    }else if(self.currencyButtonOutlet.selected==YES)
    {
        self.currencyButtonOutlet.selected=NO;
        self.crCheckFlag=true;
        [self.segmentDictionary removeObjectForKey:@"currency"];
    }
}
- (IBAction)derivativeButtonAction:(id)sender {
    if( self.derivativeButtonOutlet.selected==NO)
    {
        self.derivativeButtonOutlet.selected=YES;
        self.drCheckFlag=false;
        [self.segmentDictionary setObject:[NSNumber numberWithInt:2] forKey:@"derivative"];
        
    }else if( self.derivativeButtonOutlet.selected==YES)
    {
        self.derivativeButtonOutlet.selected=NO;
        self.drCheckFlag=true;
        [self.segmentDictionary removeObjectForKey:@"derivative"];
    }
}
- (IBAction)commoditiesButtonAction:(id)sender {
    if(self.commoditiesButtonOutlet.selected==NO)
    {
        self.commoditiesButtonOutlet.selected=YES;
        self.commCheckFlag=false;
        [self.segmentDictionary setObject:[NSNumber numberWithInt:4] forKey:@"commodities"];
    }else if(self.commoditiesButtonOutlet.selected==YES)
    {
        self.commoditiesButtonOutlet.selected=NO;
        self.commCheckFlag=true;
        [self.segmentDictionary removeObjectForKey:@"commodities"];
    }
}
- (IBAction)dayTradeButtonAction:(id)sender {
    if(self.dayTradeButtonOutlet.selected==NO)
    {
        self.dayTradeButtonOutlet.selected=YES;
        self.dayTradeCheckFlag=false;
        [self.tradeDictionary setObject:[NSNumber numberWithInt:1] forKey:@"day"];
    }else if(self.dayTradeButtonOutlet.selected==YES)
    {
        self.dayTradeButtonOutlet.selected=NO;
        self.dayTradeCheckFlag=true;
        [self.tradeDictionary removeObjectForKey:@"day"];
    }

}
- (IBAction)longTermButtonAction:(id)sender {
    if(self.longTermButtonOutlet.selected==NO)
    {
        self.longTermButtonOutlet.selected=YES;
        self.dayTradeCheckFlag=false;
        [self.tradeDictionary setObject:[NSNumber numberWithInt:2] forKey:@"long"];
    }else if(self.longTermButtonOutlet.selected==YES)
    {
        self.longTermButtonOutlet.selected=NO;
        self.dayTradeCheckFlag=true;
        [self.tradeDictionary removeObjectForKey:@"long"];
    }

}
- (IBAction)shortTermButtonAction:(id)sender {
    if(self.shortTermButtonOutlet.selected==NO)
    {
        self.shortTermButtonOutlet.selected=YES;
        self.shortTermCheckFlag=false;
        [self.tradeDictionary setObject:[NSNumber numberWithInt:3] forKey:@"short"];
    }else if(self.shortTermButtonOutlet.selected==YES)
    {
        self.shortTermButtonOutlet.selected=NO;
        self.shortTermCheckFlag=true;
        [self.tradeDictionary removeObjectForKey:@"short"];
    }

}
- (IBAction)didChangeValue:(HCSStarRatingView *)sender {
    //NSLog(@"Changed rating to %.1f", sender.value);
    int rating = sender.value;
    
    NSNumber * ratingNum = [NSNumber numberWithInt:rating];
    
    //NSLog(@"Rating:%d",rating);
    
    ratingValue = [ratingNum stringValue];
    
    
}

-(void)filterServer
{
    @try
    {
    NSDictionary *headers = @{ @"content-type": @"application/x-www-form-urlencoded",
                               @"cache-control": @"no-cache",
                               @"authtoken":delegate.zenwiseToken,
                                  @"deviceid":delegate.currentDeviceId
                               };
    
    NSMutableData *postData = [[NSMutableData alloc] initWithData:[delegate.segment3 dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[delegate.duration3 dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[delegate.rating dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower/leaders?clientid=%@",delegate.baseUrl,delegate.userID]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==403)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                            
                                                        }
                                                        else if([httpResponse statusCode]==401)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                        delegate.filterResponseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        //NSLog(@"Filter response dict:%@",delegate.filterResponseDictionary);
                                                        
                                                        if([[delegate.filterResponseDictionary objectForKey:@"message"]isEqualToString:@"No Records found"])
                                                        {
                                                            //NSLog(@"No records Found");
                                                        }else
                                                        {
                                                            delegate.filterCheckString=@"found";
                                                            [delegate.filterMonksArray addObject:delegate.filterResponseDictionary];
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                [self.navigationController popViewControllerAnimated:YES];
                                                        
                                                            });
                                                            
                                                            
                                                        }
                                                        }
                                                    }
                                                }];
    [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
- (IBAction)onActionButtonTap:(id)sender {
     NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    @try {
        
        
        
        [prefs setObject:@"checkSelected" forKey:@"0"];
        
               
        if(self.equityButtonOutlet.selected==YES)
        {
           
            
            
            
            // saving an NSString
            
            [prefs setObject:@"EquitySelected" forKey:@"1"];
            
            
            
            
            
            [prefs synchronize];
        }
        
        else  if(self.equityButtonOutlet.selected==NO)
        {
            [prefs setObject:@"EquityUnSelected" forKey:@"1"];
            
            
            
            
            
            [prefs synchronize];
        }
       
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    @try {
        
        
        
        if(self.derivativeButtonOutlet.selected==YES)
        {
            
            
            
            
            // saving an NSString
            
            [prefs setObject:@"derivativeSelected" forKey:@"2"];
            
            
            
            
            
            [prefs synchronize];
        }
        
        else  if(self.derivativeButtonOutlet.selected==NO)
        {
            [prefs setObject:@"derivativeUnSelected" forKey:@"2"];
            
            
            
            
            
            [prefs synchronize];
        }
        
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    @try {
        
       
        
        if(self.currencyButtonOutlet.selected==YES)
        {
            
            
            
            
            // saving an NSString
            
            [prefs setObject:@"CurrencySelected" forKey:@"3"];
            
            
            
            
            
            [prefs synchronize];
        }
        
        else  if(self.currencyButtonOutlet.selected==NO)
        {
            [prefs setObject:@"CurrencyUnSelected" forKey:@"3"];
            
            
            
            
            
            [prefs synchronize];
        }
        
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    @try {
        
        
        
        if(self.commoditiesButtonOutlet.selected==YES)
        {
            
            
            
            
            // saving an NSString
            
            [prefs setObject:@"CommoditySelected" forKey:@"4"];
            
            
            
            
            
            [prefs synchronize];
        }
        
        else  if(self.commoditiesButtonOutlet.selected==NO)
        {
            [prefs setObject:@"CommodityUnSelected" forKey:@"4"];
            
            
            
            
            
            [prefs synchronize];
        }
        
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    @try {
        
        
        
        if(self.dayTradeButtonOutlet.selected==YES)
        {
            
            
            
            
            // saving an NSString
            
            [prefs setObject:@"DaySelected" forKey:@"5"];
            
            
            
            
            
            [prefs synchronize];
        }
        
        else  if(self.dayTradeButtonOutlet.selected==NO)
        {
            [prefs setObject:@"DayUnSelected" forKey:@"5"];
            
            
            
            
            
            [prefs synchronize];
        }
        
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    
    @try {
        
        
        
        if(self.shortTermButtonOutlet.selected==YES)
        {
            
            
            
            
            // saving an NSString
            
            [prefs setObject:@"ShortSelected" forKey:@"6"];
            
            
            
            
            
            [prefs synchronize];
        }
        
        else  if(self.shortTermButtonOutlet.selected==NO)
        {
            [prefs setObject:@"ShortUnSelected" forKey:@"6"];
            
            
            
            
            
            [prefs synchronize];
        }
        
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    @try {
        
        
        
        if(self.longTermButtonOutlet.selected==YES)
        {
            
            
            
            
            // saving an NSString
            
            [prefs setObject:@"LongSelected" forKey:@"7"];
            
            
            
            
            
            [prefs synchronize];
        }
        
        else  if(self.longTermButtonOutlet.selected==NO)
        {
            [prefs setObject:@"LongUnSelected" forKey:@"7"];
            
            
            
            
            
            [prefs synchronize];
        }
        
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    


    

    
    


    
    //NSLog(@"Trade dict:%@",self.tradeDictionary);
    //NSLog(@"Segment Dict:%@",self.segmentDictionary);
    //NSLog(@"Rating Final:%@",ratingValue);
    
    if(ratingValue.length>0)
    {
        //[ratingValue isEqual:[NSNull null]]||[ratingValue isEqualToString:@"(null)"]||[ratingValue isEqualToString:@"null"]||[ratingValue isEqual:nil]||[ratingValue isKindOfClass:nil]||[ratingValue isEqualToString:@"nil"]||
        
    }else
    {
        ratingValue = [prefs objectForKey:@"rating"];
    }
    NSString * segmentString = [NSString stringWithFormat:@"%@",[self.segmentDictionary allValues]];
    
    NSString * segmentF = [segmentString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"()"]];
    NSString * segment1 = [segmentF stringByReplacingOccurrencesOfString:@"(" withString:@""];
    NSString * segment2 = [segment1 stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    delegate.segment3 = [segment2 stringByReplacingOccurrencesOfString:@" " withString:@""];
    //NSLog(@"SegmentString:%@",delegate.segment3);
    
    NSString * durationTypeString = [NSString stringWithFormat:@"%@",[self.tradeDictionary allValues]];
    
    
    NSString * durationF = [durationTypeString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"()"]];
    NSString * duration = [durationF stringByReplacingOccurrencesOfString:@"(" withString:@""];
    NSString * duration2 = [duration stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    delegate.duration3 = [duration2 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    //NSLog(@"DurationType:%@",delegate.duration3);
    
    delegate.rating = [NSString stringWithFormat:@"%@",ratingValue];
    
    //NSLog(@"Rating:%@",delegate.rating);
    
    if([delegate.rating isEqualToString:@"0"])
    {
        delegate.rating=@"";
    }
    
    
    @try {
        
        
            
            [prefs setObject:delegate.duration3 forKey:@"dur"];
            
            
            
            
            
            [prefs synchronize];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    @try {
        
        
        
        [prefs setObject:delegate.segment3 forKey:@"segment"];
        
        
        
        
        
        [prefs synchronize];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

    
    @try {
        
        if(ratingValue.length>0)
        {
            [prefs setObject:delegate.rating forKey:@"rating"];
            [prefs synchronize];
            
        }else
        {
           [prefs setObject:[prefs objectForKey:@"rating"] forKey:@"rating"];
            [prefs synchronize];
        
        }
        
        
        
        
        
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    


    
    
    
    

    
    
    
    
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
    

    
    
}
- (IBAction)equitiesButtonAction:(id)sender {
    
    if(self.equityButtonOutlet.selected==NO)
    {
        self.equityButtonOutlet.selected=YES;
        self.eqCheckFlag=false;
        [self.segmentDictionary setObject:[NSNumber numberWithInt:1] forKey:@"equity"];
        
    }else if(self.equityButtonOutlet.selected==YES)
    {
        self.equityButtonOutlet.selected=NO;
        self.eqCheckFlag=true;
        [self.segmentDictionary removeObjectForKey:@"equity"];
        
    }

}
@end
