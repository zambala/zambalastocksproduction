//
//  NewExploreMonksTableViewCell.h
//  PremiumServices
//
//  Created by zenwise mac 2 on 7/13/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewExploreMonksTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *leaderName;
@property (weak, nonatomic) IBOutlet UILabel *subscriBerCount;

@property (weak, nonatomic) IBOutlet UIImageView *profileImg;
@property (strong, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *serviceCountLbl;

@end
