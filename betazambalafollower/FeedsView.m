//
//  FeedsView.m
//  testing
//
//  Created by zenwise mac 2 on 12/14/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "FeedsView.h"
//#import "HHSlideView.h"
//#import "NewsFeed.h"
#import "HMSegmentedControl.h"
#import "FeedCollectionViewCell1.h"
#import "FeedCollectionViewCell2.h"
#import "FeedCollectionViewCell3.h"
#import "FeedCollectionViewCell4.h"
#import "FeedCollectionViewCell5.h"
#import "FeedCollectionViewCell6.h"
#import "FeedCollectionViewCell7.h"
#import "FeedCollectionViewCell8.h"
#import "TwitterView.h"
#import <Mixpanel/Mixpanel.h>


#import "MonksFeedCell1.h"
#import "MonksFeedCell2.h"
#import "MonksFeedCell3.h"
#import "NewsFeedDetail.h"

#import "AppDelegate.h"
#import "Reachability.h"
#import "FeedListDetail.h"



#import "XMLReader.h"

#import "FeedListView.h"
#import "sample.h"



@interface FeedsView ()
{
    BOOL check;
    AppDelegate * delegate;
    NSMutableDictionary * openingBellDict;
    NSMutableDictionary * marketPulse;
    NSMutableDictionary * corporateresults;
    NSMutableDictionary * movers;
    NSMutableDictionary * analysis;
    NSMutableDictionary * closingBell;
    NSMutableDictionary * economy;
    NSMutableDictionary * ipoAnalysis;
    HMSegmentedControl *segmentedControl;
    
    
}

@end

@implementation FeedsView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    self.imageTextFilterArray = [[NSMutableArray alloc]init];
    
    self.navigationItem.title = @"Feeds";
    
   self.collectionViewScroll.scrollEnabled=YES;
    self.collectionView1.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    self.collectionView1.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.collectionView1.layer.shadowOpacity = 1.0f;
    self.collectionView1.layer.shadowRadius = 1.0f;
    self.collectionView1.layer.cornerRadius=0.0f;
    self.collectionView1.layer.masksToBounds = YES;
    
    self.collectionView2.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    self.collectionView2.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.collectionView2.layer.shadowOpacity = 1.0f;
    self.collectionView2.layer.shadowRadius = 1.0f;
    self.collectionView2.layer.cornerRadius=0.0f;
    self.collectionView2.layer.masksToBounds = YES;
    
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"SOCIAL FEED",@"EXPERTS FEED"]];
    segmentedControl.frame = CGRectMake(0, -10, self.view.frame.size.width, 46.5);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
//    segmentedControl.verticalDividerEnabled = YES;
//    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(251/255.0) green:(196/255.0) blue:(12/255.0) alpha:1];
    segmentedControl.backgroundColor = [UIColor colorWithRed:2/255.0 green:30/255.0 blue:41/255.0 alpha:1.0f];
    [segmentedControl setTintColor:[UIColor whiteColor]];
//    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
        [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segmentedControl];
    
  [self.activityIndicator startAnimating];

//    [self.collectionView1 setHidden:NO];
//    [self.collectionView2 setHidden:NO];
//
//    [self.cellTitleView1 setHidden:NO];
//    [self.cellTitleView2 setHidden:NO];
   
    
    self.navigationItem.leftBarButtonItem = nil;
    [_feedContentView setHidden:NO];
    
    
    [_monksContentView setHidden:YES];
  
    
    //    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ClickTap)];
    //    [self.view addGestureRecognizer:gestureRecognizer];
    //    gestureRecognizer.cancelsTouchesInView = NO;
    
    
    [self.view addSubview:self.collectionViewScroll];
    [self.collectionViewScroll addSubview:_collectionView1];
    [self.collectionViewScroll addSubview:_collectionView2];
    [self.collectionViewScroll addSubview:_collectionView3];
    [self.collectionViewScroll addSubview:_collectionView4];
    [self.collectionViewScroll addSubview:_collectionView5];
    [self.collectionViewScroll addSubview:_collectionView6];
    [self.collectionViewScroll addSubview:_collectionView7];

    [self.collectionViewScroll addSubview:_collectionView8];
    
  
    //    [self.view addSubview:self.monksTbl];
    
    self.collectionView7.pagingEnabled = YES;
    self.collectionView8.pagingEnabled = YES;
    
    check=true;
    //[segmentedControl setSelectedSegmentIndex:0];
    //[self segmentedControlChangedValue:segmentedControl];
 
   }


-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    
    
}

-(BOOL)networkStatus
{
    
    
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
    }
    return YES;
    
    
}


-(void)viewDidAppear:(BOOL)animated


{
    
   
    
     Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    
     [mixpanelMini identify:delegate.userID];
    
     [mixpanelMini.people set:@{@"zambalatv":@"NO"}];
    [mixpanelMini track:@"feeds_page"];
    
    if(segmentedControl.selectedSegmentIndex==0)
    {
    TwitterView * tweetView = [[TwitterView alloc]init];
    // SampleViewController * sample = [[SampleViewController alloc] //initWithNibName:@"SampleViewController" bundle:nil];
    [self displayContentController:tweetView];
    }
//
    [self networkStatus];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    
//    self.socialFeedView.hidden=NO;
    
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
    
    if(segmentedControl.selectedSegmentIndex==2)
    {
    if(check==true)
    {
     self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    [self.collectionViewScroll setContentSize:CGSizeMake(self.collectionViewScroll.frame.size.width, self.collectionView6.frame.origin.y+self.collectionView6.frame.size.height+50)];
    
    videoImageArray = [[NSMutableArray alloc]initWithObjects:@"news.jpg", nil];
    
    detailNameArray = [[NSMutableArray alloc]init];
    subSecNameArray = [[NSMutableArray alloc]init];
    captionNameArray = [[NSMutableArray alloc]init];
    headingNameArray = [[NSMutableArray alloc]init];
    timeArray = [[NSMutableArray alloc]init];
    descriptionArray = [[NSMutableArray alloc]init];
    
    detailNameArray2 = [[NSMutableArray alloc]init];
    timeArray2 = [[NSMutableArray alloc]init];
    descriptionArray2 = [[NSMutableArray alloc]init];
    
    detailNameArray3 = [[NSMutableArray alloc]init];
    timeArray3 = [[NSMutableArray alloc]init];
    descriptionArray3 = [[NSMutableArray alloc]init];
    
    detailNameArray4 = [[NSMutableArray alloc]init];
    timeArray4 = [[NSMutableArray alloc]init];
    descriptionArray4 = [[NSMutableArray alloc]init];
    
    detailNameArray5 = [[NSMutableArray alloc]init];
    timeArray5 = [[NSMutableArray alloc]init];
    descriptionArray5 = [[NSMutableArray alloc]init];
    
    detailNameArray6 = [[NSMutableArray alloc]init];
    timeArray6 = [[NSMutableArray alloc]init];
    descriptionArray6 = [[NSMutableArray alloc]init];
    
    
  
    
    imgArray = [NSMutableArray arrayWithObjects:@"img1.jpeg",@"img2.jpeg", nil];
    
    imgArray1 = [NSMutableArray arrayWithObjects:@"image4.jpeg",@"img1.jpeg",@"image5.jpeg",@"img2.jpeg",@"image3.jpg",@"img1.jpeg",@"img2.jpeg",@"image4.jpeg",@"img1.jpeg",@"image5.jpeg",@"img2.jpeg",@"image3.jpg",@"img1.jpeg",@"img2.jpeg",@"image4.jpeg",@"img1.jpeg",@"image5.jpeg",@"img2.jpeg",@"image3.jpg",@"img1.jpeg",@"img2.jpeg",@"img1.jpeg", nil];
    
    //
    imgArray2 = [[NSMutableArray alloc]initWithObjects:@"userimage.png", nil];
    arrImage2 = [[NSMutableArray alloc]initWithObjects:@"image.jpeg", nil];
    
    
    feeds = [[NSMutableArray alloc] init];
    

    
        if([delegate.profileTOFeeds isEqualToString:@"YES"])
        {
            delegate.profileTOFeeds=@"r";
            [segmentedControl setSelectedSegmentIndex:1];
            
            
            
            [self segmentedControlChangedValue:segmentedControl];
            
            
        }
        
        else
        {
            [self.collectionViewScroll setHidden:NO];
            [self.monksTbl setHidden:YES];
            
//             [segmentedControl setSelectedSegmentIndex:0];
            [self serverhit];
            [self serverhit1];
            [self serverhit2];
            [self serverhit3];
            [self serverhit4];
            [self serverhit5];
            [self serverhit6];
            [self serverhit7];
            
        }
    }
    }
    
    if([delegate.pushNotificationSection isEqualToString:@"ZambalaTv"] ||[delegate.pushNotificationSection isEqualToString:@"EXPERTFEED"] )
    {
        
        delegate.pushNotificationSection=@"";
        [segmentedControl setSelectedSegmentIndex:1];
        [self segmentedControlChangedValue:segmentedControl];
    }else if ([delegate.pointsNavigation isEqualToString:@"watch zambala tv"])
    {
        delegate.pointsNavigation=@"";
        [segmentedControl setSelectedSegmentIndex:1];
        [self segmentedControlChangedValue:segmentedControl];
    }
    
    
    if([delegate.pushNotificationSection isEqualToString:@"SOCIALFEED"])
    {
        delegate.pushNotificationSection =@"";
        [segmentedControl setSelectedSegmentIndex:0];
        [self segmentedControlChangedValue:segmentedControl];
        
    }
    
    
}

- (void) displayContentController: (UIViewController*) content;
{
    [self addChildViewController:content];                 // 1
    content.view.bounds = self.socialFeedView.bounds;                 //2
    [self.socialFeedView addSubview:content.view];
    [content didMoveToParentViewController:self];          // 3
}


-(void)serverhit
{
    
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               @"postman-token": @"5a9e1b59-8d1b-4d16-4363-e8e6f8ebb076" };
    
  

    
    NSString * url=[NSString stringWithFormat:@"http://news.accordwebservices.com/News/GetNewsSection?Top=&PageNo=1&PageSize=200&SecId=4&SubSecId=38&NewsID=&FromDate=&ToDate=&Fincode=&token=Qva3eKigDe1_jmSec2WYOWIWZt_F7l4m"];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        openingBellDict=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        //NSLog(@"%@",openingBellDict);
                                                        
                                                        
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        self.collectionView1.delegate=self;
                                                        self.collectionView1.dataSource=self;
                                                        [self.collectionView1 reloadData];
                                                        
                                                        
                                                       
                                                    });

                                                }];
    [dataTask resume];
    
}

-(void)serverhit1
{
    
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                                };
    
    
    NSString * url=[NSString stringWithFormat:@"http://news.accordwebservices.com/News/GetNewsSection?Top=&PageNo=1&PageSize=200&SecId=5&SubSecId=56&NewsID=&FromDate=&ToDate=&Fincode=&token=Qva3eKigDe1_jmSec2WYOWIWZt_F7l4m"];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        marketPulse=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        //NSLog(@"%@",openingBellDict);
                                                        
                                                        
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        self.collectionView2.delegate=self;
                                                        self.collectionView2.dataSource=self;
                                                        [self.collectionView2 reloadData];
                                                        
                                                                                                                
                                                    });
                                                    
                                                }];
    [dataTask resume];
    
}

-(void)serverhit2
{
    
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               };
    
    
    NSString * url=[NSString stringWithFormat:@"http://news.accordwebservices.com/News/GetNewsSection?Top=&PageNo=1&PageSize=200&SecId=7&SubSecId=15&NewsID=&FromDate=&ToDate=&Fincode=&token=Qva3eKigDe1_jmSec2WYOWIWZt_F7l4m"];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        corporateresults=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        //NSLog(@"%@",openingBellDict);
                                                        
                                                        
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        self.collectionView3.delegate=self;
                                                        self.collectionView3.dataSource=self;
                                                        [self.collectionView3 reloadData];
                                                        
                                                       
                                                        
                                                        
                                                    });
                                                    
                                                }];
    [dataTask resume];
    
}

-(void)serverhit3
{
    
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               };
    
    
    NSString * url=[NSString stringWithFormat:@"http://news.accordwebservices.com/News/GetNewsSection?Top=&PageNo=1&PageSize=200&SecId=4&SubSecId=27&NewsID=&FromDate=&ToDate=&Fincode=&token=Qva3eKigDe1_jmSec2WYOWIWZt_F7l4m"];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        movers=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        //NSLog(@"%@",openingBellDict);
                                                        
                                                        
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        self.collectionView5.delegate=self;
                                                        self.collectionView5.dataSource=self;
                                                        [self.collectionView5 reloadData];
                                                        
                                                        
                                                        
                                                        
                                                    });
                                                    
                                                }];
    [dataTask resume];
    
}

-(void)serverhit4
{
    
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               };
    
    
//    NSString * url=[NSString stringWithFormat:@"http://news.accordwebservices.com/News/GetNewsSection?Top=&PageNo=1&PageSize=200&SecId=1&SubSecId=1&NewsID=&FromDate=&ToDate=&Fincode=&token=Qva3eKigDe1_jmSec2WYOWIWZt_F7l4m"];
    
     NSString * url=[NSString stringWithFormat:@"http://news.accordwebservices.com/News/GetNewsSection?Top=&PageNo=1&PageSize=200&SecId=4&SubSecId=42&NewsID=&FromDate=&ToDate=&Fincode=&token=Qva3eKigDe1_jmSec2WYOWIWZt_F7l4m"];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        analysis=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        //NSLog(@"%@",openingBellDict);
                                                        
                                                        
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        self.collectionView6.delegate=self;
                                                        self.collectionView6.dataSource=self;
                                                        [self.collectionView6 reloadData];
                                                        
                                                        
                                                    });
                                                    
                                                }];
    [dataTask resume];
    
}


-(void)serverhit5
{
    
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               };
    
    
    NSString * url=[NSString stringWithFormat:@"http://news.accordwebservices.com/News/GetNewsSection?Top=&PageNo=1&PageSize=200&SecId=4&SubSecId=47&NewsID=&FromDate=&ToDate=&Fincode=&token=Qva3eKigDe1_jmSec2WYOWIWZt_F7l4m"];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        closingBell=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        //NSLog(@"%@",openingBellDict);
                                                        
                                                        
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        self.collectionView4.delegate=self;
                                                        self.collectionView4.dataSource=self;
                                                        [self.collectionView4 reloadData];
                                                        
                                                        
                                                        
                                                    });
                                                    
                                                }];
    [dataTask resume];
    
}

-(void)serverhit6
{
    
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               };
    
    
    NSString * url=[NSString stringWithFormat:@"http://news.accordwebservices.com/News/GetNewsSection?Top=&PageNo=1&PageSize=200&SecId=5&SubSecId=23,24,25,26,36,43,44,49&NewsID=&FromDate=&ToDate=&Fincode=&token=Qva3eKigDe1_jmSec2WYOWIWZt_F7l4m"];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        economy=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        //NSLog(@"%@",economy);
                                                        
                                                        
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        self.collectionView7.delegate=self;
                                                        self.collectionView7.dataSource=self;
                                                        [self.collectionView7 reloadData];
                                                        
                                                       
                                                        
                                                        
                                                    });
                                                    
                                                }];
    [dataTask resume];
    
}

-(void)serverhit7
{
    
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               };
    
    
    NSString * url=[NSString stringWithFormat:@"http://news.accordwebservices.com/News/GetNewsSection?Top=&PageNo=1&PageSize=200&SecId=1&SubSecId=1&NewsID=&FromDate=&ToDate=&Fincode=&token=Qva3eKigDe1_jmSec2WYOWIWZt_F7l4m"];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        ipoAnalysis=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        //NSLog(@"%@",ipoAnalysis);
                                                        
                                                        
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        self.collectionView8.delegate=self;
                                                        self.collectionView8.dataSource=self;
                                                        [self.collectionView8 reloadData];
                                                        
                                                        [self.activityIndicator performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:YES];
                                                        self.activityIndicator.hidden=YES;
                                                        
                                                        
                                                    });
                                                    
                                                }];
    [dataTask resume];
    
}








- (IBAction)segmentedControlChangedValue:(HMSegmentedControl *)sender
{
    NSInteger selectedSegment = sender.selectedSegmentIndex;
     Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    
    if (selectedSegment == 2)
    {
        [mixpanelMini track:@"news_feed_page"];
        self.collectionViewScroll.hidden=NO;
        self.monksTbl.hidden=YES;
         self.socialFeedView.hidden=YES;
        [_feedContentView setHidden:NO];
        _feedContentView.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        
        
//        [_monksContentView setHidden:YES];
        
         check=true;
      
    }
    
    if (selectedSegment == 0)
    {
       
        [mixpanelMini track:@"social_feed_page"];
        self.collectionViewScroll.hidden=YES;
        self.monksTbl.hidden=YES;
        self.socialFeedView.hidden=NO;
        [_feedContentView setHidden:YES];
        _feedContentView.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        
        
        //        [_monksContentView setHidden:YES];
       
        
                TwitterView * tweetView = [[TwitterView alloc]init];
                // SampleViewController * sample = [[SampleViewController alloc] //initWithNibName:@"SampleViewController" bundle:nil];
        
                [self displayContentController:tweetView];
        
        check=true;
        
    }
    else if (selectedSegment == 1)
    {
         [mixpanelMini track:@"monks_feed_page"];
        self.collectionViewScroll.hidden=YES;
        self.monksTbl.hidden=NO;
        self.socialFeedView.hidden=YES;
        [_feedContentView setHidden:YES];
       
        _monksContentView.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        
        check=false;
       
//        [_monksContentView setHidden:NO];
        [_monksTbl setHidden:NO];
       
        _desStr2 = @"The outlook is a positive start. The concerns and fears regarding the Italian referendum have taken a backseat for now  Wall Street rose on Monday with the Dow hitting recordhighs. Data showed further strength in the US";
        
        nameString2 = @"Neeraj Goswami";
        
        timeString2 = @"Today 12.33 pm";
        
         [self monksFeedServer];
//        [_monksTbl reloadData];
        


    }
    
}

- (void)ClickTap
{
    [actionSheet removeFromSuperview];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



//----CollectionView Delegate----//

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (collectionView == _collectionView1)
    {
        return 1;
    }
    else if (collectionView == _collectionView2)
    {
        return 1;
 
    }
    else if (collectionView == _collectionView3)
    {
        return 1;
        
    }
    else if (collectionView == _collectionView4)
    {
        return 1;
        
    }
    else if (collectionView == _collectionView5)
    {
        return 1;
        
    }
    else if (collectionView == _collectionView6)
    {
        return 1;
        
    }

    else if (collectionView == _collectionView7)
    {
        return 1;
        
    }

    else if (collectionView == _collectionView8)
    {
        return 1;
        
    }

        return 0;
        
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == _collectionView1)
    {
        return [[openingBellDict objectForKey:@"Table" ]count];
//        return 5;
    }
    else if (collectionView == _collectionView2)
    {
        return [[marketPulse objectForKey:@"Table" ]count];
//        return 5;
        
    }
    else if (collectionView == _collectionView3)
    {
        return [[corporateresults objectForKey:@"Table" ]count];
//        return 5;
    }
    else if (collectionView == _collectionView4)
    {
        return [[closingBell objectForKey:@"Table" ]count];
//        return 5;
    }
    
    else if (collectionView == _collectionView5)
    {
        return [[movers objectForKey:@"Table" ]count];
//        return 5;
    }
    
    else if (collectionView == _collectionView6)
    {
        return [[analysis objectForKey:@"Table" ]count];
//        return 5;
    }
    
    else if (collectionView == _collectionView7)
    {
        return [[economy objectForKey:@"Table" ]count];
        //        return 5;
    }

    
    else if (collectionView == _collectionView8)
    {
        return [[ipoAnalysis objectForKey:@"Table" ]count];
        //        return 5;
    }



    return 0;

   
}

-(void)monksFeedServer
{
    
    NSDictionary *headers = @{ 
                               @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"authtoken":delegate.zenwiseToken,
                                  @"deviceid":delegate.currentDeviceId
                                };
    
    
    
    NSString * str=[NSString stringWithFormat:@"%@follower/advices?clientid=%@&messagetypeid=3",delegate.baseUrl,delegate.userID];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:str]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==403)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                            
                                                        }
                                                        else if([httpResponse statusCode]==401)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                        self.monksFeedResponseArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        //NSLog(@"MonksFeedResponseArray:%@",self.monksFeedResponseArray);
                                                        }
                                                       
                                                    }
                                                    
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        _monksTbl.delegate = self;
                                                        _monksTbl.dataSource = self;
                                                        [self.monksTbl reloadData];                                 });
                                                }];
    [dataTask resume];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == _collectionView1)
    {
        FeedListDetail *view = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedListDetail"];
        
        [self.navigationController pushViewController:view animated:YES];
   
    view.descriptionString = [[[openingBellDict objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Heading"];
    view.timeString = [[[openingBellDict objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Newsdate"];
    view.detailDesString = [[[openingBellDict objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Details"];
    }
    
    else if (collectionView == _collectionView2)
    {
        FeedListDetail *view = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedListDetail"];
        
        [self.navigationController pushViewController:view animated:YES];
        
        view.descriptionString = [[[marketPulse objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Heading"];
        view.timeString = [[[marketPulse objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Newsdate"];
        view.detailDesString = [[[marketPulse objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Details"];
    }
    
    else if (collectionView == _collectionView3)
    {
        FeedListDetail *view = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedListDetail"];
        
        [self.navigationController pushViewController:view animated:YES];
        
        view.descriptionString = [[[corporateresults objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Heading"];
        view.timeString = [[[corporateresults objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Newsdate"];
        view.detailDesString = [[[corporateresults objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Details"];
    }
    
    else if (collectionView == _collectionView4)
    {
        FeedListDetail *view = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedListDetail"];
        
        [self.navigationController pushViewController:view animated:YES];
        
        view.descriptionString = [[[closingBell objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Heading"];
        view.timeString = [[[closingBell objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Newsdate"];
        view.detailDesString = [[[closingBell objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Details"];
    }
    
    
    else if (collectionView == _collectionView5)
    {
        FeedListDetail *view = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedListDetail"];
        
        [self.navigationController pushViewController:view animated:YES];
        
        view.descriptionString = [[[movers objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Heading"];
        view.timeString = [[[movers objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Newsdate"];
        view.detailDesString = [[[movers objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Details"];
    }
    
    else if (collectionView == _collectionView6)
    {
        FeedListDetail *view = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedListDetail"];
        
        [self.navigationController pushViewController:view animated:YES];
        
        view.descriptionString = [[[ipoAnalysis objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Heading"];
        view.timeString = [[[ipoAnalysis objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Newsdate"];
        view.detailDesString = [[[ipoAnalysis objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Details"];
    }
    
    else if (collectionView == _collectionView7)
    {
        FeedListDetail *view = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedListDetail"];
        
        [self.navigationController pushViewController:view animated:YES];
        
        view.descriptionString = [[[economy objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Heading"];
        view.timeString = [[[analysis objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Newsdate"];
        view.detailDesString = [[[analysis objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Details"];
    }

    
    else if (collectionView == _collectionView8)
    {
        FeedListDetail *view = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedListDetail"];
        
        [self.navigationController pushViewController:view animated:YES];
        
        view.descriptionString = [[[analysis objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Heading"];
        view.timeString = [[[analysis objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Newsdate"];
        view.detailDesString = [[[analysis objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Details"];
    }




    
    

    
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView == _collectionView1)
    {
        static NSString *cellIdentifier = @"Cell11";
        
        

        
        FeedCollectionViewCell1 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        cell.descripLbl.text = [[[openingBellDict objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Heading"];
        cell.timeLbl.text = [[[openingBellDict objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Newsdate"];
//        cell.imgView.image = [UIImage imageNamed:[imgArray1 objectAtIndex:indexPath.row]];
        
       
       
        

        return cell;

    }
      else if (collectionView == _collectionView2)
    {
        static NSString *cellIdentifier = @"Cell21";
        
        FeedCollectionViewCell2 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        cell.descripLbl2.text = [[[marketPulse objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Heading"];
        cell.timeLbl2.text = [[[marketPulse objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Newsdate"];
//        cell.imgView.image = [UIImage imageNamed:[imgArray1 objectAtIndex:indexPath.row]];
        cell.imgView.image = [UIImage imageNamed:@"newsimages1.jpeg"];

        
        return cell;
        
    }
    
      else if (collectionView == _collectionView3)
    {
        static NSString *cellIdentifier = @"Cell31";
        
        FeedCollectionViewCell3 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        cell.descripLbl3.text = [[[corporateresults objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Heading"];
        cell.timeLbl3.text = [[[corporateresults objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Newsdate"];
//        cell.imgView.image = [UIImage imageNamed:[imgArray1 objectAtIndex:indexPath.row]];
//        cell.imgView.image = [UIImage imageNamed:@"newsimages.jpeg"];

        return cell;
        
    }

      else if (collectionView == _collectionView4)
      {
          static NSString *cellIdentifier = @"Cell41";
          
          FeedCollectionViewCell4 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
          
          cell.descripLbl4.text = [[[closingBell objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Heading"];
          cell.timeLbl4.text = [[[closingBell objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Newsdate"];
//          cell.imgView.image = [UIImage imageNamed:[imgArray1 objectAtIndex:indexPath.row]];
          cell.imgView.image = [UIImage imageNamed:@"newsimages1.jpeg"];

          return cell;
          
      }
    
      else if (collectionView == _collectionView5)
      {
          static NSString *cellIdentifier = @"Cell51";
          
          FeedCollectionViewCell5 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
          
          cell.descripLbl5.text = [[[movers objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Heading"];
          cell.timeLbl5.text = [[[movers objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Newsdate"];//          cell.imgView.image = [UIImage imageNamed:[imgArray2 objectAtIndex:indexPath.row]];
          cell.imgView.image = [UIImage imageNamed:@"newsimages.jpeg"];

          return cell;
          
      }
      else if (collectionView == _collectionView6)
      {
          static NSString *cellIdentifier = @"Cell61";
          
          FeedCollectionViewCell6 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
          
          cell.descripLbl6.text = [[[ipoAnalysis objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Heading"];
          cell.timeLbl6.text = [[[ipoAnalysis objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Newsdate"];
//        cell.imgView.image = [UIImage imageNamed:[imgArray2 objectAtIndex:indexPath.row]];
          cell.imgView.image = [UIImage imageNamed:@"newsimages1.jpeg"];

          return cell;
          
      }
    
      else if (collectionView == _collectionView7)
      {
          static NSString *cellIdentifier = @"CELL7";
          
          FeedCollectionViewCell7 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
          
          cell.headingLbl.text = [[[economy objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Heading"];
          cell.timeLbl.text = [[[economy objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Newsdate"];
          //        cell.imgView.image = [UIImage imageNamed:[imgArray2 objectAtIndex:indexPath.row]];
          
          
          return cell;
          
      }
    
      else if (collectionView == _collectionView8)
      {
          static NSString *cellIdentifier = @"CELL8";
          
          FeedCollectionViewCell8 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
          
          cell.heading.text = [[[analysis objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Heading"];
          cell.timeLbl.text = [[[analysis objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Newsdate"];
          //        cell.imgView.image = [UIImage imageNamed:[imgArray2 objectAtIndex:indexPath.row]];
          
          
          return cell;
          
      }



    return nil;
    
}



- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView == _collectionView1)
    {
        return CGSizeMake(self.view.frame.size.width/2, 80);

    }
    else if (collectionView == _collectionView2)
    {
       return CGSizeMake(self.view.frame.size.width/2, 80);

    }
    else if (collectionView == _collectionView3)
    {
         return CGSizeMake(self.view.frame.size.width/2, 80);
        
    }
    else if (collectionView == _collectionView4)
    {
         return CGSizeMake(self.view.frame.size.width/2, 80);
        
    }
    else if (collectionView == _collectionView5)
    {
         return CGSizeMake(self.view.frame.size.width/2, 80);
        
    }
    else if (collectionView == _collectionView6)
    {
         return CGSizeMake(self.view.frame.size.width/2, 80);
        
    }
    else if (collectionView == _collectionView7)
    {
        return CGSizeMake(self.view.frame.size.width/2, 80);
        
    }
    else if (collectionView == _collectionView8)
    {
        return CGSizeMake(self.view.frame.size.width/2, 80);
        
    }
    return CGSizeZero;

}

-(IBAction)moreBtn:(id)sender
{
    //NSLog(@"sender----%@", sender);
    
    FeedListView *next = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedListView"];
    [self.navigationController pushViewController:next animated:YES];
    
    delegate.detailNewsDict=[[NSMutableDictionary alloc]init];
  
//    next.descriptionArr = descriptionArray;
//    next.imageArray = imgArray;
//    next.timeArray = timeArray;
//    next.detailArray = detailNameArray;
    
    delegate.detailNewsDict=openingBellDict;
    
}

-(IBAction)moreBtn2:(id)sender
{
    //NSLog(@"sender----%@", sender);
    
    
    FeedListView *next = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedListView"];
    [self.navigationController pushViewController:next animated:YES];
    
//    next.descriptionArr = descriptionArray2;
//    next.imageArray = imgArray;
//    next.timeArray = timeArray2;
//    next.detailArray = detailNameArray2;
     delegate.detailNewsDict=[[NSMutableDictionary alloc]init];
    
   
    
    delegate.detailNewsDict=marketPulse;


}

-(IBAction)moreBtn3:(id)sender
{
    //NSLog(@"sender----%@", sender);
    
    
    FeedListView *next = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedListView"];
    [self.navigationController pushViewController:next animated:YES];
    
//    next.descriptionArr = descriptionArray3;
//    next.imageArray = imgArray1;
//    next.timeArray = timeArray3;
//    next.detailArray = detailNameArray3;
    
     delegate.detailNewsDict=[[NSMutableDictionary alloc]init];
        delegate.detailNewsDict=corporateresults;

    
    
}

-(IBAction)moreBtn4:(id)sender
{
    //NSLog(@"sender----%@", sender);
    
    
    FeedListView *next = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedListView"];
    [self.navigationController pushViewController:next animated:YES];
    
//    next.descriptionArr = descriptionArray4;
//   next.imageArray = imgArray1;
//    next.timeArray = timeArray4;
//    next.detailArray = detailNameArray4;
    
     delegate.detailNewsDict=[[NSMutableDictionary alloc]init];
    
    //    next.descriptionArr = descriptionArray;
    //    next.imageArray = imgArray;
    //    next.timeArray = timeArray;
    //    next.detailArray = detailNameArray;
    
    delegate.detailNewsDict=closingBell;

    
    
}

-(IBAction)moreBtn5:(id)sender
{
    //NSLog(@"sender----%@", sender);
    
    
    FeedListView *next = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedListView"];
    [self.navigationController pushViewController:next animated:YES];
    
//    next.descriptionArr = descriptionArray5;
//    next.imageArray = imgArray2;
//    next.timeArray = timeArray5;
//    next.detailArray = detailNameArray5;
    
     delegate.detailNewsDict=[[NSMutableDictionary alloc]init];
    
    //    next.descriptionArr = descriptionArray;
    //    next.imageArray = imgArray;
    //    next.timeArray = timeArray;
    //    next.detailArray = detailNameArray;
    
    delegate.detailNewsDict=movers;

    
    
}

-(IBAction)moreBtn6:(id)sender
{
    //NSLog(@"sender----%@", sender);
    
    
    FeedListView *next = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedListView"];
    [self.navigationController pushViewController:next animated:YES];
    
//    next.descriptionArr = descriptionArray6;
//    next.imageArray = imgArray2;
//    next.timeArray = timeArray6;
//    next.detailArray = detailNameArray6;
    
    delegate.detailNewsDict=[[NSMutableDictionary alloc]init];
    
    //    next.descriptionArr = descriptionArray;
    //    next.imageArray = imgArray;
    //    next.timeArray = timeArray;
    //    next.detailArray = detailNameArray;
    
    delegate.detailNewsDict=ipoAnalysis;

    
    
}



//----TableView Delegate----//

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2 ;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0)
    {
        return 1; ;
    }
    else if(section == 1)
    {
        return [[self.monksFeedResponseArray objectForKey:@"data"] count];
    }
//    else if(section == 2)
//    {
//        return [self.monksFeedResponseArray count];
//    }
    return 0;
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    if(section == 0)
//        return @"Section 1";
//    else
//        return @"Section 2";
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    
//    static NSString *CellIdentifier  = @"cell1";
//    static NSString *CellIdentifier2 = @"cell2";
//    static NSString *CellIdentifier3 = @"cell3";

    
    if (indexPath.section==0)
           {
        MonksFeedCell1 *cell = [self.monksTbl dequeueReusableCellWithIdentifier:@"cell1" forIndexPath:indexPath];
//
//        cell.imgVideo1.image = [UIImage imageNamed:[videoImageArray objectAtIndex:0]];
//
//        cell.textLabel.text=@"Zambala TV";
//
//        cell.timeLbl1.text=@"";
//
//               cell.desLbl1.text=@"You can watch your Monk live here.";
//
//
        return cell;
        
    }
//    else if (indexPath.section == 1)
//        if (indexPath.row == 0)
//        {
//        MonksFeedCell2 *cell = [self.monksTbl dequeueReusableCellWithIdentifier:@"cell2" forIndexPath:indexPath];
//        
////        cell.imgView2.image = [UIImage imageNamed:[imgArray2 objectAtIndex:indexPath.row]];
//        cell.nameLbl2.text  = nameString2;
//        cell.timeLbl2.text  = timeString2;
//        cell.desLbl2.text  = _desStr2;
//        
//        return cell;
//        
//        }
//    if (indexPath.section == 2)
//       if (indexPath.row == 0)
//        {
//            MonksFeedCell3 *cell = [self.monksTbl dequeueReusableCellWithIdentifier:@"cell3" forIndexPath:indexPath];
//            
////            cell.imgView3.image = [UIImage imageNamed:[imgArray2 objectAtIndex:indexPath.row]];
//            cell.nameLbl3.text  = nameString2;
//            cell.timeLbl3.text  = timeString2;
//            cell.desLbl3.text  = _desStr2;
//            cell.arrImage.image = [UIImage imageNamed:[arrImage2 objectAtIndex:indexPath.row]];
//
//            
//            return cell;
//        }
    if(indexPath.section==1)
    {
    if([[[[self.monksFeedResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"messageurl"]isEqual:[NSNull null]])
    {
        MonksFeedCell2 *cell = [self.monksTbl dequeueReusableCellWithIdentifier:@"cell2" forIndexPath:indexPath];
        
    //        cell.imgView2.image = [UIImage imageNamed:[imgArray2 objectAtIndex:indexPath.row]];
        
        NSString * monkName = [[[self.monksFeedResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"firstname"];
                cell.nameLbl2.text  = monkName;
        
        NSString * time = [[[self.monksFeedResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"createdon"];
                cell.timeLbl2.text  = time;
        
        NSString * message = [[[self.monksFeedResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"message"];
        
    
        
        if([message isEqual:[NSNull null]])
        {
            cell.desLbl2.text=@"";
        }else
        {
            cell.desLbl2.text  = message;
        }
                
                return cell;
    }else
    {
        MonksFeedCell3 *cell = [self.monksTbl dequeueReusableCellWithIdentifier:@"cell3" forIndexPath:indexPath];
        
        //            cell.imgView3.image = [UIImage imageNamed:[imgArray2 objectAtIndex:indexPath.row]];
        
        cell.imageWebView.hidden=YES;
        
        NSString * monkName = [[[self.monksFeedResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"firstname"];
                    cell.nameLbl3.text  = monkName;
        
        NSString * time = [[[self.monksFeedResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"createdon"];
                    cell.timeLbl3.text  = time;
        
        NSString * message = [[[self.monksFeedResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"message"];
        
        if([message isEqual:[NSNull null]])
        {
            cell.desLbl3.text=@"";
        }else
        {
            cell.desLbl3.text  = message;
        }

        NSString * image = [[[self.monksFeedResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"logourl"];
        if([image isEqual:[NSNull null]])
        {
            cell.imgView3.image = [UIImage imageNamed:[arrImage2 objectAtIndex:indexPath.row]];

        }else
        {
            //NSLog(@"Image:%@",image);
//            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:image]];
//            cell.arrImage.image = [UIImage imageWithData:imageData];
            
            
            cell.imgView3.image = nil; // or cell.poster.image = [UIImage imageNamed:@"placeholder.png"];
            
            NSURL *url = [NSURL URLWithString:image];
            
            NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (data) {
                    UIImage *image = [UIImage imageWithData:data];
                    if (image) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            MonksFeedCell3 *updateCell = (id)[tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                updateCell.imgView3.image = image;
                        });
                    }
                }
            }];
            [task resume];
            
            
           
            
//            NSURL *url = [NSURL URLWithString:image];
////            cell.arrImage.image = [UIImage imageWithCIImage:[CIImage imageWithContentsOfURL:url]];
//            NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
//            [cell.imageWebView loadRequest:urlRequest];

           // [cell.imageWebView loadHTMLString:[image description] baseURL:nil];

        
        }
        
        NSString * videoImage = [[[self.monksFeedResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"messageurl"];
        if([videoImage isEqual:[NSNull null]])
        {
            cell.arrImage.image = [UIImage imageNamed:[arrImage2 objectAtIndex:indexPath.row]];
            
        }else
        {
            //NSLog(@"Image:%@",image);
            //            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:image]];
            //            cell.arrImage.image = [UIImage imageWithData:imageData];
            cell.imageWebView.hidden=NO;
            
            cell.arrImage.image = nil; // or cell.poster.image = [UIImage imageNamed:@"placeholder.png"];
           // videoImage=[NSString stringWithFormat:@"https://youtu.be/EKUcOeY3oZQ"];
            NSURL *url = [NSURL URLWithString:videoImage];
            dispatch_async(dispatch_get_main_queue(), ^{
            [cell.imageWebView loadRequest:[NSURLRequest requestWithURL:url]];
            cell.imageWebView.scrollView.bounces=NO;
            [cell.imageWebView setMediaPlaybackRequiresUserAction:NO];
         //   [cell.imageWebView setAllowsInlineMediaPlayback:YES];
            });
            
//            NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//                if (data) {
//                    UIImage *image = [UIImage imageWithData:data];
//                    if (image) {
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            MonksFeedCell3 *updateCell = (id)[tableView cellForRowAtIndexPath:indexPath];
//                            if (updateCell)
//                                updateCell.arrImage.image = image;
//                            [cell.imageWebView loadRequest:[NSURLRequest requestWithURL:url]];
//                            cell.imageWebView.scrollView.bounces=NO;
//                            [cell.imageWebView setMediaPlaybackRequiresUserAction:NO];
//                        });
//                    }
//                }
//            }];
//            [task resume];
            
           
        }
        
        
                    
                    return cell;

    }
    }

    return nil;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        return 188.0f;
    }
    else if (indexPath.section==1)
    {
        if([[[[self.monksFeedResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"messageurl"]isEqual:[NSNull null]])
        {
        return 150;
        }
        else
        {
            return 270;
            
        }
    }
    else if (indexPath.section==2)
    {
        return 353.0f;
    }

    return 50.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
   
   if (indexPath.section == 1)
       
   {
       if (indexPath.row == 0)
      {
          NSString * messageType = [NSString stringWithFormat:@"%@",[[[self.monksFeedResponseArray objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"messagetypeid"]];
          if([messageType isEqualToString:@"3"])
          {
              sample * sampleView = [self.storyboard instantiateViewControllerWithIdentifier:@"sample"];
              sampleView.checkString=@"video";
              sampleView.urlString = [NSString stringWithFormat:@"%@",[[[self.monksFeedResponseArray objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"messageurl"]];
              [self.navigationController pushViewController:sampleView animated:YES];
          }else
          {
          NewsFeedDetail *view = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsFeedDetail"];
          
          [self.navigationController pushViewController:view animated:YES];
         view.nameString = nameString2;
           view.descripString = _desStr2;
            view.imgArray = imgArray2;
            view.timeString = timeString2;
          }
       }
       
   }
   else if (indexPath.section == 2)
    {
        if (indexPath.row == 0)
        {
            
            NewsFeedDetail *view = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsFeedDetail"];
            
            [self.navigationController pushViewController:view animated:YES];
           
            view.nameString = nameString2;
            view.descripString = _desStr2;
            view.imgArray = imgArray2;
            view.timeString = timeString2;
            view.imgArray1 = arrImage2;
        }
        
    }

    else if(indexPath.section==0)
    {
//         sample * sampleView = [self.storyboard instantiateViewControllerWithIdentifier:@"sample"];
//
//        [self.navigationController pushViewController:sampleView animated:YES];
        
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Coming Soon" message:@"Please note that Zambala TV is temporarily under development and will be made available soon." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [segmentedControl setSelectedSegmentIndex:0];
            [self segmentedControlChangedValue:segmentedControl];
        }];
        
        [alert addAction:okAction];
        
        
    });
        
        
    }
    
}





- (IBAction)moreBtn7:(id)sender {
    
    FeedListView *next = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedListView"];
    [self.navigationController pushViewController:next animated:YES];
    
    //    next.descriptionArr = descriptionArray2;
    //    next.imageArray = imgArray;
    //    next.timeArray = timeArray2;
    //    next.detailArray = detailNameArray2;
    delegate.detailNewsDict=[[NSMutableDictionary alloc]init];
    
    
    
    delegate.detailNewsDict=economy;

}

- (IBAction)moreBtn8:(id)sender {
    FeedListView *next = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedListView"];
    [self.navigationController pushViewController:next animated:YES];
    
    //    next.descriptionArr = descriptionArray2;
    //    next.imageArray = imgArray;
    //    next.timeArray = timeArray2;
    //    next.detailArray = detailNameArray2;
    delegate.detailNewsDict=[[NSMutableDictionary alloc]init];
    
    
    
    delegate.detailNewsDict=analysis;

}
  

@end
