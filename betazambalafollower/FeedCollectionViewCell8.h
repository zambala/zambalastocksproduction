//
//  FeedCollectionViewCell8.h
//  betazambalafollower
//
//  Created by zenwise mac 2 on 7/18/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedCollectionViewCell8 : UICollectionViewCell


@property (weak, nonatomic) IBOutlet UILabel *heading;

@property (weak, nonatomic) IBOutlet UILabel *timeLbl;

@end
