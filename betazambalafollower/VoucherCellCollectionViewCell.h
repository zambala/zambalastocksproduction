//
//  VoucherCellCollectionViewCell.h
//  betazambalafollower
//
//  Created by Zenwise Technologies on 17/08/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VoucherCellCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *voucherView;
@property (weak, nonatomic) IBOutlet UIImageView *voucherImageView;
@property (weak, nonatomic) IBOutlet UIView *voucherBackView;
@property (weak, nonatomic) IBOutlet UIView *labelView;

@property (weak, nonatomic) IBOutlet UILabel *voucherValueLbl;
@end
