//
//  MainView.m
//  testing
//
//  Created by zenwise technologies on 21/11/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#ifndef INMOBI_BANNER_PLACEMENT
#define INMOBI_BANNER_PLACEMENT     1487645804150
#endif

#import "MainView.h"
#import "HomePage.h"
#import "AppDelegate.h"
#import "VCFloatingActionButton.h"
#import <QuartzCore/QuartzCore.h>
#import "DGActivityIndicatorView.h"
#import "NewProfileSettings.h"
#import "Reachability.h"
#import "MessageView.h"
#import "NewMessages.h"
#import "TagEncode.h"
#import "MT.h"
#import "PSWebSocket.h"
#import<malloc/malloc.h>
#import "MarketMonksView.h"
//#import "HelpshiftCampaigns.h"
#import "LoginViewController.h"
#import "TabMenuView.h"
#import "BrokerViewNavigation.h"
#import "OpenAccountView.h"

@import Mixpanel;
//@import Tune;
#import "WisdomGrdenView.h"
@import SocketIO;
#import "NewPremimumServicesMainScreen.h"
#import "MainOrdersViewController.h"
#import "WealthCreatorViewController.h"
#import "NewExploreMonksViewController.h"
#import "TabBar.h"
#import "TopPicks.h"
#import "WalletView.h"
#import <Speech/Speech.h>
#import "HelpshiftSupport.h"
#import "HelpshiftCampaigns.h"
#import "TadeView.h"



@interface MainView () <PSWebSocketDelegate,NSStreamDelegate,SocketEngineSpec,SocketManagerSpec,SocketEngineClient,SFSpeechRecognizerDelegate>
{
    AppDelegate * delegate2;/*!<appdelegate instance*/
    DGActivityIndicatorView * activityIndicatorView;/*!<unused*/
    UIVisualEffectView * blurEffectView;/*!<used for blureffect for view*/
    float niftyFloat;/*!<used to store nifty ltp*/
    float niftyChg;/*!<used to store nifty change percentage*/
    float sensexChg;/*!<used to store sensex change percentage*/
    float sensexPrice;/*!<used to store sensex ltp*/
    NSMutableDictionary * clientCreatResponse;/*!<it stores data of user creation response*/
    NSMutableDictionary * brokerResponseDict;/*!<it stores data of broker information*/
    MT * sharedManager;/*!<MT class referance*/
    
    BOOL check;/*!<unused*/
    NSString * brokerid;/*!<it stores brokerid*/
    int packetsNumber;/*!<it stores number of packets coming from socket */
    int packetsLength;/*!<it stores length of packets */
    
    NSString * string1;/*!<message sending to socket in zerodha*/
    NSString * message;/*!<message sending to socket in zerodha*/
    
    SFSpeechRecognizer *speechRecognizer;
    SFSpeechAudioBufferRecognitionRequest *recognitionRequest;
    SFSpeechRecognitionTask *recognitionTask;
    AVAudioEngine *audioEngine;
    BOOL speechNavigation;
    NSString * notiyId;
    NSString * notifyAct;
    NSDictionary * bannerNavigationDict;
}

@end

@implementation MainView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //speech//
    
//    speechRecognizer = [[SFSpeechRecognizer alloc] initWithLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
//
//    // Set speech recognizer delegate
//    speechRecognizer.delegate = self;
//
//    // Request the authorization to make sure the user is asked for permission so you can
//    // get an authorized response, also remember to change the .plist file, check the repo's
//    // readme file or this project's info.plist
//    [SFSpeechRecognizer requestAuthorization:^(SFSpeechRecognizerAuthorizationStatus status) {
//        switch (status) {
//            case SFSpeechRecognizerAuthorizationStatusAuthorized:
//                //NSLog(@"Authorized");
//                break;
//            case SFSpeechRecognizerAuthorizationStatusDenied:
//                //NSLog(@"Denied");
//                break;
//            case SFSpeechRecognizerAuthorizationStatusNotDetermined:
//                //NSLog(@"Not Determined");
//                break;
//            case SFSpeechRecognizerAuthorizationStatusRestricted:
//                //NSLog(@"Restricted");
//                break;
//            default:
//                break;
//        }
//    }];
    
    self.mainBannerBgView.hidden=YES;
    self.bottomBannerView.hidden=YES;
  @try
    {
        
    delegate2=(AppDelegate *)[[UIApplication sharedApplication]delegate];
      
    [self.socketio disconnect];
     delegate2.logoutCheck=@"";
     sharedManager = [MT Mt1];
    delegate2.brokerLoginCheck=true;
    [self.navigationController setNavigationBarHidden:YES animated:YES];
   

    self.mutualFundsLbl.hidden=YES;
    self.usMarketsButton.hidden=YES;
    self.usMarketsView.hidden=YES;
    
    if(delegate2.firstTimeCheck==true)
    {
       
        NSString * localStr=[NSString stringWithFormat:@"(%@)",delegate2.brokerNameStr];
        
        [self.brokerName setTitle:localStr];
        
      
     };
        
        
        if([delegate2.loginActivityStr isEqualToString:@"OTP1"])
        {
            
            
            [self.brokerName setTitle:@"(GUEST)"];
            
            self.openAccountIndia.hidden = NO;
            self.openAcountlbl.hidden=NO;
            
           
            self.usMarketsButton.hidden=NO;
            self.usMarketsView.hidden=NO;
          
          
            
            
            
            
        }else if ([delegate2.loginActivityStr isEqualToString:@"CLIENT"])
        {
         
            
            self.openAccountIndia.hidden = YES;
            self.openAcountlbl.hidden=YES;
            
            
            NSString * localStr=[NSString stringWithFormat:@"(%@)",delegate2.brokerNameStr];
            
            [self.brokerName setTitle:localStr];   
            
           
            
          
             brokerid=[NSString stringWithFormat:@"%@",[delegate2.brokerInfoDict objectForKey:@"brokerid"]];
            
           
            
            
            NSString * usmarketCheck=[NSString stringWithFormat:@"%@",[delegate2.brokerInfoDict objectForKey:@"usmarket"]];
            
            if([usmarketCheck isEqual:[NSNull null]])
            {
                
                usmarketCheck=@"";
            }
     
            
         
            
            if([usmarketCheck isEqualToString:@"1"])
            {
                self.usMarketsButton.hidden=NO;
                self.usMarketsView.hidden=NO;
            }
            
            else
            {
                self.usMarketsButton.hidden=YES;
                self.usMarketsView.hidden=YES;
                
            }
                
                
            
            
        }
        
       
        
        self.tickerImageView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
        self.tickerImageView.layer.shadowOffset = CGSizeMake(0, 2.0f);
        self.tickerImageView.layer.shadowOpacity = 1.0f;
        self.tickerImageView.layer.shadowRadius = 1.0f;
        self.tickerImageView.layer.cornerRadius=4.2f;
        self.tickerImageView.layer.masksToBounds = YES;
        
        self.marketmonksBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
        self.marketmonksBtn.layer.shadowOffset = CGSizeMake(0, 2.0f);
        self.marketmonksBtn.layer.shadowOpacity = 1.0f;
        self.marketmonksBtn.layer.shadowRadius = 1.0f;
        self.marketmonksBtn.layer.cornerRadius=1.0f;
        self.marketmonksBtn.layer.masksToBounds = NO;
    
    
    self.usMarketsButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.usMarketsButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.usMarketsButton.layer.shadowOpacity = 1.0f;
    self.usMarketsButton.layer.shadowRadius = 1.0f;
    self.usMarketsButton.layer.cornerRadius=1.0f;
    self.usMarketsButton.layer.masksToBounds = NO;
        
        self.wisdomGardenButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
        self.wisdomGardenButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
        self.wisdomGardenButton.layer.shadowOpacity = 1.0f;
        self.wisdomGardenButton.layer.shadowRadius = 1.0f;
        self.wisdomGardenButton.layer.cornerRadius=1.0f;
        self.wisdomGardenButton.layer.masksToBounds = NO;
        
        self.marketWtchButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
        self.marketWtchButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
        self.marketWtchButton.layer.shadowOpacity = 1.0f;
        self.marketWtchButton.layer.shadowRadius = 1.0f;
        self.marketWtchButton.layer.cornerRadius=1.0f;
        self.marketWtchButton.layer.masksToBounds = NO;
        
        self.mutualFundsBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
        self.mutualFundsBtn.layer.shadowOffset = CGSizeMake(0, 2.0f);
        self.mutualFundsBtn.layer.shadowOpacity = 1.0f;
        self.mutualFundsBtn.layer.shadowRadius = 1.0f;
        self.mutualFundsBtn.layer.cornerRadius=1.0f;
        self.mutualFundsBtn.layer.masksToBounds = NO;
        
        self.premiumServiceBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
        self.premiumServiceBtn.layer.shadowOffset = CGSizeMake(0, 2.0f);
        self.premiumServiceBtn.layer.shadowOpacity = 1.0f;
        self.premiumServiceBtn.layer.shadowRadius = 1.0f;
        self.premiumServiceBtn.layer.cornerRadius=1.0f;
        self.premiumServiceBtn.layer.masksToBounds = NO;
        
        self.openAccountButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
        self.openAccountButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
        self.openAccountButton.layer.shadowOpacity = 1.0f;
        self.openAccountButton.layer.shadowRadius = 1.0f;
        self.openAccountButton.layer.cornerRadius=1.0f;
        self.openAccountButton.layer.masksToBounds = NO;
    [self.openAccountButton addTarget:self action:@selector(openAccountAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.openEAccountView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.openEAccountView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.openEAccountView.layer.shadowOpacity = 1.0f;
    self.openEAccountView.layer.shadowRadius = 1.0f;
    self.openEAccountView.layer.cornerRadius=1.0f;
    self.openEAccountView.layer.masksToBounds = NO;
    
        
        //    self.niftyView.layer.cornerRadius = 23.4;
        //    self.niftyView.layer.masksToBounds = YES;
        self.niftyView.backgroundColor=[UIColor colorWithRed:(251/255.0) green:(237/255.0) blue:(224/255.0) alpha:1];
        self.niftyView.layer.borderColor=[UIColor colorWithRed:(162/255.0) green:(150/255.0) blue:(154/255.0) alpha:1].CGColor;
        self.niftyView.layer.borderWidth=1.0f;
        
        
        //    self.sensexView.layer.cornerRadius = 23.4;
        //    self.sensexView.layer.masksToBounds = YES;
        self.sensexView.backgroundColor=[UIColor colorWithRed:(251/255.0) green:(237/255.0) blue:(224/255.0) alpha:5];
        self.sensexView.layer.borderColor=[UIColor colorWithRed:(162/255.0) green:(150/255.0) blue:(154/255.0) alpha:1].CGColor;
        self.sensexView.layer.borderWidth=1.0f;
        
        self.marketmonksBtn.layer.cornerRadius=1.0f;
        self.wisdomGardenButton.layer.cornerRadius=1.0f;
        self.marketWtchButton.layer.cornerRadius=1.0f;
        self.mutualFundsBtn.layer.cornerRadius=1.0f;
        self.premiumServiceBtn.layer.cornerRadius=1.0f;
        
        [self.mutualFundsBtn addTarget:self action:@selector(onClickingMutualBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.usMarketsButton addTarget:self action:@selector(onUSMarketsButtonTap) forControlEvents:UIControlEventTouchUpInside];
        
        UITapGestureRecognizer *bannerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onbottomBannerTap)];
        bannerTap.numberOfTapsRequired = 1;
        bannerTap.numberOfTouchesRequired = 1;
        self.bottomBannerView.userInteractionEnabled = YES;
        [self.bottomBannerView addGestureRecognizer:bannerTap];
    
    UITapGestureRecognizer *monks = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onMonksTap)];
    monks.numberOfTapsRequired = 1;
    monks.numberOfTouchesRequired = 1;
    _monksImgView.userInteractionEnabled = YES;
    [_monksImgView addGestureRecognizer:monks];
    
    UITapGestureRecognizer *watch = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onWatchTap)];
    watch.numberOfTapsRequired = 1;
    watch.numberOfTouchesRequired = 1;
    _watchImgView.userInteractionEnabled = YES;
    [_watchImgView addGestureRecognizer:watch];
    
    UITapGestureRecognizer *toppicks = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTopClick)];
    toppicks.numberOfTapsRequired = 1;
    toppicks.numberOfTouchesRequired = 1;
    self.topPicksImgView.userInteractionEnabled = YES;
    [self.topPicksImgView addGestureRecognizer:toppicks];
    
    UITapGestureRecognizer *equity = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onEquityTap)];
    equity.numberOfTapsRequired = 1;
    equity.numberOfTouchesRequired = 1;
    _equitiesImgView.userInteractionEnabled = YES;
    [_equitiesImgView addGestureRecognizer:equity];
    
    UITapGestureRecognizer *derivative = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDerivativeTap)];
    derivative.numberOfTapsRequired = 1;
    derivative.numberOfTouchesRequired = 1;
    _derivativesImgView.userInteractionEnabled = YES;
    [_derivativesImgView addGestureRecognizer:derivative];
    
    UITapGestureRecognizer *wealth = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onWealthTap)];
    wealth.numberOfTapsRequired = 1;
    wealth.numberOfTouchesRequired = 1;
    _wealthImgView.userInteractionEnabled = YES;
    [_wealthImgView addGestureRecognizer:wealth];
    
    UITapGestureRecognizer *orders = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onOrderTap)];
    orders.numberOfTapsRequired = 1;
    orders.numberOfTouchesRequired = 1;
    _orderImgView.userInteractionEnabled = YES;
    [_orderImgView addGestureRecognizer:orders];
    
    UITapGestureRecognizer *premium = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onPremiumTap)];
    premium.numberOfTapsRequired = 1;
    premium.numberOfTouchesRequired = 1;
    _premiumImgView.userInteractionEnabled = YES;
    [_premiumImgView addGestureRecognizer:premium];
    
    UITapGestureRecognizer *usa = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onUsaTap)];
    usa.numberOfTapsRequired = 1;
    usa.numberOfTouchesRequired = 1;
    _usaImgView.userInteractionEnabled = YES;
    [_usaImgView addGestureRecognizer:usa];
    
    UITapGestureRecognizer *openaccount = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onOpenTap)];
    openaccount.numberOfTapsRequired = 1;
    openaccount.numberOfTouchesRequired = 1;
    self.openAccountIndia.userInteractionEnabled = YES;
    [self.openAccountIndia addGestureRecognizer:openaccount];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    }

-(void)onbottomBannerTap
{
    [self bannerTapMethod];
       
}

-(void)onMonksTap
{
    MarketMonksView * view=[self.storyboard instantiateViewControllerWithIdentifier:@"MarketMonksView"];
    [self.navigationController pushViewController:view animated:YES];
}
-(void)onTopClick
{
    TopPicks * view=[self.storyboard instantiateViewControllerWithIdentifier:@"TopPicks"];
    [self.navigationController pushViewController:view animated:YES];
}
-(void)onOpenTap
{
    OpenAccountView * view=[self.storyboard instantiateViewControllerWithIdentifier:@"OpenAccountView"];
    [self.navigationController pushViewController:view animated:YES];
}

-(void)onWatchTap
{
   HomePage  * view=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
    [self.navigationController pushViewController:view animated:YES];
}

-(void)onEquityTap
{
    delegate2.equities_Derivatives=@"EQUITY";
    WisdomGrdenView * view=[self.storyboard instantiateViewControllerWithIdentifier:@"WisdomGrdenView"];
    
    [self.navigationController pushViewController:view animated:YES];
}

-(void)onDerivativeTap
{
    delegate2.equities_Derivatives=@"DERIVATIVES";
    WisdomGrdenView * view=[self.storyboard instantiateViewControllerWithIdentifier:@"WisdomGrdenView"];
    
    [self.navigationController pushViewController:view animated:YES];
}


-(void)onWealthTap
{
    WealthCreatorViewController * wealth = [self.storyboard instantiateViewControllerWithIdentifier:@"WealthCreatorViewController"];
    [self.navigationController pushViewController:wealth animated:YES];
//    HomePage  * view=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
//    [self.navigationController pushViewController:view animated:YES];
}

-(void)onOrderTap
{
    MainOrdersViewController * orders = [self.storyboard instantiateViewControllerWithIdentifier:@"orders"];
  //  orders.topViewCheck=@"YES";
  //  [self.navigationController pushViewController:orders animated:YES];
    [self presentViewController:orders animated:YES completion:nil];
    //    HomePage  * view=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
    //    [self.navigationController pushViewController:view animated:YES];
}
-(void)onPremiumTap
{
//        NewPremimumServicesMainScreen  * view=[self.storyboard instantiateViewControllerWithIdentifier:@"NewPremimumServicesMainScreen"];
//        [self.navigationController pushViewController:view animated:YES];
    
    delegate2.subscriptionId=@"2";
    NewExploreMonksViewController * exploreMonks = [self.storyboard instantiateViewControllerWithIdentifier:@"NewExploreMonksViewController"];
    
    [self.navigationController pushViewController:exploreMonks animated:YES];
}

-(void)onUsaTap
{
    UIImage *NavigationPortraitBackground= [UIImage imageNamed:@"topUS-1"];
    [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
    
    [[UINavigationBar appearance] setTranslucent:NO];
    //NSLog(@"Need to be changed");
    delegate2.usCheck=@"USA";
    delegate2.usaId=delegate2.userID;
    delegate2.USAuserID=delegate2.userID;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"USA" bundle:nil];
    TabMenuView * usLogin = [storyboard instantiateViewControllerWithIdentifier:@"TabMenuView"];
    // [self.navigationController pushViewController:usLogin animated:YES];
    [self presentViewController:usLogin animated:YES completion:nil];
    //   [self presentModalViewController:[storyboard instantiateViewControllerWithIdentifier:@"USA"] animated:NO];
}

-(void)openAccountAction
{
//    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Coming Soon" preferredStyle:UIAlertControllerStyleAlert];
//
//    [self presentViewController:alert animated:YES completion:^{
//
//    }];
//
//    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//    }];
//
//    [alert addAction:okAction];
    
    OpenAccountView * view=[self.storyboard instantiateViewControllerWithIdentifier:@"OpenAccountView"];
    [self.navigationController pushViewController:view animated:YES];
    
    
}
-(void)onUSMarketsButtonTap
{
//    delegate2.broadCastOutputstream.delegate=self;
//    delegate2.broadCastInputStream.delegate=self;
//    delegate2.inputStream.delegate=self;
//    delegate2.outputStream.delegate=self;
//    
//    [delegate2.broadCastInputStream close];
//     [delegate2.broadCastOutputstream close];
//     [delegate2.inputStream close];
//     [delegate2.outputStream close];
//
    
    UIImage *NavigationPortraitBackground= [UIImage imageNamed:@"topUS-1"];
    [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
    
    [[UINavigationBar appearance] setTranslucent:NO];
    //NSLog(@"Need to be changed");
    delegate2.usCheck=@"USA";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"USA" bundle:nil];
    TabMenuView * usLogin = [storyboard instantiateViewControllerWithIdentifier:@"TabMenuView"];
   // [self.navigationController pushViewController:usLogin animated:YES];
    [self presentViewController:usLogin animated:YES completion:nil];
 //   [self presentModalViewController:[storyboard instantiateViewControllerWithIdentifier:@"USA"] animated:NO];
}

- (void)speechRecognizer:(SFSpeechRecognizer *)speechRecognizer availabilityDidChange:(BOOL)available {
    //NSLog(@"Availability:%d",available);
}

-(void)brokerInfo
{
    @try
    {
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                                  @"deviceid":delegate2.currentDeviceId
                               };
    
    
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower/brokerinfo",delegate2.baseUrl]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==403)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                            
                                                        }
                                                        else if([httpResponse statusCode]==401)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                        brokerResponseDict=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        //NSLog(@"Broker dict:%@",brokerResponseDict);
                                                        //                                                        int a = [[[brokerResponseDict objectForKey:@"results"] count]];
                                                        //                                                        for (int i=0; i<a; i++) {
                                                        //                                                            [brokers arrayByAddingObjectsFromArray:[[[brokerResponseDict objectForKey:@"results"]objectAtIndex:i]objectForKey:@"brokername"]];
                                                        //                                                        }
                                                        //
                                                        
                                                        // //NSLog(@"Brokers:%@",brokers);
                                                        
                                                        for(int i=0;i<[[brokerResponseDict objectForKey:@"results"] count];i++)
                                                        {
                                                            NSString * brokerName=[NSString stringWithFormat:@"%@",[[[brokerResponseDict objectForKey:@"results"]objectAtIndex:i]objectForKey:@"brokername"]];
                                                            
                                                            if([brokerName containsString:@"Zerodha"])
                                                            {
                                                                delegate2.brokerInfoDict=[[brokerResponseDict objectForKey:@"results"]objectAtIndex:i];
                                                            }
                                                        }
                                                        }
                                                    }
                                                }];
    [dataTask resume];
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}


-(void)viewWillAppear:(BOOL)animated
{
    @try
    {
     [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
     if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
    {
        
    }else
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(philipIndex:)
                                                     name:@"indexWatch" object:nil];
    }
    
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"home_page"];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
     NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    [mixpanelMini identify:delegate2.userID];
    [mixpanelMini.people set:@{@"device_type":@"IOS"}];
    [mixpanelMini.people set:@{@"stocks_version":appVersionString}];
    [mixpanelMini.people set:@{@"device_token":[prefs objectForKey:@"devicetoken"]}];
    
    
    NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
    NSString * visibility=[loggedInUser objectForKey:@"visibility"];
    if([visibility isEqualToString:@"showlearn"])
    {
         [loggedInUser setObject:@"hidelearn" forKey:@"visibility"];
         [self.tabBarController setSelectedIndex:1];
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

    
}


//-(void)unSubMethodNifty:(NSNotification *)note
//{
//    [self unSubMethodNifty];
//    [self unSubSensex];
//}
-(void)pushNotificationReceived{
   
   @try
    {
        if(delegate2.notificationInfo!=nil)
        {
    //NSLog(@"%@",delegate2.notificationInfo);

    NSString * destination=[NSString stringWithFormat:@"%@",[[delegate2.notificationInfo objectForKey:@"u"]objectForKey:@"origin"]];
    if([destination isEqualToString:@"Dummy Positions"])
    {
       
        delegate2.pushNotificationSection=@"Dummy Positions";
         delegate2.notificationInfo=nil;
    [self.tabBarController setSelectedIndex:3];
    }
    else if([destination isEqualToString:@"NOTIFICATIONS"])
    {
                
       delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[[delegate2.notificationInfo objectForKey:@"u"]objectForKey:@"section"]];
        delegate2.notificationInfo=nil;
        if([delegate2.pushNotificationSection isEqualToString:@"INDIA_NOTIFICATIONS"])
        {
             [self.tabBarController setSelectedIndex:1];
        }
        else if([delegate2.pushNotificationSection isEqualToString:@"USA_NOTIFICATIONS"])
        {
            [self onUsaTap];
        }
        
    }
    else if([destination isEqualToString:@"INDIA_MARKET_EXPERT"])
    {
        
        delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[bannerNavigationDict objectForKey:@"section"]];
        bannerNavigationDict=nil;
        [self onMonksTap];
        
    }
    else if([destination isEqualToString:@"PROFILE_SETTINGS"])
    {
        
        delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",@"PROFILE_SETTINGS"];
        bannerNavigationDict=nil;
        NewProfileSettings * profile=[self.storyboard instantiateViewControllerWithIdentifier:@"NewProfileSettings"];
        
        [self presentViewController:profile animated:YES completion:nil];
        
    }
    else if([destination isEqualToString:@"INDIA_TRADES"])
    {
        
        delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",@"INDIA_TRADES"];
        bannerNavigationDict=nil;
        TadeView * view=[self.storyboard instantiateViewControllerWithIdentifier:@"trade"];
        
        [self presentViewController:view animated:YES completion:nil];
        
    }
    else if([destination isEqualToString:@"INDIA_PREMIUM_SERVICES"])
    {
        
        delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[bannerNavigationDict objectForKey:@"section"]];
        bannerNavigationDict=nil;
        [self onPremiumTap];
        
    }
    else if([destination isEqualToString:@"USA_MARKET_EXPERTS"])
    {
        
        delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[bannerNavigationDict objectForKey:@"section"]];
        bannerNavigationDict=nil;
        [self onUsaTap];
        
    }
    else if([destination isEqualToString:@"FAQ_SECTION"])
    {
        
        delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[bannerNavigationDict objectForKey:@"section"]];
        bannerNavigationDict=nil;
        
        NSDictionary *metaDataWithTags = @{@"userid":delegate2.userID,
                                           @"emailid":delegate2.emailId,
                                           HelpshiftSupportTagsKey:@[delegate2.brokerNameStr],
                                           @"country:":delegate2.country,
                                           @"locality":delegate2.locality,
                                           @"name":delegate2.name,
                                           @"postalcode":delegate2.postalCode,
                                           @"sublocality":delegate2.subLocality,
                                           
                                           @"locatedAt":delegate2.locatedAt
                                           };
        
        
        
        [HelpshiftSupport showFAQs:self
                       withOptions:@{@"gotoConversationAfterContactUs":@"YES",@"showSearchonNewConversation":@"YES",
                                     HelpshiftSupportCustomMetadataKey: metaDataWithTags}];
        
    }
    else if([destination isEqualToString:@"ZAMBALA_GUIDE"])
    {
        
        delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[[delegate2.notificationInfo objectForKey:@"u"]objectForKey:@"section"]];
        delegate2.notificationInfo=nil;
        if([delegate2.pushNotificationSection isEqualToString:@"INDIA_ZAMBALA_GUIDE"])
        {
            [self.tabBarController setSelectedIndex:1];
        }
        else if([delegate2.pushNotificationSection isEqualToString:@"USA_ZAMBALA_GUIDE"])
        {
            [self onUsaTap];
        }
        
    }
    else if([destination isEqualToString:@"USA_FEED"])
    {
        
        delegate2.pushNotificationSection=@"USA_FEED";
        bannerNavigationDict=nil;
        [self onUsaTap];
    }
    else if([destination isEqualToString:@"INDIA_MARKET_WATCH"])
    {
        
        delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[[delegate2.notificationInfo objectForKey:@"u"]objectForKey:@"section"]];
        delegate2.notificationInfo=nil;
        [self onWatchTap];
        
    }
    else if([destination isEqualToString:@"US_MARKET_WATCH"])
    {
        
        delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[[delegate2.notificationInfo objectForKey:@"u"]objectForKey:@"section"]];
        delegate2.notificationInfo=nil;
        [self onUsaTap];
        
    }
    else if([destination isEqualToString:@"INDIA_TOP_PICKS"])
    {
      
        delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[[delegate2.notificationInfo objectForKey:@"u"]objectForKey:@"section"]];
        delegate2.notificationInfo=nil;
        [self onTopClick];
        
    }
    else if([destination isEqualToString:@"USA_TOP_PICKS"])
    {
       
        delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[[delegate2.notificationInfo objectForKey:@"u"]objectForKey:@"section"]];
        delegate2.notificationInfo=nil;
        [self onUsaTap];
        
    }
            
    else if([destination isEqualToString:@"Equities"])
    {
        
        delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[[delegate2.notificationInfo objectForKey:@"u"]objectForKey:@"section"]];
        delegate2.notificationInfo=nil;
        [self onEquityTap];
    }
    else if([destination isEqualToString:@"Derivatives"])
    {
       
        delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[[delegate2.notificationInfo objectForKey:@"u"]objectForKey:@"section"]];
         delegate2.notificationInfo=nil;
        [self onDerivativeTap];
    }
    else if([destination isEqualToString:@"ZambalaTv"])
    {
      
        delegate2.pushNotificationSection=@"ZambalaTv";
          delegate2.notificationInfo=nil;
        [self.tabBarController setSelectedIndex:4];
    }
    else if([destination isEqualToString:@"INDIA_OPEN_ACCOUNT"])
    {
       
        delegate2.pushNotificationSection=@"OpenAccount";
         delegate2.notificationInfo=nil;
        [self onOpenTap];
    }
    else if([destination isEqualToString:@"USA_OPEN_ACCOUNT"])
    {
        
        delegate2.pushNotificationSection=@"USAOpenAccount";
        delegate2.notificationInfo=nil;
        [self onUsaTap];
    }
    else if([destination isEqualToString:@"PlayStoreUpdate"])
    {
        
        delegate2.pushNotificationSection=@"PlayStoreUpdate";
        delegate2.notificationInfo=nil;
        NSString *iTunesLink = @"https://itunes.apple.com/us/app/zambala-stocks-by-zenwise/id1281356740?mt=8";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
    }
    else if([destination isEqualToString:@"Rewards"])
    {
        
        delegate2.pushNotificationSection=@"Rewards";
        delegate2.notificationInfo=nil;
        WalletView * wallet=[self.storyboard instantiateViewControllerWithIdentifier:@"WalletView"];
        
        [self presentViewController:wallet animated:YES completion:nil];
    }
        }
     }
     @catch (NSException * e) {
         //NSLog(@"Exception: %@", e);
     }
     @finally {
         //NSLog(@"finally");
     }
   
}

- (void)viewDidAppear:(BOOL)animated

{
  
//    speechNavigation =true;
//    [self startListening];
    [self bannerMethod];
    @try
    {
    if(delegate2.notificationInfo!=nil)
    {
        [self pushNotificationReceived];
    }
    if([delegate2.pointsNavigation isEqualToString:@"follow"])
    {
        delegate2.pointsNavigation=@"";
        [self onMonksTap];
    }
    else if([delegate2.pointsNavigation isEqualToString:@"subscribe"])
    {
        delegate2.pointsNavigation=@"";
        [self onPremiumTap];
    }
    else if([delegate2.pointsNavigation isEqualToString:@"open account with indian broker"])
    {
        delegate2.pointsNavigation=@"";
        [self openAccountAction];
    }
    else if([delegate2.pointsNavigation isEqualToString:@"open US trading account"])
    {
        
        [self onUsaTap];
    }
    
    else if([delegate2.pointsNavigation containsString:@"watch zambala tv"])
    {
        
        [self.tabBarController setSelectedIndex:4];
    }
}
@catch (NSException * e) {
    //NSLog(@"Exception: %@", e);
}
@finally {
    //NSLog(@"finally");
}

  
    
    self.brokerNameLblNew.text=[NSString stringWithFormat:@"(%@)",[delegate2.brokerInfoDict objectForKey:@"brokername"]];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushNotificationReceived) name:@"pushNotification" object:nil];
   
    
    delegate2.tickerCheck=true;
    if(delegate2.firstTimeCheck==true)
    {
//    [self createClientMethod];
    delegate2.firstTimeCheck=false;
        
    }
    
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
    {
       [self kiteWebSocket];
    }else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
        //[self unSubSensex];
        //[self unSubMethodNifty];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(unSubMethodNifty:)
                                                     name:@"upnifty" object:nil];
        
        
        [self upstoxSocket];
//
        [self subMethod];
      
    }
    
    else
    {
        
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(showMainMenu:)
//                                                     name:@"orderwatch" object:nil];
        
        
        
         
        
        [self indicesRequest];
    }
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    
    
    
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
    
   

    [super viewDidAppear:animated];
    
    check=true;
    
    
    
    @try
    {
//        if(delegate2.emailId.length>0)
//        {
//            [Tune setUserEmail:delegate2.emailId];
//        }
//        if(delegate2.userName.length>0)
//        {
//            [Tune setUserName:delegate2.userName];
//        }
//
//        [Tune setUserId:delegate2.userID];
        
        // pod 'Tune'
//        TuneLocation *loc = [TuneLocation new];
//        loc.latitude = @(9.142276);
//        loc.longitude = @(-79.724052);
//        loc.altitude = @(15.);
//        [Tune setLocation:loc];
//
//        [Tune measureEventName:TUNE_EVENT_LOGIN];
        
//    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
//    {
////    //mixpanel//
////    Mixpanel *mixpanel = [Mixpanel sharedInstance];
////
////    [mixpanel identify:delegate2.userID];
////
////    [mixpanel.people set:@{@"first_name": delegate2.userName,@"email":delegate2.emailId,@"brokername":delegate2.brokerNameStr,@"logintime":[NSDate date],@"sublocality":delegate2.subLocality,@"location":delegate2.locality,@"postalcode":delegate2.postalCode}];
////
////
////   if(delegate2.mixpanelDeviceToken.length>0)
////   {
////     [mixpanel.people addPushDeviceToken:delegate2.mixpanelDeviceToken];
////
////   }
//
//}
//
//
//
//    else
//    {
//
//
//        Mixpanel *mixpanel = [Mixpanel sharedInstance];
//
//        [mixpanel identify:delegate2.userID];
//
//        [mixpanel.people set:@{@"brokername":delegate2.brokerNameStr,@"logintime":[NSDate date],@"sublocality":delegate2.subLocality,@"location":delegate2.locality,@"postalcode":delegate2.postalCode,@"first_name":delegate2.userID}];
//
//
//        if(delegate2.mixpanelDeviceToken.length>0)
//        {
//            [mixpanel.people addPushDeviceToken:delegate2.mixpanelDeviceToken];
//
//        }
//    }
//
   
    }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
        
if(delegate2.stream==YES)
{
        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
        {
            
         // [self liveStreaming];
        }
    
        else
        {
//        [self  broadRequest];
        
        }
}
    

    if([delegate2.mainViewBrokerStr isEqualToString:@"Show"])
    {
        NSString * localStr=[NSString stringWithFormat:@"(%@)",delegate2.brokerNameStr];
        
        [self.brokerName setTitle:localStr];
        
    }
    
    else if([delegate2.mainViewBrokerStr isEqualToString:@"DontShow"])
    {
        
        [self.brokerName setTitle:@""];
        
    }
    
    if([delegate2.mainTickerStr isEqualToString:@"Show"])
    {
        self.tickerImageView.hidden=NO;
        self.tickerImgHeightConstraint.constant=56;
        self.mainTickerView.hidden=NO;
        self.mainTickerViewHeightConstraint.constant=56;
        
    }
    
    else if([delegate2.mainTickerStr isEqualToString:@"DontShow"])
    {
        
        self.tickerImageView.hidden=YES;
        self.tickerImgHeightConstraint.constant=0;
        self.mainTickerView.hidden=YES;
        self.mainTickerViewHeightConstraint.constant=0;
        
    }
    
    

}




/*!
 @discussion It is used to create client.
 */

-(void)createClientMethod
{
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
   
    if(delegate2.currentDeviceId.length!=0)
    {
        
        if(delegate2.referralCode.length==0)
        {
            delegate2.referralCode=@"";
            
        }
        
      if(delegate2.emailId.length!=0&&delegate2.userName.length!=0)
      {
          
      }
        else
        {
            delegate2.emailId=@"";
            delegate2.userName=@"";
        }
        
        if(brokerid.length==0)
        {
            brokerid=@"";
        }
        
       
       
    @try {
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"cache-control": @"no-cache",
                                   };
        NSDictionary *parameters = @{ @"clientmemberid":delegate2.userID,
                                    @"referralcode":delegate2.referralCode,
                                    @"brokerid":brokerid,
                                    @"email":delegate2.emailId,
                                    @"firstname":delegate2.userName,
                                    @"lastname":@"",
                                    @"notificationinfo": @{ @"deviceid": delegate2.currentDeviceId, @"devicetoken":[prefs objectForKey:@"devicetoken"],
                                                               @"devicetype":@1
                                                               }
                                      
                                      
                                      
                                    };
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        
        NSString * requestStr=[NSString stringWithFormat:@"%@follower/client",delegate2.baseUrl];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestStr]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            if([httpResponse statusCode]==403)
                                                            {
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                
                                                            }
                                                            else if([httpResponse statusCode]==401)
                                                            {
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                
                                                            }
                                                            else
                                                            {
                                                                
                                                            clientCreatResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            }
                                                        }
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            delegate2.zenwiseToken=[NSString stringWithFormat:@"%@",[clientCreatResponse objectForKey:@"authtoken"]];
                                                            
                                                            NSString * meesage=[NSString stringWithFormat:@"%@",[clientCreatResponse objectForKey:@"message"]];
                                                            if([delegate2.loginActivityStr isEqualToString:@"OTP1"])
                                                            {
                                                                 
                                                                [self guestVisibility];
                                                                
                                                                NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
                                                                [loggedInUser setObject:delegate2.userID forKey:@"guestaccess"];
                                                                [loggedInUser synchronize];
                                                                
                                                                
                                                            }
                                                           
                                                            
                                                        if([meesage containsString:@"created"])
                                                                
                                                                
                                                            {
                                                                
                                                               
                                                                
                                                        delegate2.marketmonksHint=true;
                                                                delegate2.wisdomHint=true;
                                                                delegate2.feedsHint=true;
                                                        delegate2.marketwatchHinT=true;
                                                           delegate2.stockHint=true;
                                                                
                                                            }
                                                            
                                                            else
                                                            {
                                                                delegate2.marketmonksHint=false;
                                                                delegate2.wisdomHint=false;
                                                                delegate2.feedsHint=false;
                                                                delegate2.marketwatchHinT=false;
                                                                delegate2.stockHint=false;
                                                                
                                                                
                                                            }
                                                            
                                                            delegate2.stream=YES;
                                                            if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                                                            {
                                                                
                                                               // [self liveStreaming];
                                                                
                                                            }
                                                            
                                                            else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
                                                            {
//                                                                [self subMethod];
//
//                                                                    [self.socket close];
//                                                                    [self upstoxSocket];
//
                                                                
                                                            
                                                                
                                                            }
                                                            
                                                        });
                                                        
                                                    }];
        [dataTask resume];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
        
    }
    
    
    else
    {
//        [self brokerInfo];
        
    }
}
/*!
 @discussion It is used to subscribe for nifty and sensex(to get live data in upstox).
 */
-(void)subMethod
{
    @try
    {
    NSArray * exchangeArray = @[@"nse_index",@"bse_index"];
    NSArray * symbolsArray = @[@"NIFTY_50",@"SENSEX"];
    
   // NSArray * exchangeArray = @[@"nse_index"];
   // NSArray * symbolsArray = @[@"NIFTY_50"];
    
    for(int i=0;i<exchangeArray.count;i++)
    {
        NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
        NSDictionary *headers = @{ @"x-api-key":delegate2.APIKey,
                                   @"authorization": access,
                                   };
        
        NSString * url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/%@/?symbol=%@",[exchangeArray objectAtIndex:i],[symbolsArray objectAtIndex:i]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:40.0];
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData* data, NSURLResponse *response, NSError *error) {
                                                        if(data!=nil)
                                                        {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            
                                                            
                                                            NSMutableArray * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            
                                                            //NSLog(@"%@",json);
                                                        }
                                                        
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            
                                                        });
                                                        }
                                                        else
                                                        {
                                                            [self subMethod];
                                                        }
                                                        
                                                    }];
        [dataTask resume];
        
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}


//-(void)subReliance
//{
//    NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
//    NSDictionary *headers = @{ @"x-api-key":delegate2.APIKey,
//                               @"authorization": access,
//                               };
//
//    NSString * url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/%@/?symbol=%@",@"nse_eq",@"RELIANCE"];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
//                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                       timeoutInterval:30.0];
//    [request setHTTPMethod:@"GET"];
//    [request setAllHTTPHeaderFields:headers];
//
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
//                                                completionHandler:^(NSData* data, NSURLResponse *response, NSError *error) {
//                                                    if (error) {
//                                                        //NSLog(@"%@", error);
//                                                    } else {
//                                                        NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *) response;
//                                                        //NSLog(@"%@", httpResponse);
//
//
//                                                        NSMutableArray * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//
//                                                        //NSLog(@"%@",json);
//                                                    }
//
//
//                                                }];
//    [dataTask resume];
//}

//-(void)unsubReliance
//{
//    NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
//    NSDictionary *headers = @{ @"x-api-key":delegate2.APIKey,
//                               @"authorization": access,
//                               };
//
//    NSString * url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/%@/?symbol=%@",@"nse_eq",@"RELIANCE"];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
//                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                       timeoutInterval:30.0];
//    [request setHTTPMethod:@"GET"];
//    [request setAllHTTPHeaderFields:headers];
//
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
//                                                completionHandler:^(NSData* data, NSURLResponse *response, NSError *error) {
//                                                    if (error) {
//                                                        //NSLog(@"%@", error);
//                                                    } else {
//                                                        NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *) response;
//                                                        //NSLog(@"%@", httpResponse);
//
//
//                                                        NSMutableArray * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//
//                                                        //NSLog(@"%@",json);
//                                                    }
//
//
//                                                }];
//    [dataTask resume];
//}
//


/*!
 @discussion It is used to unsubscribe for nifty(to stop getting live data in upstox).
 */

//-(void)unSubMethodNifty
//{
////    NSArray * exchangeArray = @[@"nse_index",@"bse_index"];
////    NSArray * symbolsArray = @[@"NIFTY_50",@"SENSEX"];
//    //for(int i=0;i<exchangeArray.count;i++)
//  //  {
//    NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
//    NSDictionary *headers = @{ @"x-api-key": delegate2.APIKey,
//                               @"authorization": access,
//                               };
//    //NSString * url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/%@/?symbol=%@",[exchangeArray objectAtIndex:i],[symbolsArray objectAtIndex:i]];
//        NSString * url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/nse_index/?symbol=NIFTY_50"];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
//                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                       timeoutInterval:30.0];
//    [request setHTTPMethod:@"GET"];
//    [request setAllHTTPHeaderFields:headers];
//
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
//                                                completionHandler:^(NSData* data, NSURLResponse *response, NSError *error) {
//                                                    if (error) {
//                                                        //NSLog(@"%@", error);
//                                                    } else {
//                                                        NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *) response;
//                                                        //NSLog(@"%@", httpResponse);
//                                                        NSMutableArray * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//
//                                                        //NSLog(@"%@",json);
//                                                    }
//
//
//
////                                                    dispatch_async(dispatch_get_main_queue(), ^{
////
////
////                                                    });
//                                                }];
//    [dataTask resume];
////}

//      TabBar *tab = [self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
//      [tab setSelectedIndex:3];
//}

/*!
 @discussion It is used to unsubscribe for sensex(to stop getting live data in upstox).
 */
//-(void)unSubSensex
//{
////    NSArray * exchangeArray = @[@"nse_index",@"bse_index"];
////    NSArray * symbolsArray = @[@"NIFTY_50",@"SENSEX"];
//    //for(int i=0;i<exchangeArray.count;i++)
//    //  {
//    NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
//    NSDictionary *headers = @{ @"x-api-key": delegate2.APIKey,
//                               @"authorization": access,
//                               };
//    //NSString * url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/%@/?symbol=%@",[exchangeArray objectAtIndex:i],[symbolsArray objectAtIndex:i]];
//    NSString * url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/bse_index/?symbol=SENSEX"];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
//                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                       timeoutInterval:30.0];
//    [request setHTTPMethod:@"GET"];
//    [request setAllHTTPHeaderFields:headers];
//
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
//                                                completionHandler:^(NSData* data, NSURLResponse *response, NSError *error) {
//                                                    if (error) {
//                                                        //NSLog(@"%@", error);
//                                                    } else {
//                                                        NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *) response;
//                                                        //NSLog(@"%@", httpResponse);
//                                                        NSMutableArray * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//
//                                                        //NSLog(@"%@",json);
//                                                    }
//
//
//
//                                                    //                                                    dispatch_async(dispatch_get_main_queue(), ^{
//                                                    //
//                                                    //
//                                                    //                                                    });
//                                                }];
//    [dataTask resume];
//    //}
//}

-(void)niftysensex:(NSArray *)dataArray
{
   
@try
    {
    for (int i=0; i<dataArray.count; i++) {
        
        NSString * niftySymbol = [[dataArray objectAtIndex:i] objectForKey:@"stSymbol1"];
        niftySymbol = [niftySymbol stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString * ltp =[NSString stringWithFormat:@"%.2f",[[[dataArray objectAtIndex:i]objectForKey:@"dbIndexPrice"]floatValue]];
        NSString * closePrice = [NSString stringWithFormat:@"%.2f",[[[dataArray objectAtIndex:i]objectForKey:@"dbPrevIndexClose"]floatValue]];
        
        float ltpFloat = [ltp floatValue];
        float closePriceFloat = [closePrice floatValue];
        
        
        float changePercentage = ((ltpFloat-closePriceFloat) *100/closePriceFloat);
        NSString * percent = @"%";
        dispatch_async(dispatch_get_main_queue(), ^{
        if([niftySymbol containsString:@"Nifty50"])
        {
            
            self.lastPrice.text=[NSString stringWithFormat:@"%.2f",ltpFloat];
            self.changePercent.text = [NSString stringWithFormat:@"(%.2f%@)",changePercentage,percent];
            
            if([self.changePercent.text containsString:@"-"])
            {
                
                self.changePercent.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
            }
            else{
                
                self.changePercent.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
            }
           
            
        }else if ([niftySymbol containsString:@"SENSEX"])
        {
            
            self.sensexLastPrice.text=[NSString stringWithFormat:@"%.2f",ltpFloat];
            self.sensexChangePercent.text = [NSString stringWithFormat:@"(%.2f%@)",changePercentage,percent];
            
            if([self.sensexChangePercent.text containsString:@"-"])
            {
                
                self.sensexChangePercent.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
            }
            else{
                
                self.sensexChangePercent.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
            }
            
        }
      });
        }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self.socketio disconnect];
    self.socketio=nil;
    [self.socket close];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (void)viewDidDisappear:(BOOL)animated
{
    delegate2.tickerCheck=false;
    check=false;
    if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
//            [self unSubMethodNifty];
//            [self unSubSensex];
//            [self unsubReliance];
        });
       
        [self.socket close];
        self.socket=nil;
    }
    [self.socketio disconnect];
    self.socketio=nil;
//    self.socket=nil;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
/*!
 @discussion It is used to navigate to mutual fund section.
 */

-(void)onClickingMutualBtn
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Messege" message:@"Coming Soon" preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:^{
        
    }];
    
    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:okAction];
    

    
}




//-(void)onChatbuttonTap
//{
//
//
//    NewProfileSettings *profileVieww=[self.storyboard instantiateViewControllerWithIdentifier:@"NewProfileSettings"];
//
//    [self presentViewController:profileVieww animated:YES completion:nil];
//}


//REACHABILITY//

/*!
 @discussion It is used to check network status.
 @param notice notifies when net is disconnected
 */


-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
          
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    
    
}

/*!
 @discussion It is used to check network status.
 */


-(BOOL)networkStatus
{
    
    
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    
    if(netStatus==NotReachable)
    {
        
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    return YES;
    
    
}

/*!
 @discussion It is used to connect to zerodha websocket.
 */


-(void)kiteWebSocket
{
    @try
    {
     NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"wss://ws.kite.trade/?api_key=%@&access_token=%@",delegate2.APIKey,delegate2.accessToken]];
 
    
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    
    //     create the socket and assign delegate
    self.socket = [PSWebSocket clientSocketWithRequest:request];
    self.socket.delegate = self;
    //
    //    self.socket.delegateQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    //     open socket
    [self.socket open];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

/*!
 @discussion It is used to connect to upstox socket.
 */


-(void)upstoxSocket
{
    @try
    {
        //pocket socket//
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"wss://ws-api.upstox.com/?apiKey=%@&token=%@",delegate2.APIKey,delegate2.accessToken]];
        
    
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
//                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                       timeoutInterval:40];
    
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
        
        //     create the socket and assign delegate
        self.socket = [PSWebSocket clientSocketWithRequest:request];
        self.socket.delegate = self;
        //
            self.socket.delegateQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
        //     open socket
        [self.socket open];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

/*!
 @discussion It notifies socket is opend.
 @param webSocket socket instance.
 */


- (void)webSocketDidOpen:(PSWebSocket *)webSocket {
    //NSLog(@"Handshake complete");
    @try
    {
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
    {
         NSMutableArray* localInstrumentToken=[[NSMutableArray alloc]initWithObjects:[NSNumber numberWithInt:265],[NSNumber numberWithInt:256265], nil];
            
//            localInstrumentToken=delegate2.homeInstrumentToken;
        
            localInstrumentToken=[[[localInstrumentToken reverseObjectEnumerator] allObjects] mutableCopy];
            
            if(localInstrumentToken.count>0)
            {
                
                NSDictionary * subscribeDict=@{@"a": @"subscribe", @"v":localInstrumentToken};
                
                
                NSData *json;
                
                NSError * error;
                
                
                
                
                // Dictionary convertable to JSON ?
                if ([NSJSONSerialization isValidJSONObject:subscribeDict])
                {
                    // Serialize the dictionary
                    json = [NSJSONSerialization dataWithJSONObject:subscribeDict options:NSJSONWritingPrettyPrinted error:&error];
                    
                    // If no errors, let's view the JSON
                    if (json != nil && error == nil)
                    {
                        string1 = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                        
                        //            delegate2.message=[NSString stringWithFormat:@"%@",str];
                        //
                        
                        //NSLog(@"JSON: %@",string1);
                        
                    }
                }
                
                
                [self.socket send:string1];
                
                
                
                
                
                
                NSArray * mode=@[@"full",localInstrumentToken];
                
                subscribeDict=@{@"a": @"mode", @"v": mode};
                
                
                
                //     Dictionary convertable to JSON ?
                if ([NSJSONSerialization isValidJSONObject:subscribeDict])
                {
                    // Serialize the dictionary
                    json = [NSJSONSerialization dataWithJSONObject:subscribeDict options:NSJSONWritingPrettyPrinted error:&error];
                    
                    
                    // If no errors, let's view the JSON
                    if (json != nil && error == nil)
                    {
                        message = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                        
                        //            delegate2.message=[NSString stringWithFormat:@"%@",str];
                        
                        //NSLog(@"JSON: %@",message);
                        
                    }
                }
                [self.socket send:message];
                
            }
            
            
        }
    
    else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {

    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    }

/*!
 @discussion It notifies socket recieved message.
 @param message data from socket.
 */


- (void)webSocket:(PSWebSocket *)webSocket didReceiveMessage:(id)messagee {
    
    
if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
{
    @try
    {
    NSData * data=[NSKeyedArchiver archivedDataWithRootObject:messagee];
    //NSLog(@"size of Object: %zd",malloc_size((__bridge const void *)(messagee)));
    if(data.length > 247)
    {
        //NSLog(@"The websocket received a message: %@",message);
        id packets = [messagee subdataWithRange:NSMakeRange(0,2)];
        packetsNumber = CFSwapInt16BigToHost(*(int16_t*)([packets bytes]));
        //NSLog(@" number of packets %i",packetsNumber);
        int startingPosition = 2;
        //            if([instrumentTokensDict allKeys])
        //                [instrumentTokensDict removeAllObjects];
        
        for( int i =0; i< packetsNumber ; i++)
        {
            
            
            NSData * packetsLengthData = [messagee subdataWithRange:NSMakeRange(startingPosition,2)];
            packetsLength = CFSwapInt16BigToHost(*(int16_t*)([packetsLengthData bytes]));
            startingPosition = startingPosition + 2;
            
            id packetQuote = [messagee subdataWithRange:NSMakeRange(startingPosition,packetsLength)];
            
            
            id instrumentTokenData = [packetQuote subdataWithRange:NSMakeRange(0,4)];
            int32_t instrumentTokenValue = CFSwapInt32BigToHost(*(int32_t*)([instrumentTokenData bytes]));
            
            id lastPrice = [packetQuote subdataWithRange:NSMakeRange(4,4)];
            int32_t lastPriceValue = CFSwapInt32BigToHost(*(int32_t*)([lastPrice bytes]));
            
            id closingPrice = [packetQuote subdataWithRange:NSMakeRange(20,4)];
            int32_t closePriceValue = CFSwapInt32BigToHost(*(int32_t*)([closingPrice bytes]));
            
           float lastPriceFlaotValue = (float)lastPriceValue/100;
            
            float closePriceFlaotValue = (float)closePriceValue/100;
            
            //NSLog(@"CLOSE %f",closePriceFlaotValue);
            
            float changeFloatValue = lastPriceFlaotValue-closePriceFlaotValue;
            
           float changeFloatPercentageValue = ((lastPriceFlaotValue-closePriceFlaotValue) *100/closePriceFlaotValue);
            startingPosition = startingPosition + packetsLength;
            
            NSString * instrumentToken = [NSString stringWithFormat:@"%d",instrumentTokenValue];
            NSString * percent=@"%";
            if([instrumentToken isEqualToString:@"265"])
            {
                 dispatch_async(dispatch_get_main_queue(), ^{
                self.sensexLastPrice.text=[NSString stringWithFormat:@"%.2f",lastPriceFlaotValue];
                self.sensexChangePercent.text = [NSString stringWithFormat:@"(%.2f%@)",changeFloatPercentageValue,percent];
                
                if([self.sensexChangePercent.text containsString:@"-"])
                {
                  
                    self.sensexChangePercent.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                }
                else{
                   
                   self.sensexChangePercent.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                }
                 });
            }
            if ([instrumentToken isEqualToString:@"256265"])
            {
                 dispatch_async(dispatch_get_main_queue(), ^{
                self.lastPrice.text=[NSString stringWithFormat:@"%.2f",lastPriceFlaotValue];
                self.changePercent.text = [NSString stringWithFormat:@"(%.2f%@)",changeFloatPercentageValue,percent];
                
                if([self.changePercent.text containsString:@"-"])
                {
                    
                    self.changePercent.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                }
                else{
                    
                    self.changePercent.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                }
                 });
            }
            
        }
   
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
{
    @try
    {
    NSString *detailsString = [[NSString alloc] initWithData:messagee encoding:NSASCIIStringEncoding];
    
        //NSLog(@"%@",detailsString);
        
        NSArray * completeArray=[[NSArray alloc]init];
        
        completeArray= [detailsString componentsSeparatedByString:@";"] ;
        //NSLog(@"%@",completeArray);
        
        for (int i=0; i<completeArray.count; i++) {

            NSMutableArray * individualCompanyArray=[[NSMutableArray alloc]init];
            NSString * innerStr=[NSString stringWithFormat:@"%@",[completeArray objectAtIndex:i]];

            individualCompanyArray=[[innerStr componentsSeparatedByString:@","] mutableCopy];
            
            NSString * percent =@"%";
            //NSLog(@"indi:%@",individualCompanyArray);
            if([[individualCompanyArray objectAtIndex:2]isEqualToString:@"NIFTY_50"])
            {
                NSString* niftyLTP = [NSString stringWithFormat:@"%.2f",[[individualCompanyArray objectAtIndex:3]floatValue]];
                 dispatch_async(dispatch_get_main_queue(), ^{
                self.lastPrice.text = niftyLTP;
                NSString * closingNifty = [NSString stringWithFormat:@"%.2f",[[individualCompanyArray objectAtIndex:7]floatValue]];
                
                float niftyFloat = [niftyLTP floatValue];
                float closingFloat = [closingNifty floatValue];
                float changeFloatPercentageValue;
                
                changeFloatPercentageValue = ((niftyFloat-closingFloat) *100/closingFloat);
                self.changePercent.text = [NSString stringWithFormat:@"(%.2f%@)",changeFloatPercentageValue,percent];
                
                if([self.changePercent.text containsString:@"-"])
                {
                    
                    self.changePercent.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                }
                else{
                    
                    self.changePercent.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                }
                 });
            }
            
            
            else if([[individualCompanyArray objectAtIndex:2]isEqualToString:@"SENSEX"])
            {
                NSString* niftyLTP = [NSString stringWithFormat:@"%.2f",[[individualCompanyArray objectAtIndex:3]floatValue]];
                 dispatch_async(dispatch_get_main_queue(), ^{
                self.sensexLastPrice.text = niftyLTP;
                NSString * closingNifty = [NSString stringWithFormat:@"%.2f",[[individualCompanyArray objectAtIndex:7]floatValue]];
                
                float niftyFloat = [niftyLTP floatValue];
                float closingFloat = [closingNifty floatValue];
                float changeFloatPercentageValue;
                
                changeFloatPercentageValue = ((niftyFloat-closingFloat) *100/closingFloat);
                self.sensexChangePercent.text = [NSString stringWithFormat:@"(%.2f%@)",changeFloatPercentageValue,percent];
                
                if([self.sensexChangePercent.text containsString:@"-"])
                {
                    
                    self.sensexChangePercent.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                }
                else{
                    
                    self.sensexChangePercent.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                }
                 });
            }
        }


}
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}
}

/*!
 @discussion It notifies socket connection failed.
 @param error reason for interuption of socket connection.
 */



- (void)webSocket:(PSWebSocket *)webSocket didFailWithError:(NSError *)error {
    //NSLog(@"The websocket handshake/connection failed with an error: %@", error);
}


/*!
 @discussion It notifies socket recieved data.
 @param data data from socket.
 */

- (void)webSocket:(PSWebSocket *)webSocket didReceiveMessageWithData:(NSData *)data
{
    //NSLog(@"The websocket received a message: %@", data);
}


- (void)webSocket:(PSWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    //NSLog(@"The websocket closed with code: %@, reason: %@, wasClean: %@", @(code), reason, (wasClean) ? @"YES" : @"NO");
}
    




/*!
 @discussion It navigates to messages view.
 @param sender button instance.
 */

- (IBAction)onNotificationButtonTap:(id)sender {
    
    NewMessages * messageView = [self.storyboard instantiateViewControllerWithIdentifier:@"NewMessages"];
    [self presentViewController:messageView animated:YES completion:nil];
    
}

/*!
 @discussion We request multitrade server to get nifty and sensex prices.
 */

-(void)broadRequest
{
  
delegate2.mtCheck=true;


//  [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.Watch];




//        [[NSNotificationCenter defaultCenter] postNotificationName:@"broad" object:nil];



}

/*!
 @discussion It is used to send request multitrade server.
 
 */



/*!
 @discussion We will get live stream for nifty and sensex.

 */


- (void)showMainMenu:(NSNotification *)note


{
    @try
    {
    for(int i=0;i<delegate2.marketWatch1.count;i++)
    {
        NSString * tmpInt=[[delegate2.marketWatch1 objectAtIndex:i] objectForKey:@"stSecurityID"];
        
        
      
        NSString * niftyStr=@"Nifty 50";
        
        if([tmpInt isEqualToString:niftyStr])
        {
            float  priceString=[[[delegate2.marketWatch1 objectAtIndex:i] objectForKey:@"dbLTP"]floatValue];
            
            float close=[[[delegate2.marketWatch1 objectAtIndex:i] objectForKey:@"dbClose"]floatValue];;
            self.lastPrice.text=[NSString stringWithFormat:@"%.2f",priceString];
            float changePercentage=((priceString-close) *100/close);
            NSString * str=@"%";
            self.changePercent.text = [NSString stringWithFormat:@"(%.2f%@)",changePercentage,str];
            
            if([self.changePercent.text containsString:@"-"])
            {
             
               self.changePercent.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
            }
            else{
               
                 self.changePercent.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
            }
            
            
            
            
            self.sensexLastPrice.text=@"0.00";
            self.sensexChangePercent.text=@"(0.00%)";
        }
        
    
    
  }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}

/*!
 @discussion we will get the guest visibilty rules.
 
 */

-(void)guestVisibility
{
    
    @try
    {
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"authtoken":delegate2.zenwiseToken,
                                  @"deviceid":delegate2.currentDeviceId
                               };
    
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower/recommendedbrokers?clientid=%@",delegate2.baseUrl,delegate2.userID]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==403)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                            
                                                        }
                                                        else if([httpResponse statusCode]==401)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                        self.responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        //NSLog(@"Open account responseArray:%@",self.responseDict);
                                                        
                                                        }
                                                        
                                                        
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                     if([[self.responseDict  objectForKey:@"results"]count]==1)
                     {
                                                        
    NSString * mutualFundCheck=[NSString stringWithFormat:@"%@",[[[self.responseDict  objectForKey:@"results"]objectAtIndex:0]objectForKey:@"guestmutualfund"]];
                                                        
                                                        if([mutualFundCheck isEqual:[NSNull null]])
                                                        {
                                                            
                                                            mutualFundCheck=@"";
                                                        }
                                                        
                                                        
                                                        
                                                        
                                                        NSString * usmarketCheck=[NSString stringWithFormat:@"%@",[[[self.responseDict  objectForKey:@"results"]objectAtIndex:0]objectForKey:@"guestusmarket"]];
                                                        
                                                        if([usmarketCheck isEqual:[NSNull null]])
                                                        {
                                                            
                                                            usmarketCheck=@"";
                                                        }
                                                        if([mutualFundCheck isEqualToString:@"1"])
                                                        {
                                                            self.mutualFundsLbl.hidden=YES;
                                                            self.mutualFundsLbl.text=@"Mutual Funds";
                                                            
                                                        }
                                                        
                                                        else
                                                        {
                                                            self.mutualFundsLbl.hidden=YES;
                                                            
                                                        }
                                                        
                                                        if([usmarketCheck isEqualToString:@"1"])
                                                        {
                                                            
                                                            self.usMarketsButton.hidden=NO;
                                                            self.usMarketsView.hidden=NO;
                                                            
                                                            
                                                        }
                                                        
                                                        else
                                                        {
                                                            
                                                            self.usMarketsButton.hidden=YES;
                                                            self.usMarketsView.hidden=YES;
                                                            
                                                        }
                                                        
                     }
                                                        
                                                        else
                                                        {
                                                            
                                                            self.usMarketsButton.hidden=NO;
                                                            self.usMarketsView.hidden=NO;
                                                            
                                                            self.mutualFundsLbl.hidden=YES;
                                                            self.mutualFundsLbl.text=@"Mutual Funds";
                                                            
                                                        }
                                                        
                                                        
                                                        
                                                    });
                                                }];
    [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    
}

- (IBAction)equityAction:(id)sender {
    
    delegate2.equities_Derivatives=@"EQUITY";
    WisdomGrdenView * view=[self.storyboard instantiateViewControllerWithIdentifier:@"WisdomGrdenView"];
    
    [self.navigationController pushViewController:view animated:YES];
}

- (IBAction)derivativesAction:(id)sender {
    
    delegate2.equities_Derivatives=@"DERIVATIVES";
    WisdomGrdenView * view=[self.storyboard instantiateViewControllerWithIdentifier:@"WisdomGrdenView"];
    
    [self.navigationController pushViewController:view animated:YES];
}


//-(void)viewWillDisappear:(BOOL)animated
//{
//    
//    delegate2.searchToWatch=true;
//}

-(void)indicesRequest
{
    @try
    {
    NSArray * security=@[@"Nifty 50",@"1"];
    NSArray * exchange=@[@"NSECM",@"BSECM"];
//    for(int i=0;i<security.count;i++)
//    {
//        TagEncode * tag = [[TagEncode alloc]init];
//        delegate2.mtCheck=true;
//
//
//        //  [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.Watch];
//
//
//        [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.IndexWatch];
//        //        [tag TagData:sharedManager.stSecurityID stringMethod:[localInstrumentToken objectAtIndex:i]]; //2885
//        [tag TagData:sharedManager.stSecurityID stringMethod:[security objectAtIndex:i]];
//        [tag TagData:sharedManager.stExchange stringMethod:[exchange objectAtIndex:i]];
//        [tag GetBuffer];
//        [self newMessage];

        //        [[NSNotificationCenter defaultCenter] postNotificationName:@"broad" object:nil];
        
        
        
            @try {
                //            instrumentTokensDict = [[NSMutableDictionary alloc]init];
                NSURL* url = [[NSURL alloc] initWithString:delegate2.ltpServerURL];
                self.manager = [[SocketManager alloc] initWithSocketURL:url config:@{@"log": @YES}];
                
                self.socketio=self.manager.defaultSocket;
                
                [self.socketio on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
                    //NSLog(@"socket connected");
                    
                    
                    for(int i=0;i<security.count;i++)
                    {
                        NSMutableArray * subscriptionList=[[NSMutableArray alloc]init];
                        NSMutableDictionary * tmpDict = [[NSMutableDictionary alloc]init];
                        
                        [tmpDict setValue:[security objectAtIndex:i] forKey:@"securityToken"];
                        [tmpDict setValue:[exchange objectAtIndex:i] forKey:@"exchange"];
                        [subscriptionList addObject:tmpDict];
                        
                        [self.socketio emit:@"nifty" with:subscriptionList];
                         [self.socketio emit:@"initltp" with:subscriptionList];
                        
                        
                    }
                    
                    
                }];
                
                
                [self.socketio on:@"ltp" callback:^(NSArray* data, SocketAckEmitter* ack) {
                    //NSLog(@"%@",data);
//                    [self broadCastMethod:data];
                    [self niftysensex:data];
                    
                    
                    
                }];
                [self.socketio on:@"initltp" callback:^(NSArray* data, SocketAckEmitter* ack) {
                    //NSLog(@"%@",data);
                    //                    [self broadCastMethod:data];
                    [self niftysensex:data];
                    
                    
                    
                }];
                
                [self.socketio connect];
                
             
                
            }
            @catch (NSException * e) {
                //NSLog(@"Exception: %@", e);
            }
            @finally {
                //NSLog(@"finally");
            }
        

//    }
    
//    TagEncode * tag = [[TagEncode alloc]init];
//    delegate2.mtCheck=true;
//
//
//    //  [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.Watch];
//
//
//    [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.IndexWatch];
//    //        [tag TagData:sharedManager.stSecurityID stringMethod:[localInstrumentToken objectAtIndex:i]]; //2885
//    [tag TagData:sharedManager.stSecurityID stringMethod:@"Nifty 50"];
//    [tag TagData:sharedManager.stExchange stringMethod:@"NSECM"];
//    [tag GetBuffer];
//    [self newMessage];
//
//    TagEncode * tag1 = [[TagEncode alloc]init];
//    delegate2.mtCheck=true;
//
//
//    //  [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.Watch];
//
//
//    [tag1 TagData:sharedManager.shMsgCode shValueMethod:sharedManager.IndexWatch];
//    //        [tag TagData:sharedManager.stSecurityID stringMethod:[localInstrumentToken objectAtIndex:i]]; //2885
//    [tag1 TagData:sharedManager.stSecurityID "];
//    [tag1 TagData:sharedManager.stExchange stringMethod:@"BSECM"];
//    [tag1 GetBuffer];
//    [self newMessage];
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}
//- (void)startListening {
//
//    // Initialize the AVAudioEngine
////    audioEngine = [[AVAudioEngine alloc] init];
////
////    // Make sure there's not a recognition task already running
////    if (recognitionTask) {
////        [recognitionTask cancel];
////        recognitionTask = nil;
////    }
////
////    // Starts an AVAudio Session
////    NSError *error;
////    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
////    [audioSession setCategory:AVAudioSessionCategoryRecord error:&error];
////    [audioSession setActive:YES withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation error:&error];
////
////    // Starts a recognition process, in the block it logs the input or stops the audio
////    // process if there's an error.
////    recognitionRequest = [[SFSpeechAudioBufferRecognitionRequest alloc] init];
////    AVAudioInputNode *inputNode = audioEngine.inputNode;
////    recognitionRequest.shouldReportPartialResults = YES;
////    recognitionTask = [speechRecognizer recognitionTaskWithRequest:recognitionRequest resultHandler:^(SFSpeechRecognitionResult * _Nullable result, NSError * _Nullable error) {
////        BOOL isFinal = NO;
////        if (result) {
////            // Whatever you say in the microphone after pressing the button should be being logged
////            // in the console.
////            //NSLog(@"RESULT:%@",result.bestTranscription.formattedString);
////            NSString * text=[NSString stringWithFormat:@"%@",result.bestTranscription.formattedString];
////            text = [text lowercaseString];
////            if(speechNavigation==true)
////            {
////
////                @try
////                {
//                    TagEncode * enocde=[[TagEncode alloc]init];
//
//                    enocde.inputRequestString=@"speech";
//                    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
////    NSString * text =@"What is the news on Fantastic Beasts in the Guardian?";
//     NSString * text =@"Get me the reliance advices";
//                    text = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
//                                                                                                           NULL,
//                                                                                                           (CFStringRef)text,
//                                                                                                           NULL,
//                                                                                                           (CFStringRef)@"!*'();:@&=+$,/?%#[] ",
//                                                                                                           kCFStringEncodingUTF8 ));
//                    [inputArray addObject:[NSString stringWithFormat:@"http://192.168.15.51:5000/getResponse?input=%@",text]];
//
//                    [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
//
//                        //NSLog(@"%@",dict);
//
//
//                    }];
////                }
////                @catch (NSException * e) {
////                    //NSLog(@"Exception: %@", e);
////                }
////                @finally {
////                    //NSLog(@"finally");
////                }
////
////                speechNavigation =false;
////                [audioEngine stop];
////                [recognitionRequest endAudio];
////                [self onMonksTap];
////
////            }
////            isFinal = !result.isFinal;
////        }
////        if (error) {
////            [audioEngine stop];
////            [inputNode removeTapOnBus:0];
////            recognitionRequest = nil;
////            recognitionTask = nil;
////        }
////    }];
////
////    // Sets the recording format
////    AVAudioFormat *recordingFormat = [inputNode outputFormatForBus:0];
////    [inputNode installTapOnBus:0 bufferSize:1024 format:recordingFormat block:^(AVAudioPCMBuffer * _Nonnull buffer, AVAudioTime * _Nonnull when) {
////        [recognitionRequest appendAudioPCMBuffer:buffer];
////    }];
////
////    // Starts the audio engine, i.e. it starts listening.
////    [audioEngine prepare];
////    [audioEngine startAndReturnError:&error];
////    //NSLog(@"Say Something, I'm listening");
//}


- (IBAction)bottomBannerCloseAction:(id)sender {
    self.bottomBannerView.hidden=YES;
    [self closeBannerMethod:@"0" :notiyId];
}
- (IBAction)mainBannerCloseAction:(id)sender {
    self.mainBannerBgView.hidden=YES;
    [self closeBannerMethod:@"0" :notiyId];
}
- (IBAction)mainBannerPushBtnAction:(id)sender {
    
    [self bannerTapMethod];
}
-(void)bannerTapMethod
    {
        self.mainBannerBgView.hidden=YES;
        [self closeBannerMethod:@"1" :notiyId];
        @try
        {
            if(bannerNavigationDict!=nil)
            {
                //NSLog(@"%@",delegate2.notificationInfo);
                
                NSString * destination=[NSString stringWithFormat:@"%@",[bannerNavigationDict objectForKey:@"origin"]];
                if([destination isEqualToString:@"Dummy Positions"])
                {
                    
                    delegate2.pushNotificationSection=@"Dummy Positions";
                    bannerNavigationDict=nil;
                    [self.tabBarController setSelectedIndex:3];
                }
                else if([destination isEqualToString:@"INDIA_FEED"])
                {
                    
                    delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[bannerNavigationDict objectForKey:@"section"]];
                    bannerNavigationDict=nil;
                    [self.tabBarController setSelectedIndex:4];
                }
                
                else if([destination isEqualToString:@"NOTIFICATIONS"])
                {
                    
                    delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[bannerNavigationDict objectForKey:@"section"]];
                    bannerNavigationDict=nil;
                    if([delegate2.pushNotificationSection isEqualToString:@"INDIA_NOTIFICATIONS"])
                    {
                        [self.tabBarController setSelectedIndex:1];
                    }
                    else if([delegate2.pushNotificationSection isEqualToString:@"USA_NOTIFICATIONS"])
                    {
                        [self onUsaTap];
                    }
                    
                }
                else if([destination isEqualToString:@"ZAMBALA_GUIDE"])
                {
                    
                    delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[bannerNavigationDict objectForKey:@"section"]];
                    bannerNavigationDict=nil;
                    if([delegate2.pushNotificationSection isEqualToString:@"INDIA_ZAMBALA_GUIDE"])
                    {
                        [self.tabBarController setSelectedIndex:1];
                    }
                    else if([delegate2.pushNotificationSection isEqualToString:@"USA_ZAMBALA_GUIDE"])
                    {
                        [self onUsaTap];
                    }
                    
                }
                else if([destination isEqualToString:@"INDIA_MARKET_WATCH"])
                {
                    
                    delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[bannerNavigationDict objectForKey:@"section"]];
                    bannerNavigationDict=nil;
                    [self onWatchTap];
                    
                }
                else if([destination isEqualToString:@"INDIA_MARKET_EXPERT"])
                {
                    
                    delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[bannerNavigationDict objectForKey:@"section"]];
                    bannerNavigationDict=nil;
                    [self onMonksTap];
                    
                }
                else if([destination isEqualToString:@"US_MARKET_WATCH"])
                {
                    
                    delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[bannerNavigationDict objectForKey:@"section"]];
                    bannerNavigationDict=nil;
                    [self onUsaTap];
                    
                }
                else if([destination isEqualToString:@"USA_MARKET_EXPERTS"])
                {
                    
                    delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[bannerNavigationDict objectForKey:@"section"]];
                    bannerNavigationDict=nil;
                    [self onUsaTap];
                    
                }
                else if([destination isEqualToString:@"INDIA_TOP_PICKS"])
                {
                    
                    delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[bannerNavigationDict objectForKey:@"section"]];
                    bannerNavigationDict=nil;
                    [self onTopClick];
                    
                }
                else if([destination isEqualToString:@"USA_TOP_PICKS"])
                {
                    
                    delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[bannerNavigationDict objectForKey:@"section"]];
                    bannerNavigationDict=nil;
                    [self onUsaTap];
                    
                }
                
                else if([destination isEqualToString:@"Equities"])
                {
                    
                    delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[bannerNavigationDict objectForKey:@"section"]];
                    bannerNavigationDict=nil;
                    [self onEquityTap];
                }
                else if([destination isEqualToString:@"Derivatives"])
                {
                    
                    delegate2.pushNotificationSection=[NSString stringWithFormat:@"%@",[bannerNavigationDict objectForKey:@"section"]];
                    bannerNavigationDict=nil;
                    [self onDerivativeTap];
                }
                else if([destination isEqualToString:@"ZambalaTv"])
                {
                    
                    delegate2.pushNotificationSection=@"ZambalaTv";
                    bannerNavigationDict=nil;
                    [self.tabBarController setSelectedIndex:4];
                }
                else if([destination isEqualToString:@"INDIA_OPEN_ACCOUNT"])
                {
                    
                    delegate2.pushNotificationSection=@"OpenAccount";
                    bannerNavigationDict=nil;
                    [self onOpenTap];
                }
                else if([destination isEqualToString:@"USA_OPEN_ACCOUNT"])
                {
                    
                    delegate2.pushNotificationSection=@"USAOpenAccount";
                    bannerNavigationDict=nil;
                    [self onUsaTap];
                }
                else if([destination isEqualToString:@"USA_FEED"])
                {
                    
                    delegate2.pushNotificationSection=@"USA_FEED";
                    bannerNavigationDict=nil;
                    [self onUsaTap];
                }
                else if([destination isEqualToString:@"PlayStoreUpdate"])
                {
                    
                    delegate2.pushNotificationSection=@"PlayStoreUpdate";
                    bannerNavigationDict=nil;
                    NSString *iTunesLink = @"https://itunes.apple.com/us/app/zambala-stocks-by-zenwise/id1281356740?mt=8";
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                }
                else if([destination isEqualToString:@"Rewards"])
                {
                    
                    delegate2.pushNotificationSection=@"Rewards";
                    bannerNavigationDict=nil;
                    WalletView * wallet=[self.storyboard instantiateViewControllerWithIdentifier:@"WalletView"];
                    
                    [self presentViewController:wallet animated:YES completion:nil];
                }
            }
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
        
    }

-(void)bannerMethod
{
    @try
    {
        
        TagEncode * enocde=[[TagEncode alloc]init];
        NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
        NSString * number= [loggedInUser stringForKey:@"profilemobilenumber"];
        enocde.inputRequestString=@"banner";
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:[NSString stringWithFormat:@"%@notification?mobilenumber=%@&clientid=%@&authtoken=%@",delegate2.baseUrl,number,delegate2.userID,delegate2.zenwiseToken]];
        
        [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
            
           if(dict)
           {
               notiyId=@"";
               NSString * message = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"message"]];
               if([message isEqualToString:@"success"])
               {
                   NSString * notificationType = [NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"notificationtype"]];
                   if([notificationType isEqualToString:@"1"])
                   {
                      dispatch_async(dispatch_get_main_queue(), ^{
                       self.mainBannerBgView.hidden=NO;
                       self.bottomBannerView.hidden=YES;
                       self.mainBannerTitle.text = [NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"heading"]];
                       self.mainBannerDescLbl.text = [NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"message"]];
                       notiyId = [NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"id"]];
                       [self.mainBannerPushBtn setTitle:[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"buttontext"]] forState:UIControlStateNormal];
                       bannerNavigationDict = [[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"landingpage"];
                      });
                       
                       
                   }
                   else if([notificationType isEqualToString:@"2"])
                   {
                       dispatch_async(dispatch_get_main_queue(), ^{
                           self.mainBannerBgView.hidden=YES;
                           self.bottomBannerView.hidden=NO;
                           self.bottomBannerDescLbl.text = [NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"message"]];
                           notiyId = [NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"id"]];
                           bannerNavigationDict = [[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"landingpage"];
                       });
                   }
               }
           }
        }];
    }

@catch (NSException * e) {
    NSLog(@"Exception: %@", e);
}
@finally {
    NSLog(@"finally");
}


}
-(void)closeBannerMethod:(NSString *)notifyStr :(NSString *)notificationId
{
    @try
    {
        TagEncode * enocde=[[TagEncode alloc]init];
        NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
        NSString * number= [loggedInUser stringForKey:@"profilemobilenumber"];
        enocde.inputRequestString=@"closebanner";
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:number];
        [inputArray addObject:notifyStr];
        [inputArray addObject:notificationId];
        [inputArray addObject:delegate2.zenwiseToken];
        [inputArray addObject:[NSString stringWithFormat:@"%@notification/act",delegate2.baseUrl]];
        [inputArray addObject:delegate2.userID];
        
        [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            NSLog(@"%@",dict);
             dispatch_async(dispatch_get_main_queue(), ^{
            self.bottomBannerView.hidden=YES;
            self.mainBannerBgView.hidden=YES;
             });
        }];
    }
    
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
    
    
}
@end
