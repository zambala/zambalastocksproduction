//
//  ExpMonksCell.h
//  PremiumServices
//
//  Created by zenwise mac 2 on 12/30/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExpMonksCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *userImg;

@property (strong, nonatomic) IBOutlet UILabel *nameLbl, *subscribeLbl;

@property (strong, nonatomic) IBOutlet UILabel *subscribeLabel;
@property (strong, nonatomic) IBOutlet UIView *bgView;

@end
