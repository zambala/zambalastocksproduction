//
//  ViewController.h
//  PayPage
//
//  Created by zenwise mac 2 on 12/22/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]


@interface PayOut : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>

{
    NSArray *bankDataArray;
    UIPickerView *bankpickerView;
    UIView *bankViewContainer;
    
    NSString *bankDataStr;
    
    NSArray *accountDataArray;
    UIPickerView *accountPickerView;
    UIView *accountViewContainer;
    
    NSString *accountDataStr;

}
@property (strong, nonatomic) IBOutlet UIView *payOutView1;
@property (strong, nonatomic) IBOutlet UIView *payOutView2;
@property (strong, nonatomic) IBOutlet UIButton *payOutSubmitBtn;

@property (strong, nonatomic) IBOutlet UIScrollView *payOutScroll;

@property (strong, nonatomic) IBOutlet UILabel *segmentLbl, *dateLbl, *accountLbl, *withDrawalLbl;

@property (strong, nonatomic) IBOutlet UITextField *withdrawalTxtField;

@property (strong, nonatomic) IBOutlet UIButton *bankBtn, *accountBtn, *submitBtn;

- (IBAction)backBtn:(id)sender;






@end

