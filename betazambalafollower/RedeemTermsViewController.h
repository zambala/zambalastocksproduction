//
//  RedeemTermsViewController.h
//  betazambalafollower
//
//  Created by Zenwise Technologies Private Limited on 20/08/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RedeemTermsViewController : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIImageView *voucherImageView;
@property (weak, nonatomic) IBOutlet UILabel *voucherNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *vouchercategorylabel;
@property (weak, nonatomic) IBOutlet UILabel *validityLabel;
@property (weak, nonatomic) IBOutlet UIButton *denominationButton;
@property (weak, nonatomic) IBOutlet UILabel *termsAndConditionsLabel;
@property (weak, nonatomic) IBOutlet UIButton *redeemButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (nonatomic,retain)NSMutableDictionary *getListArray;
@property (weak, nonatomic) IBOutlet UIView *doneView;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property NSMutableDictionary *vaucherResponseDictionary;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;

@property NSString * zambalaPointValue;
@property NSString * rupeeString;
@property NSString * minZambalaPoints;
@property (weak, nonatomic) IBOutlet UIView *popUpView;
@property (weak, nonatomic) IBOutlet UIView *popUpBGView;
@property (weak, nonatomic) IBOutlet UIView *imageBGView;
@property (weak, nonatomic) IBOutlet UIButton *okayButton;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;

@end
