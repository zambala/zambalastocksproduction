//
//  CoupansView.m
//  betazambalafollower
//
//  Created by Zenwise Technologies on 17/04/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import "CoupansView.h"
#import "TagEncode.h"
#import "AppDelegate.h"
#import "CoupansCell.h"
@interface CoupansView ()<UITableViewDelegate,UITableViewDataSource>
{
    AppDelegate * delegate1;
    NSMutableDictionary * coupansDict;
}

@end

@implementation CoupansView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.coupansTblView.delegate=self;
    self.coupansTblView.dataSource=self;
    @try
    {
    TagEncode * enocde=[[TagEncode alloc]init];
    enocde.inputRequestString=@"coupans";
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    [inputArray addObject:[NSString stringWithFormat:@"%@coupon/getcoupons?clientid=%@&authtoken=%@",delegate1.baseUrl,delegate1.userID,delegate1.zenwiseToken]];
    
    [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
        
        //NSLog(@"%@",dict);
        coupansDict=[[NSMutableDictionary alloc]init];
        coupansDict=[dict mutableCopy];
        dispatch_async(dispatch_get_main_queue(), ^{
            
         [self.coupansTblView reloadData];
            
        });
      
    }];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    @try
    {
    int i=(int)[[[[coupansDict objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"coupans"] count];
    return i;
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @try
    {
     CoupansCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    
    cell.coupanShrotDescription.text=[NSString stringWithFormat:@"%@",[[[[[coupansDict objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"coupans"] objectAtIndex:indexPath.row] objectForKey:@"shortdescription"]];
    cell.coupanDescription.text=[NSString stringWithFormat:@"%@",[[[[[coupansDict objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"coupans"] objectAtIndex:indexPath.row] objectForKey:@"description"]];
    NSString * coupanName=[NSString stringWithFormat:@"Apply %@",[[[[[coupansDict objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"coupans"] objectAtIndex:indexPath.row] objectForKey:@"couponcode"]];
    [cell.applyAction setTitle:coupanName forState:UIControlStateNormal];
    
     return cell;
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
