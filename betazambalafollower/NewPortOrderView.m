//
//  NewPortOrderView.m
//  PremiumServices
//
//  Created by zenwise mac 2 on 7/22/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "NewPortOrderView.h"
#import "NewPortOrderCell.h"
#import "AppDelegate.h"
#import "MainOrdersViewController.h"
#import "PSWebSocket.h"
#import "PSWebSocketDriver.h"


@interface NewPortOrderView ()<PSWebSocketDelegate>
{
    
    NSString * orderType;
    NSDictionary *headers ;
    
    NSMutableURLRequest *request;
    
    NSDictionary *parameters ;
    
    NSData *postData ;
    AppDelegate * delegate;
    NSURL * url1;
    NSMutableArray * orderSegment;
    NSMutableArray * buySellArray;
    NSIndexPath * limitRow;
    NSString * price1;
    NSString * price2;
    NSString * price3;
    NSString * price4;
    NSString * price5;
    NSMutableArray * quantityArray;
    NSMutableArray * limitPriceArray;
    NSMutableArray * testCountArray;
    NSMutableDictionary * instrumentTokensDict;
    NSMutableArray * localInstrumentToken;
    NSString * string1;
    bool temp;
    int value;
    int packetsLength;
    int packetsNumber;
    float lastPriceFlaotValue;
    float closePriceFlaotValue;
    NSMutableDictionary *tmpDict;
    
    
    float changeFloatValue;
    
    float changeFloatPercentageValue;
    NSMutableArray * allKeysList;
    NSMutableArray * allKeysList1;


}

@end

@implementation NewPortOrderView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    delegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    temp=YES;
    
    allKeysList=[[NSMutableArray alloc]init];
    localInstrumentToken=[[NSMutableArray alloc]init];
    
    [self.marketBtn setImage:[UIImage imageNamed:@"radioSelected.png"] forState:UIControlStateSelected];
    [self.marketBtn setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    [self.limitBtn setImage:[UIImage imageNamed:@"radioSelected.png"] forState:UIControlStateSelected];
    [self.limitBtn setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    
    orderSegment=[[NSMutableArray alloc]init];
     buySellArray=[[NSMutableArray alloc]init];
     quantityArray=[[NSMutableArray alloc]init];
    limitPriceArray=[[NSMutableArray alloc]init];
    
    [self marketBtnAction:self.marketBtn];
    
    [self portfolioDepthServer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)marketBtnAction:(id)sender {
    
    self.marketBtn.selected=YES;
    self.limitBtn.selected=NO;
    
    orderType=@"MARKET";
    [self.portTblView reloadData];
    
}

- (IBAction)limitBtnAction:(id)sender {
    
    self.marketBtn.selected=NO;
    self.limitBtn.selected=YES;
     orderType=@"LIMIT";
    [self.portTblView reloadData];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _countArray.count;    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewPortOrderCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    
    // Here we use the provided setImageWithURL: method to load the web image
    // Ensure you use a placeholder image otherwise cells will be initialized with no image
    
    if([orderType isEqualToString:@"MARKET"])
    {
        cell.limitLbl.hidden=YES;
        cell.limitTxt.hidden=YES;
        cell.limitUnderView.hidden=YES;
        orderType=@"MARKET";
        
    }
    
    else if([orderType isEqualToString:@"LIMIT"])
    {
        cell.limitLbl.hidden=NO;
        cell.limitTxt.hidden=NO;
        cell.limitUnderView.hidden=NO;
        orderType=@"LIMIT";
    }
    
    
    cell.companyLbl.text=[[_countArray objectAtIndex:indexPath.row] objectForKey:@"symbol"];
    cell.qtyTxt.text=[[_countArray objectAtIndex:indexPath.row] objectForKey:@"quantity"];
    
    [quantityArray addObject:cell.qtyTxt.text];
    
    NSString * segment=[[[self.portfolioDepthResponseDictionary objectForKey:@"data"] objectAtIndex:0] objectForKey:@"segment"];
    
    int segmentnumber=[segment intValue];
    
    if(segmentnumber==1)
    {
        
        cell.segmentLbl.text=@"NSE CASH";
        [orderSegment addObject:@"NSE"];
    }
    
    else if(segmentnumber==2)
    {
        
        cell.segmentLbl.text=@"BSE CASH";
        [orderSegment addObject:@"BSE"];
    }
    
    if([delegate.transaction isEqualToString:@"BUY"])
    {
//        transTypeStr=@"BUY";
//        sellCheck=@"Buyacted";
//        [self.buySellSegment setSelectedSegmentIndex:0];
        
//        self.buySellSegment.tintColor=[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1];
        
        
        [buySellArray addObject:@"BUY"];
        
        [cell.sellBtn.layer setBorderWidth:1.0f];
        
        [cell.sellBtn.layer setBorderColor:[[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:0.7] CGColor]];
        
        [cell.sellBtn setTitleColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1]  forState:UIControlStateNormal];
        
        
        [cell.buyBtn setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1]];
    }
    else if([delegate.transaction isEqualToString:@"SELL"])
    {
         [buySellArray addObject:@"SELL"];
//        transTypeStr=@"SELL";
//        sellCheck=@"Sellacted";
//        
//        [self.buySellSegment setSelectedSegmentIndex:1];
//        
//        
//        self.buySellSegment.tintColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
        
        [cell.sellBtn setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1]];
        
        
        
        
        
        [cell.buyBtn.layer setBorderWidth:1.0f];
        [cell.buyBtn.layer setBorderColor:[[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:0.7] CGColor]];
        
        [cell.buyBtn setTitleColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1]  forState:UIControlStateNormal];
        

        
    }
    
    [cell.buyBtn addTarget:self action:@selector(buyAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.sellBtn addTarget:self action:@selector(sellAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.transactionLbl.text=delegate.transaction;

     cell.limitTxt.text=@"0";

    [limitPriceArray addObject:@"0"];
    
    cell.qtyTxt.delegate=self;
    cell.limitTxt.delegate=self;
    
    
    if(allKeysList.count>0)
    {
        
        
        NSMutableDictionary *tmp = [[NSMutableDictionary alloc]initWithDictionary:[instrumentTokensDict valueForKey:[allKeysList objectAtIndex:indexPath.row]]];
    
        //NSLog(@"%@",tmp);
        
        cell.LTPLbl.text=[tmp objectForKey:@"LastPriceValue"];
        
        
        cell.bestBidLbl.text=[tmp objectForKey:@"BIDPrice"];
        
        NSString * bidQty=[NSString stringWithFormat:@"(%@)",[tmp objectForKey:@"BIDQty"]];
        cell.bestBidQtyLbl.text=bidQty;
        cell.bestAskLbl.text=[tmp objectForKey:@"ASKPrice"];
        NSString * askQty=[NSString stringWithFormat:@"(%@)",[tmp objectForKey:@"ASKQty"]];

        cell.bestQtyLbl.text=askQty;
        
        NSString * changeStrPer=[NSString stringWithFormat:@"%@",[tmp objectForKey:@"ChangePercentageValue"]];
        NSString * str=@"%";
        
        if([changeStrPer containsString:@"-"])
        {
            cell.chngLbl.text = [NSString stringWithFormat:@"(%@%@)",[tmp valueForKey:@"ChangePercentageValue"],str];
            cell.chngLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
        }
        else{
            cell.chngLbl.text = [NSString stringWithFormat:@"(%@%@)",[tmp valueForKey:@"ChangePercentageValue"],str];
            cell.chngLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
        }

        
    }
    
  
    return cell;
}

-(void)portfolioDepthServer
{
    headers = @{ @"cache-control": @"no-cache",
                 @"content-type": @"application/json",
                 @"authtoken":delegate.zenwiseToken,
                    @"deviceid":delegate.currentDeviceId
                 };
    
    request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower/advices",delegate.baseUrl]]
                                      cachePolicy:NSURLRequestUseProtocolCachePolicy
                                  timeoutInterval:10.0];
    
    parameters = @{
                   @"clientid":delegate.userID,
                   @"messagetypeid":@4,
                   @"active":@(true),
                   @"issubscribed":@(true),
                   @"messageid":delegate.protFolioUserID
                   };
    
    postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==403)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                            
                                                        }
                                                        else if([httpResponse statusCode]==401)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                        self.portfolioDepthResponseDictionary= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        //NSLog(@"Portfolio Depth:%@",self.portfolioDepthResponseDictionary);
                                                        
                                                        _countArray=[[NSMutableArray alloc]init];
                                                        self.countArray = [[[[self.portfolioDepthResponseDictionary objectForKey:@"data"] objectAtIndex:0] objectForKey:@"portfoliojson"] objectForKey:@"stocks"];
                                                        
                                                        for(int i=0;i<self.countArray.count;i++)
                                                        {
                                                        
                                                            NSString * localString=[[[[[[self.portfolioDepthResponseDictionary objectForKey:@"data"] objectAtIndex:0] objectForKey:@"portfoliojson"] objectForKey:@"stocks"] objectAtIndex:i]objectForKey:@"instrumentid"];
                                                            
                                                            int instrument=[localString intValue];
                                                        
                                                            [localInstrumentToken addObject:[NSNumber numberWithInt:instrument]];
                                                            
                                                        }
                                                        
                                                        
//                                                        for (int i=0; i<self.countArray.count; i++) {
//                                                            [self.pieChartPercentageArray addObject:[[self.countArray objectAtIndex:i] objectForKey:@"percentage"]];
//                                                        }
                                                        
                                                        }
                                                        
                                                    }
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        [self webSocket];
                                                        
                                                        
                                                        
                                                        
                                                    });
                                                    
                                                }];
    [dataTask resume];
    
    
    
    //    self.portfolioDepthResponseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    //
    //    //NSLog(@"Portfolio Depth:%@",self.portfolioDepthResponseDictionary);
}


-(void)sellAction:(UIButton *)sender {
//    transTypeStr=@"SELL";
    //    [self.sellBtnOutlet setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1]];
    //
    //    [self.buyBtnOutlet setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:0.1]];
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.portTblView];
    
    
    
    NSIndexPath *indexPath = [self.portTblView indexPathForRowAtPoint:buttonPosition];
    
    //NSLog(@"Index path:%ld",(long)indexPath.row);
    NewPortOrderCell *cell = [self.portTblView cellForRowAtIndexPath:indexPath];
    
    [cell.sellBtn setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1]];
    
    [cell.sellBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [cell.buyBtn setBackgroundColor:[UIColor clearColor]];
    
    [cell.buyBtn.layer setBorderWidth:1.0f];
    [cell.buyBtn.layer setBorderColor:[[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:0.7] CGColor]];
    
    [cell.buyBtn setTitleColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1]  forState:UIControlStateNormal];
//    sellCheck=@"Sellacted";
    
//    [self limitAction];
    
}

-(void)buyAction:(UIButton *)sender {
//    transTypeStr=@"BUY";
    //    [self.sellBtnOutlet setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:0.1]];
    //
    //    [self.buyBtnOutlet setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1]];
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.portTblView];
    
    
    
    NSIndexPath *indexPath = [self.portTblView indexPathForRowAtPoint:buttonPosition];
    
    //NSLog(@"Index path:%ld",(long)indexPath.row);
    NewPortOrderCell *cell = [self.portTblView cellForRowAtIndexPath:indexPath];
    
    [cell.sellBtn.layer setBorderWidth:1.0f];
    
    [cell.sellBtn.layer setBorderColor:[[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:0.7] CGColor]];
    
    [cell.sellBtn setTitleColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1]  forState:UIControlStateNormal];
    
    [cell.sellBtn setBackgroundColor:[UIColor clearColor]];
    
    
    
    [cell.buyBtn setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1]];
    [cell.buyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    sellCheck=@"Buyacted";
//    [self limitAction];
}




- (IBAction)portOrderSubmit:(id)sender {
    
    
    testCountArray=[[NSMutableArray alloc]init];
    
    @try {
        
        for(int i=0;i<quantityArray.count;i++)
        {
            
            [testCountArray addObject:@"0"];
            
            
         NSURLSession *session = [NSURLSession sharedSession];
        
        
        
        
        if ([orderType isEqualToString:@"MARKET"])
        {
            
            
            url1 = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.kite.trade/orders/regular/?api_key=%@&access_token=%@&tradingsymbol=%@&exchange=%@&transaction_type=%@&order_type=%@&quantity=%@&product=CNC&validity=DAY",delegate.APIKey,delegate.accessToken,[[_countArray objectAtIndex:i] objectForKey:@"symbol"],[orderSegment objectAtIndex:i], [buySellArray objectAtIndex:i], orderType,[quantityArray objectAtIndex:i]]];
        }
        
        else if ([orderType isEqualToString:@"LIMIT"])
        {
            
             url1 = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.kite.trade/orders/regular/?api_key=%@&access_token=%@&tradingsymbol=%@&exchange=%@&transaction_type=%@&order_type=%@&quantity=%@&product=CNC&validity=DAY&price=%@",delegate.APIKey,delegate.accessToken,[[_countArray objectAtIndex:i] objectForKey:@"symbol"],[orderSegment objectAtIndex:i], [buySellArray objectAtIndex:i], orderType,[quantityArray objectAtIndex:i],[limitPriceArray objectAtIndex:i]]];
            
//            url1 = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.kite.trade/orders/regular/?api_key=%@&access_token=%@&tradingsymbol=%@&exchange=%@&transaction_type=%@&order_type=%@&quantity=%@&product=NRML&validity=DAY&price=%@",delegate1.APIKey,delegate1.accessToken,[symbolArray objectAtIndex:0],[[details objectAtIndex:0] objectForKey:@"exchange"],[buySellArr objectAtIndex:0], orderType,[[details objectAtIndex:0] objectForKey:@"lotsize"],price1]];
            
            
        }
        
        //NSLog(@"url 1%@",url1);
        
        
        
        
        
            NSMutableURLRequest *request1 = [NSMutableURLRequest requestWithURL:url1
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:60.0];
            
            
            
            [request1 addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request1 addValue:@"application/json" forHTTPHeaderField:@"Accept"];
            
            [request1 setHTTPMethod:@"POST"];
            
            NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request1 completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                if(data!=nil)
                {
                    
                    NSDictionary *orderDict=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                    //NSLog(@"orderDict----%@",orderDict);
                    
                    NSString * status=[NSString stringWithFormat:@"%@",[orderDict objectForKey:@"status"]];
                    
                    //NSLog(@"%@",status);


                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        
                        if(testCountArray.count==quantityArray.count)
                        {
                            
                        if([status isEqualToString:@"success"])
                        {
                            
                            
                            
                            
                           
                                UIAlertController * alert = [UIAlertController
                                                             alertControllerWithTitle:@"Order Status"
                                                             message:@" Order was placed successfully"
                                                             preferredStyle:UIAlertControllerStyleAlert];
                                
                                //Add Buttons
                                
                                UIAlertAction* okButton = [UIAlertAction
                                                           actionWithTitle:@"Ok"
                                                           style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action) {
                                                               //Handle your yes please button action here
                                                               MainOrdersViewController *nextPage = [self.storyboard instantiateViewControllerWithIdentifier:@"orders"];
                                                               [self presentViewController:nextPage animated:YES completion:nil];
                                                               
                                                               
                                                           }];
                                //Add your buttons to alert controller
                                
                                [alert addAction:okButton];
                                
                                
                                [self presentViewController:alert animated:YES completion:nil];
                                
                                
                            }
                        
                        
                        else
                        {
                            UIAlertController * alert = [UIAlertController
                                                         alertControllerWithTitle:@"Order Status"
                                                         message:@"Please try again"
                                                         preferredStyle:UIAlertControllerStyleAlert];
                            
                            //Add Buttons
                            
                            UIAlertAction* okButton = [UIAlertAction
                                                       actionWithTitle:@"Ok"
                                                       style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           //Handle your yes please button action here
                                                           
                                                       }];
                            //Add your buttons to alert controller
                            
                            [alert addAction:okButton];
                            
                            
                            [self presentViewController:alert animated:YES completion:nil];
                        }
                        
                        }
                        
                        
                        
                    });
                }
                
                
                
            }];
            
            [postDataTask resume];
            
        }
//
                    }
                                   
//
    
        
        //    MainOrdersViewController *nextPage = [self.storyboard instantiateViewControllerWithIdentifier:@"orders"];
        //    [self.navigationController pushViewController:nextPage animated:YES];
        //
        
        
        
        
        
    
    @catch (NSException *exception) {
        
    } @finally {
        
    }
    

    
    
    
    
    
}
                    
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
                    
            {
                
                
                CGPoint pointInsuperview=[textField.superview convertPoint:textField.center toView:self.portTblView];
                NSIndexPath *  indexPath=[self.portTblView indexPathForRowAtPoint:pointInsuperview];
                //NSLog(@"%lu",(long)indexPath.row);
                
                
                limitRow=indexPath;
                
                
                
                
                
                NewPortOrderCell * cell=(NewPortOrderCell*)[self.portTblView cellForRowAtIndexPath:indexPath];
                
                if(textField==cell.qtyTxt)
                {
                
                NSString * localStr = [cell.qtyTxt.text stringByReplacingCharactersInRange:range withString:string];
                
                if(localStr.length>0)
                {
                    @try {
                        
//                        if(indexPath.row==0)
//                        {
//                            price1=localStr;
//                            //NSLog(@"price1%@",price1);
//                        }
//                        else if(indexPath.row==1)
//                        {
//                            price2=localStr;
//                            //NSLog(@"price2%@",price2);
//                        }
//                        
//                        else if(indexPath.row==2)
//                        {
//                            price3=localStr;
//                            //NSLog(@"price3%@",price3);
//                        }
//                        
//                        else if(indexPath.row==3)
//                        {
//                            price4=localStr;
//                            //NSLog(@"price4%@",price4);
//                        }
//                        
//                        else if(indexPath.row==4)
//                        {
//                            price5=localStr;
//                            //NSLog(@"price4%@",price4);
//                        }
                        
                        
                        [quantityArray replaceObjectAtIndex:indexPath.row withObject:localStr];
                        
                        
                    }
                    @catch (NSException *exception) {
                        //NSLog(@"Something missing...%@",exception);
                    }
                    @finally {
                        
                        
                        
                        
                        
                    }
                    
                    
                    
                    
                    
                    
                    return YES;
                    
                    
                }
                    
                }
                
                
                else if(textField==cell.limitTxt)
                    
                {
                    NSString * localStr1 = [cell.limitTxt.text stringByReplacingCharactersInRange:range withString:string];
                    
                    if(localStr1.length>0)
                    {
                        @try {
                            
                            //                        if(indexPath.row==0)
                            //                        {
                            //                            price1=localStr;
                            //                            //NSLog(@"price1%@",price1);
                            //                        }
                            //                        else if(indexPath.row==1)
                            //                        {
                            //                            price2=localStr;
                            //                            //NSLog(@"price2%@",price2);
                            //                        }
                            //
                            //                        else if(indexPath.row==2)
                            //                        {
                            //                            price3=localStr;
                            //                            //NSLog(@"price3%@",price3);
                            //                        }
                            //
                            //                        else if(indexPath.row==3)
                            //                        {
                            //                            price4=localStr;
                            //                            //NSLog(@"price4%@",price4);
                            //                        }
                            //
                            //                        else if(indexPath.row==4)
                            //                        {
                            //                            price5=localStr;
                            //                            //NSLog(@"price4%@",price4);
                            //                        }
                            
                            
                            [limitPriceArray replaceObjectAtIndex:indexPath.row withObject:localStr1];
                            
                            
                        }
                        @catch (NSException *exception) {
                            //NSLog(@"Something missing...%@",exception);
                        }
                        @finally {
                            
                            
                            
                            
                            
                        }
                        
                        
                        
                        return YES;

                    
                    
                }
                return YES;
            }
                
}

-(void)webSocket
{
    
    @try {
        instrumentTokensDict = [[NSMutableDictionary alloc]init];
        //pocket socket//
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"wss://websocket.kite.trade/?api_key=%@&user_id=%@&public_token=%@",delegate.APIKey,delegate.userID,delegate.publicToken]];
        
        //NSLog(@"user %@",delegate.userID);
        //NSLog(@"user %@",delegate.publicToken);
        
        
        
        NSURLRequest *request2 = [NSURLRequest requestWithURL:url];
        
        
        //     create the socket and assign delegate
        self.socket = [PSWebSocket clientSocketWithRequest:request2];
        self.socket.delegate = self;
        //
        //    self.socket.delegateQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        //     open socket
        
        //        if(delegate2.instrumentToken.count>0)
        //    {
        //
        //    }
        
        [self.socket open];

        
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    
}









- (void)webSocketDidOpen:(PSWebSocket *)webSocket {
    //NSLog(@"The websocket handshake completed and is now open!");
    
   
    

    
    //     instrumentTokens=[[NSMutableArray alloc]init];
    
    
    
    //   [instrumentTokens addObject:[NSNumber numberWithInt:11547906]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:4911105]];
    //   [instrumentTokens addObject:[NSNumber numberWithInt:11508226]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:11535618]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:24026370]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:12344066]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:4954113]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:20517634]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:20517890]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:217138949]];
    
//     localInstrumentToken=[[[localInstrumentToken reverseObjectEnumerator] allObjects] mutableCopy];
    
    if(localInstrumentToken.count>0)
    {
        
        NSDictionary * subscribeDict=@{@"a": @"subscribe", @"v":localInstrumentToken};
        
        
        NSData *json;
        
        NSError * error;
        
        
        
        
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:subscribeDict])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:subscribeDict options:NSJSONWritingPrettyPrinted error:&error];
            
            // If no errors, let's view the JSON
            if (json != nil && error == nil)
            {
                string1 = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                
                //            delegate2.message=[NSString stringWithFormat:@"%@",str];
                //
                
                //NSLog(@"JSON: %@",string1);
                
            }
        }
        
        
        [self.socket send:string1];
        
        
        
        
        
        
        NSArray * mode=@[@"full",localInstrumentToken];
        
        subscribeDict=@{@"a": @"mode", @"v": mode};
        
        
        
        //     Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:subscribeDict])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:subscribeDict options:NSJSONWritingPrettyPrinted error:&error];
            
            
            // If no errors, let's view the JSON
            if (json != nil && error == nil)
            {
                _message = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                
                //            delegate2.message=[NSString stringWithFormat:@"%@",str];
                
                //NSLog(@"JSON: %@",_message);
                
            }
        }
        [self.socket send:_message];
        
    }
    
    
}




- (void)webSocket:(PSWebSocket *)webSocket didReceiveMessage:(id)message {
    
    
    @try {
        //NSLog(@"The websocket received a message: %@",message);
        
        
        
        NSData * data=[NSKeyedArchiver archivedDataWithRootObject:message];
        
        
        
        
        
        
        if(data.length > 247)
        {
            
            
            //NSLog(@"The websocket received a message: %@",message);
            
            
            id packets = [message subdataWithRange:NSMakeRange(0,2)];
            packetsNumber = CFSwapInt16BigToHost(*(int16_t*)([packets bytes]));
            //NSLog(@" number of packets %i",packetsNumber);
            
            int startingPosition = 2;
            //            if([instrumentTokensDict allKeys])
            //                [instrumentTokensDict removeAllObjects];
            
            for( int i =0; i< packetsNumber ; i++)
            {
                
                
                NSData * packetsLengthData = [message subdataWithRange:NSMakeRange(startingPosition,2)];
                packetsLength = CFSwapInt16BigToHost(*(int16_t*)([packetsLengthData bytes]));
                startingPosition = startingPosition + 2;
                
                id packetQuote = [message subdataWithRange:NSMakeRange(startingPosition,packetsLength)];
                
                
                
                
                id instrumentTokenData = [packetQuote subdataWithRange:NSMakeRange(0, 4)];
                int32_t instrumentTokenValue = CFSwapInt32BigToHost(*(int32_t*)([instrumentTokenData bytes]));
                
                id lastPrice = [packetQuote subdataWithRange:NSMakeRange(4,4)];
                int32_t lastPriceValue = CFSwapInt32BigToHost(*(int32_t*)([lastPrice bytes]));
                
                
                
                
                
                
                id closingPrice = [packetQuote subdataWithRange:NSMakeRange(40,4)];
                int32_t closePriceValue = CFSwapInt32BigToHost(*(int32_t*)([closingPrice bytes]));
                
                
                
                
                
                
                //                id marketDepth = [packetQuote subdataWithRange:NSMakeRange(44,120)];
                //                int32_t marketDepthValue = CFSwapInt32BigToHost(*(int32_t*)([marketDepth bytes]));
                //
                //                //NSLog(@"%@",marketDepth);
                
                //                NSString * exchange=[NSString stringWithFormat:@"%@",[[delegate2.filteredCompanyName objectAtIndex:i]valueForKey:@"segment"]];
                //
                //                if([exchange containsString:@"CDS"])
                //                {
                //                    lastPriceFlaotValue = (float)lastPriceValue/10000000;
                //
                //                    closePriceFlaotValue = (float)closePriceValue/10000000;
                //
                //                    //NSLog(@"CLOSE %f",closePriceFlaotValue);
                //
                //                    changeFloatValue = lastPriceFlaotValue-closePriceFlaotValue;
                //
                //                    changeFloatPercentageValue = (changeFloatValue/closePriceFlaotValue)/100;
                //                }
                //                else
                //                {
                
                
                lastPriceFlaotValue = (float)lastPriceValue/100;
                
                closePriceFlaotValue = (float)closePriceValue/100;
                
                //NSLog(@"CLOSE %f",closePriceFlaotValue);
                
                changeFloatValue = lastPriceFlaotValue-closePriceFlaotValue;
                
                changeFloatPercentageValue = ((lastPriceFlaotValue-closePriceFlaotValue) *100/closePriceFlaotValue);
                //                }
                
                
                id marketDepthBID = [packetQuote subdataWithRange:NSMakeRange(44,60)];
                int32_t marketDepthValueBID = CFSwapInt32BigToHost(*(int32_t*)([marketDepthBID bytes]));
                
                //NSLog(@"%d",marketDepthValueBID);
                
                id marketDepthBIDQuantity = [marketDepthBID subdataWithRange:NSMakeRange(0,4)];
                int32_t marketDepthValueBIDQuantity = CFSwapInt32BigToHost(*(int32_t*)([marketDepthBIDQuantity bytes]));
                
                //NSLog(@"%i",marketDepthValueBIDQuantity);
                
                
                
                
                
                id marketDepthBIDPrice = [marketDepthBID subdataWithRange:NSMakeRange(4,4)];
                int32_t marketDepthValueBIDPriceInt = CFSwapInt32BigToHost(*(int32_t*)([marketDepthBIDPrice bytes]));
                
                float marketDepthValueBIDPrice = (float)marketDepthValueBIDPriceInt/100;
                
                //NSLog(@"%.2f",marketDepthValueBIDPrice);
                
                
                
                id marketDepthASK = [packetQuote subdataWithRange:NSMakeRange(104,60)];
                int32_t marketDepthValueASK = CFSwapInt32BigToHost(*(int32_t*)([marketDepthASK bytes]));
                
                //NSLog(@"%d",marketDepthValueASK);
                
                id marketDepthASKQuantity = [marketDepthASK subdataWithRange:NSMakeRange(0,4)];
                int32_t marketDepthValueASKQuantity = CFSwapInt32BigToHost(*(int32_t*)([marketDepthASKQuantity bytes]));
                
                
                //NSLog(@"%i",marketDepthValueASKQuantity);
                
                
                
                
                
                
                id marketDepthASKPrice = [marketDepthASK subdataWithRange:NSMakeRange(4,4)];
                int32_t marketDepthValueASKPriceInt = CFSwapInt32BigToHost(*(int32_t*)([marketDepthASKPrice bytes]));
                
                float marketDepthValueASKPrice = (float)marketDepthValueASKPriceInt/100;
                
                //NSLog(@"%.2f",marketDepthValueASKPrice);
                
                startingPosition = startingPosition + packetsLength;
                
                
                @autoreleasepool {
                    
                    tmpDict = [[NSMutableDictionary alloc]init];
                    [tmpDict setValue:[NSString stringWithFormat:@"%d",instrumentTokenValue] forKey:@"InstrumentToken"];
                    
                    NSString *tmpInstrument = [NSString stringWithFormat:@"%d",instrumentTokenValue];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%.2f",closePriceFlaotValue] forKey:@"ClosePriceValue"];
                    
                    
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%.2f",lastPriceFlaotValue] forKey:@"LastPriceValue"];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%.2f",changeFloatValue] forKey:@"ChangeValue"];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%.2f",changeFloatPercentageValue] forKey:@"ChangePercentageValue"];
                    
                     [tmpDict setValue:[NSString stringWithFormat:@"%.2f",marketDepthValueBIDPrice] forKey:@"BIDPrice"];
                    
                     [tmpDict setValue:[NSString stringWithFormat:@"%d",marketDepthValueBIDQuantity] forKey:@"BIDQty"];
                    
                     [tmpDict setValue:[NSString stringWithFormat:@"%.2f",marketDepthValueASKPrice] forKey:@"ASKPrice"];
                    
                     [tmpDict setValue:[NSString stringWithFormat:@"%d",marketDepthValueASK] forKey:@"ASKQty"];
                    
                    
                    //NSLog(@"%@",tmpDict);
                    
                    
                    [instrumentTokensDict setValue:tmpDict forKeyPath:tmpInstrument];
                    
                    //NSLog(@"%@",instrumentTokensDict);
                    
                    
                    
                    
                    
                    //  [shareListArray addObject:tmpDict];
                    
                }
                
                
            }
            
            //NSLog(@"%@",instrumentTokensDict);
            
//            if(allKeysList.count>0)
//            {
//                [allKeysList removeAllObjects];
//            }
//            allKeysList = [[instrumentTokensDict allKeys]mutableCopy];
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                self.portTblView.delegate=self;
//                self.portTblView.dataSource=self;
//                
//                [self.portTblView reloadData];
//                
//                //            //NSLog(@"%@",shareListArray);
//                
//                
//            });
//            
            
            if(allKeysList.count>0)
            {
                [allKeysList removeAllObjects];
            }
            allKeysList = [[instrumentTokensDict allKeys]mutableCopy];
            
            //NSLog(@"%@",allKeysList);
            
            allKeysList1=[[NSMutableArray alloc]init];
            
            for(int i=0;i<localInstrumentToken.count;i++)
            {
                
                for(int j=0;j<allKeysList.count;j++)
                {
                    NSNumber * number1=[localInstrumentToken objectAtIndex:i];
                    int int1=[number1 intValue];
                    NSNumber * number2=[allKeysList objectAtIndex:j];
                    
                    int int2=[number2 intValue];
                    
                    if(int1==int2)
                    {
                        [allKeysList1 addObject:[allKeysList objectAtIndex:j]];
                    }
                }
                
                
            }
            
//            allKeysList1=[[[allKeysList1 reverseObjectEnumerator] allObjects] mutableCopy];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                self.portTblView.delegate=self;
                self.portTblView.dataSource=self;
                
                [self.portTblView reloadData];
                
                //            //NSLog(@"%@",shareListArray);
                
                
            });
            

            
                       
        }
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    
    
    
    
}







- (void)webSocket:(PSWebSocket *)webSocket didFailWithError:(NSError *)error {
    //NSLog(@"The websocket handshake/connection failed with an error: %@", error);
}
- (void)webSocket:(PSWebSocket *)webSocket didReceiveMessageWithData:(NSData *)data
{
    //NSLog(@"The websocket received a message: %@", data);
}
- (void)webSocket:(PSWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    //NSLog(@"The websocket closed with code: %@, reason: %@, wasClean: %@", @(code), reason, (wasClean) ? @"YES" : @"NO");
}







                    
@end
