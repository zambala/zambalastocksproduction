//
//  TradeCellTableViewCell.h
//  testing
//
//  Created by zenwise technologies on 23/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TradeCellTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *orderIdLbl;


@property (strong, nonatomic) IBOutlet UILabel *tradingSymLbl;

@property (strong, nonatomic) IBOutlet UILabel *transactionTypeLbl;
@property (strong, nonatomic) IBOutlet UILabel *priceLbl;
@property (strong, nonatomic) IBOutlet UILabel *exchangeLbl;
@property (strong, nonatomic) IBOutlet UILabel *timeLbl;
@property (strong, nonatomic) IBOutlet UILabel *qtyLbl;
@property (strong, nonatomic) IBOutlet UILabel *filledLabel;
@property (strong, nonatomic) IBOutlet UILabel *pendingLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderTypeLbl;
@property (weak, nonatomic) IBOutlet UILabel *productTypeLbl;
@property (weak, nonatomic) IBOutlet UIImageView *expandImg;

@end
