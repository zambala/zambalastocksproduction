//
//  WisdomFilterPopup.h
//  testing
//
//  Created by zenwise mac 2 on 12/8/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]


@interface WisdomFilterPopup : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource>
{

NSArray *dataArray;
UIPickerView *pickerView;
UIView *ViewContainer, *backgroundView;
NSString *dataStr;
    
    //----FromDatePicker----//
    
    UIView *fromDateViewContainer;
    UIDatePicker *fromDatePicker;
    NSDate *fromDate;
    
    //----ToDatePicker----//
    UIView *toDateViewContainer;
    UIDatePicker *toDatePicker;
    NSDate *toDate;
    
    //----fromTimePicker
    UIView *fromTimeViewContainer;
    UIDatePicker *fromTimePicker;
    NSDate *fromTime;
    
    //----toTimePicker
    UIView *toTimeViewContainer;
    UIDatePicker *toTimePicker;
    NSDate *toTime;

    

    BOOL buyFlag, sellFlag, tradeFlag, shortFlag, longFlag;
    
    


    
}

@property (strong, nonatomic) IBOutlet UIView *popUpView;

@property (strong, nonatomic) IBOutlet UITextField *symbolTxt, *leaderTxt;
@property (strong, nonatomic) IBOutlet UIButton *buyImageBtn, *sellImageBtn, *tradeImageBtn, *shortImgBtn, *longImgBtn, *cancelBtn;

@property (strong, nonatomic) IBOutlet UIButton *fromDateBtn, *fromTimeBtn, *toDateBtn, *toTimeBtn;


@end
