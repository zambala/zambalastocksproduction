//
//  TopPerformersViewController.h
//  betazambalafollower
//
//  Created by Zenwise Technologies on 22/06/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XYPieChart.h"

@interface TopPerformersViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,XYPieChartDelegate,XYPieChartDataSource>
@property (weak, nonatomic) IBOutlet UITableView *topPerformersTableView;
@property (weak, nonatomic) IBOutlet UIButton *tradeButton;

@property NSMutableArray *sliceColors;
@property NSMutableArray *pieChartPercentageArray;
@property NSString * companyNameString;
@property NSString * symbolString;
@property NSString * bullishCount;
@property NSString * berishCount;
@property NSString * potentialString;
@property NSString * segmentType;
@property NSMutableDictionary * responseArray;
@property NSString * instrumentID;
@property NSString * topCompanyName;
@property NSString * highStr;
@property NSString * lowStr;
@property NSString * averageStr;
@property (nonatomic, assign) CGFloat lastContentOffset;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
