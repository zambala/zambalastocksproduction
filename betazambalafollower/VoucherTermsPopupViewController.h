//
//  VoucherTermsPopupViewController.h
//  betazambalafollower
//
//  Created by Zenwise Technologies Private Limited on 25/08/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VoucherTermsPopupViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *termsAndConditions;
@property (weak, nonatomic) IBOutlet UIButton *okayButton;

@property NSString * termsAndConditionsString;


@end
