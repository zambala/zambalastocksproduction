//
//  OnBoardingRootViewController.m
//  NewUI
//
//  Created by Zenwise Technologies on 24/05/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "OnBoardingRootViewController.h"

@interface OnBoardingRootViewController ()
{
    int currentIndex;
    NSTimer * myTimer;
}

@end

@implementation OnBoardingRootViewController

@synthesize PageViewController,arrPageTitles,arrPageImages;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString * textOne = @"Invest in equities and derivatives on the go";
    NSString * textTwo = @"Follow Market Experts with proven track record";
    NSString * textThree = @"Access and trade in the US Stock Market";
    NSString * textFour = @"Stay updated with live market news & prices";
    
    //arrPageTitles = @[@"This is The App Guruz",@"This is Table Tennis 3D",@"This is Hide Secrets"];
    //arrPageImages =@[@"1.jpg",@"2.jpg",@"3.jpg"];
    currentIndex = 0;
    arrPageTitles = @[textOne,textTwo,textThree,textFour];
    arrPageImages =@[@"equity",@"marketmonksNew",@"USmarketNew",@"NewsNew"];
    
    self.PageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.PageViewController.dataSource = self;
    
    OnboardingViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.PageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.PageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [self addChildViewController:PageViewController];
    [self.view addSubview:PageViewController.view];
    [self.PageViewController didMoveToParentViewController:self];
    
  myTimer =   [NSTimer scheduledTimerWithTimeInterval:2.0
                                     target:self
                                   selector:@selector(loadNextController)
                                   userInfo:nil
                                    repeats:YES];
    // Do any additional setup after loading the view.
}
- (void)loadNextController {
    
    OnboardingViewController *nextViewController = [self viewControllerAtIndex:currentIndex++];
    if (nextViewController == nil) {
        
        currentIndex = 0;
        nextViewController = [self viewControllerAtIndex:currentIndex];
    }
    
    [self.PageViewController setViewControllers:@[nextViewController]
                   direction:UIPageViewControllerNavigationDirectionForward
                    animated:YES
                  completion:nil];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((OnboardingViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound))
    {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}


- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((OnboardingViewController*) viewController).pageIndex;
    
    if (index == NSNotFound)
    {
        return nil;
    }
    
    index++;
    if (index == [self.arrPageTitles count])
    {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

#pragma mark - Other Methods
- (OnboardingViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.arrPageTitles count] == 0) || (index >= [self.arrPageTitles count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    OnboardingViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OnboardingViewController"];
    pageContentViewController.imgFile = self.arrPageImages[index];
    pageContentViewController.txtTitle = self.arrPageTitles[index];
    pageContentViewController.pageIndex = index;
    if(index ==3)
    {
        [myTimer invalidate];
        myTimer = nil;
    }
    return pageContentViewController;
}

#pragma mark - No of Pages Methods
- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.arrPageTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)viewWillDisappear:(BOOL)animated
{
    [myTimer invalidate];
    myTimer = nil;
}
@end
