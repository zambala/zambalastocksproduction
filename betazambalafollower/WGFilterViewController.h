//
//  WGFilterViewController.h
//  betazambalafollower
//
//  Created by guna on 08/05/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WGFilterViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIView *leaderView;
@property (weak, nonatomic) IBOutlet UITextField *selectLeaderTF;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;
@property (weak, nonatomic) IBOutlet UIButton *sellButton;
@property (weak, nonatomic) IBOutlet UIButton *dayTradeButton;
@property (weak, nonatomic) IBOutlet UIButton *shortTermButton;
@property (weak, nonatomic) IBOutlet UIButton *longTermButton;
- (IBAction)fromDateButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *fromDateButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *toDateButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *fromTimeButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *toTimeButtonOutlet;

- (IBAction)toDateButtonAction:(id)sender;
- (IBAction)fromTimeButtonAction:(id)sender;
- (IBAction)toTimeButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *viewTopAdvicesButton;
@property (weak, nonatomic) IBOutlet UIButton *viewPremiumAdvicesButton;

@property BOOL *buyFlag,*sellFlag,*longTermFlag,*shortTermFlag,*dayTradeFlag;
@property NSMutableDictionary * buySellDict;
@property NSMutableDictionary * tradeDict;
- (IBAction)onViewTopAdvicesTap:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *onViewPremiumAdvicesTap;
- (IBAction)onApplyButtonTap:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *selectLeadersLabel;

- (IBAction)selectLeaderButton:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *symbolSearchFld;
@property NSString * exchangeType;
@property NSString * title;
@property NSMutableArray * symbols;
@property NSString * searchString;

@end
