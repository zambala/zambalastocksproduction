//
//  KnowledgeTextOnlyViewController.h
//  KnowledgeSection
//
//  Created by Zenwise Technologies on 14/06/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KnowledgeTextOnlyViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *shortDescLbl;

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@end
