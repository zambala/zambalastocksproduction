//
//  ProfileSettings.m
//  testing
//
//  Created by zenwise technologies on 19/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "ProfileSettings.h"
#import "TabBar.h"
#import "HMSegmentedControl.h"
#import "NotificationTonePopUp.h"
#import "MJSecondDetailViewController.h"
#import "RadioButton.h"

@interface ProfileSettings ()
{
    HMSegmentedControl * segmentedControl;
    
    BOOL ticker;
    BOOL notification;
}

@end

@implementation ProfileSettings

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.leaderAdviceBtn setEnabled:YES];
    [self.marketAlertBtn setEnabled:YES];
    [self.triggeralertBtn setEnabled:YES];
    [self.orderalertBtn setEnabled:YES];
    [self.stopAlertBtn setEnabled:YES];
    
    self.tadeScrolView.hidden=YES;
    
    [self.leaderAdviceBtn addTarget:self action:@selector(leaderAlert) forControlEvents:UIControlEventTouchUpInside];
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"PERSONAL",@"TRADE"]];
    segmentedControl.frame = CGRectMake(0, 58.2, 375, 40);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
//    segmentedControl.verticalDividerEnabled = YES;
//    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(251/255.0) green:(196/255.0) blue:(12/255.0) alpha:1];
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segmentedControl];
    
    self.profileImageView.layer.cornerRadius=self.profileImageView.frame.size.width / 2;
    self.profileImageView.clipsToBounds = YES;
    
    tickerBtn = [[UIButton alloc] initWithFrame:CGRectMake(320, 320, 24, 24)];
    [tickerBtn setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
    [tickerBtn setImage:[UIImage imageNamed:@"checkSel-box.png"] forState:UIControlStateSelected];
    tickerBtn.selected = YES;
    [tickerBtn addTarget: self action: @selector(tickerImgBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview: tickerBtn];
    
    notificationBtn = [[UIButton alloc] initWithFrame:CGRectMake(320, 530, 24, 24)];
    [notificationBtn setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
    [notificationBtn setImage:[UIImage imageNamed:@"checkSel-box.png"] forState:UIControlStateSelected];
    notificationBtn.selected = YES;
    [notificationBtn addTarget: self action: @selector(notificationImgBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview: notificationBtn];
    
    self.scrollView.contentSize=CGSizeMake(365, 966);
    
    self.tadeScrolView.contentSize=CGSizeMake(365, 890);
    
    
   
    
    
    [self.euityBtn setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
    [self.euityBtn setImage:[UIImage imageNamed:@"checkSel-box.png"] forState:UIControlStateSelected];
    self.euityBtn.selected = NO;
    [self.euityBtn addTarget: self action: @selector(equitiesTextBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.tadeScrolView addSubview: self.euityBtn];
    
    
    [self.deriImgBtn setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
    [self.deriImgBtn setImage:[UIImage imageNamed:@"checkSel-box.png"] forState:UIControlStateSelected];
    self.deriImgBtn.selected = NO;
    [self.deriImgBtn addTarget: self action: @selector(derivativesTextBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.tadeScrolView addSubview: self.deriImgBtn];
    
    
  
    [self.currImgBtn setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
    [self.currImgBtn setImage:[UIImage imageNamed:@"checkSel-box.png"] forState:UIControlStateSelected];
    self.currImgBtn.selected = NO;
    [self.currImgBtn addTarget: self action: @selector(currencyTextBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.tadeScrolView addSubview: self.currImgBtn];
    
    
   
    [self.commImgBtn setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
    [self.commImgBtn setImage:[UIImage imageNamed:@"checkSel-box.png"] forState:UIControlStateSelected];
    self.commImgBtn.selected = NO;
    [self.commImgBtn addTarget: self action: @selector(commoditiesTextBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.tadeScrolView addSubview: self.commImgBtn];
    
    
    
//    dayImgBtn = [[UIButton alloc] initWithFrame:CGRectMake(24, 165, 24, 24)];
//    [dayImgBtn setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
//    [dayImgBtn setImage:[UIImage imageNamed:@"checkSel-box.png"] forState:UIControlStateSelected];
//    dayImgBtn.selected = NO;
//    [dayImgBtn addTarget: self action: @selector(dayTradeTextBtn) forControlEvents:UIControlEventTouchUpInside];
//    [self.tadeScrolView addSubview: dayImgBtn];
//    
//    shortImgBtn = [[UIButton alloc] initWithFrame:CGRectMake(130, 165, 24, 24)];
//    [shortImgBtn setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
//    [shortImgBtn setImage:[UIImage imageNamed:@"checkSel-box.png"] forState:UIControlStateSelected];
//    shortImgBtn.selected = NO;
//    [shortImgBtn addTarget: self action: @selector(shortTextBtn) forControlEvents:UIControlEventTouchUpInside];
//    [self.tadeScrolView addSubview: shortImgBtn];
//    
//    longImgBtn = [[UIButton alloc] initWithFrame:CGRectMake(236, 165, 24, 24)];
//    [longImgBtn setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
//    [longImgBtn setImage:[UIImage imageNamed:@"checkSel-box.png"] forState:UIControlStateSelected];
//    longImgBtn.selected = NO;
//    [longImgBtn addTarget: self action: @selector(longTermTextBtn) forControlEvents:UIControlEventTouchUpInside];
//    [self.tadeScrolView addSubview: longImgBtn];
//    
//    dealerModeBtn = [[UIButton alloc] initWithFrame:CGRectMake(330, 640, 24, 24)];
//    [dealerModeBtn setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
//    [dealerModeBtn setImage:[UIImage imageNamed:@"checkSel-box.png"] forState:UIControlStateSelected];
//    dealerModeBtn.selected = NO;
//    [dealerModeBtn addTarget: self action: @selector(onClickingDealerModeBtn) forControlEvents:UIControlEventTouchUpInside];
//    [self.tadeScrolView addSubview: dealerModeBtn];

    
//    RadioButton *rb1 = [[RadioButton alloc] initWithGroupId:@"first group" index:0];
//    RadioButton *rb2 = [[RadioButton alloc] initWithGroupId:@"first group" index:1];
//    RadioButton *rb3 = [[RadioButton alloc] initWithGroupId:@"first group" index:2];
//    RadioButton *rb4 = [[RadioButton alloc] initWithGroupId:@"first group" index:3];
//    RadioButton *rb5 = [[RadioButton alloc] initWithGroupId:@"first group" index:4];
//    RadioButton *rb6 = [[RadioButton alloc] initWithGroupId:@"first group" index:5];
//    RadioButton *rb7 = [[RadioButton alloc] initWithGroupId:@"first group" index:6];
//    RadioButton *rb8 = [[RadioButton alloc] initWithGroupId:@"first group" index:7];
//    
//    
//    rb1.frame = CGRectMake(24,260,22,22);
//    rb2.frame = CGRectMake(130,260,22,22);
//    rb3.frame = CGRectMake(236,260,22,22);
//    rb4.frame = CGRectMake(24,370,22,22);
//    rb5.frame = CGRectMake(130,370,22,22);
//    rb6.frame = CGRectMake(236,370,22,22);
//    rb7.frame = CGRectMake(24,480,22,22);
//    rb8.frame = CGRectMake(236,480,22,22);
//    
//    [self.tadeScrolView addSubview:rb1];
//    [self.tadeScrolView addSubview:rb2];
//    [self.tadeScrolView addSubview:rb3];
//    [self.tadeScrolView addSubview:rb4];
//    [self.tadeScrolView addSubview:rb5];
//    [self.tadeScrolView addSubview:rb6];
//    [self.tadeScrolView addSubview:rb7];
//    [self.tadeScrolView addSubview:rb8];
    
    


    
   }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tickerImgBtn
{
    if (ticker == false)
    {
        tickerBtn.selected = YES;
        ticker = true;
    }
    else
    {
        tickerBtn.selected = NO;
        ticker = false;
        
    }
}

-(void)notificationImgBtn
{
    if (notification == true)
    {
        notificationBtn.selected = YES;
        notification = false;
        [self.leaderAdviceBtn setEnabled:YES];
        [self.marketAlertBtn setEnabled:YES];
        [self.triggeralertBtn setEnabled:YES];
        [self.orderalertBtn setEnabled:YES];
        [self.stopAlertBtn setEnabled:YES];
    
    }
    else
    {
        notificationBtn.selected = NO;
        notification = true;
         [self.leaderAdviceBtn setEnabled:NO];
        [self.marketAlertBtn setEnabled:NO];
        [self.triggeralertBtn setEnabled:NO];
        [self.orderalertBtn setEnabled:NO];
        [self.stopAlertBtn setEnabled:NO];
        
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)pickingImageButton:(id)sender

{
    
    UIImagePickerController *pickerController = [[UIImagePickerController alloc]
                                                 init];
    pickerController.delegate = self;
    [self presentViewController:pickerController animated:YES completion:nil];
}

- (void) imagePickerController:(UIImagePickerController *)picker
         didFinishPickingImage:(UIImage *)image
                   editingInfo:(NSDictionary *)editingInfo
{
    self.profileImageView.image = image;
    //NSLog(@"%@",editingInfo);
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)showingMainMenu:(id)sender {
    
    TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
    
    [self presentViewController:tabPage animated:YES completion:nil];

}


-(void)leaderAlert
{
    NotificationTonePopUp *detailViewController = [[NotificationTonePopUp alloc] initWithNibName:@"NotificationTonePopUp" bundle:nil];
    [self presentPopupViewController:detailViewController animationType: MJPopupViewAnimationSlideTopBottom];
    

}

-(void)equitiesTextBtn
{
    if (equityFlag == false)
    {
        self.euityBtn.selected = YES;
        equityFlag =true;
        
    }
    else
    {
        self.euityBtn.selected = NO;
        equityFlag =false;
    }
    
}

-(void)derivativesTextBtn
{
    if (deriFlag == false)
    {
        self.deriImgBtn.selected = YES;
        deriFlag =true;
        
    }
    else
    {
        self.deriImgBtn.selected = NO;
        deriFlag =false;
    }
    
}

-(void)currencyTextBtn
{
    if (currencyFlag == false)
    {
        self.currImgBtn.selected = YES;
        currencyFlag = true;
    }
    else
    {
        self.currImgBtn.selected = NO;
        currencyFlag = false;
        
    }
}

-(void)commoditiesTextBtn
{
    if (commFlag == false)
    {
        self.commImgBtn.selected = YES;
        commFlag = true;
    }
    else
    {
        self.commImgBtn.selected = NO;
        commFlag = false;
        
    }
}

//-(void)dayTradeTextBtn
//{
//    if (tradeFlag == false)
//    {
//        
//        dayImgBtn.selected = YES;
//        tradeFlag = true;
//    }
//    else
//    {
//        dayImgBtn.selected = NO;
//        tradeFlag = false;
//        
//    }
//}
//
//-(void)shortTextBtn
//{
//    if (shortTermFlag == false)
//    {
//        shortImgBtn.selected = YES;
//        shortTermFlag = true;
//    }
//    else
//    {
//        shortImgBtn.selected = NO;
//        shortTermFlag = false;
//        
//    }
//}
//
//-(void)longTermTextBtn
//{
//    if (longTermFlag == false)
//    {
//        longImgBtn.selected = YES;
//        longTermFlag = true;
//    }
//    else
//    {
//        longImgBtn.selected = NO;
//        longTermFlag = false;
//        
//    }
//}
//
//-(void)onClickingDealerModeBtn
//{
//    if (dealerFlag == false)
//    {
//        dealerModeBtn.selected = YES;
//        dealerFlag = true;
//    }
//    else
//    {
//        dealerModeBtn.selected = NO;
//        dealerFlag = false;
//        
//    }
//}


-(void)segmentedControlChangedValue
{
    if(segmentedControl.selectedSegmentIndex==0)
    {
        self.tadeScrolView.hidden=YES;
        self.scrollView.hidden=NO;
    }
    else{
        self.tadeScrolView.hidden=YES;
        self.scrollView.hidden=YES;
    }
}




@end
