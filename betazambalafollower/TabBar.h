//
//  TabBar.h
//  testing
//
//  Created by zenwise technologies on 22/11/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCFloatingActionButton.h"
@import SocketIO;


@interface TabBar : UITabBarController<UITableViewDelegate,UITableViewDataSource,floatMenuDelegate,UITabBarDelegate,UITabBarControllerDelegate>
@property UITableView * dummyTable;
@property UITabBarController * tabBar1;
@property (nonatomic, strong)  SocketIOClient* socketio;

@property (strong, nonatomic) VCFloatingActionButton *addButton;

@end
