//
//  NewExploreMonksTableViewCell.m
//  PremiumServices
//
//  Created by zenwise mac 2 on 7/13/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "NewExploreMonksTableViewCell.h"

@implementation NewExploreMonksTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.profileImg.layer.cornerRadius=self.profileImg.frame.size.width/2;
    self.profileImg.clipsToBounds=YES;
    self.bgView.layer.cornerRadius = 5.0f;
    self.bgView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.bgView.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.bgView.layer.shadowOpacity = 5.0f;
    self.bgView.layer.shadowRadius = 4.0f;
    self.bgView.layer.cornerRadius=10.0f;
    self.bgView.layer.masksToBounds = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
