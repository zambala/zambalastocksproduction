//
//  PopUpView.h
//  testing
//
//  Created by zenwise mac 2 on 11/25/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopUpView : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *derivativeImageView;
@property (weak, nonatomic) IBOutlet UIImageView *equityImageView;

@end
