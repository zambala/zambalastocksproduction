//
//  PendingOrdersCell.m
//  testing
//
//  Created by zenwise technologies on 26/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "PendingOrdersCell.h"

@implementation PendingOrdersCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
     self.transactionTypeLbl.layer.borderWidth = 1.0f;
     self.transactionTypeLbl.layer.cornerRadius = 2.0f;
    
     self.editBtn.layer.cornerRadius = 2.0f;
     self.closeBtn.layer.cornerRadius = 2.0f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)expandBtnAction:(id)sender {
}
@end
