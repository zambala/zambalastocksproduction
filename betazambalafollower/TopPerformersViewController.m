//
//  TopPerformersViewController.m
//  betazambalafollower
//
//  Created by Zenwise Technologies on 22/06/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import "TopPerformersViewController.h"
#import "TopPerfDetailTopTableViewCell.h"
#import "TopPrefDetailBottomTableViewCell.h"
#import "AppDelegate.h"
#import "EquitiesCell1.h"
#import "StockOrderView.h"
#import "StockView1.h"
#import <Mixpanel/Mixpanel.h>



@interface TopPerformersViewController ()
{
    NSDictionary *headers ;
    NSMutableURLRequest *request;
    AppDelegate * delegate;
    NSDictionary *parameters;
    NSNumber * offset;
    NSData *postData;
    NSMutableArray * finalResponseArray;
    NSMutableArray * filterAdvice;
    NSString * EPString;/*!<it store entry price*/
    NSString * SLString;/*!<it store stop loss*/
    NSString * TPString;/*!<it store target price*/
    NSString * EP;/*!<it store entry price*/
    NSString * buySell;
    BOOL rehitBool;
    UIRefreshControl * refreshControl1;
    NSString * number;
}

@end

@implementation TopPerformersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"top_performer_page"];
    NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
    number= [loggedInUser stringForKey:@"profilemobilenumber"];
    self.navigationItem.title = self.symbolString;
    self.navigationItem.leftBarButtonItem.title=@"";
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.sliceColors = [[NSMutableArray alloc]init];
    self.pieChartPercentageArray = [[NSMutableArray alloc]init];
     finalResponseArray=[[NSMutableArray alloc]init];
    [self.tradeButton addTarget:self action:@selector(onTradeButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.pieChartPercentageArray addObject:self.bullishCount];
    [self.pieChartPercentageArray addObject:self.berishCount];
    
    
    @try {
        self.sliceColors =[NSMutableArray arrayWithObjects:
                           [UIColor colorWithRed:(98.0)/255 green:(209.0)/255 blue:(132.0)/255 alpha:1],
                           [UIColor colorWithRed:(204.0)/(255.0) green:(38.0)/(255.0) blue:(57.0)/(255.0) alpha:1]
                           ,nil];
        ;
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    offset=[NSNumber numberWithInt:0];
  
   // [self arrayMethod];
    [self serverHit];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)serverHit
{
    @try
    {
    request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower/wisdomgarden",delegate.baseUrl]]
                                      cachePolicy:NSURLRequestUseProtocolCachePolicy
                                  timeoutInterval:10.0];
    
    headers = @{ @"cache-control": @"no-cache",
                 @"content-type": @"application/json",
                 @"authtoken":delegate.zenwiseToken,
                 @"deviceid":delegate.currentDeviceId
                 };
    NSString * durationType = @"3";
   
    parameters = @{
                   @"clientid":delegate.userID,
                   @"segment":@1,
                   @"active":@(true),
                   @"issubscribed":@(true),
                   @"durationtype":durationType,
                   @"buysell":@"",
                   @"limit":@10,
                   @"offset":offset,
                   @"sortby":@"DESC",
                   @"orderby":@"createdon",
                   @"leaderid":@"",
                   @"fromdate":@"",
                   @"todate":@"",
                   @"symbol":self.symbolString,
                   @"isleaderadvice":@(true),
                   @"durationtype":@"3",
                   @"mobilenumber":number,
                   @"brokerid":[delegate.brokerInfoDict objectForKey:@"brokerid"]
                   };
    
    postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    
                                                    if(data!=nil)
                                                    {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            if([httpResponse statusCode]==403)
                                                            {
                                                                
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                            
                                                            }
                                                        else if([httpResponse statusCode]==401)
                                                        {
                                                        
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                            
                                                        }
                                                            else
                                                            {
                                                        
                                                            NSMutableDictionary * sampleDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            for(int i=0;i<[[sampleDict objectForKey:@"data"] count];i++)
                                                            {
                                                                [finalResponseArray addObject:[[sampleDict objectForKey:@"data"]objectAtIndex:i]];
                                                                
                                                            }
                                                            self.responseArray=[[NSMutableDictionary alloc]init];
                                                            NSArray * userArray=[finalResponseArray mutableCopy];
                                                            NSOrderedSet *set = [NSOrderedSet orderedSetWithArray:userArray];
                                                            NSArray *uniqueArray = [set array];
                                                            [self.responseArray setObject:uniqueArray forKey:@"data"];
                                                            [self arrayMethod];
                                                            }
                                                        }
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            self.activityIndicator.hidden=YES;
                                                            [self.activityIndicator stopAnimating];
                                                            self.topPerformersTableView.delegate= self;
                                                            self.topPerformersTableView.dataSource= self;
                                                            [self.topPerformersTableView reloadData];
                                                        });
                                                    }
                                                    
                                                    else
                                                    {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            
                                                            UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                [self serverHit];
                                                               // [self arrayMethod];
                                                            }];
                                                            
                                                            UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                
                                                            }];
                                                            
                                                            
                                                            [alert addAction:retryAction];
                                                            [alert addAction:cancel];
                                                            
                                                        });
                                                        
                                                        
                                                    }
                                                    
                                                }];
    
    [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    @try
    {
    return filterAdvice.count+1;
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
    if(indexPath.row==0)
    {
        TopPerfDetailTopTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"topcell"];
        [cell.pieChartView setDelegate:self];
        [cell.pieChartView setDataSource:self];
        [cell.pieChartView setStartPieAngle:M_PI_2];
        [cell.pieChartView setAnimationSpeed:1.0];
        [cell.pieChartView setLabelFont:[UIFont fontWithName:@"DBLCDTempBlack" size:18]];
        [cell.pieChartView setLabelRadius:160];
        [cell.pieChartView setShowPercentage:YES];
        [cell.pieChartView setPieRadius:cell.pieChartView.frame.size.height-30];
        [cell.pieChartView setUserInteractionEnabled:YES];
        [cell.pieChartView setLabelShadowColor:[UIColor blackColor]];
        [cell.pieChartView setShowLabel:NO];
        dispatch_async(dispatch_get_main_queue(), ^{
            [cell.pieChartView reloadData];
        });
        
        cell.companyNameLabel.text = self.topCompanyName;
        NSString * percent = @"%";
        cell.potentialPercentLabel.text = [NSString stringWithFormat:@"%.2f%@",[self.potentialString floatValue],percent];
        cell.bullishValueLabel.text = self.bullishCount;
        cell.bearishValueLabel.text = self.berishCount;
        cell.highLbl.text =  [NSString stringWithFormat:@"%.2f",[self.highStr floatValue]];
        cell.lowLbl.text =   [NSString stringWithFormat:@"%.2f",[self.lowStr floatValue]];
        cell.averageLbl.text =   [NSString stringWithFormat:@"%.2f",[self.averageStr floatValue]];
        return cell;
    }
    else
    {
        if(filterAdvice.count>0)
        {
            NSDictionary * contra=[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"];
            NSDictionary * openAdvice=[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"];
            NSString * contraStr=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"]];
            
            if(contra && openAdvice && ![contraStr isEqualToString:@"<null>"])
            {
                int messageType=[[[[filterAdvice objectAtIndex:indexPath.row-1] objectForKey:@"contra_advice"]objectForKey:@"messagetypeid"] intValue];
                
                if(messageType!=4)
                {
                    //        self.brokerMesssagesTableView.hidden=YES;
                    //        self.notoficationstableView.hidden=NO;
                    
                    EquitiesCell1 * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
                    cell.originalAdviceView.hidden=NO;
                    cell.buySellButton.hidden=YES;
                    cell.leaderImg.image=[UIImage imageNamed:@"default"];
                    cell.equitiesImg2.layer.cornerRadius=cell.equitiesImg2.frame.size.width / 2;
                    cell.equitiesImg2.clipsToBounds = YES;
                    
                    cell.eqitiesImg1.layer.cornerRadius=cell.eqitiesImg1.frame.size.width / 2;
                    cell.eqitiesImg1.clipsToBounds = YES;
                    
                    cell.leaderImg.layer.cornerRadius=cell.leaderImg.frame.size.width / 2;
                    cell.leaderImg.clipsToBounds = YES;
                    //NSMutableAttributedString *strText = [[NSMutableAttributedString alloc] initWithString:@"SELL MUTHOOTFIN @ EP 3350.00 TP 3390.00 SL 3300.00"];
                    //NSString * test = @"SELL MUTHOOTFIN @ EP 3350.00 TP 3390.00 SL 3300.00";
                    // [strText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Ubuntu-Medium" size:12.5] range:NSMakeRange(0,3)];
                    // [strText addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, 3)];
                    
                    int tickCheck = [[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"] objectForKey:@"isacted"] intValue];
                    if(tickCheck==0)
                    {
                        cell.tickImageView.hidden=YES;
                    }else if (tickCheck==1)
                    {
                        cell.tickImageView.hidden=NO;
                        
                    }
                    
                    
                    
                    
                    for(int i=0;i<=1;i++)
                    {
                        NSString * mainString;
                        NSNumber * buttonNumber;
                        NSString * contraBuySell;
                        int buttonTitle;
                        if(i==0)
                        {
                            mainString=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"] objectForKey:@"message"]];
                            buttonNumber = [[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"] objectForKey:@"buysell"];
                            
                            
                            buttonTitle = [buttonNumber intValue];
                            if(buttonTitle==1)
                            {
                                contraBuySell=@"BUY";
                                [cell.buySellButton setTitle:@"BUY" forState:UIControlStateNormal];
                                
                                
                                
                                [cell.buySellButton setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1.0f]];
                                
                                
                                
                            }else if (buttonTitle==2)
                            {
                                contraBuySell=@"SELL";
                                [cell.buySellButton setTitle:@"SELL" forState:UIControlStateNormal];
                                
                                [cell.buySellButton setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1.0f]];
                                
                            }
                        }
                        else  if(i==1)
                        {
                            mainString=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"] objectForKey:@"message"]];
                            buttonNumber = [[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"] objectForKey:@"buysell"];
                            buttonTitle = [buttonNumber intValue];
                            
                        }
                        
                        
                        if([mainString containsString:@"@"])
                        {
                            NSString * sell = [NSString stringWithFormat:@"<html><body><div style='text-align:left;display:inline-block;font-size:14px;font-family:Ubuntu;color:#4b4d52'>  %@</div> </body></html>",contraBuySell];
                            NSString * muthootfin =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"] objectForKey:@"symbolname"]];
                            NSString * ep = @"<html><body><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#4b4d52'> @</div></html></body>";
                            NSString * tp = @"<html><body><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#4b4d52'> XP</div></html></body>";
                            NSString * value2 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"] objectForKey:@"exitprice"]];
                            NSString * sl;
                            if([mainString containsString:@"Stop Loss Hit"])
                            {
                                sl = @"<html><body><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#4b4d52'> Stop Loss Hit</div></html></body>";
                            }
                            else if([mainString containsString:@"Target Achieved"])
                            {
                                sl = @"<html><body><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#4b4d52'> Profit Target Achieved</div></html></body>";
                            }
                            else if([mainString containsString:@"Book Profit"])
                            {
                                sl = @"<html><body><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#4b4d52'> Book Profit</div></html></body>";
                            }
                            else if([mainString containsString:@"Book Loss"])
                            {
                                sl = @"<html><body><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#4b4d52'> Book Loss</div></html></body>";
                            }
                            
                            
                            
                            NSString * string1 = [sell stringByAppendingString:muthootfin];
                            NSString *string2 = [string1 stringByAppendingString:ep];
                            NSString * string4 = [string2 stringByAppendingString:tp];
                            NSString * string5 = [string4 stringByAppendingString:value2];
                            NSString * string6 = [string5 stringByAppendingString:sl];
                            
                            NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[string6 dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                            
                            //UILabel * myLabel = [[UILabel alloc] init];
                            // myLabel.attributedText = attrStr;
                            
                            
                            cell.valueChangeLabel.attributedText=attrStr;
                            
                            //                cell.valueChangeLabel.text=[mainString uppercaseString];
                            
                            //                cell.oLbl.text=@"C";
                        }
                        
                        
                        
                        else
                        {
                            
                            //                cell.oLbl.text=@"O";
                            TPString=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"] objectForKey:@"targetprice"]];
                            
                            if ([mainString isEqual:[NSNull null]]||mainString==nil||[mainString isEqualToString:@"<null>"] ||  [TPString isEqual:[NSNull null]]||TPString==nil||[TPString isEqualToString:@"<null>"]   ) {
                                cell.valueChangeLabel.text =@"<Null>";
                            }
                            
                            else{
                                
                                @try {
                                    
                                    
                                    
                                    
                                    
                                    
                                    NSArray * mainArray=[mainString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                                    
                                    
                                    
                                   
                                    
                                    //                NSString * buySell=[NSString stringWithFormat:@"%@",[mainArray objectAtIndex:0]];
                                    
                                    if(buttonTitle==1)
                                    {
                                        buySell=@"BUY";
                                        
                                    }else if (buttonTitle==2)
                                    {
                                        buySell=@"SELL";
                                        
                                    }
                                    
                                    NSString * string=[NSString stringWithFormat:@"%@",[mainArray objectAtIndex:1]];
                                    
                                    
                                    
                                    NSArray * companyArray=[string componentsSeparatedByString:@";"];
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    //               for (int i=1; i<[companyArray count];i++) {
                                    
                                    
                                    
                                    //                   NSString * string11=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:0]];
                                    //
                                    //
                                    //
                                    //                   NSArray * array2=[string11 componentsSeparatedByString:@";"];
                                    //
                                    //
                                    
                                    NSString * companyName=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:0]];
                                    NSString * EP1=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:1]];
                                    NSString * TP=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:2]];
                                    NSString * SL;
                                    NSArray * SLArray;
                                    if([mainString containsString:@"SL="])
                                    {
                                        SL=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:3]];
                                        SLArray=[SL componentsSeparatedByString:@"="];
                                        SLString=[NSString stringWithFormat:@"%@",[SLArray objectAtIndex:1]];
                                        
                                    }
                                    else
                                    {
                                        SL=@"";
                                        SLString=@"";
                                    }
                                    
                                    NSArray * EPArray=[EP1 componentsSeparatedByString:@"="];
                                    NSArray * TPArray=[TP componentsSeparatedByString:@"="];
                                    
                                    
                                    EPString=[NSString stringWithFormat:@"%@",[EPArray objectAtIndex:1]];
                                    TPString=[NSString stringWithFormat:@"%@",[TPArray objectAtIndex:1]];
                                    
                                    
                                    
                                    NSString * dummyfinalStr=[NSString stringWithFormat:@"%@ %@ @%@%@%@",buySell,companyName,EP1,TP,SL];
                                    
                                    NSString * finalStr = [dummyfinalStr stringByReplacingOccurrencesOfString:@"=" withString:@" "];
                                    
                                   
                                    
                                    //NSLog(@"COMPANY %@",companyName);
                                    
                                    
                                    //                              if (i==1) {
                                    //                                  EP=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                                    //
                                    //                                  //NSLog(@"EP %@",[array2 objectAtIndex:1]);
                                    //
                                    //                              }
                                    
                                    //                   if (i==2) {
                                    //                       EPString=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                                    //
                                    //                       //NSLog(@"TP %@",[array2 objectAtIndex:1]);
                                    //
                                    //                   }
                                    //
                                    //                   if (i==3) {
                                    //                       SLString=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                                    //
                                    //                       //NSLog(@"SL %@",[array2 objectAtIndex:1]);
                                    //
                                    //                   }
                                    //               }
                                    //
                                    
                                    
                                    NSString * sell = [NSString stringWithFormat:@"<html><body><div style='text-align:left;display:inline-block;font-size:12px;font-family:Ubuntu;color:#4b4d52'>  %@</div> </body></html>",buySell];
                                    NSString * muthootfin =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",[companyArray objectAtIndex:0]];
                                    NSString * ep = @"<html><body><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#4b4d52'> @ EP</div></html></body>";
                                    NSString * value1 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",EPString];
                                    NSString * tp = @"<html><body><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#4b4d52'> TP</div></html></body>";
                                    NSString * value2 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",TPString];
                                    NSString * sl = @"<html><body><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#4b4d52'> SL</div></html></body>";
                                    NSString * value3 =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",SLString] ;
                                    
                                    NSString * string1 = [sell stringByAppendingString:muthootfin];
                                    NSString *string2 = [string1 stringByAppendingString:ep];
                                    NSString * string3 = [string2 stringByAppendingString:value1];
                                    NSString * string4 = [string3 stringByAppendingString:tp];
                                    NSString * string5 = [string4 stringByAppendingString:value2];
                                    NSString * string6 = [string5 stringByAppendingString:sl];
                                    NSString * finalString;
                                    if([mainString containsString:@"SL="])
                                    {
                                        finalString = [string6 stringByAppendingString:value3];
                                    }
                                    else
                                    {
                                        finalString = string5;
                                    }
                                    
                                    
                                    
                                    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[finalString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                                    
                                    //UILabel * myLabel = [[UILabel alloc] init];
                                    // myLabel.attributedText = attrStr;
                                    
                                    
                                    if(i==0)
                                    {
                                        cell.valueChangeLabel.attributedText=attrStr;
                                    }
                                    else  if(i==1)
                                    {
                                        cell.originalMessageLabel.attributedText=attrStr;
                                    }
                                }
                                @catch (NSException * e) {
                                    //NSLog(@"Exception: %@", e);
                                }
                                @finally {
                                    //NSLog(@"finally");
                                }
                                
                            }
                            
                            
                            
                        }
                        
                    }
                    
                    
                    
                    cell.ltpLabel.text =[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"] objectForKey:@"lasttradedprice"]];
                    
                    NSString * changePercentlabel = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1] objectForKey:@"contra_advice"]objectForKey:@"changepercent"]];
                    
                    NSString * percent =@"  ";
                    NSString * finalChangePercentlabel = [changePercentlabel stringByAppendingString:percent];
                    if([changePercentlabel isEqual:[NSNull null]]||changePercentlabel==nil||[changePercentlabel isEqualToString:@"<null>"])
                    {
                        
                        
                        
                        NSString * percentage = @"";
                        NSString * zero = @"  0";
                        NSString* final = [zero stringByAppendingString:percentage];
                        cell.changePercentlabel.text=final;
                        cell.changePercentlabel.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                    }
                    else
                    {
                        
                        if([finalChangePercentlabel containsString:@"-"])
                        {
                            cell.changePercentlabel.text=finalChangePercentlabel;
                            cell.changePercentlabel.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                            cell.adviceButton.layer.borderColor =[[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1] CGColor];
                            [cell.adviceButton setTitleColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1] forState:UIControlStateNormal];
                              [cell.adviceButton setTitle:@"Closing Advice" forState:UIControlStateNormal];
                            //                    cell.oLbl.backgroundColor=[UIColor colorWithRed:(238/255.0) green:(43/255.0) blue:(53/255.0) alpha:1];
                        }
                        else{
                            cell.changePercentlabel.text=finalChangePercentlabel;
                            cell.changePercentlabel.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                            cell.adviceButton.layer.borderColor =[[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1] CGColor];
                            [cell.adviceButton setTitleColor:[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1] forState:UIControlStateNormal];
                              [cell.adviceButton setTitle:@"Closing Advice" forState:UIControlStateNormal];
                            //                    cell.oLbl.backgroundColor=[UIColor colorWithRed:(86/255.0) green:(168/255.0) blue:(66/255.0) alpha:1];
                        }
                        
                        
                    }
                    
                    NSString * actedBy = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1] objectForKey:@"contra_advice"]objectForKey:@"actedby"]];
                    
                    if([actedBy isEqual:[NSNull null]]||actedBy==nil||[actedBy isEqualToString:@"<null>"])
                    {
                        cell.actedByLabel.text=@"0";
                    }else
                    {
                        cell.actedByLabel.text=actedBy;
                    }
                    NSString * sharesSold = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"] objectForKey:@"sharesold"]];
                    if([sharesSold isEqual:[NSNull null]]||sharesSold==nil||[sharesSold isEqualToString:@"<null>"])
                    {
                        cell.sharesSold.text=@"0";
                    }else
                    {
                        cell.sharesSold.text=sharesSold;
                    }
                    
                    
                    
                    
                    NSString * dateStr=[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"] objectForKey:@"createdon"];
                    
                    
                    
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
                    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                    NSDate *date = [dateFormatter dateFromString:dateStr]; // create date from string
                    
                    // change to a readable time format and change to local time zone
                    [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
                    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                    NSString *finalDate = [dateFormatter stringFromDate:date];
                    
                    
                    
                    
                    
                    cell.dateAndTimeLabel.text=finalDate;
                    NSString * firstName=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"] objectForKey:@"firstname"]];
                    NSString * lastName=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"] objectForKey:@"lastname"]];
                    
                    if([firstName isEqualToString:@"<null>"])
                    {
                        firstName=@"";
                    }
                    
                    if([lastName isEqualToString:@"<null>"])
                    {
                        lastName=@"";
                    }
                    
                    cell.profileName.text=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                    
                    
                    
                    // cell.valueChangeLabel.attributedText=strText;
                    // [strText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Ubuntu-Medium" size:12.5] range:NSMakeRange(6, )];
                    
                    
                    
                    
                    
                 //   [cell.buySellButton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
                    
                 //   [cell.leaderProfileAction addTarget:self action:@selector(leaderProfileMethod:) forControlEvents:UIControlEventTouchUpInside];
                    //       dispatch_async(dispatch_get_main_queue(), ^{
                    //           cell.profileImg.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[filterAdvice objectAtIndex:indexPath.row-1] objectForKey:@"logourl"]]]];
                    //
                    //
                    //       });
                    
                    
                    
                    
                    
                    @try {
                        
                        NSString * actedBy =[ [[filterAdvice objectAtIndex:indexPath.row-1] objectForKey:@"contra_advice"]objectForKey:@"actedby"];
                        
                        
                        if([actedBy isEqual:[NSNull null]])
                        {
                            cell.actedByLabel.text=@"0";
                        }else
                        {
                            int actedByInt=[actedBy intValue];
                            cell.actedByLabel.text = [NSString stringWithFormat:@"%i",actedByInt];
                        }
                    } @catch (NSException *exception) {
                        
                    } @finally {
                        
                    }
                    
                    @try {
                        
                        NSString * sharesSold =[ [[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"] objectForKey:@"sharesold"];
                        
                        
                        
                        if([sharesSold isEqual:[NSNull null]])
                        {
                            cell.sharesSold.text = @"0";
                        }else
                        {
                            int sharesSoldInt=[sharesSold intValue];
                            cell.sharesSold.text = [NSString stringWithFormat:@"%i",sharesSoldInt];
                        }
                        
                    } @catch (NSException *exception) {
                        
                    } @finally {
                        
                    }
                    
                    @try {
                        
                        NSString * averageProfit =[ [[filterAdvice objectAtIndex:indexPath.row-1] objectForKey:@"contra_advice"]objectForKey:@"averageprofit"];
                        
                        
                        
                        if([averageProfit isEqual:[NSNull null]])
                        {
                            
                        }else
                        {
                            int sharesSoldInt=[averageProfit intValue];
                            
                        }
                        
                    } @catch (NSException *exception) {
                        
                    } @finally {
                        
                    }
                    
                    
                    
                    NSString * duration=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"] objectForKey:@"durationtype"]];
                    
                    int durationInt=[duration intValue];
                    
                    if(durationInt==1)
                    {
                        cell.durationLbl.text=@"DT";
                        
                        cell.durationLbl.backgroundColor=[UIColor colorWithRed:(246/255.0) green:(195/255.0) blue:(117/255.0) alpha:1.0f];
                    }
                    else if(durationInt==2)
                    {
                        cell.durationLbl.text=@"ST";
                        
                        cell.durationLbl.backgroundColor=[UIColor colorWithRed:(178/255.0) green:(224/255.0) blue:(239/255.0) alpha:1.0f];
                    }
                    else if(durationInt==3)
                    {
                        cell.durationLbl.text=@"LT";
                        
                        cell.durationLbl.backgroundColor=[UIColor colorWithRed:(220/255.0) green:(172/255.0) blue:(241/255.0) alpha:1.0f];
                    }
                        
              
                    
                    NSString * sub=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1] objectForKey:@"contra_advice"] objectForKey:@"subscriptiontypeid"]];
                    
                    int subInt=[sub intValue];
                    
                    if(subInt==1)
                    {
                        cell.premiumImg.hidden=YES;
                        
                    }
                    
                    else if(subInt==2)
                    {
                        cell.premiumImg.hidden=NO;
                        
                        cell.premiumImg.image=[UIImage imageNamed:@"premiumNew"];
                        
                    }
                    
                    NSString * public=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1] objectForKey:@"contra_advice"]objectForKey:@"ispublic"]];
                    int publicInt=[public intValue];
                    
                    if(publicInt==1)
                    {
                        cell.premiumImg.hidden=NO;
                        
                        cell.premiumImg.image=[UIImage imageNamed:@"publicnew"];
                        
                        
                        
                        NSString * source = [NSString stringWithFormat:@"%@",[[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"]objectForKey:@"otherinfo"]objectForKey:@"source"]];
                        cell.sourceLabel.hidden=NO;
                        cell.sourceDataLabel.hidden=NO;
                        cell.sourceDataLabel.text = source;
                        cell.analystName.hidden=NO;
                        cell.analystNameHeightConstant.constant=21;
                        NSString * analystName = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"]objectForKey:@"leaderorgname"]];
                        NSString * dummyAnalystName= [analystName stringByReplacingOccurrencesOfString:@" " withString:@""];
                        dummyAnalystName=[dummyAnalystName lowercaseString];
                        cell.profileName.text=analystName;
                        NSString * firstName = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"]objectForKey:@"firstname"]];
                        NSString * lastname = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"]objectForKey:@"lastname"]];
                        
                        NSString * dummyName;
                        if([lastname isEqual:[NSNull null]]||[lastname containsString:@"<null>"])
                        {
                            lastname=@"";
                            NSString * name = [NSString stringWithFormat:@"%@ %@",firstName,lastname];
                            dummyName = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
                            dummyName=[dummyName lowercaseString];
                            NSString * profileName=[cell.profileName.text stringByReplacingOccurrencesOfString:@" " withString:@""];
                            profileName=[profileName lowercaseString];
                            if(![dummyName isEqualToString:profileName])
                            {
                                cell.organizationNameLbl.text=[NSString stringWithFormat:@"%@",cell.profileName.text];
                                cell.profileName.text= [NSString stringWithFormat:@"%@ %@",firstName,lastname];
                                cell.expertXconstraint.constant=10;
                                
                            }
                            else
                            {
                                cell.expertXconstraint.constant=20;
                                cell.organizationNameLbl.text=@"";
                            }
                            
                        }else
                        {
                            NSString * name = [NSString stringWithFormat:@"%@ %@",firstName,lastname];
                            dummyName = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
                            dummyName=[dummyName lowercaseString];
                            NSString * profileName=[cell.profileName.text stringByReplacingOccurrencesOfString:@" " withString:@""];
                            profileName=[profileName lowercaseString];
                            if(![dummyName isEqualToString:profileName])
                            {
                                cell.organizationNameLbl.text=[NSString stringWithFormat:@"%@",cell.profileName.text];
                                cell.profileName.text= [NSString stringWithFormat:@"%@ %@",firstName,lastname];
                                cell.expertXconstraint.constant=10;
                                
                            }
                            else
                            {
                                cell.expertXconstraint.constant=20;
                                cell.organizationNameLbl.text=@"";
                            }
                            
                        }
                        if([dummyAnalystName containsString:dummyName])
                        {
                            cell.analystName.hidden=YES;
                            cell.analystNameHeightConstant.constant=0;
                        }else
                        {
                            cell.analystName.hidden=NO;
                            cell.analystNameHeightConstant.constant=21;
                            //                    cell.profileName.text = analystName;
                        }
                        
                        
                        
                        
                    }
                    
                    else if(publicInt==0)
                    {
                        cell.premiumImg.hidden=YES;
                        cell.expertXconstraint.constant=20;
                        cell.organizationNameLbl.text=@"";
                        cell.sourceLabel.hidden=YES;
                        cell.sourceDataLabel.hidden=YES;
                        NSString * firstName=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"] objectForKey:@"firstname"]];
                        NSString * lastName=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"] objectForKey:@"lastname"]];
                        
                        if([firstName isEqualToString:@"<null>"])
                        {
                            firstName=@"";
                        }
                        
                        if([lastName isEqualToString:@"<null>"])
                        {
                            lastName=@"";
                        }
                        
                        cell.profileName.text=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                        cell.analystName.hidden=YES;
                        cell.analystNameHeightConstant.constant=0;
                        
                    }
                    
                    
                    cell.leaderImg.image=nil;
                    
                    if(publicInt==1)
                    {
                        NSURL *url;
                        @try {
                            url =  [NSURL URLWithString:[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"] objectForKey:@"logourl"]];
                        } @catch (NSException *exception) {
                            
                            
                            
                        } @finally {
                            
                        }
                        
                        
                        NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                            if (data) {
                                UIImage *image = [UIImage imageWithData:data];
                                if (image) {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        EquitiesCell1 *cell = [self.topPerformersTableView cellForRowAtIndexPath:indexPath];
                                        if (cell)
                                            cell.leaderImg.image = image;
                                    });
                                }
                            }
                        }];
                        [task resume];
                    }else
                    {
                        
                        NSURL *url;
                        @try {
                            url =  [NSURL URLWithString:[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"] objectForKey:@"logourl"]];
                        } @catch (NSException *exception) {
                            
                            
                            
                        } @finally {
                            
                        }
                        
                        
                        NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                            if (data) {
                                UIImage *image = [UIImage imageWithData:data];
                                if (image) {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        
                                        EquitiesCell1 *cell = [self.topPerformersTableView cellForRowAtIndexPath:indexPath];
                                        if (cell)
                                            cell.leaderImg.image = image;
                                    });
                                }
                            }
                        }];
                        [task resume];
                    }
                    
                    
                    //original advice//
                    
                    NSString * originalLtp1=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"] objectForKey:@"lasttradedprice"]];
                    
                    if([originalLtp1 isEqual:[NSNull null]]||[originalLtp1 isEqualToString:@"<null>"])
                    {
                        cell.originalLtp.text=@"0.00";
                    }
                    else
                    {
                        cell.originalLtp.text=originalLtp1;
                        
                    }
                    
                    NSString * originalChg=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"] objectForKey:@"changepercent"]];
                    
                    if([originalChg isEqual:[NSNull null]]||[originalChg isEqualToString:@"<null>"])
                    {
                        cell.originalPL.text=@"0.00";
                    }
                    else
                    {   if([originalChg containsString:@"-"])
                    {
                        
                        cell.originalPL.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                        
                        cell.originalAdviceTypeButton.layer.borderColor =[[UIColor colorWithRed:(238/255.0) green:(43/255.0) blue:(53/255.0) alpha:1] CGColor];
                        [cell.originalAdviceTypeButton setTitleColor:[UIColor colorWithRed:(238/255.0) green:(43/255.0) blue:(53/255.0) alpha:1] forState:UIControlStateNormal];
                        //                    cell.oLbl.backgroundColor=[UIColor colorWithRed:(238/255.0) green:(43/255.0) blue:(53/255.0) alpha:1];
                    }
                    else{
                        
                        cell.originalPL.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                        cell.originalAdviceTypeButton.layer.borderColor = [[UIColor colorWithRed:(87.0)/255 green:(160.0)/255 blue:(55.0)/255 alpha:1.0] CGColor];
                        [cell.originalAdviceTypeButton setTitleColor:[UIColor colorWithRed:(87.0)/255 green:(160.0)/255 blue:(55.0)/255 alpha:1.0] forState:UIControlStateNormal];
                        //                    cell.oLbl.backgroundColor=[UIColor colorWithRed:(86/255.0) green:(168/255.0) blue:(66/255.0) alpha:1];
                    }
                        cell.originalPL.text=originalChg;
                        
                    }
                    
                    NSString * originalActed=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"] objectForKey:@"actedby"]];
                    
                    if([originalActed isEqual:[NSNull null]]||[originalActed isEqualToString:@"<null>"])
                    {
                        cell.originalActedBy.text=@"0.00";
                    }
                    else
                    {
                        cell.originalActedBy.text=originalActed;
                        
                    }
                    
                    NSString * originalShares=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"] objectForKey:@"sharesold"]];
                    
                    if([originalShares isEqual:[NSNull null]]||[originalShares isEqualToString:@"<null>"])
                    {
                        cell.originalSharesLbl.text=@"0.00";
                    }
                    else
                    {
                        cell.originalSharesLbl.text=originalShares;
                        
                    }
                    
                    NSString * dateStr1=[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"] objectForKey:@"createdon"];
                    
                    
                    
                    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
                    [dateFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
                    [dateFormatter1 setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                    NSDate *date1 = [dateFormatter1 dateFromString:dateStr1]; // create date from string
                    
                    // change to a readable time format and change to local time zone
                    [dateFormatter1 setDateFormat:@"dd MMM yyyy HH:mm"];
                    [dateFormatter1 setTimeZone:[NSTimeZone localTimeZone]];
                    NSString *finalDate1 = [dateFormatter1 stringFromDate:date1];
                    cell.originalDateLabel.text=finalDate1;
                    if(filterAdvice.count>4)
                    {
                    NSDictionary *data = [filterAdvice objectAtIndex:indexPath.row-1];
                    BOOL lastItemReached = [data isEqual:[filterAdvice lastObject]];
                    if (!lastItemReached && indexPath.row-1 == [filterAdvice count] - 3)
                    {
                        if(rehitBool==true)
                        {
                            [self refreshTableVeiwList];
                        }
                    }
                    }
                    return cell;
                }
            }
            
            
            else if(openAdvice)
            {
                int messageType=[[[[filterAdvice objectAtIndex:indexPath.row-1] objectForKey:@"open_advice"]objectForKey:@"messagetypeid"] intValue];
                
                if(messageType!=4)
                {
                    //        self.brokerMesssagesTableView.hidden=YES;
                    //        self.notoficationstableView.hidden=NO;
                    
                    EquitiesCell1 * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
                    cell.originalAdviceView.hidden=YES;
                    cell.buySellButton.hidden=YES;
                    cell.leaderImg.image=[UIImage imageNamed:@"default"];
                    cell.equitiesImg2.layer.cornerRadius=cell.equitiesImg2.frame.size.width / 2;
                    cell.equitiesImg2.clipsToBounds = YES;
                    
                    cell.eqitiesImg1.layer.cornerRadius=cell.eqitiesImg1.frame.size.width / 2;
                    cell.eqitiesImg1.clipsToBounds = YES;
                    
                    cell.leaderImg.layer.cornerRadius=cell.leaderImg.frame.size.width / 2;
                    cell.leaderImg.clipsToBounds = YES;
                    //NSMutableAttributedString *strText = [[NSMutableAttributedString alloc] initWithString:@"SELL MUTHOOTFIN @ EP 3350.00 TP 3390.00 SL 3300.00"];
                    //NSString * test = @"SELL MUTHOOTFIN @ EP 3350.00 TP 3390.00 SL 3300.00";
                    // [strText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Ubuntu-Medium" size:12.5] range:NSMakeRange(0,3)];
                    // [strText addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, 3)];
                    
                    int tickCheck = [[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"] objectForKey:@"isacted"] intValue];
                    if(tickCheck==0)
                    {
                        cell.tickImageView.hidden=YES;
                    }else if (tickCheck==1)
                    {
                        cell.tickImageView.hidden=NO;
                        
                    }
                    
                    
                    
                    
                    
                    NSString * mainString;
                    NSNumber * buttonNumber;
                    
                    
                    
                    mainString=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"] objectForKey:@"message"]];
                    buttonNumber = [[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"] objectForKey:@"buysell"];
                    
                    
                    
                    int buttonTitle = [buttonNumber intValue];
                    NSString * contraBuySell;
                    if(buttonTitle==1)
                    {
                        contraBuySell=@"BUY";
                        [cell.buySellButton setTitle:@"BUY" forState:UIControlStateNormal];
                        
                        
                        
                        [cell.buySellButton setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1.0f]];
                        
                        
                        
                    }else if (buttonTitle==2)
                    {
                        contraBuySell=@"SELL";
                        [cell.buySellButton setTitle:@"SELL" forState:UIControlStateNormal];
                        
                        [cell.buySellButton setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1.0f]];
                        
                    }
                    if([mainString containsString:@"@"])
                    {
                        NSString * sell = [NSString stringWithFormat:@"<html><body><div style='text-align:left;display:inline-block;font-size:14px;font-family:Ubuntu;color:#4b4d52'>  %@</div> </body></html>",contraBuySell];
                        NSString * muthootfin =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"] objectForKey:@"symbolname"]];
                        NSString * ep = @"<html><body><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#4b4d52'> @</div></html></body>";
                        NSString * tp = @"<html><body><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#4b4d52'> XP</div></html></body>";
                        NSString * value2 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"] objectForKey:@"exitprice"]];
                        NSString * sl;
                        if([mainString containsString:@"Stop Loss Hit"]||[mainString containsString:@"Book Loss"])
                        {
                            sl = @"<html><body><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#4b4d52'> Stop Loss Hit</div></html></body>";
                        }
                        else if([mainString containsString:@"Target Achieved"]||[mainString containsString:@"Book Profit"])
                        {
                            sl = @"<html><body><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#4b4d52'> Profit Target Achieved</div></html></body>";
                        }
                        
                        
                        
                        
                        NSString * string1 = [sell stringByAppendingString:muthootfin];
                        NSString *string2 = [string1 stringByAppendingString:ep];
                        NSString * string4 = [string2 stringByAppendingString:tp];
                        NSString * string5 = [string4 stringByAppendingString:value2];
                        NSString * string6 = [string5 stringByAppendingString:sl];
                        
                        NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[string6 dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                        
                        //UILabel * myLabel = [[UILabel alloc] init];
                        // myLabel.attributedText = attrStr;
                        
                        
                        cell.valueChangeLabel.attributedText=attrStr;
                        cell.valueChangeLabel.text=[mainString uppercaseString];
                        
                        //                cell.oLbl.text=@"C";
                    }
                    
                    
                    
                    else
                    {
                        
                        //                cell.oLbl.text=@"O";
                        TPString=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"] objectForKey:@"targetprice"]];
                        
                        if ([mainString isEqual:[NSNull null]]||mainString==nil||[mainString isEqualToString:@"<null>"] ||  [TPString isEqual:[NSNull null]]||TPString==nil||[TPString isEqualToString:@"<null>"]   ) {
                            cell.valueChangeLabel.text =@"<Null>";
                        }
                        
                        else{
                            
                            @try {
                                
                                
                                //NSLog(@"main String %@",mainString);
                                
                                
                                
                                NSArray * mainArray=[mainString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                                
                                
                                
                                //NSLog(@"array  %@",[mainArray objectAtIndex:0]);
                                
                                //NSLog(@"array  %@",[mainArray objectAtIndex:1]);
                                
                                //                NSString * buySell=[NSString stringWithFormat:@"%@",[mainArray objectAtIndex:0]];
                                
                                if(buttonTitle==1)
                                {
                                    buySell=@"BUY";
                                    
                                }else if (buttonTitle==2)
                                {
                                    buySell=@"SELL";
                                    
                                }
                                
                                NSString * string=[NSString stringWithFormat:@"%@",[mainArray objectAtIndex:1]];
                                
                                
                                
                                NSArray * companyArray=[string componentsSeparatedByString:@";"];
                                
                                
                                
                                //NSLog(@" array1 %@",companyArray);
                                
                                
                                
                                //               for (int i=1; i<[companyArray count];i++) {
                                
                                
                                
                                //                   NSString * string11=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:0]];
                                //
                                //
                                //
                                //                   NSArray * array2=[string11 componentsSeparatedByString:@";"];
                                //
                                //                   //NSLog(@"%@",array2);
                                
                                NSString * companyName=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:0]];
                                NSString * EP1=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:1]];
                                NSString * TP=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:2]];
                                NSString * SL;
                                NSArray * SLArray;
                                if([mainString containsString:@"SL="])
                                {
                                    SL=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:3]];
                                    SLArray=[SL componentsSeparatedByString:@"="];
                                    SLString=[NSString stringWithFormat:@"%@",[SLArray objectAtIndex:1]];
                                    
                                }
                                else
                                {
                                    SL=@"";
                                    SLString=@"";
                                }
                                
                                NSArray * EPArray=[EP1 componentsSeparatedByString:@"="];
                                NSArray * TPArray=[TP componentsSeparatedByString:@"="];
                                
                                
                                EPString=[NSString stringWithFormat:@"%@",[EPArray objectAtIndex:1]];
                                TPString=[NSString stringWithFormat:@"%@",[TPArray objectAtIndex:1]];
                                
                                
                                
                                NSString * dummyfinalStr=[NSString stringWithFormat:@"%@ %@ @%@%@%@",buySell,companyName,EP1,TP,SL];
                                
                                NSString * finalStr = [dummyfinalStr stringByReplacingOccurrencesOfString:@"=" withString:@" "];
                                
                                //NSLog(@"%@",finalStr);
                                
                                //NSLog(@"COMPANY %@",companyName);
                                
                                
                                //                              if (i==1) {
                                //                                  EP=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                                //
                                //                                  //NSLog(@"EP %@",[array2 objectAtIndex:1]);
                                //
                                //                              }
                                
                                //                   if (i==2) {
                                //                       EPString=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                                //
                                //                       //NSLog(@"TP %@",[array2 objectAtIndex:1]);
                                //
                                //                   }
                                //
                                //                   if (i==3) {
                                //                       SLString=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                                //
                                //                       //NSLog(@"SL %@",[array2 objectAtIndex:1]);
                                //
                                //                   }
                                //               }
                                //
                                
                                
                                NSString * sell = [NSString stringWithFormat:@"<html><body><div style='text-align:left;display:inline-block;font-size:12px;font-family:Ubuntu;color:#4b4d52'>  %@</div> </body></html>",buySell];
                                NSString * muthootfin =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",[companyArray objectAtIndex:0]];
                                NSString * ep = @"<html><body><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#4b4d52'> @ EP</div></html></body>";
                                NSString * value1 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",EPString];
                                NSString * tp = @"<html><body><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#4b4d52'> TP</div></html></body>";
                                NSString * value2 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",TPString];
                                NSString * sl = @"<html><body><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#4b4d52'> SL</div></html></body>";
                                NSString * value3 =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",SLString] ;
                                
                                NSString * string1 = [sell stringByAppendingString:muthootfin];
                                NSString *string2 = [string1 stringByAppendingString:ep];
                                NSString * string3 = [string2 stringByAppendingString:value1];
                                NSString * string4 = [string3 stringByAppendingString:tp];
                                NSString * string5 = [string4 stringByAppendingString:value2];
                                NSString * string6 = [string5 stringByAppendingString:sl];
                                NSString * finalString;
                                if([mainString containsString:@"SL="])
                                {
                                    finalString = [string6 stringByAppendingString:value3];
                                }
                                else
                                {
                                    finalString = string5;
                                }
                                
                                
                                
                                NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[finalString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                                
                                //UILabel * myLabel = [[UILabel alloc] init];
                                // myLabel.attributedText = attrStr;
                                
                                
                                cell.valueChangeLabel.attributedText=attrStr;
                            }
                            @catch (NSException * e) {
                                //NSLog(@"Exception: %@", e);
                            }
                            @finally {
                                //NSLog(@"finally");
                            }
                            
                            
                            
                            
                            
                        }
                        
                    }
                    
                    
                    
                    cell.ltpLabel.text =[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"] objectForKey:@"lasttradedprice"]];
                    
                    NSString * changePercentlabel = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1] objectForKey:@"open_advice"]objectForKey:@"changepercent"]];
                    
                    NSString * percent =@"  ";
                    NSString * finalChangePercentlabel = [changePercentlabel stringByAppendingString:percent];
                    if([changePercentlabel isEqual:[NSNull null]]||changePercentlabel==nil||[changePercentlabel isEqualToString:@"<null>"])
                    {
                        
                        
                        
                        NSString * percentage = @"";
                        NSString * zero = @"  0";
                        NSString* final = [zero stringByAppendingString:percentage];
                        cell.changePercentlabel.text=final;
                        cell.changePercentlabel.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                    }
                    else
                    {
                        
                        if([finalChangePercentlabel containsString:@"-"])
                        {
                            cell.changePercentlabel.text=finalChangePercentlabel;
                            cell.changePercentlabel.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                            cell.adviceButton.layer.borderColor =[[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1] CGColor];
                            [cell.adviceButton setTitleColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1] forState:UIControlStateNormal];
                            [cell.adviceButton setTitle:@"Opening Advice" forState:UIControlStateNormal];
                            //                    cell.oLbl.backgroundColor=[UIColor colorWithRed:(238/255.0) green:(43/255.0) blue:(53/255.0) alpha:1];
                        }
                        else{
                            cell.changePercentlabel.text=finalChangePercentlabel;
                            cell.changePercentlabel.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                            cell.adviceButton.layer.borderColor =[[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1] CGColor];
                            [cell.adviceButton setTitleColor:[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1] forState:UIControlStateNormal];
                            [cell.adviceButton setTitle:@"Opening Advice" forState:UIControlStateNormal];
                            //                    cell.oLbl.backgroundColor=[UIColor colorWithRed:(86/255.0) green:(168/255.0) blue:(66/255.0) alpha:1];
                        }
                        
                        
                    }
                    
                    NSString * actedBy = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1] objectForKey:@"open_advice"]objectForKey:@"actedby"]];
                    
                    if([actedBy isEqual:[NSNull null]]||actedBy==nil||[actedBy isEqualToString:@"<null>"])
                    {
                        cell.actedByLabel.text=@"0";
                    }else
                    {
                        cell.actedByLabel.text=actedBy;
                    }
                    NSString * sharesSold = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"] objectForKey:@"sharesold"]];
                    if([sharesSold isEqual:[NSNull null]]||sharesSold==nil||[sharesSold isEqualToString:@"<null>"])
                    {
                        cell.sharesSold.text=@"0";
                    }else
                    {
                        cell.sharesSold.text=sharesSold;
                    }
                    
                    
                    
                    
                    NSString * dateStr=[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"] objectForKey:@"createdon"];
                    
                    
                    
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
                    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                    NSDate *date = [dateFormatter dateFromString:dateStr]; // create date from string
                    
                    // change to a readable time format and change to local time zone
                    [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
                    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                    NSString *finalDate = [dateFormatter stringFromDate:date];
                    
                    
                    
                    
                    
                    cell.dateAndTimeLabel.text=finalDate;
                    
                    
                    NSString * firstName=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"] objectForKey:@"firstname"]];
                    NSString * lastName=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"] objectForKey:@"lastname"]];
                    
                    if([firstName isEqualToString:@"<null>"])
                    {
                        firstName=@"";
                    }
                    
                    if([lastName isEqualToString:@"<null>"])
                    {
                        lastName=@"";
                    }
                    
                    cell.profileName.text=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                    
                    // cell.valueChangeLabel.attributedText=strText;
                    // [strText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Ubuntu-Medium" size:12.5] range:NSMakeRange(6, )];
                    
                    
                    
                    
                    
                //    [cell.buySellButton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
                    
                  //  [cell.leaderProfileAction addTarget:self action:@selector(leaderProfileMethod:) forControlEvents:UIControlEventTouchUpInside];
                    //       dispatch_async(dispatch_get_main_queue(), ^{
                    //           cell.profileImg.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[filterAdvice objectAtIndex:indexPath.row-1] objectForKey:@"logourl"]]]];
                    //
                    //
                    //       });
                    
                    
                    
                    
                    
                    @try {
                        
                        NSString * actedBy =[ [[filterAdvice objectAtIndex:indexPath.row-1] objectForKey:@"open_advice"]objectForKey:@"actedby"];
                        
                        
                        if([actedBy isEqual:[NSNull null]])
                        {
                            cell.actedByLabel.text=@"0";
                        }else
                        {
                            int actedByInt=[actedBy intValue];
                            cell.actedByLabel.text = [NSString stringWithFormat:@"%i",actedByInt];
                        }
                    } @catch (NSException *exception) {
                        
                    } @finally {
                        
                    }
                    
                    @try {
                        
                        NSString * sharesSold =[ [[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"] objectForKey:@"sharesold"];
                        
                        
                        
                        if([sharesSold isEqual:[NSNull null]])
                        {
                            cell.sharesSold.text = @"0";
                        }else
                        {
                            int sharesSoldInt=[sharesSold intValue];
                            cell.sharesSold.text = [NSString stringWithFormat:@"%i",sharesSoldInt];
                        }
                        
                    } @catch (NSException *exception) {
                        
                    } @finally {
                        
                    }
                    
                    @try {
                        
                        NSString * averageProfit =[ [[filterAdvice objectAtIndex:indexPath.row-1] objectForKey:@"open_advice"]objectForKey:@"averageprofit"];
                        
                        
                        
                        if([averageProfit isEqual:[NSNull null]])
                        {
                            
                        }else
                        {
                            int sharesSoldInt=[averageProfit intValue];
                            
                        }
                        
                    } @catch (NSException *exception) {
                        
                    } @finally {
                        
                    }
                    
                    
                    
                    NSString * duration=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"] objectForKey:@"durationtype"]];
                    
                    int durationInt=[duration intValue];
                    cell.durationLbl.hidden=NO;
                    if(durationInt==1)
                    {
                        cell.durationLbl.text=@"DT";
                        
                        cell.durationLbl.backgroundColor=[UIColor colorWithRed:(246/255.0) green:(195/255.0) blue:(117/255.0) alpha:1.0f];
                    }
                    else if(durationInt==2)
                    {
                        cell.durationLbl.text=@"ST";
                        
                        cell.durationLbl.backgroundColor=[UIColor colorWithRed:(178/255.0) green:(224/255.0) blue:(239/255.0) alpha:1.0f];
                    }
                    else if(durationInt==3)
                    {
                        cell.durationLbl.text=@"LT";
                        
                        cell.durationLbl.backgroundColor=[UIColor colorWithRed:(220/255.0) green:(172/255.0) blue:(241/255.0) alpha:1.0f];
                    }
                    
                    NSString * sub=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1] objectForKey:@"open_advice"] objectForKey:@"subscriptiontypeid"]];
                    
                    int subInt=[sub intValue];
                    
                    if(subInt==1)
                    {
                        cell.premiumImg.hidden=YES;
                        
                    }
                    
                    else if(subInt==2)
                    {
                        cell.premiumImg.hidden=NO;
                        
                        cell.premiumImg.image=[UIImage imageNamed:@"premiumNew"];
                        
                    }
                    
                    NSString * public=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1] objectForKey:@"open_advice"]objectForKey:@"ispublic"]];
                    int publicInt=[public intValue];
                    
                    if(publicInt==1)
                    {
                        cell.premiumImg.hidden=NO;
                        
                        cell.premiumImg.image=[UIImage imageNamed:@"publicnew"];
                        
                        
                        
                        NSString * source = [NSString stringWithFormat:@"%@",[[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"]objectForKey:@"otherinfo"]objectForKey:@"source"]];
                        cell.sourceLabel.hidden=NO;
                        cell.sourceDataLabel.hidden=NO;
                        cell.sourceDataLabel.text = source;
                        cell.analystName.hidden=NO;
                        cell.analystNameHeightConstant.constant=21;
                        NSString * analystName = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"]objectForKey:@"leaderorgname"]];
                        NSString * dummyAnalystName= [analystName stringByReplacingOccurrencesOfString:@" " withString:@""];
                        dummyAnalystName=[dummyAnalystName lowercaseString];
                        cell.profileName.text=analystName;
                        NSString * firstName = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"]objectForKey:@"firstname"]];
                        NSString * lastname = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"]objectForKey:@"lastname"]];
                        
                        NSString * dummyName;
                        if([lastname isEqual:[NSNull null]]||[lastname containsString:@"<null>"])
                        {
                            lastname=@"";
                            NSString * name = [NSString stringWithFormat:@"%@ %@",firstName,lastname];
                            dummyName = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
                            dummyName=[dummyName lowercaseString];
                            NSString * profileName=[cell.profileName.text stringByReplacingOccurrencesOfString:@" " withString:@""];
                             profileName=[profileName lowercaseString];
                            if(![dummyName isEqualToString:profileName])
                            {
                                cell.organizationNameLbl.text=[NSString stringWithFormat:@"%@",cell.profileName.text];
                                cell.profileName.text= [NSString stringWithFormat:@"%@ %@",firstName,lastname];
                                cell.expertXconstraint.constant=10;
                                
                            }
                            else
                            {
                                cell.expertXconstraint.constant=20;
                                cell.organizationNameLbl.text=@"";
                            }
                        }else
                        {
                            NSString * name = [NSString stringWithFormat:@"%@ %@",firstName,lastname];
                            dummyName = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
                            dummyName=[dummyName lowercaseString];
                            NSString * profileName=[cell.profileName.text stringByReplacingOccurrencesOfString:@" " withString:@""];
                            profileName=[profileName lowercaseString];
                            if(![dummyName isEqualToString:profileName])
                            {
                                cell.organizationNameLbl.text=[NSString stringWithFormat:@"%@",cell.profileName.text];
                                cell.profileName.text= [NSString stringWithFormat:@"%@ %@",firstName,lastname];
                                cell.expertXconstraint.constant=10;
                                
                            }
                            else
                            {
                                cell.expertXconstraint.constant=20;
                                cell.organizationNameLbl.text=@"";
                            }
                           
                           
                        }
                        if([dummyAnalystName containsString:dummyName])
                        {
                            cell.analystName.hidden=YES;
                            cell.analystNameHeightConstant.constant=0;
                        }else
                        {
                            cell.analystName.hidden=NO;
                            cell.analystNameHeightConstant.constant=21;
                            //                    cell.profileName.text = analystName;
                        }
                        
                        
                        
                        
                    }
                    
                    else if(publicInt==0)
                    {
                        cell.premiumImg.hidden=YES;
                        cell.expertXconstraint.constant=20;
                        cell.organizationNameLbl.text=@"";
                        cell.sourceLabel.hidden=YES;
                        cell.sourceDataLabel.hidden=YES;
                        NSString * firstName=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"] objectForKey:@"firstname"]];
                        NSString * lastName=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"] objectForKey:@"lastname"]];
                        
                        if([firstName isEqualToString:@"<null>"])
                        {
                            firstName=@"";
                        }
                        
                        if([lastName isEqualToString:@"<null>"])
                        {
                            lastName=@"";
                        }
                        
                        cell.profileName.text=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                        cell.analystName.hidden=YES;
                        cell.analystNameHeightConstant.constant=0;
                        
                    }
                    
                    
                    cell.leaderImg.image=nil;
                    
                    if(publicInt==1)
                    {
                        NSURL *url;
                        @try {
                              url =  [NSURL URLWithString:[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"] objectForKey:@"logourl"]];
                        } @catch (NSException *exception) {
                            
                            
                            
                        } @finally {
                            
                        }
                        
                        
                        NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                            if (data) {
                                UIImage *image = [UIImage imageWithData:data];
                                if (image) {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        
                                        EquitiesCell1 *cell = [self.topPerformersTableView cellForRowAtIndexPath:indexPath];
                                        if (cell)
                                            cell.leaderImg.image = image;
                                    });
                                }
                            }
                        }];
                        [task resume];
                    }else
                    {
                        
                        NSURL *url;
                        @try {
                            url =  [NSURL URLWithString:[[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"] objectForKey:@"logourl"]];
                        } @catch (NSException *exception) {
                            
                            
                            
                        } @finally {
                            
                        }
                        
                        
                        NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                            if (data) {
                                UIImage *image = [UIImage imageWithData:data];
                                if (image) {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        
                                        EquitiesCell1 *cell = [self.topPerformersTableView cellForRowAtIndexPath:indexPath];
                                        if (cell)
                                            cell.leaderImg.image = image;
                                    });
                                }
                            }
                        }];
                        [task resume];
                    }
                    
                    if(filterAdvice.count>4)
                    {
                    NSDictionary *data = [filterAdvice objectAtIndex:indexPath.row-1];
                    BOOL lastItemReached = [data isEqual:[filterAdvice lastObject]];
                    if (!lastItemReached && indexPath.row-1 == [filterAdvice count] - 3)
                    {
                        if(rehitBool==true)
                        {
                            [self refreshTableVeiwList];
                        }
                    }
                    }
                    return cell;
                }
                
            }
            
            
        }
    }
    return 0;
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    if(filterAdvice.count>0)
    {
        @try
        {
            
            
            
            NSDictionary * contra=[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"];
            NSDictionary * openAdvice=[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"];
            NSString * contraStr=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"]];
            NSDictionary * localDict=[[NSDictionary alloc]init];
            if(contra && openAdvice && ![contraStr isEqualToString:@"<null>"])
            {
                localDict=[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"];
            }
            else if(openAdvice)
            {
                localDict=[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"];
            }
            if(localDict)
            {
                delegate.navigationCheck=@"wisdom";
                int messageType=[[localDict objectForKey:@"messagetypeid"] intValue];
                if(messageType!=4)
                    
                {
                    if(delegate.leaderAdviceDetails.count>0)
                    {
                        [delegate.leaderAdviceDetails removeAllObjects];
                    }
                    //    if(tableView==self.equitiesTableView&&segmentedControl.selectedSegmentIndex==0)
                    //    {
                    //
                    //        if(indexPath.row-1==2||indexPath.row-1==7||indexPath.row-1==15||indexPath.row-1==12||indexPath.row-1==18)
                    //        {
                    //            StockAllocationView * allocation=[self.storyboard instantiateViewControllerWithIdentifier:@"stockallocation"];
                    //
                    //            [self.navigationController pushViewController:allocation animated:YES];
                    //
                    //        }
                    
                    //        else{
                    
                    NSString * public=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"ispublic"]];
                    int publicInt=[public intValue];
                    
                    if(publicInt==0)
                    {
                        delegate.depth=@"WISDOM";
                        
                        //NSLog(@"%@",delegate.depth);
                        
                        StockView1 * stockDetails=[self.storyboard instantiateViewControllerWithIdentifier:@"stock1"];
                        
                        
                        
                        delegate.symbolDepthStr=[localDict objectForKey:@"companyname"];
                        
                        delegate.exchaneLblStr=[localDict objectForKey:@"segmenttype"];
                        
                        delegate.instrumentDepthStr=[localDict objectForKey:@"instrumentid"];
                        
                        //            delegate1.expirySeriesValue=[[filterAdvice objectAtIndex:indexPath.row-1] objectForKey:@"expirydate"];
                        delegate.expirySeriesValue=@"";
                        NSString * firstName=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"firstname"]];
                        NSString * lastName=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"lastname"]];
                        
                        if([firstName isEqualToString:@"<null>"])
                        {
                            firstName=@"";
                        }
                        
                        if([lastName isEqualToString:@"<null>"])
                        {
                            lastName=@"";
                        }
                        NSString * name=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                        
                        
                        [delegate.leaderAdviceDetails addObject:name];
                        [delegate.leaderAdviceDetails addObject:[localDict objectForKey:@"createdon"]];
                        [delegate.leaderAdviceDetails addObject:[localDict objectForKey:@"lasttradedprice"]];
                        
                        [delegate.leaderAdviceDetails addObject:[localDict objectForKey:@"changepercent"]];
                        
                        [delegate.leaderAdviceDetails addObject:[localDict objectForKey:@"message"]];
                        
                        delegate.instrumentDepthStr=[localDict objectForKey:@"instrumentid"];
                        
                        
                        [delegate.leaderAdviceDetails addObject:[localDict objectForKey:@"leaderid"]];
                        [delegate.leaderAdviceDetails addObject:[localDict objectForKey:@"messageid"]];
                        [delegate.leaderAdviceDetails addObject:[localDict objectForKey:@"buysell"]];
                        NSString * logo=[localDict objectForKey:@"logourl"];
                        
                        if([logo isEqual:[NSNull null]])
                        {
                            [delegate.leaderAdviceDetails addObject:@"no"];
                        }
                        
                        else
                        {
                            [delegate.leaderAdviceDetails addObject:[localDict objectForKey:@"logourl"]];
                        }
                        
                        
                        
                        
                        //NSLog(@"Leader Advice details:%@",delegate.leaderAdviceDetails);
                        
                        [self.navigationController pushViewController:stockDetails animated:YES];
                    }else if (publicInt ==1)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            
                            
                            
                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"This is a publicly available information from the above source. All users are advised, in their own interests, to seek and obtain advice that is specific or tailored to their own circumstances and that any reliance by any user on any such publicly available advice is entirely at the risk and consequence of such user, for which Zenwise Technologies Private Limited under the brand name Zambala is not responsible or liable." preferredStyle:UIAlertControllerStyleAlert];
                            
                            
                            
                            [self presentViewController:alert animated:YES completion:^{
                                
                                
                                
                            }];
                            
                            
                            
                            UIAlertAction * proceedAction=[UIAlertAction actionWithTitle:@"Proceed" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                
                                delegate.depth=@"WISDOM";
                                
                                //NSLog(@"%@",delegate.depth);
                                
                                StockView1 * stockDetails=[self.storyboard instantiateViewControllerWithIdentifier:@"stock1"];
                                
                                
                                
                                delegate.symbolDepthStr=[localDict objectForKey:@"companyname"];
                                
                                delegate.exchaneLblStr=[localDict objectForKey:@"segmenttype"];
                                
                                delegate.instrumentDepthStr=[localDict objectForKey:@"instrumentid"];
                                
                                //            delegate1.expirySeriesValue=[[filterAdvice objectAtIndex:indexPath.row-1] objectForKey:@"expirydate"];
                                delegate.expirySeriesValue=@"";
                                NSString * firstName=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"firstname"]];
                                NSString * lastName=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"lastname"]];
                                
                                if([firstName isEqualToString:@"<null>"])
                                {
                                    firstName=@"";
                                }
                                
                                if([lastName isEqualToString:@"<null>"])
                                {
                                    lastName=@"";
                                }
                                NSString * name=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                                
                                [delegate.leaderAdviceDetails addObject:name];
                                [delegate.leaderAdviceDetails addObject:[localDict objectForKey:@"createdon"]];
                                [delegate.leaderAdviceDetails addObject:[localDict objectForKey:@"lasttradedprice"]];
                                
                                [delegate.leaderAdviceDetails addObject:[localDict objectForKey:@"changepercent"]];
                                
                                [delegate.leaderAdviceDetails addObject:[localDict objectForKey:@"message"]];
                                
                                delegate.instrumentDepthStr=[localDict objectForKey:@"instrumentid"];
                                
                                
                                [delegate.leaderAdviceDetails addObject:[localDict objectForKey:@"leaderid"]];
                                [delegate.leaderAdviceDetails addObject:[localDict objectForKey:@"messageid"]];
                                [delegate.leaderAdviceDetails addObject:[localDict objectForKey:@"buysell"]];
                                NSString * logo=[localDict objectForKey:@"logourl"];
                                
                                if([logo isEqual:[NSNull null]])
                                {
                                    [delegate.leaderAdviceDetails addObject:@"no"];
                                }
                                
                                else
                                {
                                    [delegate.leaderAdviceDetails addObject:[localDict objectForKey:@"logourl"]];
                                }
                                
                                
                                
                                
                                //NSLog(@"Leader Advice details:%@",delegate.leaderAdviceDetails);
                                
                                [self.navigationController pushViewController:stockDetails animated:YES];
                                
                            }];
                            
                            UIAlertAction * cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                
                                
                                
                            }];
                            
                            
                            
                            
                            [alert addAction:proceedAction];
                            [alert addAction:cancelAction];
                            
                        });
                        
                    }
                    
                }
                
                else
                {
                    
                    delegate.protFolioUserID = [[filterAdvice objectAtIndex:indexPath.row-1] objectForKey:@"messageid"];
                    
                
                    //
                    
                    
                    
                    
                    
                    //        dispatch_async(dispatch_get_main_queue(), ^{
                    //
                    //
                    //
                    //
                    //            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Under Development." preferredStyle:UIAlertControllerStyleAlert];
                    //
                    //
                    //
                    //            [self presentViewController:alert animated:YES completion:^{
                    //
                    //
                    //
                    //            }];
                    //
                    //
                    //
                    //            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    //
                    //
                    //
                    //            }];
                    //
                    //
                    //
                    //            [alert addAction:okAction];
                    //
                    //        });
                    //
                    
                    
                    
                    
                }
                
                //            WisdomViewController * stockView=[self.storyboard instantiateViewControllerWithIdentifier:@"rohit"];
                //
                //            [self.navigationController pushViewController:stockView animated:YES];
                //        }
                //    }
                
                
                
            }
        }@catch (NSException *exception) {
            
        } @finally {
            
        }
    }
    
    
    
    //////
    
    
    
}
-(void)onTradeButtonTap
{
    delegate.symbolDepthStr=self.companyNameString;
    delegate.tradingSecurityDes=self.symbolString;
    delegate.orderSegment=self.segmentType;
    delegate.orderBuySell=@"";
   delegate.instrumentDepthStr=self.instrumentID;
    StockOrderView * orderView=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
    delegate.tradeBtnBool=true;
    [self.navigationController pushViewController:orderView animated:YES];
}

-(void)refreshTableVeiwList
{
    int limitInt=[offset intValue];
    
    int finalInt=limitInt+10;
    
    offset=[NSNumber numberWithInt:finalInt];
    
  //  [self arrayMethod];
    [self serverHit];
    [refreshControl1 endRefreshing];
}

-(void)arrayMethod
{
    @try
    {
//        filterAdvice=[[NSMutableArray alloc]init];
//
//        filterAdvice=delegate.topPerfomersDetailArray;
//        if(filterAdvice.count>0)
//        {
//            self.activityIndicator.hidden=YES;
//            [self.activityIndicator stopAnimating];
//            self.topPerformersTableView.delegate= self;
//            self.topPerformersTableView.dataSource= self;
//            [self.topPerformersTableView reloadData];
//        }
        
        filterAdvice=[[NSMutableArray alloc]init];
        
        filterAdvice=[self.responseArray objectForKey:@"data"];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    if(indexPath.row==0)
    {
        return 235;
    }else
    {
            
            if(filterAdvice.count>0)
            {
                
                
                int messageType=[[[filterAdvice objectAtIndex:indexPath.row-1] objectForKey:@"messagetypeid"] intValue];
                
                if(messageType!=4)
                    
                {
                    NSDictionary * contra=[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"];
                    NSDictionary * openAdvice=[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"open_advice"];
                    NSString * contraStr=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row-1]objectForKey:@"contra_advice"]];
                    
                    if(contra && openAdvice && ![contraStr isEqualToString:@"<null>"])
                        
                    {
                        return 360;
                    }
                    
                    else if(openAdvice)
                    {
                        return 182;
                    }
                    else
                    {
                        return 182;
                    }
                }
                
                else
                {
                    
                    if(  [[[[filterAdvice objectAtIndex:indexPath.row-1 ] objectForKey:@"portfoliojson"] objectForKey:@"stocks"] count]==1)
                        
                    {
                        
                        
                        return 255;
                        
                    }
                    
                    
                    
                    else if(   [[[[filterAdvice objectAtIndex:indexPath.row-1 ] objectForKey:@"portfoliojson"] objectForKey:@"stocks"] count]==2)
                        
                    {
                        
                        
                        
                        return 285;
                        
                        //return 304;
                        
                    }
                    
                    
                    
                    else if(  [[[filterAdvice objectAtIndex:indexPath.row-1 ] objectForKey:@"portfoliojson"] count]==3)
                        
                    {
                        
                        return 315;
                        
                    }
                    
                    
                    
                    else if(   [[[[filterAdvice objectAtIndex:indexPath.row-1 ] objectForKey:@"portfoliojson"] objectForKey:@"stocks"] count]==4)
                        
                    {
                        
                        return 335;
                        
                    }
                    
                    
                    
                    else if(   [[[[filterAdvice objectAtIndex:indexPath.row-1 ] objectForKey:@"portfoliojson"] objectForKey:@"stocks"] count]==4)
                        
                    {
                        
                        return 365;
                        
                    }
                    
                    
                    
                    
                    
                    return 255;
                    
                    
                    
                    
                }
            }
            
            
            
            else
            {
                return 393;
            }
    }
}
-(void)scrollViewDidScroll: (UIScrollView*)scrollView
{
    
    
    
    //    if (scrollView.contentOffset.y + scrollView.frame.size.height == scrollView.contentSize.height)
    //    {
    //        [self refreshTableVeiwList];
    //    }
    //
    //    else if (self.equitiesTableView.contentOffset.y==0)
    //    {
    //        [self refreshTableVeiwList1];
    //
    //    }
    ScrollDirection scrollDirection;
    
    if (self.lastContentOffset > scrollView.contentOffset.y) {
        scrollDirection = ScrollDirectionUp;
        rehitBool=false;
    } else if (self.lastContentOffset < scrollView.contentOffset.y) {
        scrollDirection = ScrollDirectionDown;
        rehitBool=true;
    }
    
    self.lastContentOffset = scrollView.contentOffset.y;
    //    //NSLog(@"%f",scrollView.contentOffset.y);
}


- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart
{
    return self.pieChartPercentageArray.count;
}

- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
    return [[self.pieChartPercentageArray objectAtIndex:index] doubleValue];
}

- (UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
    //    if(pieChart == self.pieChartOutlet) return nil;
    return [self.sliceColors objectAtIndex:(index % self.sliceColors.count)];
}

#pragma mark - XYPieChart Delegate
- (void)pieChart:(XYPieChart *)pieChart willSelectSliceAtIndex:(NSUInteger)index
{
    //NSLog(@"will select slice at index %lu",(unsigned long)index);
}
- (void)pieChart:(XYPieChart *)pieChart willDeselectSliceAtIndex:(NSUInteger)index
{
    //NSLog(@"will deselect slice at index %lu",(unsigned long)index);
}
- (void)pieChart:(XYPieChart *)pieChart didDeselectSliceAtIndex:(NSUInteger)index
{
    //NSLog(@"did deselect slice at index %lu",(unsigned long)index);
}
- (void)pieChart:(XYPieChart *)pieChart didSelectSliceAtIndex:(NSUInteger)index
{
    //    //NSLog(@"did select slice at index %d",index);
    //    self.selectedSliceLabel.text = [NSString stringWithFormat:@"$%@",[self.slices objectAtIndex:index]];
}
- (NSString *)pieChart:(XYPieChart *)pieChart textForSliceAtIndex:(NSUInteger)index//optional
{
    return @"test";
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
typedef NS_ENUM(NSInteger, ScrollDirection) {
    ScrollDirectionNone,
    ScrollDirectionRight,
    ScrollDirectionLeft,
    ScrollDirectionUp,
    ScrollDirectionDown,
    ScrollDirectionCrazy,
};

@end
