//
//  MTLoginView.m
//  
//
//  Created by zenwise technologies on 25/05/17.
//
//

#import "MTLoginView.h"
#import "MT.h"
#import "TagEncode.h"


@interface MTLoginView ()
{
    MT *sharedManager;
}


@end

@implementation MTLoginView

- (void)viewDidLoad {
    [super viewDidLoad];
//    NSMutableArray * btBuffer_ = [[NSMutableArray alloc]init];
    
    sharedManager = [MT Mt1];
    
    

   
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
 #pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onLoginTap:(id)sender {
    
    //NSLog(@"Setting up connection to %@ : %i", @"115.118.115.61", [@"7780" intValue]);
//    CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault, (__bridge CFStringRef) @"115.118.115.61", [@"7780" intValue],&readStream, &writeStream);
//    
    messages = [[NSMutableArray alloc] init];
    
    
    
    
    
    
    CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault, (__bridge CFStringRef)@"115.118.115.61", 7780, &readStream, &writeStream);
//    inputStream = (NSInputStream *)CFBridgingRelease(readStream);
//    outputStream = (NSOutputStream *)CFBridgingRelease(writeStream);
    
   

    inputStream = (__bridge NSInputStream *)readStream;
    outputStream = (__bridge NSOutputStream *)writeStream;
    
    [outputStream setDelegate:self];
    [inputStream setDelegate:self];
    
    [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    [inputStream open];
    [outputStream open];
    
   [self open];
    
    
}

- (void)open {
    
    //NSLog(@"Opening streams.");
    
  
    
//    short userId=55001;
//
//    NSMutableData *shortUserIdData = [NSMutableData dataWithBytes:&userId length:sizeof(userId)];
//    
//    NSString * userStr=@"ADNO6";
//    
//
//    
//    short userStrShort=(short)userStr.length;
//    
//    // Variable declaration
//    
//    
//    short shcharsize=1;
//    short shshortsize=2;
//    int bufferSize = 0;
//    short shDefaultSize = 1024;
//    short shIndex = 0;
//    
//    
//    
//    short stringLength=shcharsize+shshortsize+userStrShort;
//    //NSLog(@"stringLength:%hd",stringLength);
//    
//    NSNumber * stringNumber = [NSNumber numberWithShort:stringLength];
//    int stringInt = [stringNumber intValue];
//    NSNumber * stringNum = [NSNumber numberWithInt:stringInt];
//    
//    if(bufferSize <(shIndex+stringInt))
//    {
//        int newSize = (((shIndex+stringInt)/shDefaultSize)+1)*shDefaultSize;
//        NSMutableArray * newBuffer = [[NSMutableArray alloc]initWithObjects:stringNum, nil];
//        
//        
//    }
//    
//    
////    NSMutableData *userIdStrData = [NSMutableData dataWithBytes:&userStrShort length:sizeof(userStrShort)];
//    
//   // [shortUserIdData appendData:userIdStrData];
//    
//    short shortPassword=55002;
//    
//    NSMutableData *shortPasswordData = [NSMutableData dataWithBytes:&shortPassword length:sizeof(shortPassword)];
//    
//    NSString * passwordStr=@"Poi@.321";
//    
////    short passwordStrShort=[passwordStr intValue];
//    NSMutableData * passwordStrData = [[NSMutableData alloc]initWithData:[passwordStr dataUsingEncoding:NSASCIIStringEncoding]];
//    
//    
////    NSMutableData *passwordStrData = [NSMutableData dataWithBytes:&passwordStrShort length:sizeof(passwordStrShort)];
//    
//    [shortPasswordData appendData:passwordStrData];
//    
//    [shortUserIdData appendData:shortPasswordData];
//    
//    
//    
//    
//    
//    
////    NSMutableData * postData = [[NSMutableData alloc]initWithData:[userId dataUsingEncoding:NSASCIIStringEncoding]];
////    [postData appendData:[response1 dataUsingEncoding:NSASCIIStringEncoding]];
//    // NSData *data = [[NSData alloc] initWithData:[response dataUsingEncoding:NSASCIIStringEncoding]];
//    [outputStream write:[shortPasswordData bytes] maxLength:[shortPasswordData length]];

    
    //NSLog(@"connected");
    
    TagEncode* tag=[[TagEncode alloc]init];
    
    [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.Login];
    [tag TagData:sharedManager.stUserID stringMethod:self.userName.text];
    [tag TagData:sharedManager.stPassword stringMethod:self.passwordTxt.text];
    [tag TagData:sharedManager.btTerminal byteMethod:3];
    
    
}

//- (IBAction) sendMessage {
//    

//    
//}


- (void) messageReceived:(NSString *)message {
    
    [messages addObject:message];
    
    
    //NSLog(@"%@", message);
}

- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent {
    
    //NSLog(@"stream event %lu", streamEvent);
    
    switch (streamEvent) {
            
        case NSStreamEventOpenCompleted:
            //NSLog(@"Stream opened");
            //NSLog(@"connected");
            
            
           
            
            break;
        case NSStreamEventHasBytesAvailable:
            
            if (theStream == inputStream)
            {
                uint8_t buffer[1024];
                NSInteger len;
                
                while ([inputStream hasBytesAvailable])
                {
                    len = [inputStream read:buffer maxLength:sizeof(buffer)];
                    if (len > 0)
                    {
                        NSString *output = [[NSString alloc] initWithBytes:buffer length:len encoding:NSASCIIStringEncoding];
                        
                        NSString *output1 = [output stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        
                        short messageShort=[output1 intValue];
                        
                       
                        
                        if (nil != output)
                        {
                             //NSLog(@"server said: %hd", messageShort);
                            //NSLog(@"server said: %@", output1);
                            [self messageReceived:output];
                        }
                    }
                }
            }
            break;
            
        case NSStreamEventHasSpaceAvailable:
            //NSLog(@"Stream has space available now");
            break;
            
        case NSStreamEventErrorOccurred:
            //NSLog(@"%@",[theStream streamError].localizedDescription);
            break;
            
        case NSStreamEventEndEncountered:
            
            [theStream close];
            [theStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
            NSLog(@"Disconnected");
            NSLog(@"close stream");
            break;
        default:
            NSLog(@"Unknown event");
    }
    
}




@end
//@interface SocketConnectionVC ()
//
//@end
//
//@implementation SocketConnectionVC
//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    
//    _connectedLabel.text = @"Disconnected";
//}
//
//- (IBAction) sendMessage {
//    
//    NSString *response  = [NSString stringWithFormat:@"msg:%@", _dataToSendText.text];
//    NSData *data = [[NSData alloc] initWithData:[response dataUsingEncoding:NSASCIIStringEncoding]];
//    [outputStream write:[data bytes] maxLength:[data length]];
//    
//}
//
//
//
//
//- (IBAction)connectToServer:(id)sender {
//    
//   
//}
//
//- (IBAction)disconnect:(id)sender {
//    
//    [self close];
//}
//
//
//- (void)close {
//    //NSLog(@"Closing streams.");
//    [inputStream close];
//    [outputStream close];
//    [inputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
//    [outputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
//    [inputStream setDelegate:nil];
//    [outputStream setDelegate:nil];
//    inputStream = nil;
//    outputStream = nil;
//    
//    _connectedLabel.text = @"Disconnected";
//}

//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
//
//@end
