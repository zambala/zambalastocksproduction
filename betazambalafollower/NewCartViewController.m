//
//  NewCartViewController.m
//  PremiumServices
//
//  Created by zenwise mac 2 on 7/13/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "NewCartViewController.h"
#import "ItemsInCartTableViewCell.h"
#import "AppDelegate.h"
#import "UserDetailsView.h"
#import <Mixpanel/Mixpanel.h>
#import "TagEncode.h"
#import "PaymentSelectionView.h"

@interface NewCartViewController ()
{
    AppDelegate * deleagte1;
    NSArray * allKeys;
    NSInteger itemCount;
    NSInteger totalPriceInt;
    NSMutableArray * countArray;
    NSMutableArray * totalPriceArray;
    float discountPercentage;
    
    
}

@end

@implementation NewCartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    deleagte1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.navigationItem.title =@"Cart";
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.coupanTextFiled.delegate = self;
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"cart_page"];
    self.grandTotalView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.grandTotalView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.grandTotalView.layer.shadowOpacity = 1.0f;
    self.grandTotalView.layer.shadowRadius = 1.0f;
    self.grandTotalView.layer.cornerRadius=2.1f;
    self.grandTotalView.layer.masksToBounds = NO;
    
    
    
    self.coupanView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.coupanView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.coupanView.layer.shadowOpacity = 1.0f;
    self.coupanView.layer.shadowRadius = 1.0f;
    self.coupanView.layer.cornerRadius=2.1f;
    self.coupanView.layer.masksToBounds = NO;
    
    self.paymentAction.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.paymentAction.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.paymentAction.layer.shadowOpacity = 1.0f;
    self.paymentAction.layer.shadowRadius = 1.0f;
    self.paymentAction.layer.cornerRadius=2.1f;
    self.paymentAction.layer.masksToBounds = NO;
   
    [self.paymentAction addTarget:self action:@selector(action) forControlEvents:UIControlEventTouchUpInside];
    
    [self details];
    
    
}
-(void)action
{
    //NSLog(@"Test");
    
    if(deleagte1.cartDict.count>0)
    {
        NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
        
        NSString * number= [loggedInUser stringForKey:@"profilemobilenumber"];
         NSString * email= [loggedInUser stringForKey:@"profileemail"];
         NSString * name= [loggedInUser stringForKey:@"profilename"];
     
            
            deleagte1.emailId=email;
            deleagte1.mobileNumber=number;
            deleagte1.userName1=name;
            
//            if([self.typeOfPayment isEqualToString:@"payin"])
//            {
//                if([delegate1.brokerNameStr isEqualToString:@"Upstox"])
//                {
//                    ViewController * view=[self.storyboard instantiateViewControllerWithIdentifier:@"view1"];
//                    view.segment= self.segment;
//                    view.txnAmount= self.txnAmount;
//                    view.payInCheck=@"payin";
//                    view.upiAlias = self.upiAlias;
//
//                    [self presentViewController:view animated:YES completion:nil];
//                }
//                else
//                {
//                    ViewController * view=[self.storyboard instantiateViewControllerWithIdentifier:@"view1"];
//                    view.payInCheck=@"payin";
//                    view.accountIFSC = self.accountIFSC;
//                    view.accountNo = self.accountNo;
//                    view.segment = self.segment;
//                    view.txnAmount = self.txnAmount;
//                    view.upiAlias = self.upiAlias;
//                    view.payInCheck=self.typeOfPayment;
//                    view.bankAccount = self.bankName;
//                    //            [self.navigationController pushViewController:view animated:YES];
//                    [self presentViewController:view animated:YES completion:nil];
//                }
//            }else
//            {
                PaymentSelectionView * view=[self.storyboard instantiateViewControllerWithIdentifier:@"PaymentSelectionView"];
                
                [self.navigationController pushViewController:view animated:YES];
//            }
        
//        }
        
//        UserDetailsView * view=[self.storyboard instantiateViewControllerWithIdentifier:@"UserDetailsView"];
//
//        [self.navigationController pushViewController:view animated:YES];
    
        
    }
    else
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Please add items to cart" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            // Ok action example
        }];
        [alert addAction:okAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return deleagte1.cartDict.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
   
   
    
    for(int i=0;i<deleagte1.cartDict.count;i++)
    {
        if(section==i)
        {
            NSString * string=[allKeys objectAtIndex:i];
            return [[deleagte1.cartDict objectForKey:string] count];
        }
    }

    
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    for(int i=0;i<deleagte1.cartDict.count;i++)
    {
        if(section==i)
        {
            NSString * string=[allKeys objectAtIndex:i];
            return string;
        }
    }    return nil;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
     self.cartTblHgt.constant=itemCount*57+97;
    self.contentViewHgt.constant=720+self.cartTblHgt.constant;
    
    ItemsInCartTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    
    
    NSString * string=[allKeys objectAtIndex:indexPath.section];
    
    cell.serviceTypeLbl.text=[[[deleagte1.cartDict objectForKey:string] objectAtIndex:indexPath.row] objectForKey:@"offername"];
    
    NSString * pricevalueStr=[[[deleagte1.cartDict objectForKey:string] objectAtIndex:indexPath.row] objectForKey:@"amount"];
    
    float amount=[pricevalueStr floatValue];
    
    NSString * discountString=[NSString stringWithFormat:@"%@",[[[deleagte1.cartDict objectForKey:string] objectAtIndex:indexPath.row] objectForKey:@"discvalue"]];
    
    if([discountString isEqualToString:@"<null>"])
    {
        discountPercentage=0;
    }
    
    else
    {
        discountPercentage=[[[[deleagte1.cartDict objectForKey:string] objectAtIndex:indexPath.row] objectForKey:@"discvalue"]floatValue];
    }
    float discountPrice=amount*discountPercentage/100;
    cell.priceLbl.text=[NSString stringWithFormat:@"%.2f",amount-discountPrice];
    [cell.removeBtn addTarget:self action:@selector(removeAction:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 57;
}


-(void)removeAction:(UIButton *)sender
{
    
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.cartTblView];
    
    
    
    NSIndexPath *indexPath = [self.cartTblView indexPathForRowAtPoint:buttonPosition];
    
    //NSLog(@"Index path:%ld",(long)indexPath.row);
    
    ItemsInCartTableViewCell *cell = [self.cartTblView cellForRowAtIndexPath:indexPath];
    
    @try {
        NSString * string=[allKeys objectAtIndex:indexPath.section];
        
        NSMutableArray * localArray=[[deleagte1.cartDict objectForKey:string]mutableCopy];
        
        [localArray removeObjectAtIndex:indexPath.row];
        if(localArray.count==0)
        {
            [deleagte1.cartDict removeObjectForKey:string];

        }
        
        else
        {
            [deleagte1.cartDict setObject:localArray forKey:string];
  
        }
        
        [self details];

    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
  }



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)details
{
    
    countArray=[[NSMutableArray alloc]init];
    totalPriceArray=[[NSMutableArray alloc]init];
    allKeys=[[NSArray alloc]init];
    
    allKeys=[deleagte1.cartDict allKeys];
    self.cartTblView.delegate=self;
    self.cartTblView.dataSource=self;
    
    
    for(int i=0;i<deleagte1.cartDict.count;i++)
    {
        NSString * string=[allKeys objectAtIndex:i];
        int one=(int)[[deleagte1.cartDict objectForKey:string] count];
        
        [countArray addObject:[NSNumber numberWithInt:one]];
        
        for(int j=0;j<one;j++)
        {
            NSString * priceStr=[[[deleagte1.cartDict objectForKey:string] objectAtIndex:j]objectForKey:@"amount"];
            
            float priceFLoat=[priceStr floatValue];
//            NSString * newPrice=[NSString stringWithFormat:@"%.2f",priceFLoat];
//            
//            float newFloat=[newPrice floatValue];
            
            NSString * discountString=[NSString stringWithFormat:@"%@",[[[deleagte1.cartDict objectForKey:string] objectAtIndex:j]objectForKey:@"discvalue"]];
            
            if([discountString isEqualToString:@"<null>"])
            {
                discountPercentage=0;
            }
            
            else
            {
                discountPercentage=[[[[deleagte1.cartDict objectForKey:string] objectAtIndex:j]objectForKey:@"discvalue"]floatValue];
            }
            
            
            
            
            
            
            float discountPrice=priceFLoat*discountPercentage/100;
            
            NSString * newPrice1=[NSString stringWithFormat:@"%.2f",priceFLoat-discountPrice];
            
           float newFloat1=[newPrice1 floatValue];

            [totalPriceArray addObject:[NSNumber numberWithFloat:newFloat1]];
        }
        
        
    }
    
    itemCount = 0;
    for (NSNumber *num in countArray)
    {
        itemCount += [num intValue];
    }
    
    
    self.totalItemsLbl.text=[NSString stringWithFormat:@"%ld items",(long)itemCount];
    
    totalPriceInt = 0;
    for (NSNumber *num in totalPriceArray)
    {
        totalPriceInt += [num intValue];
    }
    
    
    self.totalPriceLbl.text=[NSString stringWithFormat:@"₹%ld.00",(long)totalPriceInt];
    
    self.discountLbl.text=@"₹0.00";
    self.subTotalLbl.text=[NSString stringWithFormat:@"₹%ld.00",(long)totalPriceInt];
    self.gstLbl.text=@"₹0.00";
    self.otherChargesLbl.text=@"₹0.00";
    self.grandTotalLbl.text=[NSString stringWithFormat:@"₹%ld.00",(long)totalPriceInt];
    NSString *newStr = [self.grandTotalLbl.text substringFromIndex:1];
    deleagte1.amountForPayment=newStr;
    
    deleagte1.premiumLeaders=[deleagte1.cartDict allKeys];
    
    
    //mixpanel
//    Mixpanel *mixpanel = [Mixpanel sharedInstance];
//    [mixpanel track:@"Premium Services"
//         properties:@{@"Cart Value":self.grandTotalLbl.text}];
    [self.cartTblView reloadData];

}

//textfiled elegate

-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    [textField resignFirstResponder];

    return YES;
}

@end
