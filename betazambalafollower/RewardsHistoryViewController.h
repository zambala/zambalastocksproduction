//
//  RewardsHistoryViewController.h
//  betazambalafollower
//
//  Created by Zenwise Technologies Private Limited on 22/08/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RewardsHistoryViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIView *segmentControlView;
@property (weak, nonatomic) IBOutlet UITableView *voucherTableView;
@property (weak, nonatomic) IBOutlet UIView *voucherPopUp;
@property (weak, nonatomic) IBOutlet UIView *voucherTermsView;
@property (weak, nonatomic) IBOutlet UILabel *voucherNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *expiryDateLabel;
@property (weak, nonatomic) IBOutlet UIButton *termsAndConditionsLabel;
@property (weak, nonatomic) IBOutlet UIView *codeView;
@property (weak, nonatomic) IBOutlet UIButton *cupyVoucherButton;
@property (weak, nonatomic) IBOutlet UIButton *voucherOkButton;
@property (weak, nonatomic) IBOutlet UILabel *termsLabel;
@property (weak, nonatomic) IBOutlet UIButton *termsOkayButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *voucherCodeLabel;

@property NSString * apiKey;
@property NSMutableDictionary * vouchersHistoryDictionary;
@property NSMutableDictionary * getListReponseDictionary;
@property (nonatomic,retain)NSMutableArray *historyArray;


@end
