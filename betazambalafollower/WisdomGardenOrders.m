//
//  WisdomGardenOrders.m
//  testing
//
//  Created by zenwise technologies on 27/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "WisdomGardenOrders.h"

@interface WisdomGardenOrders ()

@end

@implementation WisdomGardenOrders

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
  
    self.marketView.hidden=YES;
     self.buySellSegment.tintColor=[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1];
    self.profileImg.layer.cornerRadius=self.profileImg.frame.size.width / 2;
    self.profileImg.clipsToBounds = YES;
    [self.buySellSegment addTarget:self action:@selector(segmentAction) forControlEvents:UIControlEventValueChanged];
    [self.marketBtn addTarget:self action:@selector(marketAction) forControlEvents:UIControlEventTouchUpInside];
    self.marketBtn.selected=YES;
    self.marketBtn.backgroundColor=[UIColor colorWithRed:(206/255.0) green:(212/255.0) blue:(215/255.0) alpha:1];
    
    [self.limitBtn addTarget:self action:@selector(limitAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.stopLossBtn addTarget:self action:@selector(stopLossAction) forControlEvents:UIControlEventTouchUpInside];
    
    NSString * sell = @"<html><body><div style='text-align:left;display:inline-block;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'> SELL</div> </body></html>";
    NSString * muthootfin = @"<html><body><b><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#1f2022'>MUTHOOTFIN</div></b></html></body>";
    NSString * ep = @"<html><body><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'> @ EP</div></html></body>";
    NSString * value1 = @"<html><body><b><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#1f2022'> 3350.00</div></b></html></body>";
    NSString * tp = @"<html><body><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'> TP</div></html></body>";
    NSString * value2 = @"<html><body><b><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#1f2022'> 3390.00</div></b></html></body>";
    NSString * sl = @"<html><body><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'> SL</div></html></body>";
    NSString * value3 = @"<html><body><b><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#1f2022'> 3300.00</div></b></html></body>";
    
    NSString * string1 = [sell stringByAppendingString:muthootfin];
    NSString *string2 = [string1 stringByAppendingString:ep];
    NSString * string3 = [string2 stringByAppendingString:value1];
    NSString * string4 = [string3 stringByAppendingString:tp];
    NSString * string5 = [string4 stringByAppendingString:value2];
    NSString * string6 = [string5 stringByAppendingString:sl];
    NSString * finalString = [string6 stringByAppendingString:value3];
    
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[finalString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    //UILabel * myLabel = [[UILabel alloc] init];
    // myLabel.attributedText = attrStr;
    self.valueChangeLabel.attributedText=attrStr;

    
//    self.wisdomOrdersScrollView.contentSize=CGSizeMake(365, 800);
}

-(void)segmentAction
{
    if(_buySellSegment.selectedSegmentIndex==0)
    {
        self.buySellSegment.tintColor=[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1];
    }
    
    else if(_buySellSegment.selectedSegmentIndex==1)
    {
        self.buySellSegment.tintColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
    }
}

-(void)marketAction
{
    self.marketView.hidden=YES;
    self.marketBtn.selected=YES;
    self.limitBtn.selected=NO;
    self.stopLossBtn.selected=NO;
     self.marketBtn.backgroundColor=[UIColor colorWithRed:(206/255.0) green:(212/255.0) blue:(215/255.0) alpha:1];
    self.limitBtn.backgroundColor=[UIColor clearColor];
     self.stopLossBtn.backgroundColor=[UIColor clearColor];
    
}

-(void)limitAction
{
    self.marketView.hidden=NO;
    self.stopLossView.hidden=YES;
    self.marketBtn.selected=NO;
    self.limitBtn.selected=YES;
    self.stopLossBtn.selected=NO;
    self.limitLbl.hidden=NO;
    self.limitTxt.hidden=NO;
    self.limitTextFldView.hidden=NO;
     self.limitBtn.backgroundColor=[UIColor colorWithRed:(206/255.0) green:(212/255.0) blue:(215/255.0) alpha:1];
    self.marketBtn.backgroundColor=[UIColor clearColor];
    self.stopLossBtn.backgroundColor=[UIColor clearColor];
}

-(void)stopLossAction
{
    self.marketView.hidden=NO;
    self.stopLossView.hidden=NO;
    self.marketBtn.selected=NO;
    self.limitBtn.selected=NO;
    self.stopLossBtn.selected=YES;
    self.limitLbl.hidden=YES;
    self.limitTxt.hidden=YES;
    self.limitTextFldView.hidden=YES;
     self.stopLossBtn.backgroundColor=[UIColor colorWithRed:(206/255.0) green:(212/255.0) blue:(215/255.0) alpha:1];
    self.limitBtn.backgroundColor=[UIColor clearColor];
    self.marketBtn.backgroundColor=[UIColor clearColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(void)selectedSegment
//{
//    if(self.orderTypeSegment.selectedSegmentIndex==1)
//    {
//        self.limitView.hidden=NO;
//        self.stopLossView.hidden=YES;
//        //NSLog(@"sdc");
//    }
//    
//    else if(self.orderTypeSegment.selectedSegmentIndex==2)
//    {
//        self.limitView.hidden=YES;
//        self.stopLossView.hidden=NO;
//        //NSLog(@"sc");
//    }
//    
//    else if(self.orderTypeSegment.selectedSegmentIndex==0)
//    {
//        self.limitView.hidden=YES;
//        self.stopLossView.hidden=YES;
//    }
//}
//
//
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
