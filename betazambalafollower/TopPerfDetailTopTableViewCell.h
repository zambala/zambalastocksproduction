//
//  TopPerfDetailTopTableViewCell.h
//  betazambalafollower
//
//  Created by Zenwise Technologies on 22/06/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XYPieChart.h"

@interface TopPerfDetailTopTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *potentialPercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (weak, nonatomic) IBOutlet XYPieChart *pieChartView;
@property (weak, nonatomic) IBOutlet UILabel *bearishValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *bullishValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *highLbl;
@property (weak, nonatomic) IBOutlet UILabel *averageLbl;
@property (weak, nonatomic) IBOutlet UILabel *lowLbl;
@property (weak, nonatomic) IBOutlet UIView *bgview;

@end
