//
//  BrokerView.m
//  testing
//
//  Created by zenwise technologies on 24/11/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "BrokerView.h"
#import "ViewController1.h"
#import <UITextField_AutoSuggestion/UITextField+AutoSuggestion.h>
#import "AppDelegate.h"
#import "NewOTPViewController.h"
#import "Reachability.h"

#import "TabBar.h"
#import "LoginViewController.h"
#import "TabMenuView.h"

@interface BrokerView () <UITextFieldAutoSuggestionDataSource, UITextFieldDelegate>
{
    NSString * checkStr;
    
    AppDelegate * delegate;
    NSMutableDictionary * brokerResponseDict;
    float y;
    NSArray * brokers;
    NSMutableArray * filterBroker;
    NSUserDefaults *prefs;
    
}


@property (nonatomic, strong) NSMutableArray *weeks;

@end

#define BROKER_ID @"broker_id"


@implementation BrokerView

- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [self.view addGestureRecognizer:tap];
    self.phoneNumberTF.delegate=self;
    self.referralTxtFld.delegate=self;
    
    self.countryCodeTF.enabled=YES;
    self.countryCodeTF.delegate=self;

  
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    delegate.loginActivityStr=@"OTP";
    self.responseDict = [[NSMutableDictionary alloc]init];
    
    self.brokerNameLbl.hidden=YES;
    self.brokerSearchFld.hidden=YES;
    [self.guestLoginButton setImage:[UIImage imageNamed:@"radioUncheckLogin.png"] forState:UIControlStateNormal];
    [self.guestLoginButton setImage:[UIImage imageNamed:@"radioCheckLogin.png"] forState:UIControlStateSelected];
    
    [self.clientLoginButton setImage:[UIImage imageNamed:@"radioUncheckLogin.png"] forState:UIControlStateNormal];
    [self.clientLoginButton setImage:[UIImage imageNamed:@"radioCheckLogin.png"] forState:UIControlStateSelected];
    self.guestLoginButton.selected=YES;
    self.guestLoginView.hidden=NO;
    self.guetLoginViewheightConstraint.constant=93;
    self.rememberLabel.hidden=YES;
    self.chkImgBtn.hidden=YES;
    [self.continueButton setTitle:@"SEND OTP" forState:UIControlStateNormal];

    
    [_chkImgBtn setImage:[UIImage imageNamed:@"checkUnselectLogin.png"] forState:UIControlStateNormal];
    [_chkImgBtn setImage:[UIImage imageNamed:@"checkSelectLogin.png"] forState:UIControlStateSelected];
    _chkImgBtn.selected = NO;
    
   prefs = [NSUserDefaults standardUserDefaults];
    
    if(prefs !=nil)
    {
    // getting an NSStringwe
        
        if(delegate.launchCheck==true)
        {
        NSString * checkString=[prefs stringForKey:@"logintype"];
        
        if([checkString isEqualToString:@"OTP1"])
        {
            delegate.launchCheck=false;
            delegate.userID=[prefs stringForKey:@"userid"];
            delegate.brokerNameStr=[prefs stringForKey:@"brokername"];
            delegate.loginActivityStr=[prefs stringForKey:@"logintype"];
//            TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
//
//
//            [self presentViewController:tabPage animated:YES completion:nil];
//            NSDictionary *headers = @{ @"content-type": @"application/json",
//                                       @"cache-control": @"no-cache",
//                                       };
//
//
//
//
//            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower/brokerinfo",delegate.baseUrl]]
//                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                               timeoutInterval:10.0];
//            [request setHTTPMethod:@"GET"];
//            [request setAllHTTPHeaderFields:headers];
//
//            NSURLSession *session = [NSURLSession sharedSession];
//            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
//                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                                                            if (error) {
//                                                                //NSLog(@"%@", error);
//                                                            } else {
//                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
//                                                                //NSLog(@"%@", httpResponse);
//
//                                                                brokerResponseDict=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//
//                                                                //NSLog(@"Broker dict:%@",brokerResponseDict);
//                                                                //                                                        int a = [[[brokerResponseDict objectForKey:@"results"] count]];
//                                                                //                                                        for (int i=0; i<a; i++) {
//                                                                //                                                            [brokers arrayByAddingObjectsFromArray:[[[brokerResponseDict objectForKey:@"results"]objectAtIndex:i]objectForKey:@"brokername"]];
//                                                                //                                                        }
//                                                                //
//
//                                                                // //NSLog(@"Brokers:%@",brokers);
//                                                                for(int i=0;i<[[brokerResponseDict objectForKey:@"results"] count];i++)
//                                                                {
//                                                                    NSString * brokerName=[NSString stringWithFormat:@"%@",[[[brokerResponseDict objectForKey:@"results"]objectAtIndex:i]objectForKey:@"brokername"]];
//
//                                                                    if([brokerName containsString:@"Zerodha"])
//                                                                    {
//                                                                        delegate.brokerInfoDict=[[brokerResponseDict objectForKey:@"results"]objectAtIndex:i];
//                                                                    }
//                                                                }
//                                                                NewLoginViewController *view = [self.storyboard instantiateViewControllerWithIdentifier:@"NewLoginViewController"];
//                                                                //               [self presentViewController:view animated:YES completion:nil];
//                                                                [self.navigationController pushViewController:view animated:YES];
//
//                                                            }
//                                                        }];
//            [dataTask resume];
//
            
//            NewLoginViewController *view = [self.storyboard instantiateViewControllerWithIdentifier:@"NewLoginViewController"];
//
//            [self.navigationController pushViewController:view animated:YES];
            
            TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
            
            [self presentViewController:tabPage animated:YES completion:nil];
            
            
        }
            
            
        }
    
    self.brokerSearchFld.text = [prefs stringForKey:@"userName"];
        NSString * localCheck=[prefs stringForKey:@"check"];
        
        if([localCheck isEqualToString:@"SELECTED"])
        {
            _chkImgBtn.selected = YES;
        }
        else if([localCheck isEqualToString:@"SELECTED"]) if([localCheck isEqualToString:@"UNSELECTED"])
        {
            _chkImgBtn.selected = NO;
        }
    
    }
    
    

    
    //dropdown//
    
    self.weeks = @[].mutableCopy;
    
    self.brokerSearchFld.delegate = self;
   
    self.brokerSearchFld.fieldIdentifier = BROKER_ID;
    
   
//    self.brokerSearchFld.showImmediately = false;
   
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)chkBoxAction:(id)sender
{
   if(self.brokerSearchFld.text.length>0)
  {
    if (chkBoxFlag == false)
    {
        _chkImgBtn.selected = YES;
       
            
            
            // saving an NSString
            
        checkStr=@"SELECTED";
            
            [prefs setObject:self.brokerSearchFld.text forKey:@"userName"];
            [prefs setObject:checkStr forKey:@"check"];
            
            
            
            
            
            [prefs synchronize];
            
             chkBoxFlag = true;
            
        
        
    }
    else
    {
        _chkImgBtn.selected = NO;
       
         checkStr=@"UNSELECTED";
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        
        
        // saving an NSString
        
        [prefs setObject:@"" forKey:@"userName"];
        
        [prefs setObject:checkStr forKey:@"check"];
        
        
        
        [prefs synchronize];
        chkBoxFlag = false;
        
    }
      }
}

- (void)viewDidAppear:(BOOL)animated
{
    if(delegate.expiryBool==true)
    {
        delegate.expiryBool=false;
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:@"Session Expired! Kindly, re-login!" preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:^{
        
    }];
    
    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:okAction];
    }
     [self brokerInfo];
     [self networkStatus];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    
     self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
     self.reach = [Reachability reachabilityForInternetConnection];
     [self.reach startNotifier];
     [super viewDidAppear:animated];
     self.phoneNumberTF.text=@"";
     [self.navigationController setNavigationBarHidden:YES animated:YES];
}


-(IBAction)continueAction:(id)sender
{
    if(self.referralTxtFld.text.length!=0)
    {
        
        delegate.referralCode=self.referralTxtFld.text;
    }
    else
    {
        delegate.referralCode=@"";
        
    }
    delegate.brokerInfoDict=[[NSMutableDictionary alloc]init];
    if([delegate.loginActivityStr isEqualToString:@"OTP"])
    {
        for(int i=0;i<[[brokerResponseDict objectForKey:@"results"] count];i++)
        {
            NSString * brokerName=[NSString stringWithFormat:@"%@",[[[brokerResponseDict objectForKey:@"results"]objectAtIndex:i]objectForKey:@"brokername"]];
            
            if([brokerName containsString:@"Zerodha"])
            {
                delegate.brokerInfoDict=[[brokerResponseDict objectForKey:@"results"]objectAtIndex:i];
            }
        }
    }
    else
        
    {
//       NSString * textField;
//        if([self.brokerSearchFld.text containsString:@"US Market"])
//        {
//            textField=@"US Market";
//        }
//
        for(int i=0;i<[[brokerResponseDict objectForKey:@"results"] count];i++)
        {
            NSString * brokerName=[NSString stringWithFormat:@"%@",[[[brokerResponseDict objectForKey:@"results"]objectAtIndex:i]objectForKey:@"brokername"]];
            
            
            if([self.brokerSearchFld.text containsString:brokerName])
            {
                delegate.brokerInfoDict=[[brokerResponseDict objectForKey:@"results"]objectAtIndex:i];
            }
//            else if ([textField containsString:@"US Market"])
//            {
//                delegate.brokerInfoDict = [[brokerResponseDict objectForKey:@"results"] objectAtIndex:i];
//            }
        }
        
        //NSLog(@"Broker Info dict:%@",delegate.brokerInfoDict);
        
        
    }
    
    if (([self.brokerSearchFld.text isEqualToString:@"Zerodha"]||[self.brokerSearchFld.text isEqualToString:@"PhillipCapital"]||[self.brokerSearchFld.text isEqualToString:@"Arcadia"]||[self.brokerSearchFld.text isEqualToString:@"Upstox"]
         ||[self.brokerSearchFld.text isEqualToString:@"Sunidhi"]||[self.brokerSearchFld.text containsString:@"Choice Trade"])&&[delegate.loginActivityStr isEqualToString:@"CLIENT"])
   {
       
       NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
       if(netStatus==NotReachable)
       {
          
           
           UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
           
           [self presentViewController:alert animated:YES completion:^{
               
           }];
           
           UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
               
           }];
           
           [alert addAction:okAction];
       }
       else
       {
           if(delegate.brokerInfoDict.count>0)
           {
           if([self.brokerSearchFld.text isEqualToString:@"Arcadia"]||[self.brokerSearchFld.text isEqualToString:@"PhillipCapital"]||[self.brokerSearchFld.text isEqualToString:@"Sunidhi"])
           {
               UIImage *NavigationPortraitBackground= [UIImage imageNamed:@"topBg1"];
               [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
               delegate.usCheck=@"ind";
               delegate.brokerNameStr=self.brokerSearchFld.text;
            

               
           }else if ([self.brokerSearchFld.text containsString:@"Choice Trade"])
           {
               UIImage *NavigationPortraitBackground= [UIImage imageNamed:@"topUS-1"];
               [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
               
               [[UINavigationBar appearance] setTranslucent:NO];
               delegate.usCheck=@"USA";
               delegate.usNavigationCheck=@"USA";
               UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"USA" bundle:nil];
               LoginViewController * usHome = [storyboard instantiateInitialViewController];
               [self presentViewController:usHome animated:YES completion:nil];
           }
           
           else
           {
               UIImage *NavigationPortraitBackground= [UIImage imageNamed:@"topBg1"];
               [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
            delegate.usCheck=@"ind";
           delegate.brokerNameStr=self.brokerSearchFld.text;
           ViewController1 *view = [self.storyboard instantiateViewControllerWithIdentifier:@"view"];
               [self.navigationController pushViewController:view animated:YES];
           }
           }else
           {
               UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Error" message:@"Internal Problem occured. Please try again." preferredStyle:UIAlertControllerStyleAlert];
               
               [self presentViewController:alert animated:YES completion:^{
                   
               }];
               
               UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                   
               }];
               
               [alert addAction:okAction];
           }
       }
    }
    else if([delegate.loginActivityStr isEqualToString:@"OTP"])
    {
        if(self.phoneNumberTF.text.length>3)
            
        {
            delegate.usCheck=@"ind";
            delegate.brokerNameStr=@"Guest";
            
            delegate.clientPhoneNumber=self.phoneNumberTF.text;
            
            delegate.userID=self.phoneNumberTF.text;
            
            //delegate1.phoneNumber= self.phoneNumberTF.text;
            
            NSDictionary *headers = @{ @"cache-control": @"no-cache",
                                       
                                       };
            
            //        NSData *postData = [[NSData alloc] initWithData:[@"{}" dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSString * number = [self.countryCodeTF.text stringByAppendingString:self.phoneNumberTF.text];
            
           
            
            NSString * url = [NSString stringWithFormat:@"http://2factor.in/API/V1/13ffd06a-2fe3-11e7-8473-00163ef91450/SMS/%@/AUTOGEN",number];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                            
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                            
                                                               timeoutInterval:10.0];
            
            [request setHTTPMethod:@"GET"];
            
            [request setAllHTTPHeaderFields:headers];
            
            
            
            NSURLSession *session = [NSURLSession sharedSession];
            
            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                              
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            
                                                            if(data!=nil)
                                                            {
                                                            
                                                            if (error) {
                                                                
                                                                //NSLog(@"%@", error);
                                                                
                                                            } else {
                                                                
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                //NSLog(@"%@", httpResponse);
                                                                self.responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                //NSLog(@"Response Dict:%@",self.responseDict);
                                                                delegate.phoneNumberSessionID= [self.responseDict objectForKey:@"Details"];
                                                                //NSLog(@"Session id:%@",delegate.phoneNumberSessionID);
                                                            }
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                if([[self.responseDict objectForKey:@"Status"]isEqualToString:@"Success"])
                                                                    
                                                                {
                                                                    
                                                                   // OTPVerificationViewViewController * otp=[self.storyboard instantiateViewControllerWithIdentifier:@"OTPVerificationViewViewController"];
                                                                    
                                                                   // [self presentViewController:otp animated:YES completion:nil];
                                                                    
                                                                    NewOTPViewController * otp = [self.storyboard instantiateViewControllerWithIdentifier:@"NewOTPViewController"];
                                                                    [self.navigationController pushViewController:otp animated:YES];
//                                                                    [self presentViewController:otp animated:YES completion:nil];
                                                                    
                                                                }
                                                                
                                                                else
                                                                    
                                                                {
                                                                    
                                                                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Something went wrong." preferredStyle:UIAlertControllerStyleAlert];
                                                                    
                                                                    
                                                                    
                                                                    [self presentViewController:alert animated:YES completion:^{
                                                                        
                                                                        
                                                                        
                                                                    }];
                                                                    
                                                                    
                                                                    
                                                                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                        
                                                                        
                                                                        
                                                                    }];
                                                                    
                                                                    
                                                                    
                                                                    [alert addAction:okAction];
                                                                    
                                                                }
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                            });
                                                            
                                                            
                                                            
                                                            
                                                            }
                                                            
                                                            
                                                        }];
            
            [dataTask resume];
            
        }
        
        
        else
            
        {
            
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Number does not match" message:@"Please enter the mobile number that you’ve registered with us." preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            [self presentViewController:alert animated:YES completion:^{
                
                
                
            }];
            
            
            
            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                
                
            }];
            
            
            
            [alert addAction:okAction];
            
            
            
            
            
        }
    }
    
    else
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Please select broker" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
    }

    
    
}



#pragma mark - UITextFieldAutoSuggestionDataSource

- (UITableViewCell *)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
    if ([field.fieldIdentifier isEqualToString:BROKER_ID]) {
        static NSString *cellIdentifier = @"MonthAutoSuggestionCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        
        NSArray *brokerNames = filterBroker;
        
        if (text.length > 0) {
            NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF BEGINSWITH[c] %@", text];
            brokerNames = [filterBroker filteredArrayUsingPredicate:filterPredictate];
        }
        
        cell.textLabel.text = brokerNames[indexPath.row];
        
        return cell;
    }
     return nil;
   
}

- (NSInteger)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section forText:(NSString *)text {
    NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF BEGINSWITH[c] %@", text];
   
    @try {
        NSInteger count = [filterBroker filteredArrayUsingPredicate:filterPredictate].count;
       
        return count;
        
    }
    @catch (NSException *exception) {
        //NSLog(@"%@", exception.reason);
    }
        
    
     return 0;
   
    
}

- (void)loadWeekDays {
    // cancel previous requests
    @try {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(loadWeekDaysInBackground) object:self];
        [self.brokerSearchFld setLoading:false];
        
        // clear previous results
       
        [self.brokerSearchFld reloadContents];
        
        // start loading
        [self.brokerSearchFld setLoading:true];
        [self performSelector:@selector(loadWeekDaysInBackground) withObject:self afterDelay:2.0f];
        
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (void)loadWeekDaysInBackground {
    // finish loading
    @try {
        [self.brokerSearchFld setLoading:false];
        
        filterBroker=[[NSMutableArray alloc]init];
       [filterBroker addObjectsFromArray:brokers];
        
        [self.brokerSearchFld reloadContents];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}



- (void)autoSuggestionField:(UITextField *)field textChanged:(NSString *)text {
   if(text.length>0)
   {
    [self loadWeekDays];
   }
        
    
}

- (CGFloat)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
    return 50;
}

- (void)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
    //NSLog(@"Selected suggestion at index row - %ld", (long)indexPath.row);
    
  
    
    if ([field.fieldIdentifier isEqualToString:BROKER_ID]) {
        NSArray *brokerNames = filterBroker;
        
        if (text.length >= 1) {
            NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF BEGINSWITH[c] %@", text];
            brokerNames = [filterBroker filteredArrayUsingPredicate:filterPredictate];
        }
        
        self.brokerSearchFld.text = brokerNames[indexPath.row];
    } 
}



#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return true;
}

- (IBAction)clientLoginAction:(id)sender {
    self.brokerNameLbl.hidden=NO;
    self.brokerSearchFld.hidden=NO;
    delegate.loginActivityStr=@"";
    self.guestLoginView.hidden=YES;
    self.guetLoginViewheightConstraint.constant=0;
    self.clientLoginButton.selected=YES;
    self.guestLoginButton.selected=NO;
    self.rememberLabel.hidden=NO;
    self.chkImgBtn.hidden=NO;
    [self.continueButton setTitle:@"CONTINUE" forState:UIControlStateNormal];
    delegate.loginActivityStr=@"CLIENT";
    
    
}
- (IBAction)guestLoginAction:(id)sender {
    self.brokerNameLbl.hidden=YES;
    self.brokerSearchFld.hidden=YES;
    delegate.loginActivityStr=@"";
    self.guestLoginView.hidden=NO;
    self.guetLoginViewheightConstraint.constant=93;
    self.clientLoginButton.selected=NO;
    self.guestLoginButton.selected=YES;
    self.rememberLabel.hidden=YES;
    self.chkImgBtn.hidden=YES;
    [self.continueButton setTitle:@"SEND OTP" forState:UIControlStateNormal];
    delegate.loginActivityStr=@"OTP";
}

-(void)dismissKeyboard
{
    [self.phoneNumberTF resignFirstResponder];
    [self.referralTxtFld resignFirstResponder];
    [self.brokerSearchFld resignFirstResponder];
    [self.countryCodeTF resignFirstResponder];
//    [self.backView setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
//
    
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
//
//    if(textField==self.brokerSearchFld)
//    {
//     y = self.brokerSearchFld.frame.origin.y;
//    }
//    else if(textField==self.phoneNumberTF)
//    {
//        y = self.phoneNumberTF.frame.origin.y;
//    }
//
//    else if(textField==self.referralTxtFld)
//    {
//        y = self.referralTxtFld.frame.origin.y;
//    }
    return YES;
}

- (void)keyboardDidShow:(NSNotification *)notification
{
    
    // Assign new frame to your view
//    [self.backView setFrame:CGRectMake(0,y-self.view.frame.size.height/2-100,self.view.frame.size.width,self.view.frame.size.height)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
   
    
    [self.view endEditing:YES];
    return YES;
}


-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    
    
}

-(BOOL)networkStatus
{
    
    
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    
    if(netStatus==NotReachable)
    {
        
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    return YES;
    
    
}


-(void)brokerInfo
{
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               
                               };
   
    
   
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower/brokerinfo",delegate.baseUrl]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==403)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                            
                                                        }
                                                        else if([httpResponse statusCode]==401)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                        brokerResponseDict=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        //NSLog(@"Broker dict:%@",brokerResponseDict);
                                                        }
//                                                        int a = [[[brokerResponseDict objectForKey:@"results"] count]];
//                                                        for (int i=0; i<a; i++) {
//                                                            [brokers arrayByAddingObjectsFromArray:[[[brokerResponseDict objectForKey:@"results"]objectAtIndex:i]objectForKey:@"brokername"]];
//                                                        }
//
                                                        
                                                       // //NSLog(@"Brokers:%@",brokers);
                                                    }
                                                }];
    [dataTask resume];
    
    
}



-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField==self.brokerSearchFld)
    {
    NSString * searchStr = [self.brokerSearchFld.text stringByReplacingCharactersInRange:range withString:string];
    if(searchStr.length>=1)
    {
        self.brokerSearchFld.autoSuggestionDataSource = self;
         [self.brokerSearchFld observeTextFieldChanges];
        brokers=@[@"Zerodha", @"PhillipCapital",@"Arcadia",@"Upstox",@"Sunidhi",@"Choice Trade"];
        return YES;
    }
        
        
    }
    
    return YES;
}


@end
