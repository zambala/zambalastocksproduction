//
//  DematCell.m
//  testing
//
//  Created by zenwise technologies on 23/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "DematCell.h"

@implementation DematCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.tradeBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.tradeBtn.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.tradeBtn.layer.shadowOpacity = 5.0f;
    self.tradeBtn.layer.shadowRadius = 4.0f;
    self.tradeBtn.layer.cornerRadius=4.1f;
    self.tradeBtn.layer.masksToBounds = NO;
    
    self.modifyBtn.layer.cornerRadius=4.1f;
    self.modifyBtn.layer.masksToBounds = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)tradeAction:(id)sender {
}
@end
