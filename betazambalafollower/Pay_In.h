//
//  Pay_In.h
//  PayPage
//
//  Created by zenwise mac 2 on 12/22/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]


@interface Pay_In : UIViewController<UIPickerViewDelegate , UIPickerViewDataSource>

{
    NSArray *bankDataArray;
    UIPickerView *bankpickerView;
    UIView *bankViewContainer;
    
    NSString *bankDataStr;
    
    NSString *accountDataStr;
    
    NSArray *accountDataArray;
    UIPickerView *accountPickerView;
    UIView *accountViewContainer;
    
    
    NSArray *segmentDataArray;
    UIPickerView *segmentpickerView;
    UIView *segmentViewContainer;
    
    NSString *segmentDataStr;
    
    
}

@property (strong, nonatomic) IBOutlet UIView *payInView1;
@property (strong, nonatomic) IBOutlet UITextField *amountTxtField;
@property (strong, nonatomic) IBOutlet UIButton *payInSubmit;

@property (strong, nonatomic) IBOutlet UIButton *bankBtn, *accountBtn, *segmentBtn, *proceedBtn;

- (IBAction)backBtn:(id)sender;


@end
