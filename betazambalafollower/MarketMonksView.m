//
//  MarketMonksView.m
//  testing
//
//  Created by zenwise mac 2 on 11/23/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "MarketMonksView.h"
#import "FollowView.h"
#import "WhoToFollow.h"
#import "HMSegmentedControl.h"
#import "FollowCell.h"
#import "ProfileViewcontroller.h"
#import "NoMonksCell.h"
#import "AppDelegate.h"
#import "NewProfile.h"
#import "Reachability.h"
#import <Mixpanel/Mixpanel.h>
@import Mixpanel;
#import "UIImageView+WebCache.h"




@interface MarketMonksView ()
{
    HMSegmentedControl * segmentedControl;/*!<segment controller*/
    AppDelegate * delegate1;/*!<appdelegate reference*/
    NSString * messageStr;/*!<it stores status message (subscribed to leader or not)*/
    NSNumber * isSuccessNumber;/*!<unused*/
    int success;
    NSMutableArray * followingUserId;/*!<followed leaders userid*/
    NSString * userId;/*!<it stores leader id to pass to mixpanel*/
    NSArray * filterArray;/*!<it stores filter data according to user search*/
    BOOL searchCheck;/*!<to check search is active or inactive*/
    NSMutableArray * firstName;/*!<it store first name of leader*/
    NSMutableArray * newFollowArray;/*!<it stores filter data according to user search*/
    NSMutableDictionary * monksDataDict;/*!<it stores data of who to follow leaders list*/
    NSString * segmentFinal;/*!<it stores segments to filter list*/
    NSString * durationFinall;/*!<it stores duration to filter list*/
    NSString * ratingFinal;/*!<it stores rating to filter list*/
    NSDictionary * params;/*!<it stores request params */
    NSMutableArray * detailsArray;/*!<it store first name of leader*/
    NSString * filterString1;/*!<it store filtered data accordind to user search*/
    NSString * leaderUserName;/*!<it store first name of leader*/
    NSMutableArray * specialization;/*!<it store specialization list*/
    NSMutableString * specializationMutString;/*!<it is support string for specialization*/
    UIImageView *hintScreen ;
    UIWindow * window;
    UIImageView *   myImageView;
    NSString * number;
}

@property (strong, nonatomic) UISearchController *searchController;


@end

@implementation MarketMonksView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"Market Experts";
    self.navigationItem.leftBarButtonItem.title=@" ";
    self.leaderDict=[[NSMutableArray alloc]init];
    self.buttonArray = [[NSMutableArray alloc]init];
    followingUserId=[[NSMutableArray alloc]init];
    self.followPlusArray=[[NSMutableArray alloc]init];
       delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
   
    
    if(delegate1.marketmonksHint==YES)
    {
        
        delegate1.marketmonksHint=NO;
        
        UIWindow *currentWindow = [UIApplication sharedApplication].keyWindow;
        
        myImageView =[[UIImageView alloc] initWithFrame:CGRectMake(0.0,0.0,currentWindow.frame.size.width,currentWindow.frame.size.height)];
        
        myImageView.image=[UIImage imageNamed:@"marketmonks"];
        
        
        [currentWindow addSubview:myImageView ];
        [currentWindow bringSubviewToFront:myImageView];
    }
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideImage)];
    singleTap.numberOfTapsRequired = 1;
    [myImageView setUserInteractionEnabled:YES];
    [myImageView addGestureRecognizer:singleTap];
    firstName=[[NSMutableArray alloc]init];
    newFollowArray=[[NSMutableArray alloc]init];
    self.buttonArrayy = [[NSMutableArray alloc]init];
    specialization = [[NSMutableArray alloc]init];
    specializationMutString = [[NSMutableString alloc]init];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.followerTableView.hidden=YES;
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
    
   number= [loggedInUser stringForKey:@"profilemobilenumber"];
   
    
    self.followerTableView.delegate=self;
    self.followerTableView.dataSource=self;
    
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"INSTITUTIONAL",@"INDIVIDUAL"]];
    segmentedControl.frame = CGRectMake(0, 0, self.view.frame.size.width, 40);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
//    segmentedControl.verticalDividerEnabled = YES;
//    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(251/255.0) green:(196/255.0) blue:(12/255.0) alpha:1];
        segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
    segmentedControl.backgroundColor=[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1];
    [self.view addSubview:segmentedControl];
    
  
   
    self.profileNameArray=[[NSMutableArray alloc]init];
    self.followersArray = [[NSMutableArray alloc]init];
    self.userIdArray = [[NSMutableArray alloc]init];
//    self.responseDictionary = [[NSMutableDictionary alloc]init];
    
    [segmentedControl setSelectedSegmentIndex:0];
    
     if([delegate1.pushNotificationSection isEqualToString:@"INSTITUTIONAL"])
     {
          delegate1.pushNotificationSection =@"";
          [segmentedControl setSelectedSegmentIndex:0];
         
     }
     else if([delegate1.pushNotificationSection isEqualToString:@"INDIVIDUAL"])
     {
          delegate1.pushNotificationSection =@"";
          [segmentedControl setSelectedSegmentIndex:1];
       
     }
    
    self.controller.delegate=self;
    self.controller.searchResultsUpdater=self;
    
// hintScreen =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
//    hintScreen.image=[UIImage imageNamed:@"2_2_hint"];
//    [self.view addSubview:hintScreen];
//
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissImage)];
//
//
//    [self.view addGestureRecognizer:tap];
//
    
    //star reating//
    
    //new//
    
  
   
}
//-(void)dismissImage
//{
//    hintScreen.hidden=YES;
//}
-(void)viewWillAppear:(BOOL)animated
{
     [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"market_experts_page"];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.followerTableView.delegate=self;
    self.followerTableView.dataSource=self;
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    
    
    
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
    
    [self segmentedControlChangedValue];
}

/*!
 @discussion unused.
 */
-(void)starOneImg1
{
    
    if (starOneFlag1==false)
    {
        starOneImg1.selected = YES;
        starTwoImg1.selected = NO;
        starThreeImg1.selected = NO;
        starFourImg1.selected = NO;
        starFiveImg1.selected = NO;
        starOneFlag1 = true;
    }
    else
    {
        starOneImg1.selected = NO;
        starTwoImg1.selected = NO;
        starThreeImg1.selected = NO;
        starFourImg1.selected = NO;
        starFiveImg1.selected = NO;
        starOneFlag1 = false;
        
    }
}
/*!
 @discussion unused.
 */
-(void)starTwoImg1
{
    starOneImg1.selected = YES;
    starTwoImg1.selected = YES;
    starThreeImg1.selected = NO;
    starFourImg1.selected = NO;
    starFiveImg1.selected = NO;
    
}
/*!
 @discussion unused.
 */

-(void)starThreeImg1
{
    starOneImg1.selected = YES;
    starTwoImg1.selected = YES;
    starThreeImg1.selected = YES;
    starFourImg1.selected = NO;
    starFiveImg1.selected = NO;
    
}
/*!
 @discussion unused.
 */

-(void)starFourImg1
{
    
    starOneImg1.selected = YES;
    starTwoImg1.selected = YES;
    starThreeImg1.selected = YES;
    starFourImg1.selected = YES;
    starFiveImg1.selected = NO;
    
}
/*!
 @discussion unused.
 */

-(void)starFiveImg1
{
    
    
    starOneImg1.selected = YES;
    starTwoImg1.selected = YES;
    starThreeImg1.selected = YES;
    starFourImg1.selected = YES;
    starFiveImg1.selected = YES;
    
}


//- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
//{
//    NSString *searchString = searchController.searchBar.text;
//    self.followerTableView.tableHeaderView = self.searchController.searchBar;
//
//    
//  // [self searchForText:searchString scope:searchController.searchBar.selectedScopeButtonIndex];
//    
//
////    [self.followerTableView reloadData];
//}

/*!
 @discussion When ever there is change in segment selection this method will trigger.
 */

-(void)segmentedControlChangedValue
{
    @try
    {
    BOOL toFollow=true;
    
//    self.followerTableView.hidden=YES;
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
        self.followerTableView.hidden = YES;
        
        NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
        if(netStatus==NotReachable)
        {
           
            
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:^{
                
            }];
            
            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            [alert addAction:okAction];
            
            
            
        }
        else
        {
            //        [self.followerTableView reloadData];
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            
            @try {
                NSString * equityStr=[prefs stringForKey:@"0"];
                
                
                if([equityStr isEqualToString:@"checkSelected"])
                {
                    
                    @try {
                        durationFinall=[prefs stringForKey:@"dur"];
                        
                    }
                    
                    @catch (NSException * e) {
                        //NSLog(@"Exception: %@", e);
                    }
                    @finally {
                        //NSLog(@"finally");
                    }
                    
                    @try {
                        segmentFinal=[prefs stringForKey:@"segment"];
                        
                    }
                    
                    @catch (NSException * e) {
                        //NSLog(@"Exception: %@", e);
                    }
                    @finally {
                        //NSLog(@"finally");
                    }
                    
                    @try {
                        ratingFinal=[prefs stringForKey:@"rating"];
                        
                    }
                    
                    @catch (NSException * e) {
                        //NSLog(@"Exception: %@", e);
                    }
                    @finally {
                        //NSLog(@"finally");
                    }
                    
                }
                
                
                else
                {
                    durationFinall=@"";
                    segmentFinal=@"";
                    ratingFinal=@"";
                }
                
                
                
            }
            @catch (NSException * e) {
                //NSLog(@"Exception: %@", e);
            }
            @finally {
                //NSLog(@"finally");
            }
            
            
            
            @try {
                NSDictionary *headers = @{ @"content-type": @"application/json",
                                           @"cache-control": @"no-cache",
                                           @"authtoken":delegate1.zenwiseToken,
                                           @"deviceid":delegate1.currentDeviceId
                                           };
                
                
                
                @try {
                    if(segmentedControl.selectedSegmentIndex==0)
                    {
                    params=@{@"clientid":delegate1.userID,
                             @"limit":@1000,
                             @"rating":ratingFinal,
                             @"durationtype":durationFinall,
                             @"segment":segmentFinal,
                             @"mobilenumber":number,
                             @"brokerid":[delegate1.brokerInfoDict objectForKey:@"brokerid"],
                             @"isinstitutional":@(true)
                             
                             };
                    }
                    else  if(segmentedControl.selectedSegmentIndex==1)
                    {
                        params=@{@"clientid":delegate1.userID,
                                 @"limit":@1000,
                                 @"rating":ratingFinal,
                                 @"durationtype":durationFinall,
                                 @"segment":segmentFinal,
                                 @"mobilenumber":number,
                                 @"brokerid":[delegate1.brokerInfoDict objectForKey:@"brokerid"],
                                  @"isinstitutional":@(false)
                                 
                                 };
                    }
                    
                }
                @catch (NSException * e) {
                    //NSLog(@"Exception: %@", e);
                }
                @finally {
                    //NSLog(@"finally");
                }
                
                NSData * postData=[NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
                
                
                NSString * localUrl=[NSString stringWithFormat:@"%@follower/allleaders",delegate1.baseUrl];
                
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:localUrl]
                                                                       cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                   timeoutInterval:10.0];
                [request setHTTPMethod:@"POST"];
                [request setAllHTTPHeaderFields:headers];
                [request setHTTPBody:postData];
                
                
                NSURLSession *session = [NSURLSession sharedSession];
                NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                if(data)
                                                                {
                                                                    if (error) {
                                                                        //NSLog(@"%@", error);
                                                                    } else {
                                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                        //NSLog(@"%@", httpResponse);
                                                                        if([httpResponse statusCode]==403)
                                                                        {
                                                                            
                                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                            
                                                                        }
                                                                        else if([httpResponse statusCode]==401)
                                                                        {
                                                                            
                                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                            
                                                                        }
                                                                        else
                                                                        {
                                                                            
                                                                            if(followingUserId.count>0)
                                                                            {
                                                                                [followingUserId removeAllObjects];
                                                                            }
                                                                            
                                                                            NSDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                            
                                                                            self.followDict=[[NSMutableArray alloc]init];
                                                                            if(responseDict)
                                                                            {
                                                                                self.followDict=[responseDict objectForKey:@"results"];
                                                                                
                                                                                //NSLog(@"Response dicttt%@",self.followDict);
                                                                                
                                                                                for(int i=0;i<self.followDict.count;i++)
                                                                                {
                                                                                    
                                                                                    
                                                                                    
                                                                                    [followingUserId addObject:[[self.followDict objectAtIndex:i] objectForKey:@"userid"]];
                                                                                }
                                                                                
                                                                                //                                                            //NSLog(@"Response Dictionary Count:%lu",[[self.responseDictionary objectForKey:@"results"] count]);
                                                                                
                                                                                
                                                                                //                                                            for(int i=0;i<[[self.responseDictionary objectForKey:@"results"]count];i++)
                                                                                //                                                            {
                                                                                //
                                                                                //                                                            NSString * localInt = [[[self.responseDictionary objectForKey: @"results"] objectAtIndex:i]objectForKey:@"subscribed"];
                                                                                //
                                                                                //                                                            success=[localInt intValue];
                                                                                //                                                            if(success==YES)
                                                                                //                                                            {
                                                                                //                                                                [self.buttonArray addObject:@"Following"];
                                                                                //                                                            }else
                                                                                //                                                            {
                                                                                //                                                                [self.buttonArray addObject:@"Follow +"];
                                                                                //                                                            }
                                                                                //                                                            //NSLog(@"Button array:%@",self.buttonArray);
                                                                                //                                                            }
                                                                                
                                                                                //                                                                        Mixpanel *mixpanel1 = [Mixpanel sharedInstance];
                                                                                //
                                                                                //                                                                        [mixpanel1 identify:delegate1.userID];
                                                                                
                                                                                NSMutableString * string=[[NSMutableString alloc]init];
                                                                                
                                                                                for(int i=0;i<self.followDict.count;i++)
                                                                                {
                                                                                    NSMutableString * string1=[[NSMutableString alloc]init];
                                                                                    string1=[NSMutableString stringWithFormat:@"%@,",[[self.followDict objectAtIndex:i] objectForKey:@"firstname"]];
                                                                                    
                                                                                    [string appendString:string1];                                       }
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                //                                                                        [mixpanel1.people set:@{@"LeaderFollowing":string}];
                                                                                
                                                                                
                                                                                
                                                                                
                                                                            }
                                                                            
                                                                        }
                                                                    }
                                                                    
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        if(self.followDict.count>0)
                                                                        {
                                                                            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                                                            
                                                                            [prefs removeObjectForKey:@"0"];
                                                                            
                                                                            
                                                                            self.followerTableView.hidden=NO;
                                                                            
                                                                            self.activityIndicator.hidden=YES;
                                                                            [self.activityIndicator stopAnimating];
                                                                            //                                                                            self.followerTableView.delegate=self;
                                                                            //                                                                        self.followerTableView.dataSource=self;
                                                                            [self.followerTableView reloadData];
                                                                        }
                                                                        
                                                                    });
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                                            
                                                                            [self presentViewController:alert animated:YES completion:^{
                                                                                
                                                                            }];
                                                                            
                                                                            
                                                                            UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                                [self segmentedControlChangedValue];
                                                                            }];
                                                                            
                                                                            UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                                
                                                                            }];
                                                                            
                                                                            
                                                                            [alert addAction:retryAction];
                                                                            [alert addAction:cancel];
                                                                            
                                                                        });
                                                                        
                                                                        
                                                                    });
                                                                    
                                                                }
                                                                
                                                            }];
                [dataTask resume];
                
                
            }
            @catch (NSException * e) {
                //NSLog(@"Exception: %@", e);
            }
            @finally {
                //NSLog(@"finally");
            }
            
        }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

        
        if(self.controller.isActive==YES&&newFollowArray.count>0)
        {
            return newFollowArray.count;
        }
        
        
        
        
        else  if(self.followDict.count>0&&self.controller.isActive==NO)
        {
            return self.followDict.count;
        }
        
        else
        {
            return 1;
            
            
        }
        
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     @try {
    
    
       
        if(self.controller.active==YES&&self.controller.searchBar.text.length>=1)
        {
        //        self.brokerMesssagesTableView.hidden=YES;
        //        self.notoficationstableView.hidden=NO;
//        FollowCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
//        
//        cell.profileName.text=@"Ankit Sharma";
//        return cell;
        
        if([filterArray  count]>0)
        {
            FollowCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
//            if(segmentedControl.selectedSegmentIndex==0)
//            {
//            [cell.followBtn setTitle:@"Following" forState:UIControlStateNormal];
//            [cell.followBtn setUserInteractionEnabled:NO];
//            }
            
         
//             cell.profileName.text=[NSString stringWithFormat:@"%@",[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"firstname"]];
            NSString * firstName=[NSString stringWithFormat:@"%@",[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"firstname"]];
            NSString * lastName=[NSString stringWithFormat:@"%@",[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"lastname"]];
            
            if([firstName isEqualToString:@"<null>"])
            {
                firstName=@"";
            }
            
            if([lastName isEqualToString:@"<null>"])
            {
               
                cell.profileName.text=[NSString stringWithFormat:@"%@",firstName];
            }
            else
            {
                cell.profileName.text=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
            }
            
            
              cell.followeCountLabel.text=@"0 Followers";
             int ispublic=[[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"ispublicleader"] intValue];
             cell.bgImgView.hidden=YES;
            if(ispublic==0)
            {
               
                cell.bgImgView.hidden=NO;
                cell.bgImgView.image=[UIImage imageNamed:@"Expertsbg"];
                 [cell.bgView setBackgroundColor:[UIColor clearColor]];
                cell.bgView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
                cell.bgView.layer.shadowOffset = CGSizeMake(0, 3.0f);
                cell.bgView.layer.shadowOpacity = 5.0f;
                cell.bgView.layer.shadowRadius = 5.0f;
                cell.bgView.layer.cornerRadius=10.0f;
                cell.bgView.layer.masksToBounds = NO;
                int subscribed=[[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"subscribed"] intValue];
                if(subscribed==1)
                {
                    [cell.followBtn setTitle:@"Following" forState:UIControlStateNormal];
                    [cell.followBtn setBackgroundColor:[UIColor clearColor]];
                    cell.followBtn.layer.borderWidth=1.0f;
                    cell.followBtn.layer.borderColor=[[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1] CGColor];
                     [cell.followBtn setTitleColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1] forState:UIControlStateNormal];
                }
                else if(subscribed==0)
                {
                    [cell.followBtn setTitle:@"+ Follow" forState:UIControlStateNormal];
                    [cell.followBtn setBackgroundColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1]];
                    cell.followBtn.layer.borderWidth=0.0f;
                    [cell.followBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                   
                }
                NSString *followersString =[NSString stringWithFormat:@"%@ followers", [[newFollowArray objectAtIndex:indexPath.row] objectForKey:@"followercount"]];
                
                NSString * testString =[NSString stringWithFormat:@"%@", [[newFollowArray objectAtIndex:indexPath.row] objectForKey:@"followercount"]];
                
                if([testString isEqual:[NSNull null]])
                {
                    cell.followeCountLabel.text=@"0 Followers";
                }
                else
                {
                    cell.followeCountLabel.text=followersString;
                }
                
            }
            else if(ispublic==1)
            {
//                    cell.bgView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
//                    cell.bgView.layer.shadowOffset = CGSizeMake(0, 5.0f);
//                    cell.bgView.layer.shadowOpacity = 5.0f;
//                    cell.bgView.layer.shadowRadius = 4.0f;
//                    cell.bgView.layer.cornerRadius=10.0f;
//                    cell.bgView.layer.masksToBounds = NO;
//                NSString * orgname=[NSString stringWithFormat:@"%@",[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"orgname"]];
//                NSString * name=cell.profileName.text;
//                [name stringByReplacingOccurrencesOfString:@" " withString:@""];
//                NSString * localOrgName=orgname;
//               
//                if(![orgname isEqualToString:@"<null>"])
//                {
//                    if([name isEqualToString:localOrgName])
//                    {
//                        
//                    }
//                    else
//                    {
//                        NSString * final=[NSString stringWithFormat:@"%@ (%@)",cell.profileName.text,orgname];
//                        cell.profileName.text=final;
//                    }
//                }
//                else
//                {
//                    
//                }
//              
//                
//                cell.bgImgView.hidden=YES;
//                cell.publicImgView.image=nil;
//                [cell.followBtn setBackgroundColor:[UIColor colorWithRed:(208/255.0) green:(208/255.0) blue:(208/255.0) alpha:1]];
//                [cell.followBtn setTitle:@"Following" forState:UIControlStateNormal];
//                [cell.followBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//                cell.followBtn.layer.borderWidth=0.0f;
//                
                
                
                   [cell.bgView setBackgroundColor:[UIColor colorWithRed:255/255 green:255/255 blue:255/255 alpha:1]];
                cell.bgView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
                cell.bgView.layer.shadowOffset = CGSizeMake(0, 3.0f);
                cell.bgView.layer.shadowOpacity = 5.0f;
                cell.bgView.layer.shadowRadius = 5.0f;
                cell.bgView.layer.cornerRadius=10.0f;
                cell.bgView.layer.masksToBounds = NO;
                NSString * orgname=[NSString stringWithFormat:@"%@",[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"orgname"]];
                int subscribed=[[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"subscribed"] intValue];
                if(subscribed==1)
                {
                    [cell.followBtn setTitle:@"Following" forState:UIControlStateNormal];
                    [cell.followBtn setBackgroundColor:[UIColor clearColor]];
                    cell.followBtn.layer.borderWidth=1.0f;
                    cell.followBtn.layer.borderColor=[[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1] CGColor];
                    [cell.followBtn setTitleColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1] forState:UIControlStateNormal];
                }
                else if(subscribed==0)
                {
                    [cell.followBtn setTitle:@"+ Follow" forState:UIControlStateNormal];
                    [cell.followBtn setBackgroundColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1]];
                    cell.followBtn.layer.borderWidth=0.0f;
                    [cell.followBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    
                }
                NSString * name=cell.profileName.text;
                [name stringByReplacingOccurrencesOfString:@" " withString:@""];
                NSString * localOrgName=orgname;
                
                
                if(![orgname isEqualToString:@"<null>"])
                {
                    if([name isEqualToString:localOrgName])
                    {
                        
                    }
                    else
                    {
                        NSString * final=[NSString stringWithFormat:@"%@ (%@)",cell.profileName.text,orgname];
                        cell.profileName.text=final;
                    }
                }
                else
                {
                    
                }
                
                cell.bgImgView.hidden=YES;
                cell.publicImgView.image=nil;
//                [cell.followBtn setBackgroundColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1]];
//                [cell.followBtn setTitle:@"Following" forState:UIControlStateNormal];
//                [cell.followBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//                cell.followBtn.layer.borderWidth=0.0f;
            }
            
            
            
            
           
           
           
            
//              cell.aboutLbl.text = @"No info";
            
            @try {
                NSString * aboutString = [NSString stringWithFormat:@"%@",[[[newFollowArray objectAtIndex:indexPath.row] objectForKey:@"about"] objectForKey:@"info"]];
                
                
                if([aboutString isEqual:[NSNull null]])
                {
                    cell.aboutLbl.text = @"No info";
                }else
                {
                    cell.aboutLbl.text = aboutString;
                }
            } @catch (NSException *exception) {
                
              
                
            } @finally {
                
            }
            
           

            NSString * winnerCount=[NSString stringWithFormat:@"%@",[[[newFollowArray objectAtIndex:indexPath.row] objectForKey:@"openmessagedata"]objectForKey:@"winnersCount"]];
            NSString * losserCount=[NSString stringWithFormat:@"%@",[[[newFollowArray objectAtIndex:indexPath.row] objectForKey:@"openmessagedata"]objectForKey:@"losersCount"]];
            
            cell.winnerLbl.text=winnerCount;
            cell.looserLbl.text=losserCount;
            
            NSString * str=@"%";
            NSString * hitRate=[NSString stringWithFormat:@"%@",[[newFollowArray  objectAtIndex:indexPath.row] objectForKey:@"hitrate"]];
            if([hitRate isEqual:[NSNull null]]||[hitRate isEqualToString:@"<null>"])
            {
                cell.successRateLbl.text=@"0%";
            }
            else
            {
                hitRate=[NSString stringWithFormat:@"%.2f%@",[[[newFollowArray  objectAtIndex:indexPath.row] objectForKey:@"hitrate"]floatValue],str];
                cell.successRateLbl.text=hitRate;
            }
            
            NSString * rating = [NSString stringWithFormat:@"%@",[[newFollowArray objectAtIndex:indexPath.row] objectForKey:@"rating"]];
           
            
//            cell.profileName.text=[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"firstname"];
            cell.leaderActiveSts.text=@"Inactive";
            
            if([[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"created"] isEqual:[NSNull null]])
            {
                cell.leaderActiveSts.text=@"Inactive";
                
            }else
            {
            NSString *dateStr =[NSString stringWithFormat:@"%@",[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"created"]];
                
                //NSLog(@"%@",dateStr);
            
            // Convert string to date object
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];  //2017-05-31T13:45:29.623Z
                [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
                NSDate *date = [dateFormat dateFromString:dateStr];
            

            NSString *differnce = [self remaningTime:date endDate:[NSDate date]];
                
                cell.leaderActiveSts.text=[NSString stringWithFormat:@"(last active %@ ago)",differnce];
            
            //NSLog(@"%@",differnce);
            }
            
            cell.gainPer.text=@"0% gain";
            if([[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"gainpercent"] isEqual:[NSNull null]])
            {
                 cell.gainPer.text=@"0% gain";
                
            }else
            {
                NSString * gain=[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"gainpercent"];
                                 
                float gainFloat=[gain floatValue];
                
                NSString * str=@"%";
                
                cell.gainPer.text=[NSString stringWithFormat:@"%.2f%@ gain",gainFloat,str];
            }
            
            cell.starRatingView.minimumValue=0;
            
            cell.starRatingView.maximumValue=5;
            
             cell.starRatingView.value=0;
            
            if([rating isEqual:[NSNull null]])
            {
                cell.starRatingView.value=0;

            }else
            {
                 float ratingFloat = [rating floatValue];
            
            cell.starRatingView.value=ratingFloat;
            }
            cell.starRatingView.allowsHalfStars=YES;
            
            cell.starRatingView.allowsHalfStars=YES;
            
            cell.starRatingView.emptyStarImage = [UIImage imageNamed:@"ratingstarEmpty"];
            
            cell.starRatingView.halfStarImage = [UIImage imageNamed:@"starsmallHalf"]; // optional
            
            cell.starRatingView.filledStarImage = [UIImage imageNamed:@"ratingstarFilled"];
            
            [cell.starRatingView setUserInteractionEnabled:NO];
            [cell.followBtn addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            
             // or cell.poster.image = [UIImage imageNamed:@"placeholder.png"];
            
             cell.profileimage.image=[UIImage imageNamed:@"dpDefault"];
            
            NSURL *url;
            @try {
                url =  [NSURL URLWithString:[NSString stringWithFormat:@"%@",[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"logourl"]]];
            } @catch (NSException *exception) {
                
                
                
            } @finally {
                
            }
            
            
         [cell.profileimage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"dpDefault"]];
            
//            NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//                if (data) {
//                    UIImage *image = [UIImage imageWithData:data];
//                    if (image) {
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            FollowCell *cell = [self.followerTableView cellForRowAtIndexPath:indexPath];
//
//                            if (cell)
//                            {
//                                cell.profileimage.image = image;
//                            }
//                        });
//                    }
//                }
//            }];
//            [task resume];
            
            
            
            return cell;
            
        }
        else
        {
        
        NoMonksCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
        
        
        return cell;
            
        }
            
        }
        
        else
        {
            
            if(self.followDict.count>0)
            {
                FollowCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
                
                NSString *followersString = [NSString stringWithFormat:@"%@ followers",[[self.followDict  objectAtIndex:indexPath.row] objectForKey:@"followercount"]];
                
               
                NSString * testString = [NSString stringWithFormat:@"%@",[[self.followDict  objectAtIndex:indexPath.row] objectForKey:@"followercount"]];
                NSString * firstName=[NSString stringWithFormat:@"%@",[[self.followDict objectAtIndex:indexPath.row ]objectForKey:@"firstname"]];
                NSString * lastName=[NSString stringWithFormat:@"%@",[[self.followDict objectAtIndex:indexPath.row ]objectForKey:@"lastname"]];
                
                if([firstName isEqualToString:@"<null>"])
                {
                    firstName=@"";
                }
                
                if([lastName isEqualToString:@"<null>"])
                {
                    cell.profileName.text=[NSString stringWithFormat:@"%@",firstName];
                }
                else
                {
                     cell.profileName.text=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                }
                
               
                NSString * str=@"%";
                
                NSString * hitRate=[NSString stringWithFormat:@"%@",[[self.followDict  objectAtIndex:indexPath.row] objectForKey:@"hitrate"]];
                if([hitRate isEqual:[NSNull null]]||[hitRate isEqualToString:@"<null>"])
                {
                    cell.successRateLbl.text=@"0%";
                }
                else
                { hitRate=[NSString stringWithFormat:@"%.2f%@",[[[self.followDict  objectAtIndex:indexPath.row] objectForKey:@"hitrate"]floatValue],str];
                    cell.successRateLbl.text=hitRate;
                }
                
                cell.leaderActiveSts.text=@"Inactive";
                
                if([[[self.followDict objectAtIndex:indexPath.row ]objectForKey:@"created"] isEqual:[NSNull null]])
                {
                    cell.leaderActiveSts.text=@"Inactive";
                    
                }else
                {
                    NSString *dateStr =[NSString stringWithFormat:@"%@",[[self.followDict  objectAtIndex:indexPath.row ]objectForKey:@"created"]];
                    
                    // Convert string to date object
                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];  //2017-05-31T13:45:29.623Z
                    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
                    NSDate *date = [dateFormat dateFromString:dateStr];
                    
                    
                    NSString *differnce = [self remaningTime:date endDate:[NSDate date]];
                    
                    cell.leaderActiveSts.text=[NSString stringWithFormat:@"(last active %@ ago)",differnce];
                    
                    //NSLog(@"%@",differnce);
                }
                
                
//                cell.aboutLbl.text = @"No info";
                @try {
                    NSString * aboutString = [NSString stringWithFormat:@"%@",[[[self.followDict  objectAtIndex:indexPath.row ]objectForKey:@"about"]objectForKey:@"info"]];
                    
                    
                    if([aboutString isEqual:[NSNull null]])
                    {
                        cell.aboutLbl.text = @"No info";
                    }else
                    {
                        cell.aboutLbl.text = aboutString;
                    }
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                cell.gainPer.text=@"0% gain";
            
                if([[[self.followDict  objectAtIndex:indexPath.row ]objectForKey:@"gainpercent"] isEqual:[NSNull null]])
                {
                    cell.gainPer.text=@"0% gain";
                    
                }else
                {
                    NSString * gain=[NSString stringWithFormat:@"%@",[[self.followDict  objectAtIndex:indexPath.row ]objectForKey:@"gainpercent"]];
                    
                    float gainFloat=[gain floatValue];
                    
                    NSString * str=@"%";
                    
                    cell.gainPer.text=[NSString stringWithFormat:@"%.2f%@ gain",gainFloat,str];
                }

                cell.followeCountLabel.text=@"0 Followers";
                int ispublic=[[[self.followDict objectAtIndex:indexPath.row ]objectForKey:@"ispublicleader"] intValue];
                cell.bgImgView.hidden=YES;
                if(ispublic==0)
                {   cell.bgImgView.hidden=NO;
                    cell.bgImgView.image=[UIImage imageNamed:@"Expertsbg"];
                    [cell.bgView setBackgroundColor:[UIColor clearColor]];
                    cell.bgView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
                    cell.bgView.layer.shadowOffset = CGSizeMake(0, 3.0f);
                    cell.bgView.layer.shadowOpacity = 5.0f;
                    cell.bgView.layer.shadowRadius = 4.0f;
                    cell.bgView.layer.cornerRadius=10.0f;
                    cell.bgView.layer.masksToBounds = NO;
                    int subscribed=[[[self.followDict objectAtIndex:indexPath.row ]objectForKey:@"subscribed"] intValue];
                    if(subscribed==1)
                    {
                        [cell.followBtn setTitle:@"Following" forState:UIControlStateNormal];
                        [cell.followBtn setBackgroundColor:[UIColor clearColor]];
                        cell.followBtn.layer.borderWidth=1.0f;
                        cell.followBtn.layer.borderColor=[[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1] CGColor];
                        [cell.followBtn setTitleColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1] forState:UIControlStateNormal];
                    }
                    else if(subscribed==0)
                    {
                        [cell.followBtn setTitle:@"+ Follow" forState:UIControlStateNormal];
                        [cell.followBtn setBackgroundColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1]];
                        cell.followBtn.layer.borderWidth=0.0f;
                        [cell.followBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        
                    }
                    NSString *followersString =[NSString stringWithFormat:@"%@ followers", [[self.followDict objectAtIndex:indexPath.row] objectForKey:@"followercount"]];
                    
                    NSString * testString =[NSString stringWithFormat:@"%@", [[self.followDict objectAtIndex:indexPath.row] objectForKey:@"followercount"]];
                    
                    if([testString isEqual:[NSNull null]])
                    {
                        cell.followeCountLabel.text=@"0 Followers";
                    }
                    else
                    {
                        cell.followeCountLabel.text=followersString;
                    }
                }
                else if(ispublic==1)
                {
                    [cell.bgView setBackgroundColor:[UIColor colorWithRed:255/255 green:255/255 blue:255/255 alpha:1]];
                    cell.bgView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
                    cell.bgView.layer.shadowOffset = CGSizeMake(0, 3.0f);
                    cell.bgView.layer.shadowOpacity = 5.0f;
                    cell.bgView.layer.shadowRadius = 5.0f;
                    cell.bgView.layer.cornerRadius=10.0f;
                    cell.bgView.layer.masksToBounds = NO;
                    NSString * orgname=[NSString stringWithFormat:@"%@",[[self.followDict objectAtIndex:indexPath.row ]objectForKey:@"orgname"]];
                    
                    NSString * name=cell.profileName.text;
                    [name stringByReplacingOccurrencesOfString:@" " withString:@""];
                    NSString * localOrgName=orgname;
                   
                    
                    if(![orgname isEqualToString:@"<null>"])
                    {
                        if([name isEqualToString:localOrgName])
                        {
                          
                        }
                        else
                        {
                            NSString * final=[NSString stringWithFormat:@"%@ (%@)",cell.profileName.text,orgname];
                            cell.profileName.text=final;
                        }
                    }
                    else
                    {
                        
                    }
                    
                    cell.bgImgView.hidden=YES;
                    cell.publicImgView.image=nil;
                    [cell.followBtn setBackgroundColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1]];
                    [cell.followBtn setTitle:@"Following" forState:UIControlStateNormal];
                    [cell.followBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    cell.followBtn.layer.borderWidth=0.0f;
                    int subscribed=[[[self.followDict objectAtIndex:indexPath.row ]objectForKey:@"subscribed"] intValue];
                    if(subscribed==1)
                    {
                        [cell.followBtn setTitle:@"Following" forState:UIControlStateNormal];
                        [cell.followBtn setBackgroundColor:[UIColor clearColor]];
                        cell.followBtn.layer.borderWidth=1.0f;
                        cell.followBtn.layer.borderColor=[[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1] CGColor];
                        [cell.followBtn setTitleColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1] forState:UIControlStateNormal];
                    }
                    else if(subscribed==0)
                    {
                        [cell.followBtn setTitle:@"+ Follow" forState:UIControlStateNormal];
                        [cell.followBtn setBackgroundColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1]];
                        cell.followBtn.layer.borderWidth=0.0f;
                        [cell.followBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        
                    }
                }
                
                
                
                

                NSString * winnerCount=[NSString stringWithFormat:@"%@",[[[self.followDict objectAtIndex:indexPath.row] objectForKey:@"openmessagedata"]objectForKey:@"winnersCount"]];
                NSString * losserCount=[NSString stringWithFormat:@"%@",[[[self.followDict objectAtIndex:indexPath.row] objectForKey:@"openmessagedata"]objectForKey:@"losersCount"]];
                 
                cell.winnerLbl.text=winnerCount;
                cell.looserLbl.text=losserCount;
                
            
                NSString * rating = [NSString stringWithFormat:@"%@",[[self.followDict objectAtIndex:indexPath.row] objectForKey:@"rating"]];
                cell.starRatingView.minimumValue=0;
                
                cell.starRatingView.maximumValue=5;
                
                if([rating isEqual:[NSNull null]])
                {
                    cell.starRatingView.value=0;
                }else
                {
                    float ratingFloat = [rating floatValue];
                cell.starRatingView.value=ratingFloat;
                }
                
                cell.starRatingView.allowsHalfStars=YES;
                
                cell.starRatingView.allowsHalfStars=YES;
                
                cell.starRatingView.emptyStarImage = [UIImage imageNamed:@"ratingstarEmpty"];
                
                cell.starRatingView.halfStarImage = [UIImage imageNamed:@"starsmallHalf"]; // optional
                
                cell.starRatingView.filledStarImage = [UIImage imageNamed:@"ratingstarFilled"];
                
                [cell.starRatingView setUserInteractionEnabled:NO];
                [cell.followBtn addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
                
                
                 // or cell.poster.image = [UIImage imageNamed:@"placeholder.png"];
                
                cell.profileimage.image=[UIImage imageNamed:@"dpDefault"];
                
                NSURL *url;
                @try {
                    url =  [NSURL URLWithString:[NSString stringWithFormat:@"%@",[[self.followDict objectAtIndex:indexPath.row] objectForKey:@"logourl"]]];
                } @catch (NSException *exception) {
                    
                    
                    
                } @finally {
                    
                }
                
                
                
                 [cell.profileimage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"dpDefault"]];
               
                
//                NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//                    if (data) {
//                        UIImage *image = [UIImage imageWithData:data];
//                        if (image) {
//                            dispatch_async(dispatch_get_main_queue(), ^{
//
//                                FollowCell *cell = [self.followerTableView cellForRowAtIndexPath:indexPath];
//
//                                if (cell)
//                                {
//                                    cell.profileimage.image = image;
//                                }
//                            });
//                        }
//                    }
//                }];
//                [task resume];
//
                
                
                return cell;
                
            }
            else
            {
                
                NoMonksCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
                
                
                return cell;
                
            }

        }

    
  
    
        

        
//        NoMonksCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];

        
    
        
    
    
       return 0;
         
     } @catch (NSException *exception) {
         
         
         
     } @finally {
         
     }
    
    
}
// Date

/*!
 @discussion unused.
 @param from date ,to date.
 */

-(NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    @try
    {
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

}


// New Date and Time

/*!
 @discussion unused.
 @param startdate,enddate.
 */

-(NSString*)remaningTime:(NSDate*)startDate endDate:(NSDate*)endDate
{
    @try
    {
    NSDateComponents *components;
    NSInteger days;
    NSInteger hour;
    NSInteger minutes;
    NSString *durationString;
    
    components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate: startDate toDate: endDate options: 0];
    
    days = [components day];
    hour = [components hour];
    minutes = [components minute];
    
    if(days>0)
    {
        if(days>1)
            durationString=[NSString stringWithFormat:@"%ld days",(long)days];
        else
            durationString=[NSString stringWithFormat:@"%ld day",(long)days];
        return durationString;
    }
    if(hour>0)
    {
        if(hour>1)
            durationString=[NSString stringWithFormat:@"%ld hrs",(long)hour];
        else
            durationString=[NSString stringWithFormat:@"%ld hrs",(long)hour];
        return durationString;
    }
    if(minutes>0)
    {
        if(minutes>1)
            durationString = [NSString stringWithFormat:@"%ld min",(long)minutes];
        else
            durationString = [NSString stringWithFormat:@"%ld min",(long)minutes];
        
        return durationString;
    }
    return @"";
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

}

    

//follow method//

/*!
 @discussion when user tap on follow button this method will triger and here we subscribe or unsubscribe to leader.
 
 */
-(void)yourButtonClicked:(UIButton*)sender
{
//    if (sender.tag == 0)
//    {
//
//    }
    
    
    @try
    {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.followerTableView];
    
    
    
    NSIndexPath *indexPath = [self.followerTableView indexPathForRowAtPoint:buttonPosition];
    
    //NSLog(@"Index path:%ld",(long)indexPath.row);
    FollowCell *cell = [self.followerTableView cellForRowAtIndexPath:indexPath];
    
    
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
       
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        //mixpanel//
//        Mixpanel *mixpanel = [Mixpanel sharedInstance];
//
//        [mixpanel identify:delegate1.userID];
        int ispublic;
        
        @try {
            NSDictionary *headers = @{ @"content-type": @"application/x-www-form-urlencoded",
                                       @"cache-control": @"no-cache",
                                        @"authtoken":delegate1.zenwiseToken,
                                          @"deviceid":delegate1.currentDeviceId
                                       };
            
            
            if(self.controller.active==YES&&self.controller.searchBar.text.length>=1)
            {
               
                     
                      userId=[NSString stringWithFormat:@"leaderid=%@", [[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"userid"]];
                        
                
                      ispublic=[[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"ispublicleader"] intValue];

            
                
            }
            
            else
            {
                
    
                userId=[NSString stringWithFormat:@"leaderid=%@",[followingUserId objectAtIndex:indexPath.row]];
                   ispublic=[[[self.followDict objectAtIndex:indexPath.row ]objectForKey:@"ispublicleader"] intValue];
                
            
            }
                
            
            if(ispublic==0||ispublic==1)
            {
            NSString * clientId=[NSString stringWithFormat:@"&clientid=%@",delegate1.userID];
                 NSString * mobileNumber=[NSString stringWithFormat:@"&mobilenumber=%@",number];
            
            
            NSMutableData *postData = [[NSMutableData alloc] initWithData:[userId dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[clientId dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[mobileNumber dataUsingEncoding:NSUTF8StringEncoding]];
            //    if(segmentedControl.selectedSegmentIndex==0)
            //    {
            //         [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
            //    }
            //    else
            //    {
            //    if(self.buttonArray.count>0)
            //    {
            //    if([[self.buttonArray objectAtIndex:indexPath.row] isEqualToString:@"Follow +"])
            //    {
            //    [postData appendData:[@"&subscribe=true" dataUsingEncoding:NSUTF8StringEncoding]];
            //    }else if([[self.buttonArray objectAtIndex:indexPath.row] isEqualToString:@"Following"])
            //    {
            //        [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
            //
            //    }
            //    }
            //    }
            
            
            NSArray * subscribeArray=@[@1];
            
            NSString * subscribeString=[NSString stringWithFormat:@"&subscriptiontype=%@",subscribeArray];
            NSString * sub = [subscribeString stringByReplacingOccurrencesOfString:@"(" withString:@"{"];
            NSString * sub1 = [sub stringByReplacingOccurrencesOfString:@")" withString:@"}"];
            
            NSArray * unSubscribeArray=@[];
            
            NSString * unSubscribeString=[NSString stringWithFormat:@"&subscriptiontype=%@",unSubscribeArray];
            NSString * unSub = [unSubscribeString stringByReplacingOccurrencesOfString:@"(" withString:@"{"];
            NSString * unSub1 = [unSub stringByReplacingOccurrencesOfString:@")" withString:@"}"];
            
            
            if(sender.layer.borderWidth!=0)
            {
                
                
                [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
                [postData appendData:[unSub1 dataUsingEncoding:NSUTF8StringEncoding]];
            } else if(sender.layer.borderWidth==0)
            {
                [postData appendData:[@"&subscribe=true" dataUsingEncoding:NSUTF8StringEncoding]];
                [postData appendData:[sub1 dataUsingEncoding:NSUTF8StringEncoding]];
                
            }
            
     if(sender.layer.borderWidth==0)
     {
         
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower",delegate1.baseUrl]]
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:10.0];
            [request setHTTPMethod:@"POST"];
            [request setAllHTTPHeaderFields:headers];
            [request setHTTPBody:postData];
            
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            if(data !=nil)
                                                            {
                                                            if (error) {
                                                                //NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                //NSLog(@"%@", httpResponse);
                                                                if([httpResponse statusCode]==403)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                    
                                                                }
                                                                else if([httpResponse statusCode]==401)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    
                                                                NSMutableDictionary *followDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                
                                                                //NSLog(@"%@",followDict);
                                                                
                                                                messageStr=[followDict objectForKey:@"message"];
                                                                
                                                                }
                                                                
                                                            }
                                                            
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                
                                                                if([messageStr isEqualToString:@"Subscriber created"])
                                                                {
                                                                    
                                                                    
                                                                    //                                                            [cell.followBtn setTitle:[self.buttonArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
                                                                    
//                                                                    Mixpanel *mixpanel = [Mixpanel sharedInstance];
//                                                                    [mixpanel track:@"follow" properties:@{
//                                                                                                           @"leadername":leaderUserName
//                                                                                                           }];
                                                                    
                                                                    
                                                                    UIAlertController * alert = [UIAlertController
                                                                                                 alertControllerWithTitle:@""
                                                                                                 message:@"You have started following leader"
                                                                                                 preferredStyle:UIAlertControllerStyleAlert];
                                                                    
                                                                    //Add Buttons
                                                                    
                                                                    UIAlertAction* okButton = [UIAlertAction
                                                                                               actionWithTitle:@"Ok"
                                                                                               style:UIAlertActionStyleDefault
                                                                                               handler:^(UIAlertAction * action) {
                                                                                                   //Handle your yes please button action here
                                                                                                   
                                                                                                   
                                                                                                   
                                                                                               }];
                                                                    //Add your buttons to alert controller
                                                                    
                                                                    [alert addAction:okButton];
                                                                    
                                                                    
                                                                    [self presentViewController:alert animated:YES completion:nil];
                                                                    
                                                                    self.controller.searchBar.hidden=YES;
                                                                    self.controller.active=NO;
                                                                    
                                                                    [self segmentedControlChangedValue];
                                                                }
                                                            });
                                                            
                                                            }
                                                            else
                                                            {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                                    
                                                                    [self presentViewController:alert animated:YES completion:^{
                                                                        
                                                                    }];
                                                                    
                                                                    
                                                                  
                                                                    
                                                                    UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                        
                                                                    }];
                                                                    
                                                                    
                                                                   
                                                                    [alert addAction:cancel];
                                                                    
                                                                });
                                                                
                                                            }
                                                            
                                                        }];
            [dataTask resume];
         
     }
            
            else if(sender.layer.borderWidth!=0)
            {
                
                
                    dispatch_async(dispatch_get_main_queue(), ^{
                
                
                
                
                        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Are you sure you want to unfollow a leader." preferredStyle:UIAlertControllerStyleAlert];
                
                
                
                        [self presentViewController:alert animated:YES completion:^{
                
                
                
                        }];
                
                
                
                        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower",delegate1.baseUrl]]
                                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                               timeoutInterval:10.0];
                            [request setHTTPMethod:@"POST"];
                            [request setAllHTTPHeaderFields:headers];
                            [request setHTTPBody:postData];
                            
                            NSURLSession *session = [NSURLSession sharedSession];
                            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                            if(data!=nil)
                                                                            {
                                                                            
                                                                            if (error) {
                                                                                //NSLog(@"%@", error);
                                                                            } else {
                                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                                //NSLog(@"%@", httpResponse);
                                                                                if([httpResponse statusCode]==403)
                                                                                {
                                                                                    
                                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                                    
                                                                                }
                                                                                else if([httpResponse statusCode]==401)
                                                                                {
                                                                                    
                                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                                    
                                                                                }
                                                                                else
                                                                                {
                                                                                    
                                                                                NSMutableDictionary *followDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                                
                                                                                //NSLog(@"%@",followDict);
                                                                                
                                                                                messageStr=[followDict objectForKey:@"message"];
                                                                                
                                                                                }
                                                                                
                                                                            }
                                                                            
                                                                            
                                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                                
                                                                                
                                                                                if([messageStr isEqualToString:@"Subscriber created"])
                                                                                {
                                                                                    
                                                                                    
                                                                             //       NSString * leaderName = [NSString stringWithFormat:@"%@",[To be written]];
                                                                                    
                                                                                                                                                       //                                                            [cell.followBtn setTitle:[self.buttonArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
                                                                                    
                                                                                    self.controller.searchBar.hidden=YES;
                                                                                    self.controller.active=NO;
                                                                                    [self segmentedControlChangedValue];
                                                                                }
                                                                            });
                                                                            
                                                                            }
                                                                            else
                                                                            {
                                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                                                    
                                                                                    [self presentViewController:alert animated:YES completion:^{
                                                                                        
                                                                                    }];
                                                                                    
                                                                                 
                                                                                    
                                                                                    UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                                        
                                                                                    }];
                                                                                    
                                                                                    
                                                                                    
                                                                                    [alert addAction:cancel];
                                                                                    
                                                                                });
                                                                                
                                                                            }
                                                                            
                                                                        }];
                            [dataTask resume];

                
                        }];
                        
                        UIAlertAction * noAction=[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            
                            
                            
                        }];

                        
                        
                        
                        [alert addAction:okAction];
                        [alert addAction:noAction];
                        
                    });

                
            }
            
            
        }
//            else if(ispublic==1)
//            {
//                    dispatch_async(dispatch_get_main_queue(), ^{
//
//
//
//
//                        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:@"You cannot unfollow Public Experts" preferredStyle:UIAlertControllerStyleAlert];
//
//
//
//                        [self presentViewController:alert animated:YES completion:^{
//
//
//
//                        }];
//
//
//
//                        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//
//
//                        }];
//
//
//
//                        [alert addAction:okAction];
//
//                    });
//            }
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
        

    }
    
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

    
    
   }



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
   // ProfileViewcontroller * profile=[self.storyboard instantiateViewControllerWithIdentifier:@"profileView"];
    
    NewProfile * profile1 = [self.storyboard instantiateViewControllerWithIdentifier:@"NewProfile"];
    
   
        
        if(self.controller.active==YES&&self.controller.searchBar.text.length>=1)
        {
    
    @try {
        if(delegate1.leaderDetailArray.count>0)
        {
            [delegate1.leaderDetailArray removeAllObjects];
            
        }
        
        
            NSString * local = [newFollowArray objectAtIndex:indexPath.row];
            //        [delegate1.leaderDetailArray addObject:local];
            
            delegate1.leaderIDWhoToFollow = [[newFollowArray objectAtIndex:indexPath.row] objectForKey:@"userid"];
            
            //NSLog(@"Leader ID:%@",delegate1.leaderIDWhoToFollow);
        
        
        
        
        [self.navigationController pushViewController:profile1 animated:YES];
        
        //    dispatch_async(dispatch_get_main_queue(), ^{
        //
        //
        //
        //
        //        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Under Development." preferredStyle:UIAlertControllerStyleAlert];
        //
        //
        //
        //        [self presentViewController:alert animated:YES completion:^{
        //
        //
        //
        //        }];
        //
        //
        //
        //        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //            
        //            
        //            
        //        }];
        //        
        //        
        //        
        //        [alert addAction:okAction];
        //        
        //    });
        

        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
            
        }
        
        else
        {
            @try {
                if(delegate1.leaderDetailArray.count>0)
                {
                    [delegate1.leaderDetailArray removeAllObjects];
                    
                }
                
               
                    NSString * local = [self.followDict objectAtIndex:indexPath.row];
                    //        [delegate1.leaderDetailArray addObject:local];
                    
                    delegate1.leaderIDWhoToFollow = [[self.followDict objectAtIndex:indexPath.row] objectForKey:@"userid"];
                    
                    //NSLog(@"Leader ID:%@",delegate1.leaderIDWhoToFollow);
                
                
                
                
                [self.navigationController pushViewController:profile1 animated:YES];
                
                //    dispatch_async(dispatch_get_main_queue(), ^{
                //
                //
                //
                //
                //        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Under Development." preferredStyle:UIAlertControllerStyleAlert];
                //
                //
                //
                //        [self presentViewController:alert animated:YES completion:^{
                //
                //
                //
                //        }];
                //
                //
                //
                //        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                //            
                //            
                //            
                //        }];
                //        
                //        
                //        
                //        [alert addAction:okAction];
                //        
                //    });
                
                
                
            }
            @catch (NSException * e) {
                //NSLog(@"Exception: %@", e);
            }
            @finally {
                //NSLog(@"finally");
            }
        }
        
    
    
   
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

    }

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
//        if(newFollowArray.count>0||self.followDict.count>0)
//        {
//           
//            return 111;
//            
//        }
//        
//        else
//        {
//            return 393;
        //}
        
        
        if(self.controller.isActive==YES)
        {
            return 127;
        }
        
        else  if(self.followDict.count>0&&self.controller.isActive==NO)
        {
           return 127;
        }
        
        else
        {
             return 393;
            
        }

   
    
    
    
   
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*!
 @discussion search controller become active.
 */



-(IBAction)searchAction:(id)sender
{
  
    
    self.controller = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.controller.searchResultsUpdater = self;
    self.controller.hidesNavigationBarDuringPresentation = NO;
    self.controller.dimsBackgroundDuringPresentation = false;
    self.definesPresentationContext = YES;
    //    self.controller.searchBar.scopeButtonTitles = @[@"Posts", @"Users", @"Subreddits"];
    
   
    

   
    [self presentViewController:self.controller animated:YES
                     completion:nil];
    
    
}

/*!
 @discussion update the list according to user search .
 */
-(void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    @try
    {
//    if(segmentedControl.selectedSegmentIndex==0)
//    {
        
        @try {
            if(newFollowArray.count>0)
            {
                [newFollowArray removeAllObjects];
                
            }
            if(firstName.count>0)
            {
                [firstName removeAllObjects];
            }
            
            if(self.followDict.count>0)
            {
            for(int i=0;i<self.followDict.count;i++)
            {
                
                [firstName addObject:[[self.followDict objectAtIndex:i]objectForKey:@"firstname"]];
            }
                
            
            NSPredicate * predicate=[NSPredicate predicateWithFormat:@"self beginswith[c] %@",searchController.searchBar.text];
            //NSLog(@"%@",predicate);
            if(self.controller.searchBar.text.length>0)
            {
                filterArray =[[NSArray alloc]init];
                
                
                filterArray=[firstName  filteredArrayUsingPredicate:predicate];
                
                

                
                for(int i=0;i<filterArray.count;i++)
                {
                    detailsArray=[[NSMutableArray alloc]init];
                    
                    if(newFollowArray.count>0&&filterArray.count>0)
                    {
                        
                        
                        filterString1=[filterArray objectAtIndex:i];
                        for(int k=0;k<newFollowArray.count;k++)
                        {
                            [detailsArray addObject:[[newFollowArray objectAtIndex:k]objectForKey:@"firstname"]];
                            
                        }
                        
                    }

                    for(int j=0;j<self.followDict.count;j++)
                    {
                        NSString * filterString=[filterArray objectAtIndex:i];
                        NSString * responseString=[[self.followDict  objectAtIndex:j]objectForKey:@"firstname"];
                
                        
                        if(detailsArray.count>0)
                        {
                            
                            if([detailsArray containsObject:filterString1])
                            {
                                
                                
                            }
                            
                            
                            else
                            {
                                if([filterString isEqualToString:responseString])
                                {
                                    [newFollowArray addObject:[self.followDict objectAtIndex:j]];
                                }
                            }
                            
                            
                        }
                        
                        else
                        {
                            if([filterString isEqualToString:responseString])
                            {
                                [newFollowArray addObject:[self.followDict objectAtIndex:j]];
                            }
                        }

                    ///
                        
                    }
                }
            }
           
                
                if(self.controller.searchBar.text.length>=1)
                {

                    [self.followerTableView reloadData];
                }
//
                if(self.controller.isActive==NO)
                {
                     [self.followerTableView reloadData];
                }
                
                
               
                

                
            }
            
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
        
        
//
//    }
//    else if(segmentedControl.selectedSegmentIndex==1)
//    {
//        @try {
//            if(newFollowArray.count>0)
//            {
//                [newFollowArray removeAllObjects];
//
//            }
//            if(firstName.count>0)
//            {
//                [firstName removeAllObjects];
//            }
//            for(int i=0;i<[[monksDataDict objectForKey:@"results"]count];i++)
//            {
//
//                [firstName addObject:[[[monksDataDict objectForKey:@"results" ] objectAtIndex:i]objectForKey:@"firstname"]];
//            }
//            NSPredicate * predicate=[NSPredicate predicateWithFormat:@"self beginswith[c] %@",searchController.searchBar.text];
//            //NSLog(@"%@",predicate);
//            if(self.controller.searchBar.text.length>0)
//            {
//                filterArray =[[NSArray alloc]init];
//
//
//                filterArray=[firstName  filteredArrayUsingPredicate:predicate];
//
//
//
//
//                for(int i=0;i<filterArray.count;i++)
//                {
//                    detailsArray=[[NSMutableArray alloc]init];
//
//                    if(newFollowArray.count>0&&filterArray.count>0)
//                    {
//
//                        filterString1=[filterArray objectAtIndex:i];
//                        for(int k=0;k<newFollowArray.count;k++)
//                        {
//                            [detailsArray addObject:[[newFollowArray objectAtIndex:k]objectForKey:@"firstname"]];
//
//                        }
//
//                    }
//
//                    for(int j=0;j<[[monksDataDict objectForKey:@"results"]count];j++)
//                    {
//                        NSString * filterString=[filterArray objectAtIndex:i];
//                        NSString * responseString=[[[monksDataDict objectForKey:@"results" ] objectAtIndex:j]objectForKey:@"firstname"];
//
//
//
//
//
//
//                        if(detailsArray.count>0)
//                        {
//
//                            if([detailsArray containsObject:filterString1])
//                            {
//
//
//                            }
//
//
//                            else
//                            {
//                                if([filterString isEqualToString:responseString])
//                                {
//                                    [newFollowArray addObject:[[monksDataDict objectForKey:@"results" ] objectAtIndex:j]];
//                                }
//                            }
//
//
//                        }
//
//                        else
//                        {
//                            if([filterString isEqualToString:responseString])
//                            {
//                                [newFollowArray addObject:[[monksDataDict objectForKey:@"results" ] objectAtIndex:j]];
//                            }
//                        }
//
//
//
//                    }
//                }
//            }
//            if(self.controller.searchBar.text.length>=1)
//            {
//
//                [self.followerTableView reloadData];
//            }
//
//            if(self.controller.isActive==NO)
//            {
//                [self.followerTableView reloadData];
//            }
//            ;
//
//        }
//        @catch (NSException * e) {
//            //NSLog(@"Exception: %@", e);
//        }
//        @finally {
//            //NSLog(@"finally");
//        }
//
//    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

}

/*!
 @discussion filter button tapped.
 */
-(IBAction)filterAction:(id)sender
{
   
    if (filterFlag == false)
    {

    
    

        
        filterFlag = true;
    }
    else
    {
        filterFlag = false;

    }
    
}

//REACHABILITY//

-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    
    
}

-(BOOL)networkStatus
{
    
    
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    
    if(netStatus==NotReachable)
    {
        
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    return YES;
    
    
}


-(void)viewDidDisappear:(BOOL)animated
{
    if(self.controller.isActive==YES)
    {
    [self dismissViewControllerAnimated:self.controller completion:nil];
    }
    
//    if (self.view.window == nil) //you should be sure what the view is removed from the window
//    {
//        self.view = nil;
//        //remove other temporary objects
//        //self.models = nil;
//        //[self.request cancel];
//    }
}

/*!
 @discussion used to hide hint screen image.
 */

-(void)hideImage
{
    
    [myImageView removeFromSuperview];
    
}

//new//



@end
