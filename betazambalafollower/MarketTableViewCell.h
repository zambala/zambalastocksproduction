//
//  MarketTableViewCell.h
//  testing
//
//  Created by zenwise technologies on 18/11/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarketTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *timeLbl;

@property (strong, nonatomic) IBOutlet UILabel *priceLbl;

@property (strong, nonatomic) IBOutlet UILabel *companyNameLbl;

@property (strong, nonatomic) IBOutlet UILabel *percentLbl;
@property (strong, nonatomic) IBOutlet UILabel *resultsTodayLabel;

@property (strong, nonatomic) IBOutlet UILabel *companyName;
@property (strong, nonatomic) IBOutlet UILabel *changePerLbl;
@property (strong, nonatomic) IBOutlet UILabel *exchangeLbl;
@property (strong, nonatomic) IBOutlet UILabel *segmentLbl;
@property (strong, nonatomic) IBOutlet UIButton *tradeBtn;

@end
