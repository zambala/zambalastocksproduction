//
//  PortfolioCell.m
//  testing
//
//  Created by zenwise technologies on 23/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "PortfolioCell.h"

@implementation PortfolioCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.squareoffBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.squareoffBtn.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.squareoffBtn.layer.shadowOpacity = 5.0f;
    self.squareoffBtn.layer.shadowRadius = 4.0f;
    self.squareoffBtn.layer.cornerRadius=4.1f;
    self.squareoffBtn.layer.masksToBounds = NO;
    self.transactionTypeLbl.layer.borderWidth = 1.0f;
    self.transactionTypeLbl.layer.cornerRadius = 2.0f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
