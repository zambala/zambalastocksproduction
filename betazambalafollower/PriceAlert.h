//
//  PriceAlert.h
//  PriceAlert
//
//  Created by zenwise mac 2 on 12/28/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PriceAlert : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *priceAltTbl;
- (IBAction)backToMainView:(id)sender;


@end
