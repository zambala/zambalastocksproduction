//
//  TradeOrders.h
//  testing
//
//  Created by zenwise technologies on 03/01/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TradeOrders : UIViewController
@property (strong, nonatomic) IBOutlet UIView *limitView;
@property (strong, nonatomic) IBOutlet UIView *stopLossView;

- (IBAction)onSubmitOrderTap:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *submitOrderButton;

@property (strong, nonatomic) IBOutlet UIButton *marketBtn;

@property (strong, nonatomic) IBOutlet UIButton *limitBtn;
@property (strong, nonatomic) IBOutlet UIButton *stopLossBtn;

@property (strong, nonatomic) IBOutlet UISegmentedControl *buySellSegment;
@end
