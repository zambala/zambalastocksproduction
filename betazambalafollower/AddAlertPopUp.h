//
//  AddAlertPopUp.h
//  PriceAlert
//
//  Created by zenwise mac 2 on 12/28/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AddAlertPopUp : UIViewController<UITextFieldDelegate>


@property (strong, nonatomic) IBOutlet UIButton *LTPRadioBtn, *BIDRadioBtn, *ASKBtn, *lessRadioBtn, *greaterRadioBtn;

@property (strong, nonatomic) IBOutlet UITextField *MsgTxtField;

@end
