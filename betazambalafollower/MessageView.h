//
//  MessageView.h
//  testing
//
//  Created by zenwise mac 2 on 11/23/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageView : UIViewController<UITableViewDelegate,UITableViewDataSource, UIActionSheetDelegate>

{
    UIActionSheet *actionSheet;
}

@property (weak, nonatomic) IBOutlet UITableView *notoficationstableView;

@property (weak, nonatomic) IBOutlet UITableView *brokerMesssagesTableView;

- (IBAction)showingMainView:(id)sender;

@property (strong, nonatomic) IBOutlet UITableView *messagesTableView;



@end
