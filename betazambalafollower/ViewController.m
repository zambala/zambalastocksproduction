//
//  ViewController.m
//  Payment
//
//  Created by zenwise mac 2 on 7/17/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "ViewController.h"
#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCrypto.h>
#import "AppDelegate.h"
#import "TabBar.h"
#import <Mixpanel/Mixpanel.h>
#import "PaymentStatusViewController.h"

@interface ViewController ()
{
    
    NSString * merchantID;
    NSString * merchantTxnNo;
    NSString * amount;
    NSString * currencyCode;
    NSString * customerEmailId;
    NSString * transactionType;
    NSString * txnDate;
    NSString * customerMobileNo;
    NSString* newStr;
    NSString * payType;
    NSString * paymentMode;
   
    
    NSString *cardNo;
    NSString *cardExpiry;
    NSString *cvv;
    NSString *nameOnCard;
    AppDelegate * delegate1;
    NSMutableString *HMAC;
    NSString * customerUPIAlias;
    NSString * returnUrl;
    NSString * aggregatorId;
    NSMutableString *randomString;
    NSDictionary * secureHashDict;
    NSString * secureHash;
    NSMutableURLRequest * request;
    NSMutableData * postData1;
    NSMutableData *phiPostData;
    NSDictionary *parameters;
    NSString * customerID;
    NSString * upstoxPaymentMessage;
    NSMutableDictionary * upstoxResponseDict;
    
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    // Do any additional setup after loading the view, typically from a nib.
    
    //SHA
    
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"payment_page"];
     [mixpanelMini track:@"payment_web_view_page"];
    
    self.redirectURL = [NSString stringWithFormat:@"%@phicommerce/app/get/ptgResponse",delegate1.baseUrl];

    
    if([self.payInCheck isEqualToString:@"payin"])
    {
        self.topView.hidden=NO;
        self.topViewLayoutConstraint.constant=88;
    }else
    {
        self.topView.hidden=YES;
        self.topViewLayoutConstraint.constant=0;
    }
    
    [self.backButton addTarget:self action:@selector(onBackButtonTap) forControlEvents:UIControlEventTouchUpInside];
    //NSLog(@"UserID:%@",delegate1.userID);
    customerID = delegate1.userID;
    
    if([self.payInCheck isEqualToString:@"payin"])
    {
        if([delegate1.brokerNameStr isEqualToString:@"Upstox"])
        {
            [self upStoxPayment];
        }else
        {
            [self prodWebLoad];
           // [self webLoad];
        }
    }else
    {
        [self prodWebLoad];
        //[self webLoad];
    }
    
    
    
    
    
    //[self prodWebLoad];
}
-(void)viewDidAppear:(BOOL)animated
{
     self.webView.delegate=self;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paymentTryAgain) name:@"paymenttryagain" object:nil];
}
-(void)paymentTryAgain
{
    self.redirectURL = [NSString stringWithFormat:@"%@phicommerce/app/get/ptgResponse",delegate1.baseUrl];
    
    
    if([self.payInCheck isEqualToString:@"payin"])
    {
        self.topView.hidden=NO;
        self.topViewLayoutConstraint.constant=88;
    }else
    {
        self.topView.hidden=YES;
        self.topViewLayoutConstraint.constant=0;
    }
    
    [self.backButton addTarget:self action:@selector(onBackButtonTap) forControlEvents:UIControlEventTouchUpInside];
    //NSLog(@"UserID:%@",delegate1.userID);
    customerID = delegate1.userID;
    
    if([self.payInCheck isEqualToString:@"payin"])
    {
        if([delegate1.brokerNameStr isEqualToString:@"Upstox"])
        {
            [self upStoxPayment];
        }else
        {
            [self prodWebLoad];
            // [self webLoad];
        }
    }else
    {
        //[self prodWebLoad];
        [self webLoad];
    }
}

-(void)onBackButtonTap
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)upStoxPayment
{
//    NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate1.accessToken];
    @try
    {
     NSString * productType;
    
    if([self.segment isEqualToString:@"EQUITY"])
    {
        productType = @"Sec";
    }else
    {
        productType = @"Com";
    }
    
    
    NSDictionary *headers = @{
                               @"Content-Type": @"application/json",
                               @"Cache-Control": @"no-cache",
                                @"deviceid":delegate1.currentDeviceId
                                };
    NSDictionary *parameters1 = @{ @"amount":self.txnAmount,
                                   @"bank_account":delegate1.upstoxUserBankAccount,
                                   @"bank_name": delegate1.upstoxUserBankName,
                                   @"product": productType,
                                   @"access_token":delegate1.accessToken
                                   };
    
 
   
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters1 options:0 error:nil];
    
        NSString * str=[NSString stringWithFormat:@"%@upstox/payin",delegate1.baseUrl];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:str]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:30.0];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(data !=nil)
                                                        {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            if([httpResponse statusCode]==403)
                                                            {
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                
                                                            }
                                                            else if([httpResponse statusCode]==401)
                                                            {
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                
                                                            }
                                                            else if([httpResponse statusCode]==400)
                                                            {
                                                                 upstoxResponseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                               
                                                                
                                                            }
                                                            else
                                                            {
                                                                
                                                            upstoxResponseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            //NSLog(@"Response:%@",upstoxResponseDict);
                                                            
                                                            self.upStoxRedirectURL = [NSString stringWithFormat:@"%@",[[upstoxResponseDict objectForKey:@"data"]objectForKey:@"url"]];
                                                                upstoxPaymentMessage = [NSString stringWithFormat:@"%@",[upstoxResponseDict objectForKey:@"code"]];
                                                            }
                                                           
                                                        }
                                                        
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                          if([upstoxPaymentMessage containsString:@"200"])
                                                          {
                                                            NSString *escapedUrlString = [self.upStoxRedirectURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                                                            // escapedUrlString should be "http://123.237.186.221:8080/upload/textRequest.jsp?endTimestamp=2010-10-08%2016:20:47.0"
                                                            NSURL *aUrl = [NSURL URLWithString:escapedUrlString];
                                                             NSURLRequest * upstoxRequest = [NSURLRequest requestWithURL:aUrl];
                                                            [_webView loadRequest:upstoxRequest];
                                                          }
                                                            else
                                                            {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:[upstoxResponseDict objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
                                                                    
                                                                    [self presentViewController:alert animated:YES completion:^{
                                                                        
                                                                    }];
                                                                    
                                                                    
                                                                  
                                                                    
                                                                    UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                        [self.navigationController popViewControllerAnimated:YES];
                                                                    }];
                                                                    
                                                                    
                                                                    
                                                                    [alert addAction:cancel];
                                                                    
                                                                });
                                                            }
                                                            
                                                        });
                                                        }
                                                        else
                                                        {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                                
                                                                [self presentViewController:alert animated:YES completion:^{
                                                                    
                                                                }];
                                                                
                                                                
                                                                UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                    [self upStoxPayment];
                                                                }];
                                                                
                                                                UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                    
                                                                }];
                                                                
                                                                
                                                                [alert addAction:retryAction];
                                                                [alert addAction:cancel];
                                                                
                                                            });
                                                            
                                                        }
                                                    }];
        [dataTask resume];
   
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

        
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backAction:(id)sender {
    [self prodWebLoad];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView;
{
    //self.url = self.webView.request.mainDocumentURL;
    
//    https://qa.phicommerce.com/pg/api/pgresponse
//    https://qa.phicommerce.com/pg/api/merchant
    
    self.myString=[[NSString alloc]init];
    
    self.myString = self.webView.request.URL.absoluteString;
    
    NSError *error;
    if([delegate1.brokerNameStr isEqualToString:@"Zerodha"])
    {
        
    }else if([merchantID isEqualToString:@"P_00065"])
    {
   // if([self.myString containsString:@"Transaction+successful"]||[self.myString containsString:@"responseCode=0000"])
        if([self.myString containsString:@"responseCode"])
    {
        self.redirectString = [NSString stringWithFormat:@"%@",self.myString];
       // [self paymentDetails];
        PaymentStatusViewController * payment = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentStatusViewController"];
         payment.payinCheck=self.payInCheck;
        payment.txnNo=merchantTxnNo;
        [self.navigationController pushViewController:payment animated:YES];
    }else if ([delegate1.paymentString isEqualToString:@"UPI"])
    {
        PaymentStatusViewController * payment = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentStatusViewController"];
         payment.payinCheck=self.payInCheck;
        payment.txnNo=merchantTxnNo;
        [self.navigationController pushViewController:payment animated:YES];
    }
    }
    else if ([delegate1.brokerNameStr isEqualToString:@"Upstox"])
    {
        PaymentStatusViewController * payment = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentStatusViewController"];
        payment.payinCheck=self.payInCheck;
        payment.txnNo=merchantTxnNo;
        [self.navigationController pushViewController:payment animated:YES];
    }
    else
    {
        // if([self.myString containsString:@"Transaction+successful"]||[self.myString containsString:@"responseCode=0000"])
        if([self.myString containsString:@"responseCode"])
        {
            self.redirectString = [NSString stringWithFormat:@"%@",self.myString];
            // [self paymentDetails];
            PaymentStatusViewController * payment = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentStatusViewController"];
            payment.payinCheck=self.payInCheck;
            payment.txnNo=merchantTxnNo;
            [self.navigationController pushViewController:payment animated:YES];
        }else if ([delegate1.paymentString isEqualToString:@"UPI"])
        {
            PaymentStatusViewController * payment = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentStatusViewController"];
            payment.payinCheck=self.payInCheck;
            payment.txnNo=merchantTxnNo;
            [self.navigationController pushViewController:payment animated:YES];
        }
    }
   // http://52.66.159.178:8013/api/phicommerce/app/get/ptgResponse?secureHash=16e691d6a31bb46c9d71aecafedfa29bc3af4ff35832772d854debde43ca34c6&respDescription=Transaction+successful&merchantId=T_00029&addlParam2=EQUITY&paymentID=100000576008&merchantTxnNo=RPApR4&aggregatorID=J_00002&addlParam1=NB&paymentDateTime=20171228120352&txnID=T000715466683&responseCode=0000
    
    
    if([self.myString isEqualToString:@"https://qa.phicommerce.com/pg/api/merchant"])
    {
        NSURL * url=[NSURL URLWithString:self.myString];
        NSString *content = [NSString stringWithContentsOfURL:url
                                                        encoding:NSASCIIStringEncoding
                                                           error:&error];
        
        NSString *html = [self.webView stringByEvaluatingJavaScriptFromString:
                          @"document.body.innerHTML"];
        
        
//        Mixpanel *mixpanel = [Mixpanel sharedInstance];
//        [mixpanel track:@"Cart Checked Out"
//             properties:@{ @"Order Status":@"Success"}];
//
//
//
//        [mixpanel identify:delegate1.userID];
//
//        [mixpanel.people set:@{@"Premium Leaders":delegate1.premiumLeaders}];
//
        
        
        TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
        
        [self presentViewController:tabPage animated:YES completion:nil];
    }
    
}

-(void)webLoad
{
    self.webView.delegate=self;
    
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    
    int len=6;
    
    randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
    }
    
    
    
    merchantID=@"T_00029";
    merchantTxnNo=randomString;
    amount=delegate1.amountAfterRedeem;
    currencyCode=@"356";
    customerEmailId=delegate1.emailId;
    transactionType=@"SALE";
    txnDate=@"20170717101010";
    customerMobileNo=delegate1.mobileNumber;
    payType=@"0";
    
    cardNo=@"4111111111111111";
    cardExpiry=@"202207";
    cvv=@"123";
    nameOnCard=@"test";
    customerUPIAlias=delegate1.UPIDetails;
    returnUrl=[NSString stringWithFormat:@"%@",delegate1.baseUrl];
    aggregatorId=@"J_00002";
    
    if([self.payInCheck isEqualToString:@"payin"])
    {
        if([self.segment isEqualToString:@"EQUITY"])
        {
            merchantID=@"T_00030";
        }else if ([self.segment isEqualToString:@"DERIVATIVES"])
        {
            merchantID=@"T_00030";
        }else if ([self.segment isEqualToString:@"COMMODITIES"])
        {
            merchantID=@"T_00030";
        }
    }else
    {
        merchantID=@"T_00030";
    }
    merchantTxnNo=randomString;
    amount=delegate1.amountAfterRedeem;
    currencyCode=@"356";
    customerEmailId=delegate1.emailId;
    transactionType=@"SALE";
    txnDate=@"20170717101010";
    customerMobileNo=delegate1.mobileNumber;
    payType=@"0";
    
    cardNo=@"4111111111111111";
    cardExpiry=@"202207";
    cvv=@"123";
    nameOnCard=@"test";
    customerUPIAlias=delegate1.UPIDetails;
    returnUrl=[NSString stringWithFormat:@"%@",delegate1.baseUrl];
    aggregatorId=@"J_00002";
    if([delegate1.paymentString isEqualToString:@"CARD"])
    {
        paymentMode=@"CARD";
        if([self.payInCheck isEqualToString:@"payin"])
        {
            parameters=@{@"accountIFSC":self.accountIFSC,
                         @"accountNo":self.accountNo,
                         @"addlParam1":paymentMode,
                         @"addlParam2":self.segment,
                         @"aggregatorID":aggregatorId,
                         @"amount":self.txnAmount,
                         @"currencyCode":@356,
                         @"customerEmailID":customerEmailId,
                         @"customerID":customerID,
                         @"customerMobileNo":customerMobileNo,
                         @"merchantID": merchantID,
                         @"merchantTxnNo":randomString,
                         @"payType": @"0",
                         @"paymentMode":paymentMode,
                         @"returnURL": self.redirectURL,
                         @"transactionType": @"SALE",
                         @"txnDate": @"20111108011101",
                         @"bankName":self.bankAccount
                         };
        }else{
            
            
            parameters = @{@"addlParam1":paymentMode,
                           @"aggregatorId":aggregatorId,
                           @"amount":amount,
                           @"currencyCode": @356,
                           @"customerEmailId":customerEmailId,
                           @"customerMobileNo":customerMobileNo,
                           @"merchantID": merchantID,
                           @"merchantTxnNo":randomString,
                           @"payType": @"0",
                           @"paymentMode":paymentMode,
                           @"returnUrl":self.redirectURL,
                           @"transactionType": @"SALE",
                           @"txnDate": @"20111108011101",
                           @"subscriptions":delegate1.cartDict,
                           @"customerID":delegate1.userID
                           };
        }
        [self serverHit];
    }
    
    else if([delegate1.paymentString isEqualToString:@"UPI"]){
        paymentMode=@"UPI";
        if([self.payInCheck isEqualToString:@"payin"])
        {
            parameters=@{@"accountIFSC":self.accountIFSC,
                         @"accountNo":self.accountNo,
                         @"addlParam1":paymentMode,
                         @"addlParam2":self.segment,
                         @"aggregatorID":aggregatorId,
                         @"amount":self.txnAmount,
                         @"currencyCode":@356,
                         @"customerEmailID":customerEmailId,
                         @"customerID":customerID,
                         @"customerMobileNo":customerMobileNo,
                         @"customerUPIAlias":self.upiAlias,
                         @"merchantID": merchantID,
                         @"merchantTxnNo":randomString,
                         @"payType": @"0",
                         @"paymentMode":paymentMode,
                         @"returnURL": self.redirectURL,
                         @"transactionType": @"SALE",
                         @"txnDate": @"20111108011101",
                         @"bankName":self.bankAccount
                         };
        }else{
            
            parameters = @{@"addlParam1":paymentMode,
                           @"aggregatorId":aggregatorId,
                           @"amount":amount,
                           @"currencyCode": @356,
                           @"customerEmailId":customerEmailId,
                           @"customerMobileNo":customerMobileNo,
                           @"customerUPIAlias":delegate1.UPIDetails,
                           @"merchantID": merchantID,
                           @"merchantTxnNo":randomString,
                           @"payType": @"0",
                           @"paymentMode":paymentMode,
                           @"returnUrl":self.redirectURL,
                           @"transactionType": @"SALE",
                           @"txnDate": @"20111108011101",
                           @"subscriptions":delegate1.cartDict,
                           @"customerID":delegate1.userID
                           };
        }
        [self serverHit];
    }
    
    else if([delegate1.paymentString isEqualToString:@"NET"]){
        paymentMode=@"NB";
        if([self.payInCheck isEqualToString:@"payin"])
        {
            parameters=@{@"accountIFSC":self.accountIFSC,
                         @"accountNo":self.accountNo,
                         @"addlParam1":paymentMode,
                         @"addlParam2":self.segment,
                         @"aggregatorID":aggregatorId,
                         @"amount":self.txnAmount,
                         @"currencyCode":@356,
                         @"customerEmailID":customerEmailId,
                         @"customerID":customerID,
                         @"customerMobileNo":customerMobileNo,
                         @"merchantID": merchantID,
                         @"merchantTxnNo":randomString,
                         @"payType": @"0",
                         @"paymentMode":paymentMode,
                         @"returnURL": self.redirectURL,
                         @"transactionType": @"SALE",
                         @"txnDate": @"20111108011101",
                         @"bankName":self.bankAccount
                         };
        }else{
            parameters = @{@"addlParam1":paymentMode,
                           @"aggregatorId":aggregatorId,
                           @"amount":amount,
                           @"currencyCode": @356,
                           @"customerEmailId":customerEmailId,
                           @"customerMobileNo":customerMobileNo,
                           @"merchantID": merchantID,
                           @"merchantTxnNo":randomString,
                           @"payType": @"0",
                           @"paymentMode":paymentMode,
                           @"returnUrl":self.redirectURL,
                           @"transactionType": @"SALE",
                           @"txnDate": @"20111108011101",
                           @"subscriptions":delegate1.cartDict,
                           @"customerID":delegate1.userID
                           };
        }
        [self serverHit];
    }
}



-(void)prodWebLoad
{
    
   @try
    {

    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    
    int len=6;
    
    randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
    }
    
    if([self.payInCheck isEqualToString:@"payin"])
    {
        if([self.segment isEqualToString:@"EQUITY"])
        {
            merchantID=@"P_00067";
        }else if ([self.segment isEqualToString:@"DERIVATIVES"])
        {
            merchantID=@"P_00068";
        }else if ([self.segment isEqualToString:@"COMMODITIES"])
        {
            merchantID=@"P_00069";
        }
    }else
    {
    merchantID=@"P_00065";
    }
    merchantTxnNo=randomString;
    amount=delegate1.amountAfterRedeem;
    currencyCode=@"356";
    customerEmailId=delegate1.emailId;
    transactionType=@"SALE";
    txnDate=@"20170717101010";
    customerMobileNo=delegate1.mobileNumber;
    payType=@"0";
    
    cardNo=@"4111111111111111";
    cardExpiry=@"202207";
    cvv=@"123";
    nameOnCard=@"test";
    customerUPIAlias=delegate1.UPIDetails;
    returnUrl=[NSString stringWithFormat:@"%@",delegate1.baseUrl];
    aggregatorId=@"AM_00008";
    if([delegate1.paymentString isEqualToString:@"CARD"])
    {
         paymentMode=@"CARD";
        if([self.payInCheck isEqualToString:@"payin"])
        {
            parameters=@{@"accountIFSC":self.accountIFSC,
                         @"accountNo":self.accountNo,
                         @"addlParam1":paymentMode,
                         @"addlParam2":self.segment,
                         @"aggregatorID":aggregatorId,
                         @"amount":self.txnAmount,
                         @"currencyCode":@356,
                         @"customerEmailID":customerEmailId,
                         @"customerID":customerID,
                         @"customerMobileNo":customerMobileNo,
                         @"mobilenumber":customerMobileNo,
                         @"merchantID": merchantID,
                         @"merchantTxnNo":randomString,
                         @"payType": @"0",
                         @"paymentMode":paymentMode,
                         @"returnURL": self.redirectURL,
                         @"transactionType": @"SALE",
                         @"txnDate": @"20111108011101",
                         @"bankName":self.bankAccount
                         };
        }else{
       
        
        parameters = @{@"addlParam1":paymentMode,
                       @"aggregatorId":aggregatorId,
                       @"amount":amount,
                       @"currencyCode": @356,
                       @"customerEmailId":customerEmailId,
                       @"mobilenumber":customerMobileNo,
                       @"customerMobileNo":customerMobileNo,
                       @"merchantID": merchantID,
                       @"merchantTxnNo":randomString,
                       @"payType": @"0",
                       @"paymentMode":paymentMode,
                       @"returnUrl":self.redirectURL,
                       @"transactionType": @"SALE",
                       @"txnDate": @"20111108011101",
                       @"subscriptions":delegate1.cartDict,
                       @"customerID":delegate1.userID
                       };
        }
        [self prodServerHit];
    }
    
    else if([delegate1.paymentString isEqualToString:@"UPI"]){
        paymentMode=@"UPI";
        if([self.payInCheck isEqualToString:@"payin"])
        {
            parameters=@{@"accountIFSC":self.accountIFSC,
                         @"accountNo":self.accountNo,
                         @"addlParam1":paymentMode,
                         @"addlParam2":self.segment,
                         @"aggregatorID":aggregatorId,
                         @"amount":self.txnAmount,
                         @"currencyCode":@356,
                         @"customerEmailID":customerEmailId,
                         @"customerID":customerID,
                         @"mobilenumber":customerMobileNo,
                         @"customerMobileNo":customerMobileNo,
                         @"customerUPIAlias":self.upiAlias,
                         @"merchantID": merchantID,
                         @"merchantTxnNo":randomString,
                         @"payType": @"0",
                         @"paymentMode":paymentMode,
                         @"returnURL": self.redirectURL,
                         @"transactionType": @"SALE",
                         @"txnDate": @"20111108011101",
                         @"bankName":self.bankAccount
                         };
        }else{
        
        parameters = @{@"addlParam1":paymentMode,
                       @"aggregatorId":aggregatorId,
                       @"amount":amount,
                       @"currencyCode": @356,
                       @"customerEmailId":customerEmailId,
                       @"mobilenumber":customerMobileNo,
                       @"customerMobileNo":customerMobileNo,
                       @"customerUPIAlias":delegate1.UPIDetails,
                       @"merchantID": merchantID,
                       @"merchantTxnNo":randomString,
                       @"payType": @"0",
                       @"paymentMode":paymentMode,
                       @"returnUrl":self.redirectURL,
                       @"transactionType": @"SALE",
                       @"txnDate": @"20111108011101",
                       @"subscriptions":delegate1.cartDict,
                       @"customerID":delegate1.userID
                       };
        }
        [self prodServerHit];
    }
    
    else if([delegate1.paymentString isEqualToString:@"NET"]){
        paymentMode=@"NB";
        if([self.payInCheck isEqualToString:@"payin"])
        {
            parameters=@{@"accountIFSC":self.accountIFSC,
                         @"accountNo":self.accountNo,
                         @"addlParam1":paymentMode,
                         @"addlParam2":self.segment,
                         @"aggregatorID":aggregatorId,
                         @"amount":self.txnAmount,
                         @"currencyCode":@356,
                         @"customerEmailID":customerEmailId,
                         @"mobilenumber":customerMobileNo,
                         @"customerID":customerID,
                         @"customerMobileNo":customerMobileNo,
                         @"merchantID": merchantID,
                         @"merchantTxnNo":randomString,
                         @"payType": @"0",
                         @"paymentMode":paymentMode,
                         @"returnURL": self.redirectURL,
                         @"transactionType": @"SALE",
                         @"txnDate": @"20111108011101",
                          @"bankName":self.bankAccount
                         };
        }else{
        parameters = @{@"addlParam1":paymentMode,
                       @"aggregatorId":aggregatorId,
                       @"amount":amount,
                       @"currencyCode": @356,
                       @"customerEmailId":customerEmailId,
                       @"mobilenumber":customerMobileNo,
                       @"customerMobileNo":customerMobileNo,
                       @"merchantID": merchantID,
                       @"merchantTxnNo":randomString,
                       @"payType": @"0",
                       @"paymentMode":paymentMode,
                       @"returnUrl":self.redirectURL,
                       @"transactionType": @"SALE",
                       @"txnDate": @"20111108011101",
                       @"subscriptions":delegate1.cartDict,
                       @"customerID":delegate1.userID
                       };
        }
        [self prodServerHit];
    }
    }@catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
-(void)prodServerHit
{
    @try
    {
    NSString * urlStr;
    
    
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"deviceid":delegate1.currentDeviceId,
                               @"authtoken":delegate1.zenwiseToken
                               };
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    if([self.payInCheck isEqualToString:@"payin"])
    {
        urlStr=[NSString stringWithFormat:@"%@clienttrade/phicommerce/gethash/payin",delegate1.baseUrl];
      //  urlStr=[NSString stringWithFormat:@"%@clienttrade/phicommerce/gethash/payin",@"http://192.168.15.213:8013/api/"];
    }else
    {
       urlStr=[NSString stringWithFormat:@"%@clienttrade/phicommerce/gethash",delegate1.baseUrl];
       // urlStr=[NSString stringWithFormat:@"%@clienttrade/phicommerce/gethash",@"http://192.168.15.213:8013/api/"];
    }
    
    
    //NSString * urlStr=[NSString stringWithFormat:@"http://192.168.15.247:8013/api/clienttrade/phicommerce/gethash"];

    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if(data !=nil)
                                                    {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        if([httpResponse statusCode]==403)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                            
                                                        }
                                                        else if([httpResponse statusCode]==401)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                        secureHashDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        }
                                                        
                                                       //NSLog(@"Server hash:%@",secureHashDict);
                                                    }


                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        secureHash=[secureHashDict objectForKey:@"hash"];
                                                        
                                                        
                                                        if(secureHash.length>0)
                                                        {
                                                            NSString * accountIFSC = [NSString stringWithFormat:@"accountIFSC=%@",self.accountIFSC];
                                                            NSString * accountNo = [NSString stringWithFormat:@"&accountNo=%@",self.accountNo];
                                                            NSString * hash=[NSString stringWithFormat:@"&secureHash=%@",secureHash];
                                                            NSString * addParam=[NSString stringWithFormat:@"&addlParam1=%@",paymentMode];
                                                            NSString * addParam1 = [NSString stringWithFormat:@"&addlParam2=%@",self.segment];
                                                            NSString * merchantTx=[NSString stringWithFormat:@"&merchantTxnNo=%@",randomString];
                                                            NSString * amountStr=[NSString stringWithFormat:@"&amount=%@",amount];
                                                            NSString * aggregator=[NSString stringWithFormat:@"&aggregatorID=%@",aggregatorId];
                                                            
                                                            NSString * email=[NSString stringWithFormat:@"&customerEmailId=%@",customerEmailId];
                                                            
                                                            
                                                            NSString * mobile=[NSString stringWithFormat:@"&customerMobileNo=%@",customerMobileNo];
                                                            NSString * customerID = [NSString stringWithFormat:@"&customerID=%@",delegate1.userID];
                                                            NSString * VPA = [NSString stringWithFormat:@"&customerUPIAlias=%@",self.upiAlias];
                                                            NSString * paymentmode1=[NSString stringWithFormat:@"&paymentMode=%@",paymentMode];
                                                            NSString * merchantID1 = [NSString stringWithFormat:@"&merchantID=%@",merchantID];
                                                            NSString * txnAmount = [NSString stringWithFormat:@"&amount=%@",self.txnAmount];
                                                            if([self.payInCheck isEqualToString:@"payin"])
                                                            {
                                                                phiPostData = [[NSMutableData alloc]initWithData:[accountIFSC dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[accountNo dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[addParam dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[addParam1 dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[aggregator dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[txnAmount dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[@"&currencyCode=356" dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[email dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[customerID dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[mobile dataUsingEncoding:NSUTF8StringEncoding]];
                                                                if([delegate1.paymentString isEqualToString:@"UPI"])
                                                                {
                                                                    
                                                                    [phiPostData appendData:[VPA dataUsingEncoding:NSUTF8StringEncoding]];
                                                                }
                                                                [phiPostData appendData:[merchantID1 dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[merchantTx dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[@"&payType=0" dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[paymentmode1 dataUsingEncoding:NSUTF8StringEncoding]];
                                                                NSString * returnUrl1=[NSString stringWithFormat:@"&returnURL=%@",self.redirectURL];
                                                                [phiPostData appendData:[returnUrl1 dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[@"&transactionType=SALE" dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[@"&txnDate=20111108011101" dataUsingEncoding:NSUTF8StringEncoding]];
                                                                
                                                                
                                                                
                                                                
                                                                [phiPostData appendData:[hash dataUsingEncoding:NSUTF8StringEncoding]];
                                                                
                                                            }else
                                                            {
                                                                
                                                                NSString * addParam1 = [NSString stringWithFormat:@"addlParam1=%@",paymentMode];
                                                            phiPostData = [[NSMutableData alloc] initWithData:[addParam1 dataUsingEncoding:NSUTF8StringEncoding]];
                                                            
                                                            //  phiPostData = [[NSMutableData alloc] initWithData:[aggregator dataUsingEncoding:NSUTF8StringEncoding]];
                                                            [phiPostData appendData:[aggregator dataUsingEncoding:NSUTF8StringEncoding]];
                                                            
                                                            
                                                            [phiPostData appendData:[amountStr dataUsingEncoding:NSUTF8StringEncoding]];
                                                            
                                                            [phiPostData appendData:[@"&currencyCode=356" dataUsingEncoding:NSUTF8StringEncoding]];
                                                            
                                                            NSString * email=[NSString stringWithFormat:@"&customerEmailId=%@",customerEmailId];
                                                            [phiPostData appendData:[email dataUsingEncoding:NSUTF8StringEncoding]];
                                                            
                                                            
                                                            
                                                            NSString * mobile=[NSString stringWithFormat:@"&customerMobileNo=%@",customerMobileNo];
                                                            [phiPostData appendData:[mobile dataUsingEncoding:NSUTF8StringEncoding]];
                                                            if([delegate1.paymentString isEqualToString:@"UPI"])
                                                            {
                                                                NSString * VPA = [NSString stringWithFormat:@"&customerUPIAlias=%@",delegate1.UPIDetails];
                                                                [phiPostData appendData:[VPA dataUsingEncoding:NSUTF8StringEncoding]];
                                                            }
                                                            
                                                            [phiPostData appendData:[@"&merchantID=P_00065" dataUsingEncoding:NSUTF8StringEncoding]];
                                                            [phiPostData appendData:[merchantTx dataUsingEncoding:NSUTF8StringEncoding]];
                                                            
                                                            [phiPostData appendData:[@"&payType=0" dataUsingEncoding:NSUTF8StringEncoding]];
                                                            [phiPostData appendData:[paymentmode1 dataUsingEncoding:NSUTF8StringEncoding]];
                                                            
                                                            NSString * returnUrl1=[NSString stringWithFormat:@"&returnUrl=%@",self.redirectURL];
                                                            [phiPostData appendData:[returnUrl1 dataUsingEncoding:NSUTF8StringEncoding]];
                                                            
                                                            [phiPostData appendData:[@"&transactionType=SALE" dataUsingEncoding:NSUTF8StringEncoding]];
                                                            [phiPostData appendData:[@"&txnDate=20111108011101" dataUsingEncoding:NSUTF8StringEncoding]];
                                                            
                                                            [phiPostData appendData:[hash dataUsingEncoding:NSUTF8StringEncoding]];
                                                            }
                                                            NSString * str = [[NSString alloc]initWithData:phiPostData encoding:NSASCIIStringEncoding];
                                                            //NSLog(@"Final String%@",str);
                                                            [self prodPhiCommerceServer];
                                                            
                                                        }
                                                        
                                                        
                                                    });
                                                    }
                                                    else
                                                    {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            
                                                            UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                [self prodServerHit];
                                                            }];
                                                            
                                                            UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                
                                                            }];
                                                            
                                                            
                                                            [alert addAction:retryAction];
                                                            [alert addAction:cancel];
                                                            
                                                        });
                                                        
                                                    }
                                                }];
    [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

}

-(void)prodPhiCommerceServer
{
    self.webView.delegate=self;
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://secure-ptg.payphi.com/pg/api/sale"]];
        [request setHTTPMethod: @"POST"];
        [request setHTTPBody: phiPostData];
        [_webView loadRequest: request];
    
}

-(void)serverHit
{
    @try
    {
    NSString * urlStr;
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"deviceid":delegate1.currentDeviceId,
                               @"authtoken":delegate1.zenwiseToken
                               };
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    if([self.payInCheck isEqualToString:@"payin"])
    {
        urlStr=[NSString stringWithFormat:@"%@clienttrade/phicommerce/gethash/payin",delegate1.baseUrl];
       //   urlStr=[NSString stringWithFormat:@"%@clienttrade/phicommerce/gethash/payin",@"http://192.168.15.150:8013/api/"];
    }else
    {
        urlStr=[NSString stringWithFormat:@"%@clienttrade/phicommerce/gethash",delegate1.baseUrl];
        // urlStr=[NSString stringWithFormat:@"%@clienttrade/phicommerce/gethash",@"http://192.168.15.150:8013/api/"];
    }
    
    
    //NSString * urlStr=[NSString stringWithFormat:@"http://192.168.15.247:8013/api/clienttrade/phicommerce/gethash"];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if(data !=nil)
                                                    {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        secureHashDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        //NSLog(@"Server hash:%@",secureHashDict);
                                                    }
                                                    
                                                    
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        secureHash=[secureHashDict objectForKey:@"hash"];
                                                        
                                                        
                                                        if(secureHash.length>0)
                                                        {
                                                            NSString * accountIFSC = [NSString stringWithFormat:@"accountIFSC=%@",self.accountIFSC];
                                                            NSString * accountNo = [NSString stringWithFormat:@"&accountNo=%@",self.accountNo];
                                                            NSString * hash=[NSString stringWithFormat:@"&secureHash=%@",secureHash];
                                                            NSString * addParam=[NSString stringWithFormat:@"&addlParam1=%@",paymentMode];
                                                            NSString * addParam1 = [NSString stringWithFormat:@"&addlParam2=%@",self.segment];
                                                            NSString * merchantTx=[NSString stringWithFormat:@"&merchantTxnNo=%@",randomString];
                                                            NSString * amountStr=[NSString stringWithFormat:@"&amount=%@",amount];
                                                            NSString * aggregator=[NSString stringWithFormat:@"&aggregatorID=%@",aggregatorId];
                                                            
                                                            NSString * email=[NSString stringWithFormat:@"&customerEmailId=%@",customerEmailId];
                                                            
                                                            
                                                            NSString * mobile=[NSString stringWithFormat:@"&customerMobileNo=%@",customerMobileNo];
                                                            NSString * customerID = [NSString stringWithFormat:@"&customerID=%@",delegate1.userID];
                                                            NSString * VPA = [NSString stringWithFormat:@"&customerUPIAlias=%@",self.upiAlias];
                                                            NSString * paymentmode1=[NSString stringWithFormat:@"&paymentMode=%@",paymentMode];
                                                            NSString * merchantID1 = [NSString stringWithFormat:@"&merchantID=%@",merchantID];
                                                            NSString * txnAmount = [NSString stringWithFormat:@"&amount=%@",self.txnAmount];
                                                            if([self.payInCheck isEqualToString:@"payin"])
                                                            {
                                                                phiPostData = [[NSMutableData alloc]initWithData:[accountIFSC dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[accountNo dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[addParam dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[addParam1 dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[aggregator dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[txnAmount dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[@"&currencyCode=356" dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[email dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[customerID dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[mobile dataUsingEncoding:NSUTF8StringEncoding]];
                                                                if([delegate1.paymentString isEqualToString:@"UPI"])
                                                                {
                                                                    
                                                                    [phiPostData appendData:[VPA dataUsingEncoding:NSUTF8StringEncoding]];
                                                                }
                                                                [phiPostData appendData:[merchantID1 dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[merchantTx dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[@"&payType=0" dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[paymentmode1 dataUsingEncoding:NSUTF8StringEncoding]];
                                                                NSString * returnUrl1=[NSString stringWithFormat:@"&returnURL=%@",self.redirectURL];
                                                                [phiPostData appendData:[returnUrl1 dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[@"&transactionType=SALE" dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[@"&txnDate=20111108011101" dataUsingEncoding:NSUTF8StringEncoding]];
                                                                
                                                                
                                                                
                                                                
                                                                [phiPostData appendData:[hash dataUsingEncoding:NSUTF8StringEncoding]];
                                                                
                                                            }else
                                                            {
                                                                
                                                                NSString * addParam1 = [NSString stringWithFormat:@"addlParam1=%@",paymentMode];
                                                                phiPostData = [[NSMutableData alloc] initWithData:[addParam1 dataUsingEncoding:NSUTF8StringEncoding]];
                                                                
                                                                //  phiPostData = [[NSMutableData alloc] initWithData:[aggregator dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[aggregator dataUsingEncoding:NSUTF8StringEncoding]];
                                                                
                                                                
                                                                [phiPostData appendData:[amountStr dataUsingEncoding:NSUTF8StringEncoding]];
                                                                
                                                                [phiPostData appendData:[@"&currencyCode=356" dataUsingEncoding:NSUTF8StringEncoding]];
                                                                
                                                                NSString * email=[NSString stringWithFormat:@"&customerEmailId=%@",customerEmailId];
                                                                [phiPostData appendData:[email dataUsingEncoding:NSUTF8StringEncoding]];
                                                                
                                                                
                                                                
                                                                NSString * mobile=[NSString stringWithFormat:@"&customerMobileNo=%@",customerMobileNo];
                                                                [phiPostData appendData:[mobile dataUsingEncoding:NSUTF8StringEncoding]];
                                                                if([delegate1.paymentString isEqualToString:@"UPI"])
                                                                {
                                                                    NSString * VPA = [NSString stringWithFormat:@"&customerUPIAlias=%@",delegate1.UPIDetails];
                                                                    [phiPostData appendData:[VPA dataUsingEncoding:NSUTF8StringEncoding]];
                                                                }
                                                                
                                                                [phiPostData appendData:[@"&merchantID=T_00030" dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[merchantTx dataUsingEncoding:NSUTF8StringEncoding]];
                                                                
                                                                [phiPostData appendData:[@"&payType=0" dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[paymentmode1 dataUsingEncoding:NSUTF8StringEncoding]];
                                                                
                                                                NSString * returnUrl1=[NSString stringWithFormat:@"&returnUrl=%@",self.redirectURL];
                                                                [phiPostData appendData:[returnUrl1 dataUsingEncoding:NSUTF8StringEncoding]];
                                                                
                                                                [phiPostData appendData:[@"&transactionType=SALE" dataUsingEncoding:NSUTF8StringEncoding]];
                                                                [phiPostData appendData:[@"&txnDate=20111108011101" dataUsingEncoding:NSUTF8StringEncoding]];
                                                                
                                                                [phiPostData appendData:[hash dataUsingEncoding:NSUTF8StringEncoding]];
                                                            }
                                                            NSString * str = [[NSString alloc]initWithData:phiPostData encoding:NSASCIIStringEncoding];
                                                            //NSLog(@"Final String%@",str);
                                                            [self phiCommerceServer];
                                                            
                                                        }
                                                        
                                                        
                                                    });
                                                    }
                                                    else
                                                    {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            
                                                            UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                [self serverHit];
                                                            }];
                                                            
                                                            UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                
                                                            }];
                                                            
                                                            
                                                            [alert addAction:retryAction];
                                                            [alert addAction:cancel];
                                                            
                                                        });
                                                        
                                                    }
                                                }];
    [dataTask resume];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

}


-(void)paymentDetails
{
    @try
    {
    NSDictionary *headers = @{ @"Content-Type": @"application/x-www-form-urlencoded",
                               @"Cache-Control": @"no-cache",
                                };
    
    NSString * transactionNumber=[NSString stringWithFormat:@"&merchantTxnNo=%@",merchantTxnNo];
    
    NSMutableData *postData = [[NSMutableData alloc] initWithData:[transactionNumber dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"192.168.15.213:8013/api/clienttrade/phicommerce/paymentdetails"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if(data !=nil)
                                                    {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        self.paymentResponseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        //NSLog(@"Payment Details:%@",self.paymentResponseDictionary);
                                                        
                                                        
                                                        
                                                    }
                                                    }
                                                    else
                                                    {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            
                                                            UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                [self paymentDetails];
                                                            }];
                                                            
                                                            UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                
                                                            }];
                                                            
                                                            
                                                            [alert addAction:retryAction];
                                                            [alert addAction:cancel];
                                                            
                                                        });
                                                        
                                                    }
                                                }];
    [dataTask resume];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

}


-(void)balanceUpdate
{
    @try
    {
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                            };
    NSDictionary *parameters = @{ @"USERID":customerID,
                                  @"Segment": self.segment,
                                  @"BankName": self.bankAccount,
                                  @"Account": self.accountNo,
                                  @"TransacationId": @"RPApR4",
                                  @"Amount": self.txnAmount };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://103.69.91.237:3375/api/BalanceUpdate"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if(data !=nil)
                                                    {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        self.balanceUpdateResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        //NSLog(@"Balace Update Response:%@",self.balanceUpdateResponse);
                                                        
                                                        
                                                    }
                                                    }
                                                    else
                                                    {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            
                                                            UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                [self balanceUpdate];
                                                            }];
                                                            
                                                            UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                
                                                            }];
                                                            
                                                            
                                                            [alert addAction:retryAction];
                                                            [alert addAction:cancel];
                                                            
                                                        });
                                                        
                                                    }
                                                }];
    [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

}

-(void)phiCommerceServer
{
    request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://qa.phicommerce.com/pg/api/sale?v=2"]];
    [request setHTTPMethod: @"POST"];
    [request setHTTPBody: phiPostData];
    [_webView loadRequest: request];

}



@end
