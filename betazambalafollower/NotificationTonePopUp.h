//
//  NotificationTonePopUp.h
//  testing
//
//  Created by zenwise technologies on 22/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationTonePopUp : UIViewController
- (IBAction)cancelBtn:(id)sender;
- (IBAction)okBtn:(id)sender;

@end
