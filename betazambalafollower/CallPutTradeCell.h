//
//  OrderTableViewCell.h
//  ZambalaUSA
//
//  Created by guna on 20/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CallPutTradeCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UITextField *limitPriceTxtFld;

@property (strong, nonatomic) IBOutlet UILabel *transactionTypeLbl;
@property (strong, nonatomic) IBOutlet UILabel *companyLbl;
@property (strong, nonatomic) IBOutlet UITextField *quantityTxt;
@property (strong, nonatomic) IBOutlet UIButton *incrementBtn;
@property (strong, nonatomic) IBOutlet UIButton *decrementBtn;
@property (strong, nonatomic) IBOutlet UILabel *currencyImg;
@property (strong, nonatomic) IBOutlet UIButton *increment;
@property (strong, nonatomic) IBOutlet UIButton *decrement;

@end
