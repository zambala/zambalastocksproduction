//
//  KnowledgeTextOnlyViewController.m
//  KnowledgeSection
//
//  Created by Zenwise Technologies on 14/06/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "KnowledgeTextOnlyViewController.h"
#import "AppDelegate.h"

@interface KnowledgeTextOnlyViewController ()<UIWebViewDelegate>
{
    AppDelegate * delegate1;
}

@end

@implementation KnowledgeTextOnlyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.backButton addTarget:self action:@selector(onBackButtonTap) forControlEvents:UIControlEventTouchUpInside];
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.webView.delegate=self;
    NSString * shortStr=[NSString stringWithFormat:@"%@",[delegate1.resourceDetails objectForKey:@"shortdescription"]];
    if([shortStr isEqualToString:@"<null>"])
    {
        self.shortDescLbl.text=[NSString stringWithFormat:@"%@",[delegate1.resourceDetails objectForKey:@"heading"]];
    }
    else
    {
        self.shortDescLbl.text=shortStr;
    }
    
    NSString * htmlStr=[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[delegate1.resourceDetails objectForKey:@"longdescription"]]];
    
    [self.webView loadHTMLString:htmlStr baseURL:nil];

    // Do any additional setup after loading the view.
}

-(void)onBackButtonTap
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
