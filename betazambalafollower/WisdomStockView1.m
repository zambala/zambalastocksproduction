//
//  WisdomStockView1.m
//  testing
//
//  Created by zenwise technologies on 27/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "WisdomStockView1.h"
#import "HMSegmentedControl.h"

@interface WisdomStockView1 ()
{
    HMSegmentedControl * segmentedControl;
   
}
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *view2_Height;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *view1_Height;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *view3_Height;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *view4_Height;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *contentHeight;



@end

@implementation WisdomStockView1

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"DETAILS",@"NEWS"]];
    segmentedControl.frame = CGRectMake(0, 190, 375, 40);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.verticalDividerEnabled = YES;
    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(251/255.0) green:(196/255.0) blue:(12/255.0) alpha:1];
    
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
//    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
    [self.contentView addSubview:segmentedControl];
    self.view2_Height.constant = 0;
    self.view4_Height.constant = 0;
    _view2.hidden = true;
    _view4.hidden=true;
    [_view2 layoutIfNeeded];
    [_view4 layoutIfNeeded];
    self.tableView.hidden=YES;
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.contentHeight.constant=1400;
    
    self.titlesArray=[[NSMutableArray alloc]init];
    
    [self.bidbtn1 addTarget:self action:@selector(onClickingbidBtn1) forControlEvents:UIControlEventTouchUpInside];
    
    [self.bidBtn2 addTarget:self action:@selector(onClickingbidBtn2) forControlEvents:UIControlEventTouchUpInside];
    
    [self.bidBtn3 addTarget:self action:@selector(onClickingbidBtn3) forControlEvents:UIControlEventTouchUpInside];
    
    [self.bidBtn4 addTarget:self action:@selector(onClickingbidBtn4) forControlEvents:UIControlEventTouchUpInside];
    
    [self.bidBtn5 addTarget:self action:@selector(onClickingbidBtn5) forControlEvents:UIControlEventTouchUpInside];
    
    [self.bidBtn6 addTarget:self action:@selector(onClickingbidBtn6) forControlEvents:UIControlEventTouchUpInside];
    
    [self.bidBtn7 addTarget:self action:@selector(onClickingbidBtn7) forControlEvents:UIControlEventTouchUpInside];
    
    [self.bidBtn8 addTarget:self action:@selector(onClickingbidBtn8) forControlEvents:UIControlEventTouchUpInside];
    
    [self.bidBtn9 addTarget:self action:@selector(onClickingbidBtn9) forControlEvents:UIControlEventTouchUpInside];
    
    [self.bidBtn10 addTarget:self action:@selector(onClickingbidBtn10) forControlEvents:UIControlEventTouchUpInside];
    
    [self.askBtn1 addTarget:self action:@selector(onClickingaskBtn1) forControlEvents:UIControlEventTouchUpInside];
    
    [self.askBtn2 addTarget:self action:@selector(onClickingaskBtn2) forControlEvents:UIControlEventTouchUpInside];
    
    [self.askBtn3 addTarget:self action:@selector(onClickingaskBtn3) forControlEvents:UIControlEventTouchUpInside];
    
    [self.askBtn4 addTarget:self action:@selector(onClickingaskBtn4) forControlEvents:UIControlEventTouchUpInside];
    
    [self.askBtn5 addTarget:self action:@selector(onClickingaskBtn5) forControlEvents:UIControlEventTouchUpInside];
    
    [self.askBtn6 addTarget:self action:@selector(onClickingaskBtn6) forControlEvents:UIControlEventTouchUpInside];
    
    [self.askBtn7 addTarget:self action:@selector(onClickingaskBtn7) forControlEvents:UIControlEventTouchUpInside];
    
    [self.askBtn8 addTarget:self action:@selector(onClickingaskBtn8) forControlEvents:UIControlEventTouchUpInside];
    
    [self.askBtn9 addTarget:self action:@selector(onClickingaskBtn9) forControlEvents:UIControlEventTouchUpInside];
    
    [self.askBtn10 addTarget:self action:@selector(onClickingaskBtn10) forControlEvents:UIControlEventTouchUpInside];
    
    

    
  }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)segmentedControlChangedValue
{
    if(segmentedControl.selectedSegmentIndex==0)
    {
        self.tableView.hidden=YES;
        self.view1.hidden=NO;
        self.view2.hidden=NO;
        self.view3.hidden=NO;
        self.view4.hidden=NO;
        self.contentView.hidden=NO;
        self.scrollView.hidden=NO;
        
        
    }
    else{
        self.tableView.hidden=NO;
        self.contentView.hidden=YES;
        self.scrollView.hidden=YES;
        
        
    }
}





/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)dropDownBtn:(id)sender {
    
    if (_startStopButtonIsActive) {
        self.startStopButtonIsActive = !self.startStopButtonIsActive;  //toggle!
        
        self.contentHeight.constant=1200;
        
        [UIView animateWithDuration:2
                              delay:0.0
                            options:UIViewAnimationCurveEaseOut
                         animations:^(void) {
                             
                              self.imgView.image=[UIImage imageNamed:@"expandCard.png"];
                             
                             _view2_Height.constant = 0;
                             _view2.hidden = true;
                             self.contentHeight.constant=1400;
                             //  self.view2.hidden=YES;
                             
                             //                             self.headerView.frame=CGRectMake(0, 176, 375, 32);
                             //                            self.view3.frame=CGRectMake(0, 209, 375, 520);
                             
                             
                         }
                         completion:NULL];
        
        
        
        
        // do your stuff here
    } else {
        self.startStopButtonIsActive = !self.startStopButtonIsActive;  //toggle!
        // do your stuff here
        
        
        
        self.contentHeight.constant=1600;
        [UIView animateWithDuration:2
                              delay:0.0
                            options:UIViewAnimationCurveEaseOut
                         animations:^(void) {
                             
                             
                              self.imgView.image=[UIImage imageNamed:@"collapseCard.png"];
                             _view2_Height.constant = 185;
                             _view2.hidden = false;
                             self.contentHeight.constant=1200;
                             
                             //                             self.headerView.frame=CGRectMake(0, 345, 375, 32);
                             //                             self.view3.frame=CGRectMake(0, 377, 375, 520);
                             
                         }
                         completion:NULL];
        
        
    }
    
    
    
    
}
- (IBAction)dropDownBtn1:(id)sender {
    
    if (_startStopButtonIsActive1) {
        self.startStopButtonIsActive1 = !self.startStopButtonIsActive1;  //toggle!
        
        self.contentHeight.constant=1200;
        [UIView animateWithDuration:2
                              delay:0.0
                            options:UIViewAnimationCurveEaseOut
                         animations:^(void) {
                             
                             
                              self.imgView1.image=[UIImage imageNamed:@"expandCard.png"];
                             
                             
                             self.view4_Height.constant=0;
                             _view4.hidden = true;
                             self.contentHeight.constant=1400;
                             
                             
                             //  self.view2.hidden=YES;
                             
                             //                             self.headerView.frame=CGRectMake(0, 176, 375, 32);
                             //                            self.view3.frame=CGRectMake(0, 209, 375, 520);
                             
                             
                         }
                         completion:NULL];
        
        
        
        
        // do your stuff here
    } else {
        self.startStopButtonIsActive1 = !self.startStopButtonIsActive1;  //toggle!
        // do your stuff here
        
        self.contentHeight.constant=1800;
        [UIView animateWithDuration:2
                              delay:0.0
                            options:UIViewAnimationCurveEaseOut
                         animations:^(void) {
                             
                             
                              self.imgView1.image=[UIImage imageNamed:@"collapseCard.png"];
                             
                             self.view4.hidden=false;
                             
                             self.view4_Height.constant = 185;
                             self.contentHeight.constant=1600;
                             
                             
                             
                             
                             //                             self.headerView.frame=CGRectMake(0, 345, 375, 32);
                             //                             self.view3.frame=CGRectMake(0, 377, 375, 520);
                             
                         }
                         completion:NULL];
        
        
    }
    
    
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
    
    
    cell.textLabel.text=@"news";
    
    return cell;
    
}





- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if(scrollView.tag == 1000)
        return;
    
    
    float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (bottomEdge >= scrollView.contentSize.height) {
        // we are at the end
        
        
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options:UIViewAnimationCurveEaseOut
                         animations:^(void) {
                             //                            self.headerView.hidden=NO;
                             
                         }
                         completion:NULL];
        
    }
    else
    {
        
    }
    
    
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if(scrollView.tag == 1000)
        return;
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationCurveEaseOut
                     animations:^(void) {
                         
                     }
                     completion:NULL];
    
}



-(void)onClickingbidBtn1
{
    if (_startStopButtonIsActive2) {
        self.startStopButtonIsActive2 = !self.startStopButtonIsActive2;
        [self.titlesArray removeObjectAtIndex:0];
        self.bidbtn1.layer.borderWidth=0;
        [self.bidbtn1.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
        
        
        
        self.startStopButtonIsActive2 = !self.startStopButtonIsActive2;
        [self.bidbtn1.layer setBorderColor:[[UIColor orangeColor] CGColor]];
        NSString * title =[NSString stringWithFormat:@"%@",self.bidbtn1.titleLabel];
        self.bidbtn1.layer.borderWidth=4;
        
        [self.titlesArray addObject:title];
        
        
    }
}



-(void)onClickingbidBtn2
{
    if (_startStopButtonIsActive3) {
        self.startStopButtonIsActive3 = !self.startStopButtonIsActive3;
        [self.titlesArray removeObjectAtIndex:0];
        self.bidBtn2.layer.borderWidth=0;
        [self.bidBtn2.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
        
        
        
        self.startStopButtonIsActive3 = !self.startStopButtonIsActive3;
        [self.bidBtn2.layer setBorderColor:[[UIColor orangeColor] CGColor]];
        NSString * title =[NSString stringWithFormat:@"%@",self.bidBtn2.titleLabel];
        self.bidBtn2.layer.borderWidth=4;
        
        [self.titlesArray addObject:title];
        
        
    }
}


-(void)onClickingbidBtn3
{
    if (_startStopButtonIsActive4) {
        self.startStopButtonIsActive4 = !self.startStopButtonIsActive4;
        [self.titlesArray removeObjectAtIndex:0];
        self.bidBtn3.layer.borderWidth=0;
        [self.bidBtn3.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
        
        
        self.startStopButtonIsActive4 = !self.startStopButtonIsActive4;
        [self.bidBtn3.layer setBorderColor:[[UIColor orangeColor] CGColor]];
        NSString * title =[NSString stringWithFormat:@"%@",self.bidBtn3.titleLabel];
        self.bidBtn3.layer.borderWidth=4;
        
        [self.titlesArray addObject:title];
        
        
    }
}



-(void)onClickingbidBtn4
{
    if (_startStopButtonIsActive5) {
        self.startStopButtonIsActive5 = !self.startStopButtonIsActive5;
        [self.titlesArray removeObjectAtIndex:0];
        self.bidBtn4.layer.borderWidth=0;
        [self.bidBtn4.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
        
        
        self.startStopButtonIsActive5 = !self.startStopButtonIsActive5;
        [self.bidBtn4.layer setBorderColor:[[UIColor orangeColor] CGColor]];
        self.bidBtn4.layer.borderWidth=4;
        NSString * title =[NSString stringWithFormat:@"%@",self.bidBtn4.titleLabel];
        
        [self.titlesArray addObject:title];
        
        
    }
}





-(void)onClickingbidBtn5
{
    if (_startStopButtonIsActive6) {
        self.startStopButtonIsActive6 = !self.startStopButtonIsActive6;
        [self.titlesArray removeObjectAtIndex:0];
        self.bidBtn5.layer.borderWidth=0;
        [self.bidBtn5.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
        
        
        
        self.startStopButtonIsActive6 = !self.startStopButtonIsActive6;
        [self.bidBtn5.layer setBorderColor:[[UIColor orangeColor] CGColor]];
        self.bidBtn5.layer.borderWidth=4;
        NSString * title =[NSString stringWithFormat:@"%@",self.bidBtn5.titleLabel];
        
        [self.titlesArray addObject:title];
        
        
    }
}


-(void)onClickingbidBtn6
{
    if (_startStopButtonIsActive7) {
        self.startStopButtonIsActive7 = !self.startStopButtonIsActive7;
        [self.titlesArray removeObjectAtIndex:0];
        self.bidBtn6.layer.borderWidth=0;
        [self.bidBtn6.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
        
        
        
        self.startStopButtonIsActive7 = !self.startStopButtonIsActive7;
        [self.bidBtn6.layer setBorderColor:[[UIColor orangeColor] CGColor]];
        self.bidBtn6.layer.borderWidth=4;
        NSString * title =[NSString stringWithFormat:@"%@",self.bidBtn6.titleLabel];
        
        [self.titlesArray addObject:title];
        
        
    }
}



-(void)onClickingbidBtn7
{
    if (_startStopButtonIsActive8) {
        self.startStopButtonIsActive8 = !self.startStopButtonIsActive8;
        [self.titlesArray removeObjectAtIndex:0];
        self.bidBtn7.layer.borderWidth=0;
        [self.bidBtn7.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
        
        
        
        self.startStopButtonIsActive8 = !self.startStopButtonIsActive8;
        [self.bidBtn7.layer setBorderColor:[[UIColor orangeColor] CGColor]];
        self.bidBtn7.layer.borderWidth=4;
        NSString * title =[NSString stringWithFormat:@"%@",self.bidBtn7.titleLabel];
        
        [self.titlesArray addObject:title];
        
        
    }
}



-(void)onClickingbidBtn8
{
    if (_startStopButtonIsActive9) {
        self.startStopButtonIsActive9 = !self.startStopButtonIsActive9;
        [self.titlesArray removeObjectAtIndex:0];
        self.bidBtn8.layer.borderWidth=0;
        [self.bidBtn8.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
        
        
        self.startStopButtonIsActive9 = !self.startStopButtonIsActive9;
        [self.bidBtn8.layer setBorderColor:[[UIColor orangeColor] CGColor]];
        self.bidBtn8.layer.borderWidth=4;
        NSString * title =[NSString stringWithFormat:@"%@",self.bidBtn8.titleLabel];
        
        [self.titlesArray addObject:title];
        
        
    }
}



-(void)onClickingbidBtn9
{
    if (_startStopButtonIsActive10) {
        self.startStopButtonIsActive10 = !self.startStopButtonIsActive10;
        [self.titlesArray removeObjectAtIndex:0];
        self.bidBtn9.layer.borderWidth=0;
        [self.bidBtn9.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
        
        
        
        self.startStopButtonIsActive10 = !self.startStopButtonIsActive10;
        [self.bidBtn9.layer setBorderColor:[[UIColor orangeColor] CGColor]];
        self.bidBtn9.layer.borderWidth=4;
        NSString * title =[NSString stringWithFormat:@"%@",self.bidBtn9.titleLabel];
        
        [self.titlesArray addObject:title];
        
        
    }
}



-(void)onClickingbidBtn10
{
    if (_startStopButtonIsActive11) {
        self.startStopButtonIsActive11 = !self.startStopButtonIsActive11;
        [self.titlesArray removeObjectAtIndex:0];
        self.bidBtn10.layer.borderWidth=0;
        [self.bidBtn10.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )  {
        
        
        
        self.startStopButtonIsActive11 = !self.startStopButtonIsActive11;
        [self.bidBtn10.layer setBorderColor:[[UIColor orangeColor] CGColor]];
        self.bidBtn10.layer.borderWidth=4;
        NSString * title =[NSString stringWithFormat:@"%@",self.bidBtn10.titleLabel];
        
        [self.titlesArray addObject:title];
        
        
    }
}


-(void)onClickingaskBtn1
{
    if (_startStopButtonIsActive12) {
        self.startStopButtonIsActive12 = !self.startStopButtonIsActive12;
        [self.titlesArray removeObjectAtIndex:0];
        self.askBtn1.layer.borderWidth=0;
        [self.askBtn1.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
        
        
        self.startStopButtonIsActive12 = !self.startStopButtonIsActive12;
        [self.askBtn1.layer setBorderColor:[[UIColor orangeColor] CGColor]];
        self.askBtn1.layer.borderWidth=4;
        NSString * title =[NSString stringWithFormat:@"%@",self.askBtn1.titleLabel];
        
        [self.titlesArray addObject:title];
        
        
    }
}


-(void)onClickingaskBtn2
{
    if (_startStopButtonIsActive13) {
        self.startStopButtonIsActive13 = !self.startStopButtonIsActive13;
        [self.titlesArray removeObjectAtIndex:0];
        self.askBtn2.layer.borderWidth=0;
        [self.askBtn2.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
        
        
        self.startStopButtonIsActive13 = !self.startStopButtonIsActive13;
        [self.askBtn2.layer setBorderColor:[[UIColor orangeColor] CGColor]];
        self.askBtn2.layer.borderWidth=4;
        NSString * title =[NSString stringWithFormat:@"%@",self.askBtn2.titleLabel];
        
        [self.titlesArray addObject:title];
        
        
    }
}


-(void)onClickingaskBtn3
{
    if (_startStopButtonIsActive14) {
        self.startStopButtonIsActive14 = !self.startStopButtonIsActive14;
        [self.titlesArray removeObjectAtIndex:0];
        self.askBtn3.layer.borderWidth=0;
        [self.askBtn3.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    
    else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )   {
        
        
        self.startStopButtonIsActive14 = !self.startStopButtonIsActive14;
        [self.askBtn3.layer setBorderColor:[[UIColor orangeColor] CGColor]];
        self.askBtn3.layer.borderWidth=4;
        NSString * title =[NSString stringWithFormat:@"%@",self.askBtn3.titleLabel];
        
        [self.titlesArray addObject:title];
        
        
    }
}


-(void)onClickingaskBtn4
{
    if (_startStopButtonIsActive15) {
        self.startStopButtonIsActive15 = !self.startStopButtonIsActive15;
        [self.titlesArray removeObjectAtIndex:0];
        self.askBtn4.layer.borderWidth=0;
        [self.askBtn4.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
        
        
        self.startStopButtonIsActive15 = !self.startStopButtonIsActive15;
        [self.askBtn4.layer setBorderColor:[[UIColor orangeColor] CGColor]];
        self.askBtn4.layer.borderWidth=4;
        NSString * title =[NSString stringWithFormat:@"%@",self.askBtn4.titleLabel];
        
        [self.titlesArray addObject:title];
        
        
    }
}


-(void)onClickingaskBtn5
{
    if (_startStopButtonIsActive16) {
        self.startStopButtonIsActive16 = !self.startStopButtonIsActive16;
        [self.titlesArray removeObjectAtIndex:0];
        self.askBtn5.layer.borderWidth=0;
        [self.askBtn5.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )   {
        
        
        
        self.startStopButtonIsActive16 = !self.startStopButtonIsActive16;
        [self.askBtn5.layer setBorderColor:[[UIColor orangeColor] CGColor]];
        self.askBtn5.layer.borderWidth=4;
        NSString * title =[NSString stringWithFormat:@"%@",self.askBtn5.titleLabel];
        
        [self.titlesArray addObject:title];
        
        
    }
}


-(void)onClickingaskBtn6
{
    if (_startStopButtonIsActive17) {
        self.startStopButtonIsActive17 = !self.startStopButtonIsActive17;
        [self.titlesArray removeObjectAtIndex:0];
        self.askBtn6.layer.borderWidth=0;
        [self.askBtn6.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
        
        
        
        self.startStopButtonIsActive17 = !self.startStopButtonIsActive17;
        [self.askBtn6.layer setBorderColor:[[UIColor orangeColor] CGColor]];
        self.askBtn6.layer.borderWidth=4;
        NSString * title =[NSString stringWithFormat:@"%@",self.askBtn6.titleLabel];
        
        [self.titlesArray addObject:title];
        
        
    }
}


-(void)onClickingaskBtn7
{if (_startStopButtonIsActive18) {
    self.startStopButtonIsActive18 = !self.startStopButtonIsActive18;
    [self.titlesArray removeObjectAtIndex:0];
    self.askBtn7.layer.borderWidth=0;
    [self.askBtn7.layer setBorderColor:[[UIColor whiteColor] CGColor]];
}
else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
{
    
    
    self.startStopButtonIsActive18 = !self.startStopButtonIsActive18;
    [self.askBtn7.layer setBorderColor:[[UIColor orangeColor] CGColor]];
    self.askBtn7.layer.borderWidth=4;
    NSString * title =[NSString stringWithFormat:@"%@",self.askBtn7.titleLabel];
    
    [self.titlesArray addObject:title];
    
    
}
}


-(void)onClickingaskBtn8
{
    if (_startStopButtonIsActive19) {
        self.startStopButtonIsActive19 = !self.startStopButtonIsActive19;
        [self.titlesArray removeObjectAtIndex:0];
        self.askBtn8.layer.borderWidth=0;
        [self.askBtn8.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )    {
        
        
        self.startStopButtonIsActive19 = !self.startStopButtonIsActive19;
        [self.askBtn8.layer setBorderColor:[[UIColor orangeColor] CGColor]];
        self.askBtn8.layer.borderWidth=4;
        NSString * title =[NSString stringWithFormat:@"%@",self.askBtn8.titleLabel];
        
        [self.titlesArray addObject:title];
        
        
    }
}


-(void)onClickingaskBtn9
{
    if (_startStopButtonIsActive20) {
        self.startStopButtonIsActive20 = !self.startStopButtonIsActive20;
        [self.titlesArray removeObjectAtIndex:0];
        self.askBtn9.layer.borderWidth=0;
        [self.askBtn9.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )  {
        
        
        
        self.startStopButtonIsActive20 = !self.startStopButtonIsActive20;
        [self.askBtn9.layer setBorderColor:[[UIColor orangeColor] CGColor]];
        self.askBtn9.layer.borderWidth=4;
        NSString * title =[NSString stringWithFormat:@"%@",self.askBtn9.titleLabel];
        
        [self.titlesArray addObject:title];
        
        
    }
}


-(void)onClickingaskBtn10
{
    if (_startStopButtonIsActive21) {
        self.startStopButtonIsActive21 = !self.startStopButtonIsActive21;
        [self.titlesArray removeObjectAtIndex:0];
        self.askBtn10.layer.borderWidth=0;
        [self.askBtn10.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )   {
        
        
        
        self.startStopButtonIsActive21 = !self.startStopButtonIsActive21;
        [self.askBtn10.layer setBorderColor:[[UIColor orangeColor] CGColor]];
        self.askBtn10.layer.borderWidth=4;
        NSString * title =[NSString stringWithFormat:@"%@",self.askBtn10.titleLabel];
        
        [self.titlesArray addObject:title];
        
        
    }
}













@end
