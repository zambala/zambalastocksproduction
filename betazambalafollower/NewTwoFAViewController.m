//
//  NewTwoFAViewController.m
//  NewUI
//
//  Created by Zenwise Technologies on 23/05/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "NewTwoFAViewController.h"
#import "AppDelegate.h"
#import "TagEncode.h"
#import "TabBar.h"
#import <Mixpanel/Mixpanel.h>

@interface NewTwoFAViewController ()
{
    AppDelegate * delegate1;
    NSMutableArray * answersArray;
    NSDictionary * loginReplyDict;
    NSMutableArray * inputArray;

}

@end

@implementation NewTwoFAViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"login_two_fa_page"];
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    inputArray=[[NSMutableArray alloc]init];
    self.quesOneTF.layer.cornerRadius=5.0f;
    self.quesOneTF.layer.borderWidth = 1.0f;
    self.quesOneTF.layer.borderColor = [[UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0] CGColor];
    self.activityInd.hidden=YES;
    [self.activityInd stopAnimating];
    self.quesTwoTF.layer.cornerRadius=5.0f;
    self.quesTwoTF.layer.borderWidth = 1.0f;
    self.quesTwoTF.layer.borderColor = [[UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0] CGColor];
    
    self.quesThreeTF.layer.cornerRadius=5.0f;
    self.quesThreeTF.layer.borderWidth = 1.0f;
    self.quesThreeTF.layer.borderColor = [[UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0] CGColor];
    
    self.quesFourTF.layer.cornerRadius=5.0f;
    self.quesFourTF.layer.borderWidth = 1.0f;
    self.quesFourTF.layer.borderColor = [[UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0] CGColor];
    
    self.quesFiveTF.layer.cornerRadius=5.0f;
    self.quesFiveTF.layer.borderWidth = 1.0f;
    self.quesFiveTF.layer.borderColor = [[UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0] CGColor];
    
    self.quesSixTF.layer.cornerRadius=5.0f;
    self.quesSixTF.layer.borderWidth = 1.0f;
    self.quesSixTF.layer.borderColor = [[UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0] CGColor];
    
    self.continueButton.layer.cornerRadius=20.0f;
    
    [self.continueButton addTarget:self action:@selector(onContinueTap) forControlEvents:UIControlEventTouchUpInside];
    
    for(int i=0;i<delegate1.questionArray.count;i++)
    {
        if(i==0)
        {
            if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:1]])
            {
                self.qusOneLabel.text=@"What was your favourite place to visit as a child?";
                
            }
            
            else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:2]])
            {
                self.qusOneLabel.text=@"What is the name of your favourite pet?";
                
            }
            
            else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:3]])
            {
                self.qusOneLabel.text=@"Which is your favourite color?";
                
            }
            
            else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:4]])
            {
                self.qusOneLabel.text=@"Which is your favourite web browser?";
                
            }
            
            else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:5]])
            {
                self.qusOneLabel.text=@"In which city were you born?";
                
            }
            
            else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:6]])
            {
                self.qusOneLabel.text=@"Which is your favourite movie?";
                
            }
            
            
        }
        
        if(i==1)
        {
            if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:1]])
            {
                self.qusTwoLabel.text=@"What was your favourite place to visit as a child?";
                
            }
            
            else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:2]])
            {
                self.qusTwoLabel.text=@"What is the name of your favourite pet?";
                
            }
            
            else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:3]])
            {
                self.qusTwoLabel.text=@"Which is your favourite color?";
                
            }
            
            else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:4]])
            {
                self.qusTwoLabel.text=@"Which is your favourite web browser?";
                
            }
            
            else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:5]])
            {
                self.qusTwoLabel.text=@"In which city were you born?";
                
            }
            
            else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:6]])
            {
                self.qusTwoLabel.text=@"Which is your favourite movie?";
                
            }
            
            
        }
        
        
    }
    
    self.twoAnsView.hidden=YES;
    self.fourAnsView.hidden=YES;
    if(delegate1.questionArray.count==2)
    {
        self.twoAnsView.hidden=NO;
        self.quesViewHgt.constant=0;
    }
    else if(delegate1.questionArray.count==6)
    {
        self.twoAnsView.hidden=NO;
        self.fourAnsView.hidden=NO;
        self.quesViewHgt.constant=340;
    }
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
     [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

-(void)onContinueTap
{
    @try
    {
    if(delegate1.questionArray.count==2)
    {
        if(self.quesOneTF.text.length>0&&self.quesTwoTF.text.length>0)
        {
           answersArray = [[NSMutableArray alloc]initWithObjects:self.quesOneTF.text,self.quesTwoTF.text,nil];
            [self answerCheck];
        }
        
            else
            {
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Please enter missing fields" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                
                [alert addAction:okAction];
            }
        
    }
    else if(delegate1.questionArray.count==6)
    {
        if(self.quesOneTF.text.length>0&&self.quesTwoTF.text.length>0&&self.quesThreeTF.text.length>0&&self.quesFourTF.text.length>0&&self.quesFiveTF.text.length>0&&self.quesSixTF.text.length>0)
        {
             answersArray = [[NSMutableArray alloc]initWithObjects:self.quesOneTF.text,self.quesTwoTF.text,self.quesThreeTF.text,self.quesFourTF.text,self.quesFiveTF.text,self.quesSixTF.text,nil];
            [self answerCheck];
        }
        else
        {
            
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Please enter missing fields" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                
                [alert addAction:okAction];
           
        }
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
       
-(void)answerCheck
{
   @try
    {
    //    NSString * ansString = [answersArray componentsJoinedByString:@","];
    //    //NSLog(@"ansString:%@",ansString);
    //    //PreLogin
    //    delegate1.mtCheck=true;
    //    TagEncode * tag1=[[TagEncode alloc]init];
    //
    //    [tag1 TagData:sharedManager.shMsgCode shValueMethod:sharedManager.SecurityQueAndAns];
    //    [tag1 TagData:sharedManager.stUserID stringMethod:delegate1.mtClientId];
    //    [tag1 TagData:sharedManager.stPassword stringMethod:delegate1.mtPassword];
    //    [tag1 TagData:sharedManager.stNewPassword stringMethod:@""];
    //    [tag1 TagData:sharedManager.inVersion intMethod:2];
    //
    //
    //
    //    for (int i=0; i<1; i++) {
    //        NSString * ansString1 = [NSString stringWithFormat:@"%@",[answersArray objectAtIndex:i]];
    //        [tag1 TagData:sharedManager.stSingleSecurityAnswer stringMethod:ansString1];
    //        [tag1 TagData:sharedManager.nlTagFooter];
    //
    //    }
    //
    //    [tag1 GetBuffer];
    //    [self newMessage];
    
    //new//
    self.activityInd.hidden=NO;
    [self.activityInd startAnimating];
    TagEncode * enocde=[[TagEncode alloc]init];
    loginReplyDict=[[NSDictionary alloc]init];
   
   if(inputArray.count>0)
   {
       [inputArray removeAllObjects];
   }
    
    inputArray=answersArray;
    if(inputArray.count==2)
    {
         enocde.inputRequestString=@"twosecurityans";
    }
    else if(inputArray.count==6)
    {
         enocde.inputRequestString=@"sixques";
    }
    [inputArray addObject:delegate1.mtClientId];
    [inputArray addObject:delegate1.mtPassword];
    [inputArray addObject:[NSString stringWithFormat:@"%@multitradeapi/securityanswers",delegate1.mtBaseUrl]];
    
//      [inputArray addObject:[NSString stringWithFormat:@"http://192.168.15.87:8014/api/multitradeapi/securityanswers"]];
    [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        NSArray * keysArray=[dict allKeys];
        
        
        if(keysArray.count>0)
        {
            self.activityInd.hidden=YES;
            [self.activityInd stopAnimating];
        loginReplyDict=dict;
        
        NSString * msg=[[[loginReplyDict objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"loginstatus"];
        delegate1.accessToken=[[[loginReplyDict objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"token"];
        
        if([msg containsString:@"Login Successful"])
        {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                delegate1.userID=delegate1.mtClientId;
//                delegate1.loginActivityStr=@"CLIENT";
//                NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
//                delegate1.zenwiseToken=delegate1.accessToken;
//                [loggedInUser setObject:delegate1.accessToken forKey:@"acesstoken"];
//                [loggedInUser setObject:delegate1.userID forKey:@"userid"];
//                [loggedInUser setObject:delegate1.brokerNameStr forKey:@"brokername"];
//                [loggedInUser setObject:delegate1.loginActivityStr forKey:@"loginActivityStr"];
//                [loggedInUser synchronize];
//                TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
//
//                [self presentViewController:tabPage animated:YES completion:nil];
//            });
            [self clientCreation];
        }else
        {
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:msg preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:^{
                
            }];
            
            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            [alert addAction:okAction];
        }
            
        }
        
    }];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)clientCreation
{
    @try
    {
    TagEncode * enocde=[[TagEncode alloc]init];
    
    enocde.inputRequestString=@"clientcreation";
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    NSUserDefaults *loggedInUserNew = [NSUserDefaults standardUserDefaults];
    
    [inputArray addObject:delegate1.userID];
    [inputArray addObject:@""];
    [inputArray addObject:[NSString stringWithFormat:@"%@",[delegate1.brokerInfoDict objectForKey:@"brokerid"]]];
    [inputArray addObject:@""];
    [inputArray addObject:@""];
     [inputArray addObject:@""];
    [inputArray addObject:[NSString stringWithFormat:@"%@2factor/validateotp",delegate1.baseUrl]];
    [inputArray addObject:[loggedInUserNew objectForKey:@"userid"]];
    [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        
        NSArray * keysArray=[dict allKeys];
        
        
        if(keysArray.count>0)
        {
            
            delegate1.zenwiseToken=delegate1.accessToken;
            
            
            
            
            delegate1.zenwiseToken=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"] objectForKey:@"authtoken"]];
            NSUserDefaults *loggedInUserNew = [NSUserDefaults standardUserDefaults];
            delegate1.loginActivityStr=@"CLIENT";
            [loggedInUserNew setObject:delegate1.accessToken forKey:@"acesstoken"];
            [loggedInUserNew setObject:delegate1.userID forKey:@"mtuserid"];
            [loggedInUserNew setObject:@"client" forKey:@"userrole"];
            [loggedInUserNew setObject:delegate1.brokerNameStr forKey:@"brokername"];
            [loggedInUserNew setObject:delegate1.loginActivityStr forKey:@"loginActivityStr"];
            [loggedInUserNew synchronize];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
                [self presentViewController:tabPage animated:YES completion:nil];
            });
            
        }
        else
        {
            [self clientCreation];
        }
        
    }];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
