//
//  NewCartViewController.h
//  PremiumServices
//
//  Created by zenwise mac 2 on 7/13/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "ViewController.h"

@interface NewCartViewController : ViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *cartTblView;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cartTblHgt;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHgt;

@property (weak, nonatomic) IBOutlet UILabel *totalItemsLbl;

@property (weak, nonatomic) IBOutlet UILabel *totalPriceLbl;
@property (weak, nonatomic) IBOutlet UILabel *discountLbl;

@property (weak, nonatomic) IBOutlet UILabel *subTotalLbl;
@property (weak, nonatomic) IBOutlet UILabel *gstLbl;

@property (weak, nonatomic) IBOutlet UILabel *otherChargesLbl;

@property (weak, nonatomic) IBOutlet UILabel *grandTotalLbl;

@property (weak, nonatomic) IBOutlet UIView *grandTotalView;
@property (weak, nonatomic) IBOutlet UIView *coupanView;

@property (weak, nonatomic) IBOutlet UIButton *paymentBtn;

@property (weak, nonatomic) IBOutlet UIButton *paymentAction;
@property (weak, nonatomic) IBOutlet UITextField *coupanTextFiled;


@end
