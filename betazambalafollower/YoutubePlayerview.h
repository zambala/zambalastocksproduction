//
//  YoutubePlayerview.h
//  betazambalafollower
//
//  Created by Zenwise Technologies on 02/07/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YTPlayerView.h"

@interface YoutubePlayerview : UIViewController

@property (weak, nonatomic) IBOutlet YTPlayerView *playerView;
@end
