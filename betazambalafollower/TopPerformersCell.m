//
//  TopPerformersCell.m
//  betazambalafollower
//
//  Created by Zenwise Technologies on 15/06/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import "TopPerformersCell.h"

@implementation TopPerformersCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
+(NSString*)cellIdentifier{
    static NSString* cellIdentifier;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cellIdentifier = @"TopPerformersCell";
    });
    return cellIdentifier;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
