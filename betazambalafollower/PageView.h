//
//  PageView.h
//  testing
//
//  Created by zenwise technologies on 24/11/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageView : UIViewController<UIScrollViewDelegate>
{
    IBOutlet UIPageControl *pageControl;
    IBOutlet UIScrollView *scrollView;
}

//@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

-(IBAction)changePage:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *getStartBtn;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityInd;
@property (strong, nonatomic) UIWindow *window;
@end
