//
//  OpenAccountView.h
//  testing
//
//  Created by zenwise technologies on 28/02/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpenAccountView : UIViewController<UITableViewDataSource,UITableViewDelegate>
- (IBAction)informationAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *brokerTbl;
@property NSMutableDictionary *responseArray;
@end
