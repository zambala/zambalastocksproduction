//
//  ViewController.m
//  ProfileFollower
//
//  Created by guna on 16/03/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "NewProfile.h"
#import "TableViewCell.h"
#import "AppDelegate.h"
#import "MKSHorizontalLineProgressView.h"
#import "HCSStarRatingView.h"
#import "NewProfileTableViewCell.h"
#import "KNCirclePercentView.h"
#import "KNPercentLabel.h"
#import "StockView1.h"
#import "StockOrderView.h"
#import "NewOffersByMonksViewController.h"
#import "FeedsView.h"
#import "NoAdvicesProfileTableViewCell.h"
#import <Mixpanel/Mixpanel.h>
#import "KATCircularProgress.h"

@interface NewProfile ()
{
    AppDelegate * delegate1;
    int subscribed;
    NSString * EPString;
    NSString * TPString;
    NSString * SLString;
    NSString * buySell1;
    NSString * messageStr;
    NSString * buySell;
    NSString * str;
    NSArray * segmentsArray;
    NSArray * filterAdvice;
    NSString * number;
}

@end

@implementation NewProfile

- (void)viewDidLoad {
    
    [super viewDidLoad];
    filterAdvice=[[NSArray alloc]init];
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
    
    number= [loggedInUser stringForKey:@"profilemobilenumber"];
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"market_expert_details_page"];
   
 str=@"%";
    [self profileServer];
    [self recentAdvicesServer];
    self.profileTableView.delegate=self;
    self.profileTableView.dataSource=self;
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.premiumSerivceBtn addGestureRecognizer:singleFingerTap];
    
    
    
    UITapGestureRecognizer *singleFingerTap1 =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap1:)];
    [self.feedsBtn addGestureRecognizer:singleFingerTap1];
    [self.followBtn addTarget:self action:@selector(followAction) forControlEvents:UIControlEventTouchUpInside];
  
    
//    self.performanceView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
//    self.performanceView.layer.shadowOffset = CGSizeMake(0, 2.0f);
//    self.performanceView.layer.shadowOpacity = 1.0f;
//    self.performanceView.layer.shadowRadius = 1.0f;
//    self.performanceView.layer.cornerRadius=1.0f;
//    self.performanceView.layer.masksToBounds = NO;
//    
//    self.statsView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
//    self.statsView.layer.shadowOffset = CGSizeMake(0, 2.0f);
//    self.statsView.layer.shadowOpacity = 1.0f;
//    self.statsView.layer.shadowRadius = 1.0f;
//    self.statsView.layer.cornerRadius=1.0f;
//    self.statsView.layer.masksToBounds = NO;
    
//    self.premiumSerivceBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
//    self.premiumSerivceBtn.layer.shadowOffset = CGSizeMake(0, 5.0f);
//    self.premiumSerivceBtn.layer.shadowOpacity = 5.0f;
//    self.premiumSerivceBtn.layer.shadowRadius = 4.0f;
//    self.premiumSerivceBtn.layer.cornerRadius=4.1f;
//    self.premiumSerivceBtn.layer.masksToBounds = NO;
    
//    self.feedsBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
//    self.feedsBtn.layer.shadowOffset = CGSizeMake(0, 5.0f);
//    self.feedsBtn.layer.shadowOpacity = 5.0f;
//    self.feedsBtn.layer.shadowRadius = 4.0f;
//    self.feedsBtn.layer.cornerRadius=4.1f;
//    self.feedsBtn.layer.masksToBounds = NO;
    
    self.followBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.followBtn.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.followBtn.layer.shadowOpacity = 5.0f;
    self.followBtn.layer.shadowRadius = 4.0f;
    self.followBtn.layer.cornerRadius=4.1f;
    self.followBtn.layer.masksToBounds = NO;
    
    [[self.premiumSerivceBtn layer] setBorderWidth:2.0f];
    [[self.premiumSerivceBtn layer] setBorderColor:[UIColor colorWithRed:5/255.0 green:32/255.0 blue:43/255.0 alpha:1.0f].CGColor];
    self.premiumSerivceBtn.layer.cornerRadius=20;
    self.premiumSerivceBtn.clipsToBounds=YES;
    
    self.premiumSerivceBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
        self.premiumSerivceBtn.layer.shadowOffset = CGSizeMake(0, 3.0f);
        self.premiumSerivceBtn.layer.shadowOpacity = 1.0f;
        self.premiumSerivceBtn.layer.shadowRadius = 4.0f;
//        self.premiumSerivceBtn.layer.cornerRadius=1.0f;
        self.premiumSerivceBtn.layer.masksToBounds = NO;
    
    [[self.feedsBtn layer] setBorderWidth:2.0f];
    [[self.feedsBtn layer] setBorderColor:[UIColor colorWithRed:5/255.0 green:32/255.0 blue:43/255.0 alpha:1.0f].CGColor];
    self.feedsBtn.layer.cornerRadius=20;
    self.feedsBtn.clipsToBounds=YES;
    
    self.feedsBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.feedsBtn.layer.shadowOffset = CGSizeMake(0, 3.0f);
    self.feedsBtn.layer.shadowOpacity = 1.0f;
    self.feedsBtn.layer.shadowRadius = 4.0f;
    //        self.premiumSerivceBtn.layer.cornerRadius=1.0f;
    self.feedsBtn.layer.masksToBounds = NO;
    
//    [[self.feedsBtn layer] setBorderWidth:2.0f];
//    [[self.feedsBtn layer] setBorderColor:[UIColor colorWithRed:5/255.0 green:32/255.0 blue:43/255.0 alpha:1.0f].CGColor];
//    self.feedsBtn.layer.cornerRadius=20;
//    self.feedsBtn.clipsToBounds=YES;
//    self.feedsBtn.layer.shadowOffset = CGSizeMake(0, 3.0f);
//    self.feedsBtn.layer.shadowOpacity = 1.0f;
//    self.feedsBtn.layer.shadowRadius = 5.0f;
////    self.feedsBtn.layer.cornerRadius=1.0f;
//    self.feedsBtn.layer.masksToBounds = NO;
    
    [[self.gainLossView layer] setBorderWidth:2.0f];
    [[self.gainLossView layer] setBorderColor:[UIColor colorWithRed:30/255.0 green:113/255.0 blue:142/255.0 alpha:0.7f].CGColor];
    self.gainLossView.layer.cornerRadius=4;
    self.gainLossView.clipsToBounds=YES;

}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
//    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    
    delegate1.cartLeaderName=[NSString stringWithFormat:@"%@",[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"firstname"]];
    
    delegate1.leaderid=[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"userid"];
    NewOffersByMonksViewController * offers = [self.storyboard instantiateViewControllerWithIdentifier:@"NewOffersByMonksViewController"];
    [self.navigationController pushViewController:offers animated:YES];
    
    //Do stuff here...
}

- (void)handleSingleTap1:(UITapGestureRecognizer *)recognizer
{
//    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    
    delegate1.profileTOFeeds=@"YES";
    
    FeedsView * feed = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedsView"];
    [self.navigationController pushViewController:feed animated:YES];
    
    //Do stuff here...
}

-(void)followAction
{
    @try
    {
    NSDictionary *headers = @{ @"content-type": @"application/x-www-form-urlencoded",
                               @"cache-control": @"no-cache",
                                @"authtoken":delegate1.zenwiseToken,
                               @"deviceid":delegate1.currentDeviceId
                            
                               };
    
    NSString * userid=[NSString stringWithFormat:@"leaderid=%@",delegate1.leaderIDWhoToFollow];
    
    NSString * clientId=[NSString stringWithFormat:@"&clientid=%@",delegate1.userID];
    
    
    NSMutableData *postData = [[NSMutableData alloc] initWithData:[userid dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[clientId dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSArray * subscribeArray=@[@1];
    
    NSString * subscribeString=[NSString stringWithFormat:@"&subscriptiontype=%@",subscribeArray];
    NSString * sub = [subscribeString stringByReplacingOccurrencesOfString:@"(" withString:@"{"];
    NSString * sub1 = [sub stringByReplacingOccurrencesOfString:@")" withString:@"}"];
    
    NSArray * unSubscribeArray=@[];
    
    NSString * unSubscribeString=[NSString stringWithFormat:@"&subscriptiontype=%@",unSubscribeArray];
    NSString * unSub = [unSubscribeString stringByReplacingOccurrencesOfString:@"(" withString:@"{"];
    NSString * unSub1 = [unSub stringByReplacingOccurrencesOfString:@")" withString:@"}"];
    
    
    NSString * btnTitle=[NSString stringWithFormat:@"%@",self.followBtn.currentTitle];
    
    
    if([btnTitle isEqualToString:@"Following"])
        
    {
        [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[unSub1 dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        
    }
    else if ([btnTitle isEqualToString:@"+ Follow"])
    {
        [postData appendData:[@"&subscribe=true" dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[sub1 dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        
    }
    
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower",delegate1.baseUrl]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if(data !=nil)
                                                    {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==403)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                            
                                                        }
                                                        else if([httpResponse statusCode]==401)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                        NSMutableDictionary *followDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        //NSLog(@"%@",followDict);
                                                        
                                                        messageStr=[followDict objectForKey:@"message"];
                                                        }
                                                        
                                                        
                                                    }
                                                    
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        
                                                        if([messageStr isEqualToString:@"Subscriber created"])
                                                        {
                                                            if([self.followBtn.currentTitle isEqualToString:@"+ Follow"])
                                                            {
                                                                 [self.followBtn setTitle:@"Following" forState:UIControlStateNormal];
                                                            }
                                                            else
                                                            {
                                                                  [self.followBtn setTitle:@"+ Follow" forState:UIControlStateNormal];
                                                                
                                                            }
                                                           
                                                            
                                                            
                                                        }
                                                    });
                                                    
                                                    
                                                    }
                                                    else
                                                    {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            
                                                            UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                [self followAction];
                                                            }];
                                                            
                                                            UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                
                                                            }];
                                                            
                                                            
                                                            [alert addAction:retryAction];
                                                            [alert addAction:cancel];
                                                            
                                                        });
                                                        
                                                    }
                                                }];
    [dataTask resume];
    
    
    
    
    
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    
}





-(void)recentAdvicesServer
{
     @try {
    segmentsArray=[[NSArray alloc]init];
    
         if([delegate1.loginActivityStr isEqualToString:@"OTP1"])
         {
             
             segmentsArray=[delegate1.brokerInfoDict objectForKey:@"guestsegment"];
         }
         
         else
         {
             segmentsArray=[delegate1.brokerInfoDict objectForKey:@"segment"];
         }
         NSString * segmentString=[NSString stringWithFormat:@"%@",segmentsArray];
         NSString * segmentF = [segmentString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"()"]];
         NSString * segment1 = [segmentF stringByReplacingOccurrencesOfString:@"(" withString:@""];
         NSString * segment2 = [segment1 stringByReplacingOccurrencesOfString:@"\n" withString:@""];
         NSString * segment3 = [segment2 stringByReplacingOccurrencesOfString:@" " withString:@""];
         //NSLog(@"SegmentString:%@",segment3);
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                                @"authtoken":delegate1.zenwiseToken,
                               @"deviceid":delegate1.currentDeviceId
                             
                                };
    NSDictionary *parameters;
         
     
   
        parameters = @{ @"leaderid": delegate1.leaderIDWhoToFollow,
                        @"messagetypeid":@1,
                        @"active":@(true),
                        @"sortby":@"DESC",
                        @"orderby":@"createdon",
                        @"clientid":delegate1.userID,
                        @"limit":@100,
                        @"segment":segment3,
                        @"isleaderadvice":@(true),
                        @"brokerid":[delegate1.brokerInfoDict objectForKey:@"brokerid"],
                        @"mobilenumber":number
                        };

  
//
    
  
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower/wisdomgarden",delegate1.baseUrl]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if(data != nil)
                                                    {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        if(data!=nil)
                                                        {
                                                            if([httpResponse statusCode]==403)
                                                            {
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                
                                                            }
                                                            else if([httpResponse statusCode]==401)
                                                            {
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                
                                                            }
                                                            else
                                                            {
                                                                
                                                            self.recentAdvicesDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            //NSLog(@"Recent Advices:%@",self.recentAdvicesDictionary);
                                                            
                                                            }
                                                        }
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            [self.profileTableView reloadData];
                                                            
                                                            
                                                        });

                                                        
                                                    }
                                                    }
                                                    else
                                                    {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            
                                                            UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                [self recentAdvicesServer];
                                                            }];
                                                            
                                                            UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                
                                                            }];
                                                            
                                                            
                                                            [alert addAction:retryAction];
                                                            [alert addAction:cancel];
                                                            
                                                        });
                                                        
                                                        
                                                        
                                                    }
                                                    
                                                }];
    [dataTask resume];
         
     } @catch (NSException *exception) {
         
         
         
     } @finally {
         
     }
}

-(void)profileServer
{
    @try
    {
    NSDictionary *headers = @{ @"content-type": @"application/x-www-form-urlencoded",
                               @"cache-control": @"no-cache",
                               @"authtoken":delegate1.zenwiseToken,
                                  @"deviceid":delegate1.currentDeviceId
                               };
    
    NSString * leaderID = [NSString stringWithFormat:@"leaderid=%@",delegate1.leaderIDWhoToFollow];
     NSString * clientid = [NSString stringWithFormat:@"&clientid=%@",delegate1.userID];
        NSString * number = [NSString stringWithFormat:@"&mobilenumber=%@",number];
    
    NSMutableData *postData = [[NSMutableData alloc] initWithData:[leaderID dataUsingEncoding:NSUTF8StringEncoding]];
    
    [postData appendData:[clientid dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[number dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower/leaders",delegate1.baseUrl]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if(data !=nil)
                                                    {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==403)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                            
                                                        }
                                                        else if([httpResponse statusCode]==401)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                        self.profileDetailReponseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        //NSLog(@"Response Dict leader:%@",self.profileDetailReponseDictionary);
                                                        }
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        @try {
                                                            
                                                            NSString * successDetails=[NSString stringWithFormat:@"%@ out of %@ ratings were successful",[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"successcount"],[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"totaladvices"]];
                                                            
                                                            self.successDetailsLbl.text=successDetails;
                                                            
                                                            NSString * subscribeString=[NSString stringWithFormat:@"%@",[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"subscribed"]];
                                                            
                                                            int subscribeInt=[subscribeString intValue];
                                                            
                                                            if(subscribeInt==0)
                                                            {
                                                                [self.followBtn setTitle:@"+ Follow" forState:UIControlStateNormal];
                                                                
                                                            }
                                                            
                                                            else if(subscribeInt==1)
                                                            {
                                                               [self.followBtn setTitle:@"Following" forState:UIControlStateNormal];
                                                            }
                                                            
                                                            NSString * publicStr=[NSString stringWithFormat:@"%@",[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"ispublicleader"]];
                                                            
                                                            int publicInt=[publicStr intValue];
                                                            
                                                            if(publicInt==0)
                                                            {
                                                                self.followBtn.hidden=NO;
                                                                self.followerCount.hidden=NO;
                                                                self.followBtnWidth.constant=75;
                                                                self.aboutLbl.hidden=NO;
                                                                  self.aboutLblHgt.constant=30;
                                                                self.mainDetailsBgViewHgt.constant=257;
                                                                self.sebiIdLbl.hidden=NO;
                                                                self.sebiNoLbl.hidden=NO;
                                                                self.offersViewHgt.constant=80;
                                                                self.leaderOffersView.hidden=NO;
                                                                self.currentAdviceviewHgt.constant=261;
                                                                  self.sebiNoLbl.text=[NSString stringWithFormat:@"%@",[[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"about"]objectForKey:@"sebi_no"]];
                                                                
                                                            }
                                                            
                                                            else if(publicInt==1)
                                                            {
                                                                self.followBtn.hidden=NO;
                                                                self.followerCount.hidden=NO;
                                                                self.followBtnWidth.constant=0;
                                                                  self.aboutLbl.hidden=YES;
                                                                self.sebiIdLbl.text=@"Organization:";
                                                                self.sebiNoLbl.text=[NSString stringWithFormat:@"%@",[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"leaderorgname"]];
                                                                self.aboutLblHgt.constant=0;
                                                                self.mainDetailsBgViewHgt.constant=190;
                                                                self.sebiIdLbl.hidden=NO;
                                                                self.sebiNoLbl.hidden=NO;
                                                                self.offersViewHgt.constant=0;
                                                                self.leaderOffersView.hidden=YES;
                                                                 self.currentAdviceviewHgt.constant=179;
                                                            }
                                                            
                                                            NSString * winningStr=[NSString stringWithFormat:@"%@",[[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"openmessagedata"]objectForKey:@"winnersCount"]];
                                                            
                                                           
                                                            if([winningStr isEqualToString:@"<null>"]||[winningStr isEqual:[NSNull null]])
                                                            {
                                                                  self.winningLbl.text=@"0";
                                                              
                                                            }
                                                            
                                                            else
                                                            {
                                                                 self.winningLbl.text=winningStr;
                                                            }
                                                            
                                                            NSString * looserStr=[NSString stringWithFormat:@"%@",[[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"openmessagedata"]objectForKey:@"losersCount"]];
                                                            
                                                            
                                                            if([looserStr isEqualToString:@"<null>"]||[looserStr isEqual:[NSNull null]])
                                                            {
                                                                self.winningLbl.text=@"0";
                                                                
                                                            }
                                                            
                                                            else
                                                            {
                                                                self.lossingLbl.text=looserStr;
                                                            }
                                                            
                                                            
                                                            @try {
                                                                NSString* rating=[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"rating"];
                                                                if([rating isEqual:[NSNull null]])
                                                                {
                                                                    self.starRatingView.value=0;
                                                                }else
                                                                {
                                                                    self.starRatingView.value = [rating floatValue];
                                                                }
                                                                
                                                                
                                                                self.starRatingView.minimumValue=0;
                                                                self.starRatingView.maximumValue=5;
                                                                self.starRatingView.allowsHalfStars=YES;
                                                                self.starRatingView.allowsHalfStars=YES;
                                                            } @catch (NSException *exception) {
                                                                
                                                            } @finally {
                                                                
                                                            }
                                                            
                                                            
                                                           
                                int dayTrade = [[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"daytradecount"] intValue];

                             int shortTerm = [[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"shorttermcount"]intValue];

                                 int longTerm = [[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"longtermcount"]intValue];

                                        int totalAdvices=dayTrade+shortTerm+longTerm;

                                                                float dayPer=0;
                                                                float shortPer=0;
                                                                float longPer=0;


                                                                if(dayTrade!=0)
                                                                {
                                                                      dayPer=(float)dayTrade/(float)totalAdvices*100;

                                                                }

                                                                if(shortTerm!=0)
                                                                {
                                                         shortPer=(float)shortTerm/(float)totalAdvices*100;

                                                                }

                                                                if(longTerm!=0)
                                                                {
                                                                     longPer=(float)longTerm/(float)totalAdvices*100;

                                                                }
                              
                                                            NSString * str1=@"%";
                                                            
    NSString * dayTrade1 = [NSString stringWithFormat:@"%.2f%@",[[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"dthitrate"]floatValue],str1];

     NSString * shortTerm1 = [NSString stringWithFormat:@"%.2f%@",[[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"sthitrate"]floatValue],str1];

     NSString * longTerm1 = [NSString stringWithFormat:@"%.2f%@",[[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"lthitrate"]floatValue],str1];
                                                            @try {
                                                                
//                                                                int hitRate = [[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"hitrate"] intValue];
//                                                                NSString * str=@"%";
//                                                                NSString * hitrateStr=[NSString stringWithFormat:@"%i%@",hitRate,str];
//
//                                                                self.overallAccLbl.text=hitrateStr;
//
//                                                                if([self.overallAccLbl.text containsString:@"-"])
//                                                                {
//                                                                    self.overallAccLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
//                                                                }
//
//                                                                else
//                                                                {
//                                                                    self.overallAccLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
//                                                                }
//
                                                                //                                                                int hitrate1=5;
                                                                
                                                                //Clear the slice items array.
                                                                [self.dayTradeProgression.sliceItems removeAllObjects];
                                                                 [self.shortProgression.sliceItems removeAllObjects];
                                                                 [self.longTermProgression.sliceItems removeAllObjects];
                                                                
                                                                //set bar thickness
                                                                [self.dayTradeProgression setLineWidth:8.0];
                                                                //set animation duration
                                                                [self.dayTradeProgression setAnimationDuration:2];
                                                                
                                                                //set bar thickness
                                                                [self.shortProgression setLineWidth:8.0];
                                                                //set animation duration
                                                                [self.shortProgression setAnimationDuration:2];
                                                                
                                                                //set bar thickness
                                                                [self.longTermProgression setLineWidth:8.0];
                                                                //set animation duration
                                                                [self.longTermProgression setAnimationDuration:2];
                                                                
                                                                //Create Slice Item objects.
                                                                SliceItem *item1 = [[SliceItem alloc] init];
                                                                item1.itemValue = [dayTrade1 floatValue];
                                                                item1.itemColor = [UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f];
                                                                
                                                                SliceItem *item2 = [[SliceItem alloc] init];
                                                                item2.itemValue = [shortTerm1 floatValue];
                                                                item2.itemColor =[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f];
                                                                SliceItem *item3 = [[SliceItem alloc] init];
                                                                item3.itemValue = [longTerm1 floatValue];
                                                                item3.itemColor =[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f];
                                                                
                                                                SliceItem *item4 = [[SliceItem alloc] init];
                                                                float value1=100-[dayTrade1 floatValue];
                                                                item4.itemValue = value1;
                                                                item4.itemColor = [UIColor colorWithRed:250.0f/255.0f green:250.0f/255.0f blue:250.0f/255.0f alpha:1.0f];
                                                                
                                                                SliceItem *item5 = [[SliceItem alloc] init];
                                                                float value2=100-[shortTerm1 floatValue];
                                                                item5.itemValue = value2;
                                                                item5.itemColor = [UIColor colorWithRed:250.0f/255.0f green:250.0f/255.0f blue:250.0f/255.0f alpha:1.0f];
                                                                
                                                                
                                                                SliceItem *item6 = [[SliceItem alloc] init];
                                                                float value3=100-[longTerm1 floatValue];
                                                                item6.itemValue = value3;
                                                                item6.itemColor = [UIColor colorWithRed:250.0f/255.0f green:250.0f/255.0f blue:250.0f/255.0f alpha:1.0f];
                                                                
                                                                
                                                                //add objects to the sliceItems array.
                                                                [self.longTermProgression.sliceItems addObject:item1];
                                                                [self.longTermProgression.sliceItems addObject:item4];
                                                                [self.shortProgression.sliceItems addObject:item2];
                                                                [self.shortProgression.sliceItems addObject:item5];
                                                                [self.dayTradeProgression.sliceItems addObject:item3];
                                                                [self.dayTradeProgression.sliceItems addObject:item6];
                                                                
                                                                //reload the chart
                                                                [self.dayTradeProgression reloadData];
                                                                 [self.shortProgression reloadData];
                                                                [self.longTermProgression reloadData];
                                                                
                                                                
                                                            } @catch (NSException *exception) {
                                                                
                                                            } @finally {
                                                                
                                                            }
                                                            
                                                            self.dtLbl.text=longTerm1;
                                                            self.stLbl.text=shortTerm1;
                                                            self.ltLbl.text=dayTrade1;

////                                                            self.dtLbl.textColor=[UIColor colorWithRed:255.0f/255.0f green:192.0f/255.0f blue:102.0f/255.0f alpha:1.0f];
////
////                                                            self.stLbl.textColor=[UIColor colorWithRed:165.0f/255.0f green:225.0f/255.0f blue:241.0f/255.0f alpha:1.0f];
////
////                                                            self.ltLbl.textColor=[UIColor colorWithRed:227.0f/255.0f green:168.0f/255.0f blue:246.0f/255.0f alpha:1.0f];
////
//
//
//                                                                  self.dayTradeView.progressValue=100.0f;
//
//                                                                    self.shortTermView.progressValue=100.0f;
//
//                                                                    self.longTermView.progressValue=100.0f;
//
                                                            
                                                            NSString * dt = [NSString stringWithFormat:@"(%@/%@)",[[[self.profileDetailReponseDictionary objectForKey:@"results"]objectAtIndex:0]objectForKey:@"dthitcount"],[[[self.profileDetailReponseDictionary objectForKey:@"results"]objectAtIndex:0]objectForKey:@"daytradecount"]];
                                                            NSString * st = [NSString stringWithFormat:@"(%@/%@)",[[[self.profileDetailReponseDictionary objectForKey:@"results"]objectAtIndex:0]objectForKey:@"sthitcount"],[[[self.profileDetailReponseDictionary objectForKey:@"results"]objectAtIndex:0]objectForKey:@"shorttermcount"]];
                                                            NSString * lt = [NSString stringWithFormat:@"(%@/%@)",[[[self.profileDetailReponseDictionary objectForKey:@"results"]objectAtIndex:0]objectForKey:@"lthitcount"],[[[self.profileDetailReponseDictionary objectForKey:@"results"]objectAtIndex:0]objectForKey:@"longtermcount"]];
                                                            self.DT.text=lt;
                                                            self.ST.text=st;
                                                            self.LT.text=dt;
                                                                
                                                                
                                                                self.dayTradeView.barColor = [UIColor colorWithRed:255.0f/255.0f green:192.0f/255.0f blue:102.0f/255.0f alpha:1.0f];
                                                                self.dayTradeView.trackColor = [UIColor colorWithRed:241.0/255.0 green:241.0/255.0 blue:241.0/255.0 alpha:1];
                                                                self.dayTradeView.barThickness = self.dayTradeView.frame.size.height;
                                                                self.dayTradeView.showPercentageText = NO;
                                                                
                                                                
                                                                
                                                                self.shortTermView.barColor = [UIColor colorWithRed:165.0f/255.0f green:225.0f/255.0f blue:241.0f/255.0f alpha:1.0f];
                                                                self.shortTermView.trackColor = [UIColor colorWithRed:241.0/255.0 green:241.0/255.0 blue:241.0/255.0 alpha:1];
                                                                self.shortTermView.barThickness = self.shortTermView.frame.size.height;
                                                                self.shortTermView.showPercentageText = NO;
                                                                
                                                                
                                                                
                                                                self.longTermView.barColor = [UIColor colorWithRed:227.0f/255.0f green:168.0f/255.0f blue:246.0f/255.0f alpha:1.0f];
                                                                self.longTermView.trackColor = [UIColor colorWithRed:241.0/255.0 green:241.0/255.0 blue:241.0/255.0 alpha:1];
                                                                self.longTermView.barThickness = self.shortTermView.frame.size.height;
                                                                self.longTermView.showPercentageText = NO;
                                                            
                                                         
                                                            
                                                            @try {
                                                                
                                                                float hitRate = [[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"hitrate"] floatValue];
                                                                NSString * str=@"%";
                                                                NSString * hitrateStr=[NSString stringWithFormat:@"%.2f%@",hitRate,str];
                                                                
                                                                self.overallAccLbl.text=hitrateStr;
                                                                
                                                                if([self.overallAccLbl.text containsString:@"-"])
                                                                {
                                                                    self.overallAccLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                                                                }
                                                                
                                                                else
                                                                {
                                                                       self.overallAccLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                                                                }
                                                               
//                                                                int hitrate1=5;
                                                                
                                                                //Clear the slice items array.
                                                                [self.progressView.sliceItems removeAllObjects];

                                                                //set bar thickness
                                                                [self.progressView setLineWidth:15.0];
                                                                //set animation duration
                                                                [self.progressView setAnimationDuration:2];

                                                                //Create Slice Item objects.
                                                                SliceItem *item1 = [[SliceItem alloc] init];
                                                                item1.itemValue = dayPer;
                                                                item1.itemColor = [UIColor colorWithRed:255.0f/255.0f green:192.0f/255.0f blue:102.0f/255.0f alpha:1.0f];;

                                                                SliceItem *item2 = [[SliceItem alloc] init];
                                                                item2.itemValue = shortPer;
                                                                item2.itemColor =[UIColor colorWithRed:165.0f/255.0f green:225.0f/255.0f blue:241.0f/255.0f alpha:1.0f];

                                                                SliceItem *item3 = [[SliceItem alloc] init];
                                                                item3.itemValue = longPer;
                                                                item3.itemColor = [UIColor colorWithRed:227.0f/255.0f green:168.0f/255.0f blue:246.0f/255.0f alpha:1.0f];

                                                                
                                                                //add objects to the sliceItems array.
                                                                [self.progressView.sliceItems addObject:item1];
                                                                [self.progressView.sliceItems addObject:item2];
                                                                [self.progressView.sliceItems addObject:item3];
                                                               
                                                                //reload the chart
                                                                [self.progressView reloadData];
                                                                

                                                            } @catch (NSException *exception) {
                                                                
                                                            } @finally {
                                                                
                                                            }
                                                            
                                                            
                                                        } @catch (NSException *exception) {
                                                            
                                                        } @finally {
                                                            
                                                        }
                                                            
                                                        
                                                        self.starRatingView.emptyStarImage = [UIImage imageNamed:@"ratingstarEmpty"];
                                                        self.starRatingView.halfStarImage = [UIImage imageNamed:@"starsmallHalf"]; // optional
                                                        self.starRatingView.filledStarImage = [UIImage imageNamed:@"ratingstarFilled"];
                                                        [self.starRatingView setUserInteractionEnabled:NO];
                                                        
                                                        @try {
                                                            NSString * firstName=[NSString stringWithFormat:@"%@",[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"firstname"]];
                                                            NSString * lastName=[NSString stringWithFormat:@"%@",[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"lastname"]];
                                                            
                                                            if([firstName isEqualToString:@"<null>"])
                                                            {
                                                                firstName=@"";
                                                            }
                                                            
                                                            if([lastName isEqualToString:@"<null>"])
                                                            {
                                                                lastName=@"";
                                                            }
                                                            
                                                            self.leaderNameLbl.text=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                                                          
                                                        } @catch (NSException *exception) {
                                                            
                                                        } @finally {
                                                            
                                                        }
                                                        
                                                        
                                                      
                                                        
                                                        @try {
                                                            NSString *about = [[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"about"] objectForKey:@"info"];
//                                                            NSString * sebiNo= [[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"about"] objectForKey:@"sebiregno"];
                                                            if([about isEqual:[NSNull null]]||[about isEqualToString:@""])
                                                            {
                                                                self.aboutLbl.text = @"No Info";
                                                            }else
                                                            {
                                                                self.aboutLbl.text = about;
                                                            }
//                                                            if([sebiNo isEqual:[NSNull null]]||[about isEqualToString:@""]||[sebiNo isEqualToString:@"<null>"])
//                                                            {
//                                                                self.sebiNoLbl.text = @"---";
//                                                            }else
//                                                            {
//                                                               self.sebiNoLbl.text = sebiNo;
//                                                            }
                                                        } @catch (NSException *exception) {
                                                            
                                                             self.aboutLbl.text = @"No Info";
                                                            
                                                        } @finally {
                                                            
                                                        }
                                                        
                                                        
                                                        
                                                        @try {
                                                            NSString * gainPercentage = [NSString stringWithFormat:@"%@",[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"gainpercent"]];
                                                            if([gainPercentage isEqual:[NSNull null]])
                                                            {
                                                                self.gainPercentLabel.text=@"0 %";
                                                            }else
                                                            {
                                                                NSString * percent = @"%";
                                                               
                                                                float gainFloat = [gainPercentage floatValue];
                                                                NSString * gainString = [NSString stringWithFormat:@"%.2f ",gainFloat];
                                                                NSString * finalGainPercent = [gainString stringByAppendingString:percent];
                                                                self.gainPercentLabel.text=[NSString stringWithFormat:@"%@gain",finalGainPercent];
                                                            }
                                                        } @catch (NSException *exception) {
                                                            
                                                        } @finally {
                                                            
                                                        }
                                                        
                                                        @try {
                                                            
                                                            NSString * followersCount =[NSString stringWithFormat:@"%@ followers",[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"followercount"]];
                                                            
                                                            if([followersCount isEqual:[NSNull null]]||[followersCount isEqualToString:@"<null>"])
                                                            {
                                                                self.followerCount.text=@"0 followers";
                                                            }else
                                                            {
                                                                self.followerCount.text=followersCount;
                                                            }

                                                        } @catch (NSException *exception) {
                                                            
                                                        } @finally {
                                                            
                                                        }
                                                        
                                                        
                                                        @try {
                                                           
                                                            
                                NSString * avgcalls =[NSString stringWithFormat:@"%i", [[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"avgcallperday"]intValue]];
                                                            
                                                            if([avgcalls isEqual:[NSNull null]])
                                                            {
                                                                self.averageCalls.text= @"0.00";
                                                            }else
                                                            {
                                                                self.averageCalls.text =avgcalls;
                                                            }
                                                            

                                                        } @catch (NSException *exception) {
                                                            
                                                        } @finally {
                                                            
                                                        }
                                                        
                                                        @try {
                                                            NSString * maxgain = [[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"maxgainpercent"];
                                                            
                                                            
                                                            if([maxgain isEqual:[NSNull null]])
                                                            {
                                                                self.maximumGain.text= @"Max gain 0";
                                                            }else
                                                            {
                                                                NSString * maxGainPercent = [NSString stringWithFormat:@"%@",maxgain];
                                                                float maxGainFloat = [maxGainPercent floatValue];
                                                                
                                                                self.maximumGain.text = [NSString stringWithFormat:@"Max gain %.2f%@",maxGainFloat,str];
                                                            }
                                                        } @catch (NSException *exception) {
                                                            
                                                        } @finally {
                                                            
                                                        }
                                                        
                                                        
                                                        @try {
                                                            NSString * maxLoss = [[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"maxlosspercent"];
                                                            
                                                            if([maxLoss isEqual:[NSNull null]])
                                                            {
                                                                self.maximumLoss.text= @"Max loss 0";
                                                            }else
                                                            {
                                                                NSString * maxLossPercent = [NSString stringWithFormat:@"%@",maxLoss];
                                                                float maxLoss = [maxLossPercent floatValue];
                                                                
                                                                self.maximumLoss.text =[NSString stringWithFormat:@"Max loss %.2f%@",maxLoss,str];
                                                            }

                                                        } @catch (NSException *exception) {
                                                            
                                                        } @finally {
                                                            
                                                        }
                                                            
                                                        
                                                        @try {
                                                            NSMutableArray * segment = [[NSMutableArray alloc]init];
                                                            
                                                            segment=[[[self.profileDetailReponseDictionary objectForKey:@"results"]objectAtIndex:0]objectForKey:@"segment"];
                                                            
                                                            NSMutableString * segmentMutString = [[NSMutableString alloc]init];
                                                            if(segment.count>0)
                                                            {
                                                            for(int i=1;i<=4;i++)
                                                            {
                                                                
                                                                //for (int j=0; j<segment.count; j++) {
                                                                if([segment containsObject:[NSNumber numberWithInt:i]])
                                                                {
                                                                    if(i==1)
                                                                    {
                                                                        [segmentMutString appendString:@",Equities"];
                                                                    }
                                                                    if(i==2)
                                                                    {
                                                                        [segmentMutString appendString:@",Derivatives"];
                                                                    }
                                                                    if(i==3)
                                                                    {
                                                                        [segmentMutString appendString:@",Currency"];
                                                                    }
                                                                    if(i==4)
                                                                    {
                                                                        [segmentMutString appendString:@",Commodities"];
                                                                    }
                                                                    
                                                                    //  }
                                                                    
                                                                }
                                                            }
                                                            }
                                                            else
                                                            {
                                                                  [segmentMutString appendString:@"-"];
                                                            }
                                                            //NSLog(@"Segment Test:%@",segmentMutString);
                                                            
                                                            NSString * aboutString=[segmentMutString mutableCopy];
                                                            
                                                            
                                                            aboutString = [aboutString substringFromIndex:1];
                                                            
                                                          
                                                            
                                                            
                                                            self.segmentsLabel.text = aboutString;
                                                            
                                                            
                                                            NSMutableArray * specialization = [[NSMutableArray alloc]init];
                                                            
                                                            specialization=[[[self.profileDetailReponseDictionary objectForKey:@"results"]objectAtIndex:0]objectForKey:@"specialization"];
                                                            
                                                            NSMutableString * specializationMutString = [[NSMutableString alloc]init];
                                                            if(specialization.count>0)
                                                            {
                                                            for(int i=1;i<=3;i++)
                                                            {
                                                                
                                                                //for (int j=0; j<segment.count; j++) {
                                                                if([specialization containsObject:[NSNumber numberWithInt:i]])
                                                                {
                                                                    if(i==1)
                                                                    {
                                                                        [specializationMutString appendString:@",DayTrade"];
                                                                    }
                                                                    if(i==2)
                                                                    {
                                                                        [specializationMutString appendString:@",ShortTerm"];
                                                                    }
                                                                    if(i==3)
                                                                    {
                                                                        [specializationMutString appendString:@",LongTerm"];
                                                                    }
                                                                   
                                                                    
                                                                    //  }
                                                                    
                                                                }
                                                            }
                                                            }
                                                            else
                                                            {
                                                                [specializationMutString appendString:@"-"];
                                                            }
                                                            //NSLog(@"Specialization Test:%@",specializationMutString);
                                                            
                                                            NSString * aboutString1=[specializationMutString mutableCopy];
                                                            
                                                            
                                                            aboutString1 = [aboutString1 substringFromIndex:1];
                                                            self.specializationLabel.text = aboutString1;
                                                            
                                                            NSString * logo=[[[self.profileDetailReponseDictionary objectForKey:@"results"]objectAtIndex:0]objectForKey:@"logourl"];
                                                            
                                                            self.profileImage.layer.cornerRadius=self.profileImage.frame.size.width / 2;
                                                            self.profileImage.clipsToBounds = YES;
                                                            
                                                            if([logo isEqual:[NSNull null]])
                                                            {
                                                                self.profileImage.image = nil;
                                                            }
                                                            
                                                            else
                                                            {
                                                                
                                                                NSURL *url = [NSURL URLWithString:logo];
                                                                
                                                                NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                                                    if (data) {
                                                                        UIImage *image = [UIImage imageWithData:data];
                                                                        if (image) {
                                                                            
                                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                     self.profileImage.image=image;
                                                           
                                                                            });
                                                                        }
                                                                    }
                                                                }];
                                                                [task resume];
                                                            }
                                                            

                                                            NSString *dateStr =[[[self.profileDetailReponseDictionary objectForKey:@"results"]objectAtIndex:0]objectForKey:@"created"];
                                                            
                                                            //NSLog(@"%@",dateStr);
                                                            
                                                            // Convert string to date object
                                                            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];  //2017-05-31T13:45:29.623Z
                                                            [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
                                                            NSDate *date = [dateFormat dateFromString:dateStr];
                                                            
                                                            
                                                    
                                                        } @catch (NSException *exception) {
                                                            
                                                        } @finally {
                                                            
                                                        }
                                                        
                                                        
                                                    });
                                                    }
                                                    else
                                                    {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            
                                                            UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                [self profileServer];
                                                            }];
                                                            
                                                            UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                
                                                            }];
                                                            
                                                            
                                                            [alert addAction:retryAction];
                                                            [alert addAction:cancel];
                                                            
                                                        });
                                                        
                                                        
                                                        
                                                    }
                                                    
                                                    

                                                }];
    [dataTask resume];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if([[self.recentAdvicesDictionary objectForKey:@"data"] count]>0)
    {
    
    return [[self.recentAdvicesDictionary objectForKey:@"data"] count];
    }
    else
    {
        return 1;
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    @try
    {
    if([[self.recentAdvicesDictionary objectForKey:@"data"] count]>0)
    {
    
        NSArray * filterAdvice=[[NSArray alloc]init];
        filterAdvice=[self.recentAdvicesDictionary objectForKey:@"data"];
       NewProfileTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"profile" forIndexPath:indexPath];
        
        NSDictionary * contra=[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"];
        NSDictionary * openAdvice=[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"];
        NSString * contraStr=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"]];
        
        if(contra && openAdvice && ![contraStr isEqualToString:@"<null>"])
        {
       
            int messageType=[[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"contra_advice"]objectForKey:@"messagetypeid"] intValue];
            cell.subView.hidden=NO;
            
            if(messageType!=4)
            {
        
             
                int duration=[[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"contra_advice"]objectForKey:@"durationtype"] intValue];
                if(duration==1)
                {
                    cell.durationLbl.text=@"DT";
                    
                    cell.durationLbl.backgroundColor=[UIColor colorWithRed:(246/255.0) green:(195/255.0) blue:(117/255.0) alpha:1.0f];
                }
                else if(duration==2)
                {
                    cell.durationLbl.text=@"ST";
                    
                    cell.durationLbl.backgroundColor=[UIColor colorWithRed:(178/255.0) green:(224/255.0) blue:(239/255.0) alpha:1.0f];
                }
                else if(duration==3)
                {
                    cell.durationLbl.text=@"LT";
                    
                    cell.durationLbl.backgroundColor=[UIColor colorWithRed:(220/255.0) green:(172/255.0) blue:(241/255.0) alpha:1.0f];
                }
                
                for(int i=0;i<=1;i++)
                {
                    NSString * mainString;
                    NSNumber * buttonNumber;
                    NSString * contraBuySell;
                    int buttonTitle;
                    
                    if(i==0)
                    {
                        mainString=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"message"]];
                        buttonNumber = [[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"buysell"];
                        
                        buttonTitle = [buttonNumber intValue];
                        if(buttonTitle==1)
                        {
                            contraBuySell=@"BUY";
                            [cell.buySellButton setTitle:@"BUY" forState:UIControlStateNormal];
                            
                            
                            
                            [cell.buySellButton setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1.0f]];
                            
                            
                            
                        }else if (buttonTitle==2)
                        {
                            contraBuySell=@"SELL";
                            [cell.buySellButton setTitle:@"SELL" forState:UIControlStateNormal];
                            
                            [cell.buySellButton setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1.0f]];
                            
                        }
                    }
                    else  if(i==1)
                    {
                        mainString=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"message"]];
                        buttonNumber = [[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"buysell"];
                        
                         buttonTitle = [buttonNumber intValue];
                    }
                    
                    
                   
                    if([mainString containsString:@"@"])
                    {
                        cell.messageLabel.text=[mainString uppercaseString];
                        
                        //                cell.oLbl.text=@"C";
                    }
                    
                    
                    
                    else
                    {
                        
                        //                cell.oLbl.text=@"O";
                        TPString=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"targetprice"]];
                        
                        if ([mainString isEqual:[NSNull null]]||mainString==nil||[mainString isEqualToString:@"<null>"] ||  [TPString isEqual:[NSNull null]]||TPString==nil||[TPString isEqualToString:@"<null>"]   ) {
                            cell.messageLabel.text =@"<Null>";
                        }
                        
                        else{
                            
                            @try {
                                
                                
                                //NSLog(@"main String %@",mainString);
                                
                                
                                
                                NSArray * mainArray=[mainString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                                
                                
                                
                                //NSLog(@"array  %@",[mainArray objectAtIndex:0]);
                                
                                //NSLog(@"array  %@",[mainArray objectAtIndex:1]);
                                
                                //                NSString * buySell=[NSString stringWithFormat:@"%@",[mainArray objectAtIndex:0]];
                                
                                if(buttonTitle==1)
                                {
                                    buySell=@"BUY";
                                    
                                }else if (buttonTitle==2)
                                {
                                    buySell=@"SELL";
                                    
                                }
                                
                                NSString * string=[NSString stringWithFormat:@"%@",[mainArray objectAtIndex:1]];
                                
                                
                                
                                NSArray * companyArray=[string componentsSeparatedByString:@";"];
                                
                                
                                
                                //NSLog(@" array1 %@",companyArray);
                                
                                
                                
                                //               for (int i=1; i<[companyArray count];i++) {
                                
                                
                                
                                //                   NSString * string11=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:0]];
                                //
                                //
                                //
                                //                   NSArray * array2=[string11 componentsSeparatedByString:@";"];
                                //
                                //                   //NSLog(@"%@",array2);
                                
                                NSString * companyName=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:0]];
                                NSString * EP1=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:1]];
                                NSString * TP=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:2]];
                                NSString * SL;
                                NSArray * SLArray;
                                if([mainString containsString:@"SL="])
                                {
                                    SL=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:3]];
                                    SLArray=[SL componentsSeparatedByString:@"="];
                                    SLString=[NSString stringWithFormat:@"%@",[SLArray objectAtIndex:1]];
                                    
                                }
                                else
                                {
                                    SL=@"";
                                    SLString=@"";
                                }
                                
                                NSArray * EPArray=[EP1 componentsSeparatedByString:@"="];
                                NSArray * TPArray=[TP componentsSeparatedByString:@"="];
                                
                                
                                EPString=[NSString stringWithFormat:@"%@",[EPArray objectAtIndex:1]];
                                TPString=[NSString stringWithFormat:@"%@",[TPArray objectAtIndex:1]];
                                
                                
                                
                                NSString * dummyfinalStr=[NSString stringWithFormat:@"%@ %@ @%@%@%@",buySell,companyName,EP1,TP,SL];
                                
                                NSString * finalStr = [dummyfinalStr stringByReplacingOccurrencesOfString:@"=" withString:@" "];
                                
                                //NSLog(@"%@",finalStr);
                                
                                //NSLog(@"COMPANY %@",companyName);
                                
                                
                                //                              if (i==1) {
                                //                                  EP=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                                //
                                //                                  //NSLog(@"EP %@",[array2 objectAtIndex:1]);
                                //
                                //                              }
                                
                                //                   if (i==2) {
                                //                       EPString=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                                //
                                //                       //NSLog(@"TP %@",[array2 objectAtIndex:1]);
                                //
                                //                   }
                                //
                                //                   if (i==3) {
                                //                       SLString=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                                //
                                //                       //NSLog(@"SL %@",[array2 objectAtIndex:1]);
                                //
                                //                   }
                                //               }
                                //
                                
                                
                                NSString * sell = [NSString stringWithFormat:@"<html><body><div style='text-align:left;display:inline-block;font-size:10px;font-family:Ubuntu;color:#4b4d52'>  %@</div> </body></html>",buySell];
                                NSString * muthootfin =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:10px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",[companyArray objectAtIndex:0]];
                                NSString * ep = @"<html><body><div style='display:inline-block;text-align:left;font-size:10px;font-family:Ubuntu;color:#4b4d52'> @ EP</div></html></body>";
                                NSString * value1 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:10px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",EPString];
                                NSString * tp = @"<html><body><div style='display:inline-block;text-align:left;font-size:10px;font-family:Ubuntu;color:#4b4d52'> TP</div></html></body>";
                                NSString * value2 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:10px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",TPString];
                                NSString * sl = @"<html><body><div style='display:inline-block;text-align:left;font-size:10px;font-family:Ubuntu;color:#4b4d52'> SL</div></html></body>";
                                NSString * value3 =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:10px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",SLString] ;
                                
                                NSString * string1 = [sell stringByAppendingString:muthootfin];
                                NSString *string2 = [string1 stringByAppendingString:ep];
                                NSString * string3 = [string2 stringByAppendingString:value1];
                                NSString * string4 = [string3 stringByAppendingString:tp];
                                NSString * string5 = [string4 stringByAppendingString:value2];
                                NSString * string6 = [string5 stringByAppendingString:sl];
                                NSString * finalString;
                                if([mainString containsString:@"SL="])
                                {
                                    finalString = [string6 stringByAppendingString:value3];
                                }
                                else
                                {
                                    finalString = string5;
                                }
                                
                                
                                
                                NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[finalString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                                
                                //UILabel * myLabel = [[UILabel alloc] init];
                                // myLabel.attributedText = attrStr;
                                
                                
                                if(i==0)
                                {
                                    cell.messageLabel.attributedText=attrStr;
                                }
                                else  if(i==1)
                                {
                                    cell.originalAdvice.attributedText=attrStr;
                                }
                            }
                            @catch (NSException * e) {
                                //NSLog(@"Exception: %@", e);
                            }
                            @finally {
                                //NSLog(@"finally");
                            }
                            
                        }
                        
                        
                        
                    }
                    
                }
                
                
                
                cell.ltpLabel.text =[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"lasttradedprice"]];
                
                NSString * changePercentlabel = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"contra_advice"]objectForKey:@"changepercent"]];
                
                NSString * percent =@"  ";
                NSString * finalChangePercentlabel = [changePercentlabel stringByAppendingString:percent];
                if([changePercentlabel isEqual:[NSNull null]]||changePercentlabel==nil||[changePercentlabel isEqualToString:@"<null>"])
                {
                    
                    
                    
                    NSString * percentage = @"";
                    NSString * zero = @"  0";
                    NSString* final = [zero stringByAppendingString:percentage];
                    cell.changePercentLabel.text=final;
                    cell.changePercentLabel.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                }
                else
                {
                    
                    if([finalChangePercentlabel containsString:@"-"])
                    {
                        cell.changePercentLabel.text=finalChangePercentlabel;
                        cell.changePercentLabel.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                        cell.closeLbl.layer.borderColor =[[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1] CGColor];
                        [cell.closeLbl setTitleColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1] forState:UIControlStateNormal];
                        [cell.closeLbl setTitle:@"Closing Advice" forState:UIControlStateNormal];
                        //                    cell.oLbl.backgroundColor=[UIColor colorWithRed:(238/255.0) green:(43/255.0) blue:(53/255.0) alpha:1];
                    }
                    else{
                        cell.changePercentLabel.text=finalChangePercentlabel;
                        cell.changePercentLabel.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                        cell.closeLbl.layer.borderColor =[[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1] CGColor];
                        [cell.closeLbl setTitleColor:[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1] forState:UIControlStateNormal];
                        [cell.closeLbl setTitle:@"Closing Advice" forState:UIControlStateNormal];
                        //                    cell.oLbl.backgroundColor=[UIColor colorWithRed:(86/255.0) green:(168/255.0) blue:(66/255.0) alpha:1];
                    }
                    
                    
                }
                
                NSString * actedBy = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"contra_advice"]objectForKey:@"actedby"]];
                
                if([actedBy isEqual:[NSNull null]]||actedBy==nil||[actedBy isEqualToString:@"<null>"])
                {
                    cell.actedByLabel.text=@"0";
                }else
                {
                    cell.actedByLabel.text=actedBy;
                }
                NSString * sharesSold = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"sharesold"]];
                if([sharesSold isEqual:[NSNull null]]||sharesSold==nil||[sharesSold isEqualToString:@"<null>"])
                {
                    cell.sharesSoldLabel.text=@"0";
                }else
                {
                    cell.sharesSoldLabel.text=sharesSold;
                }
                
                
                
                
                NSString * dateStr=[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"createdon"];
                
                
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
                [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                NSDate *date = [dateFormatter dateFromString:dateStr]; // create date from string
                
                // change to a readable time format and change to local time zone
                [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
                [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                NSString *finalDate = [dateFormatter stringFromDate:date];
                
                
                
                
                
                cell.dateAndTimeLabel.text=finalDate;
                
                
             
                
                // cell.valueChangeLabel.attributedText=strText;
                // [strText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Ubuntu-Medium" size:12.5] range:NSMakeRange(6, )];
                
                
                //       [cell.ordersBtn addTarget:self action:@selector(showingOrders) forControlEvents:UIControlEventTouchUpInside];
                
                
                [cell.buySellButton addTarget:self action:@selector(buySellAction:) forControlEvents:UIControlEventTouchUpInside];
                
               
             
                
                
                
                
                @try {
                    
                    NSString * actedBy =[ [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"contra_advice"]objectForKey:@"actedby"];
                    
                    
                    if([actedBy isEqual:[NSNull null]])
                    {
                        cell.actedByLabel.text=@"0";
                    }else
                    {
                        int actedByInt=[actedBy intValue];
                        cell.actedByLabel.text = [NSString stringWithFormat:@"%i",actedByInt];
                    }
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                
                @try {
                    
                    NSString * sharesSold =[ [[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"sharesold"];
                    
                    
                    
                    if([sharesSold isEqual:[NSNull null]])
                    {
                        cell.sharesSoldLabel.text = @"0";
                    }else
                    {
                        int sharesSoldInt=[sharesSold intValue];
                        cell.sharesSoldLabel.text = [NSString stringWithFormat:@"%i",sharesSoldInt];
                    }
                    
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                
                @try {
                    
                    NSString * averageProfit =[ [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"contra_advice"]objectForKey:@"averageprofit"];
                    
                    
                    
                    if([averageProfit isEqual:[NSNull null]])
                    {
                        
                    }else
                    {
                        int sharesSoldInt=[averageProfit intValue];
                        
                    }
                    
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                
                
                
//                NSString * duration=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"durationtype"]];
//
//                int durationInt=[duration intValue];
//
//                if(durationInt==1)
//                {
//                    cell.durationTypeLbl.text=@"Day Trade";
//                }
//
//                else if(durationInt==2)
//                {
//                    cell.durationTypeLbl.text=@"Short Term";
//                }
//                else if(durationInt==3)
//                {
//
//                    cell.durationTypeLbl.text=@"Long Term";
//
//
//
//                }
                
               
                
                NSString * public=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"contra_advice"]objectForKey:@"ispublic"]];
                int publicInt=[public intValue];
                
                if(publicInt==1)
                {
                    cell.premiumImgView.hidden=NO;
                    
                    cell.premiumImgView.image=[UIImage imageNamed:@"publicnew"];
                    
                    
//
//                    NSString * source = [NSString stringWithFormat:@"%@",[[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"]objectForKey:@"otherinfo"]objectForKey:@"source"]];
//                    cell.sourceLabel.hidden=NO;
//                    cell.sourceDataLabel.hidden=NO;
//                    cell.sourceDataLabel.text = source;
//                    cell.analystName.hidden=NO;
//                    cell.analystNameHeightConstant.constant=21;
//                    NSString * analystName = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"]objectForKey:@"leaderorgname"]];
//                    NSString * dummyAnalystName= [analystName stringByReplacingOccurrencesOfString:@" " withString:@""];
//                    dummyAnalystName=[dummyAnalystName lowercaseString];
//                    cell.profileName.text=analystName;
//                    NSString * firstName = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"]objectForKey:@"firstname"]];
//                    NSString * lastname = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"]objectForKey:@"lastname"]];
//
//                    NSString * dummyName;
//                    if([lastname isEqual:[NSNull null]]||[lastname containsString:@"<null>"])
//                    {
//                        lastname=@"";
//                        NSString * name = [NSString stringWithFormat:@"%@ %@",firstName,lastname];
//                        dummyName = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
//                        dummyName=[dummyName lowercaseString];
//                        cell.analystName.text=name;
//                    }else
//                    {
//                        NSString * name = [NSString stringWithFormat:@"%@ %@",firstName,lastname];
//                        dummyName = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
//                        dummyName=[dummyName lowercaseString];
//                        cell.analystName.text = name;
//                    }
//                    if([dummyAnalystName containsString:dummyName])
//                    {
//                        cell.analystName.hidden=YES;
//                        cell.analystNameHeightConstant.constant=0;
//                    }else
//                    {
//                        cell.analystName.hidden=NO;
//                        cell.analystNameHeightConstant.constant=21;
//                        //                    cell.profileName.text = analystName;
//                    }
//
                    
                    
                    
                }
                
                else if(publicInt==0)
                {
                    cell.premiumImgView.hidden=YES;
                    
                    
                    NSString * sub=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"contra_advice"] objectForKey:@"subscriptiontypeid"]];
                    
                    int subInt=[sub intValue];
                    
                    if(subInt==1)
                    {
                        cell.premiumImgView.hidden=YES;
                        
                    }
                    
                    else if(subInt==2)
                    {
                        cell.premiumImgView.hidden=NO;
                        
                        cell.premiumImgView.image=[UIImage imageNamed:@"premiumNew"];
                        
                    }
                    
//                    cell.sourceLabel.hidden=YES;
//                    cell.sourceDataLabel.hidden=YES;
//                    cell.profileName.text=[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"firstname"];
//                    cell.analystName.hidden=YES;
//                    cell.analystNameHeightConstant.constant=0;
                    
                }
                
                
               
                
               
                
                //original advice//
                
                NSString * originalLtp1=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"lasttradedprice"]];
                
                if([originalLtp1 isEqual:[NSNull null]]||[originalLtp1 isEqualToString:@"<null>"])
                {
                    cell.originalLtp.text=@"0.00";
                }
                else
                {
                    cell.originalLtp.text=originalLtp1;
                    
                }
                
                NSString * originalChg=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"changepercent"]];
                
                if([originalChg isEqual:[NSNull null]]||[originalChg isEqualToString:@"<null>"])
                {
                    cell.originalPl.text=@"0.00";
                }
                else
                {   if([originalChg containsString:@"-"])
                {
                    
                    cell.originalPl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                    
                    cell.openLbl.layer.borderColor =[[UIColor colorWithRed:(238/255.0) green:(43/255.0) blue:(53/255.0) alpha:1] CGColor];
                    [cell.openLbl setTitleColor:[UIColor colorWithRed:(238/255.0) green:(43/255.0) blue:(53/255.0) alpha:1] forState:UIControlStateNormal];
                    //                    cell.oLbl.backgroundColor=[UIColor colorWithRed:(238/255.0) green:(43/255.0) blue:(53/255.0) alpha:1];
                }
                else{
                    
                    cell.originalPl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                    cell.openLbl.layer.borderColor = [[UIColor colorWithRed:(87.0)/255 green:(160.0)/255 blue:(55.0)/255 alpha:1.0] CGColor];
                    [cell.openLbl setTitleColor:[UIColor colorWithRed:(87.0)/255 green:(160.0)/255 blue:(55.0)/255 alpha:1.0] forState:UIControlStateNormal];
                    //                    cell.oLbl.backgroundColor=[UIColor colorWithRed:(86/255.0) green:(168/255.0) blue:(66/255.0) alpha:1];
                }
                    cell.originalPl.text=originalChg;
                    
                }
                
                NSString * originalActed=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"actedby"]];
                
                if([originalActed isEqual:[NSNull null]]||[originalActed isEqualToString:@"<null>"])
                {
                    cell.originalActedBy.text=@"0.00";
                }
                else
                {
                    cell.originalActedBy.text=originalActed;
                    
                }
                
                NSString * originalShares=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"sharesold"]];
                
                if([originalShares isEqual:[NSNull null]]||[originalShares isEqualToString:@"<null>"])
                {
                    cell.originalShares.text=@"0.00";
                }
                else
                {
                    cell.originalShares.text=originalShares;
                    
                }
                
                NSString * dateStr1=[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"createdon"];
                
                
                
                NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
                [dateFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
                [dateFormatter1 setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                NSDate *date1 = [dateFormatter1 dateFromString:dateStr1]; // create date from string
                
                // change to a readable time format and change to local time zone
                [dateFormatter1 setDateFormat:@"dd MMM yyyy HH:mm"];
                [dateFormatter1 setTimeZone:[NSTimeZone localTimeZone]];
                NSString *finalDate1 = [dateFormatter1 stringFromDate:date1];
                cell.originalDateLbl.text=finalDate1;
                
                
                return cell;
    }
        }
        
        else if(openAdvice)
        {
            {
                
                int messageType=[[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"open_advice"]objectForKey:@"messagetypeid"] intValue];
                 cell.subView.hidden=YES;
                if(messageType!=4)
                {
                    int duration=[[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"open_advice"]objectForKey:@"durationtype"] intValue];
                    if(duration==1)
                    {
                        cell.durationLbl.text=@"DT";
                        
                        cell.durationLbl.backgroundColor=[UIColor colorWithRed:(246/255.0) green:(195/255.0) blue:(117/255.0) alpha:1.0f];
                    }
                    else if(duration==2)
                    {
                        cell.durationLbl.text=@"ST";
                        
                        cell.durationLbl.backgroundColor=[UIColor colorWithRed:(178/255.0) green:(224/255.0) blue:(239/255.0) alpha:1.0f];
                    }
                    else if(duration==3)
                    {
                        cell.durationLbl.text=@"LT";
                        
                        cell.durationLbl.backgroundColor=[UIColor colorWithRed:(220/255.0) green:(172/255.0) blue:(241/255.0) alpha:1.0f];
                    }
                    
                    
                        NSString * mainString;
                        NSNumber * buttonNumber;
                        
                    
                            mainString=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"message"]];
                            buttonNumber = [[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"buysell"];
                     
                        
                        
                        int buttonTitle = [buttonNumber intValue];
                        if(buttonTitle==1)
                        {
                                                [cell.buySellButton setTitle:@"BUY" forState:UIControlStateNormal];
                            
                            
                            
                                                [cell.buySellButton setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1.0f]];
                            
                            
                            
                        }else if (buttonTitle==2)
                        {
                            [cell.buySellButton setTitle:@"SELL" forState:UIControlStateNormal];
                            
                            [cell.buySellButton setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1.0f]];
                            
                        }
                        if([mainString containsString:@"@"])
                        {
                            cell.messageLabel.text=[mainString uppercaseString];
                            
                            //                cell.oLbl.text=@"C";
                        }
                        
                        
                        
                        else
                        {
                            
                            //                cell.oLbl.text=@"O";
                            TPString=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"targetprice"]];
                            
                            if ([mainString isEqual:[NSNull null]]||mainString==nil||[mainString isEqualToString:@"<null>"] ||  [TPString isEqual:[NSNull null]]||TPString==nil||[TPString isEqualToString:@"<null>"]   ) {
                                cell.messageLabel.text =@"<Null>";
                            }
                            
                            else{
                                
                                @try {
                                    
                                    
                                    //NSLog(@"main String %@",mainString);
                                    
                                    
                                    
                                    NSArray * mainArray=[mainString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                                    
                                    
                                    
                                    //NSLog(@"array  %@",[mainArray objectAtIndex:0]);
                                    
                                    //NSLog(@"array  %@",[mainArray objectAtIndex:1]);
                                    
                                    //                NSString * buySell=[NSString stringWithFormat:@"%@",[mainArray objectAtIndex:0]];
                                    
                                    if(buttonTitle==1)
                                    {
                                        buySell=@"BUY";
                                        
                                    }else if (buttonTitle==2)
                                    {
                                        buySell=@"SELL";
                                        
                                    }
                                    
                                    NSString * string=[NSString stringWithFormat:@"%@",[mainArray objectAtIndex:1]];
                                    
                                    
                                    
                                    NSArray * companyArray=[string componentsSeparatedByString:@";"];
                                    
                                    
                                    
                                    //NSLog(@" array1 %@",companyArray);
                                    
                                    
                                    
                                    //               for (int i=1; i<[companyArray count];i++) {
                                    
                                    
                                    
                                    //                   NSString * string11=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:0]];
                                    //
                                    //
                                    //
                                    //                   NSArray * array2=[string11 componentsSeparatedByString:@";"];
                                    //
                                    //                   //NSLog(@"%@",array2);
                                    
                                    NSString * companyName=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:0]];
                                    NSString * EP1=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:1]];
                                    NSString * TP=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:2]];
                                    NSString * SL;
                                    NSArray * SLArray;
                                    if([mainString containsString:@"SL="])
                                    {
                                        SL=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:3]];
                                        SLArray=[SL componentsSeparatedByString:@"="];
                                        SLString=[NSString stringWithFormat:@"%@",[SLArray objectAtIndex:1]];
                                        
                                    }
                                    else
                                    {
                                        SL=@"";
                                        SLString=@"";
                                    }
                                    
                                    NSArray * EPArray=[EP1 componentsSeparatedByString:@"="];
                                    NSArray * TPArray=[TP componentsSeparatedByString:@"="];
                                    
                                    
                                    EPString=[NSString stringWithFormat:@"%@",[EPArray objectAtIndex:1]];
                                    TPString=[NSString stringWithFormat:@"%@",[TPArray objectAtIndex:1]];
                                    
                                    
                                    
                                    NSString * dummyfinalStr=[NSString stringWithFormat:@"%@ %@ @%@%@%@",buySell,companyName,EP1,TP,SL];
                                    
                                    NSString * finalStr = [dummyfinalStr stringByReplacingOccurrencesOfString:@"=" withString:@" "];
                                    
                                    //NSLog(@"%@",finalStr);
                                    
                                    //NSLog(@"COMPANY %@",companyName);
                                    
                                    
                                    //                              if (i==1) {
                                    //                                  EP=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                                    //
                                    //                                  //NSLog(@"EP %@",[array2 objectAtIndex:1]);
                                    //
                                    //                              }
                                    
                                    //                   if (i==2) {
                                    //                       EPString=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                                    //
                                    //                       //NSLog(@"TP %@",[array2 objectAtIndex:1]);
                                    //
                                    //                   }
                                    //
                                    //                   if (i==3) {
                                    //                       SLString=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                                    //
                                    //                       //NSLog(@"SL %@",[array2 objectAtIndex:1]);
                                    //
                                    //                   }
                                    //               }
                                    //
                                    
                                    
                                    NSString * sell = [NSString stringWithFormat:@"<html><body><div style='text-align:left;display:inline-block;font-size:10px;font-family:Ubuntu;color:#4b4d52'>  %@</div> </body></html>",buySell];
                                    NSString * muthootfin =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:10px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",[companyArray objectAtIndex:0]];
                                    NSString * ep = @"<html><body><div style='display:inline-block;text-align:left;font-size:10px;font-family:Ubuntu;color:#4b4d52'> @ EP</div></html></body>";
                                    NSString * value1 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:10px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",EPString];
                                    NSString * tp = @"<html><body><div style='display:inline-block;text-align:left;font-size:10px;font-family:Ubuntu;color:#4b4d52'> TP</div></html></body>";
                                    NSString * value2 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:10px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",TPString];
                                    NSString * sl = @"<html><body><div style='display:inline-block;text-align:left;font-size:10px;font-family:Ubuntu;color:#4b4d52'> SL</div></html></body>";
                                    NSString * value3 =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:10px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",SLString] ;
                                    
                                    NSString * string1 = [sell stringByAppendingString:muthootfin];
                                    NSString *string2 = [string1 stringByAppendingString:ep];
                                    NSString * string3 = [string2 stringByAppendingString:value1];
                                    NSString * string4 = [string3 stringByAppendingString:tp];
                                    NSString * string5 = [string4 stringByAppendingString:value2];
                                    NSString * string6 = [string5 stringByAppendingString:sl];
                                    NSString * finalString;
                                    if([mainString containsString:@"SL="])
                                    {
                                        finalString = [string6 stringByAppendingString:value3];
                                    }
                                    else
                                    {
                                        finalString = string5;
                                    }
                                    
                                    
                                    
                                    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[finalString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                                    
                                    //UILabel * myLabel = [[UILabel alloc] init];
                                    // myLabel.attributedText = attrStr;
                                    
                                    
                                  
                                        cell.messageLabel.attributedText=attrStr;
                                  
                                }
                                @catch (NSException * e) {
                                    //NSLog(@"Exception: %@", e);
                                }
                                @finally {
                                    //NSLog(@"finally");
                                }
                                
                            
                            
                            
                            
                        }
                        
                    }
                    
                    
                    
                    cell.ltpLabel.text =[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"lasttradedprice"]];
                    
                    NSString * changePercentlabel = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"open_advice"]objectForKey:@"changepercent"]];
                    
                    NSString * percent =@"  ";
                    NSString * finalChangePercentlabel = [changePercentlabel stringByAppendingString:percent];
                    if([changePercentlabel isEqual:[NSNull null]]||changePercentlabel==nil||[changePercentlabel isEqualToString:@"<null>"])
                    {
                        
                        
                        
                        NSString * percentage = @"";
                        NSString * zero = @"  0";
                        NSString* final = [zero stringByAppendingString:percentage];
                        cell.changePercentLabel.text=final;
                        cell.changePercentLabel.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                    }
                    else
                    {
                        
                        if([finalChangePercentlabel containsString:@"-"])
                        {
                            cell.changePercentLabel.text=finalChangePercentlabel;
                            cell.changePercentLabel.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                            cell.closeLbl.layer.borderColor =[[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1] CGColor];
                            [cell.closeLbl setTitleColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1] forState:UIControlStateNormal];
                             [cell.closeLbl setTitle:@"Opening Advice" forState:UIControlStateNormal];
                            //                    cell.oLbl.backgroundColor=[UIColor colorWithRed:(238/255.0) green:(43/255.0) blue:(53/255.0) alpha:1];
                        }
                        else{
                            cell.changePercentLabel.text=finalChangePercentlabel;
                            cell.changePercentLabel.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                            cell.closeLbl.layer.borderColor =[[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1] CGColor];
                            [cell.closeLbl setTitleColor:[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1] forState:UIControlStateNormal];
                             [cell.closeLbl setTitle:@"Opening Advice" forState:UIControlStateNormal];
                            //                    cell.oLbl.backgroundColor=[UIColor colorWithRed:(86/255.0) green:(168/255.0) blue:(66/255.0) alpha:1];
                        }
                        
                        
                    }
                    
                    NSString * actedBy = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"open_advice"]objectForKey:@"actedby"]];
                    
                    if([actedBy isEqual:[NSNull null]]||actedBy==nil||[actedBy isEqualToString:@"<null>"])
                    {
                        cell.actedByLabel.text=@"0";
                    }else
                    {
                        cell.actedByLabel.text=actedBy;
                    }
                    NSString * sharesSold = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"sharesold"]];
                    if([sharesSold isEqual:[NSNull null]]||sharesSold==nil||[sharesSold isEqualToString:@"<null>"])
                    {
                        cell.sharesSoldLabel.text=@"0";
                    }else
                    {
                        cell.sharesSoldLabel.text=sharesSold;
                    }
                    
                    
                    
                    
                    NSString * dateStr=[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"createdon"];
                    
                    
                    
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
                    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                    NSDate *date = [dateFormatter dateFromString:dateStr]; // create date from string
                    
                    // change to a readable time format and change to local time zone
                    [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
                    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                    NSString *finalDate = [dateFormatter stringFromDate:date];
                    
                    
                    
                    
                    
                    cell.dateAndTimeLabel.text=finalDate;
                    
                    
                    
                    
                    // cell.valueChangeLabel.attributedText=strText;
                    // [strText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Ubuntu-Medium" size:12.5] range:NSMakeRange(6, )];
                    
                    
                    //       [cell.ordersBtn addTarget:self action:@selector(showingOrders) forControlEvents:UIControlEventTouchUpInside];
                    
                    
                    [cell.buySellButton addTarget:self action:@selector(buySellAction:) forControlEvents:UIControlEventTouchUpInside];
                    
                    
                    
                    
                    
                    
                    
                    @try {
                        
                        NSString * actedBy =[ [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"open_advice"]objectForKey:@"actedby"];
                        
                        
                        if([actedBy isEqual:[NSNull null]])
                        {
                            cell.actedByLabel.text=@"0";
                        }else
                        {
                            int actedByInt=[actedBy intValue];
                            cell.actedByLabel.text = [NSString stringWithFormat:@"%i",actedByInt];
                        }
                    } @catch (NSException *exception) {
                        
                    } @finally {
                        
                    }
                    
                    @try {
                        
                        NSString * sharesSold =[ [[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"sharesold"];
                        
                        
                        
                        if([sharesSold isEqual:[NSNull null]])
                        {
                            cell.sharesSoldLabel.text = @"0";
                        }else
                        {
                            int sharesSoldInt=[sharesSold intValue];
                            cell.sharesSoldLabel.text = [NSString stringWithFormat:@"%i",sharesSoldInt];
                        }
                        
                    } @catch (NSException *exception) {
                        
                    } @finally {
                        
                    }
                    
                    @try {
                        
                        NSString * averageProfit =[ [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"open_advice"]objectForKey:@"averageprofit"];
                        
                        
                        
                        if([averageProfit isEqual:[NSNull null]])
                        {
                            
                        }else
                        {
                            int sharesSoldInt=[averageProfit intValue];
                            
                        }
                        
                    } @catch (NSException *exception) {
                        
                    } @finally {
                        
                    }
                    
                    
//                    
//                    NSString * duration=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"durationtype"]];
//                    
//                    int durationInt=[duration intValue];
//                    
//                    if(durationInt==1)
//                    {
//                        cell.durationTypeLbl.text=@"Day Trade";
//                    }
//                    
//                    else if(durationInt==2)
//                    {
//                        cell.durationTypeLbl.text=@"Short Term";
//                    }
//                    else if(durationInt==3)
//                    {
//                        
//                        cell.durationTypeLbl.text=@"Long Term";
//                        
//                        
//                        
//                    }
                    
                  
                    
                    NSString * public=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"open_advice"]objectForKey:@"ispublic"]];
                    int publicInt=[public intValue];
                    
                    if(publicInt==1)
                    {
                        cell.premiumImgView.hidden=NO;
                        
                        cell.premiumImgView.image=[UIImage imageNamed:@"publicnew"];
                        
                        
                        //
                        //                    NSString * source = [NSString stringWithFormat:@"%@",[[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"]objectForKey:@"otherinfo"]objectForKey:@"source"]];
                        //                    cell.sourceLabel.hidden=NO;
                        //                    cell.sourceDataLabel.hidden=NO;
                        //                    cell.sourceDataLabel.text = source;
                        //                    cell.analystName.hidden=NO;
                        //                    cell.analystNameHeightConstant.constant=21;
                        //                    NSString * analystName = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"]objectForKey:@"leaderorgname"]];
                        //                    NSString * dummyAnalystName= [analystName stringByReplacingOccurrencesOfString:@" " withString:@""];
                        //                    dummyAnalystName=[dummyAnalystName lowercaseString];
                        //                    cell.profileName.text=analystName;
                        //                    NSString * firstName = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"]objectForKey:@"firstname"]];
                        //                    NSString * lastname = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"]objectForKey:@"lastname"]];
                        //
                        //                    NSString * dummyName;
                        //                    if([lastname isEqual:[NSNull null]]||[lastname containsString:@"<null>"])
                        //                    {
                        //                        lastname=@"";
                        //                        NSString * name = [NSString stringWithFormat:@"%@ %@",firstName,lastname];
                        //                        dummyName = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
                        //                        dummyName=[dummyName lowercaseString];
                        //                        cell.analystName.text=name;
                        //                    }else
                        //                    {
                        //                        NSString * name = [NSString stringWithFormat:@"%@ %@",firstName,lastname];
                        //                        dummyName = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
                        //                        dummyName=[dummyName lowercaseString];
                        //                        cell.analystName.text = name;
                        //                    }
                        //                    if([dummyAnalystName containsString:dummyName])
                        //                    {
                        //                        cell.analystName.hidden=YES;
                        //                        cell.analystNameHeightConstant.constant=0;
                        //                    }else
                        //                    {
                        //                        cell.analystName.hidden=NO;
                        //                        cell.analystNameHeightConstant.constant=21;
                        //                        //                    cell.profileName.text = analystName;
                        //                    }
                        //
                        
                        
                        
                    }
                    
                    else if(publicInt==0)
                    {
                        cell.premiumImgView.hidden=YES;
                        
                        NSString * sub=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"open_advice"] objectForKey:@"subscriptiontypeid"]];
                        
                        int subInt=[sub intValue];
                        
                        if(subInt==1)
                        {
                            cell.premiumImgView.hidden=YES;
                            
                        }
                        
                        else if(subInt==2)
                        {
                            cell.premiumImgView.hidden=NO;
                            
                            cell.premiumImgView.image=[UIImage imageNamed:@"premiumNew"];
                            
                        }
                        
                        //                    cell.sourceLabel.hidden=YES;
                        //                    cell.sourceDataLabel.hidden=YES;
                        //                    cell.profileName.text=[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"firstname"];
                        //                    cell.analystName.hidden=YES;
                        //                    cell.analystNameHeightConstant.constant=0;
                        
                    }
                    
                    
                    
                    
                    
                    
                   
                    
                    return cell;
                }
            }
        }
        
        
        
    }
    else
    {
        NoAdvicesProfileTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"noprofile" forIndexPath:indexPath];
        return cell;
    }
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

    
}

-(void)buySellAction:(UIButton *)sender
{
    @try
    {
    delegate1.navigationCheck=@"wisdom";
    
    delegate1.wisdomCheck=true;
    
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.profileTableView];
    
    
    
    NSIndexPath *indexPath = [self.profileTableView indexPathForRowAtPoint:buttonPosition];
    //NSLog(@"Index path:%ld",(long)indexPath.row);
    NewProfileTableViewCell *cell = [self.profileTableView cellForRowAtIndexPath:indexPath];
    NSDictionary * contra=[[[self.recentAdvicesDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"contra_advice"];
    NSDictionary * openAdvice=[[[self.recentAdvicesDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"open_advice"];
    NSString * contraStr=[NSString stringWithFormat:@"%@",[[[self.recentAdvicesDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"contra_advice"]];
    NSDictionary * localDict=[[NSDictionary alloc]init];
    if(contra && openAdvice && ![contraStr isEqualToString:@"<null>"])
    {
        localDict=[[[self.recentAdvicesDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"contra_advice"];
    }
    else if(openAdvice)
    {
        localDict=[[[self.recentAdvicesDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"open_advice"];
    }
    if(localDict)
    {
    delegate1.wisdomGardemTickIDString = [localDict objectForKey:@"messageid"];
    
    NSString * messagetypeid = [NSString stringWithFormat:@"%@",[localDict objectForKey:@"messagetypeid"]];
    
    int messageInt = [messagetypeid intValue];
    
    if(messageInt==1||messageInt==6)
    {
        
        
        @try {
            delegate1.symbolDepthStr=[localDict objectForKey:@"companyname"];
             delegate1.tradingSecurityDes=[localDict objectForKey:@"symbolname"];
            
            
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
        
        @try {
            delegate1.orderSegment=[localDict objectForKey:@"segmenttype"];
            
            
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
        
        
        @try {
            delegate1.orderBuySell=cell.buySellButton.currentTitle;
           
            
            if([delegate1.brokerNameStr isEqualToString:@"Zerodha"])
            {
                delegate1.orderinstrument=[localDict objectForKey:@"instrumentid"];
            }
            
            else
            {
                delegate1.orderinstrument=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"exchangetoken"]];
            
                
                
            }
            
            
            
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            
        }
       
        
    }
    
    if(messageInt==1||messageInt==6)
    {
        if(delegate1.leaderAdviceDetails.count>0)
        {
            [delegate1.leaderAdviceDetails removeAllObjects];
        }
        //    if(tableView==self.equitiesTableView&&segmentedControl.selectedSegmentIndex==0)
        //    {
        //
        //        if(indexPath.row==2||indexPath.row==7||indexPath.row==15||indexPath.row==12||indexPath.row==18)
        //        {
        //            StockAllocationView * allocation=[self.storyboard instantiateViewControllerWithIdentifier:@"stockallocation"];
        //
        //            [self.navigationController pushViewController:allocation animated:YES];
        //
        //        }
        
        //        else{
        delegate1.depth=@"WISDOM";
        
        //NSLog(@"%@",delegate1.depth);
        
        
        
        //        delegate1.symbolDepthStr=[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"companyname"];
        //
        //        delegate1.exchaneLblStr=[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"segmenttype"];
        
        NSString * firstName=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"firstname"]];
        NSString * lastName=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"lastname"]];
        
        if([firstName isEqualToString:@"<null>"])
        {
            firstName=@"";
        }
        
        if([lastName isEqualToString:@"<null>"])
        {
            lastName=@"";
        }
        NSString * name=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
        
        [delegate1.leaderAdviceDetails addObject:name];
        [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"createdon"]];
        [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"lasttradedprice"]];
        
        [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"changepercent"]];
        
        [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"message"]];
        
        NSString * logo=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"logourl"]];
        
        if([logo isEqual:[NSNull null]])
        {
            [delegate1.leaderAdviceDetails addObject:@"no"];
        }
        
        else
        {
            [delegate1.leaderAdviceDetails addObject:logo];
        }
        
        
        //NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
        
    }
    
    
    StockOrderView * orderView=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
    
    [self.navigationController pushViewController:orderView animated:YES];
        
    }
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

   
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
    delegate1.navigationCheck=@"wisdom";
    if([[self.recentAdvicesDictionary objectForKey:@"data"] count])
    {
        NSDictionary * contra=[[[self.recentAdvicesDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"contra_advice"];
        NSDictionary * openAdvice=[[[self.recentAdvicesDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"open_advice"];
        NSString * contraStr=[NSString stringWithFormat:@"%@",[[[self.recentAdvicesDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"contra_advice"]];
        NSDictionary * localDict=[[NSDictionary alloc]init];
        if(contra && openAdvice && ![contraStr isEqualToString:@"<null>"])
        {
            localDict=[[[self.recentAdvicesDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"contra_advice"];
        }
        else if(openAdvice)
        {
            localDict=[[[self.recentAdvicesDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"open_advice"];
        }
        if(localDict)
        {
        NSString * messagetypeid = [NSString stringWithFormat:@"%@",[localDict objectForKey:@"messagetypeid"]];
        
        int messageInt = [messagetypeid intValue];
   if(messageInt==1||messageInt==6)
    {
        if(delegate1.leaderAdviceDetails.count>0)
        {
            [delegate1.leaderAdviceDetails removeAllObjects];
        }
        //    if(tableView==self.equitiesTableView&&segmentedControl.selectedSegmentIndex==0)
        //    {
        //
        //        if(indexPath.row==2||indexPath.row==7||indexPath.row==15||indexPath.row==12||indexPath.row==18)
        //        {
        //            StockAllocationView * allocation=[self.storyboard instantiateViewControllerWithIdentifier:@"stockallocation"];
        //
        //            [self.navigationController pushViewController:allocation animated:YES];
        //
        //        }
        
        //        else{
        delegate1.depth=@"WISDOM";
        
        //NSLog(@"%@",delegate1.depth);
        
        StockView1 * stockDetails=[self.storyboard instantiateViewControllerWithIdentifier:@"stock1"];
        
        
        
        delegate1.symbolDepthStr=[localDict objectForKey:@"companyname"];
        
        delegate1.tradingSecurityDes=[localDict objectForKey:@"symbolname"];
        
        delegate1.exchaneLblStr=[localDict objectForKey:@"segmenttype"];
        
        delegate1.instrumentDepthStr=[localDict objectForKey:@"instrumentid"];
        
//        delegate1.expirySeriesValue=[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"expirydate"];
        
        delegate1.expirySeriesValue=@"";
        
        NSString * firstName=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"firstname"]];
        NSString * lastName=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"lastname"]];
        
        if([firstName isEqualToString:@"<null>"])
        {
            firstName=@"";
        }
        
        if([lastName isEqualToString:@"<null>"])
        {
            lastName=@"";
        }
        NSString * name=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
        
        [delegate1.leaderAdviceDetails addObject:name];
        [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"createdon"]];
        [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"lasttradedprice"]];
        
        [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"changepercent"]];
        
        [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"message"]];
        
        
        [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"leaderid"]];
        [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"messageid"]];
        
        [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"buysell"]];
        NSString * logo=[localDict objectForKey:@"logourl"];
        
        if([logo isEqual:[NSNull null]])
        {
            [delegate1.leaderAdviceDetails addObject:@"no"];
        }
        
        else
        {
            [delegate1.leaderAdviceDetails addObject:logo];
        }
        

        
        //NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
        
        [self.navigationController pushViewController:stockDetails animated:YES];
        
    }
    }
        
    }
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([[self.recentAdvicesDictionary objectForKey:@"data"] count]>0)
    {
       
        filterAdvice=[self.recentAdvicesDictionary objectForKey:@"data"];
//        NSArray * adviceCount=[[filterAdvice objectAtIndex:indexPath.row]allKeys];
        
        int messageType=[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"messagetypeid"] intValue];
        
        if(messageType!=4)
            
        {
            NSDictionary * contra=[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"];
            NSDictionary * openAdvice=[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"];
            NSString * contraStr=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"]];
            
            if(contra && openAdvice && ![contraStr isEqualToString:@"<null>"])
            
            {
                return 330;
            }
            else if(openAdvice)
            {
                return 177;
            }
            else
            {
                return 187;
            }
        }
    }
    else
    {
        return 500;
    }
    
    return 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// New Date and Time

-(NSString*)remaningTime:(NSDate*)startDate endDate:(NSDate*)endDate
{
    @try
    {
    NSDateComponents *components;
    NSInteger days;
    NSInteger hour;
    NSInteger minutes;
    NSString *durationString;
    
    components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate: startDate toDate: endDate options: 0];
    
    days = [components day];
    hour = [components hour];
    minutes = [components minute];
    
    if(days>0)
    {
        if(days>1)
            durationString=[NSString stringWithFormat:@"%ld days",(long)days];
        else
            durationString=[NSString stringWithFormat:@"%ld day",(long)days];
        return durationString;
    }
    if(hour>0)
    {
        if(hour>1)
            durationString=[NSString stringWithFormat:@"%ld hrs",(long)hour];
        else
            durationString=[NSString stringWithFormat:@"%ld hrs",(long)hour];
        return durationString;
    }
    if(minutes>0)
    {
        if(minutes>1)
            durationString = [NSString stringWithFormat:@"%ld min",(long)minutes];
        else
            durationString = [NSString stringWithFormat:@"%ld min",(long)minutes];
        
        return durationString;
    }
    return @"";
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

        
}




@end
