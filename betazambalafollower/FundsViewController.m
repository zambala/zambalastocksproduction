//
//  FundsViewController.m
//  Funds
//
//  Created by Zenwise Technologies on 27/11/17.
//  Copyright © 2017 Zenwise Technologies. All rights reserved.
//

#import "FundsViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "FundTransferView.h"
#import "TagEncode.h"
#import "MTFunds.h"
#import "MT.h"
#import "BrokerViewNavigation.h"
#import <Mixpanel/Mixpanel.h>

@interface FundsViewController ()<NSStreamDelegate>
{
    
    AppDelegate * delegate1;
    MTFunds * sharedManagerFunds;
    MT * sharedManager;
    
}

@end

@implementation FundsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"funds_page"];
    sharedManagerFunds = [MTFunds Mt9];
    sharedManager = [MT Mt1];
      delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.payButton.hidden=YES;
    self.derivativesPayoutButton.hidden=YES;
    
    self.fundsView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.fundsView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.fundsView.layer.shadowOpacity = 1.0f;
    self.fundsView.layer.shadowRadius = 1.0f;
    self.fundsView.layer.cornerRadius=1.0f;
    self.fundsView.layer.masksToBounds = NO;
    
    
    self.payButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.payButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.payButton.layer.shadowOpacity = 1.0f;
    self.payButton.layer.shadowRadius = 1.0f;
    self.payButton.layer.cornerRadius=1.0f;
    self.payButton.layer.masksToBounds = NO;
    
    if([delegate1.brokerNameStr isEqualToString:@"Zerodha"]||[delegate1.brokerNameStr isEqualToString:@"Upstox"])
    {
        self.eqLabel1.text=@"NET CASH AVAILABLE";
        self.eqLabel2.text=@"AMOUNT TRANSFERED TODAY";
        self.eqLabel3.text=@"TOTAL ACCOUNT CASH";
        self.eqLabel4.text=@"TOTAL CASH USED TODAY";
        self.drLabel1.text=@"NET CASH AVAILABLE";
        self.drLabel2.text=@"AMOUNT TRANSFERED TODAY";
        self.drLabel3.text=@"TOTAL ACCOUNT CASH";
        self.drLabel4.text=@"TOTAL CASH USED TODAY";
    }else
    {
        self.eqLabel1.text=@"LEDGER BALANCE";
        self.eqLabel2.text=@"SECURITY VALUE";
        self.eqLabel3.text=@"TOTAL BUYING POWER";
        self.eqLabel4.text=@"TOTAL CASH USED TODAY";
        self.drLabel1.text=@"EXPOSURE MARGIN";
        self.drLabel2.text=@"SPAN MARGIN";
        self.drLabel3.text=@"TOTAL F&O MARGIN";
        self.drLabel4.text=@"TOTAL CASH USED TODAY";
        
    }
    
   
    
    [self.payButton addTarget:self action:@selector(onPayButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.payInButton addTarget:self action:@selector(onPayinButtontap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}
-(void)loginCheck
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Please choose an option." message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [self presentViewController:alert animated:YES completion:^{
        
    }];
    
    UIAlertAction * Login=[UIAlertAction actionWithTitle:@"Login" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //Navigate to login page
        NSUserDefaults *loggedInUserNew  = [NSUserDefaults standardUserDefaults];
        //
        delegate1.loginActivityStr=@"";
        //             delegate1.loginActivityStr=@"";
        [loggedInUserNew removeObjectForKey:@"loginActivityStr"];
        
        
        
        BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
        
        [self presentViewController:view animated:YES completion:nil];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addAction:Login];
    [alert addAction:cancel];
    
}


-(void)viewDidAppear:(BOOL)animated
{
    if([delegate1.loginActivityStr isEqualToString:@"OTP1"])
    {
        [self loginCheck];
    }
    else
    {
        if([delegate1.brokerNameStr isEqualToString:@"Zerodha"])
        {
            [self zerodhaFundsServer];
        }else if([delegate1.brokerNameStr isEqualToString:@"Upstox"])
        {
            [self upStoxSever];
        }
        
        else
        {
            [self mtFunds];
            
        }
    }
    
}
-(void)mtFunds
{
@try
    {
    TagEncode * enocde=[[TagEncode alloc]init];
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    enocde.inputRequestString=@"funds";
   
    [inputArray addObject:delegate1.accessToken];
    [inputArray addObject:delegate1.userID];
    [inputArray addObject:[NSString stringWithFormat:@"%@multitradeapi/funds",delegate1.mtBaseUrl]];
    
    [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        NSArray * keysArray=[dict allKeys];
        if(keysArray.count>0)
        {
            [self multitradeFunds:dict];
        }
    }];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)onPayinButtontap
{
    @try
    {
    if([delegate1.brokerNameStr isEqualToString:@"Zerodha"]||[delegate1.brokerNameStr isEqualToString:@"Proficient"])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Oops!"
                                         message:@"Payin is not available with your broker now. It will be available soon."
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            //Add Buttons
            
            UIAlertAction* okButton = [UIAlertAction
                                       actionWithTitle:@"Ok"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           //Handle your yes please button action here
                                           
                                           
                                       }];
            //Add your buttons to alert controller
            
            [alert addAction:okButton];
            
            
            [self presentViewController:alert animated:YES completion:nil];
        });
        
    }else
    {
        FundTransferView * fundsTransfer = [self.storyboard instantiateViewControllerWithIdentifier:@"FundTransferView"];
        [self presentViewController:fundsTransfer animated:YES completion:nil];
    }
    
//ViewController * view=[self.storyboard instantiateViewControllerWithIdentifier:@"view1"];
//    view.payInCheck=@"payin";
//[self.navigationController pushViewController:view animated:YES];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

}

-(void)multitradeFunds:(NSDictionary *)dict
{
    @try
    {
           dispatch_async(dispatch_get_main_queue(), ^{
    //NSLog(@"Funds:%@",delegate1.mtFundsArray);
    
    NSString * netCash = [NSString stringWithFormat:@"%.2f",[[[[[dict objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"funds"]objectForKey:@"dbBalance"]floatValue]];
    self.netCashAvailable.text = netCash;
    
    NSString * amountToday = [NSString stringWithFormat:@"%.2f", [[[[[dict objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"funds"]objectForKey:@"dbHaircutBalance"]floatValue]];
    self.amountTodayLabel.text = amountToday;
    
    float netcashint = [netCash floatValue];
    float amounttodayint = [amountToday floatValue];
    float totalInt = netcashint + amounttodayint;
    
    NSString * totalAvailableCash  = [NSString stringWithFormat:@"%.2f",totalInt];
    self.accontCashLabel.text = totalAvailableCash;
    
    NSString * cashUsedToday = [NSString stringWithFormat:@"%.2f",[[[[[dict objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"funds"]objectForKey:@"dbCCEUtilize"]floatValue]];
    self.cashTodayLabel.text = cashUsedToday;
   
    
    NSString * exposureMargin = [NSString stringWithFormat:@"%.2f", [[[[[dict objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"funds"]objectForKey:@"dbExposureMargin"]floatValue]];
    self.drCashAvailableLabel.text = exposureMargin;
    
    NSString * spanMargin = [NSString stringWithFormat:@"%.2f", [[[[[dict objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"funds"]objectForKey:@"dbSpanMargin"]floatValue]];
    self.drAmountTodayLabel.text = spanMargin;
    
    float exposureFloat = [exposureMargin floatValue];
    float spanMarginFloat = [spanMargin floatValue];
    float totalFandOMargin = exposureFloat + spanMarginFloat;
    NSString * fandoMargin = [NSString stringWithFormat:@"%.2f",totalFandOMargin];
    self.drAccountCashLabel.text = fandoMargin;
    
//    NSString * usedToday = [NSString stringWithFormat:@"%.2f",[[[delegate1.mtFundsArray objectAtIndex:0]objectForKey:@"dbExposureMargin"]floatValue]];
    self.drUsedTodayLabel.text = @"--";
    
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    
    [mixpanelMini identify:delegate1.userID];
    
    
    
    [mixpanelMini.people set:@{@"TOTAL_ACCOUNT_CASH":totalAvailableCash}];
    [mixpanelMini.people set:@{@"MARGIN_USED":cashUsedToday}];
    [mixpanelMini.people set:@{@"AMOUNT_ADDED_TODAY":amountToday}];
    [mixpanelMini.people set:@{@"NET_CASH_AVAILABLE":netCash}];
           });
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)upStoxSever
{
    @try
    {
    NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate1.accessToken];
    NSDictionary *headers = @{ @"x-api-key": delegate1.APIKey,
                               @"authorization": access,
                               };
    NSString * urlStr=[NSString stringWithFormat:@"https://api.upstox.com/live/profile/balance"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:40.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if(data !=nil)
                                                    {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        self.upstoxResponseDicitionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        //NSLog(@"upstox balance%@",self.upstoxResponseDicitionary);
                                                    }      
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        self.netCashAvailable.text=[NSString stringWithFormat:@"%@",[[[self.upstoxResponseDicitionary objectForKey:@"data"]objectForKey:@"equity"] objectForKey:@"available_margin"]];
                                                        self.amountTodayLabel.text= [NSString stringWithFormat:@"%@",[[[self.upstoxResponseDicitionary objectForKey:@"data"]objectForKey:@"equity"] objectForKey:@"payin_amount"]];
                                                        self.accontCashLabel.text=[NSString stringWithFormat:@"%@",[[[self.upstoxResponseDicitionary objectForKey:@"data"]objectForKey:@"equity"] objectForKey:@"available_margin"]];
                                                        self.cashTodayLabel.text=[NSString stringWithFormat:@"%@",[[[self.upstoxResponseDicitionary objectForKey:@"data"]objectForKey:@"equity"] objectForKey:@"used_margin"]];
                                                        
                                                        Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
                                                        
                                                        [mixpanelMini identify:delegate1.userID];
                                                        
                                                        
                                                        
                                                        [mixpanelMini.people set:@{@"TOTAL_ACCOUNT_CASH":self.accontCashLabel.text}];
                                                        [mixpanelMini.people set:@{@"MARGIN_USED":self.cashTodayLabel.text}];
                                                        [mixpanelMini.people set:@{@"AMOUNT_ADDED_TODAY":self.amountTodayLabel.text}];
                                                        [mixpanelMini.people set:@{@"NET_CASH_AVAILABLE":self.netCashAvailable.text}];
                                                    });
                                                    }
                                                    else
                                                    {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            
                                                            UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                [self upStoxSever];
                                                            }];
                                                            
                                                            UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                
                                                            }];
                                                            
                                                            
                                                            [alert addAction:retryAction];
                                                            [alert addAction:cancel];
                                                            
                                                        });
                                                        
                                                    }
                                                }];
    [dataTask resume];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

}

-(void)zerodhaFundsServer
{
    @try
    {
    NSString * auth=[NSString stringWithFormat:@"token %@:%@",delegate1.APIKey,delegate1.accessToken];
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               @"X-Kite-Version": delegate1.kiteVersion,
                               @"Authorization": auth
                               };
//    NSString * urlStr=[NSString stringWithFormat:@"https://api.kite.trade/user/margins/equity?api_key=%@&access_token=%@",delegate1.APIKey,delegate1.accessToken];
        NSString * urlStr=[NSString stringWithFormat:@"https://api.kite.trade/user/margins"];
   

    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if(data!=nil)
                                                    {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        self.zerodhaResponseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        
                                                        self.netCashAvailable.text=[NSString stringWithFormat:@"%@",[[[self.zerodhaResponseDictionary objectForKey:@"data"]objectForKey:@"equity"] objectForKey:@"net"]];
                                                        self.amountTodayLabel.text= [NSString stringWithFormat:@"%@",[[[[self.zerodhaResponseDictionary objectForKey:@"data"]objectForKey:@"equity"]objectForKey:@"available"]objectForKey:@"intraday_payin"]];
                                                        self.accontCashLabel.text=[NSString stringWithFormat:@"%@",[[[[self.zerodhaResponseDictionary objectForKey:@"data"]objectForKey:@"equity"]objectForKey:@"available"]objectForKey:@"cash"]];
                                                        self.cashTodayLabel.text=[NSString stringWithFormat:@"%@",[[[[self.zerodhaResponseDictionary objectForKey:@"data"]objectForKey:@"equity"]objectForKey:@"utilised"]objectForKey:@"debits"]];
                                                        
                                                        self.drCashAvailableLabel.text=[NSString stringWithFormat:@"%@",[[[self.zerodhaResponseDictionary objectForKey:@"data"]objectForKey:@"commodity"] objectForKey:@"net"]];
                                                        self.drAmountTodayLabel.text= [NSString stringWithFormat:@"%@",[[[[self.zerodhaResponseDictionary objectForKey:@"data"]objectForKey:@"commodity"]objectForKey:@"available"]objectForKey:@"intraday_payin"]];
                                                        self.drAccountCashLabel.text=[NSString stringWithFormat:@"%@",[[[[self.zerodhaResponseDictionary objectForKey:@"data"]objectForKey:@"commodity"]objectForKey:@"available"]objectForKey:@"cash"]];
                                                        self.drUsedTodayLabel.text=[NSString stringWithFormat:@"%@",[[[[self.zerodhaResponseDictionary objectForKey:@"data"]objectForKey:@"commodity"]objectForKey:@"utilised"]objectForKey:@"debits"]];
                                                        
                                                        
                                                        
                                                        Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
                                                        
                                                        [mixpanelMini identify:delegate1.userID];
                                                        
                                                      
                                        
                                                        
                                                        [mixpanelMini.people set:@{@"TOTAL_ACCOUNT_CASH":self.accontCashLabel.text}];
                                                        [mixpanelMini.people set:@{@"MARGIN_USED":self.cashTodayLabel.text}];
                                                        [mixpanelMini.people set:@{@"AMOUNT_ADDED_TODAY":self.amountTodayLabel.text}];
                                                        [mixpanelMini.people set:@{@"NET_CASH_AVAILABLE":self.netCashAvailable.text}];
                                                        
                                                        
                                                        
                                                        
                                                    });
                                                    }
                                                    else
                                                    {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            
                                                            UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                [self zerodhaFundsServer];
                                                            }];
                                                            UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                               
                                                            }];
                                                            
                                                            [alert addAction:retryAction];
                                                            [alert addAction:cancel];
                                                            
                                                        });
                                                    }
                                                }];
    [dataTask resume];
   
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

}
-(void)onPayButtonTap
{
//    FundTransferView * payin = [self.storyboard instantiateViewControllerWithIdentifier:@"FundTransferView"];
//    //[self.navigationController pushViewController:payin animated:YES];
//    [self.navigationController presentViewController:payin animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
