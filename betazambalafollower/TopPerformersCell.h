//
//  TopPerformersCell.h
//  betazambalafollower
//
//  Created by Zenwise Technologies on 15/06/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TopPerformersCell;

@protocol TopPerformersCellDelegate <NSObject>

@end

@interface TopPerformersCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *symbolName;
@property (weak, nonatomic) IBOutlet UILabel *companyLbl;

@property (weak, nonatomic) IBOutlet UILabel *avgTp;
@property (weak, nonatomic) IBOutlet UILabel *profitPotentialLbl;
@property (weak, nonatomic) IBOutlet UILabel *ltpLbl;
@property (weak, nonatomic) IBOutlet UIButton *tradeBtn;
@property (weak, nonatomic) IBOutlet UIImageView *consensusLabel;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIImageView *arrowButton;

@property (weak, nonatomic) IBOutlet UIButton *moreInfoBtn;
@property (weak, nonatomic) IBOutlet UILabel *noOfAdvicesLbl;
@property (weak, nonatomic) id<TopPerformersCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *arrowButtonWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *arrowButtonHeight;

+(NSString*)cellIdentifier;
@end
