//
//  VoucherPopUpViewController.h
//  betazambalafollower
//
//  Created by Zenwise Technologies Private Limited on 25/08/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VoucherPopUpViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *voucherNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *expiryDateLabel;
@property (weak, nonatomic) IBOutlet UIButton *tandCButton;
@property (weak, nonatomic) IBOutlet UIButton *cupyButton;
@property (weak, nonatomic) IBOutlet UILabel *voucherCodeLabel;
@property (weak, nonatomic) IBOutlet UIButton *okayButton;

@property NSString * voucherCompanyName;
@property NSString * voucherExpiry;
@property NSString * voucherCode;
@property NSString * voucherTerms;
@property (weak, nonatomic) IBOutlet UIButton *onCopyButtonTap;

@end
