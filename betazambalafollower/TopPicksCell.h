//
//  TopPicksCell.h
//  PayPage
//
//  Created by zenwise mac 2 on 12/22/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopPicksCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *valueChangeLabel;
@property (weak, nonatomic) IBOutlet UILabel *oLabel;
@property (weak, nonatomic) IBOutlet UILabel *stLabel;
@property (strong, nonatomic) IBOutlet UILabel *profileNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateAndTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *followerCountLabel;
@property (strong, nonatomic) IBOutlet UILabel *LTPLabel;
@property (strong, nonatomic) IBOutlet UILabel *changePercentLabel;
@property (strong, nonatomic) IBOutlet UILabel *actedByLabel;
@property (strong, nonatomic) IBOutlet UILabel *sharesSoldLabel;
@property (strong, nonatomic) IBOutlet UILabel *averageProfitLabel;
@property (strong, nonatomic) IBOutlet UIButton *buySellButton;
@property (strong, nonatomic) IBOutlet UILabel *sharesChangeLabel;
@property (strong, nonatomic) IBOutlet UILabel *gainPercent;
@property (strong, nonatomic) IBOutlet UILabel *followerCount;
@property (strong, nonatomic) IBOutlet UIButton *followBtn;
@property (strong, nonatomic) IBOutlet UIButton *topPicksProfileButton;
- (IBAction)onTopPicksProfileButtonTap:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *profileImgView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *expertsXConstraint;
@property (weak, nonatomic) IBOutlet UILabel *premiumLabel;
@property (weak, nonatomic) IBOutlet UIImageView *premiumImageView;

@property (weak, nonatomic) IBOutlet UILabel *sourceLabel;
@property (weak, nonatomic) IBOutlet UILabel *sorceDataLabel;
@property (weak, nonatomic) IBOutlet UIButton *adviceType;
@property (weak, nonatomic) IBOutlet UILabel *organizationNameLbl;


@property (weak, nonatomic) IBOutlet UILabel *durationLbl;
@end
