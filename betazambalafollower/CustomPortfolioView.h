//
//  CustomPortfolioView.h
//  betazambalafollower
//
//  Created by Zenwise Technologies on 13/06/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomPortfolioView : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UISegmentedControl *exchangeSegment;
- (IBAction)exchangeSegmentAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *qtyFld;
@property (weak, nonatomic) IBOutlet UITextField *avrgPriceFld;
@property (weak, nonatomic) IBOutlet UIButton *addToPortfolioAction;
- (IBAction)onPortfolioTap:(id)sender;
@property NSMutableArray * symbols;
@property NSMutableArray * company;
@property NSString * searchStr;
@property (weak, nonatomic) IBOutlet UIButton *addToPortfolioBtn;
@property (weak, nonatomic) IBOutlet UITextField *symbolSearch;
@property NSMutableArray * responseArray;
@property NSDictionary * detailsDict;
@end
