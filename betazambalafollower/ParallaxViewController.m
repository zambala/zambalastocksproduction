//
//  ParallaxViewController.m
//  ParralaxDetailView
//
//  Created by apple on 29/04/16.
//  Copyright © 2016 ClickApps. All rights reserved.
//

#import "ParallaxViewController.h"

#define HEADER_IMAGE_HEIGHT  300;

@interface ParallaxViewController ()<UIScrollViewDelegate>


/**
 @property bottomScroll
 @description UIScrollView place at bottom of View holding labels and text and other controls one want to place on it
 */
@property(nonatomic, weak) IBOutlet UIScrollView *bottomScroll;

/**
 @property topScroll
 @description UIScrollView place at top of View holding post image
 */
@property(nonatomic, weak) IBOutlet UIScrollView *topScroll;
/**
 @property scrollDirectionValue
 @description holding value to determine scroll direction
 */
@property(nonatomic, assign) float scrollDirectionValue;

/**
 @property yoffset
 @description set scroll contentoffset based on this offest value
 */
@property(nonatomic, assign) float yoffset;


/**
 @property alphaValue
 @description alpha to  fade in fade out nav color
 */
@property(nonatomic, assign) CGFloat alphaValue;
/**
 @property bottomViewTopConstraint
 @description constraint for aligning bottom view as per our post imageview height
 */
@property(nonatomic, weak) IBOutlet NSLayoutConstraint *bottomViewTopConstraint;



@end

@implementation ParallaxViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    


  
   // self.contentViewHeight.constant = self.contentViewHeight.constant +100;
    
    UIView *view = [[[NSBundle mainBundle]loadNibNamed:@"ParallaxViewController" owner:self options:nil] objectAtIndex:0];
    view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    [self.view insertSubview:view atIndex:0];

    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    self.bottomScroll.delegate = self;
    self.headerImageViewHeight.constant = HEADER_IMAGE_HEIGHT;
    self.bottomViewTopConstraint.constant = self.headerImageViewHeight.constant;
    self.contentViewHeight.constant = ([UIScreen mainScreen].bounds.size.height + 900) - HEADER_IMAGE_HEIGHT;
    [self.view layoutIfNeeded];
    
    starOneImg = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, 15, 15)];
    [starOneImg setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
    [starOneImg setImage:[UIImage imageNamed:@"star_sel.png"] forState:UIControlStateSelected];
    starOneImg.selected = NO;
    [starOneImg addTarget: self action: @selector(starOneImg) forControlEvents:UIControlEventTouchUpInside];
    [self.starView addSubview: starOneImg];
    
    starTwoImg = [[UIButton alloc] initWithFrame:CGRectMake(30, 0, 15, 15)];
    [starTwoImg setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
    [starTwoImg setImage:[UIImage imageNamed:@"star_sel.png"] forState:UIControlStateSelected];
    starTwoImg.selected = NO;
    [starTwoImg addTarget: self action: @selector(starTwoImg) forControlEvents:UIControlEventTouchUpInside];
    [self.starView addSubview: starTwoImg];
    
    starThreeImg = [[UIButton alloc] initWithFrame:CGRectMake(50, 0, 15, 15)];
    [starThreeImg setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
    [starThreeImg setImage:[UIImage imageNamed:@"star_sel.png"] forState:UIControlStateSelected];
    starThreeImg.selected = NO;
    [starThreeImg addTarget: self action: @selector(starThreeImg) forControlEvents:UIControlEventTouchUpInside];
    [self.starView addSubview: starThreeImg];
    
    starFourImg = [[UIButton alloc] initWithFrame:CGRectMake(70, 0, 15, 15)];
    [starFourImg setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
    [starFourImg setImage:[UIImage imageNamed:@"star_sel.png"] forState:UIControlStateSelected];
    starFourImg.selected = NO;
    [starFourImg addTarget: self action: @selector(starFourImg) forControlEvents:UIControlEventTouchUpInside];
    [self.starView addSubview: starFourImg];
    
    starFiveImg = [[UIButton alloc] initWithFrame:CGRectMake(90, 0, 15, 15)];
    [starFiveImg setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
    [starFiveImg setImage:[UIImage imageNamed:@"star_sel.png"] forState:UIControlStateSelected];
    starFiveImg.selected = NO;
    [starFiveImg addTarget: self action: @selector(starFiveImg) forControlEvents:UIControlEventTouchUpInside];
    [self.starView addSubview: starFiveImg];
    
    

   

}

-(void)starOneImg
{
    
    if (starOneFlag==false)
    {
        starOneImg.selected = YES;
        
        starTwoImg.selected = NO;
        starThreeImg.selected = NO;
        starFourImg.selected = NO;
        starFiveImg.selected = NO;
        starOneFlag = true;
    }
    else
    {
        starOneImg.selected = NO;
        starTwoImg.selected = NO;
        starThreeImg.selected = NO;
        starFourImg.selected = NO;
        starFiveImg.selected = NO;
        starOneFlag = false;
        
    }
}
-(void)starTwoImg
{
    starOneImg.selected = YES;
    starTwoImg.selected = YES;
    starThreeImg.selected = NO;
    starFourImg.selected = NO;
    starFiveImg.selected = NO;
    
}

-(void)starThreeImg
{
    starOneImg.selected = YES;
    starTwoImg.selected = YES;
    starThreeImg.selected = YES;
    starFourImg.selected = NO;
    starFiveImg.selected = NO;
    
}

-(void)starFourImg
{
    
    starOneImg.selected = YES;
    starTwoImg.selected = YES;
    starThreeImg.selected = YES;
    starFourImg.selected = YES;
    starFiveImg.selected = NO;
    
}

-(void)starFiveImg
{
    
    
    starOneImg.selected = YES;
    starTwoImg.selected = YES;
    starThreeImg.selected = YES;
    starFourImg.selected = YES;
    starFiveImg.selected = YES;
    
    
}

-(void)adjustContentViewHeight{
    self.bottomViewTopConstraint.constant = self.headerImageViewHeight.constant;
    self.contentViewHeight.constant = ([UIScreen mainScreen].bounds.size.height + 900) - self.headerImageViewHeight.constant;
    //NSLog(@"%f",self.contentViewHeight.constant);
    [self.view layoutIfNeeded];
                                                                                           
}

- (IBAction)asx:(id)sender {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    
    CGFloat offset=scrollView.contentOffset.y;
    CGFloat percentage=offset/self.headerImageViewHeight.constant;
    CGFloat value=self.headerImageViewHeight.constant*percentage; // negative when scrolling up more than the top
    
    /* if (value > scrollDirectionValue || value == scrollDirectionValue) {
     //moving upward
     //  alphaValue=fabs(percentage);
     }
     else {
     // //NSLog(@"Moving Downward");
     //alphaValue=2-fabs(percentage);
     }*/
    
    self.alphaValue=fabs(percentage);
    
    
    self.scrollDirectionValue = value;
    
    
    if (percentage < 0.00) {
        [self.bottomScroll setContentOffset:CGPointMake(0, 0)];
        
    }
    else {
        
        self.yoffset = self.bottomScroll.contentOffset.y*0.6;
        [self.topScroll setContentOffset:CGPointMake(scrollView.contentOffset.x,self.yoffset) animated:NO];
        

    }
}

@end
