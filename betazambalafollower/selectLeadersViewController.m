//
//  selectLeadersViewController.m
//  
//
//  Created by zenwise technologies on 10/07/17.
//
//

#import "selectLeadersViewController.h"
#import "selectLeadersTableViewCell.h"
#import "AppDelegate.h"

@interface selectLeadersViewController ()
{
    AppDelegate * delegate;
    NSString * number;
}

@end

@implementation selectLeadersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
    
    number= [loggedInUser stringForKey:@"profilemobilenumber"];
    [self selectLeadersServer];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)selectLeadersServer
{
    
        @try {
            NSDictionary *headers = @{ @"content-type": @"application/json",
                                       @"cache-control": @"no-cache",
                                       @"authtoken":delegate.zenwiseToken,
                                       @"deviceid":delegate.currentDeviceId
                                       };
            
            NSDictionary * params;
            
            @try {
                
                params=@{@"clientid":delegate.userID,
                         @"limit":@1000,
                         @"mobilenumber":number,
                         @"brokerid":[delegate.brokerInfoDict objectForKey:@"brokerid"]
                         
                         };
                
                
            }
            @catch (NSException * e) {
                //NSLog(@"Exception: %@", e);
            }
            @finally {
                //NSLog(@"finally");
            }
            
            NSData * postData=[NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
            
            
            NSString * localUrl=[NSString stringWithFormat:@"%@follower/allleaders",delegate.baseUrl];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:localUrl]
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:10.0];
            [request setHTTPMethod:@"POST"];
            [request setAllHTTPHeaderFields:headers];
            [request setHTTPBody:postData];
            
            
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            if(data!=nil)
                                                            {
                                                                if (error) {
                                                                    //NSLog(@"%@", error);
                                                                } else {
                                                                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                    //NSLog(@"%@", httpResponse);
                                                                    if([httpResponse statusCode]==403)
                                                                    {
                                                                        
                                                                        [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                        
                                                                    }
                                                                    else if([httpResponse statusCode]==401)
                                                                    {
                                                                        
                                                                        [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                        
                                                                    }
                                                                    else
                                                                    {
                                                                        
                                                                       NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                        self.responseArray=[[NSMutableArray alloc]init];
                                                                        for(int i=0;i<[[dict objectForKey:@"results"]count];i++)
                                                                        {
                                                                            int subscribed=[[[[dict objectForKey:@"results"] objectAtIndex:i] objectForKey:@"subscribed"] intValue];
                                                                            if(subscribed==1)
                                                                            {
                                                                                [self.responseArray addObject:[[dict objectForKey:@"results"] objectAtIndex:i]];
                                                                            }
                                                                        }
                                                                    }
                                                                    
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        
                                                                        self.selectLeadersTableView.delegate=self;
                                                                        self.selectLeadersTableView.dataSource=self;
                                                                        [self.selectLeadersTableView reloadData];
                                                                        
                                                                        
                                                                    });
                                                                    
                                                                }
                                                            }
                                                            else
                                                            {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                                    
                                                                    [self presentViewController:alert animated:YES completion:^{
                                                                        
                                                                    }];
                                                                    
                                                                    
                                                                    UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                        [self selectLeadersServer];
                                                                    }];
                                                                    
                                                                    UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                        
                                                                    }];
                                                                    
                                                                    
                                                                    [alert addAction:retryAction];
                                                                    [alert addAction:cancel];
                                                                    
                                                                });
                                                                
                                                            }
                                                            
                                                        }];
            [dataTask resume];
            
            
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
        
    }
    


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 56;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
    selectLeadersTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"selectLeadersTableViewCell" forIndexPath:indexPath];
    
    [cell.leaderSelectButton addTarget:self action:@selector(onLeaderButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString * leaderName=[NSString stringWithFormat:@"  %@",[[self.responseArray objectAtIndex:indexPath.row]objectForKey:@"firstname"]];
    NSString * lastName=[NSString stringWithFormat:@"  %@",[[self.responseArray objectAtIndex:indexPath.row]objectForKey:@"lastname"]];
        if([lastName containsString:@"<null>"])
        {
              [cell.leaderSelectButton setTitle:leaderName forState:UIControlStateNormal];
        }
        else
        {
            NSString * fullName=[NSString stringWithFormat:@"%@%@",leaderName,lastName];
             [cell.leaderSelectButton setTitle:fullName forState:UIControlStateNormal];
        }
    
  
    return cell;
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.responseArray.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
-(void)onLeaderButtonTap:(UIButton *)sender
{
    @try
    {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.selectLeadersTableView];
    
    
    
    NSIndexPath *indexPath = [self.selectLeadersTableView indexPathForRowAtPoint:buttonPosition];
    selectLeadersTableViewCell *cell = [self.selectLeadersTableView cellForRowAtIndexPath:indexPath];
    if(cell.leaderSelectButton.selected==YES)
    {
    if(delegate.selectLeadersID.count<3)
    {
       // delegate.selectLeadersID = [[[self.responseArray objectAtIndex:indexPath.row] objectForKey:@"userid"] intValue];
        
        [delegate.selectLeadersID addObject:[[self.responseArray objectAtIndex:indexPath.row] objectForKey:@"userid"]];
        [delegate.selectLeadersName addObject:[[self.responseArray objectAtIndex:indexPath.row] objectForKey:@"firstname"]];
        
        cell.leaderSelectButton.selected=YES;
        
        cell.leaderSelectButton.tag=indexPath.row;
    }
        
    
    else
    {
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.selectLeadersTableView];
        
        
        
        NSIndexPath *indexPath = [self.selectLeadersTableView indexPathForRowAtPoint:buttonPosition];
        selectLeadersTableViewCell *cell = [self.selectLeadersTableView cellForRowAtIndexPath:indexPath];
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Only 3 leaders can be selected" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            cell.leaderSelectButton.selected=NO;
            
        }];
        
        [alert addAction:okAction];
        
    }
    }else if (cell.leaderSelectButton.selected==NO)
    {
//        if(delegate.selectLeadersID.count<3)
//        {
//        [delegate.selectLeadersID removeObjectAtIndex:cell.leaderSelectButton.tag];
//            [delegate.selectLeadersName removeObjectAtIndex:cell.leaderSelectButton.tag];
//        }else if (delegate.selectLeadersID.count==3)
//        {
//            [delegate.selectLeadersID removeObjectAtIndex:cell.leaderSelectButton.tag-1];
//            [delegate.selectLeadersName removeObjectAtIndex:cell.leaderSelectButton.tag-1];
//        }
        
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.selectLeadersTableView];
        NSIndexPath *indexPath = [self.selectLeadersTableView indexPathForRowAtPoint:buttonPosition];
        [delegate.selectLeadersID removeObjectAtIndex:indexPath.row];
        [delegate.selectLeadersName removeObjectAtIndex:indexPath.row];
        
//        NSArray *selectedRows = [self.selectLeadersTableView indexPathsForSelectedRows];
//        BOOL deleteSpecificRows = selectedRows.count > 0;
//        if (deleteSpecificRows)
//        {
//            // Build an NSIndexSet of all the objects to delete, so they can all be removed at once.
//            NSMutableIndexSet *indicesOfItemsToDelete = [NSMutableIndexSet new];
//            for (NSIndexPath *selectionIndex in selectedRows)
//            {
//                [indicesOfItemsToDelete addIndex:selectionIndex.row];
//
//            }
//
//            [delegate.selectLeadersID removeObjectsAtIndexes:indicesOfItemsToDelete];
//            [delegate.selectLeadersName removeObjectsAtIndexes:indicesOfItemsToDelete];
        
//            NSMutableArray * removeArray = [[NSMutableArray alloc]init];
//
//            [self.removeSymbolsArray addObjectsFromArray:[self.symbolsListArray objectsAtIndexes:indicesOfItemsToDelete]];
//
//            for (int i=0; i<self.removeSymbolsArray.count; i++) {
//                [removeArray addObject:[[self.removeSymbolsArray objectAtIndex:i]objectForKey:@"symbol"]];
//            }
        
        
    //    }
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
