//
//  TabBar.m
//  testing
//
//  Created by zenwise technologies on 22/11/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "TabBar.h"
#import "VCFloatingActionButton.h"

#import "MessageView.h"
#import "FundTransferView.h"
#import "ProfileSettings.h"
#import "TadeView.h"
#import "PortfolioView.h"
#import "MainOrdersViewController.h"
#import "PriceAlert.h"  
#import "ContactSupport.h"
#import "AppDelegate.h"
#import "NewMessages.h"
#import "NewProfileSettings.h"
#import "BrokerView.h"
#import "HelpshiftSupport.h"
#import "HelpshiftCampaigns.h"
#import "BrokerViewNavigation.h"
#import "WealthTreeView.h"
#import "FundsViewController.h"
#import "MT.h"

#import "TagEncode.h"
#import <Foundation/Foundation.h>
#import <CoreFoundation/CoreFoundation.h>
#import <Endian.h>
#import "WalletView.h"
#import "PersonalDetailsViewController.h"
@import SocketIO;



@interface TabBar ()<NSStreamDelegate,SocketEngineSpec,SocketManagerSpec,SocketEngineClient>
{
    AppDelegate * delegate1;
    NSMutableDictionary * logout;
    NSString * userDetals;
     MT *sharedManager;
}

@end

@implementation TabBar

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
  
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(logOut:)
                                                 name:@"logout" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(relogin:)
                                                 name:@"relogin" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    [self.tabBar1 setDelegate:self];
    sharedManager = [MT Mt1];
    self.tabBar1.delegate=self;
//    float X_Co = (self.view.frame.size.width - 30)/2;
//    float Y_Co;
//    CGRect screenRect = [[UIScreen mainScreen] bounds];
//    CGFloat screenHeight = screenRect.size.height;
//    if(screenHeight>736)
//    {
//
//        Y_Co = self.view.frame.size.height - 76;
//    }
//
//    else
//    {
//        Y_Co = self.view.frame.size.height - 45;
//
//    }
    
    float X_Co = (self.view.frame.size.width - 85)/2;
    float Y_Co;
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    if(screenHeight>736)
    {
        
        Y_Co = self.view.frame.size.height - 90;
    }
    
    else
    {
        Y_Co = self.view.frame.size.height - 53;
        
    }
    
    
    
    self.addButton = [[VCFloatingActionButton alloc]initWithFrame:CGRectMake(X_Co,Y_Co, 85, 50) normalImage:[UIImage imageNamed:@"more1"] andPressedImage:[UIImage imageNamed:@"icon10PlusRotated"] withScrollview:_dummyTable];
    
   //Old array
//        self.addButton.imageArray = @[@"plusMenu10",@"plusMenu9",@"plusMenu1",@"plusMenu8",@"plusMenu4",@"plusMenu5",@"walletnew",@"plusMenu3",@"plusMenu2"];
//        self.addButton.labelArray = @[@"Log Out",@"Contact Support",@"Exclusive Offers",@"Trades",@"Orders",@"Fund Transfer",@"Rewards",@"Messages",@"Profile Settings"];
    
    
    //replace plusmenu1 with campaign.
    self.addButton.imageArray = @[@"plusMenu10",@"plusMenu9",@"notification",@"plusMenu8",@"plusMenu4",@"plusMenu5",@"rewardsIcon",@"plusMenu2"];
    self.addButton.labelArray = @[@"Log Out",@"Contact Support",@"Notifications",@"Trades",@"Orders",@"Fund Transfer",@"Rewards",@"Profile Settings"];
    
//  0  @"Log Out",
//  1  @"Contact Support",
//  2  @"Campaign",
//  3  @"Trades",
//  4  @"Orders",
//  5  @"Fund Transfer",
//  6  @"Rewards",
//  7  @"Profile Settings"
    
    //  0  @"Log Out",
    //  1  @"Contact Support",
    //  2  @"Trades",
    //  3  @"Orders",
    //  4  @"Fund Transfer",
    //  5  @"Rewards",
    //  6  @"Profile Settings"
    
//        NSDictionary *optionsDictionary = @{@"plusMenu10.png":@"plusMenu10",@"plusMenu10.png":@"plusMenu9",@"plusMenu9.png":@"plusMenu10",@"plusMenu10.png":@"plusMenu10",@"plusMenu10.png":@"plusMenu10",@"plusMenu10.png":@"plusMenu10",@"plusMenu10.png":@"plusMenu10",@"linkedin-icon":@"Linked in",@"linkedin-icon":@"Linked in",@"linkedin-icon":@"Linked in",@"linkedin-icon":@"Linked in"};
//        self.addButton.menuItemSet = optionsDictionary;
    
//    delegate1.imageArray=[[NSMutableArray alloc]initWithObjects:@"plusMenu10.png",@"plusMenu9.png",@"plusMenu8.png",@"plusMenu7.png",@"plusMenu6.png",@"plusMenu5.png",@"plusMenu4.png",@"plusMenu3@2x.png",@"plusMenu2.png",@"plusMenu1.png", nil];
    
  
        
    
   
    
  
    
    self.tabBarController.tabBar.barTintColor=[UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:1];
    self.tabBarController.tabBar.tintColor=[UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:1];
    
 
   

    
    
    self.addButton.hideWhileScrolling = YES;
    self.addButton.delegate = self;
        
    
    _dummyTable.delegate = self;
    _dummyTable.dataSource = self;
    
  
    [self.view addSubview:self.addButton];
}

- (void)applicationWillEnterForeground:(NSNotification *)notification {
    [self.tabBarController setSelectedIndex:3];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 9;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    cell.textLabel.text = [NSString stringWithFormat:@"%tu",indexPath.row];
    
    
    
    cell.imageView.image =[UIImage imageNamed:[NSString stringWithFormat:@"%tu",indexPath.row]];
    
//    cell.imageView.image=[self.imageArray objectAtIndex:indexPath.row];
    return cell;
}




-(void)logOut:(NSNotification *)note
{
    
    NSUserDefaults *loggedInUserNew  = [NSUserDefaults standardUserDefaults];
    //
    delegate1.loginActivityStr=@"";
    //             delegate1.loginActivityStr=@"";
    [loggedInUserNew removeObjectForKey:@"loginActivityStr"];
    //             [prefs removeObjectForKey:@"ID"];
    //             [prefs removeObjectForKey:@"userid"];
    //             [prefs removeObjectForKey:@"brokername"];
    //
    
    [self.socketio disconnect];
    
    BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
    
    [self presentViewController:view animated:YES completion:nil];
    
    
}
-(void)relogin:(NSNotification *)note
{
    
    NSUserDefaults *loggedInUserNew  = [NSUserDefaults standardUserDefaults];
    loggedInUserNew=nil;
    [self.socketio disconnect];
     dispatch_async(dispatch_get_main_queue(), ^{
    PersonalDetailsViewController * view=[self.storyboard instantiateViewControllerWithIdentifier:@"PersonalDetailsViewController"];
    
    [self presentViewController:view animated:YES completion:nil];
    
     });
}


-(void)contactSupportMethod
{
    @try {
        
        
        
        
        
        //NSLog(@"asx");
        
        //        ContactSupport * support=[self.storyboard instantiateViewControllerWithIdentifier:@"contact"];
        //
        //        [self presentViewController:support animated:YES completion:nil];
        
        [HelpshiftCore setName:delegate1.userName andEmail:delegate1.emailId];
        
        
        userDetals=[NSString stringWithFormat:@"%@(%@)",delegate1.brokerNameStr,delegate1.userID];
        
        
        
        //        NSDictionary *metaDataWithTags = @{@"usertype": @"paid",
        //                                           @"level":@"7",
        //                                           @"score":@"12345",
        //                                           HelpshiftSupportTagsKey:@[@"feedback",@"paid user",@"v4.1"]};
        
        
        
        NSString * broker = [NSString stringWithFormat:@"%@%@",[[delegate1.brokerNameStr substringToIndex:1] lowercaseString],[delegate1.brokerNameStr substringFromIndex:1] ];
        
        //NSLog(@"%@",broker);
        
        
        [HelpshiftCore loginWithIdentifier:userDetals withName:delegate1.userName andEmail:delegate1.emailId];
        
        
        //        [HelpshiftSupport setMetadataBlock:^(void){
        //            return [NSDictionary dictionaryWithObjectsAndKeys:delegate1.userID, @"name",
        //                    delegate1.emailId, @"email",
        //                    [NSArray arrayWithObjects:broker,nil], HelpshiftSupportTagsKey, nil];
        //        }];
        //
        //
        //
        //
        //         [HelpshiftSupport showFAQs:self withConfig:nil];
        
        if(delegate1.emailId.length>0)
        {
            
        }
        else
        {
            
            delegate1.emailId=@"not available";
        }         
        
        if(delegate1.userName.length>0)
        {
            
        }
        
        else
        {
            
            
        }
        
        
        
        NSDictionary *metaDataWithTags = @{@"userid":delegate1.userID,
                                           @"emailid":delegate1.emailId,
                                           HelpshiftSupportTagsKey:@[broker],
                                           @"country:":delegate1.country,
                                           @"locality":delegate1.locality,
                                           @"name":delegate1.name,
                                           @"postalcode":delegate1.postalCode,
                                           @"sublocality":delegate1.subLocality,
                                           
                                           @"locatedAt":delegate1.locatedAt
                                           };
        
        
        
        
        
        
        
        
        
        
        [HelpshiftSupport showFAQs:self
                       withOptions:@{@"gotoConversationAfterContactUs":@"YES",@"showSearchonNewConversation":@"YES",
                                     HelpshiftSupportCustomMetadataKey: metaDataWithTags}];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)logOutMethod
{
    @try
    {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:@"Are you sure you want to logout" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        
        UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            delegate1.marketWatchBool=true;
            delegate1.usCheck=@"";
            delegate1.usNavigationCheck=@"";
            delegate1.brokerLoginCheck=false;
            //        NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
            //
            //        loggedInUser=nil;
            //
            //        NSUserDefaults * loggedInUser = [NSUserDefaults standardUserDefaults];
            //        NSDictionary * dict = [loggedInUser dictionaryRepresentation];
            //        for (id key in dict) {
            //            [loggedInUser removeObjectForKey:key];
            ////        }
            //        [loggedInUser synchronize];
            
            if([delegate1.loginActivityStr isEqualToString:@"OTP1"])
            {
                NSUserDefaults *loggedInUserNew  = [NSUserDefaults standardUserDefaults];
                [loggedInUserNew setObject:@"exit" forKey:@"guestflow"];
                [loggedInUserNew synchronize];
                //
//                delegate1.loginActivityStr=@"";
//                //             delegate1.loginActivityStr=@"";
//                [loggedInUserNew removeObjectForKey:@"loginActivityStr"];
                //             [prefs removeObjectForKey:@"ID"];
                //             [prefs removeObjectForKey:@"userid"];
                //             [prefs removeObjectForKey:@"brokername"];
                //
                
                
                [self.socketio disconnect];
                BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
                
                [self presentViewController:view animated:YES completion:nil];
                
                
            }
            
            
            else
            {
                
                
                
                
                
                //NSLog(@"asx");
                
                
                if([delegate1.brokerNameStr isEqualToString:@"Zerodha"])
                    
                {
                    
                    
                    @try {
                        
                        NSDictionary *headers = @{ @"cache-control": @"no-cache",
                                                   @"X-Kite-Version":delegate1.kiteVersion
                                                   };
                        
                        NSString * url=[NSString stringWithFormat:@"https://api.kite.trade/session/token?api_key=%@&access_token=%@",delegate1.APIKey,delegate1.accessToken];
                        
                        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                           timeoutInterval:10.0];
                        [request setHTTPMethod:@"DELETE"];
                        [request setAllHTTPHeaderFields:headers];
                        
                        NSURLSession *session = [NSURLSession sharedSession];
                        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                        if(data!=nil)
                                                                        {
                                                                            if (error) {
                                                                                //NSLog(@"%@", error);
                                                                            } else {
                                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                                //NSLog(@"%@", httpResponse);
                                                                                if(httpResponse.statusCode==200)
                                                                                {
                                                                                    logout = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                                    
                                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                                        
                                                                                        NSString * log=[NSString stringWithFormat:@"%@",[logout objectForKey:@"status"]];
                                                                                        
                                                                                        if([log isEqualToString:@"success"])
                                                                                        {
                                                                                            //
                                                                                            delegate1.logoutCheck=@"loggedout";
                                                                                            delegate1.accessToken=@"";
                                                                                            delegate1.requestToken=@"";
                                                                                            delegate1.userID=@"";
                                                                                            delegate1.publicToken=@"";
                                                                                            delegate1.userName=@"";
                                                                                            delegate1.emailId=@"";
                                                                                            delegate1.loginActivityStr=@"OTP1";
                                                                                            NSUserDefaults *loggedInUserNew = [NSUserDefaults standardUserDefaults];
                                                                                            [loggedInUserNew setObject:@"guest" forKey:@"userrole"];
                                                                                            [loggedInUserNew setObject:@"GUEST" forKey:@"brokername"];
                                                                                            [loggedInUserNew setObject:delegate1.loginActivityStr forKey:@"loginActivityStr"];
                                                                                            
                                                                                            [loggedInUserNew synchronize];
                                                                                            
                                                                                            //NSLog(@"xsef%@",delegate1.accessToken);
                                                                                            [HelpshiftCore logout];
//                                                                                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"ApplicationCookie"];
                                                                                            BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
                                                                                            
                                                                                            [self presentViewController:view animated:YES completion:nil];
                                                                                            
                                                                                            //                                                                    [self.navigationController pushViewController:view animated:YES];
                                                                                            
                                                                                        }
                                                                                        
                                                                                        
                                                                                        
                                                                                        
                                                                                    });
                                                                                }
                                                                                else  if(httpResponse.statusCode==403)
                                                                                {
                                                                                    delegate1.logoutCheck=@"loggedout";
                                                                                    delegate1.accessToken=@"";
                                                                                    delegate1.requestToken=@"";
                                                                                    delegate1.userID=@"";
                                                                                    delegate1.publicToken=@"";
                                                                                    delegate1.userName=@"";
                                                                                    delegate1.emailId=@"";
                                                                                    delegate1.loginActivityStr=@"OTP1";
                                                                                    NSUserDefaults *loggedInUserNew = [NSUserDefaults standardUserDefaults];
                                                                                    [loggedInUserNew setObject:@"guest" forKey:@"userrole"];
                                                                                    [loggedInUserNew setObject:@"GUEST" forKey:@"brokername"];
                                                                                    [loggedInUserNew setObject:delegate1.loginActivityStr forKey:@"loginActivityStr"];
                                                                                    
                                                                                    [loggedInUserNew synchronize];
                                                                                    
                                                                                    //NSLog(@"xsef%@",delegate1.accessToken);
                                                                                    [HelpshiftCore logout];
                                                                                    
                                                                                    BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
                                                                                    
                                                                                    [self presentViewController:view animated:YES completion:nil];
                                                                                    
                                                                                    //
                                                                                }
                                                                                
                                                                                
                                                                                
                                                                            }
                                                                            
                                                                            
                                                                        }
                                                                        else
                                                                        {
                                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                                                
                                                                                [self presentViewController:alert animated:YES completion:^{
                                                                                    
                                                                                }];
                                                                                
                                                                                
                                                                                
                                                                                UIAlertAction * ok=[UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                                    
                                                                                }];
                                                                                
                                                                                
                                                                                [alert addAction:ok];
                                                                                
                                                                            });
                                                                        }
                                                                        
                                                                        
                                                                    }];
                        [dataTask resume];
                        
                        
                        
                        
                        
                        
                    }
                    @catch (NSException * e) {
                        //NSLog(@"Exception: %@", e);
                    }
                    @finally {
                        //NSLog(@"finally");
                    }
                }else if ([delegate1.brokerNameStr isEqualToString:@"Upstox"])
                {
                    delegate1.logoutCheck=@"loggedout";
                    delegate1.accessToken=@"";
                    delegate1.requestToken=@"";
                    delegate1.userID=@"";
                    delegate1.publicToken=@"";
                    delegate1.userName=@"";
                    delegate1.emailId=@"";
                    
                    //NSLog(@"xsef%@",delegate1.accessToken);
                    [HelpshiftCore logout];
                    delegate1.loginActivityStr=@"OTP1";
                    NSUserDefaults *loggedInUserNew = [NSUserDefaults standardUserDefaults];
                    [loggedInUserNew setObject:@"guest" forKey:@"userrole"];
                    [loggedInUserNew setObject:@"GUEST" forKey:@"brokername"];
                    [loggedInUserNew setObject:delegate1.loginActivityStr forKey:@"loginActivityStr"];
                    
                    [loggedInUserNew synchronize];
                    
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"ApplicationCookie"];
                    
                    BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
                    
                    [self presentViewController:view animated:YES completion:nil];
                }
                
                else
                {
                    TagEncode * encode=[[TagEncode alloc]init];
                    encode.inputRequestString=@"logout";
                    
                    NSString * brokerid=[delegate1.brokerInfoDict objectForKey:@"brokerid"];
                    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
                    [inputArray addObject:[NSString stringWithFormat:@"%@multitradeapi/logout",delegate1.mtBaseUrl]];
                    [inputArray addObject:[NSString stringWithFormat:@"%@",delegate1.accessToken]];
                    [inputArray addObject:[NSString stringWithFormat:@"%@",delegate1.userID]];
                    [inputArray addObject:[NSString stringWithFormat:@"%@",delegate1.currentDeviceId1]];
                    [inputArray addObject:[NSString stringWithFormat:@"%@",brokerid]];
                    
                    //NSLog(@"%@",inputArray);
                    
                    [encode inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
                        delegate1.loginActivityStr=@"OTP1";
                        NSUserDefaults *loggedInUserNew = [NSUserDefaults standardUserDefaults];
                        [loggedInUserNew setObject:@"guest" forKey:@"userrole"];
                        [loggedInUserNew setObject:@"GUEST" forKey:@"brokername"];
                        [loggedInUserNew setObject:delegate1.loginActivityStr forKey:@"loginActivityStr"];
                        [self.socketio disconnect];
                        [loggedInUserNew synchronize];
                        //NSLog(@"%@",dict);
                        BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
                        
                        [self presentViewController:view animated:YES completion:nil];
                    }];
                    
                    //                delegate1.mtCheck=true;
                    //                TagEncode * tag1=[[TagEncode alloc]init];
                    //                [tag1 TagData:sharedManager.shMsgCode shValueMethod:sharedManager.Logout];
                    //                [tag1 TagData:sharedManager.stUserID stringMethod:delegate1.userID];
                    //
                    //                [tag1 TagData:sharedManager.btTerminal byteMethod:3];
                    //                [tag1 GetBuffer];
                    //                [self newMessage];
                    
                    
                    
                    
                    
                }
                
                
                
            }
        }];
        
        UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        
        [alert addAction:retryAction];
        [alert addAction:cancel];
        
    });
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}



-(void) didSelectMenuOptionAtIndex:(NSInteger)row
{
    
    //  0  @"Log Out",
    //  1  @"Contact Support",
    //  2  @"Campaign",
    //  3  @"Trades",
    //  4  @"Orders",
    //  5  @"Fund Transfer",
    //  6  @"Rewards",
    //  7  @"Profile Settings"
    
    
    //  0  @"Log Out",
    //  1  @"Contact Support",
    //  2  @"Trades",
    //  3  @"Orders",
    //  4  @"Fund Transfer",
    //  5  @"Rewards",
    //  6  @"Profile Settings"
    //NSLog(@"Floating action tapped index %tu",row);
    
    if(row==0)
    {
        //  0  @"Log Out",
        [self logOutMethod];
    }
    
    if(row==1)
    {
        //  1  @"Contact Support",
        [self contactSupportMethod];
    }
    if(row==2)
    {
        //NSLog(@"asx");
        
        NewMessages * messages=[self.storyboard instantiateViewControllerWithIdentifier:@"NewMessages"];
        
        [self presentViewController:messages animated:YES completion:nil];
        
    }
    
    if(row==3)
    {
        //  3  @"Trades",
        TadeView * tradeView=[self.storyboard instantiateViewControllerWithIdentifier:@"trade"];
        [self presentViewController:tradeView animated:YES completion:nil];
    }
    
    if(row==4)
    {
        //  4  @"Orders",
        MainOrdersViewController * orderview=[self.storyboard instantiateViewControllerWithIdentifier:@"orders"];
        [self presentViewController:orderview animated:YES completion:nil];
    }
    
    if(row==5)
    {
        //  5  @"Fund Transfer",
        FundsViewController *funds=[self.storyboard instantiateViewControllerWithIdentifier:@"FundsViewController"];
        [self presentViewController:funds animated:YES completion:nil];
    }
    
    if(row==6)
    {
        //  6  @"Rewards",
        WalletView * wallet=[self.storyboard instantiateViewControllerWithIdentifier:@"WalletView"];
        [self presentViewController:wallet animated:YES completion:nil];
    }
    
    if(row==7)
    {
        //  7  @"Profile Settings"
        NewProfileSettings *profileVieww=[self.storyboard instantiateViewControllerWithIdentifier:@"NewProfileSettings"];
        [self presentViewController:profileVieww animated:YES completion:nil];
    }
    
//    if(row==7)
//    {
//        //NSLog(@"asx");
//
//        NewMessages * messages=[self.storyboard instantiateViewControllerWithIdentifier:@"NewMessages"];
//
//        [self presentViewController:messages animated:YES completion:nil];
//
//    }
//
//    if(row==2)
//    {
//        [HelpshiftCampaigns showInboxOnViewController:self withConfig:nil];
//    }
//
//    if(row==5)
//    {
//        //NSLog(@"asx");
//
//        //        FundTransferView * fundTransfer=[self.storyboard instantiateViewControllerWithIdentifier:@"fund"];
//        //
//        //        [self presentViewController:fundTransfer animated:YES completion:nil];
//
//
////        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Development mode" message:@"Under Development" preferredStyle:UIAlertControllerStyleAlert];
////
////        [self presentViewController:alert animated:YES completion:^{
////
////        }];
////
////        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
////
////        }];
////
////        [alert addAction:okAction];
//
//
//        FundsViewController *funds=[self.storyboard instantiateViewControllerWithIdentifier:@"FundsViewController"];
//
//        [self presentViewController:funds animated:YES completion:nil];
//
//    }
//    if(row==8)
//    {
//        //NSLog(@"asx");
//
//
//        NewProfileSettings *profileVieww=[self.storyboard instantiateViewControllerWithIdentifier:@"NewProfileSettings"];
//
//        [self presentViewController:profileVieww animated:YES completion:nil];
//
//        //        MessageView * messages=[self.storyboard instantiateViewControllerWithIdentifier:@"message"];
//        //        [self presentViewController:messages animated:YES completion:nil];
//
//    }
//    if(row==3)
//    {
//        //NSLog(@"asx");
//
//
//
//        TadeView * tradeView=[self.storyboard instantiateViewControllerWithIdentifier:@"trade"];
//
//        [self presentViewController:tradeView animated:YES completion:nil];
//
//
//    }
//
//    if(row==6)
//    {
//
//        WalletView * wallet=[self.storyboard instantiateViewControllerWithIdentifier:@"WalletView"];
//
//        [self presentViewController:wallet animated:YES completion:nil];
//
////                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Coming Soon" preferredStyle:UIAlertControllerStyleAlert];
////
////                [self presentViewController:alert animated:YES completion:^{
////
////                }];
////
////                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
////
////                }];
////
////                [alert addAction:okAction];
////
//
//    }
//
//    if(row==4)
//    {
//        //NSLog(@"asx");
//
//
//        MainOrdersViewController * orderview=[self.storyboard instantiateViewControllerWithIdentifier:@"orders"];
//
//        [self presentViewController:orderview animated:YES completion:nil];
//
//    }
//
//    //    if(row==5)
//    //    {
//    //        //NSLog(@"asx");
//    //
//    //        PriceAlert * price=[self.storyboard instantiateViewControllerWithIdentifier:@"PriceAlert"];
//    //
//    //        [self presentViewController:price animated:YES completion:nil];
//    //
//    //    }
//
//    if(row==1)
//    {
//        @try {
//
//
//
//
//
//        //NSLog(@"asx");
//
//        //        ContactSupport * support=[self.storyboard instantiateViewControllerWithIdentifier:@"contact"];
//        //
//        //        [self presentViewController:support animated:YES completion:nil];
//
//        [HelpshiftCore setName:delegate1.userName andEmail:delegate1.emailId];
//
//
//        userDetals=[NSString stringWithFormat:@"%@(%@)",delegate1.brokerNameStr,delegate1.userID];
//
//
//
//        //        NSDictionary *metaDataWithTags = @{@"usertype": @"paid",
//        //                                           @"level":@"7",
//        //                                           @"score":@"12345",
//        //                                           HelpshiftSupportTagsKey:@[@"feedback",@"paid user",@"v4.1"]};
//
//
//
//        NSString * broker = [NSString stringWithFormat:@"%@%@",[[delegate1.brokerNameStr substringToIndex:1] lowercaseString],[delegate1.brokerNameStr substringFromIndex:1] ];
//
//        //NSLog(@"%@",broker);
//
//
//        [HelpshiftCore loginWithIdentifier:userDetals withName:delegate1.userName andEmail:delegate1.emailId];
//
//
//        //        [HelpshiftSupport setMetadataBlock:^(void){
//        //            return [NSDictionary dictionaryWithObjectsAndKeys:delegate1.userID, @"name",
//        //                    delegate1.emailId, @"email",
//        //                    [NSArray arrayWithObjects:broker,nil], HelpshiftSupportTagsKey, nil];
//        //        }];
//        //
//        //
//        //
//        //
//        //         [HelpshiftSupport showFAQs:self withConfig:nil];
//
//        if(delegate1.emailId.length>0)
//        {
//
//        }
//        else
//        {
//
//            delegate1.emailId=@"not available";
//        }
//
//        if(delegate1.userName.length>0)
//        {
//
//        }
//
//        else
//        {
//
//
//        }
//
//
//
//        NSDictionary *metaDataWithTags = @{@"userid":delegate1.userID,
//                                           @"emailid":delegate1.emailId,
//                                           HelpshiftSupportTagsKey:@[broker],
//                                           @"country:":delegate1.country,
//                                           @"locality":delegate1.locality,
//                                           @"name":delegate1.name,
//                                           @"postalcode":delegate1.postalCode,
//                                           @"sublocality":delegate1.subLocality,
//
//                                           @"locatedAt":delegate1.locatedAt
//                                           };
//
//
//
//
//
//
//
//
//
//
//        [HelpshiftSupport showFAQs:self
//                       withOptions:@{@"gotoConversationAfterContactUs":@"YES",@"showSearchonNewConversation":@"YES",
//                                     HelpshiftSupportCustomMetadataKey: metaDataWithTags}];
//
//        }
//        @catch (NSException * e) {
//            //NSLog(@"Exception: %@", e);
//        }
//        @finally {
//            //NSLog(@"finally");
//        }
//
//
//    }
//
//    if(row==0)
//    {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:@"Are you sure you want to logout" preferredStyle:UIAlertControllerStyleAlert];
//
//            [self presentViewController:alert animated:YES completion:^{
//
//            }];
//
//
//            UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                delegate1.marketWatchBool=true;
//                delegate1.usCheck=@"";
//                delegate1.usNavigationCheck=@"";
//                delegate1.brokerLoginCheck=false;
//                //        NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
//                //
//                //        loggedInUser=nil;
//                //
//                //        NSUserDefaults * loggedInUser = [NSUserDefaults standardUserDefaults];
//                //        NSDictionary * dict = [loggedInUser dictionaryRepresentation];
//                //        for (id key in dict) {
//                //            [loggedInUser removeObjectForKey:key];
//                ////        }
//                //        [loggedInUser synchronize];
//
//                if([delegate1.loginActivityStr isEqualToString:@"OTP1"])
//                {
//                    NSUserDefaults *loggedInUserNew  = [NSUserDefaults standardUserDefaults];
//                    //
//                    delegate1.loginActivityStr=@"";
//                    //             delegate1.loginActivityStr=@"";
//                    [loggedInUserNew removeObjectForKey:@"loginActivityStr"];
//                    //             [prefs removeObjectForKey:@"ID"];
//                    //             [prefs removeObjectForKey:@"userid"];
//                    //             [prefs removeObjectForKey:@"brokername"];
//                    //
//
//
//                     [self.socketio disconnect];
//                    BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
//
//                    [self presentViewController:view animated:YES completion:nil];
//
//
//                }
//
//
//                else
//                {
//
//
//
//
//
//                    //NSLog(@"asx");
//
//
//                    if([delegate1.brokerNameStr isEqualToString:@"Zerodha"])
//
//                    {
//
//
//                        @try {
//
//                            NSDictionary *headers = @{ @"cache-control": @"no-cache",
//                                                       @"X-Kite-Version":delegate1.kiteVersion
//                                                       };
//
//                            NSString * url=[NSString stringWithFormat:@"https://api.kite.trade/session/token?api_key=%@&access_token=%@",delegate1.APIKey,delegate1.accessToken];
//
//                            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
//                                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                                               timeoutInterval:10.0];
//                            [request setHTTPMethod:@"DELETE"];
//                            [request setAllHTTPHeaderFields:headers];
//
//                            NSURLSession *session = [NSURLSession sharedSession];
//                            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
//                                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                                                                            if(data!=nil)
//                                                                            {
//                                                                                if (error) {
//                                                                                    //NSLog(@"%@", error);
//                                                                                } else {
//                                                                                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
//                                                                                    //NSLog(@"%@", httpResponse);
//                                                                                    if(httpResponse.statusCode==200)
//                                                                                    {
//                                                                                    logout = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//
//                                                                                        dispatch_async(dispatch_get_main_queue(), ^{
//
//                                                                                            NSString * log=[NSString stringWithFormat:@"%@",[logout objectForKey:@"status"]];
//
//                                                                                            if([log isEqualToString:@"success"])
//                                                                                            {
//                                                                                                //
//                                                                                                delegate1.logoutCheck=@"loggedout";
//                                                                                                delegate1.accessToken=@"";
//                                                                                                delegate1.requestToken=@"";
//                                                                                                delegate1.userID=@"";
//                                                                                                delegate1.publicToken=@"";
//                                                                                                delegate1.userName=@"";
//                                                                                                delegate1.emailId=@"";
//                                                                                                delegate1.loginActivityStr=@"OTP1";
//                                                                                                NSUserDefaults *loggedInUserNew = [NSUserDefaults standardUserDefaults];
//                                                                                                [loggedInUserNew setObject:@"guest" forKey:@"userrole"];
//                                                                                                [loggedInUserNew setObject:@"GUEST" forKey:@"brokername"];
//                                                                                                [loggedInUserNew setObject:delegate1.loginActivityStr forKey:@"loginActivityStr"];
//
//                                                                                                [loggedInUserNew synchronize];
//
//                                                                                                //NSLog(@"xsef%@",delegate1.accessToken);
//                                                                                                [HelpshiftCore logout];
//                                                                                                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"ApplicationCookie"];
//                                                                                                BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
//
//                                                                                                [self presentViewController:view animated:YES completion:nil];
//
//                                                                                                //                                                                    [self.navigationController pushViewController:view animated:YES];
//
//                                                                                            }
//
//
//
//
//                                                                                        });
//                                                                                    }
//                                                                                    else  if(httpResponse.statusCode==403)
//                                                                                    {
//                                                                                        delegate1.logoutCheck=@"loggedout";
//                                                                                        delegate1.accessToken=@"";
//                                                                                        delegate1.requestToken=@"";
//                                                                                        delegate1.userID=@"";
//                                                                                        delegate1.publicToken=@"";
//                                                                                        delegate1.userName=@"";
//                                                                                        delegate1.emailId=@"";
//                                                                                        delegate1.loginActivityStr=@"OTP1";
//                                                                                        NSUserDefaults *loggedInUserNew = [NSUserDefaults standardUserDefaults];
//                                                                                        [loggedInUserNew setObject:@"guest" forKey:@"userrole"];
//                                                                                        [loggedInUserNew setObject:@"GUEST" forKey:@"brokername"];
//                                                                                        [loggedInUserNew setObject:delegate1.loginActivityStr forKey:@"loginActivityStr"];
//
//                                                                                        [loggedInUserNew synchronize];
//
//                                                                                        //NSLog(@"xsef%@",delegate1.accessToken);
//                                                                                        [HelpshiftCore logout];
//
//                                                                                        BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
//
//                                                                                        [self presentViewController:view animated:YES completion:nil];
//
//                                                                                        //
//                                                                                    }
//
//
//
//                                                                                }
//
//
//                                                                            }
//                                                                            else
//                                                                            {
//                                                                                dispatch_async(dispatch_get_main_queue(), ^{
//                                                                                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
//
//                                                                                    [self presentViewController:alert animated:YES completion:^{
//
//                                                                                    }];
//
//
//
//                                                                                    UIAlertAction * ok=[UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//                                                                                    }];
//
//
//                                                                                    [alert addAction:ok];
//
//                                                                                });
//                                                                            }
//
//
//                                                                        }];
//                            [dataTask resume];
//
//
//
//
//
//
//                        }
//                        @catch (NSException * e) {
//                            //NSLog(@"Exception: %@", e);
//                        }
//                        @finally {
//                            //NSLog(@"finally");
//                        }
//                    }else if ([delegate1.brokerNameStr isEqualToString:@"Upstox"])
//                    {
//                        delegate1.logoutCheck=@"loggedout";
//                        delegate1.accessToken=@"";
//                        delegate1.requestToken=@"";
//                        delegate1.userID=@"";
//                        delegate1.publicToken=@"";
//                        delegate1.userName=@"";
//                        delegate1.emailId=@"";
//
//                        //NSLog(@"xsef%@",delegate1.accessToken);
//                        [HelpshiftCore logout];
//                        delegate1.loginActivityStr=@"OTP1";
//                        NSUserDefaults *loggedInUserNew = [NSUserDefaults standardUserDefaults];
//                        [loggedInUserNew setObject:@"guest" forKey:@"userrole"];
//                        [loggedInUserNew setObject:@"GUEST" forKey:@"brokername"];
//                        [loggedInUserNew setObject:delegate1.loginActivityStr forKey:@"loginActivityStr"];
//
//                        [loggedInUserNew synchronize];
//
//                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"ApplicationCookie"];
//
//                        BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
//
//                        [self presentViewController:view animated:YES completion:nil];
//                    }
//
//                    else
//                    {
//                        TagEncode * encode=[[TagEncode alloc]init];
//                        encode.inputRequestString=@"logout";
//
//                        NSString * brokerid=[delegate1.brokerInfoDict objectForKey:@"brokerid"];
//                        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
//                        [inputArray addObject:[NSString stringWithFormat:@"%@multitradeapi/logout",delegate1.mtBaseUrl]];
//                        [inputArray addObject:[NSString stringWithFormat:@"%@",delegate1.accessToken]];
//                        [inputArray addObject:[NSString stringWithFormat:@"%@",delegate1.userID]];
//                        [inputArray addObject:[NSString stringWithFormat:@"%@",delegate1.currentDeviceId1]];
//                        [inputArray addObject:[NSString stringWithFormat:@"%@",brokerid]];
//
//                        //NSLog(@"%@",inputArray);
//
//                        [encode inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
//                            delegate1.loginActivityStr=@"OTP1";
//                            NSUserDefaults *loggedInUserNew = [NSUserDefaults standardUserDefaults];
//                            [loggedInUserNew setObject:@"guest" forKey:@"userrole"];
//                            [loggedInUserNew setObject:@"GUEST" forKey:@"brokername"];
//                            [loggedInUserNew setObject:delegate1.loginActivityStr forKey:@"loginActivityStr"];
//                            [self.socketio disconnect];
//                            [loggedInUserNew synchronize];
//                            //NSLog(@"%@",dict);
//                            BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
//
//                            [self presentViewController:view animated:YES completion:nil];
//                        }];
//
//                        //                delegate1.mtCheck=true;
//                        //                TagEncode * tag1=[[TagEncode alloc]init];
//                        //                [tag1 TagData:sharedManager.shMsgCode shValueMethod:sharedManager.Logout];
//                        //                [tag1 TagData:sharedManager.stUserID stringMethod:delegate1.userID];
//                        //
//                        //                [tag1 TagData:sharedManager.btTerminal byteMethod:3];
//                        //                [tag1 GetBuffer];
//                        //                [self newMessage];
//
//
//
//
//
//                    }
//
//
//
//                }
//            }];
//
//            UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//            }];
//
//
//            [alert addAction:retryAction];
//            [alert addAction:cancel];
//
//        });
//
//
//
//
//
//
//
//    }
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    if(tabBarController.selectedIndex==2)
    {
        return NO;
    }
    else
    {
    return YES;
    }
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
