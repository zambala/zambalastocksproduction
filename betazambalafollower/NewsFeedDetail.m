//
//  NewsFeedDetail.m
//  ZambalaTest
//
//  Created by zenwise technologies on 14/12/16.
//  Copyright © 2016 zenwise technologies. All rights reserved.
//

#import "NewsFeedDetail.h"

UIImageView *myImageView;

@interface NewsFeedDetail ()

@end

@implementation NewsFeedDetail

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.nameLbl.text = _nameString;
    self.descriptionLbl.text = _descripString;
    self.timeLbl.text = _timeString;

    
    
    //NSLog(@"imageArray----%@", _imgArray);
    
    for (NSString *imgStr in _imgArray)
    {
        myImageView =[[UIImageView alloc]initWithFrame:CGRectMake(16, 5, 34, 34)];
        myImageView.image = [UIImage imageNamed:imgStr];
        myImageView.layer.cornerRadius = myImageView.frame.size.width/2;
        [self.view addSubview:myImageView];
    }
    
    for (NSString *imgString in _imgArray1)
    {
        if (![imgString isEqualToString:@""])
        {
            [_ImgView setHidden:NO];
            _ImgView =[[UIImageView alloc]initWithFrame:CGRectMake(16, self.descriptionLbl.frame.origin.y+self.descriptionLbl.frame.size.height, 343, 128)];
            _ImgView.image = [UIImage imageNamed:imgString];
            [self.view addSubview:_ImgView];

        }
        else
        {
            [_ImgView setHidden:YES];
        }
      
    }
    
   
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
