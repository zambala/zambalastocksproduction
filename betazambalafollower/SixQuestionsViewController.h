//
//  SixQuestionsViewController.h
//  2FAScreens
//
//  Created by Zenwise Technologies on 27/09/17.
//  Copyright © 2017 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SixQuestionsViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *question1TF;
@property (weak, nonatomic) IBOutlet UITextField *question2TF;
@property (weak, nonatomic) IBOutlet UITextField *question3TF;
@property (weak, nonatomic) IBOutlet UITextField *question4TF;
@property (weak, nonatomic) IBOutlet UITextField *question5TF;
@property (weak, nonatomic) IBOutlet UITextField *question6TF;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityInd;

@end
