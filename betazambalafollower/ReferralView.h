//
//  ReferralView.h
//  testing
//
//  Created by zenwise technologies on 24/11/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReferralView : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *referralTxtField;
- (IBAction)continueAction:(id)sender;

@end
