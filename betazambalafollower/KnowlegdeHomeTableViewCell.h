//
//  KnowlegdeHomeTableViewCell.h
//  KnowledgeSection
//
//  Created by Zenwise Technologies on 13/06/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KnowlegdeHomeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *videoButton;

@end
