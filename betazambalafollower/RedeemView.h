//
//  RedeemView.h
//  betazambalafollower
//
//  Created by Zenwise Technologies on 17/08/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RedeemView : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *walletPointsLbl;
@property (weak, nonatomic) IBOutlet UILabel *zambalaPointValueLbl;
@property (weak, nonatomic) IBOutlet UICollectionView *vouchersCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *chooseCatagoryBtn;
- (IBAction)onChoseeTap:(id)sender;
- (IBAction)backAction:(id)sender;

@property NSMutableDictionary *getListReponseDictionary;
@property (nonatomic,retain)NSMutableArray *getListArray;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIPickerView *categoryPickerView;
@property (weak, nonatomic) IBOutlet UIView *doneView;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UILabel *minZambalaPointsLabel;

@property NSMutableArray * categoryArray;
@property NSString * zambalaPointValue;
@property NSString * rupeeString;
@property NSString * minZambalaPoints;

@end
