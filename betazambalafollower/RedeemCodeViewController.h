//
//  RedeemCodeViewController.h
//  betazambalafollower
//
//  Created by Zenwise Technologies Private Limited on 21/08/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RedeemCodeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *pointsLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
@property (weak, nonatomic) IBOutlet UIButton *codeButton;
@property (weak, nonatomic) IBOutlet UILabel *expiryLabel;
@property (weak, nonatomic) IBOutlet UILabel *referralLabel;
@property (weak, nonatomic) IBOutlet UIButton *inviteButton;
@property (weak, nonatomic) IBOutlet UIButton *homeButton;
@property (weak, nonatomic) IBOutlet UIView *inviteView;


@property NSString *pointsRedeemed;
@property NSString *voucherValue;
@property NSString *voucherCompany;
@property NSString *voucherCode;
@property NSString *expiryDate;

@end
