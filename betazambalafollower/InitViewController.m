//
//  InitViewController.m
//  betazambalafollower
//
//  Created by Zenwise Technologies on 28/09/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import "InitViewController.h"
#import "TagEncode.h"
#import "AppDelegate.h"
#import "PersonalDetailsViewController.h"
#import "BrokerViewNavigation.h"
#import "OnBoardingRootViewController.h"
@import Mixpanel;
#import "HelpshiftCore.h"
#import "HelpshiftAll.h"
#import "TabBar.h"

@interface InitViewController ()
{
    AppDelegate * delegate2;
    NSMutableDictionary *  localArray ;
}

@end

@implementation InitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate2 = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    localArray = [[NSMutableDictionary alloc]init];
    [self baseURLMethod];
    [self.activityInd startAnimating];
    // Do any additional setup after loading the view.
    
}
-(void)baseURLMethod
{
    @try
    {
        NSDictionary *headers = @{ @"Cache-Control": @"no-cache",
                                   @"content-type": @"application/json"
                                   };
          NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
        NSString * urlStr =[NSString stringWithFormat:@"https://stockserver.zenwise.net/api/follower/bootstrapbaseurl/i?version=%@",appVersionString];
//         
       
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(data!=nil)
                                                        {
                                                            if (error) {
                                                                NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                // NSLog(@"%@", httpResponse);
                                                                if([httpResponse statusCode]==200)
                                                                {
                                                                    NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                    
//                                                                      delegate2.baseUrl = [NSString stringWithFormat:@"%@",[json objectForKey:@"url"]];
                                                                    delegate2.baseUrl = @"https://stagemainstock.zenwise.net/api/";
                                                                    if(delegate2.baseUrl.length>0)
                                                                    {
                                                                        
                                                                        
                                                                        [self initCall];
                                                                        
                                                                        //                                                                else
                                                                        //                                                                {
                                                                        //                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        ////                                                                    self.activityView.hidden=YES;
                                                                        //                                                                    });
                                                                        //                                                                }
                                                                    }else
                                                                    {
                                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Something went wrong. Please try again." preferredStyle:UIAlertControllerStyleAlert];
                                                                            
                                                                            [self presentViewController:alert animated:YES completion:^{
                                                                            }];
                                                                            
                                                                            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                                [self baseURLMethod];
                                                                            }];
                                                                            
                                                                            [alert addAction:okAction];
                                                                        });
                                                                    }
                                                                }else
                                                                {
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Something went wrong. Please try again." preferredStyle:UIAlertControllerStyleAlert];
                                                                        
                                                                        [self presentViewController:alert animated:YES completion:^{
                                                                        }];
                                                                        
                                                                        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                            [self baseURLMethod];
                                                                        }];
                                                                        
                                                                        [alert addAction:okAction];
                                                                    });
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Something went wrong. Please try again." preferredStyle:UIAlertControllerStyleAlert];
                                                                
                                                                [self presentViewController:alert animated:YES completion:^{
                                                                }];
                                                                
                                                                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                    [self baseURLMethod];
                                                                }];
                                                                
                                                                [alert addAction:okAction];
                                                            });
                                                        }
                                                        
                                                    }];
        [dataTask resume];
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
     
}

-(void)initCall
{
    {
        @try
        {
            
            NSUserDefaults *loggedInUserNew = [NSUserDefaults standardUserDefaults];
            
            TagEncode * enocde=[[TagEncode alloc]init];
            delegate2.accessToken=[loggedInUserNew objectForKey:@"acesstoken"];
            enocde.inputRequestString=@"init";
            NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
            NSString * appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
            NSString * versionBuildString = [NSString stringWithFormat:@"%@(%@)", appVersionString, appBuildString];
            
            NSMutableArray * inputArray=[[NSMutableArray alloc]init];
            
//            [inputArray addObject:[NSString stringWithFormat:@"%@init",@"http://192.168.1.8:8013/api/"]];
            [inputArray addObject:[NSString stringWithFormat:@"%@init",delegate2.baseUrl]];
            //    [inputArray addObject:[NSString stringWithFormat:@"%@init",@"https://stagemainstock.zenwise.net/api/"]];
            
            [inputArray addObject:[NSString stringWithFormat:@"%@",delegate2.currentDeviceId]];
            [inputArray addObject:[NSString stringWithFormat:@"%@",versionBuildString]];
            [inputArray addObject:[NSString stringWithFormat:@"%@",@"ios"]];
            
            NSString * userid;
            if([[loggedInUserNew objectForKey:@"loginActivityStr"] isEqualToString:@"OTP1"])
            {
                userid=[loggedInUserNew objectForKey:@"userid"];
                
                [inputArray addObject:[NSString stringWithFormat:@"%@",[loggedInUserNew objectForKey:@"guestaccess"]]];
            }
            else
            {
                userid=[loggedInUserNew objectForKey:@"mtuserid"];
                [inputArray addObject:[NSString stringWithFormat:@"%@",delegate2.accessToken]];
            }
            if(userid==nil)
            {
                [inputArray addObject:[NSString stringWithFormat:@""]];
            }
            else
            {
                [inputArray addObject:[NSString stringWithFormat:@"%@",userid]];
            }
            
            //NSLog(@"%@",inputArray);
            
            [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
                
                //NSLog(@"%@",dict);
                NSArray * keysArray=[dict allKeys];
                if(keysArray.count>0)
                {
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        self.activityView.hidden=YES;
//
//                    });
                    
                    localArray = [[dict objectForKey:@"data"] objectForKey:@"data"];
                    //NSLog(@"%@",localArray);
                    delegate2.brokerDetailsDict=[[NSMutableDictionary alloc]init];
                    delegate2.brokerDetailsDict=[dict mutableCopy];
                    // [self getAllkeys];
                    int checkInt = [[localArray objectForKey:@"alreadylogin"] intValue];
                    // NSString * checkString = [NSString stringWithFormat:@"%@",[[[localArray objectAtIndex:0]objectForKey:@"alreadylogin"]stringValue]];
                    if([[loggedInUserNew objectForKey:@"loginActivityStr"] isEqualToString:@"OTP1"]&&![[loggedInUserNew objectForKey:@"guestflow"] isEqualToString:@"exit"])
                    {
                        delegate2.userID=[loggedInUserNew objectForKey:@"userid"];
                        delegate2.ltpServerURL=[NSString stringWithFormat:@"%@",[[[[[[[localArray objectForKey:@"brokerinfo"]objectForKey:@"guestlogin"]objectAtIndex:0]objectForKey:@"connectioninfo"]objectForKey:@"data"]objectAtIndex:0]objectForKey:@"ip"]];
                        delegate2.loginActivityStr=@"OTP1";
                        delegate2.brokerNameStr=@"GUEST";
                        delegate2.usCheck=@"ind";
                        delegate2.clientPhoneNumber=delegate2.userID;
                        delegate2.zenwiseToken=[loggedInUserNew objectForKey:@"guestaccess"];
                        delegate2.accessToken=delegate2.zenwiseToken;
                        dispatch_async(dispatch_get_main_queue(), ^{
                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                            
                            
                            
                            //                                [self.window makeKeyAndVisible];
                            //                                [self.window.rootViewController presentViewController:view animated:YES completion:NULL];
                            NSString * token=[NSString stringWithFormat:@"%@",[localArray objectForKey:@"token"]];
                            if([token containsString:@"null"]||[token isEqual:[NSNull
                                                                               null]])
                            {
                                
                                PersonalDetailsViewController * personal = [self.storyboard instantiateViewControllerWithIdentifier:@"PersonalDetailsViewController"];
                                [self presentViewController:personal animated:YES completion:nil];
                                
                            }
                            else
                            {
                                [self getAllkeys];
                            }
                            delegate2.brokerInfoDict=[[NSMutableDictionary alloc]init];
                            delegate2.brokerInfoDict=[[[localArray objectForKey:@"brokerinfo"]objectForKey:@"guestlogin"]objectAtIndex:0];
                        });
                        
                        
                    }
                    else if([[loggedInUserNew objectForKey:@"clientstatus"] isEqualToString:@"clientcreated"] && ![[loggedInUserNew objectForKey:@"loginActivityStr"] isEqualToString:@"CLIENT"])
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSString * token=[NSString stringWithFormat:@"%@",[localArray objectForKey:@"token"]];
                            delegate2.zenwiseToken=token;
                            delegate2.userID=[loggedInUserNew objectForKey:@"userid"];
                            [self getAllkeys];
                            if([token containsString:@"null"]||[token isEqual:[NSNull
                                                                               null]])
                            {
                                PersonalDetailsViewController * personal = [self.storyboard instantiateViewControllerWithIdentifier:@"PersonalDetailsViewController"];
                                [self presentViewController:personal animated:YES completion:nil];
                            }
                            else
                            {
                                BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
                                [self presentViewController:view animated:YES completion:nil];
                            }
                        });
                    }
                    else
                    {
                        if(checkInt ==1)
                        {
                            delegate2.userID=[loggedInUserNew objectForKey:@"mtuserid"];
                            delegate2.accessToken = [NSString stringWithFormat:@"%@",[localArray objectForKey:@"token"]];
                            delegate2.zenwiseToken = [NSString stringWithFormat:@"%@",[localArray objectForKey:@"token"]];
                            delegate2.brokerNameStr=[loggedInUserNew objectForKey:@"brokername"];
                            delegate2.loginActivityStr=[loggedInUserNew objectForKey:@"loginActivityStr"];
                            int numberCount = (int)[[[localArray objectForKey:@"brokerinfo"] objectForKey:@"clientlogin"] count];
                            //NSLog(@"NumberCount:%d",numberCount);
                            for(int i=0;i<numberCount;i++)
                            {
                                NSString * brokerName=[NSString stringWithFormat:@"%@",[[[[localArray objectForKey:@"brokerinfo"]objectForKey:@"clientlogin"]objectAtIndex:i]objectForKey:@"brokername"]];
                                if([delegate2.brokerNameStr containsString:brokerName])
                                {
                                    delegate2.brokerInfoDict=[[[localArray objectForKey:@"brokerinfo"]objectForKey:@"clientlogin"] objectAtIndex:i];
                                }
                            }
                            if (([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"PhillipCapital"]||[delegate2.brokerNameStr isEqualToString:@"Arcadia"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"]
                                 ||[delegate2.brokerNameStr isEqualToString:@"Proficient"] ||[delegate2.brokerNameStr isEqualToString:@"BSE BOW"]||[delegate2.brokerNameStr containsString:@"Choice Trade"]))
                            {
                                if(delegate2.brokerInfoDict.count>0)
                                {
                                    if([delegate2.brokerNameStr isEqualToString:@"Arcadia"]||[delegate2.brokerNameStr isEqualToString:@"PhillipCapital"]||[delegate2.brokerNameStr isEqualToString:@"Proficient"]||[delegate2.brokerNameStr isEqualToString:@"BSE BOW"])
                                    {
                                        UIImage *NavigationPortraitBackground= [UIImage imageNamed:@"topBg1"];
                                        [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
                                        delegate2.usCheck=@"ind";
                                        delegate2.brokerNameStr=delegate2.brokerNameStr;
                                        delegate2.ltpServerURL=[NSString stringWithFormat:@"%@",[[[[delegate2.brokerInfoDict objectForKey:@"connectioninfo"]objectForKey:@"data"]objectAtIndex:0]objectForKey:@"ip"]];
                                        delegate2.mtBaseUrl = [NSString stringWithFormat:@"%@",[delegate2.brokerInfoDict objectForKey:@"ip"]];
                                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            
                                            //                                [self.window makeKeyAndVisible];
                                            //                                [self.window.rootViewController presentViewController:view animated:YES completion:NULL];
                                            NSString * token=[NSString stringWithFormat:@"%@",[localArray objectForKey:@"token"]];
                                            if([token containsString:@"null"]||[token isEqual:[NSNull
                                                                                               null]])
                                            {
                                                
                                                PersonalDetailsViewController * personal = [self.storyboard instantiateViewControllerWithIdentifier:@"PersonalDetailsViewController"];
                                                [self presentViewController:personal animated:YES completion:nil];
                                                
                                            }
                                            else
                                            {
                                                
                                                [self getAllkeys];
                                            }
                                            
                                        });
                                        
                                    }else if ([delegate2.brokerNameStr containsString:@"Choice Trade"])
                                    {
                                        UIImage *NavigationPortraitBackground= [UIImage imageNamed:@"topUS-1"];
                                        [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
                                        
                                        [[UINavigationBar appearance] setTranslucent:NO];
                                        delegate2.usCheck=@"USA";
                                        delegate2.usNavigationCheck=@"USA";
                                        //                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"USA" bundle:nil];
                                        //                            LoginViewController * usHome = [storyboard instantiateInitialViewController];
                                        //                            [self presentViewController:usHome animated:YES completion:nil];
                                    }
                                    
                                    else
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            NSString * token=[NSString stringWithFormat:@"%@",[localArray objectForKey:@"token"]];
                                            if([token containsString:@"null"]||[token isEqual:[NSNull
                                                                                               null]])
                                            {
                                                
                                                PersonalDetailsViewController * personal = [self.storyboard instantiateViewControllerWithIdentifier:@"PersonalDetailsViewController"];
                                                [self presentViewController:personal animated:YES completion:nil];
                                                
                                            }
                                            else
                                            {
                                                
                                                //                                          ViewController1 *view = [self.storyboard instantiateViewControllerWithIdentifier:@"view"];
                                                //                                          [self presentViewController:view animated:YES completion:nil];
                                                
                                                BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
                                                
                                                [self presentViewController:view animated:YES completion:nil];
                                                
                                            }
                                            
                                            
                                            UIImage *NavigationPortraitBackground= [UIImage imageNamed:@"topBg1"];
                                            [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
                                            delegate2.usCheck=@"ind";
                                            delegate2.brokerNameStr=delegate2.brokerNameStr;
                                            //                            ViewController1 *view = [self.storyboard instantiateViewControllerWithIdentifier:@"view"];
                                            //                            [self.navigationController pushViewController:view animated:YES];
                                        });
                                    }
                                }
                            }
                            
                        }
                        else
                        {
                            delegate2.brokerNameStr=[loggedInUserNew objectForKey:@"brokername"];
                            delegate2.loginActivityStr=[loggedInUserNew objectForKey:@"loginActivityStr"];
                            
                            int numberCount = (int)[[[localArray objectForKey:@"brokerinfo"] objectForKey:@"clientlogin"] count];
                            //NSLog(@"NumberCount:%d",numberCount);
                            for(int i=0;i<numberCount;i++)
                            {
                                
                                NSString * brokerName=[NSString stringWithFormat:@"%@",[[[[localArray objectForKey:@"brokerinfo"]objectForKey:@"clientlogin"]objectAtIndex:i]objectForKey:@"brokername"]];
                                
                                
                                if([delegate2.brokerNameStr containsString:brokerName])
                                {
                                    delegate2.brokerInfoDict=[[[localArray objectForKey:@"brokerinfo"]objectForKey:@"clientlogin"] objectAtIndex:i];
                                }
                            }
                            
                            if (([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"PhillipCapital"]||[delegate2.brokerNameStr isEqualToString:@"Arcadia"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"]
                                 ||[delegate2.brokerNameStr isEqualToString:@"Proficient"]||[delegate2.brokerNameStr isEqualToString:@"BSE BOW"]||[delegate2.brokerNameStr containsString:@"Choice Trade"]))
                            {
                                if(delegate2.brokerInfoDict.count>0)
                                {
                                    if([delegate2.brokerNameStr isEqualToString:@"Arcadia"]||[delegate2.brokerNameStr isEqualToString:@"PhillipCapital"]||[delegate2.brokerNameStr isEqualToString:@"Proficient"]||[delegate2.brokerNameStr isEqualToString:@"BSE BOW"])
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            UIImage *NavigationPortraitBackground= [UIImage imageNamed:@"topBg1"];
                                            [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
                                            delegate2.usCheck=@"ind";
                                            delegate2.brokerNameStr=delegate2.brokerNameStr;
                                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                            //                                    TabBar *view = [storyboard instantiateViewControllerWithIdentifier:@"tab"];
                                            //                                    //                                [self.window makeKeyAndVisible];
                                            //                                    //                                [self.window.rootViewController presentViewController:view animated:YES completion:NULL];
                                            //                                    [self presentViewController:view animated:YES completion:nil];
                                            NSString * token=[NSString stringWithFormat:@"%@",[localArray objectForKey:@"token"]];
                                            if([token containsString:@"null"]||[token isEqual:[NSNull
                                                                                               null]])
                                            {
                                                
                                                PersonalDetailsViewController * personal = [self.storyboard instantiateViewControllerWithIdentifier:@"PersonalDetailsViewController"];
                                                [self presentViewController:personal animated:YES completion:nil];
                                                
                                            }
                                            else
                                            {
                                                
                                                BrokerViewNavigation * view=[storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
                                                
                                                [self presentViewController:view animated:YES completion:nil];
                                                
                                            }
                                            
                                            
                                        });
                                        
                                        
                                    }else if ([delegate2.brokerNameStr containsString:@"Choice Trade"])
                                    {
                                        UIImage *NavigationPortraitBackground= [UIImage imageNamed:@"topUS-1"];
                                        [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
                                        
                                        [[UINavigationBar appearance] setTranslucent:NO];
                                        delegate2.usCheck=@"USA";
                                        delegate2.usNavigationCheck=@"USA";
                                        //                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"USA" bundle:nil];
                                        //                            LoginViewController * usHome = [storyboard instantiateInitialViewController];
                                        //                            [self presentViewController:usHome animated:YES completion:nil];
                                    }
                                    
                                    else
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                            //                                    ViewController1 *view = [storyboard instantiateViewControllerWithIdentifier:@"view"];
                                            //                                    [self presentViewController:view animated:YES completion:nil];
                                            
                                            BrokerViewNavigation * view=[storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
                                            
                                            [self presentViewController:view animated:YES completion:nil];
                                            
                                            UIImage *NavigationPortraitBackground= [UIImage imageNamed:@"topBg1"];
                                            [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
                                            delegate2.usCheck=@"ind";
                                            delegate2.brokerNameStr=delegate2.brokerNameStr;
                                        });
                                        //                            ViewController1 *view = [self.storyboard instantiateViewControllerWithIdentifier:@"view"];
                                        //                            [self.navigationController pushViewController:view animated:YES];
                                    }
                                }
                            }
                            
                            else
                            {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    OnBoardingRootViewController * personal = [self.storyboard instantiateViewControllerWithIdentifier:@"OnBoardingRootViewController"];
                                    [self presentViewController:personal animated:YES completion:nil];
                                });
                            }
                            
                        }
                        
                        
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //                self.activityView.hidden=YES;
                    });
                }
                else
                {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! Something went wrong. Please try again!" preferredStyle:UIAlertControllerStyleAlert];
                        
                        [self presentViewController:alert animated:YES completion:^{
                            
                        }];
                        
                        
                        UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [self initCall];
                        }];
                        
                        //                UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        //
                        //                }];
                        
                        
                        [alert addAction:retryAction];
                        //                [alert addAction:cancel];
                        
                    });
                    
                    
                }
                
            }];
            //    }
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
    }
}
-(void)getAllkeys
{
    @try
    {
        TagEncode * enocde=[[TagEncode alloc]init];
        
        enocde.inputRequestString=@"getallkeys";
        
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        
        // [inputArray addObject:[NSString stringWithFormat:@"%@follower/postinit?x-access-token=%@",delegate2.baseUrl,delegate2.zenwiseToken]];
        
        [inputArray addObject:[NSString stringWithFormat:@"%@follower/postinit?x-access-token=%@",delegate2.baseUrl,delegate2.zenwiseToken]];
        
        [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
            
            if([[dict objectForKey:@"data"] count]>0)
            {
                NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
                
                NSString * number= [loggedInUser stringForKey:@"profilemobilenumber"];
                
                NSString * name= [loggedInUser stringForKey:@"profilename"];
                NSString * email= [loggedInUser stringForKey:@"profileemail"];
                
                NSString * mixPanelToken;
                NSString * helpShiftToken;
                for (int i=0; i<[[dict objectForKey:@"data"] count]; i++) {
                    NSString * appString = [NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"] objectAtIndex:i] objectForKey:@"externalapp"]];
                    if([appString isEqualToString:@"helpshift"])
                    {
                        helpShiftToken = [NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"key"]];
                    }else if ([appString isEqualToString:@"mixedpanel"])
                    {
                        mixPanelToken = [NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"key"]];
                    }
                }
                
                Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
//                [Mixpanel sharedInstanceWithToken:mixPanelToken];
                [[Mixpanel sharedInstance] track:@"Launched"];
                [mixpanelMini identify:mixpanelMini.distinctId];
                
                if(delegate2.mixpanelDeviceToken.length>0)
                {
                    [mixpanelMini.people addPushDeviceToken:delegate2.mixpanelDeviceToken];
                    
                }
                
                [mixpanelMini track:@"select_broker_page"];
                
                //                helpshift//--
                
                [HelpshiftCore initializeWithProvider:[HelpshiftAll sharedInstance]];
                
                
                [HelpshiftCore installForApiKey:helpShiftToken domainName:@"zenwise-technologies.helpshift.com" appID:@"zenwise-technologies_platform_20170726065628934-964b6d1e092c000"];
                
                [HelpshiftCore registerDeviceToken:delegate2.helpShiftData];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    TabBar *view = [self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
                    [self presentViewController:view animated:YES completion:nil];
                });
                
            }else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Something went wrong please try again." preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self getAllkeys];
                    }];
                    
                    [alert addAction:okAction];
                });
            }
        }];
        
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
