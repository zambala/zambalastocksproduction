//
//  BrokerView.h
//  testing
//
//  Created by zenwise technologies on 24/11/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]


@interface BrokerView : UIViewController<UITextFieldDelegate>
{
    NSArray *dataArray;
    UIPickerView *pickerView;
    UIView *ViewContainer;
    
    NSString *dataStr;
    
    BOOL dropFlag, chkBoxFlag;
    
    UITableView *dropDwnTableView;


}

@property (strong, nonatomic) IBOutlet UIButton *brokerNameBtn, *chkImgBtn;
@property (strong, nonatomic) IBOutlet UITextField *phoneNumberTF;
@property (strong, nonatomic) IBOutlet UITextField *countryCodeTF;
@property Reachability * reach;

@property (weak, nonatomic) IBOutlet UILabel *brokerNameLbl;

@property (strong, nonatomic) IBOutlet UITextField *brokerSearchFld;

@property (strong, nonatomic) IBOutlet UIView *guestLoginView;
@property (strong, nonatomic) IBOutlet UIButton *rememberLabel;
@property (strong, nonatomic) IBOutlet UIButton *continueButton;
@property (strong, nonatomic) IBOutlet UIButton *guestLoginButton;
@property (strong, nonatomic) IBOutlet UIButton *clientLoginButton;
- (IBAction)clientLoginAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *guestLoginAction;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *guetLoginViewheightConstraint;
- (IBAction)guestLoginAction:(id)sender;
@property NSMutableDictionary *responseDict;
@property BOOL check;

@property (strong, nonatomic) IBOutlet UITextField *referralTxtFld;

@property (weak, nonatomic) IBOutlet UIView *backView;


@end
