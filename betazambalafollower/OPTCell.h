//
//  OPTCell.h
//  
//
//  Created by zenwise technologies on 12/04/17.
//
//

#import <UIKit/UIKit.h>

@interface OPTCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *strikeBtn;
@property (strong, nonatomic) IBOutlet UILabel *bidPriceCE;
@property (strong, nonatomic) IBOutlet UILabel *bidQtyCE;

@property (strong, nonatomic) IBOutlet UILabel *askPriceCE;
@property (strong, nonatomic) IBOutlet UILabel *askQtyCE;

@property (strong, nonatomic) IBOutlet UILabel *bidPricePE;

@property (strong, nonatomic) IBOutlet UILabel *bidQtyPE;

@property (strong, nonatomic) IBOutlet UILabel *askPricePE;

@property (strong, nonatomic) IBOutlet UILabel *askQtyPE;
@property (strong, nonatomic) IBOutlet UIButton *btn_BID_CE;
@property (strong, nonatomic) IBOutlet UIButton *btn_ASK_CE;
@property (strong, nonatomic) IBOutlet UIButton *btn_ASK_PE;
@property (strong, nonatomic) IBOutlet UIButton *btn_BID_PE;


@end
