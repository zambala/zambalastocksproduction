//
//  PaymentStatusViewController.m
//  betazambalafollower
//
//  Created by Zenwise Technologies on 02/01/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import "PaymentStatusViewController.h"
#import "AppDelegate.h"
#import "NewOffersByMonksViewController.h"
#import "FundsViewController.h"
#import "TabBar.h"
#import "ViewController.h"

@interface PaymentStatusViewController ()

@end

@implementation PaymentStatusViewController
{
    NSString * status;
    AppDelegate * delegate;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.activityIndicator.hidden=YES;
    self.bottomButton.layer.cornerRadius=4.0f;
    [self.bottomButton addTarget:self action:@selector(onButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self paymentDetails];
    // Do any additional setup after loading the view.
}

-(void)paymentDetails
{
    @try
    {
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    NSDictionary *headers = @{ @"Content-Type": @"application/x-www-form-urlencoded",
                               @"Cache-Control": @"no-cache",
                                @"authtoken":delegate.zenwiseToken,
                                @"deviceid":delegate.currentDeviceId
                               };
    
    NSString * transactionNumber=[NSString stringWithFormat:@"&merchantTxnNo=%@",self.txnNo];
    
    NSMutableData *postData = [[NSMutableData alloc] initWithData:[transactionNumber dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString * url = [NSString stringWithFormat:@"%@clienttrade/phicommerce/paymentdetails",delegate.baseUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==403)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                            
                                                        }
                                                        else if([httpResponse statusCode]==401)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                        self.paymentResponseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        //NSLog(@"Payment Details:%@",self.paymentResponseDictionary);
                                                        
                                                        status = [NSString stringWithFormat:@"%@",[self.paymentResponseDictionary objectForKey:@"respdesc"]];
                                                        }
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        self.transactionIDLabel.text = [NSString stringWithFormat:@"%@",[self.paymentResponseDictionary objectForKey:@"txnid"]];
                                                        if([status isEqualToString:@"Transaction successful"])
                                                        {
                                                            self.imageView.image= [UIImage imageNamed:@"paymentSuccess"];
                                                            self.paymentLabel.text=@"Payment Successful";
                                                            self.paymentLabel.textColor = [UIColor colorWithRed:(106/255.0) green:(194/255.0) blue:(89/255.0) alpha:1.0];
//                                                            UIAlertController * alert = [UIAlertController
//                                                                                         alertControllerWithTitle:@"Payment Successful"
//                                                                                         message:@"Press ok to continue enjoying our premium services."
//                                                                                         preferredStyle:UIAlertControllerStyleAlert];
//
//                                                            //Add Buttons
//
//                                                            UIAlertAction* okButton = [UIAlertAction
//                                                                                       actionWithTitle:@"Ok"
//                                                                                       style:UIAlertActionStyleDefault
//                                                                                       handler:^(UIAlertAction * action) {
//                                                                                           //Handle your yes please button action here
//
//
//                                                                                       }];
//                                                            //Add your buttons to alert controller
//
//                                                            [alert addAction:okButton];
//
//
//                                                            [self presentViewController:alert animated:YES completion:nil];
                                                            
                                                            if([self.payinCheck isEqualToString:@"payin"])
                                                            {
                                                                [self.bottomButton setTitle:@"BACK TO FUNDS" forState:UIControlStateNormal];
                                                            }else
                                                            {
                                                                NSString * status = @"Thank you! We have received your payment. Please check your email for the order details.";
                                                                [self.statusDescriptionButton setTitle:status forState:UIControlStateNormal];
                                                                [self.bottomButton setTitle:@"BACK TO HOME" forState:UIControlStateNormal];
                                                            }
                                                            
                                                            
                                                            
                                                        }else if ([status isEqualToString:@"Transaction rejected"]||[status isEqualToString:@"<null>"]||[status isEqual:[NSNull null]])
                                                        {
                                                            NSString * status = @"We are unable to process your payment at the moment. Please try again or contact us";
                                                            [self.statusDescriptionButton setTitle:status forState:UIControlStateNormal];
                                                            self.imageView.image = [UIImage imageNamed:@"paymentFailed"];
                                                            self.paymentLabel.text=@"Payment Failed";
                                                            self.paymentLabel.textColor = [UIColor colorWithRed:(199/255.0) green:(58/255.0) blue:(58/255.0) alpha:1.0];
                                                            [self.bottomButton setTitle:@"TRY AGAIN" forState:UIControlStateNormal];
                                                        }else
                                                        {
                                                            NSString * status = @"We are still processing your payment. We will notify you once we have a confirmation from your bank.";
                                                            [self.statusDescriptionButton setTitle:status forState:UIControlStateNormal];
                                                            self.imageView.image = [UIImage imageNamed:@"paymentPending"];
                                                            self.paymentLabel.text=@"Payment Pending";
                                                            self.paymentLabel.textColor = [UIColor colorWithRed:(255/255.0) green:(162/255.0) blue:(54/255.0) alpha:1.0];
                                                            [self.bottomButton setTitle:@"TRY AGAIN" forState:UIControlStateNormal];
                                                        }
                                                        self.activityIndicator.hidden=YES;
                                                        [self.activityIndicator stopAnimating];
                                                    });
                                                    
                                                }];
    [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)onButtonTap
{
    if([self.bottomButton.titleLabel.text isEqualToString:@"TRY AGAIN"])
    {
        ViewController * webLoad = [self.storyboard instantiateViewControllerWithIdentifier:@"view1"];
        [self.navigationController pushViewController:webLoad animated:YES];
         [[NSNotificationCenter defaultCenter] postNotificationName:@"paymenttryagain" object:nil userInfo:nil];
    }else
    {
    if([self.payinCheck isEqualToString:@"payin"])
    {
        FundsViewController * funds = [self.storyboard instantiateViewControllerWithIdentifier:@"FundsViewController"];
        [self.navigationController pushViewController:funds animated:YES];
    }else
    {
        TabBar * offersPage = [self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
        [delegate.cartDict removeAllObjects];
       // [self.navigationController pushViewController:offersPage animated:YES];
        [self presentViewController:offersPage animated:YES completion:nil];
    }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
