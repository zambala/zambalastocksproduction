//
//  CoupansView.h
//  betazambalafollower
//
//  Created by Zenwise Technologies on 17/04/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CoupansView : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *coupansTblView;

@end
