//
//  WealthCell.m
//  betazambalafollower
//
//  Created by Zenwise Technologies on 01/08/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import "WealthCell.h"

@implementation WealthCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.biggestBtn.layer.borderWidth=1.0f;
    self.biggestBtn.layer.borderColor=[[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f] CGColor];
    self.biggestBtn.layer.cornerRadius=10.0f;
    
    self.fastestBtn.layer.borderWidth=1.0f;
    self.fastestBtn.layer.borderColor=[[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f] CGColor];
    self.fastestBtn.layer.cornerRadius=10.0f;
    
    self.consistentBtn.layer.borderWidth=1.0f;
    self.consistentBtn.layer.borderColor=[[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f] CGColor];
    self.consistentBtn.layer.cornerRadius=10.0f;
    
    [self.fastestBtn setBackgroundColor:[UIColor whiteColor]];
    [self.fastestBtn setTitleColor:[UIColor colorWithRed:2/255 green:30/255 blue:41/255 alpha:1.0f] forState:UIControlStateNormal];
    
    [self.consistentBtn setBackgroundColor:[UIColor whiteColor]];
    [self.consistentBtn setTitleColor:[UIColor colorWithRed:2/255 green:30/255 blue:41/255 alpha:1.0f] forState:UIControlStateNormal];
    self.fiveYearsBtn.hidden=NO;
    self.twentyFiveYearsBtn.hidden=NO;
    [self.fiveYearsBtn setTitle:@" 5 Years" forState:UIControlStateNormal];
    [self.twentyFiveYearsBtn setTitle:@" 25 Years" forState:UIControlStateNormal];
    
    self.fiveYearsBtn.imageView.image=[UIImage imageNamed:@"radioSelect"];
    self.twentyFiveYearsBtn.imageView.image=[UIImage imageNamed:@"radioUnselect"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
