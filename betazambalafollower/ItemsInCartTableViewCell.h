//
//  ItemsInCartTableViewCell.h
//  PremiumServices
//
//  Created by zenwise mac 2 on 7/13/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemsInCartTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *serviceTypeLbl;


@property (weak, nonatomic) IBOutlet UILabel *durationLbl;

@property (weak, nonatomic) IBOutlet UILabel *priceLbl;

@property (weak, nonatomic) IBOutlet UIButton *removeBtn;


@end
