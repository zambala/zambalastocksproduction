//
//  MainOrdersViewController.m
//  testing
//
//  Created by zenwise technologies on 26/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "MainOrdersViewController.h"
#import "HMSegmentedControl.h"
#import "PendingOrdersCell.h"
#import "CompletedCell.h"
#import "CancelledOrderCell.h"
#import "TabBar.h"
#import "AppDelegate.h"
#import "NoOrderImg.h"
#import "StockOrderView.h"
#import "HomePage.h"
#import "PortfolioView.h"
#import "HomePage.h"
#import "TagEncode.h"

#import "MT.h"

#import <Foundation/Foundation.h>
#import <CoreFoundation/CoreFoundation.h>
#import <Endian.h>
#import "MTORDER.h"
#import <Mixpanel/Mixpanel.h>
#import "OpenAccountView.h"
#import "BrokerViewNavigation.h"
#define buyColor [UIColor colorWithRed:37.0/255.0 green:114.0/255.0 blue:191.0/255.0 alpha:1.0]
#define sellColor [UIColor colorWithRed:206.0/255.0 green:38.0/255.0 blue:38.0/255.0 alpha:1.0]
@interface MainOrdersViewController ()<NSStreamDelegate>
{
    HMSegmentedControl * segmentedControl;
    AppDelegate * delegate2;
    //mt//
    
    MT *sharedManager;
    MTORDER * sharedManagerOrder;
    NSMutableArray * securityTokenArray;
    NSMutableArray *symbolResponse;
    NSMutableArray * newSymbolResponseArray;
    BOOL mainorderCheck;
    NSMutableArray * mixpanelCancelledArray;
    NSMutableArray * mixpanelCompletedArray;
    NSMutableArray * mixpanelPendingArray;
    BOOL reload;
    NSTimer * timer;
    NSMutableArray * mtExchangeArray;
    NSMutableArray * mtOrdersArray;
    NSIndexPath * selectedIndex;
    BOOL expandBool;
//    NSArray *newSymbols;
}

@end

@implementation MainOrdersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     delegate2=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    expandBool = false;
    reload=true;
    //orders list//
    
    orderIDArray = [[NSMutableArray alloc]init];
    tradingSymArray = [[NSMutableArray alloc]init];
    orderTimeArray = [[NSMutableArray alloc]init];
    exchangeArray = [[NSMutableArray alloc]init];
    filledArray = [[NSMutableArray alloc]init];
    comPendingArray = [[NSMutableArray alloc]init];
    orderTypeArray = [[NSMutableArray alloc]init];
    transactionTypeArray = [[NSMutableArray alloc]init];
    priceArray = [[NSMutableArray alloc]init];
    totalQtyArray = [[NSMutableArray alloc]init];
    completedProductArray = [[NSMutableArray alloc]init];
  
    
    cancelledOrderIDArray = [[NSMutableArray alloc]init];
    cancelledTradingSymArray = [[NSMutableArray alloc]init];
    cancelledOrderTimeArray = [[NSMutableArray alloc]init];
    cancelledExchangeArray = [[NSMutableArray alloc]init];
    cancelledFilledArray = [[NSMutableArray alloc]init];
    cancelledPendingArray = [[NSMutableArray alloc]init];
    cancelledOrderTypeArray = [[NSMutableArray alloc]init];
    cancelledTransactionTypeArray = [[NSMutableArray alloc]init];
    cancelledPriceArray = [[NSMutableArray alloc]init];
    cancelledTotalQtyArray = [[NSMutableArray alloc]init];
    cancelledProductArray = [[NSMutableArray alloc]init];
    
    
    pendingOrderIDArray = [[NSMutableArray alloc]init];
    pendingTradingSymArray = [[NSMutableArray alloc]init];
    pendingOrderTimeArray = [[NSMutableArray alloc]init];
    pendingExchangeArray = [[NSMutableArray alloc]init];
    pendingFilledArray = [[NSMutableArray alloc]init];
    pendingPendingArray = [[NSMutableArray alloc]init];
    pendingOrderTypeArray = [[NSMutableArray alloc]init];
    pendingTransactionTypeArray = [[NSMutableArray alloc]init];
    pendingPriceArray = [[NSMutableArray alloc]init];
    pendingTotalQtyArray = [[NSMutableArray alloc]init];
    pendingFilledArrayy = [[NSMutableArray alloc]init];
    pendingLabelArray = [[NSMutableArray alloc]init];
    instrumentTokenn = [[NSMutableArray alloc]init];
    pendingMessageArray=[[NSMutableArray alloc]init];
    pendingProductArray = [[NSMutableArray alloc]init];

    
       
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"PENDING",@"COMPLETED",@"CANCELLED"]];
    segmentedControl.frame = CGRectMake(0,88, self.view.frame.size.width, 54.1);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.verticalDividerEnabled = YES;
    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1];
        segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
    [segmentedControl setSelectedSegmentIndex:0];
    [self segmentedControlChangedValue];
    [self.view addSubview:segmentedControl];
    
    self.pendingView.hidden=NO;
    self.completedView.hidden=YES;
    self.cancelView.hidden=YES;
    
    //NSLog(@"%@",delegate2.orderStatusCheck);
    
    //mt//
    sharedManager = [MT Mt1];
    sharedManagerOrder = [MTORDER Mt2];
   
    self.pendingOrdersTableView.hidden=YES;
    self.activityInd.hidden=NO;
    [self.activityInd startAnimating];
    
    
    if([delegate2.loginActivityStr isEqualToString:@"OTP1"])
    {
        [self.activityInd stopAnimating];
        self.activityInd.hidden=YES;
        self.pendingOrdersTableView.delegate=self;
        self.pendingOrdersTableView.dataSource=self;
        [self.pendingOrdersTableView reloadData];
    }
    
//    if([self.topViewCheck isEqualToString:@"YES"])
//    {
//        self.topViewLayoutConstraint.constant=0;
//    }else
//    {
//        self.topViewLayoutConstraint.constant=88;
//    }
    
       
}
-(void)loginCheck
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Please choose an option." message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [self presentViewController:alert animated:YES completion:^{
        
    }];
   
    UIAlertAction * Login=[UIAlertAction actionWithTitle:@"Login" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //Navigate to login page
        NSUserDefaults *loggedInUserNew  = [NSUserDefaults standardUserDefaults];
        //
        delegate2.loginActivityStr=@"";
        //             delegate1.loginActivityStr=@"";
        [loggedInUserNew removeObjectForKey:@"loginActivityStr"];
        
        
        
        BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
        
        [self presentViewController:view animated:YES completion:nil];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
   
    [alert addAction:Login];
    [alert addAction:cancel];
    
}


-(void)hideMethod
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.pendingOrdersTableView.hidden=NO;
        self.activityInd.hidden=YES;
        [self.activityInd stopAnimating];
        
        self.pendingOrdersTableView.delegate=self;
        self.pendingOrdersTableView.dataSource=self;
        
        [self.pendingOrdersTableView reloadData];
        
    });
  
    
  
}


-(void)viewDidAppear:(BOOL)animated
{
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"order_book_page"];
      [mixpanelMini track:@"pending_order_page"];
    
    
//      self.timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(hideMethod) userInfo:nil repeats:YES];
    if([delegate2.loginActivityStr isEqualToString:@"OTP1"])
    {
        [self loginCheck];
        
    }
    
    else
    {
        
    }
    
    mixpanelCancelledArray=[[NSMutableArray alloc]init];
    mixpanelCompletedArray=[[NSMutableArray alloc]init];
    mixpanelPendingArray=[[NSMutableArray alloc]init];
    
   timer=  [NSTimer scheduledTimerWithTimeInterval:2.0f
                                                      target:self selector:@selector(reloadData) userInfo:nil repeats:YES];
    securityTokenArray=[[NSMutableArray alloc]init];
    exchangeArray=[[NSMutableArray alloc]init];

   
    [[NSNotificationCenter defaultCenter] addObserver:self
                                                   selector:@selector(showMainMenu:)
                                                    name:@"loginComplete" object:nil];
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    
    newSymbolResponseArray=[[NSMutableArray alloc]init];
    
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
    delegate2.mainorderCheck=true;
    
    delegate2.allOrderHistory=[[NSMutableArray alloc]init];
    if(delegate2.orderStatusCheck.length>0)
    {
    if([delegate2.orderStatusCheck isEqualToString:@"COMPLETED"])
    {
        [segmentedControl setSelectedSegmentIndex:1];
        [self segmentedControlChangedValue];

        delegate2.orderStatusCheck=@"";


    }
//
    else if([delegate2.orderStatusCheck isEqualToString:@"REJECTED"])
    {
        [segmentedControl setSelectedSegmentIndex:2];
        [self segmentedControlChangedValue];
        delegate2.orderStatusCheck=@"";


    }
//
    else if([delegate2.orderStatusCheck isEqualToString:@"OPEN"])
    {
        [segmentedControl setSelectedSegmentIndex:0];
        [self segmentedControlChangedValue];
        delegate2.orderStatusCheck=@"";
        
    }
    }
    else
    {
        segmentedControl.selectedSegmentIndex=0;
     }

//  if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
//  {
//      [self RetrieveAllOrders];
//  }
//        
//else
//{
//    
////    delegate2.mtPendingArray=[[NSMutableArray alloc]init];
////    TagEncode * tag = [[TagEncode alloc]init];
////    delegate2.mtCheck=true;
////    [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.OrderDownloadRequest];
////    [tag GetBuffer];
////
////    [self newMessage];
//    [self mtOrderRequest];
//    
//    
//}
//    
//        
        
        
}
    

    


-(void)viewWillDisappear:(BOOL)animated
{
    delegate2.mainorderCheck=false;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
       return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    @try
    {
    if(segmentedControl.selectedSegmentIndex==0 && (pendingOrderIDArray.count>0||delegate2.mtPendingArray.count>0))
    {

        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
             return [pendingOrderIDArray count];
        }
        
        else
        {
            return [delegate2.mtPendingArray count];
        }

    
    }
    else if(segmentedControl.selectedSegmentIndex==1 && (orderIDArray.count>0||delegate2.mtCompletedArray.count>0))
        
    {
       if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
            return [orderIDArray count];
        }
        
        else
        {
            return [delegate2.mtCompletedArray count];
        }
        
        
        
    }
    
    else if(segmentedControl.selectedSegmentIndex==2 && (cancelledOrderIDArray.count>0||delegate2.mtCancelArray.count>0))
    {
        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
            return [cancelledOrderIDArray count];
        }
        
        else
        {
            return [delegate2.mtCancelArray count];
        }
        
    }
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    @try {
        if(segmentedControl.selectedSegmentIndex==0 && (pendingOrderIDArray.count>0||delegate2.mtPendingArray.count>0))
        {
            if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
            {
            PendingOrdersCell * cell2=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
                if(expandBool == false)
                {
                    cell2.expandImg.image =[UIImage imageNamed:@"expandCard"];
                }
                else
                {
                    cell2.expandImg.image =[UIImage imageNamed:@"collapseCard"];
                }
            cell2.orderIdLbl.text = [NSString stringWithFormat:@"%@", [pendingOrderIDArray objectAtIndex:indexPath.row]];
            cell2.timeLbl.text = [NSString stringWithFormat:@"%@", [pendingOrderTimeArray objectAtIndex:indexPath.row]];
            cell2.tradingSymLbl.text = [NSString stringWithFormat:@"%@", [pendingTradingSymArray objectAtIndex:indexPath.row]];
            
            
            
            cell2.exchangeLbl.text = [NSString stringWithFormat:@"%@", [pendingExchangeArray objectAtIndex:indexPath.row]];
                
                NSString * orderTypeStr = [NSString stringWithFormat:@"Type: %@",[pendingOrderTypeArray objectAtIndex:indexPath.row]];
                cell2.orderTypeLbl.text = orderTypeStr;
                
                NSString * productTypeStr = [NSString stringWithFormat:@"Product: %@",[pendingProductArray objectAtIndex:indexPath.row]];
                cell2.deliveryLbl.text = productTypeStr;
                
                NSString * transactionStr =  [NSString stringWithFormat:@"%@", [pendingTransactionTypeArray objectAtIndex:indexPath.row]];
                cell2.transactionTypeLbl.text = transactionStr;
                  cell2.transactionTypeLbl.layer.borderWidth = 1.0f;
                if([transactionStr isEqualToString:@"BUY"])
                {
                   
                    cell2.transactionTypeLbl.layer.borderColor = buyColor.CGColor;
                    cell2.transactionTypeLbl.textColor = buyColor;
                }
                if([transactionStr isEqualToString:@"SELL"])
                {
                   
                    cell2.transactionTypeLbl.layer.borderColor = sellColor.CGColor;
                    cell2.transactionTypeLbl.textColor = sellColor;
                }
            float finalPrice=[[pendingPriceArray objectAtIndex:indexPath.row] floatValue];
                
            cell2.priceLbl.text =[NSString stringWithFormat:@"%.2f",finalPrice];
            
                cell2.qtyLbl.text = [NSString stringWithFormat:@"Total Qty: %@", [pendingTotalQtyArray objectAtIndex:indexPath.row]];
                cell2.totalQty.text = [NSString stringWithFormat:@"%@/%@", [pendingFilledArrayy objectAtIndex:indexPath.row],[pendingTotalQtyArray objectAtIndex:indexPath.row]];
            
                cell2.pendingLabel.text=[NSString stringWithFormat:@"Pending Qty: %@",[pendingPendingArray objectAtIndex:indexPath.row]];
                cell2.filledLabel.text=[NSString stringWithFormat:@"Filled Qty: %@",[pendingFilledArrayy objectAtIndex:indexPath.row]];
            
            
            [cell2.editBtn addTarget:self action:@selector(editAction:) forControlEvents:UIControlEventTouchDown];
            
            [cell2.closeBtn addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchDown];
            
            return cell2;
        }
            
            else
            {
                
                
                PendingOrdersCell * cell2=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
                if(expandBool == false)
                {
                    cell2.expandImg.image =[UIImage imageNamed:@"expandCard"];
                }
                else
                {
                    cell2.expandImg.image =[UIImage imageNamed:@"collapseCard"];
                }
                cell2.orderIdLbl.text = [NSString stringWithFormat:@"%@", [[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"order_id"]];
                NSString * dateStr= [NSString stringWithFormat:@"%@", [[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"order_timestamp"]];
//                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//                [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
//                [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
//                NSDate *date = [dateFormatter dateFromString:dateStr]; // create date from string
//
//                // change to a readable time format and change to local time zone
//                [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
//                [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
//                NSString *finalDate = [dateFormatter stringFromDate:date];
                cell2.timeLbl.text  = dateStr;
//                cell2.tradingSymLbl.text = @"AXISBANK";
                
               
                NSString * orderTypeStr = [NSString stringWithFormat:@"Type: %@",[[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"order_type"]];
                cell2.orderTypeLbl.text = orderTypeStr;
                
                NSString * productTypeStr = [NSString stringWithFormat:@"Product: %@",[[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"product"]];
                cell2.deliveryLbl.text = productTypeStr;
                
                
                cell2.exchangeLbl.text = [NSString stringWithFormat:@"%@", [[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"exchange"]];
                
                 NSString * string1=[[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"transaction_type"];
                
//                if([string1 isEqualToString:@"1"])
//                {
//                    string1 = @"BUY";
//                }
//                else if([string1 isEqualToString:@"2"])
//                {
//                    string1 = @"SELL";
//                }
                cell2.transactionTypeLbl.text = [NSString stringWithFormat:@"%@",string1];
                
                cell2.transactionTypeLbl.layer.borderWidth = 1.0f;
                if([string1 isEqualToString:@"BUY"])
                {
                    
                    cell2.transactionTypeLbl.layer.borderColor = buyColor.CGColor;
                    cell2.transactionTypeLbl.textColor = buyColor;
                }
                if([string1 isEqualToString:@"SELL"])
                {
                    
                    cell2.transactionTypeLbl.layer.borderColor = sellColor.CGColor;
                    cell2.transactionTypeLbl.textColor = sellColor;
                }
                
                cell2.tradingSymLbl.text=[[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"tradingsymbol"];
                
               
                
                
                float finalPrice=[[[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"price"] floatValue];
                cell2.priceLbl.text =[NSString stringWithFormat:@"%.2f",finalPrice];
                
                cell2.qtyLbl.text = [NSString stringWithFormat:@"Total Qty: %@", [[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"quantity"]];
                cell2.totalQty.text = [NSString stringWithFormat:@"0/%@", [[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"quantity"]];
                
                NSString * pendingStr=[NSString stringWithFormat:@"Pending Qty: %@", [[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"pending_quantity"]];
                
                if([pendingStr containsString:@"(null)"])
                {
                   cell2.pendingLabel.text=@"0";
                }
                
                else
                {
                    cell2.pendingLabel.text= pendingStr;
                    
                }
                
                
                cell2.filledLabel.text=@"Filled Qty: 0";
                
                
                [cell2.editBtn addTarget:self action:@selector(editAction:) forControlEvents:UIControlEventTouchDown];
                
                [cell2.closeBtn addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchDown];
                
                
                return cell2;
                
                
            }
        
           
        }
        else if(segmentedControl.selectedSegmentIndex==1 && (orderIDArray.count>0||delegate2.mtCompletedArray.count>0))    {
            
            
            if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
            {
            
            
            
            CompletedCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            if(expandBool == false)
            {
                cell.expandImgView.image =[UIImage imageNamed:@"expandCard"];
            }
            else
            {
                 cell.expandImgView.image =[UIImage imageNamed:@"collapseCard"];
            }
            cell.orderIdLbl.text = [NSString stringWithFormat:@"%@", [orderIDArray objectAtIndex:indexPath.row]];
            cell.timeLbl.text = [NSString stringWithFormat:@"%@", [orderTimeArray objectAtIndex:indexPath.row]];
            cell.tradingSymLbl.text = [NSString stringWithFormat:@"%@", [tradingSymArray objectAtIndex:indexPath.row]];
            cell.exchangeLbl.text = [NSString stringWithFormat:@"%@", [exchangeArray objectAtIndex:indexPath.row]];
                NSString * transactionStr = [NSString stringWithFormat:@"%@",[transactionTypeArray objectAtIndex:indexPath.row]];
            cell.transactionLbl.text = [NSString stringWithFormat:@"%@",transactionStr];
                cell.transactionLbl.layer.borderWidth = 1.0f;
                if([transactionStr isEqualToString:@"BUY"])
                {

                    cell.transactionLbl.layer.borderColor = buyColor.CGColor;
                    cell.transactionLbl.textColor = buyColor;
                }
                if([transactionStr isEqualToString:@"SELL"])
                {

                    cell.transactionLbl.layer.borderColor = sellColor.CGColor;
                    cell.transactionLbl.textColor = sellColor;
                }
                
            float finalPrice=[[priceArray objectAtIndex:indexPath.row] floatValue];
            cell.priceLbl.text =[NSString stringWithFormat:@"%.2f",finalPrice];
                cell.orderTypeLbl.text = [NSString stringWithFormat:@"Type: %@", [orderTypeArray objectAtIndex:indexPath.row]];
            cell.fillQtyLbl.text = [NSString stringWithFormat:@"%@", [filledArray objectAtIndex:indexPath.row]];
            cell.pendingQtyLbl.text = [NSString stringWithFormat:@"%@", [comPendingArray objectAtIndex:indexPath.row]];
            cell.qtyLbl.text = [NSString stringWithFormat:@"%@", [totalQtyArray objectAtIndex:indexPath.row]];
                cell.productTypeLbl.text = [NSString stringWithFormat:@"Product: %@", [completedProductArray objectAtIndex:indexPath.row]];
            
            
            return cell;
                
            }
            
            else
            {
                
                CompletedCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
                if(expandBool == false)
                {
                    cell.expandImgView.image =[UIImage imageNamed:@"expandCard"];
                }
                else
                {
                    cell.expandImgView.image =[UIImage imageNamed:@"collapseCard"];
                }
                cell.orderIdLbl.text = [NSString stringWithFormat:@"%@", [[delegate2.mtCompletedArray objectAtIndex:indexPath.row] objectForKey:@"order_id"]];
                NSString * dateStr = [NSString stringWithFormat:@"%@", [[delegate2.mtCompletedArray objectAtIndex:indexPath.row] objectForKey:@"order_timestamp"]];
                
//                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//                [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
//                [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
//                NSDate *date = [dateFormatter dateFromString:dateStr]; // create date from string
//
//                // change to a readable time format and change to local time zone
//                [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
//                [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
//                NSString *finalDate = [dateFormatter stringFromDate:date];
                cell.timeLbl.text  = dateStr;
                
                NSString * string=[NSString stringWithFormat:@"Type: %@",[[delegate2.mtCompletedArray objectAtIndex:indexPath.row] objectForKey:@"order_type"]];
                cell.orderTypeLbl.text = string;
                
                NSString * productStr =[NSString stringWithFormat:@"%@", [[delegate2.mtCompletedArray objectAtIndex:indexPath.row] objectForKey:@"product_type"]];
                if([productStr isEqualToString:@"1"])
                {
                    cell.productTypeLbl.text = @"Product: Delivery";
                }
                else  if([productStr isEqualToString:@"2"])
                {
                    cell.productTypeLbl.text = @"Product: Intraday";
                }
                
          
                
                cell.exchangeLbl.text = [NSString stringWithFormat:@"%@", [[delegate2.mtCompletedArray objectAtIndex:indexPath.row] objectForKey:@"exchange"]];
                
                NSString * string1=[[delegate2.mtCompletedArray objectAtIndex:indexPath.row] objectForKey:@"transaction_type"];
//                if([string1 isEqualToString:@"1"])
//                {
//                    string1 = @"BUY";
//                }
//                else if([string1 isEqualToString:@"2"])
//                {
//                    string1 = @"SELL";
//                }
              
                cell.tradingSymLbl.text=[[delegate2.mtCompletedArray objectAtIndex:indexPath.row] objectForKey:@"tradingsymbol"];
                cell.transactionLbl.text = [NSString stringWithFormat:@"%@",string1];
                cell.transactionLbl.layer.borderWidth = 1.0f;
                if([string1 isEqualToString:@"BUY"])
                {
                    
                    cell.transactionLbl.layer.borderColor = buyColor.CGColor;
                    cell.transactionLbl.textColor = buyColor;
                }
                else if([string1 isEqualToString:@"SELL"])
                {
                    
                    cell.transactionLbl.layer.borderColor = sellColor.CGColor;
                    cell.transactionLbl.textColor = sellColor;
                }
                
               float finalPrice=[[[delegate2.mtCompletedArray objectAtIndex:indexPath.row] objectForKey:@"price"] floatValue];
                cell.priceLbl.text =[NSString stringWithFormat:@"%.2f",finalPrice];
                
//                cell.orderTypeLbl.text = [NSString stringWithFormat:@"%@", [orderTypeArray objectAtIndex:indexPath.row]];
                cell.fillQtyLbl.text =[NSString stringWithFormat:@"%@", [[delegate2.mtCompletedArray objectAtIndex:indexPath.row] objectForKey:@"filled_quantity"]];
             
                
                NSString * pendingStr=[NSString stringWithFormat:@"%@", [[delegate2.mtCompletedArray objectAtIndex:indexPath.row] objectForKey:@"pending_quantity"]];
                
                if([pendingStr isEqualToString:@"(null)"])
                {
                   
                    
                    cell.pendingQtyLbl.text=@"0";
                }
                
                else
                {
                     cell.pendingQtyLbl.text= pendingStr;
                }
                
                cell.qtyLbl.text = [NSString stringWithFormat:@"%@", [[delegate2.mtCompletedArray objectAtIndex:indexPath.row] objectForKey:@"quantity"]];
                
                
                return cell;
               
            }
            
        }
        
        else if(segmentedControl.selectedSegmentIndex==2 && (cancelledOrderIDArray.count>0||delegate2.mtCancelArray.count>0))
        {
            if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
            {
            
            CancelledOrderCell * cell1=[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                if(expandBool == false)
                {
                    cell1.expandImgView.image =[UIImage imageNamed:@"expandCard"];
                }
                else
                {
                    cell1.expandImgView.image =[UIImage imageNamed:@"collapseCard"];
                }
            cell1.orderIDLbl.text = [NSString stringWithFormat:@"%@", [cancelledOrderIDArray objectAtIndex:indexPath.row]];
            cell1.timeLbl.text = [NSString stringWithFormat:@"%@", [cancelledOrderTimeArray objectAtIndex:indexPath.row]];
            cell1.companyLbl.text = [NSString stringWithFormat:@"%@", [cancelledTradingSymArray objectAtIndex:indexPath.row]];
                
            cell1.exchangeLbl.text = [NSString stringWithFormat:@"%@", [cancelledExchangeArray objectAtIndex:indexPath.row]];
            NSString * orderTypeStr = [NSString stringWithFormat:@"Type: %@",[cancelledOrderTypeArray objectAtIndex:indexPath.row]];
            cell1.orderTypeLbl.text = orderTypeStr;
                
            NSString * productTypeStr = [NSString stringWithFormat:@"Product: %@",[cancelledProductArray objectAtIndex:indexPath.row]];
            cell1.productType.text = productTypeStr;
                
            if(pendingMessageArray.count>0)
            {
                NSString * messageStr=[NSString stringWithFormat:@"Reason: %@",[pendingMessageArray objectAtIndex:indexPath.row]];
                cell1.reasonLbl.text = messageStr;
                
            }
                    
                    
            NSString * transactionStr = [NSString stringWithFormat:@"%@",[cancelledTransactionTypeArray objectAtIndex:indexPath.row]];
            cell1.transactionLbl.text = [NSString stringWithFormat:@"%@", transactionStr];
                cell1.transactionLbl.layer.borderWidth = 1.0f;
                if([transactionStr isEqualToString:@"BUY"])
                {
                    
                    cell1.transactionLbl.layer.borderColor = buyColor.CGColor;
                    cell1.transactionLbl.textColor = buyColor;
                }
                if([transactionStr isEqualToString:@"SELL"])
                {
                    
                    cell1.transactionLbl.layer.borderColor = sellColor.CGColor;
                    cell1.transactionLbl.textColor = sellColor;
                }
                
            float finalPrice=[[cancelledPriceArray objectAtIndex:indexPath.row] floatValue];
            cell1.priceLbl.text =[NSString stringWithFormat:@"%.2f",finalPrice];
            
            cell1.qtyLbl.text = [NSString stringWithFormat:@"%@", [cancelledTotalQtyArray objectAtIndex:indexPath.row]];
                
                 return cell1;
            
            }
            
            else
            {
                CancelledOrderCell * cell1=[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                if(expandBool == false)
                {
                    cell1.expandImgView.image =[UIImage imageNamed:@"expandCard"];
                }
                else
                {
                    cell1.expandImgView.image =[UIImage imageNamed:@"collapseCard"];
                }
                cell1.orderIDLbl.text = [NSString stringWithFormat:@"%@", [[delegate2.mtCancelArray objectAtIndex:indexPath.row] objectForKey:@"order_id"]];
                NSString * dateStr = [NSString stringWithFormat:@"%@", [[delegate2.mtCancelArray objectAtIndex:indexPath.row] objectForKey:@"order_timestamp"]];
              
//                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//                [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
//                [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
//                NSDate *date = [dateFormatter dateFromString:dateStr]; // create date from string
//
//                // change to a readable time format and change to local time zone
//                [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
//                [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
//                NSString *finalDate = [dateFormatter stringFromDate:date];
                cell1.timeLbl.text  = dateStr;
                NSString * orderTypeString=[NSString stringWithFormat:@"Type: %@",[[delegate2.mtCancelArray objectAtIndex:indexPath.row] objectForKey:@"order_type"]];
                
                cell1.orderTypeLbl.text = orderTypeString;
                
                NSString * productStr =[NSString stringWithFormat:@"%@", [[delegate2.mtCancelArray objectAtIndex:indexPath.row] objectForKey:@"product_type"]];
                if([productStr isEqualToString:@"1"])
                {
                    cell1.productType.text = @"Product: Delivery";
                }
                else  if([productStr isEqualToString:@"2"])
                {
                    cell1.productType.text = @"Product: Intraday";
                }
                
                
              NSString * reasonTypeString=[NSString stringWithFormat:@"Reason: %@", [[delegate2.mtCancelArray objectAtIndex:indexPath.row] objectForKey:@"status_message"]];
                 cell1.reasonLbl.text = reasonTypeString;
                
                cell1.companyLbl.text=[NSString stringWithFormat:@"%@",[[delegate2.mtCancelArray objectAtIndex:indexPath.row]objectForKey:@"tradingsymbol"]];
                
//                NSString * productStr =[NSString stringWithFormat:@"%@", [[delegate2.mtCancelArray objectAtIndex:indexPath.row] objectForKey:@"product_type"]];
//                if([productStr isEqualToString:@"1"])
//                {
//                    cell1.productTypeLbl.text = @"Product: Delivery";
//                }
//                else  if([productStr isEqualToString:@"2"])
//                {
//                    cell1.productTypeLbl.text = @"Product: Intraday";
//                }
//
//
                
                cell1.exchangeLbl.text = [NSString stringWithFormat:@"%@", [[delegate2.mtCancelArray objectAtIndex:indexPath.row] objectForKey:@"exchange"]];
                
                NSString * string1=[[delegate2.mtCancelArray objectAtIndex:indexPath.row] objectForKey:@"transaction_type"];
//                if([string1 isEqualToString:@"1"])
//                {
//                    string1 = @"BUY";
//                }
//                else if([string1 isEqualToString:@"2"])
//                {
//                    string1 = @"SELL";
//                }
                cell1.transactionLbl.text = [NSString stringWithFormat:@"%@",string1];
                cell1.transactionLbl.layer.borderWidth = 1.0f;
                if([string1 isEqualToString:@"BUY"])
                {
                    
                    cell1.transactionLbl.layer.borderColor = buyColor.CGColor;
                    cell1.transactionLbl.textColor = buyColor;
                }
                if([string1 isEqualToString:@"SELL"])
                {
                    
                    cell1.transactionLbl.layer.borderColor = sellColor.CGColor;
                    cell1.transactionLbl.textColor = sellColor;
                }
                float finalPrice=[[[delegate2.mtCancelArray objectAtIndex:indexPath.row] objectForKey:@"price"] floatValue];
                cell1.priceLbl.text =[NSString stringWithFormat:@"%.2f",finalPrice];
                
                cell1.qtyLbl.text = [NSString stringWithFormat:@"%@", [[delegate2.mtCancelArray objectAtIndex:indexPath.row] objectForKey:@"quantity"]];
                
                return cell1;
                
            }
            
            
           
        }
        
        else
        {
            NoOrderImg * cell1=[tableView dequeueReusableCellWithIdentifier:@"CELL3" forIndexPath:indexPath];
            
            return cell1;
        }

        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
       return 0;
    
             }


- (void)tableView: (UITableView*)tableView willDisplayCell: (UITableViewCell*)cell forRowAtIndexPath: (NSIndexPath*)indexPath
{
    
//
//    if(indexPath.row % 2 == 0)
//
//
//
//     cell.backgroundColor = [UIColor whiteColor];
//    else
//    {
//        cell.backgroundColor = [UIColor colorWithRed:(245/255.0) green:(245/255.0) blue:(245/255.0) alpha:1];
//
//    }
//
}

-(void)segmentedControlChangedValue
{
    @try
    {
    self.pendingOrdersTableView.hidden=YES;
    self.activityInd.hidden=NO;
    [self.activityInd startAnimating];
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];

    if(segmentedControl.selectedSegmentIndex==0)
    {
            [mixpanelMini track:@"pending_order_page"];
        
        self.pendingView.hidden=NO;
        self.completedView.hidden=YES;
        self.cancelView.hidden=YES;
        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
           
            [self RetrieveAllOrders];
        }
        
        else
        {
           
//            delegate2.mtPendingArray=[[NSMutableArray alloc]init];
            [self mtOrderRequest];
            
           
            
        }
    
//        [self.pendingOrdersTableView reloadData];
    }
    
    else if(segmentedControl.selectedSegmentIndex==1)
    {
         [mixpanelMini track:@"completed_order_page"];
       if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
            [self RetrieveAllOrders];
        }
        
        else
        {
            
//            delegate2.mtCompletedArray=[[NSMutableArray alloc]init];
             [self mtOrderRequest];
            
        }

        self.pendingView.hidden=YES;
        self.completedView.hidden=NO;
        self.cancelView.hidden=YES;
        
//        [self.pendingOrdersTableView reloadData];

    }
    
    else if(segmentedControl.selectedSegmentIndex==2)
    {
        
         [mixpanelMini track:@"cancelled_order_page"];
        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
             [self RetrieveAllOrders];
        }
        
        else
        {
           
//            delegate2.mtCancelArray=[[NSMutableArray alloc]init];
            [self mtOrderRequest];
            
        }
        
       

        self.pendingView.hidden=YES;
        self.completedView.hidden=YES;
        self.cancelView.hidden=NO;
        
//        [self.pendingOrdersTableView reloadData];
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
   }

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(segmentedControl.selectedSegmentIndex==0&& (pendingOrderIDArray.count>0||delegate2.mtPendingArray.count>0))
    {
        if(indexPath == selectedIndex) {
            
            return 180; //Size you want to increase to
        }
        else
        {
            return 102;
        }
    }
   else  if(segmentedControl.selectedSegmentIndex==1&&(orderIDArray.count>0||delegate2.mtCompletedArray.count>0))
    {
        if(indexPath == selectedIndex) {
            
            return 180; //Size you want to increase to
        }
        else
        {
        return 104;
        }
    }
    
   else  if(segmentedControl.selectedSegmentIndex==2&&(cancelledOrderIDArray.count>0||delegate2.mtCancelArray.count>0))
    {
        if(indexPath == selectedIndex) {
            
            return 220; //Size you want to increase to
        }
        else
        {
            return 104;
        }
    }
    return 408;
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backToMainView:(id)sender {
//    

    
    
    
    
  
//
    //now present this navigation controller modally
//    [self presentViewController:tabPage
//                       animated:YES
//                     completion:^{
//                         
//                     }];
    
//    [self showDetailViewController:tabPage sender:nil];
//    if([delegate2.holdingCheck isEqualToString:@"hold"])
//    {
//        delegate2.holdingCheck=@"";
//        delegate2.orderToPort=@"ordercheck";
//
//            PortfolioView * port=[self.storyboard instantiateViewControllerWithIdentifier:@"port"];
//
//            [self presentViewController:port animated:YES completion:nil];
//    }
//
//    else if([delegate2.holdingCheck isEqualToString:@"position"])
//    {
//        delegate2.holdingCheck=@"";
//        delegate2.orderToPort=@"positionCheck";
//
//        PortfolioView * port=[self.storyboard instantiateViewControllerWithIdentifier:@"port"];
//
//        [self presentViewController:port animated:YES completion:nil];
//    }
    
   if([delegate2.modifyCheck isEqualToString:@"check"])
    {
        delegate2.modifyCheck=@"";
        
            TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
        
            [self presentViewController:tabPage animated:YES completion:nil];
    }
    
    else
    {
    
    delegate2.dismissCheck=@"dismissed";
   
   [self dismissViewControllerAnimated:true completion:^{
       
   }];
        
    }
//    StockOrderView * sov = [self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
//    
//    [sov.navigationController popViewControllerAnimated:YES];
//    if([delegate2.navigationCheck isEqualToString:@"Marketwatch"])
//    {
//        
//            StockOrderView * sov = [self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
//        
//        [sov navigationMethod];
//        
//    }
    
   
    
   
    
 
    
    
//    [self navigationController].viewControllers = [[self navigationController].viewControllers arrayByAddingObject:tabPage];
    
   
//

    
    
    
    
    
    
}

- (IBAction)searchAction:(id)sender {
}

//get all orders//

-(void)RetrieveAllOrders
{
    
     mixpanelCancelledArray=[[NSMutableArray alloc]init];
    mixpanelCompletedArray=[[NSMutableArray alloc]init];
    mixpanelPendingArray=[[NSMutableArray alloc]init];

    @try {
        [cancelledOrderIDArray removeAllObjects];
        [cancelledTradingSymArray removeAllObjects];
        [cancelledOrderTimeArray removeAllObjects];
        [cancelledExchangeArray removeAllObjects];
        [cancelledFilledArray removeAllObjects];
        [cancelledPendingArray removeAllObjects];
        [cancelledOrderTypeArray removeAllObjects];
        [cancelledTransactionTypeArray removeAllObjects];
        [cancelledPriceArray removeAllObjects];
        [cancelledTotalQtyArray removeAllObjects];
        [cancelledProductArray removeAllObjects];
        
        [orderIDArray removeAllObjects];
        [tradingSymArray removeAllObjects];
        [orderTimeArray removeAllObjects];
        [exchangeArray removeAllObjects];
        [filledArray removeAllObjects];
        [comPendingArray removeAllObjects];
        [orderTypeArray removeAllObjects];
        [transactionTypeArray removeAllObjects];
        [priceArray removeAllObjects];
        [totalQtyArray removeAllObjects];
        [completedProductArray removeAllObjects];
        
        [ pendingOrderIDArray removeAllObjects];
        [ pendingTradingSymArray removeAllObjects];
        [ pendingOrderTimeArray removeAllObjects];
        [ pendingExchangeArray removeAllObjects];
        [ pendingFilledArray removeAllObjects];
        [ pendingPendingArray removeAllObjects];
        [ pendingOrderTypeArray removeAllObjects];
        [ pendingTransactionTypeArray removeAllObjects];
        [ pendingPriceArray removeAllObjects];
        [ pendingTotalQtyArray removeAllObjects];
        [pendingLabelArray removeAllObjects];
        [pendingFilledArrayy removeAllObjects];
        [instrumentTokenn removeAllObjects];
        [pendingMessageArray removeAllObjects];
        [pendingProductArray removeAllObjects];
        NSURL *url;
        
          NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
        NSDictionary * headers;

        NSURLSession *session = [NSURLSession sharedSession];
        
        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
        {
       url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.kite.trade/orders?api_key=xt2q1dpbwzu69n97&access_token=%@",delegate2.accessToken]];
            
        }
        
        else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.upstox.com/live/orders"]];
            
          headers  = @{ @"cache-control": @"no-cache",
                 @"Content-Type" : @"application/json",
                 @"authorization":access,
                 @"x-api-key":delegate2.APIKey
                 };
        }
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:40.0];
        
   
        [request setHTTPMethod:@"GET"];
        if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
        [request setAllHTTPHeaderFields:headers];
        }
        
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(data!=nil)
            {
            
            NSDictionary *orderResponse=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            //NSLog(@"orderDict----%@",orderResponse);
            
            orderArray = [orderResponse valueForKey:@"data"];
            
            orderArray=[[[orderArray reverseObjectEnumerator] allObjects] mutableCopy];
            
            for (int i = 0; i<[orderArray count]; i++)
            {
                NSString * status=[[orderArray objectAtIndex:i]valueForKey:@"status"];
                
                if([status isEqualToString:@"COMPLETE"]||[status isEqualToString:@"complete"])
                {
                    
                    
                    
                    
                    orderIdStr = [[orderArray objectAtIndex:i]valueForKey:@"order_id"];
                    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                    {
                       tradingSymStr = [[orderArray objectAtIndex:i]valueForKey:@"tradingsymbol"];
                          orderTimeStr = [[orderArray objectAtIndex:i]valueForKey:@"order_timestamp"];
                          exchangeStr = [[orderArray objectAtIndex:i]valueForKey:@"exchange"];
                         fillQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"filled_quantity"]stringValue];
                         pendingQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"pending_quantity"]stringValue];
                        orderTypeStr = [[orderArray objectAtIndex:i]valueForKey:@"order_type"];
                        transactionTypeStr = [[orderArray objectAtIndex:i]valueForKey:@"transaction_type"];
                        completedProductStr = [[orderArray objectAtIndex:i]valueForKey:@"product"];
                        if([completedProductStr isEqualToString:@"MIS"])
                        {
                            completedProductStr=@"Intraday";
                        }
                        else if([completedProductStr isEqualToString:@"CNC"])
                        {
                            completedProductStr=@"Delivery";
                        }
                        
                    }
                    
                    else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
                    {
                        tradingSymStr = [[orderArray objectAtIndex:i]valueForKey:@"symbol"];
                        orderTimeStr = [[orderArray objectAtIndex:i]valueForKey:@"exchange_time"];;
                        
                        
                          exchangeStr = [[orderArray objectAtIndex:i]valueForKey:@"exchange"];
                        
                        if([exchangeStr isEqualToString:@"NSE_EQ"])
                        {
                            exchangeStr=@"NSE";
                        }
                        
                        else if([exchangeStr isEqualToString:@"BSE_EQ"])
                        {
                            exchangeStr=@"BSE";
                        }
                        
                        else if([exchangeStr isEqualToString:@"NSE_FO"])
                        {
                            exchangeStr=@"NFO";
                        }
                        else if([exchangeStr isEqualToString:@"BSE_FO"])
                        {
                            exchangeStr=@"BFO";
                        }
                         fillQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"traded_quantity"]stringValue];
                        pendingQtyStr = @"---";
                        
                        orderTypeStr = [[orderArray objectAtIndex:i]valueForKey:@"order_type"];
                        
                        if([orderTypeStr isEqualToString:@"M"])
                        {
                            orderTypeStr=@"MARKET";
                        }
                        else if([orderTypeStr isEqualToString:@"L"])
                        {
                            orderTypeStr=@"LIMIT";
                        }
                        
                        else if([orderTypeStr isEqualToString:@"SL"])
                        {
                            orderTypeStr=@"SL";
                        }
                        
                        completedProductStr = [[orderArray objectAtIndex:i]valueForKey:@"product"];
                        
                        if([completedProductStr isEqualToString:@"I"])
                        {
                            completedProductStr=@"Intraday";
                        }
                        else if([completedProductStr isEqualToString:@"D"])
                        {
                            completedProductStr=@"Delivery";
                        }
                        
                       
                        
                        transactionTypeStr = [[orderArray objectAtIndex:i]valueForKey:@"transaction_type"];
                        
                        if([transactionTypeStr isEqualToString:@"B"])
                        {
                            transactionTypeStr=@"BUY";
                        }
                        
                        else if([transactionTypeStr isEqualToString:@"S"])
                        {
                            transactionTypeStr=@"SELL";
                        }
                        
                        
                        
                    }
                    
                
                    priceStr = [[[orderArray objectAtIndex:i]valueForKey:@"price"]stringValue];
                    totalQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"quantity"]stringValue];
                    
                    
                    [orderIDArray addObject:orderIdStr];
                    [tradingSymArray addObject:tradingSymStr];
                    [orderTimeArray addObject:orderTimeStr];
                    [exchangeArray addObject:exchangeStr];
                    [filledArray addObject:fillQtyStr];
                    [comPendingArray addObject:pendingQtyStr];
                    [orderTypeArray addObject:orderTypeStr];
                    [transactionTypeArray addObject:transactionTypeStr];
                    [priceArray addObject:priceStr];
                    [totalQtyArray addObject:totalQtyStr];
                    [completedProductArray addObject:completedProductStr];
                }
                
                else if([status isEqualToString:@"CANCELLED"]||[status isEqualToString:@"REJECTED"]||[status isEqualToString:@"cancelled"]||[status isEqualToString:@"rejected"])
                {
                    
                   
                   
                    cancelledOrderIdStr = [[orderArray objectAtIndex:i]valueForKey:@"order_id"];
                    
                    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                    {
                        cancelledTradingSymStr = [[orderArray objectAtIndex:i]valueForKey:@"tradingsymbol"];
                        cancelledOrderTimeStr = [[orderArray objectAtIndex:i]valueForKey:@"order_timestamp"];
                        cancelledExchangeStr = [[orderArray objectAtIndex:i]valueForKey:@"exchange"];
                        cancelledFillQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"filled_quantity"]stringValue];
                        cancelledPendingQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"pending_quantity"]stringValue];
                        cancelledOrderTypeStr = [[orderArray objectAtIndex:i]valueForKey:@"order_type"];
                        
                        cancelledTransactionTypeStr = [[orderArray objectAtIndex:i]valueForKey:@"transaction_type"];
                        
                        pendingMessageStr = [[orderArray objectAtIndex:i]objectForKey:@"status_message"];
                        cancelledProductStr = [[orderArray objectAtIndex:i]valueForKey:@"product"];
                        
                        if([cancelledProductStr isEqualToString:@"MIS"])
                        {
                            cancelledProductStr=@"Intraday";
                        }
                        else if([cancelledProductStr isEqualToString:@"CNC"])
                        {
                            cancelledProductStr=@"Delivery";
                        }
                        //NSLog(@"%@",[[orderArray objectAtIndex:i]objectForKey:@"status_message"]);
                    }
                    
                    else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
                    {
                        cancelledTradingSymStr = [[orderArray objectAtIndex:i]valueForKey:@"symbol"];
                        cancelledOrderTimeStr =[[orderArray objectAtIndex:i]valueForKey:@"exchange_time"];
                        
                       
                        cancelledExchangeStr = [[orderArray objectAtIndex:i]valueForKey:@"exchange"];
                        
                        if([cancelledExchangeStr isEqualToString:@"NSE_EQ"])
                        {
                            cancelledExchangeStr=@"NSE";
                        }
                        
                        else if([cancelledExchangeStr isEqualToString:@"BSE_EQ"])
                        {
                            cancelledExchangeStr=@"BSE";
                        }
                        
                        else if([cancelledExchangeStr isEqualToString:@"NSE_FO"])
                        {
                            cancelledExchangeStr=@"NFO";
                        }
                        else if([cancelledExchangeStr isEqualToString:@"BSE_FO"])
                        {
                            cancelledExchangeStr=@"BFO";
                        }
                        cancelledFillQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"traded_quantity"]stringValue];
                        cancelledPendingQtyStr = @"---";
                        
                        cancelledOrderTypeStr = [[orderArray objectAtIndex:i]valueForKey:@"order_type"];
                        
                        if([cancelledOrderTypeStr isEqualToString:@"M"])
                        {
                            cancelledOrderTypeStr=@"MARKET";
                        }
                        else if([cancelledOrderTypeStr isEqualToString:@"L"])
                        {
                            cancelledOrderTypeStr=@"LIMIT";
                        }
                        
                        else if([cancelledOrderTypeStr isEqualToString:@"SL"])
                        {
                            cancelledOrderTypeStr=@"SL";
                        }
                        
                        cancelledTransactionTypeStr = [[orderArray objectAtIndex:i]valueForKey:@"transaction_type"];
                        
                        if([cancelledTransactionTypeStr isEqualToString:@"B"])
                        {
                            cancelledTransactionTypeStr=@"BUY";
                        }
                        
                        else if([cancelledTransactionTypeStr isEqualToString:@"S"])
                        {
                            cancelledTransactionTypeStr=@"SELL";
                        }
                        
                        pendingMessageStr = [[orderArray objectAtIndex:i]objectForKey:@"message"];
                        
                        cancelledProductStr = [[orderArray objectAtIndex:i]valueForKey:@"product"];
                        
                        if([cancelledProductStr isEqualToString:@"I"])
                        {
                            cancelledProductStr=@"Intraday";
                        }
                        else if([cancelledProductStr isEqualToString:@"D"])
                        {
                            cancelledProductStr=@"Delivery";
                        }
                        //NSLog(@"%@",[[orderArray objectAtIndex:i]objectForKey:@"message"]);
                        
                    }
                    
                    
                    cancelledPriceStr = [[[orderArray objectAtIndex:i]valueForKey:@"price"]stringValue];
                    cancelledTotalQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"quantity"]stringValue];
                  
                    
                    
                    
                    [cancelledOrderIDArray addObject:cancelledOrderIdStr];
                    [cancelledTradingSymArray addObject:cancelledTradingSymStr];
                    [cancelledOrderTimeArray addObject:cancelledOrderTimeStr];
                    [cancelledExchangeArray addObject:cancelledExchangeStr];
                    [cancelledFilledArray addObject:cancelledFillQtyStr];
                    [cancelledPendingArray addObject:cancelledPendingQtyStr];
                    [cancelledOrderTypeArray addObject:cancelledOrderTypeStr];
                    [cancelledTransactionTypeArray addObject:cancelledTransactionTypeStr];
                    [cancelledPriceArray addObject:cancelledPriceStr];
                    [cancelledTotalQtyArray addObject:cancelledTotalQtyStr];
                    [ pendingMessageArray addObject: pendingMessageStr];
                      [ cancelledProductArray addObject: cancelledProductStr];
                    
                   
                    
                   
                    
                        
                   
                    
                    
                  
                    
                    
                    
                   
                    
                }
                else if([status isEqualToString:@"OPEN"]||[status isEqualToString:@"open"])
                {
                    int filledint ;
                    int pending;
                    int instrument;
                    
                    pendingOrderIdStr = [[orderArray objectAtIndex:i]valueForKey:@"order_id"];
                   
                    
                    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                    {
                        pendingTradingSymStr = [[orderArray objectAtIndex:i]valueForKey:@"tradingsymbol"];
                        pendingOrderTimeStr = [[orderArray objectAtIndex:i]valueForKey:@"order_timestamp"];
                        pendingExchangeStr = [[orderArray objectAtIndex:i]valueForKey:@"exchange"];
                        pendingFillQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"filled_quantity"]stringValue];
                        pendingPendingQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"pending_quantity"]stringValue];
                        pendingOrderTypeStr = [[orderArray objectAtIndex:i]valueForKey:@"order_type"];
                        pendingTransactionTypeStr = [[orderArray objectAtIndex:i]valueForKey:@"transaction_type"];
                        pendingPriceStr = [[[orderArray objectAtIndex:i]valueForKey:@"price"]stringValue];
                        pendingTotalQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"quantity"]stringValue];
                        NSString * filledString = [[orderArray objectAtIndex:i] valueForKey:@"filled_quantity"];
                        filledint = [filledString intValue];
                        NSString * pendingString = [[orderArray objectAtIndex:i] valueForKey:@"pending_quantity"];
                       pending  = [pendingString intValue];
                        
                        NSString * instrumentString = [[orderArray objectAtIndex:i] valueForKey:@"instrument_token"];
                         instrument = [instrumentString intValue];
                        
                        NSNumber *filledNumber = [NSNumber numberWithInt:filledint];
                        NSNumber *pendingNumber = [NSNumber numberWithInt:pending];
                        NSNumber *instrumentNumber = [NSNumber numberWithInt:instrument];
                        [pendingFilledArrayy addObject:filledNumber];
                        [pendingLabelArray addObject:pendingNumber];
                        [instrumentTokenn addObject:instrumentNumber];
                        
                        
                    }
                    
                    else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
                    {
                        pendingTradingSymStr = [[orderArray objectAtIndex:i]valueForKey:@"symbol"];
                        pendingOrderTimeStr =[[orderArray objectAtIndex:i]valueForKey:@"exchange_time"];
                        
                        
                        pendingExchangeStr = [[orderArray objectAtIndex:i]valueForKey:@"exchange"];
                        
                        if([pendingExchangeStr isEqualToString:@"NSE_EQ"])
                        {
                            pendingExchangeStr=@"NSE";
                        }
                        
                        else if([pendingExchangeStr isEqualToString:@"BSE_EQ"])
                        {
                            pendingExchangeStr=@"BSE";
                        }
                        
                        else if([pendingExchangeStr isEqualToString:@"NSE_FO"])
                        {
                            pendingExchangeStr=@"NFO";
                        }
                        else if([pendingExchangeStr isEqualToString:@"BSE_FO"])
                        {
                            pendingExchangeStr=@"BFO";
                        }
                        pendingFillQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"traded_quantity"]stringValue];
//                        pendingPendingQtyStr = @"---";
                        
                        pendingOrderTypeStr = [[orderArray objectAtIndex:i]valueForKey:@"order_type"];
                        
                        if([pendingOrderTypeStr isEqualToString:@"M"])
                        {
                            pendingOrderTypeStr=@"MARKET";
                        }
                        else if([pendingOrderTypeStr isEqualToString:@"L"])
                        {
                            pendingOrderTypeStr=@"LIMIT";
                        }
                        
                        else if([pendingOrderTypeStr isEqualToString:@"SL"])
                        {
                            pendingOrderTypeStr=@"SL";
                        }
                        
                        pendingTransactionTypeStr = [[orderArray objectAtIndex:i]valueForKey:@"transaction_type"];
                        
                        if([pendingTransactionTypeStr isEqualToString:@"B"])
                        {
                            pendingTransactionTypeStr=@"BUY";
                        }
                        
                        else if([pendingTransactionTypeStr isEqualToString:@"S"])
                        {
                            pendingTransactionTypeStr=@"SELL";
                        }
                        pendingProductStr = [[orderArray objectAtIndex:i]valueForKey:@"product"];
                        
                        if([pendingProductStr isEqualToString:@"I"])
                        {
                            pendingProductStr=@"Intraday";
                        }
                        else if([pendingProductStr isEqualToString:@"D"])
                        {
                            pendingProductStr=@"Delivery";
                        }
                        NSString * instrumentString = [[orderArray objectAtIndex:i] valueForKey:@"token"];
                        instrument = [instrumentString intValue];
                        
                        NSString * filledString = [[orderArray objectAtIndex:i] valueForKey:@"traded_quantity"];
                        filledint = [filledString intValue];
                        NSString * pendingString = @"--";
                        pending  = [pendingString intValue];
                        
                        NSNumber *filledNumber = [NSNumber numberWithInt:filledint];
//                        NSNumber *pendingNumber = [NSNumber numberWithInt:pending];
                        NSNumber *instrumentNumber = [NSNumber numberWithInt:instrument];
                        
                        NSNumber *pendingNumber = [NSNumber numberWithInt:pending];
                        [pendingFilledArrayy addObject:filledNumber];
                        [pendingLabelArray addObject:pendingNumber];
                        [instrumentTokenn addObject:instrumentNumber];
                        
                       
                        [instrumentTokenn addObject:instrumentNumber];
                        pendingTotalQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"quantity"]stringValue];
                        pendingPendingQtyStr =[NSString stringWithFormat:@"%i",[pendingTotalQtyStr intValue]-[filledString intValue]];
                        
                     
                    }
                    
                    
                    pendingPriceStr = [[[orderArray objectAtIndex:i]valueForKey:@"price"]stringValue];
                    
                    pendingTotalQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"quantity"]stringValue];
                   
                
                    [ pendingOrderIDArray addObject: pendingOrderIdStr];
                    [ pendingTradingSymArray addObject: pendingTradingSymStr];
                    [ pendingOrderTimeArray addObject: pendingOrderTimeStr];
                    [ pendingExchangeArray addObject: pendingExchangeStr];
                    [ pendingFilledArray addObject: pendingFillQtyStr];
                    [ pendingPendingArray addObject: pendingPendingQtyStr];
                    [ pendingOrderTypeArray addObject: pendingOrderTypeStr];
                    [ pendingTransactionTypeArray addObject: pendingTransactionTypeStr];
                    [ pendingPriceArray addObject: pendingPriceStr];
                    [ pendingTotalQtyArray addObject: pendingTotalQtyStr];
                    [ pendingProductArray addObject: pendingProductStr];
                    
                    
                 
                    
                    
                }
                
            }
                
//                for(int i=0;i<cancelledTradingSymArray.count;i++)
//                {
//                NSDictionary * tmp_Dict=@{@"Symbol":[cancelledTradingSymArray objectAtIndex:i],@"Qty":[cancelledTotalQtyArray objectAtIndex:i],@"Transaction time":[cancelledOrderTimeArray objectAtIndex:i],@"Transaction type":[cancelledTransactionTypeArray objectAtIndex:i],@"Exchange":[cancelledExchangeArray objectAtIndex:i],@"Price":[cancelledPriceArray objectAtIndex:i]};
//
//                [mixpanelCancelledArray addObject:tmp_Dict];
//
//                }
//
//                for(int i=0;i<pendingOrderIDArray.count;i++)
//                {
//                    NSDictionary * tmp_Dict=@{@"Symbol":[pendingTradingSymArray objectAtIndex:i],@"Qty":[pendingTotalQtyArray objectAtIndex:i],@"Transaction time":[pendingOrderTimeArray objectAtIndex:i],@"Transaction type":[pendingTransactionTypeArray objectAtIndex:i],@"Exchange":[pendingExchangeArray objectAtIndex:i],@"Price":[pendingPriceArray objectAtIndex:i]};
//
//                    [mixpanelPendingArray addObject:tmp_Dict];
//
//                }
//
//
//                for(int i=0;i<orderIDArray.count;i++)
//                {
//                    NSDictionary * tmp_Dict=@{@"Symbol":[tradingSymArray objectAtIndex:i],@"Qty":[totalQtyArray objectAtIndex:i],@"Transaction time":[orderTimeArray objectAtIndex:i],@"Transaction type":[transactionTypeArray objectAtIndex:i],@"Exchange":[exchangeArray objectAtIndex:i],@"Price":[priceArray objectAtIndex:i]};
//
//                    [mixpanelCompletedArray addObject:tmp_Dict];
//
//                }
                
              
                Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
                
                [mixpanelMini identify:delegate2.userID];
                
                NSMutableArray * pendingArray = [[NSMutableArray alloc]init];
                NSMutableArray * completedArray = [[NSMutableArray alloc]init];
                NSMutableArray * cancelledArray = [[NSMutableArray alloc]init];
                
                for (int i=0; i<pendingOrderIDArray.count; i++) {
                    NSMutableDictionary * pendingDict = [[NSMutableDictionary alloc]init];
                    [pendingDict setObject:[NSString stringWithFormat:@"%@",[pendingTradingSymArray objectAtIndex:i]] forKey:@"symbol_name"];
                     [pendingDict setObject:[NSString stringWithFormat:@"%@",[pendingFilledArrayy objectAtIndex:i]] forKey:@"filled_quantity"];
                     [pendingDict setObject:[NSString stringWithFormat:@"%@",[pendingPriceArray objectAtIndex:i]] forKey:@"avg_price"];
                     [pendingDict setObject:[NSString stringWithFormat:@"%@",[pendingTransactionTypeArray objectAtIndex:i]] forKey:@"transaction_type"];
                     [pendingDict setObject:[NSString stringWithFormat:@"%@",[pendingLabelArray objectAtIndex:i]] forKey:@"pending_quantity"];
                     [pendingDict setObject:[NSString stringWithFormat:@"%@",[pendingTotalQtyArray objectAtIndex:i]] forKey:@"total_quantity"];
                     [pendingDict setObject:[NSString stringWithFormat:@"%@",[pendingOrderTimeArray objectAtIndex:i]] forKey:@"date_time"];
                     [pendingArray addObject:pendingDict];
                }
                
                for (int i=0; i<orderIDArray.count; i++) {
                    NSMutableDictionary * pendingDict = [[NSMutableDictionary alloc]init];
                    [pendingDict setObject:[NSString stringWithFormat:@"%@",[tradingSymArray objectAtIndex:i]] forKey:@"symbol_name"];
                    [pendingDict setObject:[NSString stringWithFormat:@"%@",[filledArray objectAtIndex:i]] forKey:@"filled_quantity"];
                    [pendingDict setObject:[NSString stringWithFormat:@"%@",[priceArray objectAtIndex:i]] forKey:@"avg_price"];
                    [pendingDict setObject:[NSString stringWithFormat:@"%@",[transactionTypeArray objectAtIndex:i]] forKey:@"transaction_type"];
                    [pendingDict setObject:[NSString stringWithFormat:@"%@",[comPendingArray objectAtIndex:i]] forKey:@"pending_quantity"];
                    [pendingDict setObject:[NSString stringWithFormat:@"%@",[totalQtyArray objectAtIndex:i]] forKey:@"total_quantity"];
                    [pendingDict setObject:[NSString stringWithFormat:@"%@",[orderTimeArray objectAtIndex:i]] forKey:@"date_time"];
                    [completedArray addObject:pendingDict];
                }
                
                for (int i=0; i<cancelledTradingSymArray.count; i++) {
                    NSMutableDictionary * pendingDict = [[NSMutableDictionary alloc]init];
                    [pendingDict setObject:[NSString stringWithFormat:@"%@",[cancelledTradingSymArray objectAtIndex:i]] forKey:@"symbol_name"];
                    [pendingDict setObject:[NSString stringWithFormat:@"%@",@"0.0"] forKey:@"filled_quantity"];
                    [pendingDict setObject:[NSString stringWithFormat:@"%@",[cancelledPriceArray objectAtIndex:i]] forKey:@"avg_price"];
                    [pendingDict setObject:[NSString stringWithFormat:@"%@",[cancelledTransactionTypeArray objectAtIndex:i]] forKey:@"transaction_type"];
                    [pendingDict setObject:[NSString stringWithFormat:@"%@",@"0.0"] forKey:@"pending_quantity"];
                    [pendingDict setObject:[NSString stringWithFormat:@"%@",[cancelledTotalQtyArray objectAtIndex:i]] forKey:@"total_quantity"];
                    [pendingDict setObject:[NSString stringWithFormat:@"%@",[cancelledOrderTimeArray objectAtIndex:i]] forKey:@"date_time"];
                    [cancelledArray addObject:pendingDict];
                }
                
                [mixpanelMini.people set:@{@"cancelledorders":cancelledArray}];

                [mixpanelMini.people set:@{@"completedorders":completedArray}];
                [mixpanelMini.people set:@{@"pendingorders":pendingArray}];
            
            //NSLog(@"cance; %@",pendingMessageArray);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                self.pendingOrdersTableView.hidden=NO;
                self.activityInd.hidden=YES;
                [self.activityInd stopAnimating];
                
                self.pendingOrdersTableView.delegate=self;
                self.pendingOrdersTableView.dataSource=self;
                
                [self.pendingOrdersTableView reloadData];
                
            });
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    
                    UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self RetrieveAllOrders];
                    }];
                    
                    UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    }];
                    
                    [alert addAction:retryAction];
                     [alert addAction:cancel];
                    
                });
                
                
            }
            
            
        }];
        
        [postDataTask resume];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
   
    
   
    
    
}


//edit action//
-(void)editAction:(UIButton*)sender
{
    @try
    {
    delegate2.editOrderBool=true;
    delegate2.modifyCheck=@"check";


    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.pendingOrdersTableView];



    NSIndexPath *indexPath = [self.pendingOrdersTableView indexPathForRowAtPoint:buttonPosition];
    PendingOrdersCell *cell = [self.pendingOrdersTableView cellForRowAtIndexPath:indexPath];

    

        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:@"Message"
                                             message:@"Zerodha does not allow to modify or cancel pending orders from Zambala"
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                //Add Buttons
                
                UIAlertAction* okButton = [UIAlertAction
                                           actionWithTitle:@"ok"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
                                               
                                               //                                    delegate2.holdingCheck=@"hold";
                                               //Handle your yes please button action here
                                               
                                               [self.navigationController popToRootViewControllerAnimated:YES];
                                               
                                           }];
                //Add your buttons to alert controller
                
                [alert addAction:okButton];
                
                
                
                
                [self presentViewController:alert animated:YES completion:nil];
            });
        }
        else  if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
            delegate2.instrumentDepthStr=[NSString stringWithFormat:@"%@",[instrumentTokenn objectAtIndex:indexPath.row]];
            
            delegate2.symbolDepthStr=cell.tradingSymLbl.text;
            
            //    FollowCell *cell = [self.followerTableView cellForRowAtIndexPath:indexPath];
            delegate2.orderSegment=cell.exchangeLbl.text;
            
            //NSLog(@"%@",delegate2.orderSegment);
            
            delegate2.orderID=[NSString stringWithFormat:@"%@",[pendingOrderIDArray objectAtIndex:indexPath.row]];
            
            delegate2.qtyStr=[NSString stringWithFormat:@"%@",[pendingTotalQtyArray objectAtIndex:indexPath.row]];
            
            //NSLog(@"%@",delegate2.qtyStr);
            
            delegate2.transaction=[NSString stringWithFormat:@"%@",[pendingTransactionTypeArray objectAtIndex:indexPath.row]];
            
            delegate2.editPrice=[NSString stringWithFormat:@"%@",[pendingPriceArray objectAtIndex:indexPath.row]];
            
            
            StockOrderView * sov = [self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
            [self presentViewController:sov animated:YES completion:nil];
        }

        else
        {
            delegate2.instrumentDepthStr=[NSString stringWithFormat:@"%@",[[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"exchangetoken"]];

            delegate2.modifyATINORDERNO=[[[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"inatinorderid"]intValue];


            delegate2.symbolDepthStr=cell.tradingSymLbl.text;
            delegate2.orderSegment=[NSString stringWithFormat:@"%@",[[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"exchange"]];

            delegate2.qtyStr=[NSString stringWithFormat:@"%@",[[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"quantity"]];

             NSString * string=[NSString stringWithFormat:@"%@",[[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"transaction_type"]];

            

            delegate2.transaction=string;

            delegate2.editPrice=[NSString stringWithFormat:@"%@",[[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"price"]];

            StockOrderView * sov = [self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
            [self presentViewController:sov animated:YES completion:nil];


        }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
   
}

//cancel action//

-(void)cancelAction:(UIButton*)sender
{
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
    {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Message"
                                         message:@"Zerodha does not allow to modify or cancel pending orders from Zambala Stocks"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            //Add Buttons
            
            UIAlertAction* okButton = [UIAlertAction
                                       actionWithTitle:@"ok"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                           //                                    delegate2.holdingCheck=@"hold";
                                           //Handle your yes please button action here
                                           
                                           [self.navigationController popToRootViewControllerAnimated:YES];
                                           
                                       }];
            //Add your buttons to alert controller
            
            [alert addAction:okButton];
            
            
            
            
            [self presentViewController:alert animated:YES completion:nil];
        });
    }
    else
    {
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Cancel Order"
                                 message:@"Do you want to cancel the order"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    
                                    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.pendingOrdersTableView];
                                    
                                    
                                    
                                    NSIndexPath *indexPath = [self.pendingOrdersTableView indexPathForRowAtPoint:buttonPosition];
//                                    PendingOrdersCell *cell = [self.pendingOrdersTableView cellForRowAtIndexPath:indexPath];
                                    
                                    @try {
                                        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
                                        {
                                        
                                        NSString * orderId=[pendingOrderIDArray objectAtIndex:indexPath.row];
                                            
                                            NSDictionary *headers;
                                            NSString * urlString;
                                            
                                            if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                                            {
                                                headers = @{ @"cache-control": @"no-cache",
                                                           };
                                                
                                                  urlString=[NSString stringWithFormat:@"https://api.kite.trade/orders/regular/%@?api_key=%@&access_token=%@",orderId,delegate2.APIKey,delegate2.accessToken];
                                            }
                                            
                                            else if ([delegate2.brokerNameStr isEqualToString:@"Upstox"])
                                            {
                                                 NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
                                                headers = @{ @"cache-control": @"no-cache",
                                                             @"Content-Type" : @"application/json",
                                                             @"authorization":access,
                                                             @"x-api-key":delegate2.APIKey
                                                             };
                                                
                                                 urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/orders/%@",orderId];
                                            }
                                        
                                       
                                        
                                      
                                        
                                        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                                           timeoutInterval:40.0];
                                        [request setHTTPMethod:@"DELETE"];
                                        [request setAllHTTPHeaderFields:headers];
                                        
                                        NSURLSession *session = [NSURLSession sharedSession];
                                        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                                        if(data !=nil)
                                                                                        {
                                                                                        if (error) {
                                                                                            //NSLog(@"%@", error);
                                                                                        } else {
                                                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                                            //NSLog(@"%@", httpResponse);
                                                                                            
                                                                                            NSMutableDictionary *orderDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                                            
                                                                                            
                                                                                            
                                                                                            
                                                                                        }
                                                                                        
                                                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                                                            
                                                                                            [self RetrieveAllOrders];
                                                                                            
                                                                                            
                                                                                            
                                                                                            
                                                                                            
                                                                                        });
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                                                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                                                                
                                                                                                [self presentViewController:alert animated:YES completion:^{
                                                                                                    
                                                                                                }];
                                                                                                
                                                                                                
                                                                                                UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                                                    
                                                                                                }];
                                                                                                
                                                                                               
                                                                                                
                                                                                                
                                                                                                [alert addAction:retryAction];
                                                                                              
                                                                                                
                                                                                            });
                                                                                            
                                                                                        }
                                                                                        
                                                                                        
                                                                                    }];
                                        [dataTask resume];

                                        }
                                        
                                       
                                        
                                        else
                                        {
                                            reload=true;
                                            int ATIONORDERNO=[[[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"inatinorderid"] intValue];
                                            
//                                            TagEncode * tag = [[TagEncode alloc]init];
//                                            delegate2.mtCheck=true;
//                                            [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.cancelOrder];
//                                            [tag TagData:sharedManager.inATINOrderNo intMethod:ATIONORDERNO];
//                                            [tag GetBuffer];
//                                            [self newMessage];
                                            
                                            //new//
                                            TagEncode * enocde=[[TagEncode alloc]init];
                                            mtOrdersArray=[[NSMutableArray alloc]init];
                                            enocde.inputRequestString=@"cancelrequest";
                                            NSMutableArray * inputArray=[[NSMutableArray alloc]init];
                                            [inputArray addObject:delegate2.accessToken];
                                            [inputArray addObject:delegate2.userID];
                                            [inputArray addObject:[NSString stringWithFormat:@"%d",ATIONORDERNO]];
                                            [inputArray addObject:[NSString stringWithFormat:@"%@multitradeapi/cancelrequest",delegate2.mtBaseUrl]];
                                            
                                            [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
                                                NSArray * keysArray=[dict allKeys];
                                                
                                                
                                                if(keysArray.count>0)
                                                 {
                                            
                                            [segmentedControl setSelectedSegmentIndex:0];
                                            [self segmentedControlChangedValue];
                                                 }
                                               else
                                                   
                                               {
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                       
                                                       [self presentViewController:alert animated:YES completion:^{
                                                           
                                                       }];
                                                       
                                                       
                                                       UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                          
                                                       }];
                                                       
                                                   
                                                       [alert addAction:retryAction];
                                                       
                                                   });
                                                   
                                               }
                                            }];
                                            
                                            
                                        }
                                    }
                                    @catch (NSException * e) {
                                        //NSLog(@"Exception: %@", e);
                                    }
                                    @finally {
                                        //NSLog(@"finally");
                                    }
                                                                   }
                                
                                
                                ];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"No"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    
    //Add your buttons to alert controller
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
        
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
//    if(segmentedControl.selectedSegmentIndex==2)
//    {
//        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
//        {
//    if(pendingMessageArray.count>0)
//    {
//    NSString * messageStr=[NSString stringWithFormat:@"%@",[pendingMessageArray objectAtIndex:indexPath.row]];
//
//    UIAlertController * alert = [UIAlertController
//                                 alertControllerWithTitle:@"Message"
//                                 message:messageStr
//                                 preferredStyle:UIAlertControllerStyleAlert];
//
//    //Add Buttons
//
//    UIAlertAction* yesButton = [UIAlertAction
//                                actionWithTitle:@"Ok"
//                                style:UIAlertActionStyleDefault
//                                handler:^(UIAlertAction * action) {
//                                    //Handle your yes please button action here
//
//
//                                }];
//
//
//    //Add your buttons to alert controller
//
//    [alert addAction:yesButton];
//
//
//    [self presentViewController:alert animated:YES completion:nil];
//
//    }
//
//        }
//
//        else
//        {
//
//            NSString * messageStr=[NSString stringWithFormat:@"%@",[[delegate2.mtCancelArray objectAtIndex:indexPath.row] objectForKey:@"status_message"]];
//
//            UIAlertController * alert = [UIAlertController
//                                         alertControllerWithTitle:@"Message"
//                                         message:messageStr
//                                         preferredStyle:UIAlertControllerStyleAlert];
//
//            //Add Buttons
//
//            UIAlertAction* yesButton = [UIAlertAction
//                                        actionWithTitle:@"Ok"
//                                        style:UIAlertActionStyleDefault
//                                        handler:^(UIAlertAction * action) {
//                                            //Handle your yes please button action here
//
//
//                                        }];
//
//
//            //Add your buttons to alert controller
//
//            [alert addAction:yesButton];
//
//
//            [self presentViewController:alert animated:YES completion:nil];
//        }
//    }
//    else
        
//    {
       
        
        
        if(selectedIndex == nil)
        {
        selectedIndex = indexPath;
           
            expandBool = true;
        }
        else
        {
            if(selectedIndex == indexPath)
            {
            selectedIndex = nil;
                expandBool = false;
            }
            else
            {
                selectedIndex = indexPath;
                expandBool = true;
            }
        }
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
     
        
//    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    


}

//reachability//

-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    
    
}

-(BOOL)networkStatus
{
    
    
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    
    if(netStatus==NotReachable)
    {
        
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    return YES;
    
    
}

//mt//






-(void)reloadData
{
    
    self.pendingOrdersTableView.delegate=self;
    self.pendingOrdersTableView.dataSource=self;
    
    [self.pendingOrdersTableView reloadData];

    
}

-(void)mtOrderRequest
{
    @try
    {
    if(delegate2.mtPendingArray.count>0)
    {
        [delegate2.mtPendingArray removeAllObjects];
    }
    if(delegate2.mtCompletedArray.count>0)
    {
        [delegate2.mtCompletedArray removeAllObjects];
    }
    if(delegate2.mtCancelArray.count>0)
    {
        [delegate2.mtCancelArray removeAllObjects];
    }
    TagEncode * enocde=[[TagEncode alloc]init];
    mtOrdersArray=[[NSMutableArray alloc]init];
    enocde.inputRequestString=@"trade";
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    [inputArray addObject:delegate2.accessToken];
     [inputArray addObject:delegate2.userID];
    [inputArray addObject:[NSString stringWithFormat:@"%@multitradeapi/retrieveorderrequest",delegate2.mtBaseUrl]];
    
    [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        
        NSArray * keysArray=[dict allKeys];
        
        
        if(keysArray.count>0)
        {
        
        mtOrdersArray=[[[dict objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"orders"];
        
            Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
            
            [mixpanelMini identify:delegate2.userID];
        
        
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"YYYY-MM-DD HH:MM:SS"];
        for(int i=0;i<mtOrdersArray.count;i++)
        {
            NSString * status=[[mtOrdersArray objectAtIndex:i] objectForKey:@"status"];
            
//            int status=[string intValue];
            
            if([status isEqualToString:@"Cancel"]||[status isEqualToString:@"Reject"]||[status isEqualToString:@"Rejected"])
            {
                
                [delegate2.mtCancelArray addObject:[mtOrdersArray objectAtIndex:i]];
                NSMutableArray * cancelledOrderArray = [[NSMutableArray alloc]init];
                for (int i=0; i<delegate2.mtCancelArray.count; i++) {
                    NSMutableDictionary * cancelledDictionary = [[NSMutableDictionary alloc]init];
                    [cancelledDictionary setObject:[NSString stringWithFormat:@"%@",[[delegate2.mtCancelArray objectAtIndex:i]objectForKey:@"tradingsymbol"]] forKey:@"symbol_name"];
                    [cancelledDictionary setObject:[NSString stringWithFormat:@"%@",@"0.0"] forKey:@"filled_quantity"];
                    [cancelledDictionary setObject:[NSString stringWithFormat:@"%@",[[delegate2.mtCancelArray objectAtIndex:i] objectForKey:@"price"]] forKey:@"avg_price"];
                    [cancelledDictionary setObject:[NSString stringWithFormat:@"%@",[[delegate2.mtCancelArray objectAtIndex:i] objectForKey:@"transaction_type"]] forKey:@"transaction_type"];
                    [cancelledDictionary setObject:[NSString stringWithFormat:@"%@",@"0.0"] forKey:@"pending_quantity"];
                    [cancelledDictionary setObject:[NSString stringWithFormat:@"%@",[[delegate2.mtCancelArray objectAtIndex:i] objectForKey:@"quantity"]] forKey:@"total_quantity"];
                    [cancelledDictionary setObject:[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:[NSDate date]]] forKey:@"date_time"];
                    [cancelledOrderArray addObject:cancelledDictionary];
                }
                [mixpanelMini.people set:@{@"cancelledorders":cancelledOrderArray}];
                delegate2.mtCancelArray = [[[delegate2.mtCancelArray reverseObjectEnumerator] allObjects] mutableCopy];
            }
            
            else  if([status isEqualToString:@"Pending"])
            {
                
                [delegate2.mtPendingArray addObject:[mtOrdersArray objectAtIndex:i]];
                NSMutableArray * pendingOrderArray = [[NSMutableArray alloc]init];
                for (int i=0; i<delegate2.mtPendingArray.count; i++) {
                    NSMutableDictionary * cancelledDictionary = [[NSMutableDictionary alloc]init];
                    [cancelledDictionary setObject:[NSString stringWithFormat:@"%@",[[delegate2.mtPendingArray objectAtIndex:i]objectForKey:@"tradingsymbol"]] forKey:@"symbol_name"];
                    [cancelledDictionary setObject:[NSString stringWithFormat:@"%@",@"0.0"] forKey:@"filled_quantity"];
                    [cancelledDictionary setObject:[NSString stringWithFormat:@"%@",[[delegate2.mtPendingArray objectAtIndex:i] objectForKey:@"price"]] forKey:@"avg_price"];
                    [cancelledDictionary setObject:[NSString stringWithFormat:@"%@",[[delegate2.mtPendingArray objectAtIndex:i] objectForKey:@"transaction_type"]] forKey:@"transaction_type"];
                    [cancelledDictionary setObject:[NSString stringWithFormat:@"%@",[[delegate2.mtPendingArray objectAtIndex:i] objectForKey:@"pending_quantity"]] forKey:@"pending_quantity"];
                    [cancelledDictionary setObject:[NSString stringWithFormat:@"%@",[[delegate2.mtPendingArray objectAtIndex:i] objectForKey:@"quantity"]] forKey:@"total_quantity"];
                    [cancelledDictionary setObject:[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:[NSDate date]]] forKey:@"date_time"];
                    [pendingOrderArray addObject:cancelledDictionary];
                }
              
                [mixpanelMini.people set:@{@"pendingorders":pendingOrderArray}];
                 delegate2.mtPendingArray = [[[delegate2.mtPendingArray reverseObjectEnumerator] allObjects] mutableCopy];
            }
            
            else  if([status isEqualToString:@"Execute"]||[status isEqualToString:@"Executed"])
            {
                [delegate2.mtCompletedArray addObject:[mtOrdersArray objectAtIndex:i]];
                
                NSMutableArray * completedOrdersArray = [[NSMutableArray alloc]init];
                for (int i=0; i<delegate2.mtCompletedArray.count; i++) {
                    NSMutableDictionary * cancelledDictionary = [[NSMutableDictionary alloc]init];
                    [cancelledDictionary setObject:[NSString stringWithFormat:@"%@",[[delegate2.mtCompletedArray objectAtIndex:i]objectForKey:@"tradingsymbol"]] forKey:@"symbol_name"];
                    [cancelledDictionary setObject:[NSString stringWithFormat:@"%@",[[delegate2.mtCompletedArray objectAtIndex:i] objectForKey:@"filled_quantity"]] forKey:@"filled_quantity"];
                    [cancelledDictionary setObject:[NSString stringWithFormat:@"%@",[[delegate2.mtCompletedArray objectAtIndex:i] objectForKey:@"price"]] forKey:@"avg_price"];
                    [cancelledDictionary setObject:[NSString stringWithFormat:@"%@",[[delegate2.mtCompletedArray objectAtIndex:i] objectForKey:@"transaction_type"]] forKey:@"transaction_type"];
                    [cancelledDictionary setObject:[NSString stringWithFormat:@"%@",[[delegate2.mtCompletedArray objectAtIndex:i] objectForKey:@"pending_quantity"]] forKey:@"pending_quantity"];
                    [cancelledDictionary setObject:[NSString stringWithFormat:@"%@",[[delegate2.mtCompletedArray objectAtIndex:i] objectForKey:@"quantity"]] forKey:@"total_quantity"];
                    [cancelledDictionary setObject:[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:[NSDate date]]] forKey:@"date_time"];
                    [completedOrdersArray addObject:cancelledDictionary];
                }
                [mixpanelMini.people set:@{@"completedorders":completedOrdersArray}];
                
                delegate2.mtCompletedArray = [[[delegate2.mtCompletedArray reverseObjectEnumerator] allObjects] mutableCopy];
                
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.pendingOrdersTableView.hidden=NO;
            self.activityInd.hidden=YES;
            [self.activityInd stopAnimating];
            
            self.pendingOrdersTableView.delegate=self;
            self.pendingOrdersTableView.dataSource=self;
            
            [self.pendingOrdersTableView reloadData];
            
            
        });
        
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                
                UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self mtOrderRequest];
                }];
                
                UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                
                
                [alert addAction:retryAction];
                [alert addAction:cancel];
                
            });
            
        }
        
        
        
    }];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

@end
