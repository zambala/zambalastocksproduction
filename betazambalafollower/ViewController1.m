//
//  ViewController1.m
//  testing
//
//  Created by zenwise mac 2 on 11/14/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "ViewController1.h"
#import "AppDelegate.h"
#import "HomePage.h"
#import <CommonCrypto/CommonDigest.h>
#import "MainView.h"
#import "TabBar.h"
#import "BrokerView.h"
#import "BrokerViewNavigation.h"
#import "TagEncode.h"
@import Mixpanel;



@interface ViewController1 ()
{
    AppDelegate * delegate2;
    
   
}

@end

@implementation ViewController1

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSData *cookieData = [[NSUserDefaults standardUserDefaults] objectForKey:@"ApplicationCookie"];
    if ([cookieData length] > 0) {
        NSArray *cookies = [NSKeyedUnarchiver unarchiveObjectWithData:cookieData];
        for (NSHTTPCookie *cookie in cookies) {
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.activityIndicator.hidden=NO;
        [self.activityIndicator startAnimating];
        
        
    });
    
 

    delegate2 = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    if(delegate2.brokerInfoDict)
    {
        delegate2.brokerNameStr=[NSString stringWithFormat:@"%@",[delegate2.brokerInfoDict objectForKey:@"brokername"]];
    }
    //NSLog(@"sd%@",delegate2.accessToken);
//    [delegate2.urlArray removeAllObjects];
    
   //  [[NSURLCache sharedURLCache] removeAllCachedResponses];
//    if([delegate2.logoutCheck isEqualToString:@"loggedout"])
//    {
//        NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
//        for (NSHTTPCookie *cookie in [storage cookies]) {
//            [storage deleteCookie:cookie];
//        }
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        delegate2.logoutCheck=@"";
//
//    }
    
//    if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
//    {
//        NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
//        for (NSHTTPCookie *cookie in [storage cookies]) {
//            [storage deleteCookie:cookie];
//        }
//        [[NSUserDefaults standardUserDefaults] synchronize];
//    }
    
  
    
  

}
- (BOOL)webView:(UIWebView *)wv shouldStartLoadWithRequest:(NSURLRequest *)rq
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.activityIndicator.hidden=NO;
        [self.activityIndicator startAnimating];
    
        
    });
    
     return YES;

  }
- (void)webViewDidFinishLoading:(UIWebView *)wv
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.activityIndicator stopAnimating];
        self.activityIndicator.hidden=YES;
        
    });
    
   
}


- (void)webView:(UIWebView *)wv didFailLoadWithError:(NSError *)error
{
    [self.activityIndicator stopAnimating];
    self.activityIndicator.hidden=YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:YES];

//    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
- (void)viewDidAppear:(BOOL)animated;
{
    [super viewDidAppear:YES];
    @try
    {
    if([delegate2.logoutCheck isEqualToString:@"loggedout"])
    {
//        NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
//        for (NSHTTPCookie *cookie in [storage cookies]) {
//            [storage deleteCookie:cookie];
//
//        }
//        [[NSUserDefaults standardUserDefaults] synchronize];
        NSData *cookieData = [[NSUserDefaults standardUserDefaults] objectForKey:@"ApplicationCookie"];
        if ([cookieData length] > 0) {
            NSArray *cookies = [NSKeyedUnarchiver unarchiveObjectWithData:cookieData];
            for (NSHTTPCookie *cookie in cookies) {
                [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
            }
        }
       
        
    }
//    else if( [[NSUserDefaults standardUserDefaults] objectForKey:@"ApplicationCookie"]==nil)
//            
//    {
//           
//    }
    else
        
    {
        NSData *cookieData = [[NSUserDefaults standardUserDefaults] objectForKey:@"ApplicationCookie"];
        if ([cookieData length] > 0) {
            NSArray *cookies = [NSKeyedUnarchiver unarchiveObjectWithData:cookieData];
            for (NSHTTPCookie *cookie in cookies) {
                [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
            }
        }
    }
        self.webView.delegate=self;
        
        
      
    delegate2.APIKey=[NSString stringWithFormat:@"%@",[[delegate2.brokerInfoDict objectForKey:@"additionalinfo"] objectForKey:@"apiKey"]];
    delegate2.secret=[NSString stringWithFormat:@"%@",[[delegate2.brokerInfoDict objectForKey:@"additionalinfo"] objectForKey:@"seceretKey"]];
    delegate2.brokerRedirectUrl=[NSString stringWithFormat:@"%@",[[delegate2.brokerInfoDict objectForKey:@"additionalinfo"] objectForKey:@"redirectUrl"]];
    
    
    
    //NSLog(@"sd%@",delegate2.accessToken);
    //    [delegate2.urlArray removeAllObjects];
    
    //     [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    
    //    if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    //    {
    //        NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    //        for (NSHTTPCookie *cookie in [storage cookies]) {
    //            [storage deleteCookie:cookie];
    //
    //        }
    //    }
    //
    self.webView.delegate=self;
    
    
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
    {
        NSString *link = [NSString stringWithFormat:@"https://kite.trade/connect/login?v=3&api_key=%@",delegate2.APIKey];
        NSURL *url=[NSURL URLWithString:link];
        
        NSURLRequest *urlReq=[NSURLRequest requestWithURL: url];
        //NSLog(@"urlreq%@",urlReq);
        [self.webView loadRequest:urlReq];
    }
    //    https://www.zenwise.net/testwebsite/redirection
    else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
        NSString *upStoxLink = [NSString stringWithFormat:@"https://api.upstox.com/index/dialog/authorize?apiKey=%@&redirect_uri=%@&response_type=code",delegate2.APIKey,delegate2.brokerRedirectUrl];
        NSURL *upStoxUrl=[NSURL URLWithString:upStoxLink];
        
        NSURLRequest *urlReq=[NSURLRequest requestWithURL:upStoxUrl];
        //NSLog(@"urlreq%@",urlReq);
        [self.webView loadRequest:urlReq];
        
    }
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}// Called when the view has been fully transitioned onto the screen. Default does nothing
//- (void)viewWillDisappear:(BOOL)animated; // Called when the view is dismissed, covered or otherwise hidden. Default does nothing

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.activityIndicator startAnimating];
        self.activityIndicator.hidden=NO;
        
    });
}
- (void)webViewDidFinishLoad:(UIWebView *)webView;
{
    //self.url = self.webView.request.mainDocumentURL;
    @try
    {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.activityIndicator stopAnimating];
        self.activityIndicator.hidden=YES;
        
    });

    
    self.myString=[[NSString alloc]init];
    
    self.myString = self.webView.request.URL.absoluteString;
    
    
    
//    [delegate2.urlArray addObject:self.url];
    
    
    
//    //NSLog(@"sd %@",self.);
    
    
//    if ([_myString isEqualToString:[delegate1.urlArray objectAtIndex:2]]) {
//        HomePage * homeView=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
//        [self presentViewController:homeView animated:YES completion:nil];
//
//        
//    }
   //    //NSLog(@"adse %@",self.myString);
    
   
    
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
    {
        
    if([_myString containsString:@"request_token"])
    {
        //NSLog(@"found");
        
        //       ' delegate2.requestToken=[self.myString stringByReplacingOccurrencesOfString:@"https://www.zenwise.net/admin/getauthentication?status=success&request_token=" withString:@""];
        //NSLog(@"token %@",delegate2.requestToken);
        
        NSMutableDictionary *queryStringDictionary = [[NSMutableDictionary alloc] init];
        
        NSArray *urlComponents1 = [self.myString componentsSeparatedByString:@"?"];
        
        NSString * paramStr=[NSString stringWithFormat:@"%@",[urlComponents1 objectAtIndex:1]];
        
        NSArray *urlComponents = [paramStr componentsSeparatedByString:@"&"];
        
        for (NSString *keyValuePair in urlComponents)
        {
            NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
            NSString *key = [[pairComponents firstObject] stringByRemovingPercentEncoding];
            NSString *value = [[pairComponents lastObject] stringByRemovingPercentEncoding];
            
            [queryStringDictionary setObject:value forKey:key];
        }
        
        delegate2.requestToken= [queryStringDictionary objectForKey:@"request_token"];
        
        
        NSString * string1=[delegate2.APIKey stringByAppendingString:delegate2.requestToken];
        
        //NSLog(@"sde %@",delegate2.requestToken);
        
        NSString * string2=[string1 stringByAppendingString:delegate2.secret];
        
        //        NSString * msg = @"Rosetta code";
        unsigned char buf[CC_SHA256_DIGEST_LENGTH];
        const char * rc = [string2 cStringUsingEncoding:NSASCIIStringEncoding];
        if (! CC_SHA256(rc, strlen(rc), buf)) {
            //NSLog(@"Failure...");
            
        }
        NSMutableString * res = [NSMutableString stringWithCapacity:(CC_SHA256_DIGEST_LENGTH * 2)];
        for (int i = 0; i < CC_SHA256_DIGEST_LENGTH; ++i) {
            [res appendFormat:@"%02x", buf[i]];
        }
        //NSLog(@"Output: %@", res);
        
        
        self.session=[NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSDictionary *headers = @{ @"X-Kite-Version":@"3"
                                   };
        
        //        self.urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://api.kite.trade/session/token"]
        //                                                  cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
        //                                              timeoutInterval:30.0];
        
        self.urlRequest=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:@"https://api.kite.trade/session/token"]];
        [self.urlRequest setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [self.urlRequest setHTTPMethod:@"POST"];
        [self.urlRequest setAllHTTPHeaderFields:headers];
        NSString *post = [[NSString alloc] initWithFormat:@"api_key=%@&request_token=%@&checksum=%@",delegate2.APIKey,delegate2.requestToken,res];
        
        NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
        
        [self.urlRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        [self.urlRequest setHTTPBody:postData];
        
        self.task1=[self.session dataTaskWithRequest:self.urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
                    {
                       if(data!=nil)
                       {
                        delegate2.dict=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                        //NSLog(@"%@",delegate2.dict);
                        
                        delegate2.userID=[NSString stringWithFormat:@"%@",[[delegate2.dict objectForKey:@"data"] objectForKey:@"user_id"]];
                        
                        delegate2.publicToken=[NSString stringWithFormat:@"%@",[[delegate2.dict objectForKey:@"data"] objectForKey:@"public_token"]];
                        
                        delegate2.accessToken=[NSString stringWithFormat:@"%@",[[delegate2.dict objectForKey:@"data"] objectForKey:@"access_token"]];
                        //NSLog(@"%@",delegate2.accessToken);
                        
                        delegate2.userName=[NSString stringWithFormat:@"%@",[[delegate2.dict objectForKey:@"data"] objectForKey:@"user_name"]];
                        
                        delegate2.emailId=[NSString stringWithFormat:@"%@",[[delegate2.dict objectForKey:@"data"] objectForKey:@"email"]];
                        
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        [prefs setObject:@"CLIENT" forKey:@"loginActivityStr"];
                     
                        
                        NSData *cookieData = [NSKeyedArchiver archivedDataWithRootObject:[[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]];
                        [[NSUserDefaults standardUserDefaults] setObject:cookieData forKey:@"ApplicationCookie"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            if([delegate2.userID isEqualToString:@"RP3997"])
                            {
                                [self kiteAccessUpdate];
                               
                            }
                            else
                            {
                                [self clientCreation];
                            }
                            
                        });
                           
                           
                       }
                       
                        //                     [self liveStreaming];
                    }];
        
        [self.task1 resume];
        //
        //AD banner//
        //    self.bannerIB.placementId = INMOBI_BANNER_PLACEMENT;
        //    self.bannerIB.transitionAnimation = UIViewAnimationTransitionCurlUp;
        //    self.bannerIB.delegate=self;
        //    [self.bannerIB shouldAutoRefresh:YES];
        //    [self.bannerIB setRefreshInterval:30];
        //    [self.bannerIB load];
        //blur effect//
        //        self.view.backgroundColor = [UIColor clearColor];
        
        //        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        //        blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        //        blurEffectView.frame = self.view.bounds;
        //        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        //
        //        [self.view addSubview:blurEffectView];
        //activity indicator//
        
        //    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeDoubleBounce tintColor:[UIColor yellowColor] size:20.0f];
        //    activityIndicatorView.frame = CGRectMake(170,280,50,50);
        //    [blurEffectView addSubview:activityIndicatorView];
        //    [activityIndicatorView startAnimating];

        NSData *cookieData = [NSKeyedArchiver archivedDataWithRootObject:[[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]];
        [[NSUserDefaults standardUserDefaults] setObject:cookieData forKey:@"ApplicationCookie"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
        
    }
        
    }
   
    
    else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
        if([_myString rangeOfString:@"https://www.zenwise.net/testwebsite/redirection?code="].location==NSNotFound)
        {
            //NSLog(@"not found");
        }
        else{
            delegate2.requestToken=[self.myString stringByReplacingOccurrencesOfString:@"https://www.zenwise.net/testwebsite/redirection?code=" withString:@""];
            //NSLog(@"token %@",delegate2.requestToken);
            
            [self upStoxAccess];
        
        }
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)upStoxAccess
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.activityIndicator.hidden=NO;
        [self.activityIndicator startAnimating];
        
        
    });
    
//    if(delegate2.upStockCheck==true)
//    {
//        delegate2.upStockCheck=false;
    @try
    {
     NSString * str=[NSString stringWithFormat:@"Basic %@",delegate2.secret];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"x-api-key": delegate2.APIKey,
                               @"authorization":str,
                               @"cache-control": @"no-cache",
                               };
    NSDictionary *parameters = @{ @"code":delegate2.requestToken,
                                  @"grant_type": @"authorization_code",
                                  @"redirect_uri": delegate2.brokerRedirectUrl };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://api.upstox.com/index/oauth/token"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:40.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if(data !=nil)
                                                    {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        self.responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        delegate2.accessToken=[self.responseDict objectForKey:@"access_token"];
                                                        
                                                    }
                                                    
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        
                                                        if(delegate2.accessToken.length==0)
                                                        {
                                                            
//                                                            NSString * message = [NSString stringWithFormat:@"Error! Reason:%@",[self.responseDict objectForKey:@"message"]];
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                UIAlertController * alert = [UIAlertController
                                                                                             alertControllerWithTitle:@"Warning"
                                                                                             message:@"Error while logging in. Try again."
                                                                                             preferredStyle:UIAlertControllerStyleAlert];
                                                                
                                                                //Add Buttons
                                                                
                                                                UIAlertAction* okButton = [UIAlertAction
                                                                                           actionWithTitle:@"Ok"
                                                                                           style:UIAlertActionStyleDefault
                                                                                           handler:^(UIAlertAction * action) {
                                                                                               //Handle your yes please button action here
                                                                                               delegate2.accessToken=@"";
                                                                                               BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
                                                                                               
                                                                                               [self presentViewController:view animated:YES completion:nil];
                                                                                               
                                                                                               
                                                                                           }];
                                                                //Add your buttons to alert controller
                                                                
                                                                [alert addAction:okButton];
                                                                
                                                                
                                                                [self presentViewController:alert animated:YES completion:nil];
                                                                
                                                            });
                                                        }else
                                                        {
                                                            delegate2.accessToken=[self.responseDict objectForKey:@"access_token"];
                                                            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                                            
                                                           
                                                            [prefs setObject:@"CLIENT" forKey:@"loginActivityStr"];
                                                            [self upStoxUser];
                                                        }
                                                        
                                                        
                                                        
                                                        
                                                    });
                                                    }
                                                    else
                                                    {
                                                        [self upStoxAccess];
                                                    }
                                                }];
    [dataTask resume];
        
//    }
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)reloadBtn:(id)sender {
    
     [self.webView reload];
}
-(void)upStoxUser
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.activityIndicator.hidden=NO;
        [self.activityIndicator startAnimating];
        
        
    });
    
    @try
    {
    NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
    NSDictionary *headers = @{ @"x-api-key":delegate2.APIKey,
                               @"authorization":access,
                               @"cache-control": @"no-cache",
                               };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://api.upstox.com/index/profile"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:40.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData * data, NSURLResponse * response, NSError *error) {
                                                    if(data !=nil)
                                                    {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse * ) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        NSMutableDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        
                                                        delegate2.userID=[NSString stringWithFormat:@"%@",[[responseDict objectForKey:@"data"] objectForKey:@"client_id"]];
                                                        delegate2.upstoxUserBankName = [NSString stringWithFormat:@"%@",[[responseDict objectForKey:@"data"]objectForKey:@"bank_name"]];
                                                        delegate2.upstoxUserBankAccount = [NSString stringWithFormat:@"%@",[[responseDict objectForKey:@"data"]objectForKey:@"bank_account"]];
                                                       
                                                       
                                                        
                                                        delegate2.userName=[NSString stringWithFormat:@"%@",[[responseDict objectForKey:@"data"] objectForKey:@"name"]];
                                                        
                                                        delegate2.emailId=[NSString stringWithFormat:@"%@",[[responseDict objectForKey:@"data"] objectForKey:@"email"]];
                                                        
                                                        
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        NSData *cookieData = [NSKeyedArchiver archivedDataWithRootObject:[[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]];
                                                        [[NSUserDefaults standardUserDefaults] setObject:cookieData forKey:@"ApplicationCookie"];
                                                        [[NSUserDefaults standardUserDefaults] synchronize];
                                                        [self  clientCreation];
                                                        
                                                        
                                                    });
                                                    }
                                                    else
                                                    {
                                                        [self upStoxUser];
                                                    }
                                                    
                                                }];
    [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}

-(void)kiteAccessUpdate
{
      @try {
    NSDictionary * headers = @{
                               @"content-type": @"application/json"
                               };
    NSDictionary *params;
  
        
        params=@{@"accesstoken":delegate2.accessToken
                 
        };
        
        
  
    
    
    NSData * postData=[NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
    NSString * urlStr=[NSString stringWithFormat:@"https://stockserver.zenwise.net/api/stock/kite/accesstoken"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if(data !=nil)
                                                    {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        if([httpResponse statusCode]==200)
                                                        {
                                                            
                                                           [self kiteAccessUpdateStage];
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                            [self kiteAccessUpdate];
                                                        }
                                                     
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                      
                                                        
                                                    });
                                                    }
                                                        else
                                                        {
                                                            [self kiteAccessUpdate];
                                                        }
                                                }];
    [dataTask resume];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}
-(void)kiteAccessUpdateStage
{  @try {
    
    NSDictionary * headers = @{
                               @"content-type": @"application/json"
                               };
    NSDictionary *params;
  
        
        params=@{@"accesstoken":delegate2.accessToken
                 
                 };
        
        
        
        
        
        NSData * postData=[NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
        NSString * urlStr=[NSString stringWithFormat:@"%@stock/kite/accesstoken",delegate2.baseUrl];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        [request setAllHTTPHeaderFields:headers];
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(data !=nil)
                                                        {
                                                            if (error) {
                                                                //NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                if([httpResponse statusCode]==403)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                    
                                                                }
                                                                else if([httpResponse statusCode]==401)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    
                                                                if([httpResponse statusCode]==200)
                                                                {
                                                                    
                                                                    [self clientCreation];
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    
                                                                    [self kiteAccessUpdateStage];
                                                                }
                                                                }
                                                                
                                                            }
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                
                                                                
                                                            });
                                                        }
                                                        else
                                                        {
                                                            [self kiteAccessUpdateStage];
                                                        }
                                                    }];
        [dataTask resume];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}

-(void)clientCreation
{
    @try
    {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.activityIndicator.hidden=NO;
        [self.activityIndicator startAnimating];
        
        
    });
    
   
    TagEncode * enocde=[[TagEncode alloc]init];
    NSUserDefaults *loggedInUserNew = [NSUserDefaults standardUserDefaults];
    enocde.inputRequestString=@"clientcreation";
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];


    [inputArray addObject:delegate2.userID];
    [inputArray addObject:@""];
    [inputArray addObject:[NSString stringWithFormat:@"%@",[delegate2.brokerInfoDict objectForKey:@"brokerid"]]];
    [inputArray addObject:delegate2.emailId];
    [inputArray addObject:delegate2.userName];
    [inputArray addObject:@""];
    [inputArray addObject:[NSString stringWithFormat:@"%@2factor/validateotp",delegate2.baseUrl]];
    [inputArray addObject:[loggedInUserNew objectForKey:@"userid"]];

    [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
       
        NSArray * keysArray=[dict allKeys];
        
        
        if(keysArray.count>0)
        {
          
            delegate2.zenwiseToken=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"] objectForKey:@"authtoken"]];
            NSUserDefaults *loggedInUserNew = [NSUserDefaults standardUserDefaults];
            delegate2.loginActivityStr=@"CLIENT";
            [loggedInUserNew setObject:@"client" forKey:@"userrole"];
            [loggedInUserNew setObject:delegate2.userID forKey:@"mtuserid"];
            [loggedInUserNew setObject:delegate2.brokerNameStr forKey:@"brokername"];
            [loggedInUserNew setObject:delegate2.loginActivityStr forKey:@"loginActivityStr"];
            [loggedInUserNew synchronize];
            delegate2.loginActivityStr=@"CLIENT";
            Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
            
            [mixpanelMini identify:delegate2.userID];
            
            [mixpanelMini.people set:@{@"name":[loggedInUserNew objectForKey:@"profilename"],@"mobileno":[loggedInUserNew objectForKey:@"profilemobilenumber"],@"email":[loggedInUserNew objectForKey:@"profileemail"],@"logintime":[NSDate date],@"sublocality":delegate2.subLocality,@"location":delegate2.locality,@"postalcode":delegate2.postalCode,@"typeofuser":delegate2.brokerNameStr,@"userid":delegate2.userID,@"deviceid":delegate2.currentDeviceId}];
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
                [self presentViewController:tabPage animated:YES completion:nil];
            });
           
        }
        else
        {
            [self clientCreation];
        }

    }];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}


@end
