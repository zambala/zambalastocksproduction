//
//  OnBoardingRootViewController.h
//  NewUI
//
//  Created by Zenwise Technologies on 24/05/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OnboardingViewController.h"

@interface OnBoardingRootViewController : UIViewController <UIPageViewControllerDataSource>


@property (nonatomic,strong) UIPageViewController *PageViewController;
@property (nonatomic,strong) NSArray *arrPageTitles;
@property (nonatomic,strong) NSArray *arrPageImages;

- (OnboardingViewController *)viewControllerAtIndex:(NSUInteger)index;

@end
