//
//  ExploreDetail.h
//  PremiumServices
//
//  Created by zenwise mac 2 on 12/30/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExploreDetail : UIViewController<UIScrollViewDelegate>

{
    BOOL premiumFlag, signalFlag, dealerFlag;
}

@property (strong, nonatomic) IBOutlet UIScrollView *scollView;

@property (strong, nonatomic) IBOutlet UIView *premiumView;
@property (strong, nonatomic) IBOutlet UIView *signalsView;
@property (strong, nonatomic) IBOutlet UIView *dealerView;
- (IBAction)premiumChkBtnAction:(id)sender;
- (IBAction)signalChkBtnAction:(id)sender;
- (IBAction)dealerChkBtnAction:(id)sender;

//@property (strong, nonatomic) IBOutlet UIView *premiumView, *signalsView, *dealerView;
//
//@property (strong, nonatomic) IBOutlet UIButton *button1, *button2, *button3, *button4;
//
//@property (strong, nonatomic) IBOutlet UIButton  *premiumChkBtn, *signalChkBtn, *dealerChkBtn;
@property (strong, nonatomic) IBOutlet UIButton *premiumChkBtn;
@property (strong, nonatomic) IBOutlet UIButton *signalChkBtn;
@property (strong, nonatomic) IBOutlet UIButton *dealerChkBtn;
@property (strong, nonatomic) IBOutlet UIButton *button1;
@property (strong, nonatomic) IBOutlet UIButton *button2;
@property (strong, nonatomic) IBOutlet UIButton *button3;
@property (strong, nonatomic) IBOutlet UIButton *button4;


@end
