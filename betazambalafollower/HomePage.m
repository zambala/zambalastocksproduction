//
//  HomePage.m
//  testing
//
//  Created by zenwise mac 2 on 11/15/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "HomePage.h"
#import "AppDelegate.h"
#import <CommonCrypto/CommonHMAC.h>
#import "PSWebSocket.h"
#import "StockView1.h"
#import "MarketTableViewCell.h"
#import "PSWebSocketDriver.h"
#import<malloc/malloc.h>
#import "StockOrderView.h"
#import "SearchSymbolView.h"

#import "TagEncode.h"
#import "MT.h"
#import <Mixpanel/Mixpanel.h>
#import "HMSegmentedControl.h"
#import "DematCell.h"
#import "SearchSymbolView.h"
#import "CustomPortfolioView.h"
@import SocketIO;
@import Firebase;

@interface HomePage () <PSWebSocketDelegate,NSStreamDelegate,SocketEngineSpec,SocketManagerSpec,SocketEngineClient>
{
     HMSegmentedControl * segmentedControl;
    AppDelegate * delegate2;
    NSDictionary * dict1;
    NSString * string1;/*!<it store message (sending to kite)*/
    NSMutableDictionary *instrumentTokensDict;/*!<it store market price data of symbols*/
     NSMutableDictionary *instrumentTokensDictCustom;
    NSMutableArray *allKeysList;/*!<it store all keys of instrumentTokensDict*/
    NSMutableArray * allKeysListCustom;
    /*!<it store all keys of instrumentTokensDict*/
    NSString * option;/*!<it store option type of symbol*/
    NSString * finalDate;/*!<it store expiry data of symbol*/
    NSMutableArray * localInstrumentToken;/*!<it store instrument tokens of symbols*/
    NSMutableDictionary * responseDict1;/*!<it store details of symbol from zambala server*/
    NSMutableArray * localExchangeTokensArray;/*!<it store exchange tokens of symbols*/
    NSMutableArray * symbolArray;
    bool temp;
    int value;
    int packetsLength;
    int packetsNumber;
    float lastPriceFlaotValue;
    float closePriceFlaotValue;
    NSMutableDictionary *tmpDict;
    NSMutableDictionary * tmpDictCustom;
    MT * sharedManager;
    TagEncode * tag1;
    BOOL disapperCheck;
    BOOL ltpReloadBool;
    NSUInteger modifyObjectIndex;
    float changeFloatValue;
    
    float changeFloatPercentageValue;
    BOOL scripCheck;
    BOOL editCheck;
    NSMutableDictionary * scripDetails;/*!<it store data of added symbols(used to upload to zambala server)*/
    NSMutableArray * scripDetailsArray;/*!<it store data of added symbols(used to upload to zambala server)*/
    NSString * isDelete;
    NSString * upstoxSymbolName;
    NSMutableArray * customLtpArray;
     NSMutableArray * customInvestmentArray;
    NSUserDefaults *loggedInUser;
    NSString * number;
    
    NSUInteger reloadIndex;
    NSIndexPath* reloadIndexPath;
    NSArray* reloadIndexArray;
    UIImageView *myImageView;
    
//mt//
    
    
    
}

@end

@implementation HomePage

- (void)viewDidLoad {
    [super viewDidLoad];
  loggedInUser = [NSUserDefaults standardUserDefaults];
    
   number= [loggedInUser stringForKey:@"profilemobilenumber"];
    temp = YES;
    scripCheck=true;
    //mt//
    sharedManager = [MT Mt1];
    //    sharedManagerMaster = [MTMaster Mt3];
    tag1 = [[TagEncode alloc]init];
    value  =0 ;
    delegate2=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.modifyBgView.hidden=YES;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    NSArray * localSegmentsArray=@[@"MARKET WATCH",@"PORTFOLIO WATCH"];
    customLtpArray=[[NSMutableArray alloc]init];
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:localSegmentsArray];
    segmentedControl.frame = CGRectMake(0, 0, self.view.frame.size.width, 43);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segmentedControl];
    [segmentedControl setSelectedSegmentIndex:0];
    [self segmentedControlChangedValue];
    if([delegate2.pushNotificationSection isEqualToString:@"MARKET_WATCH"])
    {
        delegate2.pushNotificationSection =@"";
        [segmentedControl setSelectedSegmentIndex:0];
        [self segmentedControlChangedValue];
        
    }
    else if([delegate2.pushNotificationSection isEqualToString:@"PORTFOLIO_WATCH"])
    {
         delegate2.pushNotificationSection =@"";
        [segmentedControl setSelectedSegmentIndex:1];
        [self segmentedControlChangedValue];
        
    }
    dict1=[[NSDictionary alloc]init];
    customInvestmentArray=[[NSMutableArray alloc]init];
    allKeysList = [[NSMutableArray alloc]init];
    if(delegate2.marketwatchHinT==YES)
    {   delegate2.marketwatchHinT=NO;
        UIWindow *currentWindow = [UIApplication sharedApplication].keyWindow;
        myImageView =[[UIImageView alloc] initWithFrame:CGRectMake(0.0,0.0,currentWindow.frame.size.width,currentWindow.frame.size.height)];
        myImageView.image=[UIImage imageNamed:@"watchlist"];
        [currentWindow addSubview:myImageView ];
        [currentWindow bringSubviewToFront:myImageView];}
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTap.numberOfTapsRequired = 1;
    [myImageView setUserInteractionEnabled:YES];
    [myImageView addGestureRecognizer:singleTap];
   self.navigationItem.title = @"Watch List";
    _headerView.frame = CGRectMake(_headerView.frame.origin.x,64,_headerView.frame.size.width , _headerView.frame.size.height);
   
   //firebase//
    
    [FIRAnalytics logEventWithName:@"home_page" parameters:nil];
    
    
//    if(delegate2.firstTime==true)
//    {
//    
//    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//        //NSLog(@"%@",userDefaults);
//    delegate2.instrumentToken = [userDefaults objectForKey:@"token"];
//        
//        delegate2.firstTime=false;
//    
//    }
    self.modifySubView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.modifySubView.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.modifySubView.layer.shadowOpacity = 5.0f;
    self.modifySubView.layer.shadowRadius = 4.0f;
    self.modifySubView.layer.cornerRadius=5.0f;
    self.modifySubView.layer.masksToBounds = NO;
  
    
    self.modifySaveBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.modifySaveBtn.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.modifySaveBtn.layer.shadowOpacity = 5.0f;
    self.modifySaveBtn.layer.shadowRadius = 4.0f;
    self.modifySaveBtn.layer.cornerRadius=4.1f;
    self.modifySaveBtn.layer.masksToBounds = NO;
    
      self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    
   
   if(segmentedControl.selectedSegmentIndex==0)
   {
    if(delegate2.marketWatchBool==true)
    {
        [self getScripMethod];
    }
     if(delegate2.marketWatchBool==false)
     {
     [self tmpDictMethod];
     }
    
   }
    else
    {
        if(delegate2.customWatchBool==true)
        {
            [self getScripMethod];
        }
        if(delegate2.customWatchBool==false)
        {
            [self tmpDictMethod];
        }
        
    }
   
}
-(void)tapDetected{
    //NSLog(@"single Tap on imageview");
    [myImageView removeFromSuperview];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}
-(void)hideMethod
{
    [self.activityIndicator stopAnimating];
    self.activityIndicator.hidden=YES;
    
}
- (void)viewDidAppear:(BOOL)animated
{
  loggedInUser = [NSUserDefaults standardUserDefaults];
   
   number= [loggedInUser stringForKey:@"profilemobilenumber"];
    self.marketTableView.delegate=self;
    self.marketTableView.dataSource=self;
    self.customPortfolioTblView.delegate=self;
    self.customPortfolioTblView.dataSource=self;
    
    
    ltpReloadBool=true;
    delegate2=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    delegate2.homeMt=true;
   
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(backMethod:)
                                                 name:@"back" object:nil];
    
    
  
    
    
   self.timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(hideMethod) userInfo:nil repeats:YES];
    
//     delegate2.marketWatch1=[[NSMutableArray alloc]init];
    
   
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"watch_list_page"];
    [mixpanelMini track:@"market_watch_page"];
    
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
     editCheck=true;
    
   
   if(segmentedControl.selectedSegmentIndex==0)
   {
    if(delegate2.marketWatchBool==false)
    {
        if(delegate2.searchToWatch==true)
        {
            [self.activityIndicator startAnimating];
            self.activityIndicator.hidden=NO;
           
               if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
               {
                   
                    [self instruments];
               }
                else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
                {
                    
//                    [self tmpDictMethod];
                    //[self.socket close];
                  //  [self webSocket];
                    [self instruments];
                }
                else
                {
            //    if(delegate2.searchToWatch==true)
            //    {
            //        delegate2.searchToWatch=false;
            
            //        [self instruments];
            
            //    }
            //
            //    else
            //    {
//                     [[NSNotificationCenter defaultCenter] postNotificationName:@"orderwatch" object:nil];
                    
            //
            //    }
                    
                 
                    [self instruments];
            
        }
            
//             [self resultCheck];
            
        }
        
        else
        {
            [self.activityIndicator startAnimating];
            self.activityIndicator.hidden=NO;
        [self tmpDictMethod];
      //  [self webSocket];
        [self socketHitMethod];
            //[self.socket close];
        
            
        }
        
    
//
//    }
        
        
    }
   }
    else
    {
        if(delegate2.customWatchBool==false)
        {
            if(delegate2.searchToWatch==true)
            {
                [self.activityIndicator startAnimating];
                self.activityIndicator.hidden=NO;
                
                if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                {
                    
                    [self instruments];
                }
                else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
                {
                    
                    [self tmpDictMethod];
                    //[self.socket close];
                    //  [self webSocket];
                    [self instruments];
                }
                else
                {
                    //    if(delegate2.searchToWatch==true)
                    //    {
                    //        delegate2.searchToWatch=false;
                    
                    //        [self instruments];
                    
                    //    }
                    //
                    //    else
                    //    {
                    //                     [[NSNotificationCenter defaultCenter] postNotificationName:@"orderwatch" object:nil];
                    
                    //
                    //    }
                    
                    
                    [self instruments];
                    
                }
                
                //             [self resultCheck];
                
            }
            
            else
            {
                [self.activityIndicator startAnimating];
                self.activityIndicator.hidden=NO;
                [self tmpDictMethod];
                //  [self webSocket];
                [self socketHitMethod];
                //[self.socket close];
                
                
            }
            
            
            //
            //    }
            
            
        }
    }
   
//    self.activityIndicator.hidden=YES;
//    [self.activityIndicator stopAnimating];
}

/*!
 @discussion used to connect socket.
 */
-(void)segmentedControlChangedValue
{
    if(segmentedControl.selectedSegmentIndex==0)
    {
        self.marketTableView.delegate=self;
        self.marketTableView.dataSource=self;
        [self.marketTableView reloadData];
        self.customPortfolioView.hidden=YES;
        self.marketTableView.hidden=NO;
        [self socketHitMethod];
    }
    else if(segmentedControl.selectedSegmentIndex==1)
    {
        
        Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
        [mixpanelMini track:@"portfolio_watch_page"];
        self.ifEmptyView.hidden=NO;
        if(delegate2.customWatchBool==true)
        {
            [self getScripMethod];
        }
        if(delegate2.customWatchBool==false)
        {
            [self tmpDictMethod];
        }
        [self socketHitMethod];
       
        self.customPortfolioTblView.delegate=self;
        self.customPortfolioTblView.dataSource=self;
        [self.customPortfolioTblView reloadData];
        self.customPortfolioView.hidden=NO;
        self.marketTableView.hidden=YES;
        
    }
}
-(void)webSocket
{
    @try
    {
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
    {
    @try {
//        instrumentTokensDict = [[NSMutableDictionary alloc]init];
        //pocket socket//
//        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"wss://websocket.kite.trade/?api_key=%@&user_id=%@&public_token=%@",delegate2.APIKey,delegate2.userID,delegate2.publicToken]];
         NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"wss://ws.kite.trade/?api_key=%@&access_token=%@",delegate2.APIKey,delegate2.accessToken]];
      
        //NSLog(@"user %@",delegate2.userID);
        //NSLog(@"user %@",delegate2.publicToken);
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        //     create the socket and assign delegate
        self.socket = [PSWebSocket clientSocketWithRequest:request];
        self.socket.delegate = self;
        //
        //    self.socket.delegateQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        //     open socket
        
        //        if(delegate2.instrumentToken.count>0)
        //    {
        //
        //    }
        if(segmentedControl.selectedSegmentIndex==0)
        {
        if(delegate2.homeNewCompanyArray.count>0)
        {
            self.ifEmptyView.hidden=YES;
            self.marketTableView.hidden=NO;
            [self.socket open];

        }else
        {
            self.activityIndicator.hidden=YES;
            self.ifEmptyView.hidden=NO;
            self.marketTableView.hidden=YES;
        }
        }
        else   if(segmentedControl.selectedSegmentIndex==1)
        {
            if(delegate2.homeNewCompanyArrayCustom.count>0)
            {
                self.ifEmptyView.hidden=YES;
                self.customPortfolioView.hidden=NO;
                [self.socket open];
                
            }else
            {
                self.activityIndicator.hidden=YES;
                self.ifEmptyView.hidden=NO;
                self.customPortfolioView .hidden=YES;
            }
        }

    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    }
    
    else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {@try {
//        instrumentTokensDict = [[NSMutableDictionary alloc]init];
     NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"wss://ws-api.upstox.com/?apiKey=%@&token=%@",delegate2.APIKey ,delegate2.accessToken]];
        
        //NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
           //                                                    cachePolicy:NSURLRequestUseProtocolCachePolicy
             //                                              timeoutInterval:40.0];
       
     NSURLRequest *request = [NSURLRequest requestWithURL:url];
        //     create the socket and assign delegate
        self.socket = [PSWebSocket clientSocketWithRequest:request];
        self.socket.delegate = self;
        //
//            self.socket.delegateQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        //     open socket
        
        //        if(delegate2.instrumentToken.count>0)
        //    {
        //
        //    }
         [self.socket open];
//        if(delegate2.homeNewCompanyArray.count>0)
//        {
//            self.ifEmptyView.hidden=YES;
//            self.marketTableView.hidden=NO;
//            [self.socket open];
//
//        }else
//        {
//            self.activityIndicator.hidden=YES;
//            self.ifEmptyView.hidden=NO;
//            self.marketTableView.hidden=YES;
//        }

        
    }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
    
    }
    
    else
    {
        @try {
//            instrumentTokensDict = [[NSMutableDictionary alloc]init];
           
            NSURL* url = [[NSURL alloc] initWithString:delegate2.ltpServerURL];
//             NSURL* url = [[NSURL alloc] initWithString:@"http://192.168.15.73:8012"];
            self.manager = [[SocketManager alloc] initWithSocketURL:url config:@{@"log": @YES}];
            
            self.socketio=self.manager.defaultSocket;
            
            [self.socketio on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
                //NSLog(@"socket connected");
                
                if(segmentedControl.selectedSegmentIndex==0)
                {
                for(int i=0;i<delegate2.localExchageToken.count;i++)
                {
                        NSMutableArray * subscriptionList=[[NSMutableArray alloc]init];
                         NSMutableDictionary * tmpDict = [[NSMutableDictionary alloc]init];
                    
                         [tmpDict setValue:[delegate2.localExchageToken objectAtIndex:i] forKey:@"securityToken"];
                          [tmpDict setValue:[[delegate2.homeNewCompanyArray objectAtIndex:i] objectForKey:@"exchangevalue"] forKey:@"exchange"];
                    
                   
                    
                         [subscriptionList addObject:tmpDict];
                    
                     [self.socketio emit:@"symbol" with:subscriptionList];
                     [self.socketio emit:@"initltp" with:subscriptionList];
                         
                 
                }
                }
                else
                {
                    for(int i=0;i<delegate2.localExchageTokenCustom.count;i++)
                    {
                        NSMutableArray * subscriptionList=[[NSMutableArray alloc]init];
                        NSMutableDictionary * tmpDict = [[NSMutableDictionary alloc]init];
                        
                        [tmpDict setValue:[delegate2.localExchageTokenCustom objectAtIndex:i] forKey:@"securityToken"];
                        [tmpDict setValue:[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i] objectForKey:@"exchangevalue"] forKey:@"exchange"];
                        
                        
                    
                        
                        [subscriptionList addObject:tmpDict];
                        
                        [self.socketio emit:@"symbol" with:subscriptionList];
                        [self.socketio emit:@"initltp" with:subscriptionList];
                        
                        
                    }
                }
                
               
            }];
            
          
            [self.socketio on:@"heartbeat" callback:^(NSArray* data, SocketAckEmitter* ack) {
                //NSLog(@"%@",data);
                
                
                
                
            }];
         
            [self.socketio on:@"ltp" callback:^(NSArray* data, SocketAckEmitter* ack) {
                //NSLog(@"%@",data);
                
//                dispatch_async(dispatch_get_global_queue(0, 0), ^{
//                   [self broadCastMethod:data];
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        //update UI in main thread.
//                    });
//                });
//                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                if(data.count>0)
                {
                   [self broadCastMethod:data];
                }
//                });
                
                
            }];
            
            [self.socketio on:@"initltp" callback:^(NSArray* data, SocketAckEmitter* ack) {
                //NSLog(@"%@",data);
                
                
                //                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                if(data.count>0)
                {
                    [self broadCastMethod:data];
                }
                //                });
                
                
            }];
            
            [self.socketio connect];
            
            if(segmentedControl.selectedSegmentIndex==0)
            {
           
            if(delegate2.homeNewCompanyArray.count>0)
            {
                self.ifEmptyView.hidden=YES;
                self.marketTableView.hidden=NO;
//                [self.socket open];
               
                
            }else
            {
                self.activityIndicator.hidden=YES;
                self.ifEmptyView.hidden=NO;
                self.marketTableView.hidden=YES;
            }
                
            }
            else
            {
                
                if(delegate2.homeNewCompanyArrayCustom.count>0)
                {
                    self.ifEmptyView.hidden=YES;
                    self.customPortfolioView.hidden=NO;
                    //                [self.socket open];
                    
                    
                }else
                {
                    self.activityIndicator.hidden=YES;
                    self.ifEmptyView.hidden=NO;
                    self.customPortfolioView.hidden=YES;
                }
                
            }
            
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
    }
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }


}

-(void)broadCastMethod:(NSArray *)data
{
    
    @try {
        
        if(ltpReloadBool==true)
        {
        
//       @autoreleasepool {
        //    instrumentTokensDict=[[NSMutableDictionary alloc]init];
            if(segmentedControl.selectedSegmentIndex==-0)
            {
        NSString * newExchangeToken;
        
        
        
        NSString * tmpInstrument=[NSString stringWithFormat:@"%@",[[data objectAtIndex:0] objectForKey:@"stSecurityID"]];
            NSUInteger index=[delegate2.localExchageToken indexOfObject:tmpInstrument];
        
       tmpInstrument = [[tmpInstrument componentsSeparatedByCharactersInSet:
                                [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                               componentsJoinedByString:@""];
        
        for(int j=0;j<delegate2.localExchageToken.count;j++)
        {
            NSString * exchangeToken=[NSString stringWithFormat:@"%@",[delegate2.localExchageToken objectAtIndex:j]];
            
            if([tmpInstrument containsString:exchangeToken])
            {
                newExchangeToken=exchangeToken;
//                [instrumentTokensDict setValue:[data objectAtIndex:0] forKeyPath:newExchangeToken];
                [instrumentTokensDict setValue:[data objectAtIndex:0] forKeyPath:newExchangeToken];
            }
            
            
        }
//       }
        delegate2.tmpValuesDict=instrumentTokensDict;
        if(allKeysList.count>0)
        {
            [allKeysList removeAllObjects];
        }
        allKeysList = [[instrumentTokensDict allKeys]mutableCopy];
        
        //NSLog(@"%@",allKeysList);
        
        self.allKeysList1=[[NSMutableArray alloc]init];
        
        for(int i=0;i<delegate2.localExchageToken.count;i++)
        {
            
            for(int j=0;j<allKeysList.count;j++)
            {
                NSNumber * number1=[delegate2.localExchageToken objectAtIndex:i];
                int int1=[number1 intValue];
                NSNumber * number2=[allKeysList objectAtIndex:j];
                
                int int2=[number2 intValue];
                
                if(int1==int2)
                {
                    [self.allKeysList1 addObject:[allKeysList objectAtIndex:j]];
                }
            }
            
            
        }
        //
        
        
        
        //  [shareListArray addObject:tmpDict];
        
        
        
        
        
        
        
        
        
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
           @try
            {
            self.ifEmptyView.hidden=YES;
            self.marketTableView.hidden=NO;
            self.activityIndicator.hidden=YES;
            [self.activityIndicator stopAnimating];
            
          
            NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:index inSection:0];
         
            // Add them in an index path array
            NSArray* indexArray = [NSArray arrayWithObjects:indexPath1,nil];
            // Launch reload for the two index path
            [self.marketTableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
//            [self.marketTableView reloadData];
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
            
            
        });
                
        }
            else
            
                {
                    NSString * newExchangeToken;
                    
                    
                    
                    NSString * tmpInstrument=[NSString stringWithFormat:@"%@",[[data objectAtIndex:0] objectForKey:@"stSecurityID"]];
                   
                     NSUInteger index1=[delegate2.localExchageTokenCustom indexOfObject:tmpInstrument];
                    tmpInstrument = [[tmpInstrument componentsSeparatedByCharactersInSet:
                                      [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                     componentsJoinedByString:@""];
                    
                    for(int j=0;j<delegate2.localExchageTokenCustom.count;j++)
                    {
                        NSString * exchangeToken=[NSString stringWithFormat:@"%@",[delegate2.localExchageTokenCustom objectAtIndex:j]];
                        
                        if([tmpInstrument containsString:exchangeToken])
                        {
                            newExchangeToken=exchangeToken;
                            //                [instrumentTokensDict setValue:[data objectAtIndex:0] forKeyPath:newExchangeToken];
                            [instrumentTokensDictCustom setValue:[data objectAtIndex:0] forKeyPath:newExchangeToken];
                        }
                        
                        
                    }
                    //       }
                    delegate2.tmpValuesDictCustom=instrumentTokensDictCustom;
                    if(allKeysListCustom.count>0)
                    {
                        [allKeysListCustom removeAllObjects];
                    }
                    allKeysListCustom = [[instrumentTokensDictCustom allKeys]mutableCopy];
                    
            
                    
                    self.allKeysList1Custom=[[NSMutableArray alloc]init];
                    
                    for(int i=0;i<delegate2.localExchageTokenCustom.count;i++)
                    {
                        
                        for(int j=0;j<allKeysListCustom.count;j++)
                        {
                            NSNumber * number1=[delegate2.localExchageTokenCustom objectAtIndex:i];
                            int int1=[number1 intValue];
                            NSNumber * number2=[allKeysListCustom objectAtIndex:j];
                            
                            int int2=[number2 intValue];
                            
                            if(int1==int2)
                            {
                                [self.allKeysList1Custom addObject:[allKeysListCustom objectAtIndex:j]];
                            }
                        }
                        
                        
                    }
                    //
                    
                    
                    
                    //  [shareListArray addObject:tmpDict];
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        @try
                        {
                            if(delegate2.localExchageTokenCustom.count>0)
                            {
                            self.ifEmptyView.hidden=YES;
                            self.customPortfolioView.hidden=NO;
                            self.activityIndicator.hidden=YES;
                            [self.activityIndicator stopAnimating];
                            
                            
                            NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:index1 inSection:0];

                            // Add them in an index path array
                            NSArray* indexArray = [NSArray arrayWithObjects:indexPath1,nil];
                            // Launch reload for the two index path
                            [self.customPortfolioTblView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
//                                        [self.customPortfolioTblView reloadData];
                            }else
                            {
                                self.ifEmptyView.hidden=NO;
                            }
                        } @catch (NSException *exception) {
                            
                        } @finally {
                            
                        }
                        
                        
                    });
                
            }
        }
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
}

- (void)webSocketDidOpen:(PSWebSocket *)webSocket {
    
    @try
    {
    //NSLog(@"The websocket handshake completed and is now open!");
    
    //NSLog(@"%@",delegate2.homeInstrumentToken);
    
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
    {
        if(segmentedControl.selectedSegmentIndex==0)
        {
        localInstrumentToken=[[NSMutableArray alloc]init];
        
        localInstrumentToken=delegate2.homeInstrumentToken;
        }
        else  if(segmentedControl.selectedSegmentIndex==1)
        {
            localInstrumentToken=[[NSMutableArray alloc]init];
            
            localInstrumentToken=delegate2.homeInstrumentTokenCustom;
        }
        
        localInstrumentToken=[[[localInstrumentToken reverseObjectEnumerator] allObjects] mutableCopy];
            
        
        if(localInstrumentToken.count>0)
        {
            
            NSDictionary * subscribeDict=@{@"a": @"subscribe", @"v":localInstrumentToken};
            
            
            NSData *json;
            
            NSError * error;
            
            
            
            
            // Dictionary convertable to JSON ?
            if ([NSJSONSerialization isValidJSONObject:subscribeDict])
            {
                // Serialize the dictionary
                json = [NSJSONSerialization dataWithJSONObject:subscribeDict options:NSJSONWritingPrettyPrinted error:&error];
                
                // If no errors, let's view the JSON
                if (json != nil && error == nil)
                {
                    string1 = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                    
                    //            delegate2.message=[NSString stringWithFormat:@"%@",str];
                    //
                    
                    //NSLog(@"JSON: %@",string1);
                    
                }
            }
            
            
            [self.socket send:string1];
            
            
            
            
            
            
            NSArray * mode=@[@"full",localInstrumentToken];
            
            subscribeDict=@{@"a": @"mode", @"v": mode};
            
            
            
            //     Dictionary convertable to JSON ?
            if ([NSJSONSerialization isValidJSONObject:subscribeDict])
            {
                // Serialize the dictionary
                json = [NSJSONSerialization dataWithJSONObject:subscribeDict options:NSJSONWritingPrettyPrinted error:&error];
                
                
                // If no errors, let's view the JSON
                if (json != nil && error == nil)
                {
                    _message = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                    
                    //            delegate2.message=[NSString stringWithFormat:@"%@",str];
                    
                    //NSLog(@"JSON: %@",_message);
                    
                }
            }
            [self.socket send:_message];
            
        }
            
        
       
        
        
    }
    
    else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    
    {
//
    }
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

/*!
 @discussion used to unsubscribe symbols(upstox).
 */

-(void)unSubMethod
{
    @try
    {
    if(segmentedControl.selectedSegmentIndex==0)
    {
        localInstrumentToken=[[NSMutableArray alloc]init];
        
        localInstrumentToken=delegate2.homeInstrumentToken;
        symbolArray=[[NSMutableArray alloc]init];
        
        
        NSString * url;
        //
        //    NSMutableArray * exchangeArray=[[NSMutableArray alloc]init];
        //NSLog(@"Home company:%@",delegate2.homeNewCompanyArray);
        NSMutableArray * NSEEquityArray=[[NSMutableArray alloc]init];
        NSMutableArray * BSEEquityArray=[[NSMutableArray alloc]init];
        NSMutableArray * NSEDerivativeArray=[[NSMutableArray alloc]init];
        NSMutableArray * BSEDerivativeArray=[[NSMutableArray alloc]init];
        NSMutableArray * currencyArray=[[NSMutableArray alloc]init];
        NSMutableArray * commodityArray=[[NSMutableArray alloc]init];
        NSMutableArray * symbolNameArray;
        
        for(int i=0;i<delegate2.homeNewCompanyArray.count;i++)
        {
            NSString * exchange=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i] objectForKey:@"segment"]];
            
            if([exchange containsString:@"NSE"])
            {
                
                //            exchange=@"nse_eq";
                [NSEEquityArray addObject:[delegate2.homeNewCompanyArray objectAtIndex:i]];
                
            }
            
            else if([exchange containsString:@"BSE"])
            {
                
                //            exchange=@"bse_eq";
                [BSEEquityArray addObject:[delegate2.homeNewCompanyArray objectAtIndex:i]];
                
            }
            
            else if([exchange containsString:@"NFO"])
            {
                
                //            exchange=@"nse_fo";
                [NSEDerivativeArray addObject:[delegate2.homeNewCompanyArray objectAtIndex:i]];
                
            }
            
            else if([exchange containsString:@"BFO"])
            {
                
                //            exchange=@"bse_fo";
                [BSEDerivativeArray addObject:[delegate2.homeNewCompanyArray objectAtIndex:i]];
                
            }
            
            else if([exchange containsString:@"CDS"])
            {
                
                //            exchange=@"ncd_fo";
                [currencyArray addObject:[delegate2.homeNewCompanyArray objectAtIndex:i]];
                
            }
            
            else if([exchange containsString:@"MCX"])
            {
                
                //            exchange=@"mcx_fo";
                [commodityArray addObject:[delegate2.homeNewCompanyArray objectAtIndex:i]];
                
            }
            
            
            
            
        }
        //    NSArray * exchangeArray=@[@"bse_eq",@"nse_eq ",@"nse_eq ",@"nse_fo ",@"ncd_fo",@"mcx_fo ",@"nse_index",@"bcd_fo",@"bse_index"];
        
        //        NSString * result = [[symbolArray valueForKey:@"description"] componentsJoinedByString:@","];
        //
        if(NSEEquityArray.count>0)
        {
            symbolNameArray=[[NSMutableArray alloc]init];
            
            for(int i=0;i<NSEEquityArray.count;i++)
            {
                NSString * symbol=[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"symbol"];
                CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)symbol, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
                
                symbol=[NSString stringWithFormat:@"%@",newString];
                
                [symbolNameArray addObject:symbol];
            }
            NSString *joinedString = [symbolNameArray componentsJoinedByString:@","];
            url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/nse_eq/?symbol=%@",joinedString];
            
            TagEncode * enocde=[[TagEncode alloc]init];
            enocde.inputRequestString=@"upstoxrequest";
            NSMutableArray * inputArray=[[NSMutableArray alloc]init];
            [inputArray addObject:url];
            [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
            
                
            }];
        }
        
        if(BSEEquityArray.count>0)
        {
            symbolNameArray=[[NSMutableArray alloc]init];
            
            for(int i=0;i<BSEEquityArray.count;i++)
            {
                NSString * symbol=[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"symbol"];
                CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)symbol, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
                
                symbol=[NSString stringWithFormat:@"%@",newString];
                [symbolNameArray addObject:symbol];
            }
            NSString *joinedString = [symbolNameArray componentsJoinedByString:@","];
            url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/bse_eq/?symbol=%@",joinedString];
            
            TagEncode * enocde=[[TagEncode alloc]init];
            enocde.inputRequestString=@"upstoxrequest";
            NSMutableArray * inputArray=[[NSMutableArray alloc]init];
            [inputArray addObject:url];
            [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
                
              
            }];
        }
        if(NSEDerivativeArray.count>0)
        {
            symbolNameArray=[[NSMutableArray alloc]init];
            
            for(int i=0;i<NSEDerivativeArray.count;i++)
            {
                NSString * symbol=[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"securitydesc"];
                CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)symbol, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
                
                symbol=[NSString stringWithFormat:@"%@",newString];
                [symbolNameArray addObject:symbol];
                
            }
            NSString *joinedString = [symbolNameArray componentsJoinedByString:@","];
            url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/nse_fo/?symbol=%@",joinedString];
            
            TagEncode * enocde=[[TagEncode alloc]init];
            enocde.inputRequestString=@"upstoxrequest";
            NSMutableArray * inputArray=[[NSMutableArray alloc]init];
            [inputArray addObject:url];
            [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
                
               
            }];
        }
        if(BSEDerivativeArray.count>0)
        {
            symbolNameArray=[[NSMutableArray alloc]init];
            
            for(int i=0;i<NSEDerivativeArray.count;i++)
            {
                NSString * symbol=[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"securitydesc"];
                CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)symbol, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
                
                symbol=[NSString stringWithFormat:@"%@",newString];
                [symbolNameArray addObject:symbol];
            }
            NSString *joinedString = [symbolNameArray componentsJoinedByString:@","];
            url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/bse_fo/?symbol=%@",joinedString];
            
            TagEncode * enocde=[[TagEncode alloc]init];
            enocde.inputRequestString=@"upstoxrequest";
            NSMutableArray * inputArray=[[NSMutableArray alloc]init];
            [inputArray addObject:url];
            [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
               
            }];
        }
        if(currencyArray.count>0)
        {
            symbolNameArray=[[NSMutableArray alloc]init];
            
            for(int i=0;i<NSEDerivativeArray.count;i++)
            {
                NSString * symbol=[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"securitydesc"];
                CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)symbol, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
                
                symbol=[NSString stringWithFormat:@"%@",newString];
                [symbolNameArray addObject:symbol];
            }
            NSString *joinedString = [symbolNameArray componentsJoinedByString:@","];
            url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/ncd_fo/?symbol=%@",joinedString];
            
            TagEncode * enocde=[[TagEncode alloc]init];
            enocde.inputRequestString=@"upstoxrequest";
            NSMutableArray * inputArray=[[NSMutableArray alloc]init];
            [inputArray addObject:url];
            [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
                
             
            }];
        }
        if(commodityArray.count>0)
        {
            symbolNameArray=[[NSMutableArray alloc]init];
            
            for(int i=0;i<NSEDerivativeArray.count;i++)
            {
                NSString * symbol=[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"securitydesc"];
                CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)symbol, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
                
                symbol=[NSString stringWithFormat:@"%@",newString];
                [symbolNameArray addObject:symbol];
            }
            NSString *joinedString = [symbolNameArray componentsJoinedByString:@","];
            url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/mcx_fo/?symbol=%@",joinedString];
            
            TagEncode * enocde=[[TagEncode alloc]init];
            enocde.inputRequestString=@"upstoxrequest";
            NSMutableArray * inputArray=[[NSMutableArray alloc]init];
            [inputArray addObject:url];
            [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
                
              
            }];
        }
        
        
        
        
        
        //    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
        //                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
        //                                                       timeoutInterval:40.0];
        //    [request setHTTPMethod:@"GET"];
        //    [request setAllHTTPHeaderFields:headers];
        //
        //    NSURLSession *session = [NSURLSession sharedSession];
        //    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
        //                                                completionHandler:^(NSData* data, NSURLResponse *response, NSError *error) {
        //                                                    if (error) {
        //                                                        //NSLog(@"%@", error);
        //                                                    } else {
        //                                                        NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *) response;
        //                                                        //NSLog(@"%@", httpResponse);
        //
        //
        //                                                        NSMutableArray * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        //
        //                                                        //NSLog(@"%@",json);
        //
        //                                                    }
        //
        //
        //
        //                                                    dispatch_async(dispatch_get_main_queue(), ^{
        //
        //                                                      if(i==delegate2.homeNewCompanyArray.count-1)
        //                                                       {
        ////                                                        [self webSocket];
        //
        //                                                        self.marketTableView.delegate=self;
        //                                                        self.marketTableView.dataSource=self;
        //                                                        [self.marketTableView reloadData];
        //                                                       }
        //
        //
        //
        //
        //
        //
        //
        //                                                    });
        //
        //
        //
        //                                                }];
        //    [dataTask resume];
        //
        
        
    }
    else
    {
        localInstrumentToken=[[NSMutableArray alloc]init];
        
        localInstrumentToken=delegate2.homeInstrumentTokenCustom;
        
        //    localInstrumentToken=[[[localInstrumentToken reverseObjectEnumerator] allObjects] mutableCopy];
        
        
        
        symbolArray=[[NSMutableArray alloc]init];
        
        //    NSString * symbol;
        //
        //    NSMutableArray * exchangeArray=[[NSMutableArray alloc]init];
        
        for(int i=0;i<delegate2.homeNewCompanyArrayCustom.count;i++)
        {
            NSString * symbol=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i] objectForKey:@"symbol"]];
            if([upstoxSymbolName isEqualToString:symbol])
            {
            }
            
            else
            {
                NSString * exchange=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i] objectForKey:@"segment"]];
                
                if([exchange containsString:@"NSE"])
                {
                    
                    
                    exchange=@"nse_eq";
                    
                }
                
                else if([exchange containsString:@"BSE"])
                {
                    
                    exchange=@"bse_eq";
                    
                }
                
                else if([exchange containsString:@"NFO"])
                {
                    
                    exchange=@"nse_fo";
                    
                }
                
                else if([exchange containsString:@"BFO"])
                {
                    
                    exchange=@"bse_fo";
                    
                }
                
                else if([exchange containsString:@"CDS"])
                {
                    
                    exchange=@"ncd_fo";
                    
                }
                
                else if([exchange containsString:@"MCX"])
                {
                    
                    exchange=@"mcx_fo";
                    
                }
                
                
                if([exchange containsString:@"fo"])
                {
                    //             [symbolArray addObject:[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"securitydesc"]];
                    
                    symbol=[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]objectForKey:@"securitydesc"];
                }
                
                else
                {
                    //        [symbolArray addObject:[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"symbol"]];
                    symbol=[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]objectForKey:@"symbol"];
                }
                
                CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)symbol, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
                
                symbol=[NSString stringWithFormat:@"%@",newString];
                
                
                //    NSArray * exchangeArray=@[@"bse_eq",@"nse_eq ",@"nse_eq ",@"nse_fo ",@"ncd_fo",@"mcx_fo ",@"nse_index",@"bcd_fo",@"bse_index"];
                
                //        NSString * result = [[symbolArray valueForKey:@"description"] componentsJoinedByString:@","];
                //
                
                
                NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
                NSDictionary *headers = @{ @"x-api-key": delegate2.APIKey,
                                           @"authorization": access,
                                           };
                NSString * url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/%@/?symbol=%@",exchange,symbol];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                                       cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                   timeoutInterval:40.0];
                [request setHTTPMethod:@"GET"];
                [request setAllHTTPHeaderFields:headers];
                
                NSURLSession *session = [NSURLSession sharedSession];
                NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                            completionHandler:^(NSData* data, NSURLResponse *response, NSError *error) {
                                                                if (error) {
                                                                    //NSLog(@"%@", error);
                                                                } else {
                                                                    NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *) response;
                                                                    //NSLog(@"%@", httpResponse);
                                                                    
                                                                    
                                                                    NSMutableArray * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                    
                                                                    //NSLog(@"%@",json);
                                                                }
                                                                
                                                                
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    
                                                                    
                                                                    //                                                            if(i==delegate2.homeNewCompanyArray.count-1)
                                                                    //                                                            {
                                                                    //                                                                if(disapperCheck==true)
                                                                    //                                                                {
                                                                    //                                                                    disapperCheck=false;
                                                                    //
                                                                    //                                                                    StockView1 * stockDetails=[self.storyboard instantiateViewControllerWithIdentifier:@"stock1"];
                                                                    //
                                                                    //                                                                    [self.navigationController pushViewController:stockDetails animated:YES];
                                                                    //
                                                                    //                                                                }
                                                                    //
                                                                    //                                                                else
                                                                    //                                                                {
                                                                    //
                                                                    
                                                                    //                                                                    [self webSocket];
                                                                    //                                                                }
                                                                    //
                                                                    //
                                                                    //                                                            }
                                                                    //
                                                                    
                                                                    
                                                                });
                                                            }];
                [dataTask resume];
                
            }
            
        }
        
    }
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}


/*!
 @discussion used to subscribe symbols(upstox).
 */
-(void)subMethod
{
    @try
    {
    if(segmentedControl.selectedSegmentIndex==0)
    {
    localInstrumentToken=[[NSMutableArray alloc]init];
    
    localInstrumentToken=delegate2.homeInstrumentToken;
    symbolArray=[[NSMutableArray alloc]init];
    
    
        NSString * url;
    //
    //    NSMutableArray * exchangeArray=[[NSMutableArray alloc]init];
    //NSLog(@"Home company:%@",delegate2.homeNewCompanyArray);
        NSMutableArray * NSEEquityArray=[[NSMutableArray alloc]init];
        NSMutableArray * BSEEquityArray=[[NSMutableArray alloc]init];
        NSMutableArray * NSEDerivativeArray=[[NSMutableArray alloc]init];
        NSMutableArray * BSEDerivativeArray=[[NSMutableArray alloc]init];
        NSMutableArray * currencyArray=[[NSMutableArray alloc]init];
        NSMutableArray * commodityArray=[[NSMutableArray alloc]init];
        NSMutableArray * symbolNameArray;
    
    for(int i=0;i<delegate2.homeNewCompanyArray.count;i++)
    {
        NSString * exchange=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i] objectForKey:@"segment"]];
        
        if([exchange containsString:@"NSE"])
        {
            
//            exchange=@"nse_eq";
            [NSEEquityArray addObject:[delegate2.homeNewCompanyArray objectAtIndex:i]];
            
        }
        
        else if([exchange containsString:@"BSE"])
        {
            
//            exchange=@"bse_eq";
             [BSEEquityArray addObject:[delegate2.homeNewCompanyArray objectAtIndex:i]];
            
        }
        
        else if([exchange containsString:@"NFO"])
        {
            
//            exchange=@"nse_fo";
             [NSEDerivativeArray addObject:[delegate2.homeNewCompanyArray objectAtIndex:i]];
            
        }
        
        else if([exchange containsString:@"BFO"])
        {
            
//            exchange=@"bse_fo";
              [BSEDerivativeArray addObject:[delegate2.homeNewCompanyArray objectAtIndex:i]];
            
        }
        
        else if([exchange containsString:@"CDS"])
        {
            
//            exchange=@"ncd_fo";
               [currencyArray addObject:[delegate2.homeNewCompanyArray objectAtIndex:i]];
            
        }
        
        else if([exchange containsString:@"MCX"])
        {
            
//            exchange=@"mcx_fo";
              [commodityArray addObject:[delegate2.homeNewCompanyArray objectAtIndex:i]];
            
        }
        
       
        
        
    }
        //    NSArray * exchangeArray=@[@"bse_eq",@"nse_eq ",@"nse_eq ",@"nse_fo ",@"ncd_fo",@"mcx_fo ",@"nse_index",@"bcd_fo",@"bse_index"];
        
        //        NSString * result = [[symbolArray valueForKey:@"description"] componentsJoinedByString:@","];
        //
       if(NSEEquityArray.count>0)
       {
           symbolNameArray=[[NSMutableArray alloc]init];
           
           for(int i=0;i<NSEEquityArray.count;i++)
           {
               NSString * symbol=[[NSEEquityArray objectAtIndex:i]objectForKey:@"symbol"];
               CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)symbol, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
               
               symbol=[NSString stringWithFormat:@"%@",newString];
               [symbolNameArray addObject:symbol];
           }
           NSString *joinedString = [symbolNameArray componentsJoinedByString:@","];
         url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/nse_eq/?symbol=%@",joinedString];
           
        TagEncode * enocde=[[TagEncode alloc]init];
        enocde.inputRequestString=@"upstoxrequest";
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:url];
        [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
               dispatch_async(dispatch_get_main_queue(), ^{
                 
//            self.marketTableView.delegate=self;
//            self.marketTableView.dataSource=self;
//            [self.marketTableView reloadData];
               });
         }];
       }
        
        if(BSEEquityArray.count>0)
        {
            symbolNameArray=[[NSMutableArray alloc]init];
            
            for(int i=0;i<BSEEquityArray.count;i++)
            {
                NSString * symbol=[[BSEEquityArray objectAtIndex:i]objectForKey:@"symbol"];
                CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)symbol, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
                
                symbol=[NSString stringWithFormat:@"%@",newString];
                [symbolNameArray addObject:symbol];
            }
            NSString *joinedString = [symbolNameArray componentsJoinedByString:@","];
            url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/bse_eq/?symbol=%@",joinedString];
            
            TagEncode * enocde=[[TagEncode alloc]init];
            enocde.inputRequestString=@"upstoxrequest";
            NSMutableArray * inputArray=[[NSMutableArray alloc]init];
            [inputArray addObject:url];
            [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
                  dispatch_async(dispatch_get_main_queue(), ^{
                      
//                self.marketTableView.delegate=self;
//                self.marketTableView.dataSource=self;
//                [self.marketTableView reloadData];
                  });
            }];
        }
        if(NSEDerivativeArray.count>0)
        {
            symbolNameArray=[[NSMutableArray alloc]init];
            
            for(int i=0;i<NSEDerivativeArray.count;i++)
            {
                
                NSString * symbol=[[NSEDerivativeArray objectAtIndex:i]objectForKey:@"securitydesc"];
                if([delegate2.brokerNameStr isEqualToString:@"Upstox"]&&[symbol containsString:@"BANKNIFTY"])
                {
                    symbol = [[NSEDerivativeArray objectAtIndex:i]objectForKey:@"securitydescupstox"];
                }
                CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)symbol, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
                
                symbol=[NSString stringWithFormat:@"%@",newString];
                [symbolNameArray addObject:symbol];
               
            }
            NSString *joinedString = [symbolNameArray componentsJoinedByString:@","];
            url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/nse_fo/?symbol=%@",joinedString];
            
            TagEncode * enocde=[[TagEncode alloc]init];
            enocde.inputRequestString=@"upstoxrequest";
            NSMutableArray * inputArray=[[NSMutableArray alloc]init];
            [inputArray addObject:url];
            [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
//                    self.marketTableView.delegate=self;
//                    self.marketTableView.dataSource=self;
//                    [self.marketTableView reloadData];
                });
            }];
        }
        if(BSEDerivativeArray.count>0)
        {
            symbolNameArray=[[NSMutableArray alloc]init];
            
            for(int i=0;i<BSEDerivativeArray.count;i++)
            {
                NSString * symbol=[[BSEDerivativeArray objectAtIndex:i]objectForKey:@"securitydesc"];
                if([delegate2.brokerNameStr isEqualToString:@"Upstox"]&&[symbol containsString:@"BANKNIFTY"])
                {
                    symbol = [[BSEDerivativeArray objectAtIndex:i]objectForKey:@"securitydescupstox"];
                }
                CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)symbol, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
                
                symbol=[NSString stringWithFormat:@"%@",newString];
                [symbolNameArray addObject:symbol];
            }
            NSString *joinedString = [symbolNameArray componentsJoinedByString:@","];
            url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/bse_fo/?symbol=%@",joinedString];
            
            TagEncode * enocde=[[TagEncode alloc]init];
            enocde.inputRequestString=@"upstoxrequest";
            NSMutableArray * inputArray=[[NSMutableArray alloc]init];
            [inputArray addObject:url];
            [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
//                    self.marketTableView.delegate=self;
//                    self.marketTableView.dataSource=self;
//                    [self.marketTableView reloadData];
                });
            }];
        }
        if(currencyArray.count>0)
        {
            symbolNameArray=[[NSMutableArray alloc]init];
            
            for(int i=0;i<currencyArray.count;i++)
            {
                NSString * symbol=[[currencyArray objectAtIndex:i]objectForKey:@"securitydesc"];
                CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)symbol, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
                
                symbol=[NSString stringWithFormat:@"%@",newString];
                [symbolNameArray addObject:symbol];
            }
            NSString *joinedString = [symbolNameArray componentsJoinedByString:@","];
            url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/ncd_fo/?symbol=%@",joinedString];
            
            TagEncode * enocde=[[TagEncode alloc]init];
            enocde.inputRequestString=@"upstoxrequest";
            NSMutableArray * inputArray=[[NSMutableArray alloc]init];
            [inputArray addObject:url];
            [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
//                    self.marketTableView.delegate=self;
//                    self.marketTableView.dataSource=self;
//                    [self.marketTableView reloadData];
                });
            }];
        }
        if(commodityArray.count>0)
        {
            symbolNameArray=[[NSMutableArray alloc]init];
            
            for(int i=0;i<commodityArray.count;i++)
            {
                NSString * symbol=[[commodityArray objectAtIndex:i]objectForKey:@"securitydesc"];
                CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)symbol, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
                
                symbol=[NSString stringWithFormat:@"%@",newString];
                [symbolNameArray addObject:symbol];
            }
            NSString *joinedString = [symbolNameArray componentsJoinedByString:@","];
            url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/mcx_fo/?symbol=%@",joinedString];
            
            TagEncode * enocde=[[TagEncode alloc]init];
            enocde.inputRequestString=@"upstoxrequest";
            NSMutableArray * inputArray=[[NSMutableArray alloc]init];
            [inputArray addObject:url];
            [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
//                self.marketTableView.delegate=self;
//                self.marketTableView.dataSource=self;
//                [self.marketTableView reloadData];
                 });
            }];
        }
        
        
     url = [NSString stringWithFormat:@"https://api.upstox.com/live/socket-params/"];
        TagEncode * enocde=[[TagEncode alloc]init];
        enocde.inputRequestString=@"upstoxrequest";
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:url];
        [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
            //NSLog(@"%@",dict);
        }];
   
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
//                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                       timeoutInterval:40.0];
//    [request setHTTPMethod:@"GET"];
//    [request setAllHTTPHeaderFields:headers];
//
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
//                                                completionHandler:^(NSData* data, NSURLResponse *response, NSError *error) {
//                                                    if (error) {
//                                                        //NSLog(@"%@", error);
//                                                    } else {
//                                                        NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *) response;
//                                                        //NSLog(@"%@", httpResponse);
//
//
//                                                        NSMutableArray * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//
//                                                        //NSLog(@"%@",json);
//
//                                                    }
//
//
//
//                                                    dispatch_async(dispatch_get_main_queue(), ^{
//
//                                                      if(i==delegate2.homeNewCompanyArray.count-1)
//                                                       {
////                                                        [self webSocket];
//
//                                                        self.marketTableView.delegate=self;
//                                                        self.marketTableView.dataSource=self;
//                                                        [self.marketTableView reloadData];
//                                                       }
//
//
//
//
//
//
//
//                                                    });
//
//
//
//                                                }];
//    [dataTask resume];
//
  
        
    }
    else if(segmentedControl.selectedSegmentIndex==1)
    {
        localInstrumentToken=[[NSMutableArray alloc]init];
        
        localInstrumentToken=delegate2.homeInstrumentTokenCustom;
        symbolArray=[[NSMutableArray alloc]init];
        
        
        NSString * url;
        //
        //    NSMutableArray * exchangeArray=[[NSMutableArray alloc]init];
        //NSLog(@"Home company:%@",delegate2.homeNewCompanyArrayCustom);
        NSMutableArray * NSEEquityArray=[[NSMutableArray alloc]init];
        NSMutableArray * BSEEquityArray=[[NSMutableArray alloc]init];
        NSMutableArray * NSEDerivativeArray=[[NSMutableArray alloc]init];
        NSMutableArray * BSEDerivativeArray=[[NSMutableArray alloc]init];
        NSMutableArray * currencyArray=[[NSMutableArray alloc]init];
        NSMutableArray * commodityArray=[[NSMutableArray alloc]init];
        NSMutableArray * symbolNameArray;
        
        for(int i=0;i<delegate2.homeNewCompanyArrayCustom.count;i++)
        {
            NSString * exchange=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i] objectForKey:@"segment"]];
            
            if([exchange containsString:@"NSE"])
            {
                
                //            exchange=@"nse_eq";
                [NSEEquityArray addObject:[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]];
                
            }
            
            else if([exchange containsString:@"BSE"])
            {
                
                //            exchange=@"bse_eq";
                [BSEEquityArray addObject:[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]];
                
            }
            
            else if([exchange containsString:@"NFO"])
            {
                
                //            exchange=@"nse_fo";
                [NSEDerivativeArray addObject:[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]];
                
            }
            
            else if([exchange containsString:@"BFO"])
            {
                
                //            exchange=@"bse_fo";
                [BSEDerivativeArray addObject:[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]];
                
            }
            
            else if([exchange containsString:@"CDS"])
            {
                
                //            exchange=@"ncd_fo";
                [currencyArray addObject:[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]];
                
            }
            
            else if([exchange containsString:@"MCX"])
            {
                
                //            exchange=@"mcx_fo";
                [commodityArray addObject:[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]];
                
            }
            
            
            
            
        }
        //    NSArray * exchangeArray=@[@"bse_eq",@"nse_eq ",@"nse_eq ",@"nse_fo ",@"ncd_fo",@"mcx_fo ",@"nse_index",@"bcd_fo",@"bse_index"];
        
        //        NSString * result = [[symbolArray valueForKey:@"description"] componentsJoinedByString:@","];
        //
        if(NSEEquityArray.count>0)
        {
            symbolNameArray=[[NSMutableArray alloc]init];
            
            for(int i=0;i<NSEEquityArray.count;i++)
            {
                NSString * symbol=[[NSEEquityArray objectAtIndex:i]objectForKey:@"symbol"];
                CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)symbol, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
                
                symbol=[NSString stringWithFormat:@"%@",newString];
                [symbolNameArray addObject:symbol];
            }
            NSString *joinedString = [symbolNameArray componentsJoinedByString:@","];
            url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/nse_eq/?symbol=%@",joinedString];
            
            TagEncode * enocde=[[TagEncode alloc]init];
            enocde.inputRequestString=@"upstoxrequest";
            NSMutableArray * inputArray=[[NSMutableArray alloc]init];
            [inputArray addObject:url];
            [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    //            self.marketTableView.delegate=self;
                    //            self.marketTableView.dataSource=self;
                    //            [self.marketTableView reloadData];
                });
            }];
        }
        
        if(BSEEquityArray.count>0)
        {
            symbolNameArray=[[NSMutableArray alloc]init];
            
            for(int i=0;i<BSEEquityArray.count;i++)
            {
                NSString * symbol=[[BSEEquityArray objectAtIndex:i]objectForKey:@"symbol"];
                CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)symbol, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
                
                symbol=[NSString stringWithFormat:@"%@",newString];
                [symbolNameArray addObject:symbol];
            }
            NSString *joinedString = [symbolNameArray componentsJoinedByString:@","];
            url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/bse_eq/?symbol=%@",joinedString];
            
            TagEncode * enocde=[[TagEncode alloc]init];
            enocde.inputRequestString=@"upstoxrequest";
            NSMutableArray * inputArray=[[NSMutableArray alloc]init];
            [inputArray addObject:url];
            [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    //                self.marketTableView.delegate=self;
                    //                self.marketTableView.dataSource=self;
                    //                [self.marketTableView reloadData];
                });
            }];
        }
        if(NSEDerivativeArray.count>0)
        {
            symbolNameArray=[[NSMutableArray alloc]init];
            
            for(int i=0;i<NSEDerivativeArray.count;i++)
            {
                NSString * symbol=[[NSEDerivativeArray objectAtIndex:i]objectForKey:@"securitydesc"];
                CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)symbol, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
                
                symbol=[NSString stringWithFormat:@"%@",newString];
                [symbolNameArray addObject:symbol];
                
            }
            NSString *joinedString = [symbolNameArray componentsJoinedByString:@","];
            url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/nse_fo/?symbol=%@",joinedString];
            
            TagEncode * enocde=[[TagEncode alloc]init];
            enocde.inputRequestString=@"upstoxrequest";
            NSMutableArray * inputArray=[[NSMutableArray alloc]init];
            [inputArray addObject:url];
            [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    //                    self.marketTableView.delegate=self;
                    //                    self.marketTableView.dataSource=self;
                    //                    [self.marketTableView reloadData];
                });
            }];
        }
        if(BSEDerivativeArray.count>0)
        {
            symbolNameArray=[[NSMutableArray alloc]init];
            
            for(int i=0;i<BSEDerivativeArray.count;i++)
            {
                NSString * symbol=[[BSEDerivativeArray objectAtIndex:i]objectForKey:@"securitydesc"];
                CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)symbol, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
                
                symbol=[NSString stringWithFormat:@"%@",newString];
                [symbolNameArray addObject:symbol];
            }
            NSString *joinedString = [symbolNameArray componentsJoinedByString:@","];
            url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/bse_fo/?symbol=%@",joinedString];
            
            TagEncode * enocde=[[TagEncode alloc]init];
            enocde.inputRequestString=@"upstoxrequest";
            NSMutableArray * inputArray=[[NSMutableArray alloc]init];
            [inputArray addObject:url];
            [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    //                    self.marketTableView.delegate=self;
                    //                    self.marketTableView.dataSource=self;
                    //                    [self.marketTableView reloadData];
                });
            }];
        }
        if(currencyArray.count>0)
        {
            symbolNameArray=[[NSMutableArray alloc]init];
            
            for(int i=0;i<currencyArray.count;i++)
            {
                NSString * symbol=[[currencyArray objectAtIndex:i]objectForKey:@"securitydesc"];
                CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)symbol, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
                
                symbol=[NSString stringWithFormat:@"%@",newString];
                [symbolNameArray addObject:symbol];
            }
            NSString *joinedString = [symbolNameArray componentsJoinedByString:@","];
            url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/ncd_fo/?symbol=%@",joinedString];
            
            TagEncode * enocde=[[TagEncode alloc]init];
            enocde.inputRequestString=@"upstoxrequest";
            NSMutableArray * inputArray=[[NSMutableArray alloc]init];
            [inputArray addObject:url];
            [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    //                    self.marketTableView.delegate=self;
                    //                    self.marketTableView.dataSource=self;
                    //                    [self.marketTableView reloadData];
                });
            }];
        }
        if(commodityArray.count>0)
        {
            symbolNameArray=[[NSMutableArray alloc]init];
            
            for(int i=0;i<commodityArray.count;i++)
            {
                NSString * symbol=[[commodityArray objectAtIndex:i]objectForKey:@"securitydesc"];
                CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)symbol, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
                
                symbol=[NSString stringWithFormat:@"%@",newString];
                [symbolNameArray addObject:symbol];
            }
            NSString *joinedString = [symbolNameArray componentsJoinedByString:@","];
            url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/mcx_fo/?symbol=%@",joinedString];
            
            TagEncode * enocde=[[TagEncode alloc]init];
            enocde.inputRequestString=@"upstoxrequest";
            NSMutableArray * inputArray=[[NSMutableArray alloc]init];
            [inputArray addObject:url];
            [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    //                self.marketTableView.delegate=self;
                    //                self.marketTableView.dataSource=self;
                    //                [self.marketTableView reloadData];
                });
            }];
        }
        
        
        url = [NSString stringWithFormat:@"https://api.upstox.com/live/socket-params/"];
        TagEncode * enocde=[[TagEncode alloc]init];
        enocde.inputRequestString=@"upstoxrequest";
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:url];
        [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
            //NSLog(@"%@",dict);
        }];
        
        //    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
        //                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
        //                                                       timeoutInterval:40.0];
        //    [request setHTTPMethod:@"GET"];
        //    [request setAllHTTPHeaderFields:headers];
        //
        //    NSURLSession *session = [NSURLSession sharedSession];
        //    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
        //                                                completionHandler:^(NSData* data, NSURLResponse *response, NSError *error) {
        //                                                    if (error) {
        //                                                        //NSLog(@"%@", error);
        //                                                    } else {
        //                                                        NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *) response;
        //                                                        //NSLog(@"%@", httpResponse);
        //
        //
        //                                                        NSMutableArray * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        //
        //                                                        //NSLog(@"%@",json);
        //
        //                                                    }
        //
        //
        //
        //                                                    dispatch_async(dispatch_get_main_queue(), ^{
        //
        //                                                      if(i==delegate2.homeNewCompanyArray.count-1)
        //                                                       {
        ////                                                        [self webSocket];
        //
        //                                                        self.marketTableView.delegate=self;
        //                                                        self.marketTableView.dataSource=self;
        //                                                        [self.marketTableView reloadData];
        //                                                       }
        //
        //
        //
        //
        //
        //
        //
        //                                                    });
        //
        //
        //
        //                                                }];
        //    [dataTask resume];
        //
        
        
    }

    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}


- (void)webSocket:(PSWebSocket *)webSocket didReceiveMessage:(id)message {
    
    
    @try {
        
        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
        {
        //NSLog(@"The websocket received a message: %@",message);
        
        
        
        NSData * data=[NSKeyedArchiver archivedDataWithRootObject:message];
        
       
        
        //NSLog(@"size of Object: %zd",malloc_size((__bridge const void *)(message)));
        
        if(data.length > 247)
        {
            
            
            //NSLog(@"The websocket received a message: %@",message);
            
            
            id packets = [message subdataWithRange:NSMakeRange(0,2)];
            packetsNumber = CFSwapInt16BigToHost(*(int16_t*)([packets bytes]));
            //NSLog(@" number of packets %i",packetsNumber);
            
            int startingPosition = 2;
            //            if([instrumentTokensDict allKeys])
            //                [instrumentTokensDict removeAllObjects];
            
            for( int i =0; i< packetsNumber ; i++)
            {
                
                
                NSData * packetsLengthData = [message subdataWithRange:NSMakeRange(startingPosition,2)];
                packetsLength = CFSwapInt16BigToHost(*(int16_t*)([packetsLengthData bytes]));
                startingPosition = startingPosition + 2;
                
                id packetQuote = [message subdataWithRange:NSMakeRange(startingPosition,packetsLength)];
                
                
                
                
                id instrumentTokenData = [packetQuote subdataWithRange:NSMakeRange(0, 4)];
                int32_t instrumentTokenValue = CFSwapInt32BigToHost(*(int32_t*)([instrumentTokenData bytes]));
                
                id lastPrice = [packetQuote subdataWithRange:NSMakeRange(4,4)];
                int32_t lastPriceValue = CFSwapInt32BigToHost(*(int32_t*)([lastPrice bytes]));
                
                
                
                
                
                
                id closingPrice = [packetQuote subdataWithRange:NSMakeRange(40,4)];
                int32_t closePriceValue = CFSwapInt32BigToHost(*(int32_t*)([closingPrice bytes]));
                
                
                
                
                
                
                //                id marketDepth = [packetQuote subdataWithRange:NSMakeRange(44,120)];
                //                int32_t marketDepthValue = CFSwapInt32BigToHost(*(int32_t*)([marketDepth bytes]));
                //
                //                //NSLog(@"%@",marketDepth);
                
                //                NSString * exchange=[NSString stringWithFormat:@"%@",[[delegate2.filteredCompanyName objectAtIndex:i]valueForKey:@"segment"]];
                //
                //                if([exchange containsString:@"CDS"])
                //                {
                //                    lastPriceFlaotValue = (float)lastPriceValue/10000000;
                //
                //                    closePriceFlaotValue = (float)closePriceValue/10000000;
                //
                //                    //NSLog(@"CLOSE %f",closePriceFlaotValue);
                //
                //                    changeFloatValue = lastPriceFlaotValue-closePriceFlaotValue;
                //
                //                    changeFloatPercentageValue = (changeFloatValue/closePriceFlaotValue)/100;
                //                }
                //                else
                //                {
                
                
                lastPriceFlaotValue = (float)lastPriceValue/100;
                
                closePriceFlaotValue = (float)closePriceValue/100;
                
                //NSLog(@"CLOSE %f",closePriceFlaotValue);
                
                changeFloatValue = lastPriceFlaotValue-closePriceFlaotValue;
                
                changeFloatPercentageValue = ((lastPriceFlaotValue-closePriceFlaotValue) *100/closePriceFlaotValue);
                //                }
                
                startingPosition = startingPosition + packetsLength;
                
              
                @autoreleasepool {
                    
                    tmpDict = [[NSMutableDictionary alloc]init];
                    [tmpDict setValue:[NSString stringWithFormat:@"%d",instrumentTokenValue] forKey:@"InstrumentToken"];
                    
                    NSString *tmpInstrument = [NSString stringWithFormat:@"%d",instrumentTokenValue];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%.2f",closePriceFlaotValue] forKey:@"ClosePriceValue"];
                    
                    
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%.2f",lastPriceFlaotValue] forKey:@"LastPriceValue"];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%.2f",changeFloatValue] forKey:@"ChangeValue"];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%.2f",changeFloatPercentageValue] forKey:@"ChangePercentageValue"];
                    //NSLog(@"%@",tmpDict);
                    if(segmentedControl.selectedSegmentIndex==0)
                    {
                        
                        [instrumentTokensDict setValue:tmpDict forKeyPath:tmpInstrument];
                        
                        //NSLog(@"%@",instrumentTokensDict);
                        
                        
                    }
                    else
                    {
                        
                        [instrumentTokensDictCustom setValue:tmpDict forKeyPath:tmpInstrument];
                        
                      
                        
                        
                    }
                 
                    
                    
                    
                    //  [shareListArray addObject:tmpDict];
                    
                }
                
                
            }
            
        
            if(segmentedControl.selectedSegmentIndex==0)
            {
            if(allKeysList.count>0)
            {
                [allKeysList removeAllObjects];
            }
            allKeysList = [[instrumentTokensDict allKeys]mutableCopy];
            
            //NSLog(@"%@",allKeysList);
            
            self.allKeysList1=[[NSMutableArray alloc]init];
            
            for(int i=0;i<localInstrumentToken.count;i++)
            {
                
                for(int j=0;j<allKeysList.count;j++)
                {
                    NSNumber * number1=[localInstrumentToken objectAtIndex:i];
                    int int1=[number1 intValue];
                    NSNumber * number2=[allKeysList objectAtIndex:j];
                    
                    int int2=[number2 intValue];
                    
                    if(int1==int2)
                    {
                        [ self.allKeysList1 addObject:[allKeysList objectAtIndex:j]];
                    }
                }
                
                
            }
            
             self.allKeysList1=[[[ self.allKeysList1 reverseObjectEnumerator] allObjects] mutableCopy];
            
            dispatch_async(dispatch_get_main_queue(), ^{
//                self.marketTableView.delegate=self;
//                self.marketTableView.dataSource=self;
                
                [self.marketTableView reloadData];
                
                //            //NSLog(@"%@",shareListArray);
                
                
            });
            
            
            //  [self.activityIndicator performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:YES];
            
            [self.activityIndicator stopAnimating];
            self.activityIndicator.hidden=YES;
            
            }
            else
            {
                if(allKeysListCustom.count>0)
                {
                    [allKeysListCustom removeAllObjects];
                }
                allKeysListCustom = [[instrumentTokensDictCustom allKeys]mutableCopy];
                
              
                
                self.allKeysList1Custom=[[NSMutableArray alloc]init];
                
                for(int i=0;i<localInstrumentToken.count;i++)
                {
                    
                    for(int j=0;j<allKeysListCustom.count;j++)
                    {
                        NSNumber * number1=[localInstrumentToken objectAtIndex:i];
                        int int1=[number1 intValue];
                        NSNumber * number2=[allKeysListCustom objectAtIndex:j];
                        
                        int int2=[number2 intValue];
                        
                        if(int1==int2)
                        {
                            [ self.allKeysList1Custom addObject:[allKeysListCustom objectAtIndex:j]];
                        }
                    }
                    
                    
                }
                
                self.allKeysList1Custom=[[[ self.allKeysList1Custom reverseObjectEnumerator] allObjects] mutableCopy];
                
                dispatch_async(dispatch_get_main_queue(), ^{
//                    self.customPortfolioTblView.delegate=self;
//                    self.customPortfolioTblView.dataSource=self;
                    
                    [self.customPortfolioTblView reloadData];
//                    NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:index inSection:0];
//
//                    // Add them in an index path array
//                    NSArray* indexArray = [NSArray arrayWithObjects:indexPath1,nil];
//                    // Launch reload for the two index path
//                    [self.customPortfolioTblView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
                    
                    //            //NSLog(@"%@",shareListArray);
                    
                    
                });
                
                
                //  [self.activityIndicator performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:YES];
                
                [self.activityIndicator stopAnimating];
                self.activityIndicator.hidden=YES;
            }
            
            
        }
        if(segmentedControl.selectedSegmentIndex==0)
        {
        if( self.allKeysList1.count>0)
        {
            self.ifEmptyView.hidden=YES;
            self.marketTableView.hidden=NO;
        }else
        {
            
            [self.activityIndicator stopAnimating];
            self.activityIndicator.hidden=YES;
      
        }
        }
            else
            {
                if( self.allKeysList1Custom.count>0)
                {
                    self.ifEmptyView.hidden=YES;
                    self.customPortfolioView.hidden=NO;
                }else
                {
                    
                    [self.activityIndicator stopAnimating];
                    self.activityIndicator.hidden=YES;
                    
                }
            }
            
        }
        
        else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
        
        {
           
            NSString *detailsString = [[NSString alloc] initWithData:message encoding:NSASCIIStringEncoding];
            
            
            //NSLog(@"%@",detailsString);
            
            NSArray * completeArray=[[NSArray alloc]init];
            
           completeArray= [detailsString componentsSeparatedByString:@";"] ;
          
            
            NSMutableDictionary * tmpDict=[[NSMutableDictionary alloc]init];
            
            for (int i=0; i<completeArray.count; i++) {
            
                NSMutableArray * individualCompanyArray=[[NSMutableArray alloc]init];
                NSString * innerStr=[NSString stringWithFormat:@"%@",[completeArray objectAtIndex:i]];
                
                individualCompanyArray=[[innerStr componentsSeparatedByString:@","] mutableCopy];
//            for(int i=0;i<localInstrumentToken.count;i++)
//            {
            
           
                 NSString * instrument;
                
                if(segmentedControl.selectedSegmentIndex==0)
                {
                
                for(int i=0;i<delegate2.homeNewCompanyArray.count;i++)
                {
                    NSString * zambalaSymbol;
                    NSString * zambalaExchange;
                    
                   
                  
                    NSString * upStoxSymbol=[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:2]];
                    
                     NSString * upStoxExchange=[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:1]];
                    
                    if([upStoxExchange containsString:@"NSE_FO"])
                    {
                        upStoxExchange =@"NFO";
                        
                    }
                   else  if([upStoxExchange containsString:@"BSE_FO"])
                    {
                        
                         upStoxExchange =@"BFO";
                    }
                    
                   else  if([upStoxExchange containsString:@"NCD_FO"])
                   {
                       
                       upStoxExchange =@"CDS";
                   }
                    
                   else  if([upStoxExchange containsString:@"MCX_FO"])
                   {
                       
                       upStoxExchange =@"MCX";
                   }
                    
                   else  if([upStoxExchange containsString:@"NSE_EQ"])
                   {
                       
                       upStoxExchange =@"NSE";
                   }
                    
                   else  if([upStoxExchange containsString:@"BSE_EQ"])
                   {
                       
                       upStoxExchange =@"BSE";
                   }
                  
                    @try
                    {
                        
                        NSString * exchange=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"segment"]];
                        
                        if([exchange containsString:@"FUT"]||[exchange containsString:@"OPT"]||[exchange containsString:@"MCX"])
                        {
                             zambalaSymbol=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"securitydescupstox"]];
                            
                            zambalaExchange=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"exchange"]];
                            
                        }
                        
                        else
                        {
                            
                            zambalaSymbol=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"symbol"]];
                            
                            zambalaExchange=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"exchange"]];
                            
                        }
                       
                        
                    }
                  @catch (NSException *exception) {
                    //NSLog(@"Something missing...");
                      
                      
                   }
                  @finally {
                    
                   }
                    
                    instrument=@"";
                    if([upStoxSymbol isEqualToString:zambalaSymbol]&&[upStoxExchange containsString:zambalaExchange])
                    {
                        
                        instrument=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"instrument_token"]];
                        
                       
                    }
                    if(instrument.length!=0)
                    
                    {
                        tmpDict = [[NSMutableDictionary alloc]init];
                        [tmpDict setValue:[NSString stringWithFormat:@"%@",instrument] forKey:@"InstrumentToken"];
                        
                        NSString *tmpInstrument = [NSString stringWithFormat:@"%@",instrument];
                        
                        float close=[[individualCompanyArray objectAtIndex:4] floatValue];
                        float ltp=[[individualCompanyArray objectAtIndex:3] floatValue];
                        
                        [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:4]] forKey:@"ClosePriceValue"];
                        
                        
                        
                        [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:3]] forKey:@"LastPriceValue"];
                        
                        
                        float change=ltp-close;
                        
                        
                        [tmpDict setValue:[NSString stringWithFormat:@"%.2f",change] forKey:@"ChangeValue"];
                        
                        float changePer = ((ltp-close) *100/close);
                        
                        [tmpDict setValue:[NSString stringWithFormat:@"%.2f",changePer] forKey:@"ChangePercentageValue"];
                        //NSLog(@"%@",tmpDict);
                        if(segmentedControl.selectedSegmentIndex==0)
                        {
                        
                        [instrumentTokensDict setValue:tmpDict forKeyPath:tmpInstrument];
                        
                        //NSLog(@"%@",instrumentTokensDict);
                        
                        }
                        else
                        {
                               [instrumentTokensDict setValue:tmpDict forKeyPath:tmpInstrument];
                        }
                        
                        
                        
                        //  [shareListArray addObject:tmpDict];
                        
                        
//                          reloadIndex=[[instrumentTokensDict allKeys] indexOfObject:tmpInstrument];
//                          reloadIndexPath= [NSIndexPath indexPathForRow:reloadIndex inSection:0];
//
//                        // Add them in an index path array
//                          reloadIndexArray = [NSArray arrayWithObjects:reloadIndexPath,nil];
                    
                        
                    }
                  
                    
                }
                
                
                }
                else if(segmentedControl.selectedSegmentIndex==1)
                {
                    
                    for(int i=0;i<delegate2.homeNewCompanyArrayCustom.count;i++)
                    {
                        NSString * zambalaSymbol;
                        NSString * zambalaExchange;
                        
                        
                        
                        NSString * upStoxSymbol=[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:2]];
                        
                        NSString * upStoxExchange=[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:1]];
                        
                        if([upStoxExchange containsString:@"NSE_FO"])
                        {
                            upStoxExchange =@"NFO";
                            
                        }
                        else  if([upStoxExchange containsString:@"BSE_FO"])
                        {
                            
                            upStoxExchange =@"BFO";
                        }
                        
                        else  if([upStoxExchange containsString:@"NCD_FO"])
                        {
                            
                            upStoxExchange =@"CDS";
                        }
                        
                        else  if([upStoxExchange containsString:@"MCX_FO"])
                        {
                            
                            upStoxExchange =@"MCX";
                        }
                        
                        else  if([upStoxExchange containsString:@"NSE_EQ"])
                        {
                            
                            upStoxExchange =@"NSE";
                        }
                        
                        else  if([upStoxExchange containsString:@"BSE_EQ"])
                        {
                            
                            upStoxExchange =@"BSE";
                        }
                        
                        @try
                        {
                            
                            NSString * exchange=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]objectForKey:@"segment"]];
                            
                            if([exchange containsString:@"FUT"]||[exchange containsString:@"OPT"]||[exchange containsString:@"MCX"])
                            {
                                zambalaSymbol=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]objectForKey:@"securitydesc"]];
                                
                                zambalaExchange=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]objectForKey:@"exchange"]];
                                
                            }
                            
                            else
                            {
                                
                                zambalaSymbol=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]objectForKey:@"symbol"]];
                                
                                zambalaExchange=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]objectForKey:@"exchange"]];
                                
                            }
                            
                            
                        }
                        @catch (NSException *exception) {
                            //NSLog(@"Something missing...");
                            
                            
                        }
                        @finally {
                            
                        }
                        
                        instrument=@"";
                        if([upStoxSymbol isEqualToString:zambalaSymbol]&&[upStoxExchange containsString:zambalaExchange])
                        {
                            
                            instrument=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]objectForKey:@"instrument_token"]];
                            
                            
                        }
                        if(instrument.length!=0)
                            
                        {
                            tmpDict = [[NSMutableDictionary alloc]init];
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",instrument] forKey:@"InstrumentToken"];
                            
                            NSString *tmpInstrument = [NSString stringWithFormat:@"%@",instrument];
                            
                            float close=[[individualCompanyArray objectAtIndex:4] floatValue];
                            float ltp=[[individualCompanyArray objectAtIndex:3] floatValue];
                            
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:4]] forKey:@"ClosePriceValue"];
                            
                            
                            
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:3]] forKey:@"LastPriceValue"];
                            
                            
                            float change=ltp-close;
                            
                            
                            [tmpDict setValue:[NSString stringWithFormat:@"%.2f",change] forKey:@"ChangeValue"];
                            
                            float changePer = ((ltp-close) *100/close);
                            
                            [tmpDict setValue:[NSString stringWithFormat:@"%.2f",changePer] forKey:@"ChangePercentageValue"];
                            //NSLog(@"%@",tmpDict);
                            if(segmentedControl.selectedSegmentIndex==0)
                            {
                                
                                [instrumentTokensDict setValue:tmpDict forKeyPath:tmpInstrument];
                                
                                //NSLog(@"%@",instrumentTokensDict);
                                
                            }
                            else
                            {
                                [instrumentTokensDictCustom setValue:tmpDict forKeyPath:tmpInstrument];
                            }
                            
                            
                            
                            //  [shareListArray addObject:tmpDict];
                            
//                            reloadIndex=[[instrumentTokensDictCustom allKeys] indexOfObject:tmpInstrument];
//                            reloadIndexPath= [NSIndexPath indexPathForRow:reloadIndex inSection:0];
//
//                            // Add them in an index path array
//                            reloadIndexArray = [NSArray arrayWithObjects:reloadIndexPath,nil];
//
                            
                            
                        }
                        
                        
                    }
                    
                    
                }
               
                
                

                
    }
            if(segmentedControl.selectedSegmentIndex==0)
            {
            if(allKeysList.count>0)
            {
                [allKeysList removeAllObjects];
            }
            allKeysList = [[instrumentTokensDict allKeys]mutableCopy];
            
            //NSLog(@"%@",allKeysList);
            
            self.allKeysList1=[[NSMutableArray alloc]init];
            
            for(int i=0;i<localInstrumentToken.count;i++)
            {
                
                for(int j=0;j<allKeysList.count;j++)
                {
                    NSNumber * number1=[localInstrumentToken objectAtIndex:i];
                    int int1=[number1 intValue];
                    NSNumber * number2=[allKeysList objectAtIndex:j];
                    
                    int int2=[number2 intValue];
                    
                    if(int1==int2)
                    {
                        [ self.allKeysList1 addObject:[allKeysList objectAtIndex:j]];
                        
                    }
                }
                
            }
            
//            self.allKeysList1=[[[ self.allKeysList1 reverseObjectEnumerator] allObjects] mutableCopy];
            
            dispatch_async(dispatch_get_main_queue(), ^{
//                self.marketTableView.delegate=self;
//                self.marketTableView.dataSource=self;
               
                [self.marketTableView reloadData];
               
                // Launch reload for the two index path
//                [self.marketTableView reloadRowsAtIndexPaths:reloadIndexArray withRowAnimation:UITableViewRowAnimationNone];
                
                //            //NSLog(@"%@",shareListArray);
            });
            
            
            //  [self.activityIndicator performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:YES];
            
            [self.activityIndicator stopAnimating];
            self.activityIndicator.hidden=YES;
            
            
            
            
            
            
            if( self.allKeysList1.count>0)
            {
                self.ifEmptyView.hidden=YES;
                self.marketTableView.hidden=NO;
            }else
            {
                
                [self.activityIndicator stopAnimating];
                self.activityIndicator.hidden=YES;
                
              
                
            }
            
        }
            
            else
            {
                if(allKeysListCustom.count>0)
                {
                    [allKeysListCustom removeAllObjects];
                }
                allKeysListCustom = [[instrumentTokensDictCustom allKeys]mutableCopy];
            
                
                self.allKeysList1Custom=[[NSMutableArray alloc]init];
                
                for(int i=0;i<localInstrumentToken.count;i++)
                {
                    
                    for(int j=0;j<allKeysListCustom.count;j++)
                    {
                        NSNumber * number1=[localInstrumentToken objectAtIndex:i];
                        int int1=[number1 intValue];
                        NSNumber * number2=[allKeysListCustom objectAtIndex:j];
                        
                        int int2=[number2 intValue];
                        
                        if(int1==int2)
                        {
                            [ self.allKeysList1Custom addObject:[allKeysListCustom objectAtIndex:j]];
                            
                        }
                    }
                    
                }
                
                //            self.allKeysList1=[[[ self.allKeysList1 reverseObjectEnumerator] allObjects] mutableCopy];
                
                dispatch_async(dispatch_get_main_queue(), ^{
//                    self.customPortfolioTblView.delegate=self;
//                    self.customPortfolioTblView.dataSource=self;
                    [self.customPortfolioTblView reloadData];
//                    [self.customPortfolioTblView reloadRowsAtIndexPaths:reloadIndexArray withRowAnimation:UITableViewRowAnimationNone];
                    
                    //            //NSLog(@"%@",shareListArray);
                });
                
                
                //  [self.activityIndicator performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:YES];
                
                [self.activityIndicator stopAnimating];
                self.activityIndicator.hidden=YES;
                
                
                
                
                
                
                if( self.allKeysList1Custom.count>0)
                {
                    self.ifEmptyView.hidden=YES;
                    self.customPortfolioView.hidden=NO;
                }else
                {
                    
                    [self.activityIndicator stopAnimating];
                    self.activityIndicator.hidden=YES;
                    
                    
                    
                }
                
            }
            
        }
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    

        
     
    }
    



    


- (void)webSocket:(PSWebSocket *)webSocket didFailWithError:(NSError *)error {
    //NSLog(@"The websocket handshake/connection failed with an error: %@", error);
}
- (void)webSocket:(PSWebSocket *)webSocket didReceiveMessageWithData:(NSData *)data
{
     //NSLog(@"The websocket received a message: %@", data);
}
- (void)webSocket:(PSWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    //NSLog(@"The websocket closed with code: %@, reason: %@, wasClean: %@", @(code), reason, (wasClean) ? @"YES" : @"NO");
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

      // TableView Delegate and  Data Sources//

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   // return [filteredArray count];
//   if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
//   {
    if(tableView==self.customPortfolioTblView)
    {
        if([delegate2.localExchageTokenCustom count]>0)
        {
         return  [delegate2.localExchageTokenCustom count];
        }
        else
        
        {
               dispatch_async(dispatch_get_main_queue(), ^{
            self.noWatchLbl.text=@"Your Portfolio Watch list is empty";
            self.noWatchDesLbl.text=@"Now Create your dream portfolio of your favorite stocks or simply track your stocks kept in different Demat/Broking Accounts";
            self.ifEmptyView.hidden=NO;
            self.marketTableView.hidden=YES;
            self.customPortfolioView.hidden=YES;
               });
        }
    }
    else  if(tableView==self.marketTableView)
    {
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
    {
    if( self.allKeysList1.count>0)
    {
    return [ self.allKeysList1 count];
    }
        else
            
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.noWatchLbl.text=@"Your Market Watch list is empty";
                self.noWatchDesLbl.text=@"Add symbols to your Market Watch list by clicking on the ‘+’ icon on top and stay up to date on market trends.";
            });
        }
        
    }else if ([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
        if(delegate2.homeInstrumentToken.count>0)
        {
        return delegate2.homeInstrumentToken.count;
        }
        else
            
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.noWatchLbl.text=@"Your Market Watch list is empty";
                self.noWatchDesLbl.text=@"Add symbols to your Market Watch list by clicking on the ‘+’ icon on top and stay up to date on market trends.";
            });
        }
    }
    
    else
    {
        if(delegate2.localExchageToken.count>0)
        {
        return  [delegate2.localExchageToken count];
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.noWatchLbl.text=@"Your Market Watch list is empty";
                self.noWatchDesLbl.text=@"Add symbols to your Market Watch list by clicking on the ‘+’ icon on top and stay up to date on market trends.";
            });
        }
    }
        
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    @try
    {
    if(tableView==self.customPortfolioTblView)
    {
        
          DematCell * cell = [self.customPortfolioTblView dequeueReusableCellWithIdentifier:@"CELL1"forIndexPath:indexPath];
        NSMutableDictionary *tmp = [[NSMutableDictionary alloc]initWithDictionary:[instrumentTokensDictCustom valueForKey:[self.allKeysList1Custom objectAtIndex:indexPath.row]]];
        if(customLtpArray.count>0)
        {
            [customLtpArray removeAllObjects];
        }
        if(customInvestmentArray.count>0)
        {
            [customInvestmentArray removeAllObjects];
        }
        for(int i=0;i<self.allKeysList1Custom.count;i++)
        {
            NSMutableDictionary *tmpLocal = [[NSMutableDictionary alloc]initWithDictionary:[instrumentTokensDictCustom valueForKey:[self.allKeysList1Custom objectAtIndex:i]]];
           
            for (int i=0 ;i < delegate2.homeNewCompanyArrayCustom.count ; i++)
            {
                NSString *str_ServerToken ;
                NSString *str_KiteToken;
                if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
                {
                     str_ServerToken = [NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]valueForKey:@"instrument_token"]];
                    str_KiteToken = [NSString stringWithFormat:@"%@",[tmpLocal valueForKey:@"InstrumentToken"]];
                }
                else
                {
                    str_ServerToken = [NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]valueForKey:@"exchange_token"]];
                    str_KiteToken = [NSString stringWithFormat:@"%@",[tmpLocal valueForKey:@"stSecurityID"]];
                }
                
                if([str_ServerToken isEqualToString:str_KiteToken])
                {
                    float value;
                    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
                    {
                    value=[[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]valueForKey:@"quantity"]intValue] * [[tmpLocal objectForKey:@"LastPriceValue"]floatValue];
                    }
                    else
                    {
                        value=[[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]valueForKey:@"quantity"]intValue] * [[tmpLocal objectForKey:@"dbLTP"]floatValue];
                    }
                     NSString * str=[NSString stringWithFormat:@"%.2f",value];
                     [customLtpArray addObject:str];
                    
                    float value1=[[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]valueForKey:@"quantity"]intValue] * [[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]valueForKey:@"averageprice"]floatValue];
                    NSString * str1=[NSString stringWithFormat:@"%.2f",value1];
                    [customInvestmentArray addObject:str1];
                }
            }
        }
        
        float sum = 0;
        for (NSNumber *num in customLtpArray) {
            sum += [num floatValue];
        }
        float sum1 = 0;
        for (NSNumber *num1 in customInvestmentArray) {
            sum1 += [num1 floatValue];
        }
        
        self.investmentValueLbl.text=[NSString stringWithFormat:@"%.2f",sum1];
        self.currentValueLbl.text=[NSString stringWithFormat:@"%.2f",sum];
        NSString * profitLossStr=[NSString stringWithFormat:@"%.2f",sum-sum1];
        self.profitLossLbl.text=profitLossStr;
        dispatch_async(dispatch_get_main_queue(), ^{
        if([profitLossStr containsString:@"-"])
        {
             self.customPortfolioBottomView.backgroundColor =[UIColor colorWithRed:(178.0/255.0) green:(50.0/255.0) blue:(50.0/255.0) alpha:1];
        }
        else
        {
             self.customPortfolioBottomView.backgroundColor =[UIColor colorWithRed:(53.0/255.0) green:(165.0/255.0) blue:(66.0/255.0) alpha:1];
            
        }
              });
        for (int i=0 ;i < delegate2.homeNewCompanyArrayCustom.count ; i++)
        {
            NSString *str_ServerToken ;
            NSString *str_KiteToken;
            if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
            {
                str_ServerToken = [NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]valueForKey:@"instrument_token"]];
                str_KiteToken = [NSString stringWithFormat:@"%@",[tmp valueForKey:@"InstrumentToken"]];
            }
            else
            {
                str_ServerToken = [NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]valueForKey:@"exchange_token"]];
                str_KiteToken = [NSString stringWithFormat:@"%@",[tmp valueForKey:@"stSecurityID"]];
            }
            
            if([str_ServerToken isEqualToString:str_KiteToken])
            {
                //NSLog(@"%@",tmp);
                cell.companyLabel.text= [NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]valueForKey:@"symbol"]];
                cell.QTYLabel.text=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]valueForKey:@"quantity"]];
                cell.avgPriceLbl.text=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]valueForKey:@"averageprice"]];
                 cell.exchaneLbl.text=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]valueForKey:@"exchange"]];
                
                if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
                {
                cell.LPTLabel.text=[NSString stringWithFormat:@"%.2f",[[tmp objectForKey:@"LastPriceValue"]floatValue]];
                float value=[[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]valueForKey:@"quantity"]intValue] * [[tmp objectForKey:@"LastPriceValue"]floatValue];
                NSString * valueStr=[NSString stringWithFormat:@"%.2f",value];
                cell.valueLabel.text=valueStr;
                }
                else
                {
                    cell.LPTLabel.text=[NSString stringWithFormat:@"%.2f",[[tmp objectForKey:@"dbLTP"]floatValue]];
                    float value=[[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]valueForKey:@"quantity"]intValue] * [[tmp objectForKey:@"dbLTP"]floatValue];
                    NSString * valueStr=[NSString stringWithFormat:@"%.2f",value];
                    cell.valueLabel.text=valueStr;
                }
               
                 [cell.modifyBtn addTarget:self action:@selector(modifyAction:) forControlEvents:UIControlEventTouchUpInside];
               
                
            }
            
        }
        
        return cell;
        
    }
    else  if(tableView==self.marketTableView)
    {
    
     MarketTableViewCell * cell = [self.marketTableView dequeueReusableCellWithIdentifier:@"Cell"forIndexPath:indexPath];
    
    
    NSString * str=@"%";
    
    @try {
        
        
        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
                {
        //        if( self.allKeysList1.count>0)
        //        {
                    
                    
                    NSMutableDictionary *tmp = [[NSMutableDictionary alloc]initWithDictionary:[instrumentTokensDict valueForKey:[self.allKeysList1 objectAtIndex:indexPath.row]]];
                    
                    for (int i=0 ;i < delegate2.homeNewCompanyArray.count ; i++)
                    {
                        
                        NSString *str_ServerToken = [NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"instrument_token"]];
                        NSString *str_KiteToken = [NSString stringWithFormat:@"%@",[tmp valueForKey:@"InstrumentToken"]];
                        
                        cell.resultsTodayLabel.hidden=YES;
                        
                        int result =[[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"result"] intValue];
                        if(result==0)
                        {
                            cell.resultsTodayLabel.hidden=YES;
                            cell.resultsTodayLabel.text=@"";
                        }else if(result==1)
                        {
                            cell.resultsTodayLabel.hidden=NO;
                            cell.resultsTodayLabel.text=@"Results Today";
                        }
                        
                        //NSLog(@"%@",delegate2.homeNewCompanyArray);
                        
                        if([str_ServerToken isEqualToString:str_KiteToken])
                        {
                            cell.companyName.text= [NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"symbol"]];
                            @try {
                                option=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"optiontype"]];
                                
                                NSString *myString = [[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"expiryseries"];
                                NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                                dateFormatter.dateFormat = @"yyyy-MM-dd";
                                NSDate *yourDate = [dateFormatter dateFromString:myString];
                                dateFormatter.dateFormat = @"dd MMM yyyy";
                                //NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
                                
                                finalDate=[[dateFormatter stringFromDate:yourDate] uppercaseString];
                            }
                            @catch (NSException *exception) {
                                //NSLog(@"Something missing...");
                            }
                            @finally {
                                
                            }
                            
                            
                            
                            if([option isEqualToString:@"FUT"])
                            {
                                
                                
                                cell.exchangeLbl.text= [NSString stringWithFormat:@"%@ %@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"segment"],finalDate];
                                
                            }
                            else if([option isEqualToString:@"CE"])
                            {
                                
                                cell.exchangeLbl.text= [NSString stringWithFormat:@"%@ %@ CE  %@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"segment"],[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"strikeprice"],finalDate];
                            }
                            
                            else if([option isEqualToString:@"PE"])
                            {
                                
                                cell.exchangeLbl.text= [NSString stringWithFormat:@"%@ %@ PE  %@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"segment"],[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"strikeprice"],finalDate];
                            }
                            
                            else
                            {
                                cell.exchangeLbl.text= [NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"segment"]];
                            }
                            
                            
                            //check//
                            
                            
                            
                                NSString * priceString=[tmp valueForKey:@"LastPriceValue"];
                            
                            
                            
                            NSString * changeStrPer=[tmp valueForKey:@"ChangePercentageValue"];
                            
                            
                           // cell.priceLbl.text=priceString;
                            
                            if(priceString.length>0)
                            {
                                
                                //    NSNumberFormatter *formatString = [[NSNumberFormatter alloc] init];
                                //    NSString * previousValue = cell.priceLbl.text;
                                //    NSNumber *number2 = [formatString numberFromString:previousValue];
                                
                                
                                
                                CGFloat number2 = (CGFloat)[cell.priceLbl.text floatValue];
                                //NSLog(@"number2 %f",number2);
                                
                                    if([cell.exchangeLbl.text containsString:@"CDS"])
                                    {
                                        
                                        
                                        NSString * str=[tmp valueForKey:@"LastPriceValue"];
                                        
                                    float priceValue=[str floatValue];
                                        
                                        float priceValue1;
                                        
                                        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                                        {
                                            
                                             priceValue1= (float) priceValue/100000;
                                        }
                                        
                                        else  if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
                                        {
                                            
                                            priceValue1=priceValue;
                                        }
                                    
                                   
                                    
                                    NSString * priceStr=[NSString stringWithFormat:@"%.4f",priceValue1];
                                    
                                    CGFloat number1=(CGFloat)[priceStr floatValue];
                                    
                                    //NSLog(@"number1 %f",number1);
                                    
                                    if(number1<number2)
                                    {
                                        //            cell.priceLbl.backgroundColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:0.34];
                                        
                                        cell.priceLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                                        cell.priceLbl.text=priceStr;
                                    }
                                    
                                    else if(number1>number2)
                                    {
                                        //            cell.priceLbl.backgroundColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:0.34];
                                        
                                        cell.priceLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                                        cell.priceLbl.text=priceStr;
                                    }
                                        
                                    }
                                
                                
                                
                                
                                
                                else
                                {
                                    float price1=[[tmp valueForKey:@"LastPriceValue"] floatValue];
                                    NSString * price=[NSString stringWithFormat:@"%.2f",price1];
                                    CGFloat number1=(CGFloat)[price floatValue];
                                    
                                    //NSLog(@"number1 %f",number1);
                                    
                                    if(number1<number2)
                                    {
                                        //            cell.priceLbl.backgroundColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:0.34];
                                        
                                        cell.priceLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                                        cell.priceLbl.text=price;
                                    }
                                    
                                    else if(number1>number2)
                                    {
                                        //            cell.priceLbl.backgroundColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:0.34];
                                        
                                        cell.priceLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                                        cell.priceLbl.text=price;
                                    }
                                    
                                }
                                
                                
                                
                                
                                
                                
                            }
                            
                            
                            
                            //        if(priceString.length>7)
                            //        {
                            //            NSString * str=[tmp valueForKey:@"LastPriceValue"];
                            //            float priceValue=[str floatValue];
                            //
                            //            float priceValue1= (float) priceValue/100000;
                            //
                            //            NSString * priceStr=[NSString stringWithFormat:@"%.2f",priceValue1];
                            //
                            //            cell.priceLbl.text=priceStr;
                            //        }
                            //
                            //        else
                            //        {
                            //            cell.priceLbl.text =[tmp valueForKey:@"LastPriceValue"];
                            //        }
                            //
                            
                            
                            NSString * changeStr=[tmp valueForKey:@"ChangeValue"];
                            
                            if([cell.exchangeLbl.text containsString:@"CDS"])
                            {
                                
                                
                                if([changeStrPer containsString:@"-"])
                                {
                                    cell.changePerLbl.text = [NSString stringWithFormat:@"%@%@",[tmp valueForKey:@"ChangePercentageValue"],str];
                                    cell.changePerLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                                }
                                else{
                                    cell.changePerLbl.text = [NSString stringWithFormat:@"%@%@",[tmp valueForKey:@"ChangePercentageValue"],str];
                                    cell.changePerLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                                }
                                
                                
                                
                                
                                NSString * str=[tmp valueForKey:@"ChangeValue"];
                                float priceValue=[str floatValue];
                                
                                float priceValue1;
                                
                                if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                                {
                                      priceValue1= (float) priceValue/100000;
                                    
                                }
                                
                                else
                                {
                                    
                                    priceValue1=priceValue;
                                }
                                
                               
                                
                                NSString * priceStr=[NSString stringWithFormat:@"%.2f",priceValue1];
                                
                                
                                
                                cell.percentLbl.text = priceStr;
                                
                                
                            }
                            
                            else
                            {
                                
                                if([changeStr containsString:@"-"])
                                {
                                    cell.percentLbl.text = [tmp valueForKey:@"ChangeValue"];
                                    cell.percentLbl.textColor=[UIColor colorWithRed:(6/255.0) green:(31/255.0) blue:(43/255.0) alpha:1];
                                }
                                else{
                                    cell.percentLbl.text = [tmp valueForKey:@"ChangeValue"];
                                    cell.percentLbl.textColor=[UIColor colorWithRed:(6/255.0) green:(31/255.0) blue:(43/255.0) alpha:1];
                                }
                                
                                if([changeStrPer containsString:@"-"])
                                {
                                    cell.changePerLbl.text = [NSString stringWithFormat:@"%@%@",[tmp valueForKey:@"ChangePercentageValue"],str];
                                    cell.changePerLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                                }
                                else{
                                    cell.changePerLbl.text = [NSString stringWithFormat:@"%@%@",[tmp valueForKey:@"ChangePercentageValue"],str];
                                    cell.changePerLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                                }
                                
                            }
                            
                            
                            
                            
                            
                            //

                            
                            
                            
                            
                            break;
                        }
                        
                    }
                    
                    //        UISwipeGestureRecognizer* swipeGestureLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(cellSwipedLeft:)];
                    //        [swipeGestureLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
                    //        [cell addGestureRecognizer:swipeGestureLeft];
                    //
                    //
                    //        UISwipeGestureRecognizer* swipeGestureRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(cellSwipedRight:)];
                    //        [swipeGestureRight setDirection:UISwipeGestureRecognizerDirectionRight];
                    //        [cell addGestureRecognizer:swipeGestureRight];
                    
                    [cell.tradeBtn addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
                    
                 
                    
               }
        
        else
        {
//        if( self.allKeysList1.count>0)
//        {
            //
            
            NSMutableDictionary *tmp = [[NSMutableDictionary alloc]initWithDictionary:[instrumentTokensDict valueForKey:[ self.allKeysList1 objectAtIndex:indexPath.row]]];
            
            for (int i=0 ;i < delegate2.homeNewCompanyArray.count ; i++)
            {
                
                NSString *str_ServerToken = [NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"exchange_token"]];
                NSString *str_KiteToken = [NSString stringWithFormat:@"%@",[tmp valueForKey:@"stSecurityID"]];
                cell.resultsTodayLabel.hidden=YES;
                
                int result =[[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"result"] intValue];
                if(result==0)
                {
                    cell.resultsTodayLabel.hidden=YES;
                    cell.resultsTodayLabel.text=@"";
                }else if(result==1)
                {
                    cell.resultsTodayLabel.hidden=NO;
                    cell.resultsTodayLabel.text=@"Results Today";
                }
                //NSLog(@"%@",delegate2.homeNewCompanyArray);
                
                if([str_ServerToken isEqualToString:str_KiteToken ])
                {
                    cell.companyName.text= [NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"symbol"]];
                    @try {
                        option=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"optiontype"]];
                        
                        NSString *myString = [[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"expiryseries"];
                        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                        dateFormatter.dateFormat = @"yyyy-MM-dd";
                        NSDate *yourDate = [dateFormatter dateFromString:myString];
                        dateFormatter.dateFormat = @"dd MMM yyyy";
                        //NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
                        
                        finalDate=[[dateFormatter stringFromDate:yourDate] uppercaseString];
                    }
                    @catch (NSException *exception) {
                        //NSLog(@"Something missing...");
                    }
                    @finally {
                        
                    }
                    
                    
                    
                    if([option isEqualToString:@"FUT"])
                    {
                        
                        
                        cell.exchangeLbl.text= [NSString stringWithFormat:@"%@ %@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"segment"],finalDate];
                        
                    }
                    else if([option isEqualToString:@"CE"])
                    {
                        
                        cell.exchangeLbl.text= [NSString stringWithFormat:@"%@ %@ CE  %@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"segment"],[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"strikeprice"],finalDate];
                    }
                    
                    else if([option isEqualToString:@"PE"])
                    {
                        
                        cell.exchangeLbl.text= [NSString stringWithFormat:@"%@ %@ PE  %@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"segment"],[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"strikeprice"],finalDate];
                    }
                    
                    else
                    {
                        cell.exchangeLbl.text= [NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"segment"]];
                    }
                    
                    
                    //check//
                    
                    
                    NSString * ltp=[NSString stringWithFormat:@"%.2f",[[tmp valueForKey:@"dbLTP"] floatValue]];
                    float  priceString=[ltp floatValue];
                     NSString * closeStr=[NSString stringWithFormat:@"%.2f",[[tmp valueForKey:@"dbClose"] floatValue]];
                    float close=[closeStr floatValue];
                    //
                    float change=priceString-close;
                    //
                    //
                    //            cell.percentLbl.text=[NSString stringWithFormat:@"%.2f",change];
                    //
                    float changePercentage=((priceString-close) *100/close);
                    
                    
                    
                    NSString * changeStrPer=[NSString stringWithFormat:@"%.2f",changePercentage];
                    NSString * changeValue=[NSString stringWithFormat:@"%.2f",change];
                    
                    NSString * priceStr = [NSString stringWithFormat:@"%.2f",priceString];
                    
                   
                    
                   // if(cell.priceLbl.text.length>0)
                    if(priceStr.length>0)
                    {
                        
                         //cell.priceLbl.text=@"0.00";
                        
                        //    NSNumberFormatter *formatString = [[NSNumberFormatter alloc] init];
                        //    NSString * previousValue = cell.priceLbl.text;
                        //    NSNumber *number2 = [formatString numberFromString:previousValue];
                        
                      //  cell.priceLbl.text=[NSString stringWithFormat:@"%.2f",priceString];
                        
                        CGFloat number2 = (CGFloat)[cell.priceLbl.text floatValue];
                        //NSLog(@"number2 %f",number2);
                        
                        if([cell.exchangeLbl.text containsString:@"CDS"])
                        {
                            
                            
                            NSString * str=[tmp valueForKey:@"dbLTP"];
                            
                            float priceValue=[str floatValue];
                            
                            float priceValue1= (float) priceValue/100000;
                            
                            NSString * priceStr=[NSString stringWithFormat:@"%.4f",priceValue1];
                            
                            CGFloat number1=(CGFloat)[priceStr floatValue];
                            
                            //NSLog(@"number1 %f",number1);
                            
                            if(number1<number2)
                            {
                                //            cell.priceLbl.backgroundColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:0.34];
                                
                                cell.priceLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                               cell.priceLbl.text=[NSString stringWithFormat:@"%@",priceStr];
                            }
                            
                            else if(number1>number2)
                            {
                                //            cell.priceLbl.backgroundColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:0.34];
                                
                                cell.priceLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                                cell.priceLbl.text=[NSString stringWithFormat:@"%@",priceStr];
                            }
                            
                        }
                        
                        
                        
                        
                        
                        else
                        {
//                            NSString * price=[tmp valueForKey:@"dbLTP"];
                            CGFloat number1=(CGFloat)[priceStr floatValue];
                            
                            //NSLog(@"number1 %f",number1);
                            
                            if(number1<number2)
                            {
                                //            cell.priceLbl.backgroundColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:0.34];
                                
                                cell.priceLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                                cell.priceLbl.text=[NSString stringWithFormat:@"%@",priceStr];
                            }
                            
                            else if(number1>number2)
                            {
                                //            cell.priceLbl.backgroundColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:0.34];
                                
                                cell.priceLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                                cell.priceLbl.text=[NSString stringWithFormat:@"%@",priceStr];
                            }
                            
                        }
                        
                        
                        
                        
                        
                        
                    }
                    
                    
                    
                    //        if(priceString.length>7)
                    //        {
                    //            NSString * str=[tmp valueForKey:@"LastPriceValue"];
                    //            float priceValue=[str floatValue];
                    //
                    //            float priceValue1= (float) priceValue/100000;
                    //
                    //            NSString * priceStr=[NSString stringWithFormat:@"%.2f",priceValue1];
                    //
                    //            cell.priceLbl.text=priceStr;
                    //        }
                    //
                    //        else
                    //        {
                    //            cell.priceLbl.text =[tmp valueForKey:@"LastPriceValue"];
                    //        }
                    //
                    
                    
                    NSString * changeStr=changeValue;
                    
                    if([cell.exchangeLbl.text containsString:@"CDS"])
                    {
                        
                        
                        if([changeStrPer containsString:@"-"])
                        {
                            cell.changePerLbl.text = [NSString stringWithFormat:@"%@%@",changeStrPer,str];
                            cell.changePerLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                        }
                        else{
                            cell.changePerLbl.text = [NSString stringWithFormat:@"%@%@",changeStrPer,str];
                            cell.changePerLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                        }
                        
                        
                        
                        
                        NSString * str=changeValue;
                        float priceValue=[str floatValue];
                        
                        float priceValue1= (float) priceValue/100000;
                        
                        NSString * priceStr=[NSString stringWithFormat:@"%.2f",priceValue1];
                        
                        
                        
                        cell.percentLbl.text = priceStr;
                        
                        
                    }
                    
                    else
                    {
                        
                        if([changeStr containsString:@"-"])
                        {
                            cell.percentLbl.text = changeValue;
                            //        cell.percentLbl.textColor=[UIColor colorWithRed:(6/255.0) green:(31/255.0) blue:(43/255.0) alpha:1];
                        }
                        else{
                            cell.percentLbl.text =changeValue;
                            //        cell.percentLbl.textColor=[UIColor colorWithRed:(6/255.0) green:(31/255.0) blue:(43/255.0) alpha:1];
                        }
                        
                        if([changeStrPer containsString:@"-"])
                        {
                            cell.changePerLbl.text = [NSString stringWithFormat:@"%@%@",changeStrPer,str];
                            cell.changePerLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                        }
                        else{
                            cell.changePerLbl.text = [NSString stringWithFormat:@"%@%@",changeStrPer,str];
                            cell.changePerLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                        }
                        
                    }
                    
                    
                    
                    
                    
                    //
                    
                    
                    
                    
                    
                    break;
                }
                
            }
            
            //        UISwipeGestureRecognizer* swipeGestureLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(cellSwipedLeft:)];
            //        [swipeGestureLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
            //        [cell addGestureRecognizer:swipeGestureLeft];
            //
            //
            //        UISwipeGestureRecognizer* swipeGestureRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(cellSwipedRight:)];
            //        [swipeGestureRight setDirection:UISwipeGestureRecognizerDirectionRight];
            //        [cell addGestureRecognizer:swipeGestureRight];
            
            [cell.tradeBtn addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            
           
//        }
            
          
            
        }

        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
        return cell;
    
    }
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    return nil;
}

//- (void)cellSwipedLeft:(UIGestureRecognizer *)gestureRecognizer {
//    
//    if (gestureRecognizer.state == UIGestureRecognizerStateEnded)
//    {
//        [self.socket close];
//    }
//    
//    
//}
//
//- (void)cellSwipedRight:(UIGestureRecognizer *)gestureRecognizer {
//    
//    if (gestureRecognizer.state == UIGestureRecognizerStateEnded)
//    {
//         [self webSocket];
//    }
//    
//   
//}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
    if(tableView==self.customPortfolioTblView)
    {
        
    }
    else if(tableView==self.marketTableView)
    
    {
    @try {
    delegate2.navigationCheck=@"Marketwatch";
    
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Market Watch stock detail page"];
MarketTableViewCell * cell = [self.marketTableView cellForRowAtIndexPath:indexPath];
    
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
    
    NSMutableDictionary *tmp = [[NSMutableDictionary alloc]initWithDictionary:[instrumentTokensDict valueForKey:[ self.allKeysList1 objectAtIndex:indexPath.row]]];
    delegate2.instrumentDepthStr=[tmp valueForKey:@"InstrumentToken"];
        upstoxSymbolName=cell.companyName.text;
//    delegate2.symbolDepthStr=[[delegate2.filteredCompanyName objectAtIndex:indexPath.row]valueForKey:@"symbol"];
    //NSLog(@"%@",tmp);
        
    }
    
    else
    {
        
        delegate2.instrumentDepthStr=[ self.allKeysList1 objectAtIndex:indexPath.row];
    }
   
    
    

    
    
    delegate2.symbolDepthStr=cell.companyName.text;
    
        delegate2.exchaneLblStr=cell.exchangeLbl.text;
    
    
    for(int i=0;i<delegate2.homeNewCompanyArray.count;i++)
    {
        int instrument=[[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"instrument_token"] intValue];
        
        if(instrument==[delegate2.instrumentDepthStr intValue])
        {
    
    delegate2.expirySeriesValue=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"expiryseries"]];
    
        }
        
        
    }
    
    
    delegate2.LTPStr=cell.priceLbl.text;
    
  
     //NSLog(@"%@",delegate2.symbolDepthStr);
    

    delegate2.depth=@"MARKETWATCH";
     //NSLog(@"%@",delegate2.depth);
    
    


//        if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
//        {
//                    disapperCheck=true;
//                    [self unSubMethod];
//
//        }
        
//        else
//        {
            StockView1 * stockDetails=[self.storyboard instantiateViewControllerWithIdentifier:@"stock1"];
            
            [self.navigationController pushViewController:stockDetails animated:YES];
            
//        }

        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
        
        
    }
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

   
}
/*!
 @discussion used to delete symbol from list.
 */

- (IBAction)editTableViewBtn:(id)sender
  {
      @try {
          if(editCheck==true)
          {
              @try {
                  [self.socket close];
                  ltpReloadBool=false;
              if(segmentedControl.selectedSegmentIndex==0)
              {
              [self.marketTableView setEditing: YES animated: YES];
              }
                  else
                  {
                        [self.customPortfolioTblView setEditing: YES animated: YES];
                  }
                  
              }
              @catch (NSException *exception) {
                  //NSLog(@"%@", exception.reason);
              }
              @finally {
                  
              }
              
              
              
              editCheck=false;
          }
          
          else if(editCheck==false)
          {
              @try {
                  ltpReloadBool=true;
                  [self tmpDictMethod];
                  [self webSocket];
                  if(segmentedControl.selectedSegmentIndex==0)
                  {
                      [self.marketTableView setEditing: NO animated: YES];
                  }
                  else
                  {
                        [self.customPortfolioTblView setEditing: NO animated: YES];
                  }
               
                  
              }
              @catch (NSException *exception) {
                  //NSLog(@"%@", exception.reason);
              }
              @finally {
                  
              }
              
              
              
              editCheck=true;
              
          }
          

          
      }
      @catch (NSException * e) {
          //NSLog(@"Exception: %@", e);
      }
      @finally {
          //NSLog(@"finally");
      }
      
      
      
  }
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.marketTableView.editing)
    {
        return UITableViewCellEditingStyleDelete;
    }
   else  if(self.customPortfolioTblView.editing)
    {
        return UITableViewCellEditingStyleDelete;
    }
     return UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
   // [self.socket close];
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    
        @try {

         if(segmentedControl.selectedSegmentIndex==0)
         {
                if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
                {
                    NSMutableDictionary *tmp = [[NSMutableDictionary alloc]initWithDictionary:[instrumentTokensDict valueForKey:[ self.allKeysList1 objectAtIndex:indexPath.row]]];
                    delegate2.instrumentDepthStr=[NSString stringWithFormat:@"%@",[tmp valueForKey:@"InstrumentToken"]];
                }
                else
                {
                    
                    delegate2.instrumentDepthStr=[ self.allKeysList1 objectAtIndex:indexPath.row];
                }
                if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
                {
                    NSMutableArray * newArray=[[NSMutableArray alloc]init];
                    newArray= delegate2.homeNewCompanyArray;
                    
//                    newArray=[[[newArray reverseObjectEnumerator] allObjects] mutableCopy];
                    
                    
                    for(int i=0;i<newArray.count;i++)
                    {
                        int instrument=[[[newArray objectAtIndex:i]valueForKey:@"instrument_token"]intValue];
                        
                        if(instrument==[delegate2.instrumentDepthStr intValue])
                        {
//                         isDelete=[NSString stringWithFormat:@"%@",[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@""]];
                             [delegate2.userMarketWatch removeObjectAtIndex:i];
                            [delegate2.homeInstrumentToken removeObjectAtIndex:i];
                            [self tmpDictMethod];
                            [self webSocket];
                            //[self unSubMethod];
                            [self instruments];
                            
                           
                                
                        }
                            
                        }
                    
                    }
                    
            
                
                else  if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                {
                    for(int i=0;i<delegate2.homeNewCompanyArray.count;i++)
                    {
                        int instrument=[[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"instrument_token"] intValue];
                        
                        if(instrument==[delegate2.instrumentDepthStr intValue])
                        {
                            
                           [delegate2.userMarketWatch removeObjectAtIndex:i];
                             [self instruments];
                            
                        }
                    }
                    
                    
                }
            
                else
                {
                    for(int i=0;i<delegate2.homeNewCompanyArray.count;i++)
                    {
                        int instrument=[[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"exchange_token"] intValue];
                        
                        if(instrument==[delegate2.instrumentDepthStr intValue])
                        {
                            
                            [delegate2.userMarketWatch removeObjectAtIndex:i];
                            //[delegate2.homeNewCompanyArray removeObjectAtIndex:i];
                            
                            [delegate2.localExchageToken removeObjectAtIndex:i];
                            [self tmpDictMethod];
                            [self instruments];
                            
                            
                        }
                    }
                    
                    
                }
                
                
            
            
            
              [self.marketTableView setEditing: NO animated: YES];
            
            
            
            
            
//            [self resultCheck];
             
         }
            
            else
            {
                if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
                {
                    NSMutableDictionary *tmp = [[NSMutableDictionary alloc]initWithDictionary:[instrumentTokensDictCustom valueForKey:[ self.allKeysList1Custom objectAtIndex:indexPath.row]]];
                    delegate2.instrumentDepthStr=[NSString stringWithFormat:@"%@",[tmp valueForKey:@"InstrumentToken"]];
                }
                else
                {
                    
                    delegate2.instrumentDepthStr=[self.allKeysList1Custom objectAtIndex:indexPath.row];
                }
                if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
                {
                    NSMutableArray * newArray=[[NSMutableArray alloc]init];
                    newArray= delegate2.homeNewCompanyArrayCustom;
                    
                    //                    newArray=[[[newArray reverseObjectEnumerator] allObjects] mutableCopy];
                    
                    
                    for(int i=0;i<newArray.count;i++)
                    {
                        int instrument=[[[newArray objectAtIndex:i]valueForKey:@"instrument_token"]intValue];
                        
                        if(instrument==[delegate2.instrumentDepthStr intValue])
                        {
                            //                         isDelete=[NSString stringWithFormat:@"%@",[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@""]];
                            [delegate2.userMarketWatchCustom removeObjectAtIndex:i];
                            [delegate2.homeInstrumentTokenCustom removeObjectAtIndex:i];
                            [self tmpDictMethod];
                            [self webSocket];
                            //[self unSubMethod];
                            [self instruments];
                            
                            
                            
                        }
                        
                    }
                    
                }
                
                
                
                else  if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                {
                    for(int i=0;i<delegate2.homeNewCompanyArrayCustom.count;i++)
                    {
                        int instrument=[[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]valueForKey:@"instrument_token"] intValue];
                        
                        if(instrument==[delegate2.instrumentDepthStr intValue])
                        {
                            
                            [delegate2.userMarketWatchCustom removeObjectAtIndex:i];
                            [self instruments];
                            
                        }
                    }
                    
                    
                }
                
                else
                {
                    for(int i=0;i<delegate2.homeNewCompanyArrayCustom.count;i++)
                    {
                        int instrument=[[[delegate2.homeNewCompanyArrayCustom objectAtIndex:i]valueForKey:@"exchange_token"] intValue];
                        
                        if(instrument==[delegate2.instrumentDepthStr intValue])
                        {
                            
                            [delegate2.userMarketWatchCustom removeObjectAtIndex:i];
                            //[delegate2.homeNewCompanyArray removeObjectAtIndex:i];
                            
                            [delegate2.localExchageTokenCustom removeObjectAtIndex:i];
                            [self tmpDictMethod];
                            [self instruments];
                            
                            
                        }
                    }
                    
                    
                }
                
                
                
                
                
                
                  [self.customPortfolioTblView setEditing: NO animated: YES];
                
                
                
                
                //            [self resultCheck];
                
            }
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
    
   
        
    
    
    
    
 
   
}


/*!
 @discussion used to navigate to order page.
 */
-(void)yourButtonClicked:(UIButton*)sender

{
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"trade_click"];
     @try {
    delegate2.navigationCheck=@"Marketwatch";
    if(delegate2.depthLotSize.count)
    {
        [delegate2.depthLotSize removeAllObjects];
    }
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.marketTableView];
    
    
    
    NSIndexPath *indexPath = [self.marketTableView indexPathForRowAtPoint:buttonPosition];
    MarketTableViewCell *cell = [self.marketTableView cellForRowAtIndexPath:indexPath];
    
  
        
        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
        NSMutableDictionary *tmp = [[NSMutableDictionary alloc]initWithDictionary:[instrumentTokensDict valueForKey:[ self.allKeysList1 objectAtIndex:indexPath.row]]];
        delegate2.instrumentDepthStr=[NSString stringWithFormat:@"%@",[tmp valueForKey:@"InstrumentToken"]];
        }
        else
        {
            
             delegate2.instrumentDepthStr=[ self.allKeysList1 objectAtIndex:indexPath.row];
        }
        if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {  delegate2.upstoxLtpCheck=@"true";
            upstoxSymbolName=cell.companyName.text;
            NSMutableArray * newArray=[[NSMutableArray alloc]init];
            newArray= delegate2.homeNewCompanyArray;
            
              newArray=[[[newArray reverseObjectEnumerator] allObjects] mutableCopy];
            
            
            for(int i=0;i<newArray.count;i++)
            {
                 int instrument=[[[newArray objectAtIndex:i]valueForKey:@"instrument_token"]intValue];
                
                if(instrument==[delegate2.instrumentDepthStr intValue])
                {
                    
                    delegate2.tradingSecurityDes=[[newArray objectAtIndex:i]valueForKey:@"securitydesc"];
                    if([upstoxSymbolName containsString:@"BANKNIFTY"])
                    {
                        delegate2.tradingSecurityDes=[[newArray objectAtIndex:i]valueForKey:@"securitydescupstox"];
                    }
                    @try {
                        NSString * localStr=[NSString stringWithFormat:@"%@",[[newArray objectAtIndex:i]valueForKey:@"lotsize"]];
                        [delegate2.depthLotSize addObject:localStr];
                        
                        //NSLog(@"%@",delegate2.depthLotSize);
                        
                    } @catch (NSException *exception) {
                        
                    } @finally {
                        
                    }
                
                }
            }
            
        }
        
        else  if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
        {
            for(int i=0;i<delegate2.homeNewCompanyArray.count;i++)
            {
               int instrument=[[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"instrument_token"] intValue];
                
                if(instrument==[delegate2.instrumentDepthStr intValue])
                {
                    
                    delegate2.tradingSecurityDes=[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"securitydesc"];
                    
                    @try {
                        NSString * localStr=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"lotsize"]];
                        [delegate2.depthLotSize addObject:localStr];
                        
                        //NSLog(@"%@",delegate2.depthLotSize);
                        
                    } @catch (NSException *exception) {
                        
                    } @finally {
                        
                    }
                    
                }
            }
            
            
        }
        
        else
        {
            for(int i=0;i<delegate2.homeNewCompanyArray.count;i++)
            {
                int instrument=[[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"exchange_token"] intValue];
                
                if(instrument==[delegate2.instrumentDepthStr intValue])
                {
                    
                    delegate2.tradingSecurityDes=[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"securitydesc"];
                    
                    @try {
                        NSString * localStr=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"lotsize"]];
                        [delegate2.depthLotSize addObject:localStr];
                        
                        //NSLog(@"%@",delegate2.depthLotSize);
                        
                    } @catch (NSException *exception) {
                        
                    } @finally {
                        
                    }
                    
                }
            }
            
            
        }

       
        
        //NSLog(@"%@", delegate2.tradingSecurityDes);
        
        delegate2.symbolDepthStr=cell.companyName.text;
        delegate2.orderSegment=cell.exchangeLbl.text;
        
        //NSLog(@"%@",delegate2.symbolDepthStr);
        
        
        
        
        if(delegate2.instrumentDepthStr.length>0 && delegate2.symbolDepthStr.length>0 && delegate2.orderSegment.length>0)
        {
            
            StockOrderView * stockDetails=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
            
            delegate2.tradeBtnBool=true;
            
            
            [self.navigationController pushViewController:stockDetails animated:YES];
        }
            
        
        

        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
//   [delegate2.broadCastInputStream close];
    
   
    
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
    {
        
    }
    else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
//        [self unSubMethod];
        
    }
    else
    {
        [self.socketio disconnect];
        self.socketio=nil;

    }
}
/*!
 @discussion used to upload list of symbols to zambala server.
 */
-(void)instruments
{
    @try {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
//        if(prefs !=nil)
//        {
//            
//            // getting an NSString
//            
//            //        delegate2.homeInstrumentToken=[prefs objectForKey:@"instrumentToken"];
//            //        delegate2.homeinstrumentName=[prefs objectForKey:@"instrumentNameArr"];
//            //        delegate2.homeLotDepth=[prefs objectForKey:@"sampleDepthLot"];
//            //        delegate2.homeExchange=[prefs objectForKey:@"exchange"];
//            //        delegate2.homeFilterCompanyName=[prefs objectForKey:@"filteredCompanyName"];
//            
//            delegate2.homeInstrumentToken = [NSMutableArray arrayWithArray:[prefs objectForKey:@"instrumentToken"]];
//            delegate2.homeinstrumentName = [NSMutableArray arrayWithArray:[prefs objectForKey:@"instrumentNameArr"]];
//            delegate2.homeLotDepth = [NSMutableArray arrayWithArray:[prefs objectForKey:@"sampleDepthLot"]];
//            delegate2.homeExchange = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"exchange"]];
//            delegate2.homeFilterCompanyName = [NSMutableArray arrayWithArray:[prefs objectForKey:@"filteredCompanyName"]];
//            
//             delegate2.homeExchangeToken = [NSMutableArray arrayWithArray:[prefs objectForKey:@"exchange_token"]];
//             delegate2.homeMtExchange = [NSMutableArray arrayWithArray:[prefs objectForKey:@"exchangevalue"]];
//            
//            @try {
//                delegate2.homeLotSizeArray = [NSMutableArray arrayWithArray:[prefs objectForKey:@"lotsize"]];
//                delegate2.homeExpiryDate = [NSMutableArray arrayWithArray:[prefs objectForKey:@"expiryseries"]];
//                
//            } @catch (NSException *exception) {
//                
//            } @finally {
//                
//            }
//            
//            @try {
//                delegate2.homeOptionArray = [NSMutableArray arrayWithArray:[prefs objectForKey:@"optiontype"]];
//                
//                delegate2.homeStrkeArray = [NSMutableArray arrayWithArray:[prefs objectForKey:@"strikeprice"]];
//            } @catch (NSException *exception) {
//                
//            } @finally {
//                
//            }
//            
//            
//          
//           
//            scripDetailsArray=[[NSMutableArray alloc]init];
//            scripDetails=[[NSMutableDictionary alloc]init];
//            
//            
//            
//            
//            for(int i=0;i<delegate2.userMarketWatch.count;i++)
//            {
//                 NSMutableDictionary * tmp_dict=[[NSMutableDictionary alloc]init];
//                
//                [tmp_dict setObject:[delegate2.userMarketWatch objectAtIndex:i] forKey:@"instrument_token"];
//                
//                 [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"name"] forKey:@"name"];
//                 [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"symbol"] forKey:@"symbol"];
//                 [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"exchange"] forKey:@"exchange"];
//                [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"segment"] forKey:@"segment"];
//                 [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"exchange_token"] forKey:@"exchange_token"];
//                [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"exchangevalue"] forKey:@"exchangevalue"];
//                [tmp_dict setObject:@"0" forKey:@"ltp"];
//                
//                
//                @try {
//             [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"fincode"] forKey:@"fincode"];
//                } @catch (NSException *exception) {
//                    [tmp_dict setObject:@"0" forKey:@"fincode"];
//                } @finally {
//                    
//                }
//                
//                @try {
//                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"expiryseries"] forKey:@"expiryseries"];
//                     [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"lotsize"] forKey:@"lotsize"];
//                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"securitydesc"] forKey:@"securitydesc"];
//                } @catch (NSException *exception) {
//                    [tmp_dict setObject:@"0" forKey:@"lotsize"];
//                    [tmp_dict setObject:@"0" forKey:@"expiryseries"];
//                } @finally {
//                    
//                }
//                
//                @try {
//                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"optiontype"] forKey:@"optiontype"];
//                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"strikeprice"] forKey:@"strikeprice"];
//                } @catch (NSException *exception) {
//                    [tmp_dict setObject:@"0" forKey:@"optiontype"];
//                    [tmp_dict setObject:@"0" forKey:@"strikeprice"];
//                } @finally {
//                    
//                }
//
//                
//                
//                
//                [scripDetailsArray insertObject:tmp_dict atIndex:i];
//                
//            }
//            
////            NSCharacterSet * set=[NSCharacterSet newlineCharacterSet];
////            
////            NSString * localString=[NSString stringWithFormat:@"%@",scripDetailsArray];
////            
////            NSString * localSring1=[localString stringByReplacingOccurrencesOfString:@"(" withString:@"["]
////            ;
////            
////            NSString * localSring2=[localSring1 stringByReplacingOccurrencesOfString:@")" withString:@"]"];
////            
////             NSString * localSring3 = [localSring2 stringByReplacingOccurrencesOfString:@"\n" withString:@""];
////            NSString * localSring4 = [localSring3 stringByReplacingOccurrencesOfString:@" " withString:@""];
////            
////            NSString * localSring5 = [[localSring4 componentsSeparatedByCharactersInSet:set]componentsJoinedByString:@""];
//        
//            
//            
//            
//            
//            
//            
//            [scripDetails setObject:scripDetailsArray forKey:@"data"];
//
//            
//            
//          
//            
//            
//            //NSLog(@"%@",scripDetails);
//            
//            
//            
//            
//            
//            
//        }
//        
//        else
//        {
//            
//            delegate2.homeExchange=delegate2.exchange;
//            delegate2.homeinstrumentName=delegate2.instrumentNameArr;
//            delegate2.homeInstrumentToken=delegate2.instrumentToken;
//            delegate2.homeLotDepth=delegate2.sampleDepthLot;
//            delegate2.homeFilterCompanyName=delegate2.filteredCompanyName;
//            delegate2.homeExchangeToken=delegate2.exchangeToken;
//            delegate2.homeMtExchange=delegate2.mtExchange;
//            
//            
        
//
//
        if(segmentedControl.selectedSegmentIndex==0)
        {
            scripDetailsArray=[[NSMutableArray alloc]init];
            scripDetails=[[NSMutableDictionary alloc]init];
        if(delegate2.userMarketWatch.count>=0)
        {
            

            
            NSMutableArray * uniqueArray=[[NSMutableArray alloc]init];
            
            for(int i=0;i<delegate2.userMarketWatch.count;i++)
            {
                
                
            
            NSMutableDictionary* E1 = [delegate2.userMarketWatch objectAtIndex:i];
                
                NSString * string=[NSString stringWithFormat:@"%@",[E1 objectForKey:@"exchange_token"]];
            BOOL hasDuplicate = [[uniqueArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"exchange_token == %@",string]] count] > 0;
            
            if (!hasDuplicate)
            {
                [uniqueArray addObject:E1];
                //NSLog(@"No unique");
                
            }
                
                else
                    
                {
                    
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Symbol already added in your list" preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                    }];
                    
                    [alert addAction:okAction];
                }
                
                
            }
            
            


            
            for(int i=0;i<uniqueArray.count;i++)
            {
                
                NSMutableDictionary * tmp_dict=[[NSMutableDictionary alloc]init];
                //                [tmp_dict setObject:[delegate2.userMarketWatch objectAtIndex:i] forKey:@"instrument_token"];
                
//                [tmp_dict setObject:[uniqueArray objectAtIndex:i] forKey:@"name"];
                
                tmp_dict=[uniqueArray objectAtIndex:i];
                
                //                  [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"symbol"] forKey:@"symbol"];
                //
                //                [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"exchange"] forKey:@"exchange"];
                //                [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"segment"] forKey:@"segment"];
                //                [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"exchange_token"] forKey:@"exchange_token"];
                //                [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"exchangevalue"] forKey:@"exchangevalue"];
                //                [tmp_dict setObject:@"0" forKey:@"ltp"];
                
                //                @try {
                //                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"fincode"] forKey:@"fincode"];
                //                } @catch (NSException *exception) {
                //                    [tmp_dict setObject:@"0"forKey:@"fincode"];
                //                } @finally {
                //
                //                }
                //
                //                @try {
                //                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"expiryseries"] forKey:@"expiryseries"];
                //                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"lotsize"] forKey:@"lotsize"];
                //                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"securitydesc"] forKey:@"securitydesc"];
                //                } @catch (NSException *exception) {
                //                    [tmp_dict setObject:@"0" forKey:@"lotsize"];
                //                    [tmp_dict setObject:@"0" forKey:@"expiryseries"];
                //                } @finally {
                //
                //                }
                //
                //                @try {
                //                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"optiontype"] forKey:@"optiontype"];
                //                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"strikeprice"] forKey:@"strikeprice"];
                //                } @catch (NSException *exception) {
                //                    [tmp_dict setObject:@"0" forKey:@"optiontype"];
                //                    [tmp_dict setObject:@"0" forKey:@"strikeprice"];
                //                } @finally {
                //
                //                }
                
                
                
                
                [scripDetailsArray insertObject:tmp_dict atIndex:i];
                
                //                NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:scripDetailsArray];
                //                NSArray *arrayWithoutDuplicates = [scripDetailsArray array];
                
                //            }
                
                //             NSCharacterSet * set=[NSCharacterSet newlineCharacterSet];
                //
                //            NSString * string=@"";
                //
                //            NSString * localString=[NSString stringWithFormat:@"%@",scripDetailsArray];
                //
                //            NSString * localSring1=[localString stringByReplacingOccurrencesOfString:@"(" withString:@"["]
                //            ;
                //
                //            NSString * localSring2=[localSring1 stringByReplacingOccurrencesOfString:@")" withString:@"]"];
                //
                //            NSString * localSring3 = [localSring2 stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                //            NSString * localSring4 = [localSring3 stringByReplacingOccurrencesOfString:@" " withString:@""];
                //
                //
                //
                //            NSString * localSring5 = [[localSring4 componentsSeparatedByCharactersInSet:set]componentsJoinedByString:@""];
                //
                //
                
                
                
                
                [scripDetails setObject:scripDetailsArray forKey:@"data"];
                
                
                //NSLog(@"%@",scripDetails);
                
                
            }
            
            //NSLog(@"%@",delegate2.homeInstrumentToken);
            
        }
            if(scripDetails)
            {
                [self resultCheck];
            }
        }
        else
        {
            scripDetailsArray=[[NSMutableArray alloc]init];
            scripDetails=[[NSMutableDictionary alloc]init];
            if(delegate2.userMarketWatchCustom.count==0)
            {
               
                if(delegate2.customPortfolioArray.count>0)
                {
                    [delegate2.userMarketWatchCustom addObject:[delegate2.customPortfolioArray objectAtIndex:0]];
                    [delegate2.customPortfolioArray removeAllObjects];
                }
            }
            else
            {
              
                if(delegate2.customPortfolioArray.count>0)
                {
                      [delegate2.userMarketWatchCustom addObject:[delegate2.customPortfolioArray objectAtIndex:0]];
                    [delegate2.customPortfolioArray removeAllObjects];
                }
            }
            if(delegate2.userMarketWatchCustom.count>=0)
            {
                
                
                
                NSMutableArray * uniqueArray=[[NSMutableArray alloc]init];
                
                for(int i=0;i<delegate2.userMarketWatchCustom.count;i++)
                {
                    
                    
                    
                    NSMutableDictionary* E1 = [delegate2.userMarketWatchCustom objectAtIndex:i];
                    
                    NSString * string=[NSString stringWithFormat:@"%@",[E1 objectForKey:@"exchange_token"]];
                    BOOL hasDuplicate = [[uniqueArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"exchange_token == %@",string]] count] > 0;
                    
                    if (!hasDuplicate)
                    {
                        [uniqueArray addObject:E1];
                        //NSLog(@"No unique");
                        
                    }
                    
                    else
                        
                    {
                        
                        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Symbol already added in your list" preferredStyle:UIAlertControllerStyleAlert];
                        
                        [self presentViewController:alert animated:YES completion:^{
                            
                        }];
                        
                        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            
                        }];
                        
                        [alert addAction:okAction];
                    }
                    
                    
                }
                
                
                
                
                
                for(int i=0;i<uniqueArray.count;i++)
                {
                    
                    NSMutableDictionary * tmp_dict=[[NSMutableDictionary alloc]init];
                    //                [tmp_dict setObject:[delegate2.userMarketWatch objectAtIndex:i] forKey:@"instrument_token"];
                    
                    //                [tmp_dict setObject:[uniqueArray objectAtIndex:i] forKey:@"name"];
                    
                    tmp_dict=[uniqueArray objectAtIndex:i];
                    
                    //                  [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"symbol"] forKey:@"symbol"];
                    //
                    //                [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"exchange"] forKey:@"exchange"];
                    //                [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"segment"] forKey:@"segment"];
                    //                [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"exchange_token"] forKey:@"exchange_token"];
                    //                [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"exchangevalue"] forKey:@"exchangevalue"];
                    //                [tmp_dict setObject:@"0" forKey:@"ltp"];
                    
                    //                @try {
                    //                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"fincode"] forKey:@"fincode"];
                    //                } @catch (NSException *exception) {
                    //                    [tmp_dict setObject:@"0"forKey:@"fincode"];
                    //                } @finally {
                    //
                    //                }
                    //
                    //                @try {
                    //                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"expiryseries"] forKey:@"expiryseries"];
                    //                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"lotsize"] forKey:@"lotsize"];
                    //                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"securitydesc"] forKey:@"securitydesc"];
                    //                } @catch (NSException *exception) {
                    //                    [tmp_dict setObject:@"0" forKey:@"lotsize"];
                    //                    [tmp_dict setObject:@"0" forKey:@"expiryseries"];
                    //                } @finally {
                    //
                    //                }
                    //
                    //                @try {
                    //                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"optiontype"] forKey:@"optiontype"];
                    //                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"strikeprice"] forKey:@"strikeprice"];
                    //                } @catch (NSException *exception) {
                    //                    [tmp_dict setObject:@"0" forKey:@"optiontype"];
                    //                    [tmp_dict setObject:@"0" forKey:@"strikeprice"];
                    //                } @finally {
                    //
                    //                }
                    
                    
                    
                    
                    [scripDetailsArray insertObject:tmp_dict atIndex:i];
                    
                    //                NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:scripDetailsArray];
                    //                NSArray *arrayWithoutDuplicates = [scripDetailsArray array];
                    
                    //            }
                    
                    //             NSCharacterSet * set=[NSCharacterSet newlineCharacterSet];
                    //
                    //            NSString * string=@"";
                    //
                    //            NSString * localString=[NSString stringWithFormat:@"%@",scripDetailsArray];
                    //
                    //            NSString * localSring1=[localString stringByReplacingOccurrencesOfString:@"(" withString:@"["]
                    //            ;
                    //
                    //            NSString * localSring2=[localSring1 stringByReplacingOccurrencesOfString:@")" withString:@"]"];
                    //
                    //            NSString * localSring3 = [localSring2 stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                    //            NSString * localSring4 = [localSring3 stringByReplacingOccurrencesOfString:@" " withString:@""];
                    //
                    //
                    //
                    //            NSString * localSring5 = [[localSring4 componentsSeparatedByCharactersInSet:set]componentsJoinedByString:@""];
                    //
                    //
                    
                    
                    
                    
                    [scripDetails setObject:scripDetailsArray forKey:@"data"];
                    
                    
                    //NSLog(@"%@",scripDetails);
                    
                    
                }
                
                //NSLog(@"%@",delegate2.homeInstrumentToken);
                
            }
            if(scripDetails)
            {
             [self resultCheck];
            }
        }
            
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
        
    
        
        
        
    
    
}

//RECHABILITY//

-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    
    
}

-(BOOL)networkStatus
{
    
    
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    
    if(netStatus==NotReachable)
    {
        
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    return YES;
    
    
}
/*!
 @discussion used to upload list of symbols to zambala server.
 */
-(void)resultCheck
{
    @try {
      
        
   
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"authtoken":delegate2.zenwiseToken,
                                  @"deviceid":delegate2.currentDeviceId
                               };
    
    
    
    
        NSString * localUrl;
        if(segmentedControl.selectedSegmentIndex==0)
        {
              localUrl=[NSString stringWithFormat:@"%@clienttrade/watch",delegate2.baseUrl];
        }
        else
        {
            localUrl=[NSString stringWithFormat:@"%@clienttrade/watchportfolio",delegate2.baseUrl];
        }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:localUrl]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSDictionary * parameters = @{
                   @"clientid":delegate2.userID,
                   @"watchinfo":scripDetails,
                   @"mobilenumber":number
                   
                   };
    
    NSData * postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];

    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if(data!=nil)
                                                    {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==403)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                            
                                                        }
                                                        else if([httpResponse statusCode]==401)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                        
                                                            
                                                        NSMutableDictionary * responseDict=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        //NSLog(@"%@",responseDict);

                                                        
                                                        }
                                                    
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                       
                                                        [self getScripMethod];
                                                        
                                                    });
                                                    }
                                                    else
                                                    {
                                                        [self resultCheck];
                                                    }
                                                }];
    [dataTask resume];
    
    
}
@catch (NSException * e) {
    //NSLog(@"Exception: %@", e);
}
@finally {
    //NSLog(@"finally");
}



}

/*!
 @discussion used to get list of symbols to zambala server.
 */
-(void)getScripMethod
{
    @try
    {
      
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"authtoken":delegate2.zenwiseToken,
                                  @"deviceid":delegate2.currentDeviceId
                               };
    
    NSString * localUrl;
        
        if(segmentedControl.selectedSegmentIndex==0)
        {
            localUrl=[NSString stringWithFormat:@"%@clienttrade/watch/%@?mobilenumber=%@",delegate2.baseUrl,delegate2.userID,number];
          
        }
        else if(segmentedControl.selectedSegmentIndex==1)
        {
             localUrl=[NSString stringWithFormat:@"%@clienttrade/watchportfolio/%@?mobilenumber=%@",delegate2.baseUrl,delegate2.userID,number];
           
        }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:localUrl]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
   
    
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if(data!=nil)
                                                    {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        if([httpResponse statusCode]==403)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                            
                                                        }
                                                        else if([httpResponse statusCode]==401)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                        
                                                        responseDict1=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                       if(segmentedControl.selectedSegmentIndex==0)
                                                       {
                                                        
                                                        delegate2.marketWatchBool=false;
                                                        //NSLog(@"Response dict1:%@",responseDict1);
                                                        
                                                        delegate2.localExchageToken=[[NSMutableArray alloc]init];
                                                        delegate2.homeNewCompanyArray=[[NSMutableArray alloc]init];
                                                        delegate2.userMarketWatch=[[NSMutableArray alloc]init];
                                                        delegate2.homeInstrumentToken=[[NSMutableArray alloc]init];
                                                        
                                                        for(int i=0;i<[[responseDict1 objectForKey:@"data"] count];i++)
                                                        {
//                                                            [delegate2.localExchageToken addObject:[[[[responseDict1 objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"] objectForKey:@"exchange_token"]];
//                                                            [delegate2.homeInstrumentToken addObject:[[[[responseDict1 objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"] objectForKey:@"instrument_token"]];
//
//                                                           [delegate2.homeNewCompanyArray addObject:[[[responseDict1 objectForKey:@"data"]objectAtIndex:i] objectForKey:@"name"]];
//
//                                                            [delegate2.userMarketWatch addObject:[[[responseDict1 objectForKey:@"data"]objectAtIndex:i] objectForKey:@"name"]];
//
                                                            
                                                            [delegate2.localExchageToken addObject:[[[responseDict1 objectForKey:@"data"]objectAtIndex:i] objectForKey:@"exchange_token"]];
                                                            [delegate2.homeInstrumentToken addObject:[[[responseDict1 objectForKey:@"data"]objectAtIndex:i] objectForKey:@"instrument_token"]];
                                                            
                                                            [delegate2.homeNewCompanyArray addObject:[[responseDict1 objectForKey:@"data"]objectAtIndex:i]];
                                                            
//                                                            if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
//                                                            {
//                                                                            delegate2.homeInstrumentToken=[[[delegate2.homeInstrumentToken reverseObjectEnumerator] allObjects] mutableCopy];
//                                                            }
                                                            
                                                            [delegate2.userMarketWatch addObject:[[responseDict1 objectForKey:@"data"]objectAtIndex:i]];
                                                        }
                                                        
                                                           Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
                                                           
                                                           [mixpanelMini identify:delegate2.userID];
                                                           
                                                        
                                                        
                                                        
                                                        NSMutableArray * scripArray=[[NSMutableArray alloc]init];
                                                        
                                                        for(int i=0;i<[[responseDict1 objectForKey:@"data"]count];i++)
                                                        {
                                                            NSString * string2=[[NSString alloc]init];
                                                            string2=[NSString stringWithFormat:@"%@", [[[responseDict1 objectForKey:@"data"]objectAtIndex:i]objectForKey:@"symbol"]];
                                                            [scripArray addObject:string2];
                                                                                               }
                                                        
                                                        
                                                        NSArray * scripArrayNew=[scripArray mutableCopy];
                                                        
                                                        [mixpanelMini.people set:@{@"watchlist":scripArrayNew}];
                                                        
                                                        
                                                       }
                                                        
                                                        else if(segmentedControl.selectedSegmentIndex==1)
                                                        {
                                                            Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
                                                            
                                                            [mixpanelMini identify:delegate2.userID];
                                                            
                                                            delegate2.customWatchBool=false;
                                                            delegate2.localExchageTokenCustom=[[NSMutableArray alloc]init];
                                                            delegate2.homeNewCompanyArrayCustom=[[NSMutableArray alloc]init];
                                                            delegate2.userMarketWatchCustom=[[NSMutableArray alloc]init];
                                                            delegate2.homeInstrumentTokenCustom=[[NSMutableArray alloc]init];
                                                            for(int i=0;i<[[responseDict1 objectForKey:@"data"] count];i++)
                                                            {
                                                                [delegate2.localExchageTokenCustom addObject:[[[responseDict1 objectForKey:@"data"]objectAtIndex:i] objectForKey:@"exchange_token"]];
                                                                [delegate2.homeInstrumentTokenCustom addObject:[[[responseDict1 objectForKey:@"data"]objectAtIndex:i] objectForKey:@"instrument_token"]];
                                                                
                                                                [delegate2.homeNewCompanyArrayCustom addObject:[[responseDict1 objectForKey:@"data"]objectAtIndex:i]];
                                                                
                                                                //                                                            if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
                                                                //                                                            {
                                                                //                                                                            delegate2.homeInstrumentToken=[[[delegate2.homeInstrumentToken reverseObjectEnumerator] allObjects] mutableCopy];
                                                                //                                                            }
                                                                
                                                                [delegate2.userMarketWatchCustom addObject:[[responseDict1 objectForKey:@"data"]objectAtIndex:i]];
                                                            }
                                                            NSArray * scripArrayNew=[delegate2.userMarketWatchCustom mutableCopy];
                                                            
                                                            [mixpanelMini.people set:@{@"portfoliowatchlist":scripArrayNew}];
                                                            
                                                        }
                                                        }
                                                        
                                                    }
                                                    
                                                   
                                                        
                                                        
                                                        delegate2.searchToWatch=false;
                                                       // [self tmpDictMethod];
                                                    
                                                    
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        
                                                         [self tmpDictMethod];
                                                        
                                                        [self socketHitMethod];
                                                        
                                                        
                                                    });
                                                    }
                                                    else
                                                    {
                                                        [self getScripMethod];
                                                    }
                                                }];
    [dataTask resume];
    
    
}
@catch (NSException * e) {
    //NSLog(@"Exception: %@", e);
}
@finally {
    //NSLog(@"finally");
}



    
}




/*!
 @discussion used to send broadcast request to multitrade.
 */



/*!
 @discussion used to send message to socket.
 */


/*!
 @discussion recieves message from socket(multitrade).
 */

/*!
 @discussion used to send broadcast request(multitrade).
 */


/*!
 @discussion used to send message to socket(multitrade).
 */

/*!
 @discussion used to open socket when app resign active.
 */

- (void)backMethod:(NSNotification *)note
{
    
//    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
//    {
//      sad  [self instruments];
//
//    }
//
//    else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
//    {
////        [self webSocket];
//        [self instruments];
//    }
//    else
//    {
//
//
//        //    if(delegate2.searchToWatch==true)
//        //    {
//        //        delegate2.searchToWatch=false;
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"broad" object:nil];
//        [self instruments];
//
//        //    }
//        //
//        //    else
//        //    {
//        //         [[NSNotificationCenter defaultCenter] postNotificationName:@"orderwatch" object:nil];
//        //
//        //    }
//
//    }
    
    
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
    [self webSocket];
    [self tmpDictMethod];
    [self socketHitMethod];
    }
}


-(void)viewDidDisappear:(BOOL)animated
{
    
//    [delegate2.broadCastInputStream close];
    delegate2.homeMt=false;
    self.socket=nil;
    
    if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
        //[self.socket close];
        disapperCheck=true;
        
        //dispatch_async(dispatch_get_main_queue(), ^{
//            [self unSubMethod];
    //    });
    }
//    
//    if (self.view.window == nil) //you should be sure what the view is removed from the window
//    {
//        self.view = nil;
//        //remove other temporary objects
//        //self.models = nil;
//        //[self.request cancel];
//    }
}

-(void)socketHitMethod
{
    @try
    {
    
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
    {
        
        [self webSocket];
        
    }
    
    else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
         [self subMethod];
         [self webSocket];
    }
    
    
    
    else
    {
        if(segmentedControl.selectedSegmentIndex==0)
        {
        if(delegate2.homeNewCompanyArray.count>0)
        {
           
            self.ifEmptyView.hidden=YES;
            self.marketTableView.hidden=NO;
            [self webSocket];
            
            
        }else
        {
            self.activityIndicator.hidden=YES;
            self.ifEmptyView.hidden=NO;
            self.marketTableView.hidden=YES;
        }
        }
        else
        {
            if(delegate2.homeNewCompanyArrayCustom.count>0)
            {
                
                self.ifEmptyView.hidden=YES;
                self.customPortfolioView.hidden=NO;
                self.marketTableView.hidden=YES;
                [self webSocket];
                
                
            }else
            {
                self.activityIndicator.hidden=YES;
                self.ifEmptyView.hidden=NO;
                self.marketTableView.hidden=YES;
                self.customPortfolioView.hidden=YES;
            }
        }
        
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

}



-(void)tmpDictMethod
{
    @try
    {
    if(segmentedControl.selectedSegmentIndex==0)
    {
    if(delegate2.tmpValuesDict.count>0)
    {
        instrumentTokensDict=delegate2.tmpValuesDict;
        
        allKeysList = [[instrumentTokensDict allKeys]mutableCopy];
        
        //NSLog(@"%@",allKeysList);
        
        self.allKeysList1=[[NSMutableArray alloc]init];
        
        for(int i=0;i<delegate2.localExchageToken.count;i++)
        {
            
            for(int j=0;j<allKeysList.count;j++)
            {
                NSNumber * number1=[delegate2.localExchageToken objectAtIndex:i];
                int int1=[number1 intValue];
                NSNumber * number2=[allKeysList objectAtIndex:j];
                
                int int2=[number2 intValue];
                
                if(int1==int2)
                {
                    [ self.allKeysList1 addObject:[allKeysList objectAtIndex:j]];
                }
            }
            
            
        }
        
        
        self.activityIndicator.hidden=YES;
        self.ifEmptyView.hidden=YES;
        self.marketTableView.hidden=NO;
        
        
        //        dispatch_async(dispatch_get_main_queue(), ^{
        
//        self.marketTableView.delegate=self;
//        self.marketTableView.dataSource=self;
        [self.marketTableView reloadData];
        //        });
        
        
        //NSLog(@"%@",tmpDict);
        
        //NSLog(@"%@",instrumentTokensDict);
    }
    else
    {
    if([delegate2.brokerNameStr isEqualToString:@"Upstox"]||[delegate2.brokerNameStr isEqualToString:@"Zerodha"])
    {
        instrumentTokensDict=[[NSMutableDictionary alloc]init];
        //NSLog(@"Home Instrument token:%@",delegate2.homeInstrumentToken);
        
       
        
        if(delegate2.homeInstrumentToken.count>0)
        {
            for(int i=0;i<delegate2.homeInstrumentToken.count;i++)
            {
                tmpDict = [[NSMutableDictionary alloc]init];
                [tmpDict setValue:@"0.00" forKey:@"ClosePriceValue"];
                [tmpDict setValue:@"0.00" forKey:@"LastPriceValue"];
                [tmpDict setValue:@"0.00" forKey:@"ChangeValue"];
                [tmpDict setValue:@"0.00" forKey:@"ChangePercentageValue"];
                [tmpDict setValue:[NSString stringWithFormat:@"%@",[delegate2.homeInstrumentToken objectAtIndex:i]] forKey:@"InstrumentToken"];
                [instrumentTokensDict setValue:tmpDict forKeyPath:[NSString stringWithFormat:@"%@",[delegate2.homeInstrumentToken objectAtIndex:i]]];
            }
            
            allKeysList = [[instrumentTokensDict allKeys]mutableCopy];
            
            //NSLog(@"%@",allKeysList);
            
             self.allKeysList1=[[NSMutableArray alloc]init];
            
            for(int i=0;i<delegate2.homeInstrumentToken.count;i++)
            {
                
                for(int j=0;j<allKeysList.count;j++)
                {
                    NSNumber * number1=[delegate2.homeInstrumentToken objectAtIndex:i];
                    int int1=[number1 intValue];
                    NSNumber * number2=[allKeysList objectAtIndex:j];
                    
                    int int2=[number2 intValue];
                    
                    if(int1==int2)
                    {
                        [ self.allKeysList1 addObject:[allKeysList objectAtIndex:j]];
                    }
                }
                
                
            }
            
            self.activityIndicator.hidden=YES;
            self.ifEmptyView.hidden=YES;
            self.marketTableView.hidden=NO;
           
            
            dispatch_async(dispatch_get_main_queue(), ^{

//                self.marketTableView.delegate=self;
//                self.marketTableView.dataSource=self;
                [self.marketTableView reloadData];
            });
            
           
        }
        
    }
    
    

    else
        
    {
    if(delegate2.localExchageToken.count>0)
    {
    
    instrumentTokensDict=[[NSMutableDictionary alloc]init];
    
   
   
    for(int i=0;i<delegate2.localExchageToken.count;i++)
    {
    tmpDict = [[NSMutableDictionary alloc]init];
    [tmpDict setValue:@"0.00" forKey:@"dbLTP"];
    [tmpDict setValue:@"0.00" forKey:@"dbClose"];
    [tmpDict setValue:[NSString stringWithFormat:@"%@",[delegate2.localExchageToken objectAtIndex:i]] forKey:@"stSecurityID"];
        
          [instrumentTokensDict setValue:tmpDict forKeyPath:[delegate2.localExchageToken objectAtIndex:i]];
       
        
    }
        
        allKeysList = [[instrumentTokensDict allKeys]mutableCopy];
        
        //NSLog(@"%@",allKeysList);
        
         self.allKeysList1=[[NSMutableArray alloc]init];
        
        for(int i=0;i<delegate2.localExchageToken.count;i++)
        {
            
            for(int j=0;j<allKeysList.count;j++)
            {
                NSNumber * number1=[delegate2.localExchageToken objectAtIndex:i];
                int int1=[number1 intValue];
                NSNumber * number2=[allKeysList objectAtIndex:j];
                
                int int2=[number2 intValue];
                
                if(int1==int2)
                {
                    [ self.allKeysList1 addObject:[allKeysList objectAtIndex:j]];
                }
            }
            
            
        }
        
        
        self.activityIndicator.hidden=YES;
        self.ifEmptyView.hidden=YES;
        self.marketTableView.hidden=NO;
        
   
//        dispatch_async(dispatch_get_main_queue(), ^{

//              self.marketTableView.delegate=self;
//              self.marketTableView.dataSource=self;
              [self.marketTableView reloadData];
//        });
        

    //NSLog(@"%@",tmpDict);
    
    //NSLog(@"%@",instrumentTokensDict);
        
        
    }
    
    }
        
    }
    

    }
    
    else if(segmentedControl.selectedSegmentIndex==1)
    {
        
      
            if([delegate2.brokerNameStr isEqualToString:@"Upstox"]||[delegate2.brokerNameStr isEqualToString:@"Zerodha"])
            {
                instrumentTokensDictCustom=[[NSMutableDictionary alloc]init];
               
                
                
                
                if(delegate2.homeInstrumentTokenCustom.count>0)
                {
                    for(int i=0;i<delegate2.homeInstrumentTokenCustom.count;i++)
                    {
                        tmpDictCustom = [[NSMutableDictionary alloc]init];
                       
                        [tmpDictCustom setValue:@"0.00" forKey:@"LastPriceValue"];
                        [tmpDictCustom setValue:[NSString stringWithFormat:@"%@",[delegate2.homeInstrumentTokenCustom objectAtIndex:i]] forKey:@"InstrumentToken"];
                        [instrumentTokensDictCustom setValue:tmpDictCustom forKeyPath:[NSString stringWithFormat:@"%@",[delegate2.homeInstrumentTokenCustom objectAtIndex:i]]];
                    }
                    
                    allKeysListCustom = [[instrumentTokensDictCustom allKeys]mutableCopy];
                    
               
                    self.allKeysList1Custom=[[NSMutableArray alloc]init];
                    
                    for(int i=0;i<delegate2.homeInstrumentTokenCustom.count;i++)
                    {
                        
                        for(int j=0;j<allKeysListCustom.count;j++)
                        {
                            NSNumber * number1=[delegate2.homeInstrumentTokenCustom objectAtIndex:i];
                            int int1=[number1 intValue];
                            NSNumber * number2=[allKeysListCustom objectAtIndex:j];
                            
                            int int2=[number2 intValue];
                            
                            if(int1==int2)
                            {
                                [ self.allKeysList1Custom addObject:[allKeysListCustom objectAtIndex:j]];
                            }
                        }
                        
                        
                    }
                    
                    if(delegate2.homeInstrumentTokenCustom.count>0)
                    {
                    
                    self.activityIndicator.hidden=YES;
                    self.ifEmptyView.hidden=YES;
                    self.customPortfolioView.hidden=NO;
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
//                        self.customPortfolioTblView.delegate=self;
//                        self.customPortfolioTblView.dataSource=self;
                        [self.customPortfolioTblView reloadData];
                    });
                    }else
                    {
                        self.ifEmptyView.hidden=NO;
                    }
                    
                    
                }
                
            }
            
            
            
            else
                
            {
                if(delegate2.localExchageTokenCustom.count>0)
                {
                    
                    instrumentTokensDictCustom=[[NSMutableDictionary alloc]init];
                    
                    
                    
                    for(int i=0;i<delegate2.localExchageTokenCustom.count;i++)
                    {
                        tmpDictCustom = [[NSMutableDictionary alloc]init];
                        [tmpDictCustom setValue:@"0.00" forKey:@"dbLTP"];
                        [tmpDictCustom setValue:[NSString stringWithFormat:@"%@",[delegate2.localExchageTokenCustom objectAtIndex:i]] forKey:@"stSecurityID"];
                        
                        [instrumentTokensDictCustom setValue:tmpDictCustom forKeyPath:[delegate2.localExchageTokenCustom objectAtIndex:i]];
                        
                        
                    }
                    
                    allKeysListCustom = [[instrumentTokensDictCustom allKeys]mutableCopy];
                    
                  
                    
                    self.allKeysList1Custom=[[NSMutableArray alloc]init];
                    
                    for(int i=0;i<delegate2.localExchageTokenCustom.count;i++)
                    {
                        
                        for(int j=0;j<allKeysListCustom.count;j++)
                        {
                            NSNumber * number1=[delegate2.localExchageTokenCustom objectAtIndex:i];
                            int int1=[number1 intValue];
                            NSNumber * number2=[allKeysListCustom objectAtIndex:j];
                            
                            int int2=[number2 intValue];
                            
                            if(int1==int2)
                            {
                                [ self.allKeysList1Custom addObject:[allKeysListCustom objectAtIndex:j]];
                            }
                        }
                        
                        
                    }
                    
                    if(delegate2.localExchageTokenCustom.count>0)
                    {
                    self.activityIndicator.hidden=YES;
                    self.ifEmptyView.hidden=YES;
                    self.customPortfolioView.hidden=NO;
                    
                    
                    //        dispatch_async(dispatch_get_main_queue(), ^{
                    
//                    self.customPortfolioTblView.delegate=self;
//                    self.customPortfolioTblView.dataSource=self;
                    [self.customPortfolioTblView reloadData];
                    }else
                    {
                        self.ifEmptyView.hidden=NO;
                    }
                    //        });
                    
                    
                 
                    
                    
                    
                    
                }
                
            }
            
        }
    
        
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)sender{
    //executes when you scroll the scrollView
    ltpReloadBool=false;
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    ltpReloadBool=true;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    if(tableView==self.customPortfolioTblView)
    {
        return 90;
    }
   else if(tableView==self.marketTableView)
    {
        return 66;
    }
    else
    {
        return 0;
    }
    
}
- (IBAction)addSymbolBtn:(id)sender {
    if(segmentedControl.selectedSegmentIndex==0)
    {
        
       SearchSymbolView  * searchView = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchSymbolView"];
        [self.navigationController pushViewController:searchView animated:YES];
    }
    else if(segmentedControl.selectedSegmentIndex==1)
    {
        
        CustomPortfolioView  * customView = [self.storyboard instantiateViewControllerWithIdentifier:@"CustomPortfolioView"];
        [self.navigationController pushViewController:customView animated:YES];
    }
}
- (IBAction)onModifySaveTap:(id)sender {
    @try
    {
    [self.modifyAvgPriceLbl resignFirstResponder];
    [self.modifyQtyLbl resignFirstResponder];
    if(self.modifyQtyLbl.text.length>0&&self.modifyAvgPriceLbl.text.length>0)
    {
    self.modifyBgView.hidden=YES;
    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
    NSDictionary *oldDict = (NSDictionary *)[delegate2.userMarketWatchCustom objectAtIndex:modifyObjectIndex];
    [newDict addEntriesFromDictionary:oldDict];
    [newDict setObject:self.modifyQtyLbl.text forKey:@"quantity"];
    [newDict setObject:self.modifyAvgPriceLbl.text forKey:@"averageprice"];
    [delegate2.userMarketWatchCustom replaceObjectAtIndex:modifyObjectIndex withObject:newDict];
    [self instruments];
    }
    else
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Please fill all fields" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

}

-(void)modifyAction:(UIButton*)sender
{
    @try
    {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.customPortfolioTblView];
    NSIndexPath *indexPath = [self.customPortfolioTblView indexPathForRowAtPoint:buttonPosition];
    //NSLog(@"Index path:%ld",(long)indexPath.row);
    modifyObjectIndex=indexPath.row;
    DematCell *cell = [self.customPortfolioTblView cellForRowAtIndexPath:indexPath];
    self.modifyCompanyLbl.text=cell.companyLabel.text;
    self.modifyQtyLbl.text=cell.QTYLabel.text;
    self.modifyAvgPriceLbl.text=cell.avgPriceLbl.text;
    self.modifyBgView.hidden=NO;
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

}
@end
