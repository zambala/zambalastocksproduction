//
//  NewProfileTableViewCell.m
//  betazambalafollower
//
//  Created by zenwise technologies on 23/03/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "NewProfileTableViewCell.h"

@implementation NewProfileTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.oLabel.layer.cornerRadius=12.5;
    self.oLabel.clipsToBounds = YES;
    
    self.stLabel.layer.cornerRadius=12.5;
    self.stLabel.clipsToBounds = YES;
    
//    self.buySellButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
//    self.buySellButton.layer.shadowOffset = CGSizeMake(0, 5.0f);
//    self.buySellButton.layer.shadowOpacity = 5.0f;
//    self.buySellButton.layer.shadowRadius = 4.0f;
    self.buySellButton.layer.cornerRadius=4.1f;
    self.buySellButton.layer.masksToBounds = NO;
    self.durationLbl.layer.cornerRadius=self.durationLbl.frame.size.width/2;
    self.durationLbl.clipsToBounds=YES;
    
    self.subView.layer.cornerRadius=10.0f;
    self.subView.layer.borderWidth = 1.0f;
    self.subView.layer.borderColor = [[UIColor colorWithRed:(99.0)/255 green:(99.0)/255 blue:(99.0)/255 alpha:1.0] CGColor];
    self.openLbl.layer.borderWidth = 1.0f;
    
    self.closeLbl.layer.cornerRadius=10.0f;
    self.closeLbl.layer.masksToBounds = NO;
    
    self.openLbl.layer.cornerRadius=10.0f;
    self.openLbl.layer.masksToBounds = NO;
    
 
    self.closeLbl.layer.borderWidth = 1.0f;
    self.closeLbl.layer.borderColor = [[UIColor colorWithRed:(87.0)/255 green:(160.0)/255 blue:(55.0)/255 alpha:1.0] CGColor];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
