//
//  NewPremimumServicesMainScreen.m
//  PremiumServices
//
//  Created by zenwise mac 2 on 7/13/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "NewPremimumServicesMainScreen.h"
#import "NewExploreMonksViewController.h"
#import "AppDelegate.h"

@interface NewPremimumServicesMainScreen ()
{
    AppDelegate * delegate1;
}

@end

@implementation NewPremimumServicesMainScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
     self.navigationItem.title = @"Premium Services";
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
     [self.navigationController setNavigationBarHidden:NO animated:YES];

    self.navigationItem.leftBarButtonItem.title=@" ";
    self.premiumAdvicesView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.premiumAdvicesView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.premiumAdvicesView.layer.shadowOpacity = 1.0f;
    self.premiumAdvicesView.layer.shadowRadius = 4.2f;
    self.premiumAdvicesView.layer.cornerRadius=1.0f;
    self.premiumAdvicesView.layer.masksToBounds = NO;
    
    
    
    self.signalsView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.signalsView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.signalsView.layer.shadowOpacity = 1.0f;
    self.signalsView.layer.shadowRadius = 4.2f;
    self.signalsView.layer.cornerRadius=1.0f;
    self.signalsView.layer.masksToBounds = NO;
    
    
    self.dealerModeView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.dealerModeView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.dealerModeView.layer.shadowOpacity = 1.0f;
    self.dealerModeView.layer.shadowRadius = 4.2f;
    self.dealerModeView.layer.cornerRadius=1.0f;
    self.dealerModeView.layer.masksToBounds = NO;
    
    
//    [self.premiumAdvicesButton addTarget:self action:@selector(onButtonsTap:) forControlEvents:UIControlEventTouchUpInside];
//    
//    [self.signalsButton addTarget:self action:@selector(onButtonsTap:) forControlEvents:UIControlEventTouchUpInside];
//    
//    [self.dealerModeButton addTarget:self action:@selector(onButtonsTap:) forControlEvents:UIControlEventTouchUpInside];
}
-(void)viewWillAppear:(BOOL)animated
{
     [super viewWillAppear:YES];
     [self.navigationController setNavigationBarHidden:NO animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)onButtonsTap:(UIButton*)sender
{
    NewExploreMonksViewController * exploreMonks = [self.storyboard instantiateViewControllerWithIdentifier:@"NewExploreMonksViewController"];
    
    [self.navigationController pushViewController:exploreMonks animated:YES];
}
- (IBAction)onPremiumAdvicesButtonTap:(id)sender {
    
    delegate1.subscriptionId=@"2";
    NewExploreMonksViewController * exploreMonks = [self.storyboard instantiateViewControllerWithIdentifier:@"NewExploreMonksViewController"];
    
    [self.navigationController pushViewController:exploreMonks animated:YES];
}

- (IBAction)onSignalsButtonTap:(id)sender {
    delegate1.subscriptionId=@"3";
    NewExploreMonksViewController * exploreMonks = [self.storyboard instantiateViewControllerWithIdentifier:@"NewExploreMonksViewController"];
    
    [self.navigationController pushViewController:exploreMonks animated:YES];
}
- (IBAction)onDealerModeButtonTap:(id)sender {
    delegate1.subscriptionId=@"4";
    NewExploreMonksViewController * exploreMonks = [self.storyboard instantiateViewControllerWithIdentifier:@"NewExploreMonksViewController"];
    
    [self.navigationController pushViewController:exploreMonks animated:YES];
}
@end
