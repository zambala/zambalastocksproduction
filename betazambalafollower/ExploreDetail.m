//
//  ExploreDetail.m
//  PremiumServices
//
//  Created by zenwise mac 2 on 12/30/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "ExploreDetail.h"

@interface ExploreDetail ()

@end

@implementation ExploreDetail

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    self.scollView.frame = CGRectMake(self.scollView.frame.origin.x, self.scollView.frame.origin.y, self.scollView.frame.size.width, 900);
    
    [self.scollView setContentSize:CGSizeMake(self.scollView.frame.size.width, 800)];
    
    [self setViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setViews
{
    self.premiumView.backgroundColor = [UIColor whiteColor];
    self.premiumView.layer.cornerRadius = 3.0f;
    self.premiumView.clipsToBounds = YES;
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.premiumView.bounds];
    self.premiumView.layer.masksToBounds = NO;
    self.premiumView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.premiumView.layer.shadowOffset = CGSizeMake(0.0f, 3.0f);
    self.premiumView.layer.shadowOpacity = 0.1f;
    self.premiumView.layer.shadowPath = shadowPath.CGPath;
    
    self.signalsView.backgroundColor = [UIColor whiteColor];
    self.signalsView.layer.cornerRadius = 3.0f;
    self.signalsView.clipsToBounds = YES;
    UIBezierPath *shadowPath1 = [UIBezierPath bezierPathWithRect:self.signalsView.bounds];
    self.signalsView.layer.masksToBounds = NO;
    self.signalsView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.signalsView.layer.shadowOffset = CGSizeMake(0.0f, 3.0f);
    self.signalsView.layer.shadowOpacity = 0.1f;
    self.signalsView.layer.shadowPath = shadowPath1.CGPath;

    
    self.dealerView.backgroundColor = [UIColor whiteColor];
    self.dealerView.layer.cornerRadius = 3.0f;
    self.dealerView.clipsToBounds = YES;
    UIBezierPath *shadowPath2 = [UIBezierPath bezierPathWithRect:self.dealerView.bounds];
    self.dealerView.layer.masksToBounds = NO;
    self.dealerView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.dealerView.layer.shadowOffset = CGSizeMake(0.0f, 3.0f);
    self.dealerView.layer.shadowOpacity = 0.1f;
    self.dealerView.layer.shadowPath = shadowPath2.CGPath;
    
    self.button1.layer.cornerRadius=3.0f;
    self.button1.clipsToBounds = YES;
    UIBezierPath *shadowPath3 = [UIBezierPath bezierPathWithRect:self.button1.bounds];
    self.button1.backgroundColor = [UIColor whiteColor];
    self.button1.layer.borderWidth=0.1f;
    self.button1.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.button1.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    self.button1.layer.shadowPath = shadowPath3.CGPath;

    
    self.button2.layer.cornerRadius=3.0f;
    self.button2.clipsToBounds = YES;
    self.button2.backgroundColor = [UIColor whiteColor];
    self.button2.layer.borderWidth=0.1f;
    self.button2.layer.borderColor =  [UIColor lightGrayColor].CGColor;
    self.button2.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    UIBezierPath *shadowPath4 = [UIBezierPath bezierPathWithRect:self.button2.bounds];
    self.button1.layer.shadowPath = shadowPath4.CGPath;

    self.button3.layer.cornerRadius=3.0f;
    self.button3.clipsToBounds = YES;
    self.button3.backgroundColor = [UIColor whiteColor];
    self.button3.layer.borderWidth=0.1f;
    self.button3.layer.borderColor =  [UIColor lightGrayColor].CGColor;
    self.button3.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    UIBezierPath *shadowPath5 = [UIBezierPath bezierPathWithRect:self.button3.bounds];
    self.button1.layer.shadowPath = shadowPath5.CGPath;
    
    self.button4.layer.cornerRadius=3.0f;
    self.button4.clipsToBounds = YES;
    self.button4.backgroundColor = [UIColor whiteColor];
    self.button4.layer.borderWidth=0.1f;
    self.button4.layer.borderColor =  [UIColor lightGrayColor].CGColor;
    self.button4.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    UIBezierPath *shadowPath6 = [UIBezierPath bezierPathWithRect:self.button4.bounds];
    self.button1.layer.shadowPath = shadowPath6.CGPath;
    
    [_premiumChkBtn setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
    [_premiumChkBtn setImage:[UIImage imageNamed:@"checkSel-box.png"] forState:UIControlStateSelected];
    _premiumChkBtn.selected = NO;
    
    [_signalChkBtn setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
    [_signalChkBtn setImage:[UIImage imageNamed:@"checkSel-box.png"] forState:UIControlStateSelected];
    _signalChkBtn.selected = NO;
    
    [_dealerChkBtn setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
    [_dealerChkBtn setImage:[UIImage imageNamed:@"checkSel-box.png"] forState:UIControlStateSelected];
    _dealerChkBtn.selected = NO;


}

-(IBAction)premiumAction:(id)sender
{
    
}

-(IBAction)signalAction:(id)sender
{
   
    
    
}


-(IBAction)dealerAction:(id)sender
{
    
    
}



- (IBAction)premiumChkBtnAction:(id)sender {
    if (premiumFlag == false)
    {
        _premiumChkBtn.selected = YES;
        premiumFlag = true;
        
    }
    else
    {
        _premiumChkBtn.selected = NO;
        premiumFlag =false;
    }

}

- (IBAction)signalChkBtnAction:(id)sender {
    if (signalFlag == false)
    {
        _signalChkBtn.selected = YES;
        signalFlag = true;
        
    }
    else
    {
        _signalChkBtn.selected = NO;
        signalFlag =false;
    }
}

- (IBAction)dealerChkBtnAction:(id)sender {
    if (dealerFlag == false)
    {
        _dealerChkBtn.selected = YES;
        dealerFlag = true;
        
    }
    else
    {
        _dealerChkBtn.selected = NO;
        dealerFlag =false;
    }

}
@end
