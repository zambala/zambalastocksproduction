//
//  VoucherPopUpViewController.m
//  betazambalafollower
//
//  Created by Zenwise Technologies Private Limited on 25/08/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import "VoucherPopUpViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "VoucherTermsPopupViewController.h"

@interface VoucherPopUpViewController ()

@end

@implementation VoucherPopUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.voucherNameLabel.text = self.voucherCompanyName;
    self.expiryDateLabel.text = self.voucherExpiry;
    self.voucherCodeLabel.text = self.voucherCode;
    self.tandCButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.tandCButton addTarget:self action:@selector(onButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.okayButton addTarget:self action:@selector(onOkayButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.onCopyButtonTap addTarget:self action:@selector(copyButton) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view from its nib.
}

-(void)copyButton
{
    [[UIPasteboard generalPasteboard] setString:self.voucherCodeLabel.text];
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Code copied successfully." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        
        [alert addAction:okAction];
    });
}

-(void)onOkayButtonTap
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

-(void)onButtonTap
{
    VoucherTermsPopupViewController *terms = [[VoucherTermsPopupViewController alloc] initWithNibName:@"VoucherTermsPopupViewController" bundle:nil];
    terms.termsAndConditionsString = self.voucherTerms;
    [self presentPopupViewController:terms animationType: MJPopupViewAnimationFade];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
