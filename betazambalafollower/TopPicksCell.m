//
//  TopPicksCell.m
//  PayPage
//
//  Created by zenwise mac 2 on 12/22/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "TopPicksCell.h"

@implementation TopPicksCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
//    self.buySellButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
//    self.buySellButton.layer.shadowOffset = CGSizeMake(0, 5.0f);
//    self.buySellButton.layer.shadowOpacity = 5.0f;
//    self.buySellButton.layer.shadowRadius = 4.0f;
    self.buySellButton.layer.cornerRadius=4.1f;
    self.buySellButton.layer.masksToBounds = NO;
   
    self.adviceType.layer.cornerRadius=10.0f;
    self.adviceType.layer.masksToBounds = NO;
    
    self.adviceType.layer.borderWidth = 1.0f;
    self.adviceType.layer.borderColor = [[UIColor colorWithRed:(87.0)/255 green:(160.0)/255 blue:(55.0)/255 alpha:1.0] CGColor];
    self.profileImgView.layer.cornerRadius=self.profileImgView.frame.size.width / 2;
    self.profileImgView.clipsToBounds = YES;
    self.durationLbl.layer.cornerRadius=self.durationLbl.frame.size.width/2;
    self.durationLbl.clipsToBounds=YES;
    
    self.profileImgView.layer.borderWidth = 0.5f;
    self.profileImgView.layer.borderColor = [[UIColor colorWithRed:(2)/255 green:(30)/255 blue:(41)/255 alpha:1.0] CGColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onTopPicksProfileButtonTap:(id)sender {
    
    
}
@end
