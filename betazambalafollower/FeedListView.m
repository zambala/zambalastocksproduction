//
//  FeedListView.m
//  testing
//
//  Created by zenwise mac 2 on 12/19/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "FeedListView.h"
#import "FeedListCell.h"
#import "FeedListDetail.h"
#import "AppDelegate.h"
#import <Mixpanel/Mixpanel.h>

@interface FeedListView ()
{
    AppDelegate * delegate1;
}

@end

@implementation FeedListView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"news_feed_detail_page"];
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    //NSLog(@"_descriptionArr----%@", _descriptionArr);
    
   }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[delegate1.detailNewsDict objectForKey:@"Table"]count];
}

- (FeedListCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    FeedListCell *cell = [self.feedListTbl dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        cell = [[FeedListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
//    cell.imgView.image = [UIImage imageNamed:[_imageArray objectAtIndex:indexPath.row]];
    cell.desLbl.text = [[[delegate1.detailNewsDict objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Heading"];;
    cell.timeLbl.text =[[[delegate1.detailNewsDict objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Newsdate"];
    
    
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    FeedListDetail *view = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedListDetail"];
    
    [self.navigationController pushViewController:view animated:YES];
    
    view.imgString = _imageArray [indexPath.row];
    view.descriptionString = [[[delegate1.detailNewsDict objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Heading"];
    view.timeString = [[[delegate1.detailNewsDict objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Newsdate"];
    view.detailDesString = [[[delegate1.detailNewsDict objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Details"];
    
}





@end
