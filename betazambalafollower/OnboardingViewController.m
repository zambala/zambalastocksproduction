//
//  OnboardingViewController.m
//  NewUI
//
//  Created by Zenwise Technologies on 23/05/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "OnboardingViewController.h"
#import "PersonalDetailsViewController.h"
#import "TabBar.h"
#import "TagEncode.h"
#import "AppDelegate.h"
#import "ViewController1.h"
#import "BrokerViewNavigation.h"


@interface OnboardingViewController ()
{
    AppDelegate * delegate2;
    NSMutableDictionary * localArray;
    UIPageControl *pageControl;
}

@end

@implementation OnboardingViewController

@synthesize centerView,contentLabel;
@synthesize pageIndex,imgFile,txtTitle;

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate2=(AppDelegate *)[[UIApplication sharedApplication]delegate];
   
    

   
    localArray = [[NSMutableDictionary alloc]init];
    pageControl = [UIPageControl appearance];
    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    pageControl.backgroundColor = [UIColor clearColor];
    [self.activityInd startAnimating];
    if( delegate2.onboardcheck==true)
    {
        delegate2.onboardcheck=false;
        
    }
    else
    {
        self.activityView.hidden=YES;
       
    }
    self.backView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5] CGColor];
    self.backView.layer.shadowOffset = CGSizeMake(10, 20);
    self.backView.layer.shadowOpacity = 1;
    self.backView.layer.shadowRadius = 1.0;
    self.backView.layer.shadowOpacity = 3.0f;
    self.backView.layer.masksToBounds = NO;
    self.backView.layer.cornerRadius=40.0f;
    
//
//    self.getStartedButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5] CGColor];
//    self.getStartedButton.layer.shadowOffset = CGSizeMake(10, 20);
//    self.getStartedButton.layer.shadowOpacity = 1;
//    self.getStartedButton.layer.shadowRadius = 1.0;
//    self.getStartedButton.layer.shadowOpacity = 3.0f;
    self.getStartedButton.layer.masksToBounds = NO;
    self.getStartedButton.layer.cornerRadius=30.0f;
    
    self.getStartedButton.hidden=YES;
    
    self.centerView.image = [UIImage imageNamed:self.imgFile];
    self.contentLabel.text = self.txtTitle;
    if([self.txtTitle containsString:@"news"])
    {
        self.getStartedButton.hidden=NO;
        pageControl.hidden=YES;
    }else
    {
        self.getStartedButton.hidden=YES;
       pageControl = [UIPageControl appearance];
        pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
        pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
        pageControl.backgroundColor = [UIColor clearColor];
    }
    
    [self.getStartedButton addTarget:self action:@selector(onButtonTap) forControlEvents:UIControlEventTouchUpInside];
    pageControl.enabled=false;
    
    // Do any additional setup after loading the view.
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
     [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}




-(void)onButtonTap
{
    PersonalDetailsViewController * personal = [self.storyboard instantiateViewControllerWithIdentifier:@"PersonalDetailsViewController"];
    [self presentViewController:personal animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



@end
