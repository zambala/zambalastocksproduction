//
//  RedeemTermsViewController.m
//  betazambalafollower
//
//  Created by Zenwise Technologies Private Limited on 20/08/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import "RedeemTermsViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+WebCache.h"
#import "AppDelegate.h"
#import "RedeemCodeViewController.h"
#import "WalletView.h"
@import Mixpanel;


@interface RedeemTermsViewController ()
{
     UIToolbar * toolbar;
    AppDelegate * delegate;
    NSString * redeemPoints;
    NSString * redeemAmount;
    NSString * voucherCode;
    NSString * voucherName;
    NSString * voucherExpiry;
    
    
    int zambalaPointInt;
    float rupeeInt;
    float finalPointValue;
    float minValueInt;
    float finalMinPointValue;
    
    NSString * value;
    
    NSString * backCheck;
}

@end

@implementation RedeemTermsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"redeem_brand_page"];
    
    self.doneView.hidden=YES;
    self.pickerView.hidden=YES;
    self.activityIndicator.hidden=YES;
    self.topView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16] CGColor];
    self.topView.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    self.topView.layer.shadowOpacity = 1.0f;
    self.topView.layer.shadowRadius = 2.0f;
    self.topView.layer.cornerRadius=5.0f;
    self.topView.layer.masksToBounds = NO;
    //self.topView.clipsToBounds=YES;
    
    
    self.popUpView.hidden=YES;
    self.popUpBGView.layer.cornerRadius=5.0f;
    self.okayButton.layer.cornerRadius = 5.0f;
    self.imageBGView.layer.cornerRadius = self.imageBGView.layer.frame.size.width/2;
    
    self.denominationButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16] CGColor];
    self.denominationButton.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    self.denominationButton.layer.shadowOpacity = 1.0f;
    self.denominationButton.layer.shadowRadius = 2.0f;
    self.denominationButton.layer.cornerRadius=5.0f;
    self.denominationButton.layer.masksToBounds = NO;
    //self.denominationButton.clipsToBounds=YES;
    
    backCheck = @"back";
    
    self.redeemButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16] CGColor];
    self.redeemButton.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    self.redeemButton.layer.shadowOpacity = 1.0f;
    self.redeemButton.layer.shadowRadius = 2.0f;
    self.redeemButton.layer.cornerRadius=20.0f;
    self.redeemButton.layer.masksToBounds = NO;
   // self.denominationButton.clipsToBounds=NO;
    
    
    zambalaPointInt = [self.zambalaPointValue intValue];
    rupeeInt = [self.rupeeString floatValue];
    minValueInt = [self.minZambalaPoints floatValue];
    finalMinPointValue = [delegate.rewardPoints floatValue]-minValueInt;
    finalPointValue = (finalMinPointValue*rupeeInt)/zambalaPointInt;
    
    [self assignUI];
    
    [self.backButton addTarget:self action:@selector(onBackButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.redeemButton addTarget:self action:@selector(onReddemTap) forControlEvents:UIControlEventTouchUpInside];
    [self.okayButton addTarget:self action:@selector(onOkayButtonTap) forControlEvents:UIControlEventTouchUpInside];
    if([[self.getListArray objectForKey:@"denomination"] count]==1)
    {
        
    }else
    {
    [self.denominationButton addTarget:self action:@selector(onDenominationTap) forControlEvents:UIControlEventTouchUpInside];
    }
    // Do any additional setup after loading the view.
}

-(void)onOkayButtonTap
{
    WalletView * wallet=[self.storyboard instantiateViewControllerWithIdentifier:@"WalletView"];
    wallet.navigationCheck = @"voucher";
    [self presentViewController:wallet animated:YES completion:nil];
}

-(void)assignUI
{
    @try
    {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString * vaucherImage = [NSString stringWithFormat:@"%@",[self.getListArray objectForKey:@"logo_link"]];
        [self.voucherImageView sd_setImageWithURL:[NSURL URLWithString:vaucherImage] placeholderImage:[UIImage imageNamed:@""]];
        NSString * companyName = [NSString stringWithFormat:@"%@",[self.getListArray objectForKey:@"brand_name"]];
        self.voucherNameLabel.text = companyName;
        NSString * category = [NSString stringWithFormat:@"%@",[self.getListArray objectForKey:@"category"]];
        self.vouchercategorylabel.text = category;
        NSString * validity = [NSString stringWithFormat:@"%@",[self.getListArray objectForKey:@"validity"]];
        self.validityLabel.text = [NSString stringWithFormat:@"Validity: %@",validity];
        NSString * encodedString = [NSString stringWithFormat:@"%@",[self.getListArray objectForKey:@"terms"]];
        NSString *decodeString = [encodedString stringByRemovingPercentEncoding];
        NSString *decodeString1 = [decodeString stringByReplacingOccurrencesOfString:@"+" withString:@" "];
        NSString * denomination = [NSString stringWithFormat:@"%@",[[self.getListArray objectForKey:@"denomination"]objectAtIndex:0]];
        self.termsAndConditionsLabel.text = [self convertHtmlPlainText:decodeString1];
        [self.denominationButton setTitle:denomination forState:UIControlStateNormal];
    });
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(NSString*)convertHtmlPlainText:(NSString*)HTMLString{
    NSData *HTMLData = [HTMLString dataUsingEncoding:NSUTF8StringEncoding];
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithData:HTMLData options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:NULL error:NULL];
    NSString *plainString = attrString.string;
    
    return plainString;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView
numberOfRowsInComponent:(NSInteger)component {
    return [[self.getListArray objectForKey:@"denomination"] count];
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [NSString stringWithFormat:@"%@",[[self.getListArray objectForKey:@"denomination"] objectAtIndex:row]];//Or, your suitable title; like Choice-a, etc.
}
- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    [self.denominationButton setTitle:[NSString stringWithFormat:@"%@  ",[[self.getListArray objectForKey:@"denomination"] objectAtIndex:row]] forState:UIControlStateNormal];
}

-(void)onReddemTap
{
    @try
    {
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    NSString * pointString = [NSString stringWithFormat:@"%@",self.denominationButton.titleLabel.text];
    float pointDenomination = [pointString floatValue];
    if(finalPointValue>pointDenomination||finalPointValue==pointDenomination)
    {
        NSUserDefaults *loggedInUserNew = [NSUserDefaults standardUserDefaults];
        NSString * name = [NSString stringWithFormat:@"%@",[loggedInUserNew objectForKey:@"profilename"]];
        NSString * email = [NSString stringWithFormat:@"%@",[loggedInUserNew objectForKey:@"profileemail"]];
        NSString*number= [loggedInUserNew stringForKey:@"profilemobilenumber"];
//        NSArray* words = [name componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//        NSString* nospacestring = [words componentsJoinedByString:@""];
        NSArray* words1 = [email componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString* nospacestring1 = [words1 componentsJoinedByString:@""];
        NSArray* words2 = [number componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString* nospacestring2 = [words2 componentsJoinedByString:@""];

        value = [NSString stringWithFormat:@"%@",self.denominationButton.titleLabel.text];
        NSString * productID = [NSString stringWithFormat:@"%@",[self.getListArray objectForKey:@"product_id"]];
        
        
        NSDictionary *headers = @{@"Content-Type": @"application/json",
                                  @"authtoken":delegate.zenwiseToken,
                                   @"deviceid":delegate.currentDeviceId,
                                   @"mobilenumber":nospacestring2
                                   };
//        NSDictionary *headers = @{ @"Content-Type": @"application/json",
//                                   @"Authorization":auth,
//                                   };
        if(delegate.voucherMobileNumber.length==0)
        {
            
        }
        NSDictionary *parameters = @{ @"value": value,
                                      @"country": @"IN",
                                      @"product_code":productID,
                                      @"tag": delegate.userID,
                                      @"to_name":name,
                                      @"to_email":nospacestring1,
                                      @"to_phone":nospacestring2
                                      };
        NSString * url = [NSString stringWithFormat:@"%@voucher/redeemvoucher",delegate.baseUrl];
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            if([httpResponse statusCode]==200||[httpResponse statusCode]==201)
                                                            {
                                                                self.vaucherResponseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                redeemAmount = [NSString stringWithFormat:@"%@",[self.vaucherResponseDictionary objectForKey:@"value"]];
                                                                voucherCode= [NSString stringWithFormat:@"%@",[[self.vaucherResponseDictionary objectForKey:@"product"]objectForKey:@"code"]];
                                                                voucherName = [NSString stringWithFormat:@"%@",[[self.vaucherResponseDictionary objectForKey:@"product"]objectForKey:@"product_name"]];
                                                                voucherExpiry = [NSString stringWithFormat:@"%@",[[self.vaucherResponseDictionary objectForKey:@"product"]objectForKey:@"validity_date"]];
                                                                [self updateServer];
                                                            }else
                                                            {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    self.activityIndicator.hidden=YES;
                                                                    [self.activityIndicator stopAnimating];
                                                                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Sorry, we are unable to generate your voucher at the moment. Please try again" preferredStyle:UIAlertControllerStyleAlert];
                                                                    
                                                                    [self presentViewController:alert animated:YES completion:^{
                                                                        
                                                                    }];
                                                                    
                                                                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                        
                                                                    }];
                                                                    
                                                                    
                                                                    [alert addAction:okAction];
                                                                });
                                                                
                                                                
                                                            }
                                                        }
                                                    }];
        [dataTask resume];
    }else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.activityIndicator.hidden=YES;
            [self.activityIndicator stopAnimating];
        });
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Sorry, you don't have enough points to redeem this voucher." preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}

-(void)updateServer
{
    @try
    {
        NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
        NSString*number= [loggedInUser stringForKey:@"profilemobilenumber"];
        NSDictionary *parameters;
        
        NSNumber * redeemPointAmount = [NSNumber numberWithInt:[value intValue]];
        
        int reddemAmountInt = [redeemPointAmount intValue];
        int zambalaPointInt = [self.zambalaPointValue intValue];
        int rupeeInt = [self.rupeeString intValue];
        float finalValue = (reddemAmountInt*zambalaPointInt)/rupeeInt;
        NSNumber * redeemPointNumber = [NSNumber numberWithFloat:finalValue];
        NSString * orderID = [NSString stringWithFormat:@"%@",[self.vaucherResponseDictionary objectForKey:@"id"]];
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"cache-control": @"no-cache",
                                   @"authtoken":delegate.zenwiseToken,
                                   @"deviceid":delegate.currentDeviceId
                                   };
        
//        parameters = @{ @"clientid": delegate.userID,
//                        @"mobilenumber":number,
//                        @"redeempoint":redeemPointNumber,
//                        @"redeemamount":redeemPointAmount,
//                        @"vouchercode":voucherCode,
//                        @"voucherproductname":voucherName,
//                        @"voucherexpiry":voucherExpiry,
//                        @"orderid":orderID
//                        };
        
        parameters = @{ @"clientid": delegate.userID,
                        @"mobilenumber":number,
                        @"redeempoint":redeemPointNumber,
                        @"redeemamount":redeemPointAmount,
                        };
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        
        NSString * url = [NSString stringWithFormat:@"%@coupon/redeemvoucher",delegate.baseUrl];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            if([httpResponse statusCode]==200)
                                                            {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    self.activityIndicator.hidden=YES;
                                                                    [self.activityIndicator stopAnimating];
                                                                    self.popUpView.hidden=NO;
                                                                    NSString * descString = [NSString stringWithFormat:@"Thank you for redeeming your voucher for %@ worth ₹%@. \nYou will receive the voucher in your registered email and mobile number within 24-72hrs.",self.voucherNameLabel.text,value];
                                                                    self.descLabel.text = descString;
                                                                    backCheck = @"noback";
//                                                                    RedeemCodeViewController * redeem = [self.storyboard instantiateViewControllerWithIdentifier:@"RedeemCodeViewController"];
//                                                                    NSString * pointsRedeemedString = [NSString stringWithFormat:@"%d",(int)finalValue];
//                                                                    redeem.pointsRedeemed = pointsRedeemedString;
//                                                                    redeem.voucherValue=redeemAmount;
//                                                                    redeem.voucherCompany=voucherName;
//                                                                    redeem.voucherCode=voucherCode;
//                                                                    redeem.expiryDate= voucherExpiry;
//
//                                                                    [self presentViewController:redeem animated:YES completion:nil];
                                                                });
                                                                
                                                            }
                                                            else
                                                            {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    self.activityIndicator.hidden=YES;
                                                                    [self.activityIndicator stopAnimating];
                                                                });
                                                                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Sorry, we are unable to generate your voucher at the moment. Please try again" preferredStyle:UIAlertControllerStyleAlert];
                                                                
                                                                [self presentViewController:alert animated:YES completion:^{
                                                                    
                                                                }];
                                                                
                                                                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                   
                                                                }];
                                                                
                                                                [alert addAction:okAction];
                                                            }
                                                            
                                                        }
                                                    }];
        [dataTask resume];
    } @catch (NSException *exception) {
        //NSLog(@"Caught Exception");
    } @finally {
        //NSLog(@"Finally");
    }
}
-(void)onDenominationTap
{
    CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
    [self.scrollView setContentOffset:bottomOffset animated:YES];
    self.doneView.hidden=NO;
    self.pickerView.hidden=NO;
    [self.pickerView setBackgroundColor:[UIColor colorWithRed:(248.0/255.0) green:(248.0/255.0) blue:(248.0/255.0) alpha:1.0]];
//    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(onPickerDoneButtonTap)];
//    UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
//    toolbar= [[UIToolbar alloc]initWithFrame:CGRectMake(0,0,self.doneView.frame.size.width,self.doneView.frame.size.height)];
//    //toolbar = [[UIToolbar alloc]init];
//    NSArray *toolbarItems = [NSArray arrayWithObjects:flexible,doneButton,nil];
//    [toolbar setItems:toolbarItems];
    [self.doneButton addTarget:self action:@selector(onPickerDoneButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.doneView addSubview:toolbar];
    self.pickerView.delegate=self;
    self.pickerView.dataSource=self;
}

-(void)onPickerDoneButtonTap
{
    [self.scrollView setContentOffset:CGPointZero animated:YES];
    self.pickerView.hidden=YES;
    self.doneView.hidden=YES;
}

-(void)onBackButtonTap
{
    if([backCheck isEqualToString:@"back"])
    {
    [self dismissViewControllerAnimated:YES completion:nil];
    }else if([backCheck isEqualToString:@"noback"])
    {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
