//
//  MainView.h
//  testing
//
//  Created by zenwise technologies on 21/11/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//



#import <UIKit/UIKit.h>
#import "VCFloatingActionButton.h"
#import "Reachability.h"
#import "PSWebSocket.h"
@import SocketIO;

/*!
 @author Sanka Revanth
 @class MainView
 @discussion This class has different sections(market monks,wisdom garden etc) where the user can navigate to preffered section.
 @superclass SuperClass: UIViewController\n
 @classdesign    Designed using UIView,Button,BarButton,ImageView,Label.
 @coclass    AppDelegate

 */

@interface MainView : UIViewController<floatMenuDelegate,UITabBarDelegate>
- (IBAction)onNotificationButtonTap:(id)sender;/*!<navigates to messages view*/
@property (nonatomic, strong)  SocketManager* manager;
@property (nonatomic, strong)  SocketIOClient* socketio;
@property (weak, nonatomic) IBOutlet UIView *bottomBannerView;
@property (weak, nonatomic) IBOutlet UILabel *bottomBannerDescLbl;

- (IBAction)bottomBannerCloseAction:(id)sender;
@property NSMutableURLRequest * urlRequest;/*!<unused*/
@property NSURLSession * session;/*!<unused*/
@property NSURLSessionDataTask * task1;/*!<unused*/
@property (strong, nonatomic) IBOutlet UIImageView *tickerImageView;/*!<ticker view background image*/
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tickerImgHeightConstraint;
@property (strong, nonatomic) IBOutlet UIView *mainTickerView;/*!<ticker view (showing nifty and sensex)*/
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *mainTickerViewHeightConstraint;/*!<ticker view height constant variable*/
@property Reachability * reach;/*!<used for network rechability*/

@property NSMutableURLRequest * urlRequest2;/*!<unused*/
@property NSURLSession * session2;/*!<unused*/
@property NSURLSessionDataTask * task3;/*!<unused*/

@property NSMutableURLRequest * urlRequest3;/*!<unused*/
@property NSURLSession * session3;/*!<unused*/
@property NSURLSessionDataTask * task4;/*!<unused*/

@property (weak, nonatomic) IBOutlet UILabel *lastPrice;/*!<nifty LTP */
@property (weak, nonatomic) IBOutlet UILabel *changePercent;/*!<nifty change percentage*/
@property UITableView * dummyTable;

@property (strong, nonatomic) VCFloatingActionButton *addButton;/*!<floating button*/
@property NSTimer * timer;/*!<unused*/
@property (strong, nonatomic) IBOutlet UIButton *openAccountButton;/*!<it navigates to open account view*/

@property (weak, nonatomic) IBOutlet UIImageView *openAccountIndia;

@property (weak, nonatomic) IBOutlet UILabel *openAcountlbl;

@property (weak, nonatomic) IBOutlet UILabel *sensexLastPrice;/*!<sensex LTP */
@property (weak, nonatomic) IBOutlet UILabel *sensexChangePercent;/*!<sensex change percentage*/

@property (strong, nonatomic) IBOutlet UIView *openEAccountView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *openEAccountViewHeightConstraint;/*!<open account button height variable*/

@property (strong, nonatomic) IBOutlet UIView *niftyView;/*!<nifty view (showing LTP and change percentage*/

@property (strong, nonatomic) IBOutlet UIView *sensexView;/*!<sensex view (showing LTP and change percentage*/
@property (strong, nonatomic) IBOutlet UIButton *marketmonksBtn;/*!<it navigates to marketmonks page*/

@property (strong, nonatomic) IBOutlet UIBarButtonItem *profileImgBtn;/*!<unused variable*/

@property (strong, nonatomic) IBOutlet UIButton *wisdomGardenButton;/*!<it navigates to wisdomgarden page*/
@property (strong, nonatomic) IBOutlet UIButton *marketWtchButton;/*!<it navigates to marekt watch page*/

@property (strong, nonatomic) IBOutlet UIButton *premiumServiceBtn;/*!<it navigates to premium services page*/
@property (weak, nonatomic) IBOutlet UIImageView *topPicksImgView;


@property (strong, nonatomic) IBOutlet UIButton *mutualFundsBtn;/*!<mutualfund button*/
@property NSMutableDictionary *imageResponseDict; /*!< It stores data of user profile image */

@property (weak, nonatomic) IBOutlet UIButton *usMarketsButton;
@property (weak, nonatomic) IBOutlet UIView *usMarketsView;

//@property (nonatomic, weak) IBOutlet IMBanner* bannerIB;



@property (strong, nonatomic) IBOutlet UIBarButtonItem *brokerName;/*!<label is used to show broker name*/

@property (weak, nonatomic) IBOutlet UIBarButtonItem *userImage;/*!<unused label*/

@property (weak, nonatomic) IBOutlet UILabel *brokerNameLblNew;

@property (weak, nonatomic) IBOutlet UILabel *mutualFundsLbl;/*!<label used to show mutual fund section */
@property NSMutableDictionary * responseDict;/*!< It stores data for guest visibility rules */
@property PSWebSocket * socket;/**< Zerodha websocket variable */

//methods//order

-(void)createClientMethod;
-(void)subMethod;
-(void)unSubMethodNifty;
-(void)unSubSensex;
-(void)onClickingMutualBtn;
-(void) checkNetworkStatus:(NSNotification *)notice;
-(BOOL)networkStatus;
-(void)kiteWebSocket;
-(void)upstoxSocket;
- (void)webSocketDidOpen:(PSWebSocket *)webSocket ;
- (void)webSocket:(PSWebSocket *)webSocket didReceiveMessage:(id)message;
- (void)webSocket:(PSWebSocket *)webSocket didFailWithError:(NSError *)error;
- (void)webSocket:(PSWebSocket *)webSocket didReceiveMessageWithData:(NSData *)data;
-(void)broadRequest;
-(void)newMessage;
- (void)showMainMenu:(NSNotification *)note;
-(void)guestVisibility;
- (IBAction)equityAction:(id)sender;
- (IBAction)derivativesAction:(id)sender;
//new//

@property (weak, nonatomic) IBOutlet UIImageView *monksImgView;

@property (weak, nonatomic) IBOutlet UIImageView *watchImgView;
@property (weak, nonatomic) IBOutlet UIImageView *equitiesImgView;
@property (weak, nonatomic) IBOutlet UIImageView *derivativesImgView;
@property (weak, nonatomic) IBOutlet UIImageView *wealthImgView;
@property (weak, nonatomic) IBOutlet UIImageView *orderImgView;
@property (weak, nonatomic) IBOutlet UIImageView *premiumImgView;
@property (weak, nonatomic) IBOutlet UIImageView *usaImgView;
- (IBAction)mainBannerCloseAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *mainBannerTitle;
@property (weak, nonatomic) IBOutlet UITextView *mainBannerDescLbl;
@property (weak, nonatomic) IBOutlet UIButton *mainBannerPushBtn;
- (IBAction)mainBannerPushBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *mainBannerBgView;

@property (weak, nonatomic) IBOutlet UIView *mainbannerView;

@end
