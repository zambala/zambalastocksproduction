//
//  NewSelectBrokerViewController.h
//  NewUI
//
//  Created by Zenwise Technologies on 22/05/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewSelectBrokerViewController : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIButton *helpButton;
@property (weak, nonatomic) IBOutlet UITextField *searchBrokerTF;
@property (weak, nonatomic) IBOutlet UICollectionView *brokersCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *guestButton;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *continueButtonHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *guestButtonHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *orLabel;
@property (weak, nonatomic) IBOutlet UIView *bgview;
- (IBAction)guestAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *indiaMarketView;
@property (weak, nonatomic) IBOutlet UIView *usMarketView;
@property (weak, nonatomic) IBOutlet UIView *indiaMarketSubView;
@property (weak, nonatomic) IBOutlet UIView *usMarketSubView;

@end
