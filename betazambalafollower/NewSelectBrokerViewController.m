//
//  NewSelectBrokerViewController.m
//  NewUI
//
//  Created by Zenwise Technologies on 22/05/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "NewSelectBrokerViewController.h"
#import "NewSelectBrokerCollectionViewCell.h"
#import "NewBrokerLoginViewController.h"
#import "AppDelegate.h"
#import "TabBar.h"
#import "ViewController1.h"
#import "LoginViewController.h"
#import "TagEncode.h"
@import Mixpanel;
#import "HelpshiftCore.h"
#import "HelpshiftAll.h"
#import "TabMenuView.h"


#define CELL_WIDTH 200
#define CELL_SPACING 10
#define COLLECTIONVIEW_WIDTH 380

@interface NewSelectBrokerViewController ()
{
    AppDelegate * delegate;
    NSIndexPath * storedIndex;
    NSMutableArray * firstName;
    NSArray * filterArray;
    NSMutableArray * newBrokerListArray;
    NSDictionary * brokerDict;
    NSInteger SMEPGiPadViewControllerCellWidth;
NSInteger _numberOfCells;
    NSString * localBrokerName;
    BOOL reloadImage;
    NSString * inputSection;

}

@end

@implementation NewSelectBrokerViewController
{
    NSString * check;
    NSString * indexString;
    UIButton * button;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
   
    SMEPGiPadViewControllerCellWidth= 332;
    reloadImage=true;
    _numberOfCells = 1;
    NSUserDefaults *loggedInUserNew = [NSUserDefaults standardUserDefaults];
    self.searchBrokerTF.delegate=self;
    [loggedInUserNew setObject:@"client" forKey:@"userrole"];
    [loggedInUserNew setObject:@"OTP1" forKey:@"loginActivityStr"];
//    [loggedInUserNew setObject:@"entered" forKey:@"guestflow"];
     [self getAllkeys];
    [loggedInUserNew synchronize];
    inputSection = @"india";
    self.searchBrokerTF.layer.cornerRadius=5.0f;
    self.searchBrokerTF.layer.borderWidth = 1.0f;
    self.searchBrokerTF.layer.borderColor = [[UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0] CGColor];
    self.searchBrokerTF.delegate=self;
//    self.guestButton.layer.cornerRadius=20.0f;
//    self.guestButton.layer.borderWidth = 1.0f;
//    self.guestButton.layer.borderColor = [[UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0] CGColor];
    [self.guestButton setTitle:@"Or continue as Guest  " forState:UIControlStateNormal];
//     self.usMarketView.layer.cornerRadius=5.0f;
//     self.usMarketView.clipsToBounds=YES;
//
//
    self.guestButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16] CGColor];
    self.guestButton.layer.shadowOffset = CGSizeMake(0, -4);
    self.guestButton.layer.shadowOpacity = 1.0f;
    self.guestButton.layer.shadowRadius = 5.0f;
    self.continueButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16] CGColor];
    self.continueButton.layer.shadowOffset = CGSizeMake(0, -4);
    self.continueButton.layer.shadowOpacity = 1.0f;
    self.continueButton.layer.shadowRadius = 5.0f;
    UIBezierPath *maskPath = [UIBezierPath
                              bezierPathWithRoundedRect:self.indiaMarketView.bounds
                              byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft)
                              cornerRadii:CGSizeMake(5, 5)
                              ];
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    
    maskLayer.frame = self.indiaMarketView.bounds;
    maskLayer.path = maskPath.CGPath;
    self.indiaMarketView.layer.mask = maskLayer;
    
    
    UIBezierPath *maskPath1 = [UIBezierPath
                              bezierPathWithRoundedRect:self.usMarketView.bounds
                              byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight)
                              cornerRadii:CGSizeMake(5, 5)
                              ];
    
    CAShapeLayer *maskLayer1 = [CAShapeLayer layer];
    
    maskLayer1.frame = self.usMarketView.bounds;
    maskLayer1.path = maskPath1.CGPath;
    self.usMarketView.layer.mask = maskLayer1;
    
//    self.indiaMarketView.layer.cornerRadius=5.0f;
//    self.indiaMarketView.clipsToBounds=YES;
    
    self.continueButton.hidden=YES;
//    self.continueButtonHeightConstraint.constant=0;
//    self.continueButton.layer.cornerRadius=20.0f;
    
    self.brokersCollectionView.allowsMultipleSelection=NO;
    [self.searchBrokerTF resignFirstResponder];
    check=@"selected";
    [self.helpButton addTarget:self action:@selector(onHelpButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.continueButton addTarget:self action:@selector(onContinueTap) forControlEvents:UIControlEventTouchUpInside];
   
    
    UITapGestureRecognizer *indiaTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onIndiaTap)];
    indiaTap.numberOfTapsRequired = 1;
    indiaTap.numberOfTouchesRequired = 1;
    self.indiaMarketView.userInteractionEnabled = YES;
    [self.indiaMarketView addGestureRecognizer:indiaTap];
    
    UITapGestureRecognizer *USTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onUSTap)];
    USTap.numberOfTapsRequired = 1;
    USTap.numberOfTouchesRequired = 1;
    self.usMarketView.userInteractionEnabled = YES;
    [self.usMarketView addGestureRecognizer:USTap];
//    [self onIndiaTap];
    // Do any additional setup after loading the view.
    
    
    //guest//
    
//    delegate.loginActivityStr=@"OTP1";
//    NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
//    [loggedInUser setObject:delegate.userID forKey:@"userid"];
//    [loggedInUser setObject:delegate.loginActivityStr forKey:@"loginActivityStr"];
 
    UITapGestureRecognizer *bgTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onBgTap)];
    bgTap.numberOfTapsRequired = 1;
    bgTap.numberOfTouchesRequired = 1;
    self.bgview.userInteractionEnabled = YES;
    [self.bgview addGestureRecognizer:bgTap];
    
    
     
}
-(void)onBgTap
{
    if(firstName.count>0)
    {
//    NSString * str=[NSString stringWithFormat:@"%@",[firstName objectAtIndex:0]];
//    if([str containsString:@"Choice Trade"])
//    {
//
//    }
//    else
//    {
    storedIndex=nil;
    [self.brokersCollectionView reloadData];
    self.guestButton.hidden=NO;
    self.continueButton.hidden=YES;
//    }
    }
    else
    {
        storedIndex=nil;
        [self.brokersCollectionView reloadData];
        self.guestButton.hidden=NO;
        self.continueButton.hidden=YES;
    }
}


//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
////    NSInteger numberOfCells = self.view.frame.size.width / SMEPGiPadViewControllerCellWidth;
////    NSInteger edgeInsets = (self.view.frame.size.width - (numberOfCells * SMEPGiPadViewControllerCellWidth)) / (numberOfCells + 1);
////    return UIEdgeInsetsMake(0, edgeInsets, 0, edgeInsets);
//
//    NSInteger viewWidth = COLLECTIONVIEW_WIDTH;
//    NSInteger totalCellWidth = CELL_WIDTH * _numberOfCells;
//    NSInteger totalSpacingWidth = CELL_SPACING * (_numberOfCells -1);
//
//    NSInteger leftInset = (viewWidth - (totalCellWidth + totalSpacingWidth)) / 2;
//    NSInteger rightInset = leftInset;
//
//    return UIEdgeInsetsMake(0, leftInset, 0, rightInset);
//}
-(void)onIndiaTap
{
    inputSection = @"india";
    self.searchBrokerTF.enabled=YES;
    storedIndex=nil;
    localBrokerName=@"";
    self.searchBrokerTF.hidden=NO;
    self.continueButton.hidden=YES;
//    self.continueButtonHeightConstraint.constant=0;
    self.orLabel.hidden=NO;
    self.guestButton.hidden=NO;
//    self.guestButtonHeightConstraint.constant=43;
    [self.indiaMarketView setBackgroundColor:[UIColor colorWithRed:(219/255.0) green:(219/255.0) blue:(219/255.0) alpha:1]];
    [self.indiaMarketSubView setBackgroundColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1]];
    [[UINavigationBar appearance] setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed:(6/255.0) green:(31/255.0) blue:(43/255.0) alpha:1]];
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:(6/255.0) green:(31/255.0) blue:(43/255.0) alpha:1]];
    delegate.usCheck=@"ind";
    [self.usMarketView setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(242/255.0) blue:(242/255.0) alpha:1]];
    [self.usMarketSubView setBackgroundColor:[UIColor colorWithRed:(211/255.0) green:(211/255.0) blue:(211/255.0) alpha:1]];
    NSMutableArray * localArray=[[NSMutableArray alloc]init];
   
        firstName =[[NSMutableArray alloc]init];
        for(int i=0;i<[[[[[delegate.brokerDetailsDict objectForKey:@"data"] objectForKey:@"data"]objectForKey:@"brokerinfo"]objectForKey:@"clientlogin"]count];i++)
        {
            NSString * name=[NSString stringWithFormat:@"%@",[[[[[[delegate.brokerDetailsDict objectForKey:@"data"] objectForKey:@"data"]objectForKey:@"brokerinfo"]objectForKey:@"clientlogin"]objectAtIndex:i]objectForKey:@"brokername"]];
          
            if([name containsString:@"Choice Trade"])
            {

            }
            else
            {
                  [firstName addObject:name];
                  [localArray addObject:[[[[[delegate.brokerDetailsDict objectForKey:@"data"] objectForKey:@"data"]objectForKey:@"brokerinfo"]objectForKey:@"clientlogin"]objectAtIndex:i]];
            }
        }
    
       brokerDict=@{@"data":@{@"data":@{@"brokerinfo":@{@"clientlogin":localArray}}}};
    self.brokersCollectionView.delegate=self;
    self.brokersCollectionView.dataSource=self;
    [self.brokersCollectionView reloadData];
    
}

-(void)onUSTap
{
    inputSection = @"usa";
    self.searchBrokerTF.text=@"";
    [self.searchBrokerTF resignFirstResponder];
      self.searchBrokerTF.enabled=NO;
    storedIndex=nil;
    self.searchBrokerTF.hidden=YES;
    self.continueButton.hidden=YES;
//    self.continueButtonHeightConstraint.constant=40;
    self.orLabel.hidden=YES;
    self.guestButton.hidden=NO;
//    self.guestButtonHeightConstraint.constant=0;
    NSDictionary * localDict;
   
    [self.usMarketView setBackgroundColor:[UIColor colorWithRed:(219/255.0) green:(219/255.0) blue:(219/255.0) alpha:1]];
    [self.usMarketSubView setBackgroundColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1]];
    
    [self.indiaMarketView setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(242/255.0) blue:(242/255.0) alpha:1]];
    [self.indiaMarketSubView setBackgroundColor:[UIColor colorWithRed:(211/255.0) green:(211/255.0) blue:(211/255.0) alpha:1]];
    NSMutableArray * localArray=[[NSMutableArray alloc]init];
    firstName =[[NSMutableArray alloc]init];
    for(int i=0;i<[[[[[delegate.brokerDetailsDict objectForKey:@"data"] objectForKey:@"data"]objectForKey:@"brokerinfo"]objectForKey:@"clientlogin"]count];i++)
    {
        NSString * name=[NSString stringWithFormat:@"%@",[[[[[[delegate.brokerDetailsDict objectForKey:@"data"] objectForKey:@"data"]objectForKey:@"brokerinfo"]objectForKey:@"clientlogin"]objectAtIndex:i]objectForKey:@"brokername"]];
        
        if([name containsString:@"Choice Trade"])
        {
            [firstName addObject:name];
            [localArray addObject:[[[[[delegate.brokerDetailsDict objectForKey:@"data"] objectForKey:@"data"]objectForKey:@"brokerinfo"]objectForKey:@"clientlogin"]objectAtIndex:i]];



            localBrokerName=[[localArray objectAtIndex:0] objectForKey:@"brokername"];
            delegate.brokerInfoDict=[[localArray objectAtIndex:0] mutableCopy];
        }
        else
        {

        }
    }
    
    brokerDict=@{@"data":@{@"data":@{@"brokerinfo":@{@"clientlogin":localArray}}}};
    self.brokersCollectionView.delegate=self;
    self.brokersCollectionView.dataSource=self;
    [self.brokersCollectionView reloadData];
}


-(void)viewDidAppear:(BOOL)animated
{
    
    reloadImage=true;
    [self onIndiaTap];
//    delegate.loginActivityStr=@"OTP1";
    
//    [loggedInUserNew setObject:delegate.loginActivityStr forKey:@"loginActivityStr"];
//
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
   
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    @try
    {
    if(self.searchBrokerTF.text.length>0)
    {
        if(newBrokerListArray.count>0)
        {
        return  [newBrokerListArray count];
        }
    }
    else
    {
        return [[[[[brokerDict objectForKey:@"data"] objectForKey:@"data"]objectForKey:@"brokerinfo"]objectForKey:@"clientlogin"] count];
        
    }
    return 0;
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
    NewSelectBrokerCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NewSelectBrokerCollectionViewCell" forIndexPath:indexPath];
    NSDictionary * local;
   
    if(self.searchBrokerTF.text.length>0)
    {
          local=[newBrokerListArray objectAtIndex:indexPath.row];
    }
    else
    {
         local=[[[[[brokerDict objectForKey:@"data"] objectForKey:@"data"]objectForKey:@"brokerinfo"]objectForKey:@"clientlogin"]objectAtIndex:indexPath.row];
        
    }
    
    cell.brokerImageButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    cell.brokerImageButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    cell.brokerImageButton.layer.shadowOpacity = 1.0f;
    cell.brokerImageButton.layer.shadowRadius = 1.0f;
    cell.brokerImageButton.layer.borderWidth = 0.0f;
    cell.brokerImageButton.layer.borderColor = [[UIColor clearColor] CGColor];
//     cell.brokerImageButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
//     cell.brokerImageButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
//     cell.brokerImageButton.layer.shadowOpacity = 1.0f;
//    cell.brokerImageButton.layer.shadowRadius = 1.0f;
//    cell.brokerImageButton.layer.masksToBounds = YES;
    cell.brokerNameLabel.textColor = [UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0];
    cell.brokerImageButton.tag = indexPath.row;
    cell.brokerImageButton.layer.cornerRadius = cell.brokerImageButton.frame.size.width /2;
    cell.brokerImageButton.clipsToBounds=YES;
    indexString = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
    [cell.brokerImageButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    cell.brokerNameLabel.text=[NSString stringWithFormat:@"%@",[local objectForKey:@"brokername"]];
    UIImage * defaultImage=[UIImage imageNamed:@"dpDefault"];
   
    
//    if([cell.brokerNameLabel.text containsString:@"Choice Trade"])
//    {
//        localBrokerName=cell.brokerNameLabel.text;
//        [cell.brokerImageButton setBackgroundImage:defaultImage forState:UIControlStateNormal];
//        [cell.brokerImageButton setBackgroundImage:defaultImage forState:UIControlStateSelected];
//        cell.brokerImageButton.layer.borderWidth = 3.0f;
//        cell.brokerImageButton.layer.borderColor = [[UIColor colorWithRed:(244.0)/255 green:(187.0)/255 blue:(27.0)/255 alpha:1.0] CGColor];
//        cell.brokerNameLabel.textColor = [UIColor colorWithRed:(244.0)/255 green:(187.0)/255 blue:(27.0)/255 alpha:1.0];
//        [self.continueButton setTitle:[NSString stringWithFormat:@"Continue with %@  ",cell.brokerNameLabel.text] forState:UIControlStateNormal];
//    }
//    else
//    {
        [cell.brokerImageButton setBackgroundImage:defaultImage forState:UIControlStateNormal];
        [cell.brokerImageButton setBackgroundImage:defaultImage forState:UIControlStateSelected];
//    }
    
  
   
    NSURL *url;
    @try {
        url =  [NSURL URLWithString:[NSString stringWithFormat:@"%@",[[local objectForKey:@"about"]objectForKey:@"logourl"]]];
        
    } @catch (NSException *exception) {
        
        
        
    } @finally {
        
    }
    
    
    
    
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if (cell)
                      [cell.brokerImageButton setBackgroundImage:image forState:UIControlStateNormal];
                      [cell.brokerImageButton setBackgroundImage:image forState:UIControlStateSelected];
                });
            }
        }
    }];
    [task resume];
   
   
    
   
  
    
//    if(indexPath.row == selectedIndex)
//    {
//        //show this button as selected by setting different name/color/image for button
//        cell.brokerImageButton.enabled = TRUE;
//    }
//    else
//    {
//        cell.brokerImageButton.enabled = FALSE;
//    }
    //cell.brokerImageButton.selected=YES;
   // button=cell.brokerImageButton;
    
//    if (cell.selected) {
//        cell.brokerImageButton.layer.borderWidth = 3.0f;
//        cell.brokerImageButton.layer.borderColor = [[UIColor colorWithRed:(244.0)/255 green:(187.0)/255 blue:(27.0)/255 alpha:1.0] CGColor];
//        cell.brokerNameLabel.textColor = [UIColor colorWithRed:(244.0)/255 green:(187.0)/255 blue:(27.0)/255 alpha:1.0];
//        check=@"unselected";
//        self.continueButton.hidden=NO;
//        self.continueButtonHeightConstraint.constant=40;
//        self.orLabel.hidden=YES;
//        self.guestButton.hidden=YES;
//        self.guestButtonHeightConstraint.constant=0;
//        // highlight selection
//    }
//    else
//    {
//        cell.brokerImageButton.layer.borderWidth = 0.0f;
//        cell.brokerImageButton.layer.borderColor = [[UIColor clearColor] CGColor];
//        cell.brokerNameLabel.textColor = [UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0];
//        check=@"selected";
//        self.continueButton.hidden=YES;
//        self.continueButtonHeightConstraint.constant=0;
//        self.orLabel.hidden=NO;
//        self.guestButton.hidden=NO;
//        self.guestButtonHeightConstraint.constant=20;
//        // Default color
//    }
//    if([cell.brokerNameLabel.text containsString:@"Choice Trade"])
//    {
//    }
//    else
//
//    {
        if(storedIndex==indexPath)
        {
            
            cell.brokerImageButton.layer.borderWidth = 3.0f;
            cell.brokerImageButton.layer.borderColor = [[UIColor colorWithRed:(244.0)/255 green:(187.0)/255 blue:(27.0)/255 alpha:1.0] CGColor];
            cell.brokerNameLabel.textColor = [UIColor colorWithRed:(244.0)/255 green:(187.0)/255 blue:(27.0)/255 alpha:1.0];
        }
        else
        {
            cell.brokerImageButton.layer.borderWidth = 0.0f;
            cell.brokerImageButton.layer.borderColor = [[UIColor clearColor] CGColor];
            cell.brokerNameLabel.textColor = [UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0];
        }

    
//    }
    return cell;
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)onContinueTap
{
    @try
    {
    if(delegate.brokerInfoDict)
    {
        NSString * brokername=[NSString stringWithFormat:@"%@",[delegate.brokerInfoDict objectForKey:@"brokername"]];
     
        
        if([brokername isEqualToString:@"Zerodha"]||[brokername isEqualToString:@"Upstox"])
        {
            ViewController1 *view = [self.storyboard instantiateViewControllerWithIdentifier:@"view"];
            [self.navigationController pushViewController:view animated:YES];
        }else if ([brokername containsString:@"Choice"])
        {
                    UIImage *NavigationPortraitBackground= [UIImage imageNamed:@"topUS-1"];
                    [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
                    [[UINavigationBar appearance] setTranslucent:NO];
                    delegate.usCheck=@"USA";
                    delegate.usNavigationCheck=@"USA";
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"USA" bundle:nil];
                    LoginViewController * usHome = [storyboard instantiateInitialViewController];
                    [self presentViewController:usHome animated:YES completion:nil];
            
        }
        else
        {
            delegate.mtBaseUrl=[NSString stringWithFormat:@"%@",[delegate.brokerInfoDict objectForKey:@"ip"]];
            delegate.ltpServerURL=[NSString stringWithFormat:@"%@",[[[[delegate.brokerInfoDict objectForKey:@"connectioninfo"] objectForKey:@"data"] objectAtIndex:0] objectForKey:@"ip"]];
          NewBrokerLoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"NewBrokerLoginViewController"];
          [self.navigationController pushViewController:login animated:YES];
        }
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}


-(void)onButtonTap:(UIButton*)sender
{
    @try
    {
//
    CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
    [self.scrollView setContentOffset:bottomOffset animated:YES];
    [self.searchBrokerTF resignFirstResponder];
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.brokersCollectionView];
    NSIndexPath *indexPath = [self.brokersCollectionView indexPathForItemAtPoint:buttonPosition];
    //NSLog(@"Index path:%ld",(long)indexPath.row);

    //NSLog(@"Index path:%ld",(long)indexPath.row);
    if(storedIndex)
    {
        if(storedIndex!=indexPath)
        {
         NewSelectBrokerCollectionViewCell *cell = (NewSelectBrokerCollectionViewCell *)[self.brokersCollectionView cellForItemAtIndexPath:storedIndex];

        if(cell==nil)
        {

        }
        else
        {
        cell.brokerImageButton.layer.borderWidth = 0.0f;
        cell.brokerImageButton.layer.borderColor = [[UIColor clearColor] CGColor];
        cell.brokerNameLabel.textColor = [UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0];

        }
        }


    }

     storedIndex=indexPath;
//    NewSelectBrokerCollectionViewCell *cell = [self.brokersCollectionView cellForItemAtIndexPath:indexPath];

    NewSelectBrokerCollectionViewCell *cell = (NewSelectBrokerCollectionViewCell *)[self.brokersCollectionView cellForItemAtIndexPath:indexPath];

//    UIButton *currentButton = (UIButton *)sender;
//    for(UIView *view in self.view.subviews){
//
//        if([view isKindOfClass:[UIButton class]]){
//
//            UIButton *btn = (UIButton *)view;
//            [btn setSelected:NO];
//        }
//    }
//
//    [currentButton setSelected:YES];
//
    if(sender.layer.borderWidth==3.0f)
    {
        storedIndex=nil;
        //deselect the selected button
        sender.layer.borderWidth = 0.0f;
        sender.layer.borderColor = [[UIColor clearColor] CGColor];
        cell.brokerNameLabel.textColor = [UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0];

//        cell.brokerImageButton.layer.borderWidth = 0.0f;
//        cell.brokerImageButton.layer.borderColor = [[UIColor clearColor] CGColor];
//        cell.brokerNameLabel.textColor = [UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0];
        check=@"selected";
        self.continueButton.hidden=YES;
//        self.continueButtonHeightConstraint.constant=0;
        self.orLabel.hidden=NO;
        self.guestButton.hidden=NO;
//        self.guestButtonHeightConstraint.constant=43;

    }
    else if(sender.layer.borderWidth==0.0f)
    {
         //select the tapped button
        sender.layer.borderWidth = 3.0f;
        sender.layer.borderColor = [[UIColor colorWithRed:(244.0)/255 green:(187.0)/255 blue:(27.0)/255 alpha:1.0] CGColor];
        cell.brokerNameLabel.textColor = [UIColor colorWithRed:(244.0)/255 green:(187.0)/255 blue:(27.0)/255 alpha:1.0];

//    cell.brokerImageButton.layer.borderWidth = 3.0f;
//    cell.brokerImageButton.layer.borderColor = [[UIColor colorWithRed:(244.0)/255 green:(187.0)/255 blue:(27.0)/255 alpha:1.0] CGColor];
//    cell.brokerNameLabel.textColor = [UIColor colorWithRed:(244.0)/255 green:(187.0)/255 blue:(27.0)/255 alpha:1.0];
    check=@"unselected";
    self.continueButton.hidden=NO;
        [self.continueButton setTitle:[NSString stringWithFormat:@"Continue with %@  ",cell.brokerNameLabel.text] forState:UIControlStateNormal];
//    self.continueButtonHeightConstraint.constant=40;
    self.orLabel.hidden=YES;
    self.guestButton.hidden=YES;
//    self.guestButtonHeightConstraint.constant=0;
        NSDictionary * localDict;
        if(self.searchBrokerTF.text.length>0)
        {
            if(newBrokerListArray.count>0)
            {
            localDict=[newBrokerListArray objectAtIndex:indexPath.row];
            }
        }
        else
        {
            localDict=[[[[[brokerDict objectForKey:@"data"] objectForKey:@"data"]objectForKey:@"brokerinfo"]objectForKey:@"clientlogin"]objectAtIndex:indexPath.row];
        }
       delegate.brokerInfoDict=[localDict mutableCopy];
//    [self.brokersCollectionView reloadData];
    }
//    if([check isEqualToString:@"selected"])
//    {
//        cell.brokerImageButton.layer.borderWidth = 3.0f;
//        cell.brokerImageButton.layer.borderColor = [[UIColor colorWithRed:(244.0)/255 green:(187.0)/255 blue:(27.0)/255 alpha:1.0] CGColor];
//        cell.brokerNameLabel.textColor = [UIColor colorWithRed:(244.0)/255 green:(187.0)/255 blue:(27.0)/255 alpha:1.0];
//        check=@"unselected";
//        self.continueButton.hidden=NO;
//        self.continueButtonHeightConstraint.constant=40;
//        self.orLabel.hidden=YES;
//        self.guestButton.hidden=YES;
//        self.guestButtonHeightConstraint.constant=0;
//    }else if([check isEqualToString:@"unselected"])
//    {
//        cell.brokerImageButton.layer.borderWidth = 0.0f;
//        cell.brokerImageButton.layer.borderColor = [[UIColor clearColor] CGColor];
//        cell.brokerNameLabel.textColor = [UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0];
//        check=@"selected";
//        self.continueButton.hidden=YES;
//        self.continueButtonHeightConstraint.constant=0;
//        self.orLabel.hidden=NO;
//        self.guestButton.hidden=NO;
//        self.guestButtonHeightConstraint.constant=20;
//    }
    
    [self.brokersCollectionView reloadData];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)onHelpButtonTap
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Existing clients of any of the following brokers, please use the login credentials provided by your registered broker at the time of your registration. For any further assistance, please contact your broker." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        UIAlertAction * ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:ok];
        
    });
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
//    NewSelectBrokerCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NewSelectBrokerCollectionViewCell" forIndexPath:indexPath];
//     //UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
//    cell.brokerImageButton.layer.borderWidth = 3.0f;
//    cell.brokerImageButton.layer.borderColor = [[UIColor colorWithRed:(244.0)/255 green:(187.0)/255 blue:(27.0)/255 alpha:1.0] CGColor];
//    cell.brokerNameLabel.textColor = [UIColor colorWithRed:(244.0)/255 green:(187.0)/255 blue:(27.0)/255 alpha:1.0];
//    if(self.searchBrokerTF.text.length>1)
//    {
//        [self onContinueTap];
//    }
//
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
//    if(![localBrokerName containsString:@"Choice Trade"])
//    {
    NewSelectBrokerCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NewSelectBrokerCollectionViewCell" forIndexPath:indexPath];
    cell.brokerImageButton.layer.borderWidth = 0.0f;
    cell.brokerImageButton.layer.borderColor = [[UIColor clearColor] CGColor];
    cell.brokerNameLabel.textColor = [UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0];
//    }
}

- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}
- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
  
//    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
//    [mixpanelMini track:@"scroll_broker_list_page"];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)guestAction:(id)sender {
    @try
    {
  NSArray *  localArray = [[[[delegate.brokerDetailsDict objectForKey:@"data"] objectForKey:@"data"]objectForKey:@"brokerinfo"]objectForKey:@"guestlogin"];
    
    
    delegate.brokerInfoDict=[localArray objectAtIndex:0];
    delegate.ltpServerURL=[NSString stringWithFormat:@"%@",[[[[delegate.brokerInfoDict objectForKey:@"connectioninfo"]objectForKey:@"data"] objectAtIndex:0] objectForKey:@"ip"]];
    NSUserDefaults *loggedInUserNew = [NSUserDefaults standardUserDefaults];
    delegate.userID=[loggedInUserNew objectForKey:@"userid"];
    delegate.loginActivityStr=@"OTP1";
    delegate.brokerNameStr=@"GUEST";
    [loggedInUserNew setObject:@"guest" forKey:@"userrole"];
    [loggedInUserNew setObject:@"GUEST" forKey:@"brokername"];
    [loggedInUserNew setObject:delegate.loginActivityStr forKey:@"loginActivityStr"];
    [loggedInUserNew setObject:@"entered" forKey:@"guestflow"];
    [loggedInUserNew synchronize];
    delegate.zenwiseToken=[loggedInUserNew objectForKey:@"guestaccess"];
    delegate.loginActivityStr=@"OTP1";
    
    //                                                                NewLoginViewController *view = [self.storyboard instantiateViewControllerWithIdentifier:@"NewLoginViewController"];
    //                                                                [self presentViewController:view animated:YES completion:nil];
        
        if([inputSection isEqualToString:@"india"])
        {
        TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
    
        [self presentViewController:tabPage animated:YES completion:nil];
        }
        else if([inputSection isEqualToString:@"usa"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"USA" bundle:nil];
            TabMenuView * usLogin = [storyboard instantiateViewControllerWithIdentifier:@"TabMenuView"];
            // [self.navigationController pushViewController:usLogin animated:YES];
            [self presentViewController:usLogin animated:YES completion:nil];
        }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    @try
    {
    newBrokerListArray=[[NSMutableArray alloc]init];
    NSString * searchStr = [self.searchBrokerTF.text stringByReplacingCharactersInRange:range withString:string];
        if(searchStr.length>0)
        {
       NSPredicate * predicate=[NSPredicate predicateWithFormat:@"self beginswith[c] %@",searchStr];
       //NSLog(@"%@",predicate);
    
  
      filterArray =[[NSArray alloc]init];
        
    filterArray=[firstName  filteredArrayUsingPredicate:predicate];
    
    for(int i=0;i<filterArray.count;i++)
    {
        NSString * brokername=[filterArray objectAtIndex:i];
        for(int j=0;j<[[[[[brokerDict objectForKey:@"data"] objectForKey:@"data"]objectForKey:@"brokerinfo"]objectForKey:@"clientlogin"]count];j++)
        {
            NSString * brokername2=[NSString stringWithFormat:@"%@",[[[[[[brokerDict objectForKey:@"data"] objectForKey:@"data"]objectForKey:@"brokerinfo"]objectForKey:@"clientlogin"]objectAtIndex:j]objectForKey:@"brokername"]];
            
            if([brokername isEqualToString:brokername2])
            {
                [newBrokerListArray addObject:[[[[[brokerDict objectForKey:@"data"] objectForKey:@"data"]objectForKey:@"brokerinfo"]objectForKey:@"clientlogin"]objectAtIndex:j]];
            }
        }
    }
        
    [self.brokersCollectionView reloadData];
    
    return YES;
        }
    else
    {
        self.searchBrokerTF.text=@"";
        [self.brokersCollectionView reloadData];
        return YES;
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if([localBrokerName containsString:@"Choice Trade"])
    {
        CGFloat totalCellWidth = 133 * [[[[[brokerDict objectForKey:@"data"] objectForKey:@"data"]objectForKey:@"brokerinfo"]objectForKey:@"clientlogin"] count];
        CGFloat totalSpacingWidth = 0 * (((float)[[[[[brokerDict objectForKey:@"data"] objectForKey:@"data"]objectForKey:@"brokerinfo"]objectForKey:@"clientlogin"] count] - 1) < 0 ? 0 :[[[[[brokerDict objectForKey:@"data"] objectForKey:@"data"]objectForKey:@"brokerinfo"]objectForKey:@"clientlogin"] count] - 1);
        CGFloat leftInset = (self.view.frame.size.width-30 - (totalCellWidth + totalSpacingWidth)) / 2;
        CGFloat rightInset = leftInset;
        UIEdgeInsets sectionInset = UIEdgeInsetsMake(0, leftInset, 0, rightInset);
        return sectionInset;
    }
    else

    {
    return  UIEdgeInsetsMake(0,0,0,0);
    }
}
-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    [textField resignFirstResponder];
    
    
    return YES;
}
-(void)getAllkeys
{
    @try
    {
        if(delegate.referralCode.length==0)
        {
            delegate.referralCode=@"";
        }
        TagEncode * enocde=[[TagEncode alloc]init];
        
        enocde.inputRequestString=@"getallkeys";
        
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        
        [inputArray addObject:[NSString stringWithFormat:@"%@follower/postinit?x-access-token=%@",delegate.baseUrl,delegate.zenwiseToken]];
        
        [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
            
            if([[dict objectForKey:@"data"] count]>0)
            {
                
                NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
                
                NSString * number= [loggedInUser stringForKey:@"profilemobilenumber"];
                
                NSString * name= [loggedInUser stringForKey:@"profilename"];
                NSString * email= [loggedInUser stringForKey:@"profileemail"];
                NSString * mixPanelToken;
                NSString * helpShiftToken;
                for (int i=0; i<[[dict objectForKey:@"data"] count]; i++) {
                    NSString * appString = [NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"] objectAtIndex:i] objectForKey:@"externalapp"]];
                    if([appString isEqualToString:@"helpshift"])
                    {
                        helpShiftToken = [NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"key"]];
                    }else if ([appString isEqualToString:@"mixedpanel"])
                    {
                        mixPanelToken = [NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"key"]];
                    }
                }
                Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
//                [Mixpanel sharedInstanceWithToken:mixPanelToken];
//                [[Mixpanel sharedInstance] track:@"Launched"];
                @try
                {
                [mixpanelMini identify:delegate.userID];
               
                if(delegate.mixpanelUserRegistration)
                {
                      [mixpanelMini.people set:delegate.mixpanelUserRegistration];
                }
                }
                @catch (NSException * e) {
                    NSLog(@"Exception: %@", e);
                }
                @finally {
                    NSLog(@"finally");
                }
              
                    if(delegate.mixpanelDeviceToken.length>0)
                    {
                        [mixpanelMini.people addPushDeviceToken:delegate.mixpanelDeviceToken];
                    }
               
                [mixpanelMini track:@"select_broker_page"];
                
//                helpshift//--
                
                [HelpshiftCore initializeWithProvider:[HelpshiftAll sharedInstance]];
                
                
                [HelpshiftCore installForApiKey:helpShiftToken domainName:@"zenwise-technologies.helpshift.com" appID:@"zenwise-technologies_platform_20170726065628934-964b6d1e092c000"];
                
                 [HelpshiftCore registerDeviceToken:delegate.helpShiftData];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                   
                });
                
            }else
            {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Something went wrong please try again." preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self getAllkeys];
                }];
                
                [alert addAction:okAction];
            });
            }
        }];
        
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
    
    
}
        
@end
