//
//  AddAlertPopUp.m
//  PriceAlert
//
//  Created by zenwise mac 2 on 12/28/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "AddAlertPopUp.h"

@interface AddAlertPopUp ()

@end

@implementation AddAlertPopUp

@synthesize LTPRadioBtn, BIDRadioBtn, ASKBtn, lessRadioBtn, greaterRadioBtn;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.MsgTxtField.delegate = self;
    
    [self setBIDRadioBtn];
    
    [self outFocusTextField];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.MsgTxtField resignFirstResponder];
}



-(void)setBIDRadioBtn
{
    [LTPRadioBtn setImage:[UIImage imageNamed:@"radio.png"] forState:UIControlStateNormal];
    [LTPRadioBtn setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateSelected];
    LTPRadioBtn.selected = NO;
    
    [BIDRadioBtn setImage:[UIImage imageNamed:@"radio.png"] forState:UIControlStateNormal];
    [BIDRadioBtn setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateSelected];
    BIDRadioBtn.selected = NO;

    [ASKBtn setImage:[UIImage imageNamed:@"radio.png"] forState:UIControlStateNormal];
    [ASKBtn setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateSelected];
    ASKBtn.selected = NO;

    [lessRadioBtn setImage:[UIImage imageNamed:@"radio.png"] forState:UIControlStateNormal];
    [lessRadioBtn setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateSelected];
    lessRadioBtn.selected = NO;

    [greaterRadioBtn setImage:[UIImage imageNamed:@"radio.png"] forState:UIControlStateNormal];
    [greaterRadioBtn setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateSelected];
    greaterRadioBtn.selected = NO;

}

-(void)inFocusTextField
{
    self.MsgTxtField.leftViewMode = UITextFieldViewModeAlways;
    self.MsgTxtField.font=[UIFont fontWithName:@"Helvetica Neue" size:12];
    self.MsgTxtField.textColor = [UIColor blackColor];
    self.MsgTxtField.backgroundColor = [UIColor clearColor];
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, self.MsgTxtField.frame.size.height - 1, self.MsgTxtField.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = [UIColor darkGrayColor].CGColor;
    [self.MsgTxtField.layer addSublayer:bottomBorder];
}

-(void)outFocusTextField
{
    self.MsgTxtField.leftViewMode = UITextFieldViewModeAlways;
    self.MsgTxtField.font=[UIFont fontWithName:@"Helvetica Neue" size:12];
    self.MsgTxtField.textColor = [UIColor blackColor];
    self.MsgTxtField.backgroundColor = [UIColor clearColor];
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, self.MsgTxtField.frame.size.height - 1, self.MsgTxtField.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
    [self.MsgTxtField.layer addSublayer:bottomBorder];
}


-(IBAction)LTPRadioAction:(id)sender
{
    LTPRadioBtn.selected = YES;
    BIDRadioBtn.selected = NO;
    ASKBtn.selected = NO;

    
}
-(IBAction)BIDRadioAction:(id)sender
{
    LTPRadioBtn.selected = NO;
    BIDRadioBtn.selected = YES;
    ASKBtn.selected = NO;
}
-(IBAction)ASKRadioAction:(id)sender
{
    LTPRadioBtn.selected = NO;
    BIDRadioBtn.selected = NO;
    ASKBtn.selected = YES;
}

-(IBAction)lessRadioAction:(id)sender
{
    lessRadioBtn.selected = YES;
    greaterRadioBtn.selected = NO;
    
}
-(IBAction)greaterRadioAction:(id)sender
{
    lessRadioBtn.selected = NO;
    greaterRadioBtn.selected = YES;
 
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField

{
    [self inFocusTextField];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self outFocusTextField];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > self.MsgTxtField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [self.MsgTxtField.text length] + [string length] - range.length;
    return newLength <= 300;
}


@end
