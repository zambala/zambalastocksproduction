//
//  EquitiesCell1.h
//  testing
//
//  Created by zenwise technologies on 26/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EquitiesCell1 : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *eqitiesImg1;


@property (weak, nonatomic) IBOutlet UIImageView *equitiesImg2;

@property (weak, nonatomic) IBOutlet UIImageView *profileImg;
@property (strong, nonatomic) IBOutlet UIImageView *tickImageView;

@property (strong, nonatomic) IBOutlet UIButton *buySellButton;

@property (weak, nonatomic) IBOutlet UIButton *ordersBtn;



@property (weak, nonatomic) IBOutlet UILabel *durationLbl;

@property (weak, nonatomic) IBOutlet UILabel *organizationNameLbl;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *expertXconstraint;




- (IBAction)leaderProfileButton:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *leaderProfileAction;




@property (weak, nonatomic) IBOutlet UIImageView *premiumImg;
@property (weak, nonatomic) IBOutlet UILabel *sourceLabel;

@property (weak, nonatomic) IBOutlet UILabel *analystName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *analystNameHeightConstant;
//new ui//
@property (weak, nonatomic) IBOutlet UILabel *valueChangeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateAndTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *ltpLabel;
@property (weak, nonatomic) IBOutlet UILabel *changePercentlabel;
@property (weak, nonatomic) IBOutlet UILabel *actedByLabel;
@property (weak, nonatomic) IBOutlet UILabel *sharesSold;
@property (weak, nonatomic) IBOutlet UILabel *sourceDataLabel;
@property (weak, nonatomic) IBOutlet UILabel *profileName;
@property (weak, nonatomic) IBOutlet UILabel *originalMessageLabel;
@property (weak, nonatomic) IBOutlet UILabel *originalLtp;
@property (weak, nonatomic) IBOutlet UILabel *originalPL;
@property (weak, nonatomic) IBOutlet UIButton *originalAdviceTypeButton;
@property (weak, nonatomic) IBOutlet UIButton *adviceButton;

@property (weak, nonatomic) IBOutlet UIImageView *leaderImg;

@property (weak, nonatomic) IBOutlet UIView *originalAdviceView;
@property (weak, nonatomic) IBOutlet UILabel *originalDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *originalActedBy;
@property (weak, nonatomic) IBOutlet UILabel *originalSharesLbl;

@end
