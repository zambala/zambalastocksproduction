//
//  ViewController.h
//  ProfileFollower
//
//  Created by guna on 16/03/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"
#import "MKSHorizontalLineProgressView.h"
#import "KNCirclePercentView.h"
#import "KATCircularProgress.h"


@interface NewProfile : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *sebiNoLbl;
@property (weak, nonatomic) IBOutlet UITableView *profileTableView;
@property (strong, nonatomic) IBOutlet KNCirclePercentView *hitRateCircluarView;

@property (strong, nonatomic) IBOutlet UILabel *leaderNameLbl;
@property (strong, nonatomic) IBOutlet MKSHorizontalLineProgressView *dayTradeView;
@property (strong, nonatomic) IBOutlet MKSHorizontalLineProgressView *shortTermView;
@property (weak, nonatomic) IBOutlet UIView *statsView;

@property (strong, nonatomic) IBOutlet MKSHorizontalLineProgressView *longTermView;
@property (weak, nonatomic) IBOutlet UIView *gainLossView;
@property (weak, nonatomic) IBOutlet UIView *performanceView;

@property (weak, nonatomic) IBOutlet UIView *successRateView;
@property (strong, nonatomic) IBOutlet UIButton *followBtn;
@property (strong, nonatomic) IBOutlet UILabel *averageCalls;
@property (strong, nonatomic) IBOutlet UILabel *maximumGain;
@property (weak, nonatomic) IBOutlet UILabel *winningLbl;
@property (weak, nonatomic) IBOutlet UILabel *lossingLbl;

@property (strong, nonatomic) IBOutlet UILabel *maximumLoss;
@property (strong, nonatomic) IBOutlet UILabel *dayTrade;

@property (strong, nonatomic) IBOutlet UILabel *shortTerm;
@property (strong, nonatomic) IBOutlet UILabel *longTerm;
@property (strong, nonatomic) IBOutlet UILabel *followerCount;
@property (strong, nonatomic) IBOutlet UILabel *gainPercentLabel;
@property (strong, nonatomic) IBOutlet UIButton *followButton;
@property (strong, nonatomic) IBOutlet UITextView *aboutLabel;
@property (strong, nonatomic) IBOutlet HCSStarRatingView *starRatingView;
@property NSMutableDictionary *profileDetailReponseDictionary;
@property (strong, nonatomic) IBOutlet UITextView *aboutLeaderLabel;
@property NSMutableDictionary *recentAdvicesDictionary;
@property (strong, nonatomic) IBOutlet UILabel *segmentsLabel;
@property (strong, nonatomic) IBOutlet UILabel *specializationLabel;

@property (weak, nonatomic) IBOutlet UIView *premiumSerivceBtn;

@property (weak, nonatomic) IBOutlet UIView *feedsBtn;


@property (strong, nonatomic) IBOutlet UIImageView *profileImage;


@property (weak, nonatomic) IBOutlet UILabel *aboutLbl;
@property (weak, nonatomic) IBOutlet UILabel *activeStatusLbl;

@property (weak, nonatomic) IBOutlet UILabel *subscriberCountLbl;

@property (weak, nonatomic) IBOutlet UILabel *feedsActiveLbl;


@property (weak, nonatomic) IBOutlet KATCircularProgress *progressView;
@property (weak, nonatomic) IBOutlet UILabel *overallAccLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainDetailsBgViewHgt;

@property (weak, nonatomic) IBOutlet UILabel *dtLbl;
@property (weak, nonatomic) IBOutlet UILabel *stLbl;
@property (weak, nonatomic) IBOutlet UILabel *ltLbl;

@property (weak, nonatomic) IBOutlet UILabel *DT;
@property (weak, nonatomic) IBOutlet UILabel *ST;
@property (weak, nonatomic) IBOutlet UILabel *LT;
@property (weak, nonatomic) IBOutlet UILabel *successDetailsLbl;
@property (weak, nonatomic) IBOutlet KATCircularProgress *dayTradeProgression;
@property (weak, nonatomic) IBOutlet KATCircularProgress *shortProgression;
@property (weak, nonatomic) IBOutlet KATCircularProgress *longTermProgression;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *aboutLblHgt;
@property (weak, nonatomic) IBOutlet UILabel *sebiIdLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *followBtnWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *premiumServiceBtnHgt;
@property (weak, nonatomic) IBOutlet UIView *leaderOffersView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *offersViewHgt;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *currentAdviceviewHgt;

@end

