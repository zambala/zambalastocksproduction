//
//  ExploreMonks.m
//  PremiumServices
//
//  Created by zenwise mac 2 on 12/30/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "ExploreMonks.h"
#import "ExpMonksCell.h"
#import "ExploreDetail.h"
#import "AppDelegate.h"

@interface ExploreMonks ()
{
    
    AppDelegate * delegate1;
    
}

@end

@implementation ExploreMonks

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    [self.navigationItem.backBarButtonItem setTitle:@"Explore Monks"];
    
    [self serverHit];
    self.expMonksTbl.delegate=self;
    self.expMonksTbl.dataSource=self;
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)serverHit
{
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                                @"authtoken":delegate1.zenwiseToken,
                                  @"deviceid":delegate1.currentDeviceId
                               };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower/leaders?limit=1000",delegate1.baseUrl]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if(data!=nil)
                                                    {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==403)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                            
                                                        }
                                                        else if([httpResponse statusCode]==401)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                        self.responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        //NSLog(@"Response:%@",self.responseDict);
                                                        //NSLog(@"Count: %lu",[[self.responseDict objectForKey:@"results"] count]);
                                                        
                                                        }
                                    
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            
                                                            
                                                            [self.expMonksTbl reloadData];
                                                            
                                                            
                                                            
                                                        });

                                                        
                                                        
                                                    }
                                                        
                                                    }
                                                }];
    [dataTask resume];
}
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.responseDict objectForKey:@"results"] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
        
    ExpMonksCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
//    NSString * imagelogo = [NSString stringWithFormat:@"%@",[[[self.responseDict objectForKey:@"results"] objectAtIndex:indexPath.row] objectForKey:@"logourl"]];
    
    
    
    
    @try {
        cell.userImg.image = [UIImage imageNamed:@"userNoData.png"];
        // cell.userImg.image = [UIImage imageNamed:imagelogo];
        
        cell.nameLbl.text = [[[self.responseDict objectForKey:@"results"] objectAtIndex:indexPath.row] objectForKey:@"firstname"];
        
        
        NSString * number = [NSString stringWithFormat:@"%@",[[[self.responseDict objectForKey:@"results"] objectAtIndex:indexPath.row] objectForKey:@"followercount"]];
        
        if([number isEqual:[NSNull null]]||[number isEqualToString:@"<null>"])
        {
            NSString * number = @"0";
            
            NSString * label = @" Subscribers";
            
            NSString * finalLabel = [number stringByAppendingString:label];
            //NSLog(@"Subscribers:%@",finalLabel);
            
            
            cell.subscribeLabel.text = finalLabel;
        }else
        {
            NSString * number = [NSString stringWithFormat:@"%@",[[[self.responseDict objectForKey:@"results"] objectAtIndex:indexPath.row] objectForKey:@"followercount"]];
            NSString * label = @" Subscribers";
            
            NSString * finalLabel = [number stringByAppendingString:label];
            //NSLog(@"Subscribers:%@",finalLabel);
            
            
            cell.subscribeLabel.text = finalLabel;
        }
        

    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
       return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    ExploreDetail * explore = [self.storyboard instantiateViewControllerWithIdentifier:@"explore"];
    [self.navigationController pushViewController:explore animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 65;
}


@end
