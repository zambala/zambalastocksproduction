//
//  NewExploreMonksViewController.h
//  PremiumServices
//
//  Created by zenwise mac 2 on 7/13/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "ViewController.h"

@interface NewExploreMonksViewController : ViewController <UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *exploreMonksTableView;

- (IBAction)actionView:(id)sender;


@end
