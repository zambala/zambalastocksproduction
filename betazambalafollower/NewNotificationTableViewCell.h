//
//  NewNotificationTableViewCell.h
//  zambala leader
//
//  Created by zenwise mac 2 on 3/15/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewNotificationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *bulletView;
@property (weak, nonatomic) IBOutlet UILabel *messageLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@end
