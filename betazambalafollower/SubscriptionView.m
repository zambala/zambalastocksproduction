//
//  SubscriptionView.m
//  PremiumServices
//
//  Created by zenwise mac 2 on 12/30/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "SubscriptionView.h"
#import "SubscriptionViewCell.h"

@interface SubscriptionView ()

@end

@implementation SubscriptionView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationItem.backBarButtonItem setTitle:@"Subscription View"];
    self.subcriptionTbl.delegate=self;
    self.subcriptionTbl.dataSource=self;
    


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}


- (SubscriptionViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *CellIdentifier = @"CELL";
    
    SubscriptionViewCell *cell = [self.subcriptionTbl dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.userImg.image = [UIImage imageNamed:@"userNoData.png"];
    cell.nameLbl.text = @"Rithu Jain";
    
    
//    cell.cellView.backgroundColor = [UIColor whiteColor];
//   cell.cellView.layer.cornerRadius = 2.0f;
//    cell.cellView.clipsToBounds = YES;
//    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:cell.cellView.bounds];
//   cell.cellView.layer.masksToBounds = NO;
//    cell.cellView.layer.shadowColor = [UIColor blackColor].CGColor;
//    cell.cellView.layer.shadowOffset = CGSizeMake(0.0f, 3.0f);
//   cell.cellView.layer.shadowOpacity = 0.1f;
//    cell.cellView.layer.shadowPath = shadowPath.CGPath;
    cell.subscriptionView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    cell.subscriptionView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    cell.subscriptionView.layer.shadowOpacity = 1.0f;
    cell.subscriptionView.layer.shadowRadius = 1.0f;
    cell.subscriptionView.layer.cornerRadius=1.0f;
    cell.subscriptionView.layer.masksToBounds = NO;

    
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 110;
}



@end
