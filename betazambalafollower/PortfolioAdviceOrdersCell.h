//
//  PortfolioAdviceOrdersCell.h
//  testing
//
//  Created by zenwise technologies on 27/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PortfolioAdviceOrdersCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UISegmentedControl *buySellSegment;


@property (strong, nonatomic) IBOutlet UIButton *marketBtn;

@property (strong, nonatomic) IBOutlet UIButton *limitBtn;
@property (strong, nonatomic) IBOutlet UIButton *stopLossBtn;
@property (strong, nonatomic) IBOutlet UIView *marketView;
@property (strong, nonatomic) IBOutlet UIView *stopLossView;
@property (strong, nonatomic) IBOutlet UILabel *limitLbl;
@property (strong, nonatomic) IBOutlet UIView *limitTextFieldView;
@property (strong, nonatomic) IBOutlet UITextField *limitTxt;

@end
