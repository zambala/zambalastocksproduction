//
//  PersonalDetailsViewController.h
//  NewUI
//
//  Created by Zenwise Technologies on 22/05/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface PersonalDetailsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *fullNameTF;
@property (weak, nonatomic) IBOutlet UITextField *countryCodeTF;
@property (weak, nonatomic) IBOutlet UITextField *mobileNumberTF;
@property (weak, nonatomic) IBOutlet UITextField *emailIDTF;
@property (weak, nonatomic) IBOutlet UITextField *referralCodeTF;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property Reachability * reach;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityInd;
@property (weak, nonatomic) IBOutlet UILabel *termsAndConditionsLabel;
- (IBAction)verifiedAction:(id)sender;
- (IBAction)resendEmailAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIView *succesView;
@property (weak, nonatomic) IBOutlet UILabel *mailVerificationLbl;

@property (weak, nonatomic) IBOutlet UIView *bgSubView;
@property (weak, nonatomic) IBOutlet UIButton *verifiedBtn;
@end
