//
//  KiteWebView.m
//  testing
//
//  Created by zenwise technologies on 24/11/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "KiteWebView.h"

@interface KiteWebView ()

@end

@implementation KiteWebView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSString *link = @"https://zerodha.com/open-account?c";
    NSURL *url=[NSURL URLWithString:link];
    
    NSURLRequest *urlReq=[NSURLRequest requestWithURL: url];
    //NSLog(@"urlreq%@",urlReq);
    [self.kiteWebView loadRequest:urlReq];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
