//
//  FeedCollectionViewCell5.h
//  testing
//
//  Created by zenwise mac 2 on 12/26/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedCollectionViewCell5 : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic) IBOutlet UILabel *descripLbl5;
@property (strong, nonatomic) IBOutlet UILabel *timeLbl5;

@end
