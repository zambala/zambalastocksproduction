//
//  selectLeadersViewController.h
//  
//
//  Created by zenwise technologies on 10/07/17.
//
//

#import <UIKit/UIKit.h>

@interface selectLeadersViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *selectLeadersTableView;

@property NSMutableArray * responseArray;

@end
