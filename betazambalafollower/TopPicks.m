//
//  TopPicks.m
//  PayPage
//
//  Created by zenwise mac 2 on 12/22/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "TopPicks.h"
#import "TopPicksCell.h"
#import "AppDelegate.h"
#import "StockView1.h"
#import "StockOrderView.h"
#import "NewProfile.h"
#import "NoTopicksCell.h"
#import "HMSegmentedControl.h"
#import "TopPerformersCell.h"
#import "TopPerformersViewController.h"
#import <Mixpanel/Mixpanel.h>
#import "UIViewController+MJPopupViewController.h"
#import "PotentialGainersPopUpViewController.h"
#import "UIImageView+WebCache.h"

@interface TopPicks ()<HVTableViewDataSource,HVTableViewDelegate,TopPerformersCellDelegate,UITextFieldDelegate>
{
    AppDelegate * delegate1;
}
@property (strong, nonatomic) NSMutableSet *expandedIndexPaths;
@property (strong, nonatomic) NSTimer * searchTimer;



@end

@implementation TopPicks
{
    NSString * EPString;
    NSString * TPString;
    NSString * SLString;
    NSString * buysell;
    NSString * messageStr;
    NSArray * segmentsArray;
    HMSegmentedControl * segmentedControl;
    NSString * urlString;
    NSString * buttonCheck;
    BOOL rehitBool;
    NSNumber * offset;
    UIRefreshControl * refreshControl;
    UIRefreshControl * refreshControl1;
    NSMutableArray * finalResponseArray;
    NSString * number;
   

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"top_picks_page"];
    [mixpanelMini track:@"trending_advices_page"];
    NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
    number= [loggedInUser stringForKey:@"profilemobilenumber"];
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.topPicksResponseArray=[[NSMutableDictionary alloc]init];
    self.potentialGainersDictionary = [[NSMutableDictionary alloc]init];
    self.finalResponseArrayGainers= [[NSMutableArray alloc]init];
    self.topPerformersTblView.HVTableViewDelegate = self;
    self.topPerformersTblView.HVTableViewDataSource = self;
    //self.topPerformersTblView.expandOnlyOneCell=TRUE;
    self.topPicksTbl.delegate=self;
    self.topPicksTbl.dataSource=self;
    offset=@0;
    NSArray * localSegmentsArray=@[@"POTENTIAL GAINERS",@"TRENDING ADVICES"];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.topPerformersArray = [[NSMutableDictionary alloc]init];
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:localSegmentsArray];
    segmentedControl.frame = CGRectMake(0, 0, self.view.frame.size.width, 40);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segmentedControl];
    [segmentedControl setSelectedSegmentIndex:0];
    self.topPerformersView.hidden=YES;
    self.searchFld.delegate=self;
    self.blueChipsButton.layer.cornerRadius=10.0f;
    self.bullishButton.layer.cornerRadius = 10.0f;
    self.bearishButton.layer.cornerRadius = 10.0f;
    self.allResultsButton.layer.cornerRadius = 10.0f;
    self.topPicksResponseArray = [[NSMutableDictionary alloc]init];

    [self.blueChipsButton setBackgroundColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f]];
    [self.bullishButton setBackgroundColor:[UIColor clearColor]];
    [self.bearishButton setBackgroundColor:[UIColor clearColor]];
    [self.allResultsButton setBackgroundColor:[UIColor clearColor]];
    self.bullishButton.layer.borderWidth = 1.0f;
    self.bullishButton.layer.borderColor = [UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f].CGColor;
    self.bearishButton.layer.borderWidth = 1.0f;
    self.bearishButton.layer.borderColor = [UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f].CGColor;
    self.allResultsButton.layer.borderWidth = 1.0f;
    self.allResultsButton.layer.borderColor = [UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f].CGColor;
    [self.blueChipsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.bullishButton setTitleColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f] forState:UIControlStateNormal];
    [self.bearishButton setTitleColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f] forState:UIControlStateNormal];
    [self.allResultsButton setTitleColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f] forState:UIControlStateNormal];
//    urlString=@"bluechipwinners";
//    buttonCheck= @"bluechips";
    self.topPerformsScrollView.contentSize = CGSizeMake(self.topPerformsScrollView.contentSize.width,self.topPerformsScrollView.frame.size.height);

    [self.blueChipsButton addTarget:self action:@selector(onTopButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.bullishButton addTarget:self action:@selector(onTopButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.bearishButton addTarget:self action:@selector(onTopButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.allResultsButton addTarget:self action:@selector(onTopButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.searchFld addTarget:self
                       action:@selector(textFieldDidChange:)
             forControlEvents:UIControlEventEditingChanged];
   if([delegate1.pushNotificationSection isEqualToString:@"POTENTIAL_GAINERS"])
   {
       delegate1.pushNotificationSection=@"";
       [segmentedControl setSelectedSegmentIndex:0];
       [self segmentedControlChangedValue];
   }
  else if([delegate1.pushNotificationSection isEqualToString:@"TRENDING_ADVICES"])
    {
        delegate1.pushNotificationSection=@"";
        [segmentedControl setSelectedSegmentIndex:1];
        [self segmentedControlChangedValue];
    }
}

-(void)onTopButtonTap:(UIButton*)sender
{
    offset=@0;
    self.finalResponseArrayGainers = [[NSMutableArray alloc]init];
     rehitBool=false;
    @try
    {
    [self.view setUserInteractionEnabled:NO];
    if(sender == self.blueChipsButton)
    {
        self.topPerformersTblView.hidden=YES;
        self.activityIndicator.hidden=NO;
        [self.activityIndicator startAnimating];
        [self.blueChipsButton setBackgroundColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f]];
        [self.bullishButton setBackgroundColor:[UIColor clearColor]];
        [self.bearishButton setBackgroundColor:[UIColor clearColor]];
        [self.allResultsButton setBackgroundColor:[UIColor clearColor]];
        self.bullishButton.layer.borderWidth = 1.0f;
        self.bullishButton.layer.borderColor = [UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f].CGColor;
        self.bearishButton.layer.borderWidth = 1.0f;
        self.bearishButton.layer.borderColor = [UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f].CGColor;
        self.allResultsButton.layer.borderWidth = 1.0f;
        self.allResultsButton.layer.borderColor = [UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f].CGColor;
        [self.blueChipsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.bullishButton setTitleColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f] forState:UIControlStateNormal];
        [self.bearishButton setTitleColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f] forState:UIControlStateNormal];
        [self.allResultsButton setTitleColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f] forState:UIControlStateNormal];
        urlString=@"bluechipwinners";
        buttonCheck= @"bluechips";
        [self potentialGainersServer];
    }else if (sender == self.bullishButton)
    {
        
        
        self.topPerformersTblView.hidden=YES;
        self.activityIndicator.hidden=NO;
        [self.activityIndicator startAnimating];
        [self.bullishButton setBackgroundColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f]];
        [self.blueChipsButton setBackgroundColor:[UIColor clearColor]];
        [self.bearishButton setBackgroundColor:[UIColor clearColor]];
        [self.allResultsButton setBackgroundColor:[UIColor clearColor]];
        self.blueChipsButton.layer.borderWidth = 1.0f;
        self.blueChipsButton.layer.borderColor = [UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f].CGColor;
        self.bearishButton.layer.borderWidth = 1.0f;
        self.bearishButton.layer.borderColor = [UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f].CGColor;
        self.allResultsButton.layer.borderWidth = 1.0f;
        self.allResultsButton.layer.borderColor = [UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f].CGColor;
        [self.bullishButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.blueChipsButton setTitleColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f] forState:UIControlStateNormal];
        [self.bearishButton setTitleColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f] forState:UIControlStateNormal];
        [self.allResultsButton setTitleColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f] forState:UIControlStateNormal];
        urlString=@"potentialwinners";
        buttonCheck= @"bullish";
        [self potentialGainersServer];
    }else if (sender == self.bearishButton)
    {
       
        self.topPerformersTblView.hidden=YES;
        self.activityIndicator.hidden=NO;
        [self.activityIndicator startAnimating];
        [self.bearishButton setBackgroundColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f]];
        [self.blueChipsButton setBackgroundColor:[UIColor clearColor]];
        [self.bullishButton setBackgroundColor:[UIColor clearColor]];
        [self.allResultsButton setBackgroundColor:[UIColor clearColor]];
        self.blueChipsButton.layer.borderWidth = 1.0f;
        self.blueChipsButton.layer.borderColor = [UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f].CGColor;
        self.bullishButton.layer.borderWidth = 1.0f;
        self.bullishButton.layer.borderColor = [UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f].CGColor;
        self.allResultsButton.layer.borderWidth = 1.0f;
        self.allResultsButton.layer.borderColor = [UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f].CGColor;
        [self.bearishButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.bullishButton setTitleColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f] forState:UIControlStateNormal];
        [self.blueChipsButton setTitleColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f] forState:UIControlStateNormal];
        [self.allResultsButton setTitleColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f] forState:UIControlStateNormal];
        urlString=@"potentialwinners";
        buttonCheck= @"bearish";
        [self potentialGainersServer];
    }else if (sender == self.allResultsButton)
    {
        self.topPerformersTblView.hidden=YES;
        self.activityIndicator.hidden=NO;
        [self.activityIndicator startAnimating];
        [self.allResultsButton setBackgroundColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f]];
        [self.blueChipsButton setBackgroundColor:[UIColor clearColor]];
        [self.bullishButton setBackgroundColor:[UIColor clearColor]];
        [self.bearishButton setBackgroundColor:[UIColor clearColor]];
        self.blueChipsButton.layer.borderWidth = 1.0f;
        self.blueChipsButton.layer.borderColor = [UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f].CGColor;
        self.bullishButton.layer.borderWidth = 1.0f;
        self.bullishButton.layer.borderColor = [UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f].CGColor;
        self.bearishButton.layer.borderWidth = 1.0f;
        self.bearishButton.layer.borderColor = [UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f].CGColor;
        [self.allResultsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.bullishButton setTitleColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f] forState:UIControlStateNormal];
        [self.bearishButton setTitleColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f] forState:UIControlStateNormal];
        [self.blueChipsButton setTitleColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f] forState:UIControlStateNormal];
        urlString=@"potentialwinners";
        buttonCheck= @"all";
       
        [self potentialGainersServer];
        
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)textFieldDidChange :(UITextField *)textField{
    @try
    {
    // if a timer is already active, prevent it from firing
    if (self.searchTimer != nil) {
        [self.searchTimer invalidate];
        self.searchTimer = nil;
    }
    
    // reschedule the search: in 1.0 second, call the searchForKeyword: method on the new textfield content
    self.searchTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                        target: self
                                                      selector: @selector(searchForKeyword:)
                                                      userInfo: self.searchFld.text
                                                       repeats: NO];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (void) searchForKeyword:(NSTimer *)timer
{
    @try
    {
    // retrieve the keyword from user info
    NSString *keyword = (NSString*)timer.userInfo;
        self.searchStr=keyword;
        [self onTopButtonTap:self.allResultsButton];
    
//        offset=@0;
//     if(self.searchStr.length>0)
//     {
////    [self segmentedControlChangedValue];
//     }
    // perform your search (stubbed here using //NSLog)
    //NSLog(@"Searching for keyword %@", keyword);
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
     [self.searchFld resignFirstResponder];
}
-(void)segmentedControlChangedValue
{
    
    [self.view setUserInteractionEnabled:NO];
     Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
   if(segmentedControl.selectedSegmentIndex==1)
   {
//       NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//       [self.topPicksTbl scrollToRowAtIndexPath:indexPath
//                                        atScrollPosition:UITableViewScrollPositionTop
//                                                animated:YES];
       self.infoButton.enabled=NO;
       [self.searchFld resignFirstResponder];
       [self.navigationItem.leftBarButtonItem setEnabled:NO];
       segmentedControl.enabled=false;
       [mixpanelMini track:@"trending_advices_page"];
        self.topPerformersView.hidden=YES;
        finalResponseArray = [[NSMutableArray alloc]init];
        offset=@0;
       [self topPicksServer];
   }
    else if(segmentedControl.selectedSegmentIndex==0)
    {
//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//        [self.topPerformersTblView scrollToRowAtIndexPath:indexPath
//                             atScrollPosition:UITableViewScrollPositionTop
//                                     animated:YES];
        [self.navigationItem.leftBarButtonItem setEnabled:YES];
        self.infoButton.enabled=YES;
        segmentedControl.enabled=false;
        [mixpanelMini track:@"potential_gainers_page"];
        self.topPicksTbl.hidden=YES;
        self.finalResponseArrayGainers = [[NSMutableArray alloc]init];
        offset=@0;
        [self potentialGainersServer];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
     [self.navigationController setNavigationBarHidden:NO animated:YES];
}
-(void)viewDidAppear:(BOOL)animated
{
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
    
//    self.activityIndicator.hidden=NO;
//    [self.activityIndicator startAnimating];
    if(segmentedControl.selectedSegmentIndex==0)
    {
    [self onTopButtonTap:self.allResultsButton];
    }
    //[self segmentedControlChangedValue];
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([self.searchFld.text isEqualToString:@""])
    {
        self.searchStr=@"";
        [self segmentedControlChangedValue];
    }
    [self.searchFld resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)potentialGainersServer
{
    @try
    {
    self.activityIndicator.hidden =NO;
    [self.activityIndicator startAnimating];
    NSData *   postData;
    NSDictionary *headers = @{
                              @"cache-control": @"no-cache",
                              @"content-type": @"application/json",
                              @"authtoken":delegate1.zenwiseToken,
                              @"deviceid":delegate1.currentDeviceId
                              };
    NSString * localString;
    NSDictionary * parameters;
    localString=[NSString stringWithFormat:@"%@follower/%@",delegate1.baseUrl,urlString];
    if([urlString isEqualToString:@"potentialwinners"])
    {
        if(self.searchStr.length>0)
        {
            rehitBool=false;
            
            self.searchStr = [self.searchStr uppercaseString];
            
            if([buttonCheck isEqualToString:@"bullish"])
            {
                parameters=@{
                             @"buysell":@1,
                             @"limit":@200000,
                             @"offset":offset,
                             @"symbol":self.searchStr,
                             @"mobilenumber":number,
                             @"brokerid":[delegate1.brokerInfoDict objectForKey:@"brokerid"],
                             @"clientid":delegate1.userID,
                             };
            }else if ([buttonCheck isEqualToString:@"bearish"])
            {
                parameters=@{
                             @"buysell":@2,
                             @"limit":@200000,
                             @"offset":offset,
                             @"symbol":self.searchStr,
                             @"mobilenumber":number,
                             @"brokerid":[delegate1.brokerInfoDict objectForKey:@"brokerid"],
                             @"clientid":delegate1.userID,
                             };
            }else if ([buttonCheck isEqualToString:@"all"])
            {
                parameters=@{
                             @"buysell":@0,
                             @"limit":@200000,
                             @"offset":offset,
                             @"symbol":self.searchStr,
                             @"mobilenumber":number,
                             @"brokerid":[delegate1.brokerInfoDict objectForKey:@"brokerid"],
                             @"clientid":delegate1.userID,
                             };
            }
        }else
        {
            if([buttonCheck isEqualToString:@"bullish"])
            {
                parameters=@{
                             @"buysell":@1,
                             @"limit":@200000,
                             @"offset":offset,
                             @"mobilenumber":number,
                             @"brokerid":[delegate1.brokerInfoDict objectForKey:@"brokerid"],
                             @"clientid":delegate1.userID,
                             };
            }else if ([buttonCheck isEqualToString:@"bearish"])
            {
                parameters=@{
                             @"buysell":@2,
                             @"limit":@200000,
                             @"offset":offset,
                             @"mobilenumber":number,
                             @"brokerid":[delegate1.brokerInfoDict objectForKey:@"brokerid"],
                             @"clientid":delegate1.userID,
                             };
            }else if ([buttonCheck isEqualToString:@"all"])
            {
                parameters=@{
                             @"buysell":@0,
                             @"limit":@200000,
                             @"offset":offset,
                             @"mobilenumber":number,
                             @"brokerid":[delegate1.brokerInfoDict objectForKey:@"brokerid"],
                             @"clientid":delegate1.userID,
                             
                             };
            }
        }
    }else if ([urlString isEqualToString:@"bluechipwinners"])
    {
        self.searchStr = [self.searchStr uppercaseString];
        if(self.searchStr.length>0)
        {
            parameters=@{
                         @"symbol":self.searchStr,
                         @"limit":@200000,
                         @"offset":offset,
                         @"mobilenumber":number,
                         @"brokerid":[delegate1.brokerInfoDict objectForKey:@"brokerid"],
                         @"clientid":delegate1.userID,
                         };
        }else
        {
            parameters=@{
                         @"limit":@200000,
                         @"offset":offset,
                         @"mobilenumber":number,
                         @"brokerid":[delegate1.brokerInfoDict objectForKey:@"brokerid"],
                         @"clientid":delegate1.userID,
                         };
        }
    }
    if ([urlString isEqualToString:@"potentialwinners"])
    {
        postData  = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    }else if ([urlString isEqualToString:@"bluechipwinners"])
    {
        postData  = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    }
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:localString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    
    [request setAllHTTPHeaderFields:headers];
    if([urlString isEqualToString:@"potentialwinners"]||[urlString isEqualToString:@"bluechipwinners"])
    {
        [request setHTTPBody:postData];
    }
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if(data != nil)
                                                    {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            if([httpResponse statusCode]==403)
                                                            {
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                
                                                            }
                                                            else if([httpResponse statusCode]==401)
                                                            {
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                
                                                            }
                                                            else
                                                            {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    
                                                                    NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                    for(int i=0;i<[[json objectForKey:@"data"] count];i++)
                                                                    {
                                                                        
                                                                        [self.finalResponseArrayGainers addObject:[[json objectForKey:@"data"]objectAtIndex:i]];
                                                                        
                                                                    }
                                                                    
                                                                    //                                                                self.topPicksResponseArray=[[NSMutableDictionary alloc]init];
                                                                    if(self.potentialGainersDictionary.count>0)
                                                                    {
                                                                        [self.potentialGainersDictionary removeAllObjects];
                                                                    }
                                                                    
                                                                    NSArray * userArray=[self.finalResponseArrayGainers mutableCopy];
                                                                    NSOrderedSet *set = [NSOrderedSet orderedSetWithArray:userArray];
                                                                    NSArray *uniqueArray = [set array];
                                                                    
                                                                    
                                                                    
                                                                    [self.potentialGainersDictionary setObject:uniqueArray forKey:@"data"];
                                                                    
                                                                });
                                                            }
                                                        }
                                                        
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            segmentedControl.enabled=true;
                                                            if(segmentedControl.selectedSegmentIndex==0)
                                                            {
                                                                //                                                                if(!self.topPicksTbl.isDragging && !self.topPicksTbl.isDecelerating)
                                                                //                                                                {
                                                                if([[self.potentialGainersDictionary objectForKey:@"data"] count]>0)
                                                                {
                                                                    //[self.searchFld resignFirstResponder];
                                                                    self.topPerformersTblView.HVTableViewDelegate = self;
                                                                    self.topPerformersTblView.HVTableViewDataSource = self;
                                                                    [self.topPerformersTblView reloadData];
                                                                    [self.activityIndicator stopAnimating];
                                                                    self.activityIndicator.hidden=YES;
                                                                    self.topPerformersView.hidden=NO;
                                                                    self.topPerformersTblView.hidden=NO;
                                                                   // self.topPerformersTblView.expandOnlyOneCell=TRUE;
                                                                    [self.view setUserInteractionEnabled:YES];
                                                                }
                                                                else
                                                                {
                                                                    self.topPerformersTblView.hidden=YES;
                                                                    [self.view setUserInteractionEnabled:YES];
                                                                }
                                                                //}
                                                            }
                                                            [self.activityIndicator stopAnimating];
                                                            self.activityIndicator.hidden=YES;
                                                            
                                                            
                                                        });
                                                        
                                                    }
                                                    
                                                    
                                                    else
                                                    {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            
                                                            UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                [self potentialGainersServer];
                                                            }];
                                                            
                                                            [alert addAction:retryAction];
                                                            
                                                        });
                                                        
                                                        
                                                    }
                                                }];
    [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

}
-(void)topPicksServer
{
    @try
    {
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    segmentsArray=[[NSArray alloc]init];
    NSData *   postData;
    
    segmentsArray=[delegate1.brokerInfoDict objectForKey:@"segment"];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateStyle: NSDateFormatterShortStyle];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *date = [dateFormat stringFromDate:[NSDate date]];
    
    
    @try {
        NSDictionary *headers = @{
                                   @"cache-control": @"no-cache",
                                    @"content-type": @"application/json",
                                    @"authtoken":delegate1.zenwiseToken,
                                    @"deviceid":delegate1.currentDeviceId
                                   };
        NSString * localString;
        NSDictionary *    parameters;
     
        if(segmentedControl.selectedSegmentIndex==1)
        {
             localString=[NSString stringWithFormat:@"%@follower/toppicks",delegate1.baseUrl];
            parameters = @{
                           @"clientid":delegate1.userID,
                           @"messagetypeid":@1,
                           @"limit":@10,
                           @"offset":offset,
                           @"sortby":@"DESC",
                           @"orderby":@"changepercent",
                           @"closed":@(false),
                           @"segment":segmentsArray,
                           @"active":@(true),
                            @"mobilenumber":number,
                             @"brokerid":[delegate1.brokerInfoDict objectForKey:@"brokerid"]
                          
                           };
        }
        else
        {
            localString=[NSString stringWithFormat:@"%@follower/%@",delegate1.baseUrl,urlString];
            if([urlString isEqualToString:@"potentialwinners"])
            {
                if(self.searchStr.length>0)
                {
                    self.searchStr = [self.searchStr uppercaseString];
                    
                if([buttonCheck isEqualToString:@"bullish"])
                {
                    parameters=@{
                                 @"buysell":@1,
                                 @"limit":@20,
                                 @"offset":offset,
                                 @"symbol":self.searchStr
                                 };
                }else if ([buttonCheck isEqualToString:@"bearish"])
                {
                    parameters=@{
                                 @"buysell":@2,
                                 @"limit":@20,
                                 @"offset":offset,
                                 @"symbol":self.searchStr
                                 };
                }else if ([buttonCheck isEqualToString:@"all"])
                {
                    parameters=@{
                                 @"buysell":@0,
                                 @"limit":@20,
                                 @"offset":offset,
                                 @"symbol":self.searchStr
                                 };
                }
                }else
                {
                    if([buttonCheck isEqualToString:@"bullish"])
                    {
                        parameters=@{
                                     @"buysell":@1,
                                     @"limit":@10,
                                     @"offset":offset,
                                     };
                    }else if ([buttonCheck isEqualToString:@"bearish"])
                    {
                        parameters=@{
                                     @"buysell":@2,
                                     @"limit":@10,
                                     @"offset":offset,
                                     };
                    }else if ([buttonCheck isEqualToString:@"all"])
                    {
                        parameters=@{
                                     @"buysell":@0,
                                     @"limit":@10,
                                     @"offset":offset,
                                     
                                     };
                    }
                }
            }else if ([urlString isEqualToString:@"bluechipwinners"])
            {
                self.searchStr = [self.searchStr uppercaseString];
                if(self.searchStr.length>0)
                {
                parameters=@{
                             @"symbol":self.searchStr,
                             @"limit":@10,
                             @"offset":offset,
                             };
                }else
                {
                    parameters=@{
                                 @"limit":@10,
                                 @"offset":offset,
                                 };
                }
            }
        }
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:localString]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
      
    if(segmentedControl.selectedSegmentIndex==1)
    {
        postData  = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    }else if ([urlString isEqualToString:@"potentialwinners"])
    {
        postData  = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    }else if ([urlString isEqualToString:@"bluechipwinners"])
    {
        postData  = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    }
        [request setHTTPMethod:@"POST"];
        
        [request setAllHTTPHeaderFields:headers];
        
    if(segmentedControl.selectedSegmentIndex==1)
    {
        [request setHTTPBody:postData];
    }else if([urlString isEqualToString:@"potentialwinners"]||[urlString isEqualToString:@"bluechipwinners"])
    {
        [request setHTTPBody:postData];
    }
        
        
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(data != nil)
                                                        {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            if([httpResponse statusCode]==403)
                                                            {
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                
                                                            }
                                                            else if([httpResponse statusCode]==401)
                                                            {
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                
                                                            }
                                                            else
                                                            {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    
                                                            NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                for(int i=0;i<[[json objectForKey:@"data"] count];i++)
                                                                {
                                                                    
                                                                    [finalResponseArray addObject:[[json objectForKey:@"data"]objectAtIndex:i]];
                                                                    
                                                                }
                                                                
//                                                                self.topPicksResponseArray=[[NSMutableDictionary alloc]init];
                                                                if(self.topPicksResponseArray.count>0)
                                                                {
                                                                    [self.topPicksResponseArray removeAllObjects];
                                                                }
                                                                
                                                                NSArray * userArray=[finalResponseArray mutableCopy];
                                                                NSOrderedSet *set = [NSOrderedSet orderedSetWithArray:userArray];
                                                                NSArray *uniqueArray = [set array];
                                                                
                                                                
                                                                
                                                                [self.topPicksResponseArray setObject:uniqueArray forKey:@"data"];
                                                                
                                                            });
                                                            }
                                                        }
                                                        
                                                          
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            segmentedControl.enabled=true;
                                                            if(segmentedControl.selectedSegmentIndex==1)
                                                            {
                                                                if([[self.topPicksResponseArray objectForKey:@"data"] count]>0)
                                                                {
                                                                     [self.topPicksTbl reloadData];
                                                                     self.topPicksTbl.hidden=NO;
                                                                    [self.view setUserInteractionEnabled:YES];
                                                                }
                                                            }
//                                                            else if(segmentedControl.selectedSegmentIndex==0)
//                                                            {
////                                                                if(!self.topPicksTbl.isDragging && !self.topPicksTbl.isDecelerating)
////                                                                {
//                                                                if([[self.topPicksResponseArray objectForKey:@"data"] count]>0)
//                                                                {
//                                                                [self.searchFld resignFirstResponder];
//                                                                [self.topPerformersTblView reloadData];
//                                                                    [self.activityIndicator stopAnimating];
//                                                                    self.activityIndicator.hidden=YES;
//                                                                    self.topPerformersView.hidden=NO;
//                                                                    self.topPerformersTblView.hidden=NO;
//                                                                    self.topPerformersTblView.expandOnlyOneCell=TRUE;
//                                                                    [self.view setUserInteractionEnabled:YES];
//                                                                }
//                                                                else
//                                                                {
//                                                                     self.topPerformersTblView.hidden=YES;
//                                                                    [self.view setUserInteractionEnabled:YES];
//                                                                }
//                                                                //}
//                                                            }
                                                            [self.activityIndicator stopAnimating];
                                                            self.activityIndicator.hidden=YES;
                                                           
                                                            
                                                        });
                                                        
                                                        }
                                                        
                                                        
                                                        else
                                                        {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                                
                                                                [self presentViewController:alert animated:YES completion:^{
                                                                    
                                                                }];
                                                                
                                                                
                                                                UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                    [self topPicksServer];
                                                                }];
                                                                
                                                                [alert addAction:retryAction];
                                                                
                                                            });
                                                            
                                                            
                                                        }
                                                    }];
        [dataTask resume];

        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
   }

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(segmentedControl.selectedSegmentIndex==1)
    {
    if([[self.topPicksResponseArray objectForKey:@"data"] count]>0)
    {
    return [[self.topPicksResponseArray objectForKey:@"data"] count];
    }
    }else if (segmentedControl.selectedSegmentIndex==0)
    {
        return [[self.potentialGainersDictionary objectForKey:@"data"] count];
    }
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//   if(indexPath.row<[[self.topPicksResponseArray objectForKey:@"data"] count])
//   {
//
//   }else
//   {
  
    @try
    {
    if(tableView==self.topPicksTbl)
    {
    if([[self.topPicksResponseArray objectForKey:@"data"] count]>0)
    {
    
    TopPicksCell *cell1 = [tableView dequeueReusableCellWithIdentifier:@"TopPicks" forIndexPath:indexPath];
        
   
//        if (!cell1) {
//            // if cell is empty create the cell
//            cell1 = [[TopPicksCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TopPicks"];
//        }
    cell1.oLabel.layer.cornerRadius = 12.5f;
    cell1.oLabel.layer.masksToBounds = YES;
    cell1.stLabel.layer.cornerRadius = 12.5f;
    cell1.stLabel.layer.masksToBounds = YES;

       
        int duration=[[[[[self.topPicksResponseArray objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"durationtype"] intValue];
        if(duration==1)
        {
            cell1.durationLbl.text=@"DT";
            
            cell1.durationLbl.backgroundColor=[UIColor colorWithRed:(246/255.0) green:(195/255.0) blue:(117/255.0) alpha:1.0f];
        }
        else if(duration==2)
        {
            cell1.durationLbl.text=@"ST";
            
            cell1.durationLbl.backgroundColor=[UIColor colorWithRed:(178/255.0) green:(224/255.0) blue:(239/255.0) alpha:1.0f];
        }
        else if(duration==3)
        {
            cell1.durationLbl.text=@"LT";
            
            cell1.durationLbl.backgroundColor=[UIColor colorWithRed:(220/255.0) green:(172/255.0) blue:(241/255.0) alpha:1.0f];
        }
    
    [cell1.followBtn addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
     [cell1.buySellButton addTarget:self action:@selector(yourButtonClicked1:) forControlEvents:UIControlEventTouchUpInside];
        
        NSDictionary * openAdvice=[[[self.topPicksResponseArray objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"open_advice"];
       
        
        if(openAdvice)
        {
        NSString * public=[NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"ispublic"]];
        int publicInt=[public intValue];
    
       // NSString * sub=[NSString stringWithFormat:@"%@",[[self.topPicksResponseArray objectAtIndex:indexPath.row] objectForKey:@"subscriptiontypeid"]];
        
       
        
        if(publicInt==0)
        {
            cell1.premiumLabel.hidden=YES;
            cell1.premiumImageView.hidden=YES;
            cell1.premiumImageView.image=[UIImage imageNamed:@"premiumNew"];
          
            NSString * sub = [NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"subscriptiontypeid"]];
            
            int subInt=[sub intValue];
            
            if(subInt==1)
            {
              
                cell1.premiumImageView.hidden=YES;
            }
            
            else if(subInt==2)
            {
               
                cell1.premiumImageView.hidden=NO;
            }
        }else if (publicInt ==1)
        {
            cell1.premiumLabel.hidden=NO;
            cell1.premiumImageView.hidden=NO;
            cell1.premiumImageView.image=[UIImage imageNamed:@"publicnew"];
           
        }
        NSString * mainString=[NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"message"]];
        
    if([mainString containsString:@"@"])
    {
        cell1.valueChangeLabel.text=[mainString uppercaseString];
    }
    
    else
    {
        
        TPString=[NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"targetprice"]];
        
        if ([mainString isEqual:[NSNull null]]||mainString==nil||[mainString isEqualToString:@"<null>"] ||  [TPString isEqual:[NSNull null]]||TPString==nil||[TPString isEqualToString:@"<null>"]   ) {
            
        }
        
        else{
            
            @try {
                
                NSString * buttonNumber = [openAdvice objectForKey:@"buysell"];
                int buttonTitle = [buttonNumber intValue];
                
                if(buttonTitle==1)
                {
                    buysell=@"BUY";
                    
                    [cell1.buySellButton setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1.0f]];
                    
                    [cell1.buySellButton setTitle:buysell forState:UIControlStateNormal];
                    
                }
                
                else if(buttonTitle==2)
                {
                    buysell=@"SELL";
                    
                    [cell1.buySellButton setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1.0f]];
                    
                    [cell1.buySellButton setTitle:buysell forState:UIControlStateNormal];
                    
                    
                    
                    
                }
                
                
                
                
                //NSLog(@"main String %@",mainString);
                
                
                
                NSArray * mainArray=[mainString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                
                
                
                //NSLog(@"array  %@",[mainArray objectAtIndex:0]);
                
                //NSLog(@"array  %@",[mainArray objectAtIndex:1]);
                
               
                
               
                
              
                NSString * string=[NSString stringWithFormat:@"%@",[mainArray objectAtIndex:1]];
                
                
                
                NSArray * companyArray=[string componentsSeparatedByString:@";"];
                
                
                
                //NSLog(@" array1 %@",companyArray);
                
                
                
                //               for (int i=1; i<[companyArray count];i++) {
                
                
                
                //                   NSString * string11=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:0]];
                //
                //
                //
                //                   NSArray * array2=[string11 componentsSeparatedByString:@";"];
                //
                //                   //NSLog(@"%@",array2);
                
                NSString * companyName=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:0]];
                NSString * EP1=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:1]];
                NSString * TP=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:2]];
                NSString * SL;
                NSArray * SLArray;
                if([mainString containsString:@"SL="])
                {
                    SL=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:3]];
                    SLArray=[SL componentsSeparatedByString:@"="];
                    SLString=[NSString stringWithFormat:@"%@",[SLArray objectAtIndex:1]];
                    
                }
                else
                {
                    SL=@"";
                    SLString=@"";
                }
                NSArray * EPArray=[EP1 componentsSeparatedByString:@"="];
                NSArray * TPArray=[TP componentsSeparatedByString:@"="];
               
                
                EPString=[NSString stringWithFormat:@"%@",[EPArray objectAtIndex:1]];
                TPString=[NSString stringWithFormat:@"%@",[TPArray objectAtIndex:1]];
               
                
                
                
                NSString * dummyfinalStr=[NSString stringWithFormat:@"%@ %@ @%@%@%@",buysell,companyName,EP1,TP,SL];
                
                NSString * finalStr = [dummyfinalStr stringByReplacingOccurrencesOfString:@"=" withString:@" "];
                
                //NSLog(@"%@",finalStr);
                
                //NSLog(@"COMPANY %@",companyName);
                
                NSString * sell = [NSString stringWithFormat:@"<html><body><div style='text-align:left;display:inline-block;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'>  %@</div> </body></html>",buysell];
                NSString * muthootfin =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",[companyArray objectAtIndex:0]];
                NSString * ep = @"<html><body><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'> @ EP</div></html></body>";
                NSString * value1 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",EPString];
                NSString * tp = @"<html><body><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'> TP</div></html></body>";
                NSString * value2 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",TPString];
                NSString * sl = @"<html><body><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'> SL</div></html></body>";
                NSString * value3 =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",SLString] ;
                
                NSString * string1 = [sell stringByAppendingString:muthootfin];
                NSString *string2 = [string1 stringByAppendingString:ep];
                NSString * string3 = [string2 stringByAppendingString:value1];
                NSString * string4 = [string3 stringByAppendingString:tp];
                NSString * string5 = [string4 stringByAppendingString:value2];
                NSString * string6 = [string5 stringByAppendingString:sl];
                NSString * finalString;
                if([mainString containsString:@"SL="])
                {
                    finalString = [string6 stringByAppendingString:value3];
                }
                else
                {
                    finalString = string5;
                }
                
                
                
                NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[finalString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                
                //UILabel * myLabel = [[UILabel alloc] init];
                // myLabel.attributedText = attrStr;
                cell1.valueChangeLabel.attributedText=attrStr;
                
            }
            
            
            
            @catch (NSException * e) {
                //NSLog(@"Exception: %@", e);
            }
            @finally {
                //NSLog(@"finally");
                
            }
            
        }
        
        
    }
    
    
                NSString * buttonNumber = [openAdvice objectForKey:@"buysell"];
                int buttonTitle = [buttonNumber intValue];
                
                if(buttonTitle==1)
                {
                    buysell=@"BUY";
                    
                    [cell1.buySellButton setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1.0f]];
                    
                    [cell1.buySellButton setTitle:buysell forState:UIControlStateNormal];
                    
                }
                
                else if(buttonTitle==2)
                {
                    buysell=@"SELL";
                    
                    [cell1.buySellButton setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1.0f]];
                    
                    [cell1.buySellButton setTitle:buysell forState:UIControlStateNormal];
                    
                    
                    
                    
                }
                
                
    
    
        
            
        
                
                
                //                              if (i==1) {
                //                                  EP=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                //
                //                                  //NSLog(@"EP %@",[array2 objectAtIndex:1]);
                //
                //                              }
                
                //                   if (i==2) {
                //                       EPString=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                //
                //                       //NSLog(@"TP %@",[array2 objectAtIndex:1]);
                //
                //                   }
                //
                //                   if (i==3) {
                //                       SLString=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                //
                //                       //NSLog(@"SL %@",[array2 objectAtIndex:1]);
                //
                //                   }
                //               }
                //
            
    cell1.profileNameLabel.text=@"nill";
    cell1.dateAndTimeLabel.text=@"nill";
            
            @try
            {
                NSString * profileName = [openAdvice objectForKey:@"firstname"];
                NSString * lastName = [openAdvice objectForKey:@"lastname"];
                
                if([lastName isEqual:[NSNull null]])
                {
                    lastName=@"";
                }else
                {
                    lastName =lastName;
                }
                NSString * name = [profileName stringByAppendingString:lastName];

                NSString * analystName = [openAdvice objectForKey:@"leaderorgname"];
                if(publicInt==0)
                {
                cell1.profileNameLabel.text = profileName;
                }else if (publicInt ==1)
                {
                    cell1.profileNameLabel.text = analystName;
                }
                
                NSString * dateStr=[openAdvice objectForKey:@"createdon"];
                
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
                [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                NSDate *date = [dateFormatter dateFromString:dateStr]; // create date from string
                
                // change to a readable time format and change to local time zone
                [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
                [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                NSString *finalDate = [dateFormatter stringFromDate:date];
                
                
                
                
                
                cell1.dateAndTimeLabel.text=finalDate;
                

            }
            @catch (NSException * e) {
                //NSLog(@"Exception: %@", e);
            }
            @finally {
                //NSLog(@"finally");
            }
    cell1.stLabel.text=@"nill";
            
    @try {
        
        NSString * duration=[NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"durationtype"]];
        
        int durationInt=[duration intValue];
        
        if(durationInt==1)
        {
            cell1.stLabel.text=@"DT";
            
            cell1.stLabel.backgroundColor=[UIColor colorWithRed:(255/255.0) green:(192/255.0) blue:(102/255.0) alpha:1];
        }
        
        else if(durationInt==2)
        {
            cell1.stLabel.text=@"ST";
            
            cell1.stLabel.backgroundColor=[UIColor colorWithRed:(165/255.0) green:(225/255.0) blue:(241/255.0) alpha:1];
        }
        else if(durationInt==3)
        {
            
            
            
            cell1.stLabel.text=@"LT";
            
            cell1.stLabel.backgroundColor=[UIColor colorWithRed:(227/255.0) green:(168/255.0) blue:(246/255.0) alpha:1];
            
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
            
            
            // NSString * followerCount = [[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@""];
            
           
            
            if([[openAdvice objectForKey:@"lasttradedprice"] isEqual:[NSNull null]])
            {
                
                cell1.LTPLabel.text=@"0";
                
                
            }else
            {
                
                 NSString * ltp =[NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"lasttradedprice"]];
            
            cell1.LTPLabel.text = ltp;
                
            }
            
            @try {
                NSString * changePercentlabel = [NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"changepercent"]];
                
                NSString * percent =@"  ";
                NSString * finalChangePercentlabel = [changePercentlabel stringByAppendingString:percent];
                if([changePercentlabel isEqual:[NSNull null]]||changePercentlabel==nil||[changePercentlabel isEqualToString:@"<null>"])
                {
                    
                    
                    
                    NSString * percentage = @" ";
                    NSString * zero = @"0  ";
                    NSString* final = [zero stringByAppendingString:percentage];
                    
                    cell1.changePercentLabel.text=final;
                    cell1.changePercentLabel.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                }
                else
                {
                    
                    if([finalChangePercentlabel containsString:@"-"])
                    {
                        cell1.changePercentLabel.text=finalChangePercentlabel;
                        cell1.changePercentLabel.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                        cell1.adviceType.layer.borderColor =[[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1] CGColor];
                        [cell1.adviceType setTitleColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1] forState:UIControlStateNormal];
                        [cell1.adviceType setTitle:@"Opening Advice" forState:UIControlStateNormal];
                         cell1.oLabel.backgroundColor=[UIColor colorWithRed:(238/255.0) green:(43/255.0) blue:(53/255.0) alpha:1];
                    }
                    else{
                        cell1.changePercentLabel.text=finalChangePercentlabel;
                        cell1.changePercentLabel.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                        cell1.adviceType.layer.borderColor =[[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1] CGColor];
                        [cell1.adviceType setTitleColor:[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1] forState:UIControlStateNormal];
                        [cell1.adviceType setTitle:@"Opening Advice" forState:UIControlStateNormal];
                         cell1.oLabel.backgroundColor=[UIColor colorWithRed:(86/255.0) green:(168/255.0) blue:(66/255.0) alpha:1];
                    }
                    
                    
                }

            } @catch (NSException *exception) {
                
            } @finally {
                
            }
             cell1.gainPercent.text=@"0  ";
            
            @try {
                if([[openAdvice objectForKey:@"gainpercent"] isEqual:[NSNull null]])
                {
                    cell1.gainPercent.text=@"0%";
                    
                    
                }else
                {
                    NSString * gain=[openAdvice objectForKey:@"gainpercent"];
                    
                    float gainFloat=[gain floatValue];
                    
                    NSString * percent=@"%";
                    
                    cell1.gainPercent.text=[NSString stringWithFormat:@"%.2f%@",gainFloat,percent];
                }

            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            
//             cell1.followerCountLabel.text=@"0 Followers";
            @try {
                NSString *followersString = [NSString stringWithFormat:@"%@ Followers",[openAdvice objectForKey:@"followercount"]];
                
                
                NSString * testString = [openAdvice objectForKey:@"followercount"];
                
                NSString * firstName = [openAdvice objectForKey:@"firstname"];
                NSString * lastname = [openAdvice objectForKey:@"lastname"];
                NSString * dummyName;
                if([lastname isEqual:[NSNull null]]||[lastname containsString:@"<null>"])
                {
                    lastname=@"";
                    NSString * name = [NSString stringWithFormat:@"%@ %@",firstName,lastname];
                    dummyName = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
                    dummyName=[dummyName lowercaseString];
                    NSString * profileName=[cell1.profileNameLabel.text stringByReplacingOccurrencesOfString:@" " withString:@""];
                    profileName=[profileName lowercaseString];
                    if(![dummyName isEqualToString:profileName])
                    {
                        cell1.organizationNameLbl.text=[NSString stringWithFormat:@"%@",cell1.profileNameLabel.text];
                        cell1.profileNameLabel.text= [NSString stringWithFormat:@"%@ %@",firstName,lastname];
                        cell1.expertsXConstraint.constant=10;
                        
                    }
                    else
                    {
                        cell1.expertsXConstraint.constant=20;
                        cell1.organizationNameLbl.text=@"";
                    }
                    
                    
                    
                }else
                {
                    NSString * name = [NSString stringWithFormat:@"%@ %@",firstName,lastname];
                    dummyName = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
                    dummyName=[dummyName lowercaseString];
                    NSString * profileName=[cell1.profileNameLabel.text stringByReplacingOccurrencesOfString:@" " withString:@""];
                    profileName=[profileName lowercaseString];
                    if(![dummyName isEqualToString:profileName])
                    {
                        cell1.organizationNameLbl.text=[NSString stringWithFormat:@"%@",cell1.profileNameLabel.text];
                        cell1.profileNameLabel.text= [NSString stringWithFormat:@"%@ %@",firstName,lastname];
                        cell1.expertsXConstraint.constant=10;
                        
                    }
                    else
                    {
                        cell1.expertsXConstraint.constant=20;
                        cell1.organizationNameLbl.text=@"";
                    }
                    
                    
                }
               
                
               
                if(publicInt==0)
                {
                    cell1.expertsXConstraint.constant=20;
                    cell1.organizationNameLbl.text=@"";
                if([testString isEqual:[NSNull null]])
                {
                    cell1.followerCountLabel.text=@"0 Followers";
                }
                else
                {
                    cell1.followerCountLabel.text=followersString;
                }
                }else if(publicInt==1)
                {
                   
                }
                

            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            
        // NSString * subscribedString=[NSString stringWithFormat:@"%@",[[[self.topPicksResponseArray objectForKey:@"data"]  objectAtIndex:indexPath.row] objectForKey:@"subscribed"]];
        
        
        int subscribedInt =[[openAdvice objectForKey:@"subscribed"] intValue];
        
        
        //  NSNumber * subscribedNumber = [NSNumber numberWithInt:subscribedInt];
        
        if(subscribedInt==1)
        {
            [cell1.followBtn setTitle:@"Following" forState:UIControlStateNormal];
            
        }else if (subscribedInt==0)
        {
            [cell1.followBtn setTitle:@"Follow +" forState:UIControlStateNormal];
        }
        
        //    if([subscribedString isEqualToString:@"false"])
        //    {
        //        [cell1.followBtn setTitle:@"Follow +" forState:UIControlStateNormal];
        //    }
        //    
        //    else if([subscribedString isEqualToString:@"true"])
        //    {
        //        [cell1.followBtn setTitle:@"Following" forState:UIControlStateNormal];
        //    }

       
       [cell1.topPicksProfileButton addTarget:self action:@selector(topPicksleaderProfileMethod:) forControlEvents:UIControlEventTouchUpInside];
     cell1.actedByLabel.text=@"0";
            
            @try {
                
                NSString * actedBy = [openAdvice objectForKey:@"actedby"];
                
                
                if([actedBy isEqual:[NSNull null]])
                {
                    cell1.actedByLabel.text=@"0";
                }else
                {
                    int actedByInt=[actedBy intValue];
                    cell1.actedByLabel.text = [NSString stringWithFormat:@"%i",actedByInt];
                }
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
    cell1.sharesSoldLabel.text = @"0";
            
            @try {
                
                NSString * sharesSold = [openAdvice objectForKey:@"sharesold"];
                
                
                
                if([sharesSold isEqual:[NSNull null]])
                {
                    cell1.sharesSoldLabel.text = @"0";
                }else
                {
                    int sharesSoldInt=[sharesSold intValue];
                    cell1.sharesSoldLabel.text = [NSString stringWithFormat:@"%i",sharesSoldInt];
                }
                
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
    
     cell1.averageProfitLabel.text = @"0";
            @try {
                
                NSString * averageProfit = [openAdvice objectForKey:@"averageprofit"];
                
                
                
                if([averageProfit isEqual:[NSNull null]])
                {
                    cell1.averageProfitLabel.text = @"0";
                }else
                {
                    int sharesSoldInt=[averageProfit intValue];
                    cell1.averageProfitLabel.text = [NSString stringWithFormat:@"%i",sharesSoldInt];
                }
                
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
        
       
        
        if(publicInt==0)
        {
            cell1.followBtn.hidden=NO;
            cell1.profileImgView.image=[UIImage imageNamed:@"userimage.png"];
            
            NSString * logo=[openAdvice objectForKey:@"logourl"];
            
            if([logo isEqual:[NSNull null]])
            {
                cell1.profileImgView.image=[UIImage imageNamed:@"userimage.png"];
            }
            
            else
            {
                
                NSURL *url = [NSURL URLWithString:logo];
                  [cell1.profileImgView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"userimage.png"]];
//                NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//                    if (data) {
//                        UIImage *image = [UIImage imageWithData:data];
//                        if (image) {
//                            dispatch_async(dispatch_get_main_queue(), ^{
//
//                                if (cell1)
//                                    cell1.profileImgView.image = image;
//                            });
//                        }
//                    }
//                }];
//                [task resume];
                
            }
        }else if (publicInt==1)
        {
            cell1.followBtn.hidden=YES;
            NSURL *url;
            @try {
                  NSString * logo=[openAdvice objectForKey:@"logourl"];
                url =  [NSURL URLWithString:logo];
            } @catch (NSException *exception) {
                
                
                
            } @finally {
                
            }
            
            [cell1.profileImgView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"userimage.png"]];
//            NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//                if (data) {
//                    UIImage *image = [UIImage imageWithData:data];
//                    if (image) {
//                        dispatch_async(dispatch_get_main_queue(), ^{
//
//                            if (cell1)
//                                cell1.profileImgView.image = image;
//                        });
//                    }
//                }
//            }];
//            [task resume];
        }
        
        if(publicInt==0)
        {
            cell1.sourceLabel.hidden=YES;
            cell1.sorceDataLabel.hidden=YES;
        }else if (publicInt==1)
        {
            cell1.sourceLabel.hidden=NO;
            cell1.sorceDataLabel.hidden=NO;
            NSString * source = [NSString stringWithFormat:@"%@",[[openAdvice objectForKey:@"otherinfo"]objectForKey:@"source"]];
            cell1.sorceDataLabel.text = source;
        }
        
        
        
//        NSString * dummyAnalystName=cell1.profileNameLabel.text;
//        dummyAnalystName=[dummyAnalystName stringByReplacingOccurrencesOfString:@" " withString:@""];
//        dummyAnalystName=[dummyAnalystName lowercaseString];
//        NSString * dummyName=cell1.followerCountLabel.text;
//        dummyName=[dummyName stringByReplacingOccurrencesOfString:@" " withString:@""];
//        dummyName=[dummyName lowercaseString];
//        
//        if([dummyAnalystName containsString:dummyName])
//        {
//            cell1.followerCountLabel.hidden=YES;
//        }
//        else
//        {
//            cell1.followerCountLabel.hidden=NO;
//        }

            NSDictionary *data = [[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row];
            BOOL lastItemReached = [data isEqual:[[self.topPicksResponseArray objectForKey:@"data"] lastObject]];
            if (!lastItemReached && indexPath.row == [[self.topPicksResponseArray objectForKey:@"data"] count] - 3)
            {
                if(rehitBool==true)
                {
                    [self refreshTableVeiwList];
                }
            }
            
    
    return cell1;
    }
        
    }
    
    else
    {
        
          NoTopicksCell *cell2 = [tableView dequeueReusableCellWithIdentifier:@"notoppicks" forIndexPath:indexPath];
        
        return cell2;
    }
        
    }
   
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
  // }
    
    
}

-(void)scrollViewDidScroll: (UIScrollView*)scrollView
{
    
    
    
    //    if (scrollView.contentOffset.y + scrollView.frame.size.height == scrollView.contentSize.height)
    //    {
    //        [self refreshTableVeiwList];
    //    }
    //
    //    else if (self.equitiesTableView.contentOffset.y==0)
    //    {
    //        [self refreshTableVeiwList1];
    //
    //    }
    ScrollDirection scrollDirection;
    
    if (self.lastContentOffset > scrollView.contentOffset.y) {
        scrollDirection = ScrollDirectionUp;
        rehitBool=false;
    } else if (self.lastContentOffset < scrollView.contentOffset.y) {
        scrollDirection = ScrollDirectionDown;
        rehitBool=true;
    }
    
    self.lastContentOffset = scrollView.contentOffset.y;
    //    //NSLog(@"%f",scrollView.contentOffset.y);
}
//pagination//
-(void)refreshTableVeiwList
{
    int limitInt=[offset intValue];
    
    int finalInt=limitInt+10;
    
    offset=[NSNumber numberWithInt:finalInt];
    if(segmentedControl.selectedSegmentIndex==1)
    {
    [self topPicksServer];
    }else if (segmentedControl.selectedSegmentIndex==0)
    {
        [self potentialGainersServer];
    }
    [refreshControl1 endRefreshing];
}

-(void)tableView:(UITableView *)tableView expandCell:(TopPerformersCell *)cell withIndexPath:(NSIndexPath *)indexPath{
   // cell.purchaseButton.alpha = 0;
    
    [UIView animateWithDuration:.5 animations:^{
//        cell.detailLabel.text = @"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
//        cell.purchaseButton.alpha = 1;
        [cell.arrowButton setImage:[UIImage imageNamed:@"downarrow"]];
        cell.arrowButtonWidth.constant=10;
        cell.arrowButtonHeight.constant=6;
       // cell.arrowButton.transform = CGAffineTransformMakeRotation(M_PI-90);
    }];
    
}

-(void)tableView:(UITableView *)tableView collapseCell:(TopPerformersCell *)cell withIndexPath:(NSIndexPath *)indexPath{
//    cell.purchaseButton.alpha = 0;
    //cell.arrowButton.transform = CGAffineTransformMakeRotation(-M_PI+0.00);
//    cell.detailLabel.text = @"Lorem ipsum dolor sit ame";
    
    [UIView animateWithDuration:.5 animations:^{
        [cell.arrowButton setImage:[UIImage imageNamed:@"up-arrow"]];
        cell.arrowButtonWidth.constant=6;
        cell.arrowButtonHeight.constant=10;
       // cell.arrowButton.transform = CGAffineTransformMakeRotation(0);
    }];
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath isExpanded:(BOOL)isExpanded
{
    @try
    {
        if(tableView==self.topPerformersTblView)
        {
        if([[self.potentialGainersDictionary objectForKey:@"data"] count]>0)
        {
    TopPerformersCell*cell = [tableView dequeueReusableCellWithIdentifier:@"TopPerformersCell"];
//    if (!cell) {
//        // if cell is empty create the cell
//        cell = [[TopPerformersCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TopPerformersCell"];
//    }
    cell.delegate = self;
    if (!isExpanded) {
//        cell.detailLabel.text = @"Lorem ipsum dolor sit amet";
     // cell.arrowButton.transform = CGAffineTransformMakeRotation(0);
        [cell.arrowButton setImage:[UIImage imageNamed:@"up-arrow"]];

        
//        cell.purchaseButton.alpha = 0;
    }
    else
    {
//        cell.detailLabel.text = @"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
      //  cell.arrowButton.transform = CGAffineTransformMakeRotation(M_PI);
        [cell.arrowButton setImage:[UIImage imageNamed:@"downarrow"]];
//        cell.purchaseButton.alpha = 1;
    }
    NSString * symbol = [NSString stringWithFormat:@"%@",[[[self.potentialGainersDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"symbol"]];
    cell.symbolName.text = symbol;
    NSString * companyName = [NSString stringWithFormat:@"%@",[[[self.potentialGainersDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"companyname"]];
    cell.companyLbl.text = companyName;
   int buysellRec = [[[[self.potentialGainersDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"buysellrecommendation"]intValue];
    //0 neutral
    //1 buy
    //2 sell
    NSString * avgTP;
    NSString * percent=@"%";
    NSString * profitPotential = [NSString stringWithFormat:@"%.2f%@",[[[[self.potentialGainersDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"potential"]floatValue],percent];
    cell.profitPotentialLbl.text = profitPotential;
    
    if(buysellRec==0)
    {
        cell.consensusLabel.image = [UIImage imageNamed:@"neutralNew"];
        avgTP = [NSString stringWithFormat:@"%.2f",[[[[self.potentialGainersDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"meanbuytargetprice"]floatValue]];
    }else if (buysellRec==1)
    {
        cell.consensusLabel.image = [UIImage imageNamed:@"thumbup"];
        avgTP = [NSString stringWithFormat:@"%.2f",[[[[self.potentialGainersDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"meanbuytargetprice"]floatValue]];
    }else if (buysellRec==2)
    {
        cell.consensusLabel.image = [UIImage imageNamed:@"thumbdown"];
        avgTP = [NSString stringWithFormat:@"%.2f",[[[[self.potentialGainersDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"meanselltargetprice"]floatValue]];
    }
    cell.avgTp.text = avgTP;
//     NSUInteger advicesCount = [[[[self.topPicksResponseArray objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"messages"]count];
        int buyCount=[[[[self.potentialGainersDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"buycount"]intValue];
        int sellCount=[[[[self.potentialGainersDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"sellcount"]intValue];
        int adviceCount=buyCount + sellCount;
    cell.noOfAdvicesLbl.text = [NSString stringWithFormat:@"%i",adviceCount];
    NSString * ltp = [NSString stringWithFormat:@"%.2f",[[[[self.potentialGainersDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"lasttradedprice"]floatValue]];
    cell.ltpLbl.text = [NSString stringWithFormat:@"%@",ltp];
    cell.moreInfoBtn.layer.cornerRadius = 5.0f;
    cell.tradeBtn.layer.cornerRadius = 5.0f;
    [cell.moreInfoBtn addTarget:self action:@selector(onMoreInfoTap:) forControlEvents:UIControlEventTouchUpInside];
    [cell.tradeBtn addTarget:self action:@selector(onTradeTap:) forControlEvents:UIControlEventTouchUpInside];
//            NSIndexPath *p =indexPath;
//            NSInteger row = p.row;
//            NSInteger count=[[self.potentialGainersDictionary objectForKey:@"data"] count];
//    NSDictionary *data = [[self.potentialGainersDictionary objectForKey:@"data"] objectAtIndex:p.row];
//    BOOL lastItemReached = [data isEqual:[[self.potentialGainersDictionary objectForKey:@"data"] lastObject]];
//            if(self.searchFld.text.length>0)
//            {
//
//            }else
//            {
//        if(lastItemReached == true)
//        {
//            rehitBool=true;
//        }
//        }
            
//    if (lastItemReached && row == count - 3)
//    {
//         [self refreshTableVeiwList];
//    }
//////        if(rehitBool==true)
//////        {
//////            rehitBool=false;
////            [self refreshTableVeiwList];
//////        }
////    }
//            if(lastItemReached==true)
//            {
//                 [self refreshTableVeiwList];
//            }
           
//            if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
//                [self refreshTableVeiwList];
//            }
          
//   if (row == count - 1 ) {
//       [self refreshTableVeiwList];
//   }
    return cell;
        }
        }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
       // //NSLog(@"finally");
    }
  
   
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath isExpanded:(BOOL)isexpanded
{
    if (isexpanded)
        return 176;
    
    return 80;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    if(tableView==self.topPicksTbl)
    {
        if([[self.topPicksResponseArray objectForKey:@"data"] count]>0)
        {
            return 185;
        }
        
        else
        {
        
           return 335;
        }
        
    }
    return 0;

}

-(void)onMoreInfoTap:(UIButton*)sender
{
    @try
    {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.topPerformersTblView];
    NSIndexPath *indexPath = [self.topPerformersTblView indexPathForRowAtPoint:buttonPosition];
//    if(delegate1.topPerfomersDetailArray.count>0)
//    {
//        [delegate1.topPerfomersDetailArray removeAllObjects];
//    }
    
//    delegate1.topPerfomersDetailArray = [[NSMutableArray alloc]init];
//    delegate1.topPerfomersDetailArray = [[[self.topPicksResponseArray objectForKey:@"data"]objectAtIndex:indexPath.row] objectForKey:@"messages"];
//    if(delegate1.topPerfomersDetailArray.count>0)
//    {
        TopPerformersViewController * topDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"TopPerformersViewController"];
        topDetail.bullishCount = [NSString stringWithFormat:@"%@",[[[self.potentialGainersDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"buycount"]];
        topDetail.berishCount = [NSString stringWithFormat:@"%@",[[[self.potentialGainersDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"sellcount"]];
        topDetail.companyNameString = [NSString stringWithFormat:@"%@",[[[self.potentialGainersDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"symbol"]];
        topDetail.symbolString = [NSString stringWithFormat:@"%@",[[[self.potentialGainersDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"symbol"]];
        topDetail.potentialString = [NSString stringWithFormat:@"%@",[[[self.potentialGainersDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"potential"]];
        topDetail.topCompanyName = [NSString stringWithFormat:@"%@",[[[self.potentialGainersDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"companyname"]];
        
        topDetail.highStr = [NSString stringWithFormat:@"%@",[[[self.potentialGainersDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"maxtargetprice"]];
        
        topDetail.averageStr = [NSString stringWithFormat:@"%@",[[[self.potentialGainersDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"overallmeantargetprice"]];
        
        topDetail.lowStr = [NSString stringWithFormat:@"%@",[[[self.potentialGainersDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"mintargetprice"]];
        
        if([delegate1.brokerNameStr isEqualToString:@"Zerodha"]||[delegate1.brokerNameStr isEqualToString:@"Upstox"])
        {
            topDetail.segmentType = [NSString stringWithFormat:@"%@",[[[self.potentialGainersDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"segmenttype"]];
            topDetail.instrumentID = [NSString stringWithFormat:@"%@",[[[self.potentialGainersDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"instrumentid"]];
        }else
        {
            topDetail.segmentType = [NSString stringWithFormat:@"%@",[[[self.potentialGainersDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"exchangevalue"]];
            topDetail.instrumentID = [NSString stringWithFormat:@"%@",[[[self.potentialGainersDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"exchangetoken"]];
        }
        [self.navigationController pushViewController:topDetail animated:YES];
//    }else
//    {
//        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"No advices found!" preferredStyle:UIAlertControllerStyleAlert];
//
//        [self presentViewController:alert animated:YES completion:^{
//
//        }];
//
//        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//        }];
//
//        [alert addAction:okAction];
//    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)onTradeTap:(UIButton*)sender
{
    @try
    {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.topPerformersTblView];
    NSIndexPath *indexPath = [self.topPerformersTblView indexPathForRowAtPoint:buttonPosition];
     NSDictionary * openAdvice=[[self.potentialGainersDictionary objectForKey:@"data"]objectAtIndex:indexPath.row];
    delegate1.symbolDepthStr=[openAdvice objectForKey:@"symbol"];
    delegate1.tradingSecurityDes=[NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"symbol"]];
    delegate1.orderSegment=[openAdvice objectForKey:@"segmenttype"];
    int buysellRec = [[[[self.potentialGainersDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"buysellrecommendation"]intValue];
    if(buysellRec==1)
    {
        delegate1.orderBuySell=@"BUY";
    }else if (buysellRec==2)
    {
        delegate1.orderBuySell=@"SELL";
    }else if (buysellRec==0)
    {
        delegate1.orderBuySell=@"";
    }
    
    if([delegate1.brokerNameStr isEqualToString:@"Zerodha"])
    {
        delegate1.orderinstrument=[openAdvice objectForKey:@"instrumentid"];
    }
    
    else
    {
        delegate1.orderinstrument=[NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"exchangetoken"]];
    }
    StockOrderView * orderView=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
    [self.navigationController pushViewController:orderView animated:YES];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)yourButtonClicked:(UIButton*)sender
{
    //    if (sender.tag == 0)
    //    {
    //
    //    }
    @try
    {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.topPicksTbl];
    
    
    
    NSIndexPath *indexPath = [self.topPicksTbl indexPathForRowAtPoint:buttonPosition];
    
    //NSLog(@"Index path:%ld",(long)indexPath.row);
    TopPicksCell *cell = [self.topPicksTbl cellForRowAtIndexPath:indexPath];
    
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
                
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    
    
    @try {
        NSDictionary *headers = @{ @"content-type": @"application/x-www-form-urlencoded",
                                   @"cache-control": @"no-cache",
                                    @"authtoken":delegate1.zenwiseToken,
                                    @"deviceid":delegate1.currentDeviceId
                                   };
        
        NSString * userId=[NSString stringWithFormat:@"leaderid=%@",[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"leaderid"]];
        
        NSString * clientId=[NSString stringWithFormat:@"&clientid=%@",delegate1.userID];
         NSString * mobileNumber=[NSString stringWithFormat:@"&mobilenumber=%@",delegate1.userID];
        
        
        NSMutableData *postData = [[NSMutableData alloc] initWithData:[userId dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[clientId dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[mobileNumber dataUsingEncoding:NSUTF8StringEncoding]];
        //    if(segmentedControl.selectedSegmentIndex==0)
        //    {
        //         [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
        //    }
        //    else
        //    {
        //    if(self.buttonArray.count>0)
        //    {
        //    if([[self.buttonArray objectAtIndex:indexPath.row] isEqualToString:@"Follow +"])
        //    {
        //    [postData appendData:[@"&subscribe=true" dataUsingEncoding:NSUTF8StringEncoding]];
        //    }else if([[self.buttonArray objectAtIndex:indexPath.row] isEqualToString:@"Following"])
        //    {
        //        [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
        //
        //    }
        //    }
        //    }
        
        NSArray * subscribeArray=@[@1];
        
        NSString * subscribeString=[NSString stringWithFormat:@"&subscriptiontype=%@",subscribeArray];
        NSString * sub = [subscribeString stringByReplacingOccurrencesOfString:@"(" withString:@"{"];
        NSString * sub1 = [sub stringByReplacingOccurrencesOfString:@")" withString:@"}"];
        
        NSArray * unSubscribeArray=@[];
        
        NSString * unSubscribeString=[NSString stringWithFormat:@"&subscriptiontype=%@",unSubscribeArray];
        NSString * unSub = [unSubscribeString stringByReplacingOccurrencesOfString:@"(" withString:@"{"];
        NSString * unSub1 = [unSub stringByReplacingOccurrencesOfString:@")" withString:@"}"];

        
        NSString * btnTitle=[NSString stringWithFormat:@"%@",cell.followBtn.currentTitle];
        
        
        if([btnTitle isEqualToString:@"Following"])
            
        {
            [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[unSub1 dataUsingEncoding:NSUTF8StringEncoding]];
            
        }
        else if ([btnTitle isEqualToString:@"Follow +"])
        {
            [postData appendData:[@"&subscribe=true" dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[sub1 dataUsingEncoding:NSUTF8StringEncoding]];
            
        }
        
        
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower",delegate1.baseUrl]]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            if([httpResponse statusCode]==403)
                                                            {
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                
                                                            }
                                                            else if([httpResponse statusCode]==401)
                                                            {
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                
                                                            }
                                                            else
                                                            {
                                                                
                                                            NSMutableDictionary *followDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            
                                                            //NSLog(@"%@",followDict);
                                                            
                                                            messageStr=[followDict objectForKey:@"message"];
                                                            }
                                                            
                                                            
                                                        }
                                                        
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            
                                                            if([messageStr isEqualToString:@"Subscriber created"])
                                                            {
                                                                //                                                            [cell.followBtn setTitle:[self.buttonArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
                                                                [self.topPicksTbl reloadData];
                                                                
                                                                [self topPicksServer];
                                                            }
                                                        });
                                                        
                                                        
                                                        
                                                    }];
        [dataTask resume];
        
        

        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    }
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
    if(tableView==self.topPicksTbl)
    {
    delegate1.navigationCheck=@"TOP";

    
    
   
        if(delegate1.leaderAdviceDetails.count>0)
        {
            [delegate1.leaderAdviceDetails removeAllObjects];
        }
        //    if(tableView==self.equitiesTableView&&segmentedControl.selectedSegmentIndex==0)
        //    {
        //
        //        if(indexPath.row==2||indexPath.row==7||indexPath.row==15||indexPath.row==12||indexPath.row==18)
        //        {
        //            StockAllocationView * allocation=[self.storyboard instantiateViewControllerWithIdentifier:@"stockallocation"];
        //
        //            [self.navigationController pushViewController:allocation animated:YES];
        //
        //        }
        
        //        else{
        delegate1.depth=@"WISDOM";
        
        //NSLog(@"%@",delegate1.depth);
    
    
    NSDictionary * openAdvice=[[[self.topPicksResponseArray objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"open_advice"];
    
    NSString * public=[NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"ispublic"]];
    int publicInt=[public intValue];
    if(publicInt==0)
    {
        
        StockView1 * stockDetails=[self.storyboard instantiateViewControllerWithIdentifier:@"stock1"];
    
    @try {
        delegate1.symbolDepthStr=[openAdvice objectForKey:@"companyname"];
        
        delegate1.exchaneLblStr=[openAdvice objectForKey:@"segmenttype"];
        
          delegate1.instrumentDepthStr=[openAdvice objectForKey:@"instrumentid"];
        
          delegate1.expirySeriesValue=[openAdvice objectForKey:@"expirydate"];
        
        delegate1.expirySeriesValue=@"";
        NSString * firstName=[NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"firstname"]];
        NSString * lastName=[NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"lastname"]];
        
        if([firstName isEqualToString:@"<null>"])
        {
            firstName=@"";
        }
        
        if([lastName isEqualToString:@"<null>"])
        {
            lastName=@"";
        }
        NSString * name=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
        [delegate1.leaderAdviceDetails addObject:name];
        [delegate1.leaderAdviceDetails addObject:[openAdvice objectForKey:@"createdon"]];
        [delegate1.leaderAdviceDetails addObject:[openAdvice objectForKey:@"lasttradedprice"]];
        
        [delegate1.leaderAdviceDetails addObject:[openAdvice objectForKey:@"changepercent"]];
        
        [delegate1.leaderAdviceDetails addObject:[openAdvice objectForKey:@"message"]];
        [delegate1.leaderAdviceDetails addObject:[openAdvice objectForKey:@"leaderid"]];
        
        [delegate1.leaderAdviceDetails addObject:[openAdvice objectForKey:@"messageid"]];
        [delegate1.leaderAdviceDetails addObject:[openAdvice objectForKey:@"buysell"]];
        NSString * logo=[openAdvice objectForKey:@"logourl"];
        if([logo isEqual:[NSNull null]])
        {
            [delegate1.leaderAdviceDetails addObject:@"no"];
        }
        
        else
        {
            [delegate1.leaderAdviceDetails addObject:logo];
        }
        
        //NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
        
        [self.navigationController pushViewController:stockDetails animated:YES];

        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    }else if (publicInt==1)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"This is a publicly available information from the above source. All users are advised, in their own interests, to seek and obtain advice that is specific or tailored to their own circumstances and that any reliance by any user on any such publicly available advice is entirely at the risk and consequence of such user, for which Zenwise Technologies Private Limited under the brand name Zambala is not responsible or liable." preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            [self presentViewController:alert animated:YES completion:^{
                
                
                
            }];
            
            
            
            UIAlertAction * proceedAction=[UIAlertAction actionWithTitle:@"Proceed" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
               
                StockView1 * stockDetails=[self.storyboard instantiateViewControllerWithIdentifier:@"stock1"];
                
                @try {
                    delegate1.symbolDepthStr=[openAdvice objectForKey:@"companyname"];
                    
                    delegate1.exchaneLblStr=[openAdvice objectForKey:@"segmenttype"];
                    
                    delegate1.instrumentDepthStr=[openAdvice objectForKey:@"instrumentid"];
                    
                    delegate1.expirySeriesValue=[openAdvice objectForKey:@"expirydate"];
                    
                    delegate1.expirySeriesValue=@"";
                    NSString * firstName=[NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"firstname"]];
                    NSString * lastName=[NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"lastname"]];
                    
                    if([firstName isEqualToString:@"<null>"])
                    {
                        firstName=@"";
                    }
                    
                    if([lastName isEqualToString:@"<null>"])
                    {
                        lastName=@"";
                    }
                    NSString * name=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                    [delegate1.leaderAdviceDetails addObject:name];
                    [delegate1.leaderAdviceDetails addObject:[openAdvice objectForKey:@"createdon"]];
                    [delegate1.leaderAdviceDetails addObject:[openAdvice objectForKey:@"lasttradedprice"]];
                    
                    [delegate1.leaderAdviceDetails addObject:[openAdvice objectForKey:@"changepercent"]];
                    
                    [delegate1.leaderAdviceDetails addObject:[openAdvice objectForKey:@"message"]];
                    [delegate1.leaderAdviceDetails addObject:[openAdvice objectForKey:@"leaderid"]];
                    
                    [delegate1.leaderAdviceDetails addObject:[openAdvice objectForKey:@"messageid"]];
                    [delegate1.leaderAdviceDetails addObject:[openAdvice objectForKey:@"buysell"]];
                    NSString * logo=[openAdvice objectForKey:@"logourl"];
                    if([logo isEqual:[NSNull null]])
                    {
                        [delegate1.leaderAdviceDetails addObject:@"no"];
                    }
                    
                    else
                    {
                        [delegate1.leaderAdviceDetails addObject:logo];
                    }
                    
                    //NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
                    
                    [self.navigationController pushViewController:stockDetails animated:YES];
                    
                    
                }
                @catch (NSException * e) {
                    //NSLog(@"Exception: %@", e);
                }
                @finally {
                    //NSLog(@"finally");
                }
            }];
            
            UIAlertAction * cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                
                
            }];
            
            
            
            
            [alert addAction:proceedAction];
            [alert addAction:cancelAction];
            
        });
        
    }
    
    
    }else if (tableView==self.topPerformersTblView)
    {
         TopPerformersCell *cell1 = [tableView dequeueReusableCellWithIdentifier:@"TopPerformersCell" forIndexPath:indexPath];
    }
     }
     @catch (NSException * e) {
         //NSLog(@"Exception: %@", e);
     }
     @finally {
         //NSLog(@"finally");
     }
}

-(void)topPicksleaderProfileMethod:(UIButton *)sender
{
    @try
    {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.topPicksTbl];
    
    
    
    NSIndexPath *indexPath = [self.topPicksTbl indexPathForRowAtPoint:buttonPosition];
    
    //NSLog(@"Index path:%ld",(long)indexPath.row);
    
    
    TopPicksCell *cell = [self.topPicksTbl cellForRowAtIndexPath:indexPath];
     NSDictionary * openAdvice=[[[self.topPicksResponseArray objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"open_advice"];
    NSString * public=[NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"ispublic"]];
    int publicInt=[public intValue];
    
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
       if(publicInt==1)
       {
           
       }else if (publicInt==0)
       {
        

    
    @try {
        if([[openAdvice objectForKey:@"portfoliojson"] isEqual:[NSNull null]])
        {
            
            delegate1.leaderIDWhoToFollow=[openAdvice objectForKey:@"leaderid"];
        }
        
        NewProfile * new=[self.storyboard instantiateViewControllerWithIdentifier:@"NewProfile"];
        
        [self.navigationController pushViewController:new animated:YES];

        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
        
    }
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}


-(void)yourButtonClicked1:(UIButton*)sender
{
    @try
    {
    delegate1.navigationCheck=@"TOP";
    delegate1.wisdomCheck=true;
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.topPicksTbl];
    
  
    
    NSIndexPath *indexPath = [self.topPicksTbl indexPathForRowAtPoint:buttonPosition];
       NSDictionary * openAdvice=[[[self.topPicksResponseArray objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"open_advice"];
    //NSLog(@"Index path:%ld",(long)indexPath.row);
    TopPicksCell *cell = [self.topPicksTbl cellForRowAtIndexPath:indexPath];
    
    
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
        NSString * public=[NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"ispublic"]];
        int publicInt=[public intValue];
        
        if(publicInt==0)
        {
    
        
        
        @try {
            delegate1.symbolDepthStr=[openAdvice objectForKey:@"companyname"];
            
            delegate1.tradingSecurityDes=[NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"symbolname"]];
            
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
        
        @try {
            delegate1.orderSegment=[openAdvice objectForKey:@"segmenttype"];
            
            
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
        
        
        delegate1.orderBuySell=cell.buySellButton.currentTitle;
       
        if([delegate1.brokerNameStr isEqualToString:@"Zerodha"])
        {
             delegate1.orderinstrument=[openAdvice objectForKey:@"instrumentid"];
        }
        
        else
        {
             delegate1.orderinstrument=[NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"exchangetoken"]];
            
          
        }
        
    
    

        if(delegate1.leaderAdviceDetails.count>0)
        {
            [delegate1.leaderAdviceDetails removeAllObjects];
        }
        //    if(tableView==self.equitiesTableView&&segmentedControl.selectedSegmentIndex==0)
        //    {
        //
        //        if(indexPath.row==2||indexPath.row==7||indexPath.row==15||indexPath.row==12||indexPath.row==18)
        //        {
        //            StockAllocationView * allocation=[self.storyboard instantiateViewControllerWithIdentifier:@"stockallocation"];
        //
        //            [self.navigationController pushViewController:allocation animated:YES];
        //
        //        }
        
        //        else{
        delegate1.depth=@"WISDOM";
        
        //NSLog(@"%@",delegate1.depth);
        
        
        
        //        delegate1.symbolDepthStr=[[self.responseArray objectAtIndex:indexPath.row] objectForKey:@"companyname"];
        //
        //        delegate1.exchaneLblStr=[[self.responseArray objectAtIndex:indexPath.row] objectForKey:@"segmenttype"];
        
    @try {
        NSString * firstName=[NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"firstname"]];
        NSString * lastName=[NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"lastname"]];
        
        if([firstName isEqualToString:@"<null>"])
        {
            firstName=@"";
        }
        
        if([lastName isEqualToString:@"<null>"])
        {
            lastName=@"";
        }
        NSString * name=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
        [delegate1.leaderAdviceDetails addObject:name];
        [delegate1.leaderAdviceDetails addObject:[openAdvice objectForKey:@"createdon"]];
        [delegate1.leaderAdviceDetails addObject:[openAdvice objectForKey:@"lasttradedprice"]];
        
        [delegate1.leaderAdviceDetails addObject:[openAdvice objectForKey:@"changepercent"]];
        
        [delegate1.leaderAdviceDetails addObject:[openAdvice objectForKey:@"message"]];
        
        NSString * logo=[NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"logourl"]];
        
        if([logo isEqual:[NSNull null]])
        {
            [delegate1.leaderAdviceDetails addObject:@"no"];
        }
        
        else
        {
            [delegate1.leaderAdviceDetails addObject:logo];
        }
        
        
        //NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
        
        
        //NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
        NSString * subscribedStr = [NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"subscribed"]];
        
        if([subscribedStr isEqualToString:@"0"])
        {
            
            [self followMethod:openAdvice];
        

            
        }
        
        else
        {
            
            
            StockOrderView * orderView=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
            
            [self.navigationController pushViewController:orderView animated:YES];
        }
        
        
        
        
      
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
        
    
    
    

    }else if (publicInt==1)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"This is a publicly available information from the above source. All users are advised, in their own interests, to seek and obtain advice that is specific or tailored to their own circumstances and that any reliance by any user on any such publicly available advice is entirely at the risk and consequence of such user, for which Zenwise Technologies Private Limited under the brand name Zambala is not responsible or liable." preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            [self presentViewController:alert animated:YES completion:^{
                
                
                
            }];
            
            
            
            UIAlertAction * proceedAction=[UIAlertAction actionWithTitle:@"Proceed" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
              
                    
                    @try {
                        delegate1.symbolDepthStr=[openAdvice objectForKey:@"companyname"];
                        
                        delegate1.tradingSecurityDes=[NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"symbolname"]];
                        
                    }
                    @catch (NSException * e) {
                        //NSLog(@"Exception: %@", e);
                    }
                    @finally {
                        //NSLog(@"finally");
                    }
                    
                    @try {
                        
                        
                        delegate1.orderSegment=[openAdvice objectForKey:@"segmenttype"];
                      
                        
                    }
                    @catch (NSException * e) {
                        //NSLog(@"Exception: %@", e);
                    }
                    @finally {
                        //NSLog(@"finally");
                    }
                    
                    
                    delegate1.orderBuySell=cell.buySellButton.currentTitle;
                    
                    if([delegate1.brokerNameStr isEqualToString:@"Zerodha"])
                    {
                        delegate1.orderinstrument=[openAdvice objectForKey:@"instrumentid"];
                    }
                    
                    else
                    {
                        delegate1.orderinstrument=[NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"exchangetoken"]];
                        
                        
                    }
                    
                    
                    
                    
                    if(delegate1.leaderAdviceDetails.count>0)
                    {
                        [delegate1.leaderAdviceDetails removeAllObjects];
                    }
                    //    if(tableView==self.equitiesTableView&&segmentedControl.selectedSegmentIndex==0)
                    //    {
                    //
                    //        if(indexPath.row==2||indexPath.row==7||indexPath.row==15||indexPath.row==12||indexPath.row==18)
                    //        {
                    //            StockAllocationView * allocation=[self.storyboard instantiateViewControllerWithIdentifier:@"stockallocation"];
                    //
                    //            [self.navigationController pushViewController:allocation animated:YES];
                    //
                    //        }
                    
                    //        else{
                    delegate1.depth=@"WISDOM";
                    
                    //NSLog(@"%@",delegate1.depth);
                    
                    
                    
                    //        delegate1.symbolDepthStr=[[self.responseArray objectAtIndex:indexPath.row] objectForKey:@"companyname"];
                    //
                    //        delegate1.exchaneLblStr=[[self.responseArray objectAtIndex:indexPath.row] objectForKey:@"segmenttype"];
                    
                    @try {
                        NSString * firstName=[NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"firstname"]];
                        NSString * lastName=[NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"lastname"]];
                        
                        if([firstName isEqualToString:@"<null>"])
                        {
                            firstName=@"";
                        }
                        
                        if([lastName isEqualToString:@"<null>"])
                        {
                            lastName=@"";
                        }
                        NSString * name=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                        [delegate1.leaderAdviceDetails addObject:name];
                        [delegate1.leaderAdviceDetails addObject:[openAdvice objectForKey:@"createdon"]];
                        [delegate1.leaderAdviceDetails addObject:[openAdvice objectForKey:@"lasttradedprice"]];
                        
                        [delegate1.leaderAdviceDetails addObject:[openAdvice objectForKey:@"changepercent"]];
                        
                        [delegate1.leaderAdviceDetails addObject:[openAdvice objectForKey:@"message"]];
                        
                        NSString * logo=[NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"logourl"]];
                        
                        if([logo isEqual:[NSNull null]])
                        {
                            [delegate1.leaderAdviceDetails addObject:@"no"];
                        }
                        
                        else
                        {
                            [delegate1.leaderAdviceDetails addObject:logo];
                        }
                        
                        
                        //NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
                        
                        
                        //NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
                        
                        
//                        if([cell.followBtn.currentTitle isEqualToString:@"Follow +"])
//                        {
//
//                            NSDictionary *headers = @{ @"content-type": @"application/x-www-form-urlencoded",
//                                                       @"cache-control": @"no-cache",
//                                                       };
//
//                            NSString * userId=[NSString stringWithFormat:@"leaderid=%@",[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"leaderid"]];
//
//                            NSString * clientId=[NSString stringWithFormat:@"&clientid=%@",delegate1.userID];
//
//
//                            NSMutableData *postData = [[NSMutableData alloc] initWithData:[userId dataUsingEncoding:NSUTF8StringEncoding]];
//                            [postData appendData:[clientId dataUsingEncoding:NSUTF8StringEncoding]];
//                            //    if(segmentedControl.selectedSegmentIndex==0)
//                            //    {
//                            //         [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
//                            //    }
//                            //    else
//                            //    {
//                            //    if(self.buttonArray.count>0)
//                            //    {
//                            //    if([[self.buttonArray objectAtIndex:indexPath.row] isEqualToString:@"Follow +"])
//                            //    {
//                            //    [postData appendData:[@"&subscribe=true" dataUsingEncoding:NSUTF8StringEncoding]];
//                            //    }else if([[self.buttonArray objectAtIndex:indexPath.row] isEqualToString:@"Following"])
//                            //    {
//                            //        [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
//                            //
//                            //    }
//                            //    }
//                            //    }
//
//                            NSArray * subscribeArray=@[@1];
//
//                            NSString * subscribeString=[NSString stringWithFormat:@"&subscriptiontype=%@",subscribeArray];
//                            NSString * sub = [subscribeString stringByReplacingOccurrencesOfString:@"(" withString:@"{"];
//                            NSString * sub1 = [sub stringByReplacingOccurrencesOfString:@")" withString:@"}"];
//
//                            NSArray * unSubscribeArray=@[];
//
//                            NSString * unSubscribeString=[NSString stringWithFormat:@"&subscriptiontype=%@",unSubscribeArray];
//                            NSString * unSub = [unSubscribeString stringByReplacingOccurrencesOfString:@"(" withString:@"{"];
//                            NSString * unSub1 = [unSub stringByReplacingOccurrencesOfString:@")" withString:@"}"];
//
//
//                            NSString * btnTitle=[NSString stringWithFormat:@"%@",cell.followBtn.currentTitle];
//
//
//                            if([btnTitle isEqualToString:@"Following"])
//
//                            {
//                                [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
//                                [postData appendData:[unSub1 dataUsingEncoding:NSUTF8StringEncoding]];
//
//
//
//                            }
//                            else if ([btnTitle isEqualToString:@"Follow +"])
//                            {
//                                [postData appendData:[@"&subscribe=true" dataUsingEncoding:NSUTF8StringEncoding]];
//                                [postData appendData:[sub1 dataUsingEncoding:NSUTF8StringEncoding]];
//
//
//
//                            }
//
//
//
//                            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower",delegate1.baseUrl]]
//                                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                                               timeoutInterval:10.0];
//                            [request setHTTPMethod:@"POST"];
//                            [request setAllHTTPHeaderFields:headers];
//                            [request setHTTPBody:postData];
//
//                            NSURLSession *session = [NSURLSession sharedSession];
//                            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
//                                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                                                                            if (error) {
//                                                                                //NSLog(@"%@", error);
//                                                                            } else {
//                                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
//                                                                                //NSLog(@"%@", httpResponse);
//
//                                                                                NSMutableDictionary *followDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//
//                                                                                //NSLog(@"%@",followDict);
//
//                                                                                messageStr=[followDict objectForKey:@"message"];
//
//
//
//                                                                            }
//
//
//                                                                            dispatch_async(dispatch_get_main_queue(), ^{
//
//
//                                                                                if([messageStr isEqualToString:@"Subscriber created"])
//                                                                                {
//                                                                                    //                                                            [cell.followBtn setTitle:[self.buttonArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
//                                                                                    //                                                                    [self.topPicksTbl reloadData];
//                                                                                    //
//                                                                                    //                                                                    [self topPicksServer];
//
//                                                                                    StockOrderView * orderView=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
//
//                                                                                    [self.navigationController pushViewController:orderView animated:YES];
//                                                                                }
//                                                                            });
//
//
//
//                                                                        }];
//                            [dataTask resume];
//
//
//
//
//
//
//
//
//
//                        }
                        
                       // else
                        //{
                        NSString * subscribedStr = [NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"subscribed"]];
                        if([subscribedStr isEqualToString:@"0"])
                        {
                            
                            [self followMethod:openAdvice];
                            
                            
                            
                        }
                        else
                        {
                            
                            StockOrderView * orderView=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
                            
                            [self.navigationController pushViewController:orderView animated:YES];
                        }
                       // }
                        
                        
                        
                        
                        
                        
                    }
                    @catch (NSException * e) {
                        //NSLog(@"Exception: %@", e);
                    }
                    @finally {
                        //NSLog(@"finally");
                    }
                    
                    
                    
                    
                    
                
                
            }];
            
            UIAlertAction * cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                
                
            }];
            
            
            
            
            [alert addAction:proceedAction];
            [alert addAction:cancelAction];
            
        });
        
    }
    
    }
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

//follow//

-(void)followMethod:(NSDictionary *)openAdvice
{
    
   
        
        NSDictionary *headers = @{ @"content-type": @"application/x-www-form-urlencoded",
                                   @"cache-control": @"no-cache",
                                   @"authtoken":delegate1.zenwiseToken
                                   };
        
        NSString * userId=[NSString stringWithFormat:@"leaderid=%@",[openAdvice objectForKey:@"leaderid"]];
        
        NSString * clientId=[NSString stringWithFormat:@"&clientid=%@",delegate1.userID];
        
        
        NSMutableData *postData = [[NSMutableData alloc] initWithData:[userId dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[clientId dataUsingEncoding:NSUTF8StringEncoding]];
        //    if(segmentedControl.selectedSegmentIndex==0)
        //    {
        //         [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
        //    }
        //    else
        //    {
        //    if(self.buttonArray.count>0)
        //    {
        //    if([[self.buttonArray objectAtIndex:indexPath.row] isEqualToString:@"Follow +"])
        //    {
        //    [postData appendData:[@"&subscribe=true" dataUsingEncoding:NSUTF8StringEncoding]];
        //    }else if([[self.buttonArray objectAtIndex:indexPath.row] isEqualToString:@"Following"])
        //    {
        //        [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
        //
        //    }
        //    }
        //    }
        
        NSArray * subscribeArray=@[@1];
        
        NSString * subscribeString=[NSString stringWithFormat:@"&subscriptiontype=%@",subscribeArray];
        NSString * sub = [subscribeString stringByReplacingOccurrencesOfString:@"(" withString:@"{"];
        NSString * sub1 = [sub stringByReplacingOccurrencesOfString:@")" withString:@"}"];
        
        NSArray * unSubscribeArray=@[];
        
        NSString * unSubscribeString=[NSString stringWithFormat:@"&subscriptiontype=%@",unSubscribeArray];
        NSString * unSub = [unSubscribeString stringByReplacingOccurrencesOfString:@"(" withString:@"{"];
        NSString * unSub1 = [unSub stringByReplacingOccurrencesOfString:@")" withString:@"}"];
        
        
        NSString * btnTitle=[NSString stringWithFormat:@"%@",[openAdvice objectForKey:@"subscribed"] ];
        
        
        if([btnTitle isEqualToString:@"Following"])
            
        {
            [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[unSub1 dataUsingEncoding:NSUTF8StringEncoding]];
            
            
            
        }
        else if ([btnTitle isEqualToString:@"Follow +"])
        {
            [postData appendData:[@"&subscribe=true" dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[sub1 dataUsingEncoding:NSUTF8StringEncoding]];
            
            
            
        }
        
        
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower",delegate1.baseUrl]]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            if([httpResponse statusCode]==403)
                                                            {
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                
                                                            }
                                                            else if([httpResponse statusCode]==401)
                                                            {
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                
                                                            }
                                                            else
                                                            {
                                                                
                                                                NSMutableDictionary *followDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                
                                                                //NSLog(@"%@",followDict);
                                                                
                                                                messageStr=[followDict objectForKey:@"message"];
                                                                
                                                            }
                                                            
                                                        }
                                                        
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            
                                                            if([messageStr isEqualToString:@"Subscriber created"])
                                                            {
                                                                //                                                            [cell.followBtn setTitle:[self.buttonArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
                                                                //                                                                    [self.topPicksTbl reloadData];
                                                                //
                                                                //                                                                    [self topPicksServer];
                                                                
                                                                StockOrderView * orderView=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
                                                                
                                                                [self.navigationController pushViewController:orderView animated:YES];
                                                            }
                                                        });
                                                        
                                                        
                                                        
                                                    }];
        [dataTask resume];
        
        
        
        
        
        
        
        
}


//REACHABILITY//

-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    
    
}

-(BOOL)networkStatus
{
    
    
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    
    if(netStatus==NotReachable)
    {
        
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    return YES;
    
    
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
//    @try
//    {
//        if ([string isEqualToString:@""]) {
//            //NSLog(@"Backspace");
//        }else
//        {
//        self.searchStr = [self.searchFld.text stringByReplacingCharactersInRange:range withString:string];
//
//        [self segmentedControlChangedValue];
//        }
//    }
//    @catch (NSException * e) {
//        //NSLog(@"Exception: %@", e);
//    }
//    @finally {
//        //NSLog(@"finally");
//    }
    
    return YES;
    
}
typedef NS_ENUM(NSInteger, ScrollDirection) {
    ScrollDirectionNone,
    ScrollDirectionRight,
    ScrollDirectionLeft,
    ScrollDirectionUp,
    ScrollDirectionDown,
    ScrollDirectionCrazy,
};
- (IBAction)onInfoTap:(id)sender {
    PotentialGainersPopUpViewController *detailViewController = [[PotentialGainersPopUpViewController alloc] initWithNibName:@"PotentialGainersPopUpViewController" bundle:nil];
    [self presentPopupViewController:detailViewController animationType: MJPopupViewAnimationFade];
}
@end
