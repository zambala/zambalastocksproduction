//
//  RedeemView.m
//  betazambalafollower
//
//  Created by Zenwise Technologies on 17/08/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import "RedeemView.h"
#import "AppDelegate.h"
#import "VoucherCellCollectionViewCell.h"
#import "TagEncode.h"
#import "UIImageView+WebCache.h"
#import <QuartzCore/QuartzCore.h>
#import "RedeemTermsViewController.h"
@import Mixpanel;

@interface RedeemView ()
{
    AppDelegate * delegate1;
    UIToolbar * toolbar;
    NSString * categoryString;
    int zambalaPointInt;
    float rupeeInt;
    float finalPointValue;
    float minValueInt;
    float finalMinPointValue;
}

@end

@implementation RedeemView

- (void)viewDidLoad {
    [super viewDidLoad];
    @try
    {
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
        Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
        [mixpanelMini track:@"redeem_brand_page"];
        
//    self.zambalaPointValue=@"2";
//    self.rupeeString=@"1";
    self.walletPointsLbl.text=[NSString stringWithFormat:@"You currently have %@ Zambala Points in your Wallet",delegate1.rewardPoints];
    if([self.minZambalaPoints isEqualToString:@"0"])
    {
        self.minZambalaPointsLabel.hidden=YES;
    }else
    {
        self.minZambalaPointsLabel.hidden=NO;
        self.minZambalaPointsLabel.text = [NSString stringWithFormat:@"Note: You need to have minimum %@ Zambala Points in your wallet.",self.minZambalaPoints];
    }
   
       
    [self.chooseCatagoryBtn setTitle:@"Categories  " forState:UIControlStateNormal];
    self.chooseCatagoryBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16] CGColor];
    self.chooseCatagoryBtn.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    self.chooseCatagoryBtn.layer.shadowOpacity = 1.0f;
    self.chooseCatagoryBtn.layer.shadowRadius = 2.0f;
    self.chooseCatagoryBtn.layer.cornerRadius=5.0f;
    self.chooseCatagoryBtn.layer.masksToBounds = NO;
    self.doneView.hidden=YES;
    self.categoryPickerView.hidden=YES;
    self.categoryArray=[[NSMutableArray alloc]init];
    self.getListArray = [[NSMutableArray alloc]init];
    zambalaPointInt = [self.zambalaPointValue intValue];
    rupeeInt = [self.rupeeString floatValue];
    minValueInt = [self.minZambalaPoints floatValue];
    finalMinPointValue = [delegate1.rewardPoints floatValue]-minValueInt;
    finalPointValue = (finalMinPointValue*rupeeInt)/zambalaPointInt;
    //NSLog(@"%f",finalPointValue);
    NSString * pointString = [NSString stringWithFormat:@"(Every %@ Zambala Points = ₹%@)",self.zambalaPointValue,self.rupeeString];
    self.zambalaPointValueLbl.text = pointString;
    [self getBrandsList];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
     categoryString = @"All";
}

-(void)getBrandsList
{
    @try
    {
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    
        NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
       NSString*number= [loggedInUser stringForKey:@"profilemobilenumber"];
    
   
        NSDictionary *headers = @{ @"authtoken":delegate1.zenwiseToken,
                                   @"deviceid":delegate1.currentDeviceId,
                                   @"mobilenumber":number
                            };
    
        NSString * url = [NSString stringWithFormat:@"%@voucher/list",delegate1.baseUrl];
        
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==200)
                                                        {
                                                            self.getListReponseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            self.getListArray = [[NSMutableArray alloc]initWithArray:[self.getListReponseDictionary objectForKey:@"data"]];
                                                            [self.categoryArray addObject:@"All"];
                                                            for (int i=0; i<[[self.getListReponseDictionary objectForKey:@"data"] count]; i++) {
                                                                [self.categoryArray addObject:[[[self.getListReponseDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"category"]];
                                                            }
                                                            NSOrderedSet *mySet = [[NSOrderedSet alloc] initWithArray:self.categoryArray];
                                                            self.categoryArray = [[NSMutableArray alloc] initWithArray:[mySet array]];
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                self.activityIndicator.hidden=YES;
                                                                [self.activityIndicator stopAnimating];
                                                                self.vouchersCollectionView.delegate = self;
                                                                self.vouchersCollectionView.dataSource = self;
                                                                [self.vouchersCollectionView reloadData];
                                                            });
                                                        }else
                                                        {
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Sorry, we are unable to get the vouchers at the moment." preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                self.activityIndicator.hidden=YES;
                                                                [self.activityIndicator stopAnimating];
                                                            }];
                                                            UIAlertAction * tryAgain=[UIAlertAction actionWithTitle:@"Try again" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                [self getBrandsList];
                                                            }];
                                                            
                                                            [alert addAction:okAction];
                                                            [alert addAction:tryAgain];
                                                        }
                                                    }
                                                }];
    [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
   
    return self.getListArray.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((collectionView.frame.size.width/2)-20, 125);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
    VoucherCellCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"VoucherCellCollectionViewCell" forIndexPath:indexPath];
   
//    cell.voucherView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16] CGColor];
//    cell.voucherView.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
//    cell.voucherView.layer.shadowOpacity = 1.0f;
//    cell.voucherView.layer.shadowRadius = 2.0f;
    cell.voucherView.layer.cornerRadius=5.0f;
    cell.voucherView.clipsToBounds = YES;
    
    cell.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16] CGColor];
    cell.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    cell.layer.shadowOpacity = 1.0f;
    cell.layer.shadowRadius = 2.0f;
    cell.layer.cornerRadius=5.0f;
    cell.layer.masksToBounds = NO;
    
    float pointDenomination = [[[[self.getListArray objectAtIndex:indexPath.row] objectForKey:@"denomination"] objectAtIndex:0] floatValue];
    if(finalPointValue>pointDenomination||finalPointValue==pointDenomination)
    {
         cell.voucherBackView.hidden=YES;
    }else
    {
        cell.voucherBackView.hidden=NO;
    }
    
    NSString * vaucherImage = [NSString stringWithFormat:@"%@",[[self.getListArray objectAtIndex:indexPath.row]objectForKey:@"logo_link"]];
     [cell.voucherImageView sd_setImageWithURL:[NSURL URLWithString:vaucherImage] placeholderImage:[UIImage imageNamed:@""]];
    NSString * vaucherValueLabel = [NSString stringWithFormat:@"%@",[[[self.getListArray  objectAtIndex:indexPath.row]objectForKey:@"denomination"]objectAtIndex:0]];
    cell.voucherValueLbl.text = [NSString stringWithFormat:@"Starts from ₹%@",vaucherValueLabel];
    return cell;
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
    float pointDenomination = [[[[self.getListArray objectAtIndex:indexPath.row] objectForKey:@"denomination"] objectAtIndex:0] floatValue];
    if(finalPointValue>pointDenomination||finalPointValue==pointDenomination)
    {
        RedeemTermsViewController * terms = [self.storyboard instantiateViewControllerWithIdentifier:@"RedeemTermsViewController"];
        terms.getListArray = [self.getListArray objectAtIndex:indexPath.row];
        terms.zambalaPointValue=self.zambalaPointValue;
        terms.rupeeString = self.rupeeString;
        terms.minZambalaPoints = self.minZambalaPoints;
        [self presentViewController:terms animated:YES completion:nil];
    }else
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Sorry, you don't have enough points to redeem this voucher." preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.activityIndicator.hidden=YES;
            [self.activityIndicator stopAnimating];
        }];
       
        [alert addAction:okAction];
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onChoseeTap:(id)sender {
    self.categoryPickerView.hidden=NO;
    self.doneView.hidden=NO;
    [self.categoryPickerView setBackgroundColor:[UIColor colorWithRed:(248.0/255.0) green:(248.0/255.0) blue:(248.0/255.0) alpha:1.0]];
//    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(onPickerDoneButtonTap)];
//    UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
//    toolbar= [[UIToolbar alloc]initWithFrame:CGRectMake(0,0,self.doneView.frame.size.width,self.doneView.frame.size.height)];
//    //toolbar = [[UIToolbar alloc]init];
//    NSArray *toolbarItems = [NSArray arrayWithObjects:flexible,doneButton,nil];
//    [toolbar setItems:toolbarItems];
//    [self.doneView addSubview:toolbar];
    
    [self.doneButton addTarget:self action:@selector(onPickerDoneButtonTap) forControlEvents:UIControlEventTouchUpInside];
    self.categoryPickerView.delegate=self;
    self.categoryPickerView.dataSource=self;
}
-(void)onPickerDoneButtonTap
{
    @try
    {
    self.doneView.hidden=YES;
    self.categoryPickerView.hidden=YES;
    if([categoryString isEqualToString:@"All"])
    {
        [self.chooseCatagoryBtn setTitle:@"Categories  " forState:UIControlStateNormal];
        self.getListArray = [[NSMutableArray alloc]initWithArray:[self.getListReponseDictionary objectForKey:@"data"]];
    }else
    {
    [self.getListArray removeAllObjects];
    for (int i=0; i<[[self.getListReponseDictionary objectForKey:@"data"] count]; i++) {
        if([[[[self.getListReponseDictionary objectForKey:@"data"] objectAtIndex:i] objectForKey:@"category"]isEqualToString:categoryString])
        {
            [self.getListArray addObject:[[self.getListReponseDictionary objectForKey:@"data"]objectAtIndex:i]];
        }
    }
    }
    if(self.getListArray.count>0)
    {
        [self.vouchersCollectionView reloadData];
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView
numberOfRowsInComponent:(NSInteger)component {
    return self.categoryArray.count;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [NSString stringWithFormat:@"%@",[self.categoryArray objectAtIndex:row]];//Or, your suitable title; like Choice-a, etc.
}
- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    [self.chooseCatagoryBtn setTitle:[NSString stringWithFormat:@"%@  ",[self.categoryArray objectAtIndex:row]] forState:UIControlStateNormal];
    categoryString = [NSString stringWithFormat:@"%@",[self.categoryArray objectAtIndex:row]];
}

- (IBAction)backAction:(id)sender {
      [self dismissViewControllerAnimated:YES completion:nil];
}
@end
