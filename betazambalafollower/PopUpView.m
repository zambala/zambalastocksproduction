//
//  PopUpView.m
//  testing
//
//  Created by zenwise mac 2 on 11/25/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "PopUpView.h"
#import "AppDelegate.h"

@interface PopUpView ()

@end

@implementation PopUpView
{
    AppDelegate * delegate;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if([delegate.equities_Derivatives isEqualToString:@"EQUITY"])
    {
        self.derivativeImageView.hidden=YES;
        self.equityImageView.hidden=NO;
    }else if([delegate.equities_Derivatives isEqualToString:@"DERIVATIVES"])
    {
        self.derivativeImageView.hidden=NO;
        self.equityImageView.hidden=YES;
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
