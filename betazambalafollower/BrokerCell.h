//
//  BrokerCell.h
//  testing
//
//  Created by zenwise technologies on 28/02/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrokerCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *brokerImgView;
@property (strong, nonatomic) IBOutlet UILabel *brokerTitle;
@property (strong, nonatomic) IBOutlet UILabel *brokerDetail;
@property (strong, nonatomic) IBOutlet UIButton *moreDetails;
@property (strong, nonatomic) IBOutlet UIView *brokerView;

@end
