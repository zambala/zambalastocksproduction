//
//  NewProfileSettings.h
//  
//
//  Created by zenwise technologies on 21/03/17.
//
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import <QuartzCore/QuartzCore.h>

@interface NewProfileSettings : UIViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property (strong, nonatomic) IBOutlet UIView *tradeView;
@property (strong, nonatomic) IBOutlet UIView *personalView;
- (IBAction)brokerNameTickerTap:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *brokerNameTickerBtn;
@property (strong, nonatomic) IBOutlet UIButton *showTickerBtn;
- (IBAction)showTickerTap:(id)sender;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityInd;

@property (strong, nonatomic) IBOutlet UIButton *notificationTickerBtn;
- (IBAction)notificationTickerTap:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *adviceFrmLeadersBtn;
- (IBAction)backButtonTap:(id)sender;
@property Reachability * reach;

@property (strong, nonatomic) IBOutlet UIButton *marketPriceAlertsBtn;
@property (strong, nonatomic) IBOutlet UIButton *triggerPriceAlertBtn;
@property (strong, nonatomic) IBOutlet UIButton *stopLossBtn;
@property (strong, nonatomic) IBOutlet UIButton *orderExecutionBtn;
@property (strong, nonatomic) IBOutlet UIButton *equitiesBtn;
@property (strong, nonatomic) IBOutlet UIButton *derivativesBtn;
@property (strong, nonatomic) IBOutlet UIButton *currencyBtn;
@property (strong, nonatomic) IBOutlet UIButton *commodityBtn;
@property (strong, nonatomic) IBOutlet UIButton *dayTradeBtn;
@property (strong, nonatomic) IBOutlet UIButton *shortTermBtn;
@property (strong, nonatomic) IBOutlet UIButton *longTermBtn;
@property (strong, nonatomic) IBOutlet UIButton *BSEBtn;
@property (strong, nonatomic) IBOutlet UIButton *NSEBtn;
@property (strong, nonatomic) IBOutlet UIButton *MCXBtn;
@property (strong, nonatomic) IBOutlet UIButton *marketBtn;
@property (strong, nonatomic) IBOutlet UIButton *limitBtn;
@property (strong, nonatomic) IBOutlet UIButton *stopAndLossBtn;
@property (strong, nonatomic) IBOutlet UIButton *quantityBtn;
@property (strong, nonatomic) IBOutlet UIButton *valueBtn;
@property (strong, nonatomic) IBOutlet UIButton *dealerModeBtn;
@property NSString * profileImage;

- (IBAction)imagePickerBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *profileImgView;
- (IBAction)tradeSettingsUpdateBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *quantityTxt;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

- (IBAction)profileUpdateBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *userNameFld;
@property (strong, nonatomic) IBOutlet UIButton *adviceButton;
@property (strong, nonatomic) IBOutlet UIButton *marketPriceButton;
@property (strong, nonatomic) IBOutlet UIButton *triggerPriceButton;
@property (strong, nonatomic) IBOutlet UIButton *orderExecutinButton;

@property (strong, nonatomic) IBOutlet UIButton *stopLossButton;

@property (strong, nonatomic) IBOutlet UIButton *camBtnOtlt;
@property NSString * imageURLString;
@property UIAlertAction * camera;
@property UIAlertAction * gallery;
@property BOOL newMedia;
@property (weak, nonatomic) IBOutlet UIButton *profileUpdtBtn;
@property NSMutableDictionary * tradeDictionary;
@property (weak, nonatomic) IBOutlet UIView *mobileEditView;
@property (weak, nonatomic) IBOutlet UILabel *versionLbl;
@property (weak, nonatomic) IBOutlet UILabel *emailLbl;
@property (weak, nonatomic) IBOutlet UIView *nofityView;

- (IBAction)publicNotifySwitch:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *userNameTxtFld;
@property (weak, nonatomic) IBOutlet UIView *userIconview;
@property (weak, nonatomic) IBOutlet UITextField *mobileTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *emailTxtFld;
@property (weak, nonatomic) IBOutlet UIView *emailEditView;
@property (weak, nonatomic) IBOutlet UIButton *orderPrefBtn;
- (IBAction)orderPreBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UIView *view2;
@property (weak, nonatomic) IBOutlet UIButton *orderPreBtn;
@property (weak, nonatomic) IBOutlet UILabel *mobileNumberLbl;
@property (weak, nonatomic) IBOutlet UISwitch *adviceSwitch;

@property (weak, nonatomic) IBOutlet UILabel *userNameLbl;
@end
