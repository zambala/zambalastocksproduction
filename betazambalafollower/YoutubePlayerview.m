//
//  YoutubePlayerview.m
//  betazambalafollower
//
//  Created by Zenwise Technologies on 02/07/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import "YoutubePlayerview.h"
#import "YTPlayerView.h"
#import "AppDelegate.h"

@interface YoutubePlayerview ()
{
    AppDelegate * delegate;
}

@end

@implementation YoutubePlayerview

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    if(delegate.tutorialurl.length>0)
    {
    NSMutableDictionary *queryStringDictionary = [[NSMutableDictionary alloc] init];
    
    NSArray *urlComponents1 = [delegate.tutorialurl componentsSeparatedByString:@"?"];
    
    NSString * paramStr=[NSString stringWithFormat:@"%@",[urlComponents1 objectAtIndex:1]];
    
    NSArray *urlComponents = [paramStr componentsSeparatedByString:@"&"];
    
    for (NSString *keyValuePair in urlComponents)
    {
        NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
        NSString *key = [[pairComponents firstObject] stringByRemovingPercentEncoding];
        NSString *value = [[pairComponents lastObject] stringByRemovingPercentEncoding];
        
        [queryStringDictionary setObject:value forKey:key];
    }
      NSString * videoID= [queryStringDictionary objectForKey:@"v"];
   
    [self.playerView loadWithVideoId:videoID];
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
