//
//  StockView1.h
//  testing
//
//  Created by zenwise technologies on 01/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSWebSocket.h"
@import SocketIO;

#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]

@interface StockView1 : UIViewController<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource>
{
    
        NSArray *dataArray;
        UIPickerView *pickerView;
        UIView *ViewContainer;
        
        NSString *dataStr;
        
        BOOL dropFlag, chkBoxFlag;
        
        UITableView *dropDwnTableView;
        
        
    

}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIView *view1;
@property (nonatomic, strong)  SocketManager* manager;
@property (nonatomic, strong)  SocketIOClient* socketio;

@property (weak, nonatomic) IBOutlet UIView *view2;

@property (weak, nonatomic) IBOutlet UIView *view3;

- (IBAction)dropDownBtn1:(id)sender;

- (IBAction)dropDownBtn:(id)sender;
@property  NSMutableArray * titlesArray;

@property (nonatomic, strong) NSMutableDictionary *instrumentTokensDict;
@property (nonatomic) BOOL startStopButtonIsActive;
@property (nonatomic) BOOL startStopButtonIsActive1;
@property (nonatomic) BOOL startStopButtonIsActive2;
@property (nonatomic) BOOL startStopButtonIsActive3;
@property (nonatomic) BOOL startStopButtonIsActive4;
@property (nonatomic) BOOL startStopButtonIsActive5;
@property (nonatomic) BOOL startStopButtonIsActive6;
@property (nonatomic) BOOL startStopButtonIsActive7;
@property (nonatomic) BOOL startStopButtonIsActive8;
@property (nonatomic) BOOL startStopButtonIsActive9;
@property (nonatomic) BOOL startStopButtonIsActive10;
@property (nonatomic) BOOL startStopButtonIsActive11;
@property (nonatomic) BOOL startStopButtonIsActive12;
@property (nonatomic) BOOL startStopButtonIsActive13;
@property (nonatomic) BOOL startStopButtonIsActive14;
@property (nonatomic) BOOL startStopButtonIsActive15;
@property (nonatomic) BOOL startStopButtonIsActive16;
@property (nonatomic) BOOL startStopButtonIsActive17;
@property (nonatomic) BOOL startStopButtonIsActive18;
@property (nonatomic) BOOL startStopButtonIsActive19;
@property (nonatomic) BOOL startStopButtonIsActive20;
@property (nonatomic) BOOL startStopButtonIsActive21;


@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UIButton *upDownButton;



@property (weak, nonatomic) IBOutlet UITableView *tableView;


@property (strong, nonatomic) IBOutlet UIView *view4;

@property (strong, nonatomic) IBOutlet UIButton *bidbtn1;


@property (strong, nonatomic) IBOutlet UIButton *bidBtn2;



@property (strong, nonatomic) IBOutlet UIButton *bidBtn3;

@property (strong, nonatomic) IBOutlet UIButton *bidBtn4;


@property (strong, nonatomic) IBOutlet UIButton *bidBtn5;


@property (strong, nonatomic) IBOutlet UIButton *bidBtn6;


@property (strong, nonatomic) IBOutlet UIButton *bidBtn7;


@property (strong, nonatomic) IBOutlet UIButton *bidBtn8;


@property (strong, nonatomic) IBOutlet UIButton *bidBtn9;

@property (strong, nonatomic) IBOutlet UIButton *bidBtn10;

@property (strong, nonatomic) IBOutlet UIButton *aslBtn1;



@property (strong, nonatomic) IBOutlet UIButton *askBtn2;


@property (strong, nonatomic) IBOutlet UIButton *askBtn3;


@property (strong, nonatomic) IBOutlet UIButton *askBtn4;


@property (strong, nonatomic) IBOutlet UIButton *askBtn5;

@property (strong, nonatomic) IBOutlet UIButton *askBtn6;

@property (strong, nonatomic) IBOutlet UIButton *askBtn7;

@property (strong, nonatomic) IBOutlet UIButton *askBtn8;

@property (strong, nonatomic) IBOutlet UIButton *askBtn9;


@property (strong, nonatomic) IBOutlet UIButton *askBtn10;


@property (strong, nonatomic) IBOutlet UIButton *askBtn1;


@property (strong, nonatomic) IBOutlet UIButton *strikeBtn1;

@property (strong, nonatomic) IBOutlet UIButton *strike2;


@property (strong, nonatomic) IBOutlet UILabel *cashBidSize;

@property (strong, nonatomic) IBOutlet UILabel *askBidSize;



@property (strong, nonatomic) IBOutlet UILabel *cashOpen;


@property (strong, nonatomic) IBOutlet UILabel *cashHigh;

@property (strong, nonatomic) IBOutlet UILabel *cashLow;

@property (strong, nonatomic) IBOutlet UILabel *cashClose;
@property (strong, nonatomic) IBOutlet UILabel *cashVolume;


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *depthTableViewHeight;


@property NSMutableURLRequest * urlRequest2;
@property NSURLSession * session2;
@property NSURLSessionDataTask * task2;

@property (strong, nonatomic) IBOutlet UIButton *profileButton;
- (IBAction)onProfileButtonTap:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *ltpLbl;

@property (strong, nonatomic) IBOutlet UILabel *chngLbl;


@property (strong, nonatomic) IBOutlet UILabel *chngperLbl;


@property (strong, nonatomic) IBOutlet UILabel *timeLbl;


@property (strong, nonatomic) IBOutlet UILabel *bidLbl;


@property (strong, nonatomic) IBOutlet UILabel *askLbl;

@property PSWebSocket * socket;
@property NSString * message;

@property (strong, nonatomic) IBOutlet UILabel *companyTxt1;

@property (strong, nonatomic) IBOutlet UILabel *companyTxt2;
@property (strong, nonatomic) IBOutlet UIImageView *imgView1;
@property (strong, nonatomic) IBOutlet UIImageView *imgView2;

@property (strong, nonatomic) IBOutlet UILabel *FutLtpLbl;

@property (strong, nonatomic) IBOutlet UILabel *futChgLbl;

@property (strong, nonatomic) IBOutlet UILabel *futChgPerLbl;


@property (strong, nonatomic) IBOutlet UILabel *bidPriceLbl;

@property (strong, nonatomic) IBOutlet UILabel *askPriceLbl;


@property (strong, nonatomic) IBOutlet UILabel *bidQtyLbl;


@property (strong, nonatomic) IBOutlet UILabel *askQtyLbl;


@property (strong, nonatomic) IBOutlet UILabel *openLbl;


@property (strong, nonatomic) IBOutlet UILabel *highLbl;

@property (strong, nonatomic) IBOutlet UILabel *lowLbl;

@property (strong, nonatomic) IBOutlet UILabel *closeLbl;


@property (strong, nonatomic) IBOutlet UILabel *volumeLbl;
@property (strong, nonatomic) IBOutlet UIView *view5;
- (IBAction)futDate:(id)sender;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic) IBOutlet UIButton *expiryBtnOut;

@property (strong, nonatomic) IBOutlet UIButton *strike1;
@property (strong, nonatomic) IBOutlet UILabel *bidPriceCE1;


@property (strong, nonatomic) IBOutlet UILabel *askPriceCE1;
@property (strong, nonatomic) IBOutlet UILabel *bidPricePE1;

@property (strong, nonatomic) IBOutlet UILabel *askPricePE1;

@property NSString * localWisdomCheck;

@property (strong, nonatomic) IBOutlet UILabel *bidQtyCE1;

@property (strong, nonatomic) IBOutlet UILabel *askQtyCE1;

@property (strong, nonatomic) IBOutlet UILabel *bidQtyPE1;
@property (strong, nonatomic) IBOutlet UILabel *askQtyPE1;

@property (strong, nonatomic) IBOutlet UITableView *depthTableView;
- (IBAction)OPTBtnAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *OPTBtn;

@property (strong, nonatomic) IBOutlet UILabel *cashSegment;

@property (strong, nonatomic) IBOutlet UILabel *futureSegment;
@property (strong, nonatomic) IBOutlet UILabel *optionSegment;
@property (strong, nonatomic) IBOutlet UILabel *optionCompanyName;
@property (strong, nonatomic) IBOutlet UIButton *cashBuyButton;
@property (strong, nonatomic) IBOutlet UIButton *cashSellButton;

@property (strong, nonatomic) IBOutlet UIButton *futBuyButton;
@property (strong, nonatomic) IBOutlet UIButton *sellBuyButton;

- (IBAction)cashBuy:(id)sender;


- (IBAction)cashSell:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *futSell;


- (IBAction)futBuy:(id)sender;
- (IBAction)futSell:(id)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *view5Hgt;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *wisdomViewHgt;
- (IBAction)tradeBtn:(id)sender;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *view1Top;


@property (strong, nonatomic) IBOutlet UIImageView *wisdomProfileImg;

@property (strong, nonatomic) IBOutlet UILabel *wisdomProfileName;


@property (strong, nonatomic) IBOutlet UILabel *wisdomDate;

@property (strong, nonatomic) IBOutlet UILabel *wisdomMsg;
@property (strong, nonatomic) IBOutlet UILabel *wisdomLTP;
@property (strong, nonatomic) IBOutlet UILabel *wisdomCHG;

@property (strong, nonatomic) IBOutlet UILabel *wisdomActedBy;

@property (strong, nonatomic) IBOutlet UILabel *wisdomShares;
@property (strong, nonatomic) IBOutlet UIView *wisdomLTPValue;

@property (strong, nonatomic) IBOutlet UILabel *wisdomChgValue;

@property (strong, nonatomic) IBOutlet UILabel *wisdomActedValue;

@property (strong, nonatomic) IBOutlet UILabel *wisdomShareValue;

@property (strong, nonatomic) IBOutlet UILabel *wisdomOLbl;

@property (strong, nonatomic) IBOutlet UILabel *wisdomStLbl;
@property (strong, nonatomic) IBOutlet UILabel *wisdomLtpValue1;
@property (strong, nonatomic) IBOutlet UILabel *chgValue1;

@property (strong, nonatomic) IBOutlet UILabel *detailMsg;

- (IBAction)doneAction:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *doneView;

@property NSTimer * timer;



@end
