//
//  MarketTableViewCell.m
//  testing
//
//  Created by zenwise technologies on 18/11/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "MarketTableViewCell.h"

@implementation MarketTableViewCell

@synthesize timeLbl, priceLbl, percentLbl, companyNameLbl;

- (void)awakeFromNib {
    [super awakeFromNib];
    
//    self.tradeBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
//    self.tradeBtn.layer.shadowOffset = CGSizeMake(0, 2.0f);
//    self.tradeBtn.layer.shadowOpacity = 5.0f;
//    self.tradeBtn.layer.shadowRadius = 2.0f;
    self.tradeBtn.layer.cornerRadius=4.1f;
    self.tradeBtn.layer.masksToBounds = NO;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
