//
//  CancelledOrderCell.h
//  testing
//
//  Created by zenwise technologies on 29/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CancelledOrderCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *orderIDLbl;

@property (strong, nonatomic) IBOutlet UILabel *timeLbl;


@property (strong, nonatomic) IBOutlet UILabel *companyLbl;

@property (strong, nonatomic) IBOutlet UILabel *transactionLbl;

@property (strong, nonatomic) IBOutlet UILabel *priceLbl;
@property (strong, nonatomic) IBOutlet UILabel *qtyLbl;
@property (strong, nonatomic) IBOutlet UILabel *exchangeLbl;
@property (weak, nonatomic) IBOutlet UILabel *orderTypeLbl;
@property (weak, nonatomic) IBOutlet UILabel *productType;
@property (weak, nonatomic) IBOutlet UILabel *reasonLbl;
@property (weak, nonatomic) IBOutlet UIImageView *expandImgView;

@end
