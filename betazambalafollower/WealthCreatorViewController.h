//
//  WealthCreatorViewController.h
//  betazambalafollower
//
//  Created by Zenwise Technologies on 22/03/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WealthCreatorViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *wealthCreatorCollectionView;

@end
