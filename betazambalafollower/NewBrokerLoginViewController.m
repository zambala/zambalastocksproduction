//
//  NewBrokerLoginViewController.m
//  NewUI
//
//  Created by Zenwise Technologies on 23/05/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "NewBrokerLoginViewController.h"
#import "NewTwoFAViewController.h"
#import "TagEncode.h"
#import "AppDelegate.h"
#import "TabBar.h"
#import "UIImageView+WebCache.h"
@import Mixpanel;

@interface NewBrokerLoginViewController ()
{
    AppDelegate * delegate1;
    NSString * number;
    NSUserDefaults *loggedInUser;
}

@end

@implementation NewBrokerLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"client_login_page"];
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    loggedInUser = [NSUserDefaults standardUserDefaults];
    number= [loggedInUser stringForKey:@"profilemobilenumber"];
    self.clientIDTF.text = @"ZAM_CLI1-T9072";
    self.passwordTF.text = @"a.123456";
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if([delegate1.brokerNameStr isEqualToString:@"Proficient"])
        {
            self.clientIDTF.enabled = false;
            self.forgotPasswordBtn.hidden = false;
            self.resetPasswordTF.hidden = true;
            if(number)
            {
                self.clientIDTF.text=[NSString stringWithFormat:@"%@",number];
            }
        }
        else
        {
            self.clientIDTF.enabled = true;
            self.forgotPasswordBtn.hidden = true;
            self.resetPasswordTF.hidden = false;
        }
    });
    
  
    self.clientIDTF.layer.cornerRadius=5.0f;
    self.clientIDTF.layer.borderWidth = 1.0f;
    self.clientIDTF.layer.borderColor = [[UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0] CGColor];
    self.activityInd.hidden=YES;
    [self.activityInd stopAnimating];
    self.passwordTF.layer.cornerRadius=5.0f;
    self.passwordTF.layer.borderWidth = 1.0f;
    self.passwordTF.layer.borderColor = [[UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0] CGColor];
    
    self.passwordNew.layer.cornerRadius=5.0f;
    self.passwordNew.layer.borderWidth = 1.0f;
    self.passwordNew.layer.borderColor = [[UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0] CGColor];
    
    self.loginButton.layer.cornerRadius=20.0f;
    self.passwordNew.hidden=YES;
   
      self.passwordNewHgt.constant=0;
    self.brokerImageView.layer.cornerRadius=self.brokerImageView.frame.size.width / 2;
    self.brokerImageView.clipsToBounds = YES;
    NSURL *url;
    @try {
        url =  [NSURL URLWithString:[NSString stringWithFormat:@"%@",[[delegate1.brokerInfoDict objectForKey:@"about"]objectForKey:@"logourl"]]];
          [self.brokerImageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"dpDefault"]];
    } @catch (NSException *exception) {
        
        
        
    } @finally {
        
    }
    
    
    if(delegate1.brokerInfoDict)
    {
        delegate1.brokerNameStr=[NSString stringWithFormat:@"%@",[delegate1.brokerInfoDict objectForKey:@"brokername"]];
    }
    
//    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//        if (data) {
//            UIImage *image = [UIImage imageWithData:data];
//            if (image) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//
//
//                });
//            }
//        }
//    }];
//    [task resume];
      self.brokerNameLabel.text=[NSString stringWithFormat:@"%@",[delegate1.brokerInfoDict objectForKey:@"brokername"]];
   
//    [self.loginButton addTarget:self action:@selector(onLoginButtonTap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

-(void)onLoginButtonTap
{
    
}
-(void)viewWillAppear:(BOOL)animated
{
     [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)resetAction:(id)sender {
dispatch_async(dispatch_get_main_queue(), ^{
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"reset_password_page"];
    self.passwordNew.hidden=NO;
    self.passwordNewHgt.constant=36;
     self.resetPasswordTF.hidden=YES;
     });
}

- (IBAction)loginAction:(id)sender {
    @try
    {
    self.activityInd.hidden=NO;
    [self.activityInd startAnimating];
    if(self.clientIDTF.text.length>0&&self.passwordTF.text.length>0)
    {
        if(self.passwordNew.text.length>0)
        {
            
            [self bseChangePasswordRequest:delegate1.accessToken];
        }
        else
        {
            
      if([delegate1.brokerNameStr isEqualToString:@"BSE BOW"])
       {
        TagEncode * enocde=[[TagEncode alloc]init];
        delegate1.mtClientId=self.clientIDTF.text;
        delegate1.userID=self.clientIDTF.text;
        delegate1.mtPassword=self.passwordTF.text;
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        NSString *query = @"Thick%20Client";
       
       
        NSString * inputStr = [NSString stringWithFormat:@"USLOGINID=%@&USPassword=%@&VenderCode=1_ZAM&USBackOfficeId=&USTransactionPassword=&version=1.0.0&SecuritiesMaxSequenceId=1&NSEContractsMaxSequenceId=82&NcdexContractsMaxSequenceId=60&MCXContractsMaxSequenceId=139&BSEContractsMaxSequenceId=1&BSECurrencyContractsMaxSequenceId=1&NSECurrencyContractsMaxSequenceId=2125&SessionKey=&%@=Y",delegate1.mtClientId,delegate1.mtPassword,query];
//        CFStringRef newInputStr = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)inputStr, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
//        inputStr = [NSString stringWithFormat:@"%@",newInputStr];
        [inputArray addObject:[NSString stringWithFormat:@"%@/LoginServlet?%@",delegate1.mtBaseUrl,inputStr]];
        
        [enocde bseBowInputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            
            NSLog(@"%@",dict);
            int loginStatus = [[[dict objectForKey:@"Record1"] objectAtIndex:0] intValue];
            if(loginStatus == 0)
            {
                delegate1.userID = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"Record2"]objectAtIndex:0]];
                delegate1.accessToken = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"Record2"]objectAtIndex:1]];
                
                if([[dict objectForKey:@"Record6"]count]>0)
                {
                     NSString * msg = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"Record6"] objectAtIndex:1]];
                      if([msg containsString:@"Your password period has expired"])
                      {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:msg preferredStyle:UIAlertControllerStyleAlert];
                        
                        [self presentViewController:alert animated:YES completion:^{
                            
                        }];
                        
                        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            self.activityInd.hidden=YES;
                            [self.activityInd stopAnimating];
                            if([msg containsString:@"Your password period has expired"])
                            {
                                [self resetAction:self.resetPasswordTF];
                            }
                        }];
                        
                        [alert addAction:okAction];
                    });
                      }
                    else
                    {
                        [self clientCreation];
                    }
                }
                else
                {
                     [self clientCreation];
                }
                
            }
            else if(loginStatus == 1)
            {
                
                
            }
        }];
    }
    else
    {
    TagEncode * enocde=[[TagEncode alloc]init];
    enocde.inputRequestString=@"prelogin";
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    delegate1.mtClientId=self.clientIDTF.text;
    delegate1.userID=self.clientIDTF.text;
    delegate1.mtPassword=self.passwordTF.text;
    [inputArray addObject:delegate1.mtClientId];
    [inputArray addObject:delegate1.mtPassword];
//        delegate1.mtBaseUrl=@"http://192.168.15.191:8014/api/";
    [inputArray addObject:[NSString stringWithFormat:@"%@multitradeapi/prelogin",delegate1.mtBaseUrl]];
//         [inputArray addObject:[NSString stringWithFormat:@"http://192.168.15.87:8014/api/multitradeapi/prelogin"]];
       
    [inputArray addObject:self.passwordNew.text];
    if(self.passwordNew.text.length>0)
    {
    delegate1.mtPassword=self.passwordNew.text;
    }
    delegate1.questionArray=[[NSMutableArray alloc]init];
    [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        NSArray * keysArray=[dict allKeys];
        
        
        if(keysArray.count>0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.activityInd.hidden=YES;
                [self.activityInd stopAnimating];
            });
            if([delegate1.brokerNameStr isEqualToString:@"Proficient"])
            {
                NSString * statusMsg=[[dict objectForKey:@"data"] objectForKey:@"status"];
                if([statusMsg isEqualToString:@"success"])
                {
                int userStatus=[[[[dict objectForKey:@"data"] objectForKey:@"data"]  objectForKey:@"isnewuser"] intValue];
                delegate1.accessToken=[[[dict objectForKey:@"data"]  objectForKey:@"data"] objectForKey:@"token"];
                 
                if(userStatus == 0)
                {
                    [self clientCreation];
                }
                else
                {
                    delegate1.mtNewUserBool = true;
                    [self resetAction:self.resetPasswordTF];
                }
                }
                else
                {
                    NSString * msg = [NSString stringWithFormat:@"%@",[[[[dict objectForKey:@"data"] objectForKey:@"messages"]objectAtIndex:0]objectForKey:@"msg"]];
                    //NSLog(@"Error Msg:%@",msg);
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:msg preferredStyle:UIAlertControllerStyleAlert];
                        
                        [self presentViewController:alert animated:YES completion:^{
                            
                        }];
                        
                        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                         
                        }];
                        
                        [alert addAction:okAction];
                    });
                }
            }
            else
            {
            NSString * loginStatus;
            NSString * statusMsg=[[dict objectForKey:@"data"] objectForKey:@"status"];
            if([statusMsg isEqualToString:@"success"])
            {
                NSString * loginStatus;
                int factorint=[[[[dict objectForKey:@"data"] objectForKey:@"data"]  objectForKey:@"twofactor"] intValue];
                if(factorint==1)
                {
                    loginStatus=[[[dict objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"loginstatus"];
                    NSArray * array=[[[dict objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"questions"];
                    if(array.count>0)
                    {
                        for(int i=0;i<array.count;i++)
                        {
                            //                        int j=(int)[array objectAtIndex:i];
                            [delegate1.questionArray addObject:[array objectAtIndex:i]];
                        }
                    }
                }
                else  if(factorint==0)
                {
                    loginStatus=[[[dict objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"loginstatus"];
                    
                    delegate1.accessToken=[[[dict objectForKey:@"data"]  objectForKey:@"data"] objectForKey:@"token"];
                }
                
                if([loginStatus containsString:@"Login Successful"])
                {
                    if(delegate1.questionArray.count==2)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NewTwoFAViewController * twoFA = [self.storyboard instantiateViewControllerWithIdentifier:@"NewTwoFAViewController"];
                            [self.navigationController pushViewController:twoFA animated:YES];
                            
                        });
                        
                    }
                    else if(delegate1.questionArray.count==6)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NewTwoFAViewController * twoFA = [self.storyboard instantiateViewControllerWithIdentifier:@"NewTwoFAViewController"];
                            [self.navigationController pushViewController:twoFA animated:YES];
                            
                        });
                    }
                    
                    else
                        
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            delegate1.userID=delegate1.mtClientId;
                            [self clientCreation];
                        });
                    }
                    
                    
                
                }
                
            
                
            }
            
            else
                
            {
                
                NSString * msg = [NSString stringWithFormat:@"%@",[[[[dict objectForKey:@"data"] objectForKey:@"messages"]objectAtIndex:0]objectForKey:@"msg"]];
                //NSLog(@"Error Msg:%@",msg);
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:msg preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        self.activityInd.hidden=YES;
                        [self.activityInd stopAnimating];
                        if([msg containsString:@"Password expire, Please change the password"])
                        {
                            [self resetAction:self.resetPasswordTF];
                        }
                    }];
                    
                    [alert addAction:okAction];
                });
            }
            
        }
        }
        
    }];
    }
    }
    }
    
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Please fill all missing fields." preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            [self presentViewController:alert animated:YES completion:^{
                
                
                
            }];
            
            
            
            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                self.activityInd.hidden=YES;
                [self.activityInd stopAnimating];
                
            }];
            
            
            
            [alert addAction:okAction];
            
        });
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)clientCreation
{
    @try
    {
    TagEncode * enocde=[[TagEncode alloc]init];
     NSUserDefaults *loggedInUserNew = [NSUserDefaults standardUserDefaults];
    enocde.inputRequestString=@"clientcreation";
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    
        NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
        
        NSString * number= [loggedInUser stringForKey:@"profilemobilenumber"];
       
        NSString * name= [loggedInUser stringForKey:@"profilename"];
       
        NSString * email= [loggedInUser stringForKey:@"profileemail"];
      
    [inputArray addObject:delegate1.userID];
    [inputArray addObject:@""];
    [inputArray addObject:[NSString stringWithFormat:@"%@",[delegate1.brokerInfoDict objectForKey:@"brokerid"]]];
    [inputArray addObject:email];
    [inputArray addObject:name];
    [inputArray addObject:@""];
    [inputArray addObject:[NSString stringWithFormat:@"%@2factor/validateotp",delegate1.baseUrl]];
    [inputArray addObject:[loggedInUserNew objectForKey:@"userid"]];
    [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        NSArray * keysArray=[dict allKeys];
        
        
        if(keysArray.count>0)
       
        {
           
            delegate1.zenwiseToken=delegate1.accessToken;
            delegate1.zenwiseToken=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"] objectForKey:@"authtoken"]];
            NSUserDefaults *loggedInUserNew = [NSUserDefaults standardUserDefaults];
            delegate1.loginActivityStr=@"CLIENT";
            [loggedInUserNew setObject:delegate1.accessToken forKey:@"acesstoken"];
            [loggedInUserNew setObject:delegate1.userID forKey:@"mtuserid"];
            [loggedInUserNew setObject:@"client" forKey:@"userrole"];
            [loggedInUserNew setObject:delegate1.brokerNameStr forKey:@"brokername"];
            [loggedInUserNew setObject:delegate1.loginActivityStr forKey:@"loginActivityStr"];
            [loggedInUserNew synchronize];
            Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
            
            [mixpanelMini identify:delegate1.userID];
            
            [mixpanelMini.people set:@{@"name":[loggedInUserNew objectForKey:@"profilename"],@"mobileno":[loggedInUserNew objectForKey:@"profilemobilenumber"],@"email":[loggedInUserNew objectForKey:@"profileemail"],@"logintime":[NSDate date],@"sublocality":delegate1.subLocality,@"location":delegate1.locality,@"postalcode":delegate1.postalCode,@"typeofuser":delegate1.brokerNameStr,@"userid":delegate1.userID}];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
                [self presentViewController:tabPage animated:YES completion:nil];
            });
            
        }
        else
        {
            [self clientCreation];
        }
        
    }];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (IBAction)onForgotPasswordTap:(id)sender {
    self.activityInd.hidden = NO;
    [self.activityInd startAnimating];
    @try
    {
        TagEncode * enocde=[[TagEncode alloc]init];
        enocde.inputRequestString=@"forgotpassword";
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
        
        NSString * number= [loggedInUser stringForKey:@"profilemobilenumber"];
        NSString * email= [loggedInUser stringForKey:@"profileemail"];
        
        [inputArray addObject:number];
        [inputArray addObject:email];
        [inputArray addObject:[NSString stringWithFormat:@"%@multitradeapi/forgotpassword",delegate1.mtBaseUrl]];
        [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            NSArray * keysArray=[dict allKeys];
            
            NSString * status = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"status"]];
            if([status isEqualToString:@"success"])
            {
                
                NSString * message = [NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"loginstatus"]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.activityInd.hidden = YES;
                    [self.activityInd stopAnimating];
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                        self.passwordTF.text = @"";
                        self.passwordTF.placeholder = @"Temporary Password";
                        [self resetAction:self.resetPasswordTF];
                    }];
                    
                    [alert addAction:okAction];
                });
            }
           
        
            
        }];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)bseChangePasswordRequest:(NSString *)sessionKey
{
    @try {
        
        if([delegate1.brokerNameStr isEqualToString:@"BSE BOW"])
        {
            TagEncode * enocde=[[TagEncode alloc]init];
            NSMutableArray * inputArray=[[NSMutableArray alloc]init];
            NSString *query = @"Thick%20Client";
            
            
            NSString * inputStr = [NSString stringWithFormat:@"OldPassword=%@&PHPassword=%@&ConfirmPassword=%@&userflag=1&SessionKey=%@&%@=Y",delegate1.mtPassword,self.passwordNew.text,self.passwordNew.text,sessionKey,query];
            //        CFStringRef newInputStr = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)inputStr, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
            //        inputStr = [NSString stringWithFormat:@"%@",newInputStr];
            [inputArray addObject:[NSString stringWithFormat:@"%@/ChangePassword?%@",delegate1.mtBaseUrl,inputStr]];
            [inputArray addObject:self.passwordNew.text];
           
            [enocde bseBowInputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
                
                NSLog(@"%@",dict);
                int status = [[[dict objectForKey:@"Record1"] objectAtIndex:0] intValue];
                
                if(status==0)
                {
                    delegate1.accessToken = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"Record1"]objectAtIndex:3]];
                    [self clientCreation];
                }
                else  if(status==1)
                {
                    NSString * msg = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"Record1"] objectAtIndex:1]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Meesage" message:msg preferredStyle:UIAlertControllerStyleAlert];
                        
                        [self presentViewController:alert animated:YES completion:^{
                            
                        }];
                        
                        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            self.activityInd.hidden=YES;
                            [self.activityInd stopAnimating];
                           
                        }];
                        
                        [alert addAction:okAction];
                    });
                }
            }];
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
}
@end
