//
//  PortfolioOrderView.h
//  
//
//  Created by zenwise technologies on 20/03/17.
//
//

#import <UIKit/UIKit.h>

@interface PortfolioOrderView : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *orderTableView;


@end
