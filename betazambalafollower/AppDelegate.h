//
//  AppDelegate.h
//  testing
//
//  Created by zenwise mac 2 on 11/14/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property NSMutableArray * historyArray;
@property NSString * referralCodeFromServer;
@property NSString * voucherMobileNumber;
@property NSDictionary * notificationInfo;
@property NSMutableArray * urlArray;;
@property NSString * requestToken;
@property NSString * APIKey;
@property NSString * secret;
@property NSMutableDictionary * dict;
@property NSDictionary * dict1;
@property NSString * userID;
@property NSString * publicToken;
@property NSString * accessToken;
@property NSString *lastPriceStr;
@property NSString *lastTimeStr;
@property NSString *changePercentageStr;
@property NSMutableArray * companyArray;
@property NSMutableArray * companyNameArray;
@property NSMutableArray * lastPriceArray;
@property NSMutableArray * percentageChangeArray;
@property NSMutableArray * timeArray;
@property NSMutableDictionary * NIFTYdict;
@property NSMutableDictionary * dict123;
@property NSString * message;
@property UINavigationController * navigationController;
@property NSString * currentDeviceId1;
@property NSString * kiteVersion;
@property NSMutableDictionary * SENSEXdict;
@property NSString * rewardPoints;
@property NSString * NIFTYLastPrice;
@property NSString * NIFTYChangePercentage;
@property NSString * SENSEXLastPrice;
@property NSString * SENSEXChangePercentage;
@property NSString * pushNotificationSection;
@property NSString * openString;
@property NSString * highString;
@property NSString * lowString;
@property NSString * changeString;
@property NSString * volumeString;
@property NSString * instrumentDepthStr;
@property NSString * brokerRedirectUrl;
@property NSData *  helpShiftData;
@property UITabBarController * tabBarController;
@property NSDictionary * mixpanelUserRegistration;
@property NSMutableDictionary * cashDic;
@property NSString * closeStr;
@property NSString * bidSizeStr;
@property NSString * askSizeStr;
@property NSMutableArray * instrumentNameArr;
@property NSMutableArray * instrumentToken;
@property BOOL launchCheck;
@property NSMutableDictionary * brokerDetailsDict;
@property NSMutableArray * imageArray;
@property NSMutableArray * filteredCompanyName;
@property NSString * symbolDepthStr;
@property NSString * exchange;
@property BOOL editOrderBool;
@property NSString * wisdomSearchSymbol;
//clientinfo//
@property NSString * referralString;
@property NSString * numberStr;
@property NSMutableString *bankBranchString,*ifscCodeString,*bankNameString,*mcirString,*accountNumberString;
@property NSString * referralCode;
@property NSMutableDictionary * searchSymbolDict;
@property NSString * exchaneLblStr;

@property NSString * orderBuySell;
@property NSString * orderinstrument;
@property NSString * orderSegment;
@property BOOL firstTime;
@property BOOL orderBool;
@property BOOL tradeBtnBool;
@property NSString * orderID;
@property NSString * LTPStr;
@property NSString * qtyStr;
@property NSString * transaction;
@property NSString * editPrice;
@property NSMutableArray * leaderDetailArray;
@property NSMutableArray * optionOrderArray;
@property NSMutableArray * optionOrderTransactionArray;
@property NSString * lotSize;
@property NSString * depth;
@property BOOL test;
@property BOOL test1;
@property NSString * holdingsQty;
@property BOOL  portfolioBackBool;
@property NSString *buttonTitle;
@property NSString * orderStatusCheck;
@property NSMutableArray * depthLotSize;
@property NSMutableArray * sampleDepthLot;
@property NSMutableArray * optionLimitPriceArray;
@property NSMutableArray * leaderAdviceDetails;
@property NSString *phoneNumberSessionID;
@property NSString * orderStr;
@property NSMutableArray * optionSymbolArray;
@property NSString * brokerNameStr;
@property NSString * userName;
@property NSString * mainViewBrokerStr;
@property NSString * mainTickerStr;
@property NSMutableDictionary * filterResponseDictionary;
@property NSString * filterCheckString;
@property NSMutableArray * filterMonksArray;
//monks filter//
@property NSString * segment3;
@property NSString * duration3;
@property NSString * rating;
@property NSMutableArray * homeInstrumentToken;
@property NSMutableArray * homeinstrumentName;
@property NSString * homeExchange;
@property NSMutableArray * homeLotDepth;
@property NSMutableArray * homeFilterCompanyName;

@property NSInteger anIndex;
@property NSInteger anIndex1;
@property NSInteger anIndex2;
@property NSInteger anIndex3;
@property NSString * portFolioIDString;
@property BOOL wisdomCheck;
@property NSString * leaderIDWhoToFollow;
@property NSString * leaderIDFollowing;
@property NSString * tradingSecurityDes;
@property NSString * navigationCheck;
@property NSString * dismissCheck;
@property NSString * holdingCheck;
@property NSString * orderToPort;
@property NSString * logoutCheck;
@property NSString * clientPhoneNumber;
@property NSString * loginActivityStr;
@property NSString * protFolioUserID;
@property NSString * wisdomGardemTickIDString;
@property NSString * adviceSecurityId;
@property NSString * wisdomDuration;
@property NSString * wisdomBuySell;
@property BOOL wisdomTopAdvices;
@property BOOL wisdomBool;
@property NSString * modifyCheck;
@property NSString * profileImg;
@property NSString * fincode;
@property NSMutableArray * btnCountArray;
@property NSMutableArray * selectLeadersID;
@property NSMutableDictionary * detailNewsDict;
@property NSMutableArray * selectLeadersName;
@property BOOL marketmonksHint;
@property BOOL wisdomHint;
@property BOOL stockHint;
@property BOOL marketwatchHinT;
@property BOOL feedsHint;
@property BOOL stream;
@property NSString * subscriptionId;
@property NSString * leaderid;
@property NSMutableDictionary * cartDict;
@property NSMutableArray * cartArray;
@property NSString * paymentString;
@property NSString * amountForPayment;
@property NSString * userName1;
@property NSString * emailId;
@property NSString * mobileNumber;
@property NSString * amountAfterRedeem;
@property NSString * cartLeaderName;
@property NSString * UPIDetails;
@property NSString * profileTOFeeds;
@property BOOL firstTimeCheck;
@property NSString * region;
@property NSString * country;
@property NSString *locality;
@property NSString *name;
@property NSString *ocean;
@property NSString *postalCode;
@property NSString * subLocality;
@property NSString * location;
@property NSString * locatedAt;
@property NSArray * premiumLeaders;
@property NSString * token;
@property BOOL upStockCheck;
@property NSString * expirySeriesValue;
@property BOOL brokerLoginCheck;
@property NSString * equities_Derivatives;
@property NSString * upstoxLtpCheck;
@property BOOL onboardcheck;
//multi trade//
@property NSMutableDictionary * tmpValuesDict;
@property NSMutableDictionary * tmpValuesDictCustom;
@property NSMutableArray * btOutBuffer;
@property BOOL mtCheck;
@property int referanceOrderNo;
@property NSMutableArray * customPortfolioArray;
@property NSMutableArray *localExchageTokenCustom;
@property NSMutableArray *homeNewCompanyArrayCustom;
@property NSMutableArray *userMarketWatchCustom;
@property NSMutableArray *homeInstrumentTokenCustom;
@property BOOL customWatchBool;
@property NSDictionary * resourceDetails;
@property BOOL messageCheck;

@property int orderStatus;
@property NSMutableArray * allOrderHistory;
@property NSMutableArray * allTradeArray;
@property BOOL  orderCheckMt;
@property NSMutableArray * mtCancelArray;
@property NSMutableArray * mtPendingArray;
@property NSMutableArray * mtCompletedArray;
@property int modifyATINORDERNO;
@property NSMutableArray * exchangeToken;
@property NSString * mtExchangeToken;
@property NSMutableArray * homeExchangeToken;
@property NSMutableArray * mtExchange;
@property NSMutableArray * homeMtExchange;
@property NSMutableArray * marketWatch1;
@property BOOL depthMt;
@property NSMutableArray * mtpositionsArray;
@property NSMutableArray * homeNewCompanyArray;
@property NSMutableArray * expiryDateArray;
@property NSMutableArray * homeExpiryDate;
@property NSMutableArray * strikeArray;
@property NSMutableArray * homeStrkeArray;
@property NSMutableArray * optionArray;
@property NSMutableArray * homeOptionArray;
@property NSMutableArray * lotsizeArray;
@property NSMutableArray * homeLotSizeArray;
@property NSMutableArray * userMarketWatch;
@property BOOL broadcastOpen;
@property NSString * notificationToneName;
@property BOOL  searchToWatch;
@property NSMutableArray * localExchageToken;
@property NSMutableArray * tradeCompletedArray;
@property BOOL mtOrderCheck;
@property  NSData * mixpanelDeviceToken;
@property BOOL mainorderCheck;
@property NSString * mtClientId;
@property NSString * mtPassword;
@property NSMutableArray * questionArray;
@property NSMutableDictionary * brokerInfoDict;
@property NSArray * scripsMt;
@property NSString * POSITIONtransctionType;
@property NSString * currentDeviceId;
@property NSMutableArray * stdealerId;
@property NSMutableArray * mtHoldingArray;
@property NSString * baseUrl;
@property NSMutableArray * allTradeOrderArray;
@property BOOL marketWatchBool;
@property NSMutableArray *mtIndexWatchArray;
@property BOOL  tickerCheck;
@property NSString * upstoxUserBankAccount;
@property NSString * upstoxUserBankName;
@property NSMutableArray * mtFundsArray;
@property BOOL homeMt;
@property BOOL orderMt;
@property BOOL holdingsMt;
@property BOOL holdingRequestCheck;
@property BOOL expiryBool;
@property NSString * profileDuration;
@property BOOL durationUpdateBool;
@property NSString * wisdomFromStr;
@property NSString * wisdomaToStr;
@property NSString * mtBaseUrl;
@property NSString * zenwiseToken;
@property NSString * ltpServerURL;
@property NSString * adviceType;
@property NSString * tutorialurl;
@property NSDictionary * knowledgeDetailsDict;
@property NSMutableArray * topPerfomersDetailArray;
@property NSString * pointsNavigation;


////////// US Markets //////////
@property NSMutableArray *expertIDArray;
@property NSString * sessionID;
@property NSString * stockTickerString;
@property NSString * orderRecomdationType;
@property NSMutableArray * mainOrdersArray;
@property NSString * USAuserID;
@property NSString * accountNumber;
@property NSString * dummyAccountURL;
@property NSString * realAccountURL;
@property NSString * accountCheck;
@property NSMutableArray * equityTickerNameArray;
@property NSMutableArray * equityCompanyNameArray;
@property NSString *etfCheck;
@property NSMutableDictionary * marketWatchLocalStoreDict;
@property NSString * addCheck;
@property NSString * usCheck;
@property NSString * usaId;
@property NSString * usNavigationCheck;
@property NSString * etfOrderCheck;
@property BOOL brandsNavigationCheck;
@property NSString * companyName;
@property NSString * tickerName;
@property NSString * usLTPURL;
@property BOOL mtNewUserBool;

//// US Open Account ////


@property NSMutableDictionary * addressDict;
@property NSString * tickCheck;
@property NSMutableDictionary * finalChoiceTradeDict;
@property NSString * appIDString;
@property NSString * producationURL;
@property NSString * apiKey;
@property NSString * openAccountStepCheck;






@end



