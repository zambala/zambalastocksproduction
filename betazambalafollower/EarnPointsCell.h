//
//  EarnPointsCell.h
//  betazambalafollower
//
//  Created by Zenwise Technologies on 18/07/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EarnPointsCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *descriptionLbl;
@property (weak, nonatomic) IBOutlet UILabel *pointsLbl;

@property (weak, nonatomic) IBOutlet UIView *bgView;
@end
