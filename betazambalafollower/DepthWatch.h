//
//  DepthWatch.h
//  betazambalafollower
//
//  Created by zenwise mac 2 on 8/29/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DepthWatch : NSObject

@property short usAT;
@property int btDataType;   //Byte
@property short usSize;
@property short usMsgCode;
@property int inSeqID;
@property NSString* stExchange;
@property int btNull1;   //Byte
@property NSString* stSecurityID;
@property int btNull2;
@property int inBroadCastTime;
@property int btSessionIndex; //int


//Buy Depth

@property double dbBuy_Price_0;
@property int inBuy_Qty_0;




//Buy Depth

@property int inBuy_Order_0;
@property double dbBuy_Price_1;
@property int inBuy_Qty_1;
@property int inBuy_Order_1;
@property double dbBuy_Price_2;
@property int inBuy_Qty_2;
@property int inBuy_Order_2;
@property double dbBuy_Price_3;
@property int inBuy_Qty_3;
@property int inBuy_Order_3;

@property double      dbBuy_Price_4;
@property int         inBuy_Qty_4;
@property int         inBuy_Order_4;

//Sell Depth

@property double      dbSell_Price_0;
@property int         inSell_Qty_0;
@property int         inSell_Order_0;

@property double      dbSell_Price_1;
@property int         inSell_Qty_1;
@property int         inSell_Order_1;

@property double      dbSell_Price_2;
@property int         inSell_Qty_2;
@property int         inSell_Order_2;

@property double      dbSell_Price_3;
@property int         inSell_Qty_3;
@property int         inSell_Order_3;

@property double      dbSell_Price_4;
@property int         inSell_Qty_4;
@property int         inSell_Order_4;


@property int         inTotalQtyTraded; //Total Volume
@property double      dbLTP;
@property int         inLTQ;
@property int         inTradeTime;
@property double      dbVWap; // Volume Wighted Average Price
@property int         inTotalBuyQty;
@property int         inTotalSellQty;
@property double      dbOpen;
@property double      dbHigh;
@property double      dbLow;
@property double      dbClose;
@property int         inOpenInterest;
@property double      dbLifeLow; // Life Time Low
@property double      dbLifeHigh; // Life Time High
@property double      dbTotalValue;

+ (id)Mt4;


@end
