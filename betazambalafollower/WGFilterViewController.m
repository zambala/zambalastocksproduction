//
//  WGFilterViewController.m
//  betazambalafollower
//
//  Created by guna on 08/05/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "WGFilterViewController.h"
#import "AppDelegate.h"
#import <UITextField_AutoSuggestion/UITextField+AutoSuggestion.h>

@interface WGFilterViewController ()<UITextFieldAutoSuggestionDataSource,UITextFieldDelegate>
{
    UIDatePicker * fromDatePickerView;
    UIDatePicker * toDatePickerView;
    UIDatePicker * fromTimePickerView;
    UIDatePicker * toTimePickerView;
    UIToolbar * toolbar;
    UIBarButtonItem * rightBtn;
    NSString * datePickerViewString;
    BOOL check,check1,check2,check3,check4;
    NSDictionary *headers ;
    
    NSMutableURLRequest *request;
    
    NSDictionary *parameters ;
    
    NSData *postData ;
    AppDelegate * delegate;
    NSUserDefaults *prefs;
    NSMutableArray * companyArray;
    NSDictionary * detailsDict;
    NSMutableArray * symbolArray;
    NSMutableArray * companyName;
    NSDate * checkFromDate;
    NSDate * checkToDate;
    

}

@property (strong, nonatomic) NSTimer * searchTimer;



@end
#define Symbol_ID @"symbol_id"
#define WEEKS @[@"Monday", @"Tuesday", @"Wednesday", @"Thirsday", @"Friday", @"Saturday", @"Sunday"]

@implementation WGFilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    check=true;
    check1=true;
    check2=true;
    check3=true;
    check4=true;
   prefs = [NSUserDefaults standardUserDefaults];
    self.symbolSearchFld.delegate = self;
    self.symbolSearchFld.autoSuggestionDataSource = self;
    self.symbolSearchFld.fieldIdentifier = Symbol_ID;
    [self.symbolSearchFld observeTextFieldChanges];
    
    [self.symbolSearchFld resignFirstResponder];

    self.buySellDict = [[NSMutableDictionary alloc]init];
    self.tradeDict = [[NSMutableDictionary alloc]init];
    
//    self.navigationItem.title = @"Filter";
    
    self.navigationController.navigationBarHidden=NO;
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *backBtnImage = [UIImage imageNamed:@"screenClose.png"]  ;
    [backBtn setBackgroundImage:backBtnImage forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(goback) forControlEvents:UIControlEventTouchUpInside];
    backBtn.frame = CGRectMake(3,12,36, 36);
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn] ;
    self.navigationItem.leftBarButtonItem = backButton;
    
    //    UIImage *NavigationPortraitBackground= [UIImage imageNamed:@"screenClose.png"];
    //    [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
    
    
    
    
    self.leaderView.layer.shadowOffset = CGSizeMake(0, 2);
    self.leaderView.layer.shadowOpacity = 0.1;
    self.leaderView.layer.shadowRadius = 3.1;
    self.leaderView.clipsToBounds = NO;
    
    self.viewTopAdvicesButton.layer.shadowOffset = CGSizeMake(0, 2);
    self.viewTopAdvicesButton.layer.shadowOpacity = 0.1;
    self.viewTopAdvicesButton.layer.shadowRadius = 3.1;
    self.viewTopAdvicesButton.clipsToBounds = NO;
    
    self.viewPremiumAdvicesButton.layer.shadowOffset = CGSizeMake(0, 2);
    self.viewPremiumAdvicesButton.layer.shadowOpacity = 0.1;
    self.viewPremiumAdvicesButton.layer.shadowRadius = 3.1;
    self.viewPremiumAdvicesButton.clipsToBounds = NO;
    [self.viewPremiumAdvicesButton addTarget:self action:@selector(onPremiumTap) forControlEvents:UIControlEventTouchUpInside];
    
    [self.buyButton setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    [self.buyButton setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    [self.buyButton addTarget:self action:@selector(onBuyButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.sellButton setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    [self.sellButton setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    [self.sellButton addTarget:self action:@selector(onSellButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    [self.dayTradeButton setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    [self.dayTradeButton setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    [self.dayTradeButton addTarget:self action:@selector(onDayTradeButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [self.longTermButton setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    [self.longTermButton setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    [self.longTermButton addTarget:self action:@selector(onLongTermButtonTap) forControlEvents:UIControlEventTouchUpInside];
    

    [self.shortTermButton setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    [self.shortTermButton setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    [self.shortTermButton addTarget:self action:@selector(onShortTermButtonTap) forControlEvents:UIControlEventTouchUpInside];
    


    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated
{
     self.navigationItem.title = self.title;
    if(delegate.selectLeadersName.count>0)
    {
        NSMutableString * string = [[delegate.selectLeadersName componentsJoinedByString:@","] mutableCopy];
        
//        for(int i=0;i<delegate.selectLeadersName.count;i++)
//        {
//            
//           // [string stringByAppendingString:[[delegate.selectLeadersName objectAtIndex:i] stringValue]];
//            [string appendString:[NSString stringWithFormat:@"%@",[[delegate.selectLeadersName objectAtIndex:i] stringValue]]];
//        }
        
        //NSLog(@"String:%@",string);
        self.selectLeadersLabel.text = [string mutableCopy];
        
    }
}

-(void)onBuyButtonTap
{
    if(check==true)
    {
        self.buyButton.selected=YES;
        check=false;
        [self.buySellDict setObject:[NSNumber numberWithInt:1] forKey:@"buy"];
        [prefs setObject:@"buySelected" forKey:@"1"];
        [prefs synchronize];
    }else
    {
        self.buyButton.selected=NO;
        check=true;
        [self.buySellDict removeObjectForKey:@"buy"];
        [prefs setObject:@"buyUnselected" forKey:@"1"];
        [prefs synchronize];
    }
}
-(void)onSellButtonTap
{
    if(check1==true)
    {
        self.sellButton.selected=YES;
        check1=false;
        [self.buySellDict setObject:[NSNumber numberWithInt:2] forKey:@"sell"];
        [prefs setObject:@"sellSelected" forKey:@"2"];
        [prefs synchronize];

        
    }else
    {
        self.sellButton.selected=NO;
        check1=true;
        [self.buySellDict removeObjectForKey:@"sell"];
        [prefs setObject:@"sellUnSelected" forKey:@"2"];
        [prefs synchronize];

    }
}
-(void)onDayTradeButtonTap
{
    if(check2==true)
    {
        self.dayTradeButton.selected=YES;
        check2=false;
        [self.tradeDict setObject:[NSNumber numberWithInt:1] forKey:@"day"];
        [prefs setObject:@"daySelected" forKey:@"3"];
        [prefs synchronize];

    }else
    {
        self.dayTradeButton.selected=NO;
        check2=true;
        [self.tradeDict removeObjectForKey:@"day"];
        [prefs setObject:@"dayUnSelected" forKey:@"3"];
        [prefs synchronize];

    }
}
-(void)onLongTermButtonTap
{
    if(check3==true)
    {
        self.longTermButton.selected=YES;
        check3=false;
        [self.tradeDict setObject:[NSNumber numberWithInt:3] forKey:@"long"];
        [prefs setObject:@"longSelected" forKey:@"4"];
        [prefs synchronize];

    }else
    {
        self.longTermButton.selected=NO;
        check3=true;
        [self.tradeDict removeObjectForKey:@"long"];
        [prefs setObject:@"longUnSelected" forKey:@"4"];
        [prefs synchronize];

    }
}
-(void)onShortTermButtonTap
{
    if(check4==true)
    {
        self.shortTermButton.selected=YES;
        check4=false;
        [self.tradeDict setObject:[NSNumber numberWithInt:2] forKey:@"short"];
        [prefs setObject:@"shortSelected" forKey:@"5"];
        [prefs synchronize];

    }else
    {
        self.shortTermButton.selected=NO;
        check4=true;
        [self.tradeDict removeObjectForKey:@"short"];
        [prefs setObject:@"shortUnSelected" forKey:@"5"];
        [prefs synchronize];

    }
}
- (void)goback
{
    [self.navigationController popViewControllerAnimated:YES];
}

//-(void)filterServer
//{
//    headers = @{ @"cache-control": @"no-cache",
//                 @"content-type": @"application/json"
//                 };
//    
//    request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://zambaladev.ap-southeast-1.elasticbeanstalk.com/api/follower/advices"]
//                                      cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                  timeoutInterval:10.0];
//    
//    parameters = @{
//                   @"clientid":delegate1.userID,
//                   @"segment":@1,
//                   @"active":@(true),
//                   @"issubscribed":@(true),
//                   @"durationtype":delegate1.wisdomDuration,
//                   @"limit":@3000
//                   
//                   };
//    
//    postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
//    [request setHTTPMethod:@"POST"];
//    [request setAllHTTPHeaderFields:headers];
//    [request setHTTPBody:postData];
//    
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
//                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                                                    if (error) {
//                                                        //NSLog(@"%@", error);
//                                                    } else {
//                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
//                                                        //NSLog(@"%@", httpResponse);
//                                                        
//                                                
//                                                        
//                                                    }
//                                                    dispatch_async(dispatch_get_main_queue(), ^{
//                                                        
//                                                       
//                                                        
//                                                        
//                                                    });
//                                                    
//                                                }];
//    [dataTask resume];
//    
//}
//
//
//
//}
//


//-(void)buyButtonMethod
//{
//    [self.buyButton setImage:[UIImage imageNamed:@"checkSel-box.png"] forState:UIControlStateNormal];
//    [self.sellButton setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
//}
//
//-(void)sellButtonMethod
//{
//    [self.buyButton setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
//    [self.sellButton setImage:[UIImage imageNamed:@"checkSel-box.png"] forState:UIControlStateNormal];
// 
//}
//-(void)longTermButtonMethod
//{
//    [self.dayTradeButton setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
//    [self.longTermButton setImage:[UIImage imageNamed:@"checkSel-box.png"] forState:UIControlStateNormal];
//    [self.shortTermButton setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
//
// 
//}
//-(void)shortTermButtonMethod
//{
//    [self.dayTradeButton setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
//    [self.longTermButton setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
//    [self.shortTermButton setImage:[UIImage imageNamed:@"checkSel-box.png"] forState:UIControlStateNormal];
//    
//    
//}
//-(void)dayTradeButtonMethod
//{
//    [self.dayTradeButton setImage:[UIImage imageNamed:@"checkSel-box.png"] forState:UIControlStateNormal];
//    [self.longTermButton setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
//    [self.shortTermButton setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
//    
//    
//}

-(void)LabelTitle:(id)sender
{
    @try
    {
    if ([datePickerViewString isEqualToString:@"fromDate"]) {
        NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
        dateFormat.dateStyle=NSDateFormatterMediumStyle;
        [dateFormat setDateFormat:@"EE,MM dd yyyy"];
         NSString *str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:fromDatePickerView.date]];
        
//        [dateFormat setDateFormat:@"yyyydd"];
       
        NSString * serverDate = [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:fromDatePickerView.date]];
        //NSLog(@"Server date:%@",serverDate);
        //assign text to label
        [self.fromDateButtonOutlet setTitle:str forState:UIControlStateNormal];
        
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
       delegate.wisdomFromStr=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:fromDatePickerView.date]];
        
        checkFromDate=fromDatePickerView.date;

    }
    if ([datePickerViewString isEqualToString:@"toDate"]) {
        NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
        dateFormat.dateStyle=NSDateFormatterMediumStyle;
        [dateFormat setDateFormat:@"EE,MM dd yyyy"];
        NSString *str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:toDatePickerView.date]];
        //assign text to label
        [self.toDateButtonOutlet setTitle:str forState:UIControlStateNormal];
        
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        delegate.wisdomaToStr=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:toDatePickerView.date]];
        
        checkToDate=toDatePickerView.date;
        
    }
    if ([datePickerViewString isEqualToString:@"fromTime"]) {
        NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
        dateFormat.dateStyle=NSDateFormatterFullStyle;
        [dateFormat setDateFormat:@"hh:mm"];
        NSString *str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:fromTimePickerView.date]];
        //assign text to label
        [self.fromTimeButtonOutlet setTitle:str forState:UIControlStateNormal];
        
    }
    if ([datePickerViewString isEqualToString:@"toTime"]) {
        NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
        dateFormat.dateStyle=NSDateFormatterFullStyle;
        [dateFormat setDateFormat:@"hh:mm"];
        NSString *str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:toTimePickerView.date]];
        //assign text to label
        [self.toTimeButtonOutlet setTitle:str forState:UIControlStateNormal];
        
    }

    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
 
   

   }

-(void)save:(id)sender
{
  //  self.navigationItem.rightBarButtonItem=nil;
    @try
    {
        if ([datePickerViewString isEqualToString:@"fromDate"]) {
            NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
            dateFormat.dateStyle=NSDateFormatterMediumStyle;
            [dateFormat setDateFormat:@"EE,MM dd yyyy"];
            NSString *str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:fromDatePickerView.date]];
            
            //        [dateFormat setDateFormat:@"yyyydd"];
            
            NSString * serverDate = [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:fromDatePickerView.date]];
            //NSLog(@"Server date:%@",serverDate);
            //assign text to label
            [self.fromDateButtonOutlet setTitle:str forState:UIControlStateNormal];
            
            [dateFormat setDateFormat:@"yyyy-MM-dd"];
            delegate.wisdomFromStr=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:fromDatePickerView.date]];
             checkFromDate=fromDatePickerView.date;
            
            
        }
        if ([datePickerViewString isEqualToString:@"toDate"]) {
            NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
            dateFormat.dateStyle=NSDateFormatterMediumStyle;
            [dateFormat setDateFormat:@"EE,MM dd yyyy"];
            NSString *str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:toDatePickerView.date]];
            //assign text to label
            [self.toDateButtonOutlet setTitle:str forState:UIControlStateNormal];
            
            [dateFormat setDateFormat:@"yyyy-MM-dd"];
            delegate.wisdomaToStr=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:toDatePickerView.date]];
            checkToDate=toDatePickerView.date;
        }
        if ([datePickerViewString isEqualToString:@"fromTime"]) {
            NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
            dateFormat.dateStyle=NSDateFormatterFullStyle;
            [dateFormat setDateFormat:@"hh:mm"];
            NSString *str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:fromTimePickerView.date]];
            //assign text to label
            [self.fromTimeButtonOutlet setTitle:str forState:UIControlStateNormal];
            
        }
        if ([datePickerViewString isEqualToString:@"toTime"]) {
            NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
            dateFormat.dateStyle=NSDateFormatterFullStyle;
            [dateFormat setDateFormat:@"hh:mm"];
            NSString *str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:toTimePickerView.date]];
            //assign text to label
            [self.toTimeButtonOutlet setTitle:str forState:UIControlStateNormal];
            
        }
        
        
        
    
    [fromDatePickerView removeFromSuperview];
    [fromTimePickerView removeFromSuperview];
    [toTimePickerView removeFromSuperview];
    [toDatePickerView removeFromSuperview];
    [toolbar removeFromSuperview];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)fromDateButtonAction:(id)sender {
    [fromDatePickerView removeFromSuperview];
    [toolbar removeFromSuperview];
    [toDatePickerView removeFromSuperview];
    [toolbar removeFromSuperview];
    fromDatePickerView =[[UIDatePicker alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-125,self.view.frame.size.width,125)];
    fromDatePickerView.datePickerMode=UIDatePickerModeDate;
    fromDatePickerView.hidden=NO;
    fromDatePickerView.date=[NSDate date];
    fromDatePickerView.maximumDate = [NSDate date];
    [fromDatePickerView addTarget:self action:@selector(LabelTitle:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:fromDatePickerView];
    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(save:)];
    
    self->toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-175,self.view.frame.size.width,50)];
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton,nil];
    [self->toolbar setItems:toolbarItems];
    [self.view addSubview:toolbar];
    
    fromDatePickerView.backgroundColor=[UIColor lightGrayColor];
    
    datePickerViewString=@"fromDate";
}
- (IBAction)toDateButtonAction:(id)sender {
    [fromDatePickerView removeFromSuperview];
    [toolbar removeFromSuperview];
    [toDatePickerView removeFromSuperview];
    [toolbar removeFromSuperview];
    toDatePickerView =[[UIDatePicker alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-125,self.view.frame.size.width,125)];
    toDatePickerView.datePickerMode=UIDatePickerModeDate;
    toDatePickerView.hidden=NO;
    toDatePickerView.date=[NSDate date];
    toDatePickerView.maximumDate = [NSDate date];
    [toDatePickerView addTarget:self action:@selector(LabelTitle:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:toDatePickerView];
    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(save:)];
    
    self->toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-175,self.view.frame.size.width,50)];
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton,nil];
    [self->toolbar setItems:toolbarItems];
    [self.view addSubview:toolbar];
    
    toDatePickerView.backgroundColor=[UIColor lightGrayColor];
    
    datePickerViewString=@"toDate";

}

- (IBAction)fromTimeButtonAction:(id)sender {
    [toTimePickerView removeFromSuperview];
    [toolbar removeFromSuperview];
    [fromTimePickerView removeFromSuperview];
    [toolbar removeFromSuperview];

    
    fromTimePickerView =[[UIDatePicker alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-125,self.view.frame.size.width,125)];
    fromTimePickerView.datePickerMode=UIDatePickerModeTime;
    fromTimePickerView.hidden=NO;
    fromTimePickerView.date=[NSDate date];
    fromTimePickerView.maximumDate = [NSDate date];
    [fromTimePickerView addTarget:self action:@selector(LabelTitle:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:fromTimePickerView];
    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(save:)];
    
    self->toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-175,self.view.frame.size.width,50)];
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton,nil];
    [self->toolbar setItems:toolbarItems];
    [self.view addSubview:toolbar];
    fromTimePickerView.backgroundColor=[UIColor lightGrayColor];
    
    datePickerViewString=@"fromTime";
}

- (IBAction)toTimeButtonAction:(id)sender {
    [fromTimePickerView removeFromSuperview];
    [toolbar removeFromSuperview];
    [toDatePickerView removeFromSuperview];
    [toolbar removeFromSuperview];
    toTimePickerView =[[UIDatePicker alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-125,self.view.frame.size.width,125)];
    toTimePickerView.datePickerMode=UIDatePickerModeTime;
    toTimePickerView.hidden=NO;
    toTimePickerView.date=[NSDate date];
    toTimePickerView.maximumDate = [NSDate date];
    [toTimePickerView addTarget:self action:@selector(LabelTitle:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:toTimePickerView];
    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(save:)];
    
    self->toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-175,self.view.frame.size.width,50)];
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton,nil];
    [self->toolbar setItems:toolbarItems];
    [self.view addSubview:toolbar];
    toTimePickerView.backgroundColor=[UIColor lightGrayColor];
    
    datePickerViewString=@"toTime";

}
- (IBAction)onViewTopAdvicesTap:(id)sender {
    delegate.wisdomTopAdvices=true;
    delegate.adviceType=@"Public";
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)onPremiumTap
{
    delegate.wisdomTopAdvices=true;
    delegate.adviceType=@"Premium";
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onApplyButtonTap:(id)sender {
    
   
    if(!(checkToDate==nil)&&!(checkFromDate==nil))
    {
    NSTimeInterval distanceBetweenDates = [checkToDate timeIntervalSinceDate:checkFromDate];
    double secondsInMinute = 60;
    NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;
    
    if (secondsBetweenDates > 0)
    {
        @try
        {
            delegate.wisdomDuration = [NSString stringWithFormat:@"%@",[self.tradeDict allValues]];
            //NSLog(@"WisdomDuration:%@",delegate.wisdomDuration);
            delegate.wisdomBuySell = [NSString stringWithFormat:@"%@",[self.buySellDict allValues]];
            //NSLog(@"BuySell:%@",delegate.wisdomBuySell);
            
            //NSLog(@"Delegate leader ID:%@",delegate.selectLeadersID);
            delegate.wisdomBool=true;
            
            checkToDate=nil;
            checkFromDate=nil;
            [self.navigationController popViewControllerAnimated:YES];
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
        

    }
    else if (secondsBetweenDates < 0)
   {
        dispatch_async(dispatch_get_main_queue(), ^{
       UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"To date should be greater than from date" preferredStyle:UIAlertControllerStyleAlert];
       
       [self presentViewController:alert animated:YES completion:^{
           
       }];
       
       UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
           
       }];
       
       [alert addAction:okAction];
        });
   }
    }
    else
    
    {
        @try
        {
            delegate.wisdomDuration = [NSString stringWithFormat:@"%@",[self.tradeDict allValues]];
            //NSLog(@"WisdomDuration:%@",delegate.wisdomDuration);
            delegate.wisdomBuySell = [NSString stringWithFormat:@"%@",[self.buySellDict allValues]];
            //NSLog(@"BuySell:%@",delegate.wisdomBuySell);
            
            //NSLog(@"Delegate leader ID:%@",delegate.selectLeadersID);
            delegate.wisdomBool=true;
            
            
            [self.navigationController popViewControllerAnimated:YES];
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
        

    }
    

}
- (IBAction)selectLeaderButton:(id)sender {
    
    [delegate.selectLeadersName removeAllObjects];
    [delegate.selectLeadersID removeAllObjects];
}
//auto suggestion

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.symbolSearchFld resignFirstResponder];
        return true;
}
- (void) searchForKeyword:(NSTimer *)timer
{
    // retrieve the keyword from user info
    NSString *keyword = (NSString*)timer.userInfo;
    self.searchString=keyword;
    [self searchServer:self.searchString];
    [self loadWeekDays];
    // [self segmentedControlChangedValue];
    // perform your search (stubbed here using //NSLog)
    //NSLog(@"Searching for keyword %@", keyword);
}

-(void)searchServer:(NSString *)string
{
    @try
    {
      //  NSString * searchStr = [self.symbolSearchFld.text stringByReplacingCharactersInRange:range withString:string];
        
       // searchStr;
       NSString *  searchStr = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                          NULL,
                                                                                          (CFStringRef)string,
                                                                                          NULL,
                                                                                          (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                          kCFStringEncodingUTF8 ));
        
        
        if([searchStr containsString:@"&"])
        {
//            searchStr=[searchStr stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
            
            CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)searchStr, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
            
            searchStr=[NSString stringWithFormat:@"%@",newString];
            
        }
        
        if(searchStr.length>=1)
        {
            
            
            
            if([self.exchangeType isEqualToString:@"EQ"])
            {
                @try {
                    NSDictionary *headers = @{
                                              @"cache-control": @"no-cache",
                                              @"authtoken":delegate.zenwiseToken,
                                              @"deviceid":delegate.currentDeviceId
                                              };
                    
                    NSString * urlStr=[NSString stringWithFormat:@"%@stock/searchsymbol/?searchsymbol=%@&exchange=%@&limit=20000",delegate.baseUrl,searchStr,@"NSE"];
                    
                    
                    
                    //NSLog(@"url %@",urlStr);
                    
                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                       timeoutInterval:10.0];
                    [request setHTTPMethod:@"GET"];
                    [request setAllHTTPHeaderFields:headers];
                    
                    NSURLSession *session = [NSURLSession sharedSession];
                    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                    if (error) {
                                                                        //NSLog(@"%@", error);
                                                                    } else {
                                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                        //NSLog(@"%@", httpResponse);
                                                                        if([httpResponse statusCode]==403)
                                                                        {
                                                                            
                                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                            
                                                                        }
                                                                        else if([httpResponse statusCode]==401)
                                                                        {
                                                                            
                                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                            
                                                                        }
                                                                        else
                                                                        {
                                                                            
                                                                            companyArray=[[NSMutableArray alloc]init];
                                                                            
                                                                            
                                                                            companyName=[[NSMutableArray alloc]init];
                                                                            
                                                                            NSMutableArray * responseArray=[[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] mutableCopy];
                                                                            
                                                                            //                                                                    [delegate1.searchSymbolDict setObject:responseArray forKey:@"details"];
                                                                            
                                                                            
                                                                            
                                                                            //                                                            NSString * string=[NSString stringWithFormat:@"%@",self.responseArray];
                                                                            
                                                                            detailsDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSArray arrayWithArray:responseArray],@"details", nil];
                                                                            
                                                                            if(responseArray.count>0)
                                                                            {
                                                                                
                                                                                for(int i=0;i<responseArray.count;i++)
                                                                                {
                                                                                    
                                                                                    [companyArray addObject:[[responseArray objectAtIndex:i]objectForKey:@"symbol"]];
                                                                                    
                                                                                     [companyName addObject:[[responseArray objectAtIndex:i]objectForKey:@"name"]];
                                                                                    
                                                                                }
                                                                                
                                                                                [responseArray removeAllObjects];
                                                                            }
                                                                        }
                                                                    }
                                                                    
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        
                                                                        //                                                          HomePage * home1=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
                                                                        //
                                                                        //                                                          [self.navigationController pushViewController:home1 animated:YES];
                                                                        
                                                                        
                                                                        
                                                                    });
                                                                }];
                    [dataTask resume];
                    
                    
                    
                }
                @catch (NSException * e) {
                    //NSLog(@"Exception: %@", e);
                }
                @finally {
                    //NSLog(@"finally");
                }
            }
            
            
            
            else if([self.exchangeType isEqualToString:@"CR"])
                
            {
                @try {
                    NSDictionary *headers = @{
                                              @"cache-control": @"no-cache",
                                              @"authtoken":delegate.zenwiseToken,
                                              @"deviceid":delegate.currentDeviceId
                                              };
                    
                    NSString * urlStr=[NSString stringWithFormat:@"%@stock/fao?exchange=CDS&symbol=%@&limit=20000",delegate.baseUrl,searchStr];
                    
                    //NSLog(@"url %@",urlStr);
                    
                    
                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                       timeoutInterval:10.0];
                    [request setHTTPMethod:@"GET"];
                    [request setAllHTTPHeaderFields:headers];
                    
                    NSURLSession *session = [NSURLSession sharedSession];
                    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                    if (error) {
                                                                        //NSLog(@"%@", error);
                                                                    } else {
                                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                        //NSLog(@"%@", httpResponse);
                                                                        if([httpResponse statusCode]==403)
                                                                        {
                                                                            
                                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                            
                                                                        }
                                                                        else if([httpResponse statusCode]==401)
                                                                        {
                                                                            
                                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                            
                                                                        }
                                                                        else
                                                                        {
                                                                            
                                                                            
                                                                            
                                                                            companyArray=[[NSMutableArray alloc]init];
                                                                            
                                                                            NSMutableArray * responseArray=[[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] mutableCopy];
                                                                            
                                                                            
                                                                            
                                                                            //                                                            NSString * string=[NSString stringWithFormat:@"%@",self.responseArray];
                                                                            
                                                                            detailsDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSArray arrayWithArray:responseArray],@"details", nil];
                                                                            
                                                                            
                                                                            if(responseArray.count>0)
                                                                            {
                                                                                
                                                                                for(int i=0;i<responseArray.count;i++)
                                                                                {
                                                                                    
                                                                                    [companyArray addObject:[[responseArray objectAtIndex:i]objectForKey:@"symbol"]];
                                                                                    
                                                                                    
                                                                                    
                                                                                }
                                                                                
                                                                                [responseArray removeAllObjects];
                                                                            }
                                                                        }
                                                                    }
                                                                    
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        
                                                                        //                                                          HomePage * home1=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
                                                                        //
                                                                        //                                                          [self.navigationController pushViewController:home1 animated:YES];
                                                                        
                                                                        
                                                                        
                                                                    });
                                                                }];
                    [dataTask resume];
                }
                @catch (NSException * e) {
                    //NSLog(@"Exception: %@", e);
                }
                @finally {
                    //NSLog(@"finally");
                }
                
                
                
                
                
                
            }
            
            
            
            
            
            
            
            
            
            else if ([self.exchangeType isEqualToString:@"DR"])
            {
                @try {
                    NSDictionary *headers = @{
                                              @"cache-control": @"no-cache",
                                              @"authtoken":delegate.zenwiseToken,
                                              @"deviceid":delegate.currentDeviceId
                                              };
                    
                    NSString * urlStr=[NSString stringWithFormat:@"%@stock/searchsymbolfao/?exchange=%@&searchsymbol=%@&segment=%@&limit=20000",delegate.baseUrl,@"NFO",searchStr,@"NFO-FUT"];
                    
                    
                    
                    //NSLog(@"url %@",urlStr);
                    
                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                       timeoutInterval:10.0];
                    [request setHTTPMethod:@"GET"];
                    [request setAllHTTPHeaderFields:headers];
                    
                    NSURLSession *session = [NSURLSession sharedSession];
                    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                    if (error) {
                                                                        //NSLog(@"%@", error);
                                                                    } else {
                                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                        //NSLog(@"%@", httpResponse);
                                                                        if([httpResponse statusCode]==403)
                                                                        {
                                                                            
                                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                            
                                                                        }
                                                                        else if([httpResponse statusCode]==401)
                                                                        {
                                                                            
                                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                            
                                                                        }
                                                                        else
                                                                        {
                                                                            
                                                                            companyArray=[[NSMutableArray alloc]init];
                                                                            
                                                                            
                                                                            companyName=[[NSMutableArray alloc]init];
                                                                            
                                                                            NSMutableArray * responseArray=[[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] mutableCopy];
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            detailsDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSArray arrayWithArray:responseArray],@"details", nil];
                                                                            
                                                                            for(int i=0;i<responseArray.count;i++)
                                                                            {
                                                                                
                                                                                [companyArray addObject:[[responseArray objectAtIndex:i]objectForKey:@"symbol"]];
                                                                                
                                                                                  [companyName addObject:[[responseArray objectAtIndex:i]objectForKey:@"name"]];
                                                                                
                                                                            }
                                                                            
                                                                            [responseArray removeAllObjects];
                                                                        }
                                                                    }
                                                                    
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        
                                                                        //                                                          HomePage * home1=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
                                                                        //
                                                                        //                                                          [self.navigationController pushViewController:home1 animated:YES];
                                                                        
                                                                        
                                                                        
                                                                    });
                                                                }];
                    [dataTask resume];
                    
                    
                }
                @catch (NSException * e) {
                    //NSLog(@"Exception: %@", e);
                }
                @finally {
                    //NSLog(@"finally");
                }
                
            }
            
            else if([self.exchangeType isEqualToString:@"CO"])
            {
                @try {
                    NSDictionary *headers = @{
                                              @"cache-control": @"no-cache",
                                              @"authtoken":delegate.zenwiseToken,
                                              @"deviceid":delegate.currentDeviceId
                                              };
                    
                    NSString * urlStr=[NSString stringWithFormat:@"%@stock/fao?exchange=MCX&symbol=%@&limit=20000",delegate.baseUrl,searchStr];
                    
                    
                    //NSLog(@"url %@",urlStr);
                    
                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                       timeoutInterval:10.0];
                    [request setHTTPMethod:@"GET"];
                    [request setAllHTTPHeaderFields:headers];
                    
                    NSURLSession *session = [NSURLSession sharedSession];
                    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                    if (error) {
                                                                        //NSLog(@"%@", error);
                                                                    } else {
                                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                        //NSLog(@"%@", httpResponse);
                                                                        if([httpResponse statusCode]==403)
                                                                        {
                                                                            
                                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                            
                                                                        }
                                                                        else if([httpResponse statusCode]==401)
                                                                        {
                                                                            
                                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                            
                                                                        }
                                                                        else
                                                                        {
                                                                            
                                                                            
                                                                            
                                                                               companyArray=[[NSMutableArray alloc]init];
                                                                            NSMutableArray * responseArray=[[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] mutableCopy];
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            detailsDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSArray arrayWithArray:responseArray],@"details", nil];
                                                                            
                                                                            for(int i=0;i<responseArray.count;i++)
                                                                            {
                                                                                
                                                                                [companyArray addObject:[[responseArray objectAtIndex:i]objectForKey:@"symbol"]];
                                                                                
                                                                                
                                                                                
                                                                            }
                                                                            
                                                                            [responseArray removeAllObjects];
                                                                        }
                                                                    }
                                                                    
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        
                                                                        //                                                          HomePage * home1=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
                                                                        //
                                                                        //                                                          [self.navigationController pushViewController:home1 animated:YES];
                                                                        
                                                                        
                                                                        
                                                                    });
                                                                }];
                    [dataTask resume];
                    
                    
                }
                @catch (NSException * e) {
                    //NSLog(@"Exception: %@", e);
                }
                @finally {
                    //NSLog(@"finally");
                }
                
            }
            
        }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
- (void)loadWeekDays {
    // cancel previous requests
    @try {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(loadWeekDaysInBackground) object:self];
        [self.symbolSearchFld setLoading:false];
        
        // clear previous results
        [self.symbols removeAllObjects];
        [self.symbolSearchFld reloadContents];
        
        // start loading
        [self.symbolSearchFld setLoading:true];
        [self performSelector:@selector(loadWeekDaysInBackground) withObject:self afterDelay:2.0f];
        
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (void)loadWeekDaysInBackground {
    // finish loading
    @try {
        [self.symbolSearchFld setLoading:false];
        self.symbols=[[NSMutableArray alloc]init];
        
        [self.symbols addObjectsFromArray:companyArray];
        
        [self.symbolSearchFld reloadContents];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
//    @try
//    {
//    NSString * searchStr = [self.symbolSearchFld.text stringByReplacingCharactersInRange:range withString:string];
//
//
//        searchStr = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
//                                                                                               NULL,
//                                                                                               (CFStringRef)searchStr,
//                                                                                               NULL,
//                                                                                               (CFStringRef)@"!*'();:@&=+$,/?%#[]",
//                                                                                               kCFStringEncodingUTF8 ));
//
//
//    if([searchStr containsString:@"&"])
//    {
//        searchStr=[searchStr stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
//
//    }
//
//    if(searchStr.length>=1)
//    {
//
//
//
//        if([self.exchangeType isEqualToString:@"EQ"])
//        {
//            @try {
//                NSDictionary *headers = @{
//                                          @"cache-control": @"no-cache",
//                                          @"authtoken":delegate.zenwiseToken,
//                                           @"deviceid":delegate.currentDeviceId
//                                          };
//
//                NSString * urlStr=[NSString stringWithFormat:@"%@stock/searchsymbol/?searchsymbol=%@&exchange=%@&limit=20000",delegate.baseUrl,searchStr,@"NSE"];
//
//
//
//                //NSLog(@"url %@",urlStr);
//
//                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
//                                                                       cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                                   timeoutInterval:10.0];
//                [request setHTTPMethod:@"GET"];
//                [request setAllHTTPHeaderFields:headers];
//
//                NSURLSession *session = [NSURLSession sharedSession];
//                NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
//                                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                                                                if (error) {
//                                                                    //NSLog(@"%@", error);
//                                                                } else {
//                                                                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
//                                                                    //NSLog(@"%@", httpResponse);
//                                                                    if([httpResponse statusCode]==403)
//                                                                    {
//
//                                                                        [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
//
//                                                                    }
//                                                                    else if([httpResponse statusCode]==401)
//                                                                    {
//
//                                                                        [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
//
//                                                                    }
//                                                                    else
//                                                                    {
//
//                                                                    companyArray=[[NSMutableArray alloc]init];
//
//
//                                                                    ;
//
//                                                                   NSMutableArray * responseArray=[[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] mutableCopy];
//
////                                                                    [delegate1.searchSymbolDict setObject:responseArray forKey:@"details"];
//
//
//
//                                                                    //                                                            NSString * string=[NSString stringWithFormat:@"%@",self.responseArray];
//
//                                                                    detailsDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSArray arrayWithArray:responseArray],@"details", nil];
//
//                                                                    if(responseArray.count>0)
//                                                                    {
//
//                                                                        for(int i=0;i<responseArray.count;i++)
//                                                                        {
//
//                                                                            [companyArray addObject:[[responseArray objectAtIndex:i]objectForKey:@"symbol"]];
//
//
//
//                                                                        }
//
//                                                                        [responseArray removeAllObjects];
//                                                                    }
//                                                                    }
//                                                                }
//
//                                                                dispatch_async(dispatch_get_main_queue(), ^{
//
//                                                                    //                                                          HomePage * home1=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
//                                                                    //
//                                                                    //                                                          [self.navigationController pushViewController:home1 animated:YES];
//
//
//
//                                                                });
//                                                            }];
//                [dataTask resume];
//
//
//
//            }
//            @catch (NSException * e) {
//                //NSLog(@"Exception: %@", e);
//            }
//            @finally {
//                //NSLog(@"finally");
//            }
//        }
//
//
//
//        else if([self.exchangeType isEqualToString:@"CR"])
//
//        {
//            @try {
//                NSDictionary *headers = @{
//                                          @"cache-control": @"no-cache",
//                                          @"authtoken":delegate.zenwiseToken,
//                                           @"deviceid":delegate.currentDeviceId
//                                          };
//
//                NSString * urlStr=[NSString stringWithFormat:@"%@stock/fao?exchange=CDS&symbol=%@&limit=20000",delegate.baseUrl,searchStr];
//
//                //NSLog(@"url %@",urlStr);
//
//
//                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
//                                                                       cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                                   timeoutInterval:10.0];
//                [request setHTTPMethod:@"GET"];
//                [request setAllHTTPHeaderFields:headers];
//
//                NSURLSession *session = [NSURLSession sharedSession];
//                NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
//                                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                                                                if (error) {
//                                                                    //NSLog(@"%@", error);
//                                                                } else {
//                                                                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
//                                                                    //NSLog(@"%@", httpResponse);
//                                                                    if([httpResponse statusCode]==403)
//                                                                    {
//
//                                                                        [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
//
//                                                                    }
//                                                                    else if([httpResponse statusCode]==401)
//                                                                    {
//
//                                                                        [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
//
//                                                                    }
//                                                                    else
//                                                                    {
//
//                                                                    companyArray=[[NSMutableArray alloc]init];
//
//
//                                                                    ;
//
//                                                                   NSMutableArray * responseArray=[[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] mutableCopy];
//
//
//
//                                                                    //                                                            NSString * string=[NSString stringWithFormat:@"%@",self.responseArray];
//
//                                                                   detailsDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSArray arrayWithArray:responseArray],@"details", nil];
//
//
//                                                                    if(responseArray.count>0)
//                                                                    {
//
//                                                                        for(int i=0;i<responseArray.count;i++)
//                                                                        {
//
//                                                                            [companyArray addObject:[[responseArray objectAtIndex:i]objectForKey:@"symbol"]];
//
//
//
//                                                                        }
//
//                                                                        [responseArray removeAllObjects];
//                                                                    }
//                                                                    }
//                                                                }
//
//                                                                dispatch_async(dispatch_get_main_queue(), ^{
//
//                                                                    //                                                          HomePage * home1=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
//                                                                    //
//                                                                    //                                                          [self.navigationController pushViewController:home1 animated:YES];
//
//
//
//                                                                });
//                                                            }];
//                [dataTask resume];
//            }
//            @catch (NSException * e) {
//                //NSLog(@"Exception: %@", e);
//            }
//            @finally {
//                //NSLog(@"finally");
//            }
//
//
//
//
//
//
//        }
//
//
//
//
//
//
//
//
//
//        else if ([self.exchangeType isEqualToString:@"DR"])
//        {
//            @try {
//                NSDictionary *headers = @{
//                                          @"cache-control": @"no-cache",
//                                          @"authtoken":delegate.zenwiseToken,
//                                           @"deviceid":delegate.currentDeviceId
//                                          };
//
//                NSString * urlStr=[NSString stringWithFormat:@"%@stock/searchsymbolfao/?exchange=%@&searchsymbol=%@&segment=%@&limit=20000",delegate.baseUrl,@"NFO",searchStr,@"NFO-FUT"];
//
//
//
//                //NSLog(@"url %@",urlStr);
//
//                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
//                                                                       cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                                   timeoutInterval:10.0];
//                [request setHTTPMethod:@"GET"];
//                [request setAllHTTPHeaderFields:headers];
//
//                NSURLSession *session = [NSURLSession sharedSession];
//                NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
//                                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                                                                if (error) {
//                                                                    //NSLog(@"%@", error);
//                                                                } else {
//                                                                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
//                                                                    //NSLog(@"%@", httpResponse);
//                                                                    if([httpResponse statusCode]==403)
//                                                                    {
//
//                                                                        [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
//
//                                                                    }
//                                                                    else if([httpResponse statusCode]==401)
//                                                                    {
//
//                                                                        [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
//
//                                                                    }
//                                                                    else
//                                                                    {
//
//                                                                    companyArray=[[NSMutableArray alloc]init];
//
//
//                                                                    ;
//
//                                                                    NSMutableArray * responseArray=[[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] mutableCopy];
//
//
//
//
//
//
//                                                                    detailsDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSArray arrayWithArray:responseArray],@"details", nil];
//
//                                                                    for(int i=0;i<responseArray.count;i++)
//                                                                    {
//
//                                                                        [companyArray addObject:[[responseArray objectAtIndex:i]objectForKey:@"symbol"]];
//
//
//
//                                                                    }
//
//                                                                    [responseArray removeAllObjects];
//                                                                    }
//                                                                }
//
//                                                                dispatch_async(dispatch_get_main_queue(), ^{
//
//                                                                    //                                                          HomePage * home1=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
//                                                                    //
//                                                                    //                                                          [self.navigationController pushViewController:home1 animated:YES];
//
//
//
//                                                                });
//                                                            }];
//                [dataTask resume];
//
//
//            }
//            @catch (NSException * e) {
//                //NSLog(@"Exception: %@", e);
//            }
//            @finally {
//                //NSLog(@"finally");
//            }
//
//        }
//
//        else if([self.exchangeType isEqualToString:@"CO"])
//        {
//            @try {
//                NSDictionary *headers = @{
//                                          @"cache-control": @"no-cache",
//                                          @"authtoken":delegate.zenwiseToken,
//                                           @"deviceid":delegate.currentDeviceId
//                                          };
//
//                NSString * urlStr=[NSString stringWithFormat:@"%@stock/fao?exchange=MCX&symbol=%@&limit=20000",delegate.baseUrl,searchStr];
//
//
//                //NSLog(@"url %@",urlStr);
//
//                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
//                                                                       cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                                   timeoutInterval:10.0];
//                [request setHTTPMethod:@"GET"];
//                [request setAllHTTPHeaderFields:headers];
//
//                NSURLSession *session = [NSURLSession sharedSession];
//                NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
//                                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                                                                if (error) {
//                                                                    //NSLog(@"%@", error);
//                                                                } else {
//                                                                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
//                                                                    //NSLog(@"%@", httpResponse);
//                                                                    if([httpResponse statusCode]==403)
//                                                                    {
//
//                                                                        [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
//
//                                                                    }
//                                                                    else if([httpResponse statusCode]==401)
//                                                                    {
//
//                                                                        [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
//
//                                                                    }
//                                                                    else
//                                                                    {
//
//                                                                    companyArray=[[NSMutableArray alloc]init];
//
//
//                                                                 NSMutableArray * responseArray=[[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] mutableCopy];
//
//
//
//
//
//                                                                  detailsDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSArray arrayWithArray:responseArray],@"details", nil];
//
//                                                                    for(int i=0;i<responseArray.count;i++)
//                                                                    {
//
//                                                                        [companyArray addObject:[[responseArray objectAtIndex:i]objectForKey:@"symbol"]];
//
//
//
//                                                                    }
//
//                                                                    [responseArray removeAllObjects];
//                                                                    }
//                                                                }
//
//                                                                dispatch_async(dispatch_get_main_queue(), ^{
//
//                                                                    //                                                          HomePage * home1=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
//                                                                    //
//                                                                    //                                                          [self.navigationController pushViewController:home1 animated:YES];
//
//
//
//                                                                });
//                                                            }];
//                [dataTask resume];
//
//
//            }
//            @catch (NSException * e) {
//                //NSLog(@"Exception: %@", e);
//            }
//            @finally {
//                //NSLog(@"finally");
//            }
//
//        }
//
//    }
//
//    return YES;
//
//    }
//    @catch (NSException * e) {
//        //NSLog(@"Exception: %@", e);
//    }
//    @finally {
//        //NSLog(@"finally");
//    }
    
    return YES;
    

}

- (void)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    //
    [self.view endEditing:YES];
    
    //NSLog(@"Selected suggestion at index row - %ld", (long)indexPath.row);
    
   
    
//    NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@", text];
    NSArray *weeks = self.symbols;
    
    self.symbolSearchFld.text = weeks[indexPath.row];
    delegate.wisdomSearchSymbol=self.symbolSearchFld.text;
    
    //    NSIndexPath *selectedIndexPath = [tableView indexPathForSelectedRow];
   
}
- (UITableViewCell *)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
    static NSString *cellIdentifier = @"WeekAutoSuggestionCell";
    @try {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        
        //    NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@", text];
        //    NSArray *weeks = [self.symbols filteredArrayUsingPredicate:filterPredictate];
        NSArray *weeks = self.symbols;
        if(companyName.count>0)
        {
            
            
            NSString * finalName=[NSString stringWithFormat:@"%@(%@)",weeks[indexPath.row],[companyName objectAtIndex:indexPath.row]];
            cell.textLabel.text = finalName;
        }
        else
        {
            NSString * finalName=[NSString stringWithFormat:@"%@",weeks[indexPath.row]];
            cell.textLabel.text = finalName;
        }
        
        
        return cell;
    }
    @catch (NSException *exception) {
        //NSLog(@"%@", exception.reason);
    }
}

- (NSInteger)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section forText:(NSString *)text {
    
//    NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@", text];
    
    @try {
        NSInteger count = self.symbols.count;
        return count;
    }
    @catch (NSException *exception) {
        //NSLog(@"%@", exception.reason);
    }
    
    return 0;
    
}

- (void)autoSuggestionField:(UITextField *)field textChanged:(NSString *)text {
    if (self.searchTimer != nil) {
        [self.searchTimer invalidate];
        self.searchTimer = nil;
    }
    
    // reschedule the search: in 1.0 second, call the searchForKeyword: method on the new textfield content
    self.searchTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                        target: self
                                                      selector: @selector(searchForKeyword:)
                                                      userInfo: self.symbolSearchFld.text
                                                       repeats: NO];
}

- (CGFloat)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
    return 50;
}
@end
