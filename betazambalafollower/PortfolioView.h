//
//  PortfolioView.h
//  testing
//
//  Created by zenwise technologies on 23/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "MQTTKit.h"
@import SocketIO;

@interface PortfolioView : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *positionsTableView;
@property (nonatomic, strong) MQTTClient *client;
- (IBAction)backBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *bottomProfitAndLoss;

@property (strong, nonatomic) IBOutlet UIView *dummyBottomView;
@property (nonatomic, strong)  SocketManager* manager;
@property (nonatomic, strong)  SocketIOClient* socketio;
@property (weak, nonatomic) IBOutlet UIView *postionView;
@property (weak, nonatomic) IBOutlet UIView *noPortfolioView;

@property (weak, nonatomic) IBOutlet UIView *dummyPotfolioView;
@property (strong, nonatomic) IBOutlet UILabel *netProfitAndLoss;
@property (strong, nonatomic) IBOutlet UILabel *assumptionsLabel;


@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityInd;
@property (weak, nonatomic) IBOutlet UITableView *dummyTableView;


@property (weak, nonatomic) IBOutlet UIView *dematView;

@property (weak, nonatomic) IBOutlet UITableView *dematTableView;
@property Reachability * reach;
@property (weak, nonatomic) IBOutlet UIView *bottomInsideView;


@end
