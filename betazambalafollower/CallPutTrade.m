					//
//  OrderViewController1.m
//  ZambalaUSA
//
//  Created by guna on 20/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "CallPutTrade.h"
#import "CallPutTradeCell.h"
#import "AppDelegate.h"
#import "MainOrdersViewController.h"
#import "TagEncode.h"

#import "MT.h"

#import <Foundation/Foundation.h>
#import <CoreFoundation/CoreFoundation.h>
#import <Endian.h>
#import "MTORDER.h"
#import<malloc/malloc.h>
#import "BrokerViewNavigation.h"
#import "OpenAccountView.h"

@interface CallPutTrade ()<NSStreamDelegate,UIWebViewDelegate>
{
    AppDelegate * delegate1;
    NSMutableArray * limitPriceArray;
    NSString * price1;
    NSString * price2;
     NSString * price3;
     NSString * price4;
    NSURL * url1;
    NSString * orderType;
    NSString * quantityCheck;
    NSIndexPath * limitRow;
    NSMutableArray * symbolArray;
    NSMutableArray * limitPrice;
    NSMutableArray * buySellArr;
    NSMutableArray * details;
    MT *sharedManager;
    MTORDER * sharedManagerOrder;
 
    TagEncode * tag1;
    NSMutableArray * localOrderArray;
    NSString * upstoxStatus;
    NSString * upstoxOrderID;
    NSMutableDictionary * mtOrderResponseArray;
    NSString * productType;
    
}

@end

@implementation CallPutTrade

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.marketBtn.layer setBorderColor:[[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1] CGColor]];
    [self.limitBtn.layer setBorderColor:[[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1] CGColor]];
  
    self.marketBtn.layer.borderWidth=1.0f;
    self.marketBtn.layer.cornerRadius=2.0f;
    self.marketBtn.clipsToBounds=YES;
    self.limitBtn.layer.borderWidth=1.0f;
    self.limitBtn.layer.cornerRadius=2.0f;
    self.limitBtn.clipsToBounds=YES;
   
        self.navigationItem.title = @"Order";
    self.orderTableView.delegate=self;
    self.orderTableView.dataSource=self;
    limitPriceArray=[[NSMutableArray alloc]init];
    symbolArray=[[NSMutableArray alloc]init];
    limitPrice=[[NSMutableArray alloc]init];
    buySellArr=[[NSMutableArray alloc]init];
    details=[[NSMutableArray alloc]init];
    localOrderArray=[[NSMutableArray alloc]init];
    [self.deliverySegmentController setSelectedSegmentIndex:0];
    [self onDeliverySegmentTap:self.deliverySegmentController];
    sharedManager = [MT Mt1];
    sharedManagerOrder = [MTORDER Mt2];
    self.activityInd.hidden=YES;
    
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    delegate1.allOrderHistory=[[NSMutableArray alloc]init];
    if([delegate1.brokerNameStr isEqualToString:@"Zerodha"]||[delegate1.brokerNameStr isEqualToString:@"Proficient"])
    {
       
        self.ordertypeLblStr.hidden=YES;
        self.orderTypeViewStr.hidden=YES;
    }
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.orderTableView addGestureRecognizer:gestureRecognizer];
    
    UITapGestureRecognizer *gestureRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view1 addGestureRecognizer:gestureRecognizer1];
    
       self.kiteOrderWebview=[[UIWebView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,self.view.frame.size.height)];
    [self.view1 addSubview:self.kiteOrderWebview];
    self.kiteOrderWebview.delegate=self;
    self.kiteOrderWebview.hidden=YES;
    @try {
        
        if(limitPrice.count>0)
        {
            [limitPrice removeAllObjects];
            [buySellArr removeAllObjects];
            [details removeAllObjects];
        }
        for (int i=0; i<delegate1.optionLimitPriceArray.count; i++)
        {
            
            NSString * local1=[NSString stringWithFormat:@"%@",[delegate1.optionLimitPriceArray objectAtIndex:i]];
            
           
            
            if([local1 isEqualToString:@"check"])
            {
                
            }
            else
            {
                
                [limitPrice addObject:[delegate1.optionLimitPriceArray objectAtIndex:i]];
            }
            
            
            NSString * local2=[NSString stringWithFormat:@"%@",[delegate1.optionOrderArray objectAtIndex:i]];
            
           
            
            if([local2 isEqualToString:@"check"])
            {
                
            }
            
            else
            {
                [details addObject:[delegate1.optionOrderArray objectAtIndex:i]];
                
            }
            
            NSString * local3=[NSString stringWithFormat:@"%@",[delegate1.optionOrderTransactionArray objectAtIndex:i]];
            
            
            
           if([local3 isEqualToString:@"check"])
            {
                
            }
            
            else
            {
                [buySellArr addObject:[delegate1.optionOrderTransactionArray objectAtIndex:i]];
            }
           
            
        }

        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

    //NSLog(@"%@",limitPrice);
     //NSLog(@"%@",buySellArr);
     //NSLog(@"%@",details);
    
    
    
    for(int i=0;i<limitPrice.count;i++)
    {
        NSString * localStr=[limitPrice objectAtIndex:i];
        @try {
            
            if(i==0)
            {
                price1=localStr;
                //NSLog(@"price1%@",price1);
            }
            else if(i==1)
            {
                price2=localStr;
                //NSLog(@"price2%@",price2);
            }
            
            else if(i==2)
            {
                price3=localStr;
                //NSLog(@"price3%@",price3);
            }
            
            else if(i==3)
            {
                price4=localStr;
                //NSLog(@"price4%@",price4);
            }
            
            
        }
        @catch (NSException *exception) {
            //NSLog(@"Something missing...%@",exception);
        }
        @finally {
            
        }

        
    }
    
    
    
    self.submitOrderButton.layer.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.12f] CGColor];
    self.submitOrderButton.layer.shadowOpacity = 0;
    self.submitOrderButton.layer.shadowRadius = 0;
    self.submitOrderButton.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    [self.submitOrderButton addTarget:self action:@selector(submitAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.marketBtn addTarget:self action:@selector(marketAction)  forControlEvents:UIControlEventTouchUpInside];
    
     [self.limitBtn addTarget:self action:@selector(limitAction)  forControlEvents:UIControlEventTouchUpInside];
    if([delegate1.brokerNameStr  isEqualToString:@"Zerodha"])
    {
        [self marketAction];
    }
    else
    {
         [self limitAction];
    }
   
    
    
    self.orderTypeView.layer.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.12f] CGColor];
    self.orderTypeView.layer.shadowOpacity = 0;
    self.orderTypeView.layer.shadowRadius = 2.1;
    self.orderTypeView.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);



    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showMainMenu:)
                                                 name:@"ordersuccess" object:nil];
    
    
    
  
    [[NSNotificationCenter defaultCenter] postNotificationName:@"orderwatch" object:nil];
    if([delegate1.brokerNameStr isEqualToString:@"Zerodha"])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Message"
                                         message:@"Zerodha allows only market orders from Zambala Stocks"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            //Add Buttons
            
            UIAlertAction* okButton = [UIAlertAction
                                       actionWithTitle:@"Cancel"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                           //                                    delegate2.holdingCheck=@"hold";
                                           //Handle your yes please button action here
                                           
                                           [self.navigationController popToRootViewControllerAnimated:YES];
                                           
                                       }];
            //Add your buttons to alert controller
            
            [alert addAction:okButton];
            
            UIAlertAction* proceed = [UIAlertAction
                                      actionWithTitle:@"Proceed"
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          //                                    delegate2.holdingCheck=@"hold";
                                          //Handle your yes please button action here
                                          
                                          
                                          
                                      }];
            //Add your buttons to alert controller
            
            [alert addAction:proceed];
            
            
            [self presentViewController:alert animated:YES completion:nil];
            
        });
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) hideKeyboard {
    CallPutTradeCell * cell=(CallPutTradeCell*)[self.orderTableView cellForRowAtIndexPath:limitRow];
    cell.limitPriceTxtFld.delegate=self;
    
    [cell.limitPriceTxtFld resignFirstResponder];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view1 endEditing:YES];
    

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(details.count>0)
    {
        //NSLog(@"%lu",details.count);
        unsigned long hgt=details.count * 104;
        self.tableViewHgt.constant=hgt;
    return details.count;
    }
    return 0;
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @try
    {
    
    if([orderType isEqualToString:@"LIMIT"])
    {
    CallPutTradeCell * cell=[tableView dequeueReusableCellWithIdentifier:@"order" forIndexPath:indexPath];
//    [[cell.orderTextButton layer] setBorderWidth:1.0f];
//    [[cell.orderTextButton  layer] setBorderColor:[UIColor colorWithRed:69/255.0 green:133/255.0 blue:157/255.0 alpha:1].CGColor];
    cell.limitPriceTxtFld.layer.borderWidth=1.0f;
    cell.limitPriceTxtFld.layer.borderColor=[[UIColor colorWithRed:69/255.0 green:133/255.0 blue:157/255.0 alpha:1]CGColor];
        
       
            if(buySellArr.count>0)
            {
                cell.transactionTypeLbl.text=[buySellArr objectAtIndex:indexPath.row];
                
                NSString * localStr=[buySellArr objectAtIndex:indexPath.row];
                
                if([localStr isEqualToString:@"BUY"])
                {
                    cell.transactionTypeLbl.textColor=[UIColor colorWithRed:24/255.0 green:131/255.0 blue:213/255.0 alpha:1];
                }
                
                else if([localStr isEqualToString:@"SELL"])
                {
                    cell.transactionTypeLbl.textColor=[UIColor colorWithRed:242/255.0 green:30/255.0 blue:51/255.0 alpha:1];
                }
                
                NSString * companyStr=[[details objectAtIndex:indexPath.row] objectForKey:@"symbol"];
                //NSLog(@"symbol%@",companyStr);
                NSString * strike=[[details objectAtIndex:indexPath.row] objectForKey:@"strikeprice"];
                //NSLog(@"symbol%@",strike);
                NSString * option=[[details objectAtIndex:indexPath.row] objectForKey:@"optiontype"];
                //NSLog(@"symbol%@",option);
                NSString * date=[[details objectAtIndex:indexPath.row] objectForKey:@"expiryseries"];
                //NSLog(@"symbol%@",date);
                
                NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                dateFormatter.dateFormat = @"yyyy-MM-dd";
                NSDate *yourDate = [dateFormatter dateFromString:date];
                dateFormatter.dateFormat = @"ddMMMyyyy";
                //NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
                
                NSString * finalDate=[[dateFormatter stringFromDate:yourDate] uppercaseString];
                NSString * finalDate1=[finalDate substringToIndex:5];
                NSString * finalDat2=[finalDate1 substringFromIndex:2];
                NSString * yearStr=[finalDate substringFromIndex:7];
                NSString * orderDate=[yearStr stringByAppendingString:finalDat2];
                
                //NSLog(@"%@",orderDate);
              
                NSString * orderStrSymbol=[NSString stringWithFormat:@"%@%@%@%@",companyStr,orderDate,strike,option];
                
                //NSLog(@"%@",orderStrSymbol);
                NSString * symbol = [NSString stringWithFormat:@"%@",[[details objectAtIndex:indexPath.row] objectForKey:@"securitydesc"]];
               
                if([delegate1.brokerNameStr isEqualToString:@"Upstox"]&&[symbol containsString:@"BANKNIFTY"])
                {
                    [symbolArray addObject:[[details objectAtIndex:indexPath.row] objectForKey:@"securitydescupstox"]];
                }
                else
                {
                    [symbolArray addObject:[[details objectAtIndex:indexPath.row] objectForKey:@"securitydesc"]];
                }
                
                NSString * finalStr=[NSString stringWithFormat:@"%@  %@  %@  %@",companyStr,strike,option,date];
                
                cell.companyLbl.text=finalStr;
                
                cell.quantityTxt.text=[[details objectAtIndex:indexPath.row] objectForKey:@"lotsize"];
                
                cell.limitPriceTxtFld.text=[limitPrice objectAtIndex:indexPath.row];
                
                cell.limitPriceTxtFld.delegate=self;
                cell.limitPriceTxtFld.hidden=NO;
                cell.currencyImg.hidden=NO;
                
                [cell.increment addTarget:self action:@selector(incrementAction:) forControlEvents:UIControlEventTouchUpInside];
                [cell.decrement addTarget:self action:@selector(decrementAction:) forControlEvents:UIControlEventTouchUpInside];
                
                 return cell;
            }
        
        
    }
    
        
        
    
          else if ([orderType isEqualToString:@"MARKET"])
          {
            CallPutTradeCell * cell=[tableView dequeueReusableCellWithIdentifier:@"order" forIndexPath:indexPath];
            //    [[cell.orderTextButton layer] setBorderWidth:1.0f];
            //    [[cell.orderTextButton  layer] setBorderColor:[UIColor colorWithRed:69/255.0 green:133/255.0 blue:157/255.0 alpha:1].CGColor];
            cell.limitPriceTxtFld.layer.borderWidth=1.0f;
            cell.limitPriceTxtFld.layer.borderColor=[[UIColor colorWithRed:69/255.0 green:133/255.0 blue:157/255.0 alpha:1]CGColor];
            
            if(buySellArr.count>0)
            {
                cell.transactionTypeLbl.text=[buySellArr objectAtIndex:indexPath.row];
                
                NSString * localStr=[buySellArr objectAtIndex:indexPath.row];
                
                if([localStr isEqualToString:@"BUY"])
                {
                    cell.transactionTypeLbl.textColor=[UIColor colorWithRed:24/255.0 green:131/255.0 blue:213/255.0 alpha:1];
                }
                
                else if([localStr isEqualToString:@"SELL"])
                {
                    cell.transactionTypeLbl.textColor=[UIColor colorWithRed:242/255.0 green:30/255.0 blue:51/255.0 alpha:1];
                }
                
                NSString * companyStr=[[details objectAtIndex:indexPath.row] objectForKey:@"symbol"];
                NSString * strike=[[details objectAtIndex:indexPath.row] objectForKey:@"strikeprice"];
                
                NSString * option=[[details objectAtIndex:indexPath.row] objectForKey:@"optiontype"];
                
                NSString * date=[[details objectAtIndex:indexPath.row] objectForKey:@"expiryseries"];
                
                NSString * finalStr=[NSString stringWithFormat:@"%@  %@  %@  %@",companyStr,strike,option,date];
                
                cell.companyLbl.text=finalStr;
                
                cell.quantityTxt.text=[[details objectAtIndex:indexPath.row] objectForKey:@"lotsize"];
                
                cell.limitPriceTxtFld.hidden=YES;
                cell.currencyImg.hidden=YES;
                
                [cell.increment addTarget:self action:@selector(incrementAction:) forControlEvents:UIControlEventTouchUpInside];
                [cell.decrement addTarget:self action:@selector(decrementAction:) forControlEvents:UIControlEventTouchUpInside];
                NSString * symbol = [NSString stringWithFormat:@"%@",[[details objectAtIndex:indexPath.row] objectForKey:@"securitydesc"]];
              
                if([delegate1.brokerNameStr isEqualToString:@"Upstox"]&&[symbol containsString:@"BANKNIFTY"])
                {
                    [symbolArray addObject:[[details objectAtIndex:indexPath.row] objectForKey:@"securitydescupstox"]];
                }
                else
                {
                    [symbolArray addObject:[[details objectAtIndex:indexPath.row] objectForKey:@"securitydesc"]];
                }
                
                
            }
            return cell;
            

            
        }
        
        
    
    
     return nil;
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string

{
    @try
    {
    CGPoint pointInsuperview=[textField.superview convertPoint:textField.center toView:self.orderTableView];
    NSIndexPath *  indexPath=[self.orderTableView indexPathForRowAtPoint:pointInsuperview];
    //NSLog(@"%lu",(long)indexPath.row);
    
   
    limitRow=indexPath;
    
    
    
    CallPutTradeCell * cell=(CallPutTradeCell*)[self.orderTableView cellForRowAtIndexPath:indexPath];
    
     NSString * localStr = [cell.limitPriceTxtFld.text stringByReplacingCharactersInRange:range withString:string];
    
    if(localStr.length>0)
    {
            @try {
                
                if(indexPath.row==0)
                {
                    price1=localStr;
                    //NSLog(@"price1%@",price1);
                }
                else if(indexPath.row==1)
                {
                    price2=localStr;
                    //NSLog(@"price2%@",price2);
                }
                
                else if(indexPath.row==2)
                {
                    price3=localStr;
                    //NSLog(@"price3%@",price3);
                }
                
                else if(indexPath.row==3)
                {
                    price4=localStr;
                    //NSLog(@"price4%@",price4);
                }

            
        }
        @catch (NSException *exception) {
            //NSLog(@"Something missing...%@",exception);
        }
        @finally {
            
        }
        

        
       
        
        
        return YES;
        
   }
return YES;
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    }

-(void)limitAction
{
    self.limitBtn.selected=YES;
    self.marketBtn.selected=NO;
    orderType=@"LIMIT";
    self.limitBtn.backgroundColor=[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1];
    self.marketBtn.backgroundColor=[UIColor clearColor];

    
    [self.orderTableView reloadData];
    [self.limitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.marketBtn setTitleColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1] forState:UIControlStateNormal];
  
    
}

-(void)marketAction
{
    self.limitBtn.selected=NO;
    self.marketBtn.selected=YES;
    orderType=@"MARKET";
    self.marketBtn.backgroundColor=[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1];
    self.limitBtn.backgroundColor=[UIColor clearColor];

    [self.orderTableView reloadData];
    [self.marketBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.limitBtn setTitleColor:[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1] forState:UIControlStateNormal];
 
    
    
}
                                   
                  


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)loginCheck
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Please choose an option." message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [self presentViewController:alert animated:YES completion:^{
        
    }];
    UIAlertAction * OpenAccount=[UIAlertAction actionWithTitle:@"Open Broking Account" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //Navigate to open E- Account page
        //        OpenAccountView * openAccountWebView = [self.storyboard instantiateViewControllerWithIdentifier:@"BrokersWebViewControllerUSA"];
        //        [self.navigationController pushViewController:openAccountWebView animated:YES];
        
        OpenAccountView * view=[self.storyboard instantiateViewControllerWithIdentifier:@"OpenAccountView"];
        [self.navigationController pushViewController:view animated:YES];
    }];
    UIAlertAction * Login=[UIAlertAction actionWithTitle:@"Login" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //Navigate to login page
        NSUserDefaults *loggedInUserNew  = [NSUserDefaults standardUserDefaults];
        //
        delegate1.loginActivityStr=@"";
        //             delegate1.loginActivityStr=@"";
        [loggedInUserNew removeObjectForKey:@"loginActivityStr"];
        
        
        
        BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
        
        [self presentViewController:view animated:YES completion:nil];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:OpenAccount];
    [alert addAction:Login];
    [alert addAction:cancel];
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView;
{
    NSString * myString = self.kiteOrderWebview.request.URL.absoluteString;
    //NSLog(@"%@",myString);
    
    if([myString containsString:@"success"])
    {
        delegate1.orderStatusCheck=@"OPEN";
        MainOrdersViewController *nextPage = [self.storyboard instantiateViewControllerWithIdentifier:@"orders"];
        [self presentViewController:nextPage animated:YES completion:nil];
        
    }
    
    else if([myString containsString:@"cancelled"])
    {
        self.kiteOrderWebview.hidden=YES;
       
    }
    
}


-(void)submitAction
{
    @try
    {
    if([delegate1.loginActivityStr isEqualToString:@"OTP1"])
    {
        
//        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Open E-account" message:@"Please have a account with us to trade" preferredStyle:UIAlertControllerStyleAlert];
//
//        [self presentViewController:alert animated:YES completion:^{
//
//        }];
//
//        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//        }];
//
//        [alert addAction:okAction];
        
        [self loginCheck];
        
        
       
        
    }else if ([delegate1.loginActivityStr isEqualToString:@"CLIENT"])
    {
    
        self.activityInd.hidden=NO;
        [self.activityInd startAnimating];
        
    
    if([delegate1.brokerNameStr isEqualToString:@"Zerodha"])
    {
    @try {
        
        self.kiteOrderWebview.hidden=NO;
        orderType=@"MARKET";
        
        NSMutableString * basketStr=[[NSMutableString alloc]init];
        if([productType isEqualToString:@"D"])
        {
            productType = @"NRML";
            
        }
        else if([productType isEqualToString:@"I"])
        {
            productType = @"MIS";
            
        }
        if ([orderType isEqualToString:@"MARKET"])
        {
            
            for(int i=0;i<buySellArr.count;i++)
            {
                
             NSString *  str=[NSMutableString stringWithFormat:@"kite.add({'exchange': '%@','tradingsymbol': '%@','quantity': %@,'transaction_type': '%@','order_type': 'MARKET','product': '%@'});",[[details objectAtIndex:i] objectForKey:@"exchange"],[symbolArray objectAtIndex:i],[[details objectAtIndex:i] objectForKey:@"lotsize"], [buySellArr objectAtIndex:i],productType];
                
                [basketStr appendString:str];
                //NSLog(@"%@",basketStr);
            }
        }
        
        else if ([orderType isEqualToString:@"LIMIT"])
        {
            
           
            for(int i=0;i<buySellArr.count;i++)
            {
                NSString * localPrice=@"";
                if(i==0)
                {
                    localPrice=price1;
                }
                else if(i==1)
                {
                     localPrice=price2;
                }
                else if(i==2)
                {
                     localPrice=price3;
                }
                else if(i==3)
                {
                     localPrice=price4;
                }
                
                NSString *  str=[NSMutableString stringWithFormat:@"kite.add({'exchange': '%@','tradingsymbol': '%@','quantity': %@,'transaction_type': '%@','order_type': 'LIMIT','price':%@,,'product':%@});",[[details objectAtIndex:i] objectForKey:@"exchange"],[symbolArray objectAtIndex:i],[[details objectAtIndex:i] objectForKey:@"lotsize"], [buySellArr objectAtIndex:i],localPrice,productType];
                
                [basketStr appendString:str];
                //NSLog(@"%@",basketStr);
            }
            
        }
        
        NSString * finalStr=[NSString stringWithFormat:@"<html><head><body><button id='custom-button'></button><script src='https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script><script src='https://kite.trade/publisher.js?v=1'></script><script>KiteConnect.ready(function() {var kite = new KiteConnect('%@');%@kite.finished(function(status, request_token) {window.location = 'https://www.zenwise.net/productionwebsite/'+status;});kite.renderButton('#default-button');kite.link('#custom-button');});</script><script>$( document ).ready(function(){document.getElementById('custom-button').click();});</script></body></head></html>",delegate1.APIKey,basketStr];
         [self.kiteOrderWebview loadHTMLString:finalStr baseURL:nil];
        

        
      
        

        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
        
    }
        
    else if([delegate1.brokerNameStr isEqualToString:@"Upstox"])
    {
        [self upstoxOrderMethod];
            
    }
    
    else
    {
        
        [self SendSocketBuyRequest];
    }
        
    }
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

//-(void)secondOrder
//{
//    NSURLSession *session = [NSURLSession sharedSession];
//
//
//
//    if ([orderType isEqualToString:@"MARKET"])
//    {
//
//
//        url1 = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.kite.trade/orders/regular/?api_key=%@&access_token=%@&tradingsymbol=%@&exchange=%@&transaction_type=%@&order_type=%@&quantity=%@&product=NRML&validity=DAY",delegate1.APIKey,delegate1.accessToken,[symbolArray objectAtIndex:1],[[details objectAtIndex:1] objectForKey:@"exchange"], [buySellArr objectAtIndex:1], orderType,[[details objectAtIndex:1] objectForKey:@"lotsize"]]];
//    }
//
//    else if ([orderType isEqualToString:@"LIMIT"])
//    {
//
//        url1 = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.kite.trade/orders/regular/?api_key=%@&access_token=%@&tradingsymbol=%@&exchange=%@&transaction_type=%@&order_type=%@&quantity=%@&product=NRML&validity=DAY&price=%@",delegate1.APIKey,delegate1.accessToken,[symbolArray objectAtIndex:1],[[details objectAtIndex:1] objectForKey:@"exchange"],[buySellArr objectAtIndex:1], orderType,[[details objectAtIndex:1] objectForKey:@"lotsize"],price2]];
//
//
//    }
//
//
//     //NSLog(@"url 2%@",url1);
//
//    @try {
//        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url1
//                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                           timeoutInterval:60.0];
//
//
//
//        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
//
//        [request setHTTPMethod:@"POST"];
//
//        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//            if(data!=nil)
//            {
//            NSDictionary *orderDict=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//            //NSLog(@"orderDict----%@",orderDict);
//
//            NSString * status=[NSString stringWithFormat:@"%@",[orderDict objectForKey:@"status"]];
//
//            //NSLog(@"%@",status);
//
//            dispatch_async(dispatch_get_main_queue(), ^{
//
//                if([status isEqualToString:@"success"])
//                {
//                    //                    UIAlertController * alert = [UIAlertController
//                    //                                                 alertControllerWithTitle:@"Order Status"
//                    //                                                 message:@"Order was placed successfully"
//                    //                                                 preferredStyle:UIAlertControllerStyleAlert];
//                    //
//                    //                    //Add Buttons
//                    //
//                    //                    UIAlertAction* okButton = [UIAlertAction
//                    //                                               actionWithTitle:@"Ok"
//                    //                                               style:UIAlertActionStyleDefault
//                    //                                               handler:^(UIAlertAction * action) {
//                    //                                                   //Handle your yes please button action here
//                    //                                                   MainOrdersViewController *nextPage = [self.storyboard instantiateViewControllerWithIdentifier:@"orders"];
//                    //                                                   [self presentViewController:nextPage animated:YES completion:nil];
//                    //
//                    //
//                    //                                               }];
//                    //                    //Add your buttons to alert controller
//                    //
//                    //                    [alert addAction:okButton];
//
//
//                    //                    [self presentViewController:alert animated:YES completion:nil];
//
//                    if(details.count>=3)
//                    {
//                        [self thirdOrder];
//                    }
//
//                    else
//                    {
//
//                        UIAlertController * alert = [UIAlertController
//                                                     alertControllerWithTitle:@"Order Status"
//                                                     message:@"2 Order was placed successfully"
//                                                     preferredStyle:UIAlertControllerStyleAlert];
//
//                        //Add Buttons
//
//                        UIAlertAction* okButton = [UIAlertAction
//                                                   actionWithTitle:@"Ok"
//                                                   style:UIAlertActionStyleDefault
//                                                   handler:^(UIAlertAction * action) {
//                                                       //Handle your yes please button action here
//                                                       MainOrdersViewController *nextPage = [self.storyboard instantiateViewControllerWithIdentifier:@"orders"];
//                                                       [self presentViewController:nextPage animated:YES completion:nil];
//
//
//                                                   }];
//                        //Add your buttons to alert controller
//
//                        [alert addAction:okButton];
//
//
//                        [self presentViewController:alert animated:YES completion:nil];
//
//
//                    }
//                }
//
//                else
//                {
//                                        UIAlertController * alert = [UIAlertController
//                                                                     alertControllerWithTitle:@"Order Status"
//                                                                     message:@"Please try again"
//                                                                     preferredStyle:UIAlertControllerStyleAlert];
//
//                                        //Add Buttons
//
//                                        UIAlertAction* okButton = [UIAlertAction
//                                                                   actionWithTitle:@"Ok"
//                                                                   style:UIAlertActionStyleDefault
//                                                                   handler:^(UIAlertAction * action) {
//                                                                       //Handle your yes please button action here
//
//                                                                   }];
//                                        //Add your buttons to alert controller
//
//                                        [alert addAction:okButton];
//
//
//                                        [self presentViewController:alert animated:YES completion:nil];
//                }
//
//
//
//
//            });
//
//            }
//
//        }];
//
//        [postDataTask resume];
//
//    }
//    @catch (NSException *exception) {
//        //NSLog(@"Something missing...");
//    }
//    @finally {
//
//    }
//
//    //    MainOrdersViewController *nextPage = [self.storyboard instantiateViewControllerWithIdentifier:@"orders"];
//    //    [self.navigationController pushViewController:nextPage animated:YES];
//    //
//
//
//
//
//}

//-(void)thirdOrder
//{
//    NSURLSession *session = [NSURLSession sharedSession];
//
//
//
//    if ([orderType isEqualToString:@"MARKET"])
//    {
//
//
//        url1 = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.kite.trade/orders/regular/?api_key=%@&access_token=%@&tradingsymbol=%@&exchange=%@&transaction_type=%@&order_type=%@&quantity=%@&product=NRML&validity=DAY",delegate1.APIKey,delegate1.accessToken,[symbolArray objectAtIndex:2],[[details objectAtIndex:2] objectForKey:@"exchange"], [buySellArr objectAtIndex:2], orderType,[[details objectAtIndex:2] objectForKey:@"lotsize"]]];
//    }
//
//    else if ([orderType isEqualToString:@"LIMIT"])
//    {
//
//        url1 = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.kite.trade/orders/regular/?api_key=%@&access_token=%@&tradingsymbol=%@&exchange=%@&transaction_type=%@&order_type=%@&quantity=%@&product=NRML&validity=DAY&price=%@",delegate1.APIKey,delegate1.accessToken,[symbolArray objectAtIndex:2],[[details objectAtIndex:2] objectForKey:@"exchange"],[buySellArr objectAtIndex:2], orderType,[[details objectAtIndex:2] objectForKey:@"lotsize"],price3]];
//
//
//    }
//
//
//     //NSLog(@"url 3%@",url1);
//
//    @try {
//        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url1
//                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                           timeoutInterval:60.0];
//
//
//
//        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
//
//        [request setHTTPMethod:@"POST"];
//
//        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//            if(data!=nil)
//            {
//            NSDictionary *orderDict=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//            //NSLog(@"orderDict----%@",orderDict);
//
//            NSString * status=[NSString stringWithFormat:@"%@",[orderDict objectForKey:@"status"]];
//
//            //NSLog(@"%@",status);
//
//            dispatch_async(dispatch_get_main_queue(), ^{
//
//                if([status isEqualToString:@"success"])
//                {
//                    //                    UIAlertController * alert = [UIAlertController
//                    //                                                 alertControllerWithTitle:@"Order Status"
//                    //                                                 message:@"Order was placed successfully"
//                    //                                                 preferredStyle:UIAlertControllerStyleAlert];
//                    //
//                    //                    //Add Buttons
//                    //
//                    //                    UIAlertAction* okButton = [UIAlertAction
//                    //                                               actionWithTitle:@"Ok"
//                    //                                               style:UIAlertActionStyleDefault
//                    //                                               handler:^(UIAlertAction * action) {
//                    //                                                   //Handle your yes please button action here
//                    //                                                   MainOrdersViewController *nextPage = [self.storyboard instantiateViewControllerWithIdentifier:@"orders"];
//                    //                                                   [self presentViewController:nextPage animated:YES completion:nil];
//                    //
//                    //
//                    //                                               }];
//                    //                    //Add your buttons to alert controller
//                    //
//                    //                    [alert addAction:okButton];
//
//
//                    //                    [self presentViewController:alert animated:YES completion:nil];
//
//                    if(details.count>=4)
//                    {
//                        [self fourthOrder];
//                    }
//
//                    else
//                    {
//
//                        UIAlertController * alert = [UIAlertController
//                                                     alertControllerWithTitle:@"Order Status"
//                                                     message:@"3 Order was placed successfully"
//                                                     preferredStyle:UIAlertControllerStyleAlert];
//
//                        //Add Buttons
//
//                        UIAlertAction* okButton = [UIAlertAction
//                                                   actionWithTitle:@"Ok"
//                                                   style:UIAlertActionStyleDefault
//                                                   handler:^(UIAlertAction * action) {
//                                                       //Handle your yes please button action here
//                                                       MainOrdersViewController *nextPage = [self.storyboard instantiateViewControllerWithIdentifier:@"orders"];
//                                                       [self presentViewController:nextPage animated:YES completion:nil];
//
//
//                                                   }];
//                        //Add your buttons to alert controller
//
//                        [alert addAction:okButton];
//
//
//                        [self presentViewController:alert animated:YES completion:nil];
//
//
//                    }
//                }
//
//                else
//                {
//                                        UIAlertController * alert = [UIAlertController
//                                                                     alertControllerWithTitle:@"Order Status"
//                                                                     message:@"Please try again"
//                                                                     preferredStyle:UIAlertControllerStyleAlert];
//
//                                        //Add Buttons
//
//                                        UIAlertAction* okButton = [UIAlertAction
//                                                                   actionWithTitle:@"Ok"
//                                                                   style:UIAlertActionStyleDefault
//                                                                   handler:^(UIAlertAction * action) {
//                                                                       //Handle your yes please button action here
//
//                                                                   }];
//                                        //Add your buttons to alert controller
//
//                                        [alert addAction:okButton];
//
//
//                                        [self presentViewController:alert animated:YES completion:nil];
//                }
//
//
//
//
//            });
//
//
//            }
//        }];
//
//        [postDataTask resume];
//
//    }
//    @catch (NSException *exception) {
//        //NSLog(@"Something missing...");
//    }
//    @finally {
//
//    }
//
//    //    MainOrdersViewController *nextPage = [self.storyboard instantiateViewControllerWithIdentifier:@"orders"];
//    //    [self.navigationController pushViewController:nextPage animated:YES];
//    //
//
//
//
//
//}

//-(void)fourthOrder
//{
//    NSURLSession *session = [NSURLSession sharedSession];
//
//
//
//    if ([orderType isEqualToString:@"MARKET"])
//    {
//
//
//        url1 = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.kite.trade/orders/regular/?api_key=%@&access_token=%@&tradingsymbol=%@&exchange=%@&transaction_type=%@&order_type=%@&quantity=%@&product=NRML&validity=DAY",delegate1.APIKey,delegate1.accessToken,[symbolArray objectAtIndex:3],[[details objectAtIndex:3] objectForKey:@"exchange"], [buySellArr objectAtIndex:3], orderType,[[details objectAtIndex:3] objectForKey:@"lotsize"]]];
//    }
//
//    else if ([orderType isEqualToString:@"LIMIT"])
//    {
//
//        url1 = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.kite.trade/orders/regular/?api_key=%@&access_token=%@&tradingsymbol=%@&exchange=%@&transaction_type=%@&order_type=%@&quantity=%@&product=NRML&validity=DAY&price=%@",delegate1.APIKey,delegate1.accessToken,[symbolArray objectAtIndex:3],[[details objectAtIndex:3] objectForKey:@"exchange"],[buySellArr objectAtIndex:3], orderType,[[details objectAtIndex:3] objectForKey:@"lotsize"],price4]];
//
//
//    }
//
//     //NSLog(@"url 4%@",url1);
//
//
//    @try {
//        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url1
//                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                           timeoutInterval:60.0];
//
//
//
//        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
//
//        [request setHTTPMethod:@"POST"];
//
//        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//            if(data!=nil)
//            {
//            NSDictionary *orderDict=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//            //NSLog(@"orderDict----%@",orderDict);
//
//            NSString * status=[NSString stringWithFormat:@"%@",[orderDict objectForKey:@"status"]];
//
//            //NSLog(@"%@",status);
//
//            dispatch_async(dispatch_get_main_queue(), ^{
//
//                if([status isEqualToString:@"success"])
//                {
//                                        UIAlertController * alert = [UIAlertController
//                                                                     alertControllerWithTitle:@"Order Status"
//                                                                     message:@"4 Order was placed successfully"
//                                                                     preferredStyle:UIAlertControllerStyleAlert];
//
//                                        //Add Buttons
//
//                                        UIAlertAction* okButton = [UIAlertAction
//                                                                   actionWithTitle:@"Ok"
//                                                                   style:UIAlertActionStyleDefault
//                                                                   handler:^(UIAlertAction * action) {
//                                                                       //Handle your yes please button action here
//                                                                       MainOrdersViewController *nextPage = [self.storyboard instantiateViewControllerWithIdentifier:@"orders"];
//                                                                       [self presentViewController:nextPage animated:YES completion:nil];
//
//
//                                                                   }];
//                                        //Add your buttons to alert controller
//
//                                        [alert addAction:okButton];
//
//
//                                        [self presentViewController:alert animated:YES completion:nil];
//
//                }
//
//                else
//                {
//                                        UIAlertController * alert = [UIAlertController
//                                                                     alertControllerWithTitle:@"Order Status"
//                                                                     message:@"Please try again"
//                                                                     preferredStyle:UIAlertControllerStyleAlert];
//
//                                        //Add Buttons
//
//                                        UIAlertAction* okButton = [UIAlertAction
//                                                                   actionWithTitle:@"Ok"
//                                                                   style:UIAlertActionStyleDefault
//                                                                   handler:^(UIAlertAction * action) {
//                                                                       //Handle your yes please button action here
//
//                                                                   }];
//                                        //Add your buttons to alert controller
//
//                                        [alert addAction:okButton];
//
//
//                                        [self presentViewController:alert animated:YES completion:nil];
//                }
//
//
//
//
//            });
//
//            }
//
//        }];
//
//        [postDataTask resume];
//
//    }
//    @catch (NSException *exception) {
//        //NSLog(@"Something missing...");
//    }
//    @finally {
//
//    }
//
//    //    MainOrdersViewController *nextPage = [self.storyboard instantiateViewControllerWithIdentifier:@"orders"];
//    //    [self.navigationController pushViewController:nextPage animated:YES];
//    //
//
//
//
//
//}

-(void)incrementAction:(UIButton*)sender
{
    //    if (sender.tag == 0)
    //    {
    //
    //    }
    @try
    {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.orderTableView];
    
   
    
    NSIndexPath *indexPath = [self.orderTableView indexPathForRowAtPoint:buttonPosition];
    CallPutTradeCell *cell = [self.orderTableView cellForRowAtIndexPath:indexPath];
    
    id i= [[details objectAtIndex:indexPath.row] objectForKey:@"lotsize"];
    
    int j=[i intValue];
    
    int k=[i intValue];
    
    if (j>=k) {
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber * myNumber = [f numberFromString:[NSString stringWithFormat:@"%@",cell.quantityTxt.text]];
        
        myNumber = @([myNumber intValue] + [i intValue]);
        
        
        
        j =(int)[myNumber integerValue];
        //NSLog(@"i %d",j);

        NSString *convertNumber = [f stringForObjectValue:myNumber];
        
        
        
        cell.quantityTxt.text=convertNumber;
    }
    

    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    
}

-(void)decrementAction:(UIButton*)sender
{
    @try
    {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.orderTableView];
    
    
    
    NSIndexPath *indexPath = [self.orderTableView indexPathForRowAtPoint:buttonPosition];
    CallPutTradeCell *cell = [self.orderTableView cellForRowAtIndexPath:indexPath];
    
    id i= [[details objectAtIndex:indexPath.row] objectForKey:@"lotsize"];
    if(cell.quantityTxt.text.length>0)
    {
     quantityCheck=cell.quantityTxt.text;
    }
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber * myNumber = [f numberFromString:[NSString stringWithFormat:@"%@",quantityCheck]];
    
    

    
     int  j=[myNumber intValue];
    
    //NSLog(@"%i",j);
    
    NSNumberFormatter *f1 = [[NSNumberFormatter alloc] init];
    f1.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber * myNumber1 = [f1 numberFromString:[NSString stringWithFormat:@"%@",i]];
    
    
    
    
    int  k=[myNumber1 intValue];
    
    //NSLog(@"%i",k);
    
   
    
    

    if (j>k) {
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber * myNumber = [f numberFromString:[NSString stringWithFormat:@"%@",cell.quantityTxt.text]];
        
        myNumber = @([myNumber intValue] -k);
        j =(int)[myNumber integerValue];
        //NSLog(@"i %i",j);
        NSString *convertNumber = [f stringForObjectValue:myNumber];
        
        
        
        cell.quantityTxt.text=convertNumber;
    }
    
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    //
    
      
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}

- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    [self.view1 setFrame:CGRectMake(0,-50,self.view.frame.size.width,self.view.frame.size.height)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view1 endEditing:YES];
    return YES;
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    [self.view1 setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
}


-(void)viewWillAppear:(BOOL)animated
{
    if([delegate1.dismissCheck isEqualToString:@"dismissed"])
    {
        
        delegate1.dismissCheck=@"";
        
        if([delegate1.navigationCheck isEqualToString:@"TOP"])
        {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else
        {
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
        }
        
        //    [self.navigationController popToViewController:homePage animated:YES];
        
        
        
        
        
        
        //    [self.navigationController popToViewController:homePage animated:YES];
        
    }
    
    
}

-(void)SendSocketBuyRequest
{
    @try
    {
    self.activityInd.hidden=NO;
    [self.activityInd startAnimating];
    for(int i=0;i<details.count;i++)
    {
    
   
    delegate1.referanceOrderNo=delegate1.referanceOrderNo+1;
    int orderno = delegate1.referanceOrderNo;
    
   
    int orderTypeInt = 1;
    
    if([orderType isEqualToString:@"MARKET"])
    {
        orderTypeInt=1;
    }
    
    else if([orderType isEqualToString:@"LIMIT"])
    {
        orderTypeInt=2;
    }
    
    if([orderType isEqualToString:@"SL"])
    {
        orderTypeInt=4;
    }
    NSString * orderQty;
   
        orderQty=[[details objectAtIndex:i] objectForKey:@"lotsize"];
    
    
    int inOrderqty =[orderQty intValue];
    int inPendingqty= inOrderqty;
    NSString * buysell=[NSString stringWithFormat:@"%@",[buySellArr objectAtIndex:i]];
        if([buysell isEqualToString:@"BUY"])
        {
            buysell=@"1";
        }
        
       else if([buysell isEqualToString:@"SELL"])
        {
            buysell=@"2";
            
        }
    int btSide =[buysell intValue];
    int btOrderType =orderTypeInt;
//    NSString *stClientID=@"P202230";
      //  NSString *stClientID=[NSString stringWithFormat:@"%@",[delegate1.stdealerId objectAtIndex:0]];
    //    NSString  * stClientID=@"OWN";
        NSString * localLimit;
        
        if(i==0)
        {
            
           localLimit=price1;
        }
       else if(i==1)
        {
            localLimit=price2;
            
        }
       else if(i==2)
       {
            localLimit=price3;
           
       }
       else if(i==3)
       {
            localLimit=price4;
           
       }
    
    
    double price=[localLimit doubleValue];
    //    double dbTriggerPirce=84;
    
    NSString * segment=[[details objectAtIndex:i] objectForKey:@"segment"];
    
    if([segment containsString:@"NFO"])
    {
        segment=@"NSEFO";
        
    }
    
    else if([segment containsString:@"BFO"])
    {
        segment=@"BSEFO";
        
    }
    
    NSString * security=[[details objectAtIndex:i] objectForKey:@"exchange_token"];
    
//    TagEncode * tag = [[TagEncode alloc]init];
//    delegate1.mtCheck=true;
//    [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.Order];
//    [tag TagData:sharedManager.inOrderNo intMethod:orderno];
//    [tag TagData:sharedManager.nlTagFooter];
//    [tag TagData:sharedManager.stExchange stringMethod:segment];
//    [tag TagData:sharedManager.stSecurityID stringMethod:security];
//    [tag TagData:sharedManager.inOrderQty intMethod:inOrderqty];
//    [tag TagData:sharedManager.inPendingQty intMethod:inPendingqty];
//    [tag TagData:sharedManager.dbPrice doubleMethod:price];
//
//    [tag TagData:sharedManager.btSide byteMethod:btSide];
//    [tag TagData:sharedManager.btOrderType byteMethod:btOrderType];
//
//    //    [tag TagData:sharedManager.dbPrice doubleMethod:dbTriggerPirce];
//
//    [tag TagData:sharedManager.stClientID stringMethod:stClientID];
//    [tag TagData:sharedManager.btTimeinForce byteMethod:btTimeinForce];
//
//    [tag TagData:sharedManager.inProductType intMethod:inProductType];
//    [tag TagData:sharedManager.nlTagFooter];
//
//    [tag GetBuffer];
//
//    //    NewLoginViewController * new = [[NewLoginViewController alloc]init];
//    //    [new newMessage];
//
//    delegate1.mtOrderCheck=true;
//
    
    if([productType isEqualToString:@"D"])
    {
        productType = @"1";
    }
    else if([productType isEqualToString:@"I"])
    {
         productType = @"2";
    }
   
    
        TagEncode * enocde=[[TagEncode alloc]init];
        mtOrderResponseArray=[[NSMutableDictionary alloc]init];
        enocde.inputRequestString=@"order";
        
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        
        
        [inputArray addObject:delegate1.accessToken];
        [inputArray addObject:delegate1.userID];
        [inputArray addObject:segment];
        [inputArray addObject:security];
        [inputArray addObject:[NSString stringWithFormat:@"%i",inOrderqty]];
        [inputArray addObject:[NSString stringWithFormat:@"%i",inPendingqty]];
        [inputArray addObject:[NSString stringWithFormat:@"%f",price]];
        [inputArray addObject:[NSString stringWithFormat:@"%i",btSide]];
        [inputArray addObject:[NSString stringWithFormat:@"%i",btOrderType]];
        [inputArray addObject:[NSString stringWithFormat:@"%@multitradeapi/placeorderrequest",delegate1.mtBaseUrl]];
        [inputArray addObject:[NSString stringWithFormat:@"%@",[symbolArray objectAtIndex:i]]];
        [inputArray addObject:[NSString stringWithFormat:@"%@",productType]];
        
        
        [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            
            //NSLog(@"%@",dict);
            
            mtOrderResponseArray=[[dict objectForKey:@"data"] objectForKey:@"data"];
            
            if(mtOrderResponseArray.count>0)
            {
                
                
                
                NSString * status=[NSString stringWithFormat:@"%@",[mtOrderResponseArray objectForKey:@"orderstatus"]];
                //            int orderSituation=[[[mtOrderResponseArray objectAtIndex:0] objectForKey:@"btOrderSituation"] intValue];
                
            
                
                if([status isEqualToString:@"Pending"])
                {
                    delegate1.orderStatusCheck=@"OPEN";
                    
                }
                
                else   if([status isEqualToString:@"Completed"])
                {
                    delegate1.orderStatusCheck=@"COMPLETED";
                    
                }
                
                else if([status isEqualToString:@"Cancelled"])
                {
                    delegate1.orderStatusCheck=@"REJECTED";
                    
                }
                
                else if([status isEqualToString:@"Rejected"])
                {
                    delegate1.orderStatusCheck=@"REJECTED";
                    
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:@"Order Status"
                                             message:@"Order was placed successfully"
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                //Add Buttons
                
                UIAlertAction* okButton = [UIAlertAction
                                           actionWithTitle:@"Ok"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
                                               
                                               
                                               MainOrdersViewController *nextPage = [self.storyboard instantiateViewControllerWithIdentifier:@"orders"];
                                               [self presentViewController:nextPage animated:YES completion:nil];
                                               
                                               
                                               
                                               //                                       delegate2.holdingCheck=@"hold";
                                               //Handle your yes please button action here
                                               
                                               
                                               
                                           }];
                //Add your buttons to alert controller
                
                [alert addAction:okButton];
                
                
                [self presentViewController:alert animated:YES completion:nil];
                
                });
                
           
              
                //            }
                
            }
            
        }];
    }
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    
    
    
    
    
    
    
    
    
    
  
    
}



- (void)showMainMenu:(NSNotification *)note
{
//    for(int i=0;i<delegate1.allOrderHistory.count;i++)
//    {
//        [localOrderArray addObject:[delegate1.allOrderHistory objectAtIndex:i]];
//
//    }
//    if(localOrderArray.count==details.count)
//    {
//
   
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Order Status"
                                 message:@"Order was placed successfully"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   
                                   
                                   MainOrdersViewController *nextPage = [self.storyboard instantiateViewControllerWithIdentifier:@"orders"];
                                   [self presentViewController:nextPage animated:YES completion:nil];
                                   
                                   
                                   
                                   //                                       delegate2.holdingCheck=@"hold";
                                   //Handle your yes please button action here
                                   
                                   
                                   
                               }];
    //Add your buttons to alert controller
    
    [alert addAction:okButton];
    
    
    [self presentViewController:alert animated:YES completion:nil];
   
   
        
//    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    return 90;
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    self.activityInd.hidden=YES;
    [self.activityInd stopAnimating];
}
-(void)upstoxOrderMethod
{
    @try
    {
    NSMutableArray * orderIdArray=[[NSMutableArray alloc]init];
    NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate1.accessToken];
    NSDictionary *   headers = @{ @"cache-control": @"no-cache",
                                  @"Content-Type" : @"application/json",
                                  @"authorization":access,
                                  @"x-api-key":delegate1.APIKey
                                  };
    NSString * orderTypeString=[NSString stringWithFormat:@"https://api.upstox.com/live/orders"];

 
    
    
    for(int i=0;i<buySellArr.count;i++)
    {
        NSString * transTypeStr=[NSString stringWithFormat:@"%@",[buySellArr objectAtIndex:i]];
         NSString * segment;
        NSString * localLimit;
        
        if(i==0)
        {
            
            localLimit=price1;
        }
        else if(i==1)
        {
            localLimit=price2;
            
        }
        else if(i==2)
        {
            localLimit=price3;
            
        }
        else if(i==3)
        {
            localLimit=price4;
            
        }
        
        if([transTypeStr isEqualToString:@"BUY"])
        {
            transTypeStr=@"b";
        }
        
        else if([transTypeStr isEqualToString:@"SELL"])
        {
            transTypeStr=@"s";
            
        }
        
        NSString * orderSeg=[NSString stringWithFormat:@"%@",[[details objectAtIndex:0] objectForKey:@"exchange"]];
        
        if([orderSeg containsString:@"NSE"])
        {
            segment=@"NSE_EQ";
        }
        
        else if([orderSeg containsString:@"BSE"])
        {
            segment=@"BSE_EQ";
        }
        
        else if([orderSeg containsString:@"NFO"])
        {
            segment=@"NSE_FO";
        }
        
        else if([orderSeg containsString:@"BFO"])
        {
            segment=@"BSE_FO";
        }
        
        else if([orderSeg containsString:@"CDS"])
        {
            segment=@"NCD_FO";
        }
        
        if([orderType isEqualToString:@"MARKET"])
        {
            orderType=@"m";
        }
        
        else if([orderType isEqualToString:@"LIMIT"])
        {
            orderType=@"l";
        }
        
        else if([orderType isEqualToString:@"SL"])
        {
            orderType=@"sl";
        }
      
        NSDictionary * upStoxOrderParms;
        CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)[symbolArray objectAtIndex:i], NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
        
        NSString * symbol=[NSString stringWithFormat:@"%@",newString];
        
            if([orderType isEqualToString:@"m"])
            {
             
                upStoxOrderParms = @{ @"transaction_type":transTypeStr,
                                      @"exchange":segment,
                                      @"symbol":symbol,
                                      @"quantity":[[details objectAtIndex:i] objectForKey:@"lotsize"],
                                      @"order_type":orderType,
                                      @"product":productType
                                      
                                      };
                
                
            }
            
            else if([orderType isEqualToString:@"l"])
            {
                
                upStoxOrderParms = @{ @"transaction_type":transTypeStr,
                                      @"exchange":segment,
                                      @"symbol":symbol,
                                      @"quantity":[[details objectAtIndex:i] objectForKey:@"lotsize"],
                                      @"order_type":orderType,
                                      @"product":productType,
                                      @"price":localLimit
                                      
                                      
                                      };
                
                
            }
           
       
        
    NSData *  postData = [NSJSONSerialization dataWithJSONObject:upStoxOrderParms options:0 error:nil];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:orderTypeString]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:40.0];
        //NSLog(@"%@",request);
        
        NSURLSession *session = [NSURLSession sharedSession];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        
        
        
        
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data1, NSURLResponse *response, NSError *error) {
            
            NSDictionary *orderDict=[NSJSONSerialization JSONObjectWithData:data1 options:0 error:nil];
            
            if(data1!=nil)
            {
                NSString * status;
                //NSLog(@"orderDict----%@",orderDict);
                
                
                    upstoxStatus=[NSString stringWithFormat:@"%@",[orderDict objectForKey:@"message"]];
                    
                    upstoxOrderID=[NSString stringWithFormat:@"%@",[[orderDict objectForKey:@"data"]objectForKey:@"order_id"]];
                
                [orderIdArray addObject:upstoxOrderID];
                
                
                //NSLog(@"%@",status);
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                        
//                    if(buySellArr.count==orderIdArray.count)
//                    {
                        UIAlertController * alert = [UIAlertController
                                                     alertControllerWithTitle:@"Order Status"
                                                     message:@"Order was placed successfully"
                                                     preferredStyle:UIAlertControllerStyleAlert];
                        
                        //Add Buttons
                        
                        UIAlertAction* okButton = [UIAlertAction
                                                   actionWithTitle:@"Ok"
                                                   style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       //Handle your yes please button action here
                                                       MainOrdersViewController *nextPage = [self.storyboard instantiateViewControllerWithIdentifier:@"orders"];
                                                       [self presentViewController:nextPage animated:YES completion:nil];
                                                       
                                                       
                                                   }];
                        //Add your buttons to alert controller
                        
                        [alert addAction:okButton];
                        
                        
                        [self presentViewController:alert animated:YES completion:nil];
                        
//
//                    }
//
//
//
//
//                    else
//                    {
//                        UIAlertController * alert = [UIAlertController
//                                                     alertControllerWithTitle:@"Order Status"
//                                                     message:@"Please try again"
//                                                     preferredStyle:UIAlertControllerStyleAlert];
//
//                        //Add Buttons
//
//                        UIAlertAction* okButton = [UIAlertAction
//                                                   actionWithTitle:@"Ok"
//                                                   style:UIAlertActionStyleDefault
//                                                   handler:^(UIAlertAction * action) {
//                                                       //Handle your yes please button action here
//
//                                                   }];
//                        //Add your buttons to alert controller
//
//                        [alert addAction:okButton];
//
//
//                        [self presentViewController:alert animated:YES completion:nil];
//                    }
                    
                    
                    
                    
                });
                
            }
            
            
            
        }];
        
        [postDataTask resume];
        
        
        
    
        
    }
   
        
       
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    
  
        
    
        
    
}



- (IBAction)onDeliverySwitchTap:(id)sender {
}
- (IBAction)onDeliverySegmentTap:(id)sender {
    
    if(self.deliverySegmentController.selectedSegmentIndex == 0)
    {
        productType = @"D";
    }
    else if(self.deliverySegmentController.selectedSegmentIndex == 1)
    {
         productType = @"I";
    }
}
@end
