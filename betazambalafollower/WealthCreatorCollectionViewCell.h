//
//  WealthCreatorCollectionViewCell.h
//  betazambalafollower
//
//  Created by Zenwise Technologies on 22/03/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WealthCreatorCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *symbolLabel;
@property (weak, nonatomic) IBOutlet UILabel *returnTimesLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentValueLabel;

@end
