//
//  NewSelectBrokerCollectionViewCell.m
//  NewUI
//
//  Created by Zenwise Technologies on 22/05/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "NewSelectBrokerCollectionViewCell.h"

@implementation NewSelectBrokerCollectionViewCell


- (void)awakeFromNib {
    [super awakeFromNib];
    self.brokerImageButton.layer.borderWidth = 0.0f;
    self.brokerImageButton.layer.borderColor = [[UIColor clearColor] CGColor];
    self.brokerNameLabel.textColor = [UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0];
}
@end
