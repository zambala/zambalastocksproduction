//
//  FeedCollectionViewCell3.h
//  testing
//
//  Created by zenwise mac 2 on 12/26/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedCollectionViewCell3 : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic) IBOutlet UILabel *descripLbl3;
@property (strong, nonatomic) IBOutlet UILabel *timeLbl3;

@end
