//
//  NotificationTonesPopup.m
//  betazambalaleader
//
//  Created by zenwise mac 2 on 3/23/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "NotificationTonesPopup.h"
#import "UIViewController+MJPopupViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "ProfileSettings.h"
#import "AppDelegate.h"



@interface NotificationTonesPopup ()
{
    BOOL check;
    AppDelegate * delegate1;
}

@end

@implementation NotificationTonesPopup
{
    NSMutableArray *audioFileList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    [self.noneButton setImage:[UIImage imageNamed:@"radioUnselect.png"] forState:UIControlStateNormal];
    [self.noneButton setImage:[UIImage imageNamed:@"radioSelected.png"] forState:UIControlStateSelected];
    
    [self.tone1Button setImage:[UIImage imageNamed:@"radioUnselect.png"] forState:UIControlStateNormal];
    [self.tone1Button setImage:[UIImage imageNamed:@"radioSelected.png"] forState:UIControlStateSelected];

    
    [self.tone2Button setImage:[UIImage imageNamed:@"radioUnselect.png"] forState:UIControlStateNormal];
    [self.tone2Button setImage:[UIImage imageNamed:@"radioSelected.png"] forState:UIControlStateSelected];

    
    [self.tone3Button setImage:[UIImage imageNamed:@"radioUnselect.png"] forState:UIControlStateNormal];
    [self.tone3Button setImage:[UIImage imageNamed:@"radioSelected.png"] forState:UIControlStateSelected];

    
    [self.tone4button setImage:[UIImage imageNamed:@"radioUnselect.png"] forState:UIControlStateNormal];
    [self.tone4button setImage:[UIImage imageNamed:@"radioSelected.png"] forState:UIControlStateSelected];

    
    [self.tone5Button setImage:[UIImage imageNamed:@"radioUnselect.png"] forState:UIControlStateNormal];
    [self.tone5Button setImage:[UIImage imageNamed:@"radioSelected.png"] forState:UIControlStateSelected];

    
    
    [self.noneButton addTarget:self action:@selector(noneButtonTap) forControlEvents:UIControlEventTouchUpInside];
     [self.tone1Button addTarget:self action:@selector(tone1ButtonTap) forControlEvents:UIControlEventTouchUpInside];
     [self.tone2Button addTarget:self action:@selector(tone2ButtonTap) forControlEvents:UIControlEventTouchUpInside];
     [self.tone3Button addTarget:self action:@selector(tone3ButtonTap) forControlEvents:UIControlEventTouchUpInside];
     [self.tone4button addTarget:self action:@selector(tone4ButtonTap) forControlEvents:UIControlEventTouchUpInside];
     [self.tone5Button addTarget:self action:@selector(tone5ButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self loadAudioFileList];
    // Do any additional setup after loading the view from its nib.
}


-(void)loadAudioFileList{
    audioFileList = [[NSMutableArray alloc] init];
    
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSURL *directoryURL = [NSURL URLWithString:@"/System/Library/Audio/UISounds"];
    NSArray *keys = [NSArray arrayWithObject:NSURLIsDirectoryKey];
    
    NSDirectoryEnumerator *enumerator = [fileManager
                                         enumeratorAtURL:directoryURL
                                         includingPropertiesForKeys:keys
                                         options:0
                                         errorHandler:^(NSURL *url, NSError *error) {
                                             // Handle the error.
                                             // Return YES if the enumeration should continue after the error.
                                             return YES;
                                         }];
    
    for (NSURL *url in enumerator) {
        NSError *error;
        NSNumber *isDirectory = nil;
        if (! [url getResourceValue:&isDirectory forKey:NSURLIsDirectoryKey error:&error]) {
            // handle error
        }
        else if (! [isDirectory boolValue]) {
            [audioFileList addObject:url];
        }
    }
    
    //NSLog(@"%lu",(unsigned long)audioFileList.count);
}



-(void)noneButtonTap
{
    self.noneButton.selected=YES;
    self.tone1Button.selected=NO;
    self.tone2Button.selected=NO;
    self.tone3Button.selected=NO;
    self.tone4button.selected=NO;
    self.tone5Button.selected=NO;
    delegate1.notificationToneName=@"None";

}
-(void)tone1ButtonTap
{
    self.noneButton.selected=NO;
    self.tone1Button.selected=YES;
    self.tone2Button.selected=NO;
    self.tone3Button.selected=NO;
    self.tone4button.selected=NO;
    self.tone5Button.selected=NO;
    
//    NSString * tone2=[NSString stringWithFormat:@"%@",[audioFileList objectAtIndex:1]];
    
   
        
        //        SystemSoundID soundID;
        //        AudioServicesCreateSystemSoundID((__bridge_retained CFURLRef)[audioFileList objectAtIndex:1],&soundID);
        //        AudioServicesPlaySystemSound(soundID);
        
        AudioServicesPlaySystemSound(1000);
        
        //NSLog(@"File url: %@", [[audioFileList objectAtIndex:1] description]);
    delegate1.notificationToneName=@"Bloom";
        

}
-(void)tone2ButtonTap
{
    self.noneButton.selected=NO;
    self.tone1Button.selected=NO;
    self.tone2Button.selected=YES;
    self.tone3Button.selected=NO;
    self.tone4button.selected=NO;
    self.tone5Button.selected=NO;
     AudioServicesPlaySystemSound(1002);
    delegate1.notificationToneName=@"Classic";
}
-(void)tone3ButtonTap
{
    self.noneButton.selected=NO;
    self.tone1Button.selected=NO;
    self.tone2Button.selected=NO;
    self.tone3Button.selected=YES;
    self.tone4button.selected=NO;
    self.tone5Button.selected=NO;
     AudioServicesPlaySystemSound(1003);
    delegate1.notificationToneName=@"Sencha";
}
-(void)tone4ButtonTap
{
    self.noneButton.selected=NO;
    self.tone1Button.selected=NO;
    self.tone2Button.selected=NO;
    self.tone3Button.selected=NO;
    self.tone4button.selected=YES;
    self.tone5Button.selected=NO;
     AudioServicesPlaySystemSound(1007);
    delegate1.notificationToneName=@"Test";
}
-(void)tone5ButtonTap
{
    self.noneButton.selected=NO;
    self.tone1Button.selected=NO;
    self.tone2Button.selected=NO;
    self.tone3Button.selected=NO;
    self.tone4button.selected=NO;
    self.tone5Button.selected=YES;
     AudioServicesPlaySystemSound(1003);
    delegate1.notificationToneName=@"Test";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onCancelButtonTap:(id)sender {
    
     [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}
- (IBAction)okButtonTap:(id)sender {
    
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];

}
@end
