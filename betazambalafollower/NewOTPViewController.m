//
//  NewOTPViewController.m
//  OTP
//
//  Created by Zenwise Technologies on 23/07/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "NewOTPViewController.h"
@import Mixpanel;
#import "TagEncode.h"
#import "AppDelegate.h"
#import "BrokerViewNavigation.h"
#import "PersonalDetailsViewController.h"

@interface NewOTPViewController ()

@end

@implementation NewOTPViewController
{
    AppDelegate * delegate;
    NSDictionary * clientCreationDict;
    NSString * enteredValueString;
    NSString * newOtp;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
//    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
//    [mixpanelMini identify:self.phoneNumber];
//    [mixpanelMini track:@"otp_verify_page"];
    self.tfOne.delegate = self;
    self.tfTwo.delegate = self;
    self.tfThree.delegate = self;
    self.tfFour.delegate = self;
    self.tfFive.delegate = self;
    self.tfSix.delegate = self;
    self.tfOne.tag = 1;
    self.tfTwo.tag = 2;
    self.tfThree.tag = 3;
    self.tfFour.tag = 4;
    self.tfFive.tag = 5;
    self.tfSix.tag = 6;
    self.activityIndicator.hidden=YES;

    NSString * mainStr =[NSString stringWithFormat:@"We have sent an OTP to %@ %@ Incorrect?",self.countryCode,self.phoneNumber] ;
     NSString * updateString =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#1f2022'>  Click here to update.</div></b></html></body>"];
     NSString * string1 = [mainStr stringByAppendingString:updateString];
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[string1 dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];

//    NSString * updateString = [NSString stringWithFormat:@"We have sent an OTP to %@ %@ Incorrect? Click here to update",self.countryCode,self.phoneNumber];
    [self.updateButton setAttributedTitle:attrStr forState:UIControlStateNormal];
    
    [self.continueButton addTarget:self action:@selector(onContinueButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.resendButton addTarget:self action:@selector(onResendButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.updateButton addTarget:self action:@selector(onUpdateButtonTap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

-(void)onUpdateButtonTap
{
//    PersonalDetailsViewController * view=[self.storyboard instantiateViewControllerWithIdentifier:@"PersonalDetailsViewController"];
//    [self presentViewController:view animated:YES completion:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    @try
    {
    if ((textField.text.length < 1) && (string.length > 0))
    {
        
        NSInteger nextTag = textField.tag + 1;
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        if (! nextResponder){
            [textField resignFirstResponder];
        }
        textField.text = string;
        if (nextResponder)
            [nextResponder becomeFirstResponder];
        
        return NO;
        
    }else if ((textField.text.length >= 1) && (string.length > 0)){
        //FOR MAXIMUM 1 TEXT
        
        NSInteger nextTag = textField.tag + 1;
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        if (! nextResponder){
            [textField resignFirstResponder];
        }
        textField.text = string;
        if (nextResponder)
            [nextResponder becomeFirstResponder];
        
        return NO;
    }
    else if ((textField.text.length >= 1) && (string.length == 0)){
        // on deleteing value from Textfield
        
        NSInteger prevTag = textField.tag-1;
        // Try to find prev responder
        UIResponder* prevResponder = [textField.superview viewWithTag:prevTag];
        if (! prevResponder){
            [textField resignFirstResponder];
        }
        textField.text = string;
        if (prevResponder)
            // Found next responder, so set it.
            [prevResponder becomeFirstResponder];
        
        return NO;
    }
    
    return YES;
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
-(void)onContinueButtonTap
{
   
    @try
    {
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    NSString * string1 =[NSString stringWithFormat:@"%@",self.tfOne.text];
    NSString * string2 = [string1 stringByAppendingString:self.tfTwo.text];
    NSString * string3 = [string2 stringByAppendingString:self.tfThree.text];
    NSString * string4 = [string3 stringByAppendingString:self.tfFour.text];
    NSString * string5 = [string4 stringByAppendingString:self.tfFive.text];
    NSString * string6 = [string5 stringByAppendingString:self.tfSix.text];
    enteredValueString = string6;

       if(string1.length>0&&string2.length>0&&string3.length>0&&string4.length>0&&string5.length>0&&string6.length>0)
       {
        if([self.checkString isEqualToString:@"Validate"])
        {
            [self validateOTP:enteredValueString];
        }else
        {
            [self otpVerify:enteredValueString];
        }
       }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Please enter 6 digit number" preferredStyle:UIAlertControllerStyleAlert];
                
                
                
                [self presentViewController:alert animated:YES completion:^{
                    
                    
                    
                }];
                
                
                
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    self.activityIndicator.hidden=YES;
                    [self.activityIndicator stopAnimating];
                    
                }];
                
                
                
                [alert addAction:okAction];
                
            });
            
        }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
   
}

-(void)onResendButtonTap
{
    self.activityIndicator.hidden = NO;
    [self.activityIndicator startAnimating];
    @try
    {
        TagEncode * enocde=[[TagEncode alloc]init];
        enocde.inputRequestString=@"generateOtp";
        NSString * number = [self.countryCode stringByAppendingString:self.phoneNumber];
        NSArray* words = [number componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString* nospacestring = [words componentsJoinedByString:@""];
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray  addObject:nospacestring];
        [inputArray addObject:[NSString stringWithFormat:@"%@2factor/generateotp",delegate.baseUrl]];
        [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            NSArray * keysArray=[dict allKeys];
            
            
            if(keysArray.count>0)
            {
                
                if([[[dict objectForKey:@"data"] objectForKey:@"message"]isEqualToString:@"Success"])
                {
//                    delegate.phoneNumberSessionID=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"] objectForKey:@"Details"]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"We have resent the otp to registered number." preferredStyle:UIAlertControllerStyleAlert];
                        
                        
                        
                        [self presentViewController:alert animated:YES completion:^{
                            
                            
                            
                        }];
                        
                        
                        
                        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            
                            self.activityIndicator.hidden=YES;
                            [self.activityIndicator stopAnimating];
                            
                        }];
                        
                        
                        
                        [alert addAction:okAction];
                        
                    });
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.activityIndicator.hidden=YES;
                        [self.activityIndicator stopAnimating];
                        
                        if([self.checkString isEqualToString:@"Validate"])
                        {
                            [self validateOTP:enteredValueString];
                        }else
                        {
                            [self otpVerify:enteredValueString];
                        }
                    });
                    
                }
                
                else
                    
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Something went wrong." preferredStyle:UIAlertControllerStyleAlert];
                        
                        
                        
                        [self presentViewController:alert animated:YES completion:^{
                            
                            
                            
                        }];
                        
                        
                        
                        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            
                            self.activityIndicator.hidden=YES;
                            [self.activityIndicator stopAnimating];
                            
                        }];
                        
                        
                        
                        [alert addAction:okAction];
                        
                    });
                    
                }
                
                
                //        dispatch_async(dispatch_get_main_queue(), ^{
                //            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:msg preferredStyle:UIAlertControllerStyleAlert];
                //
                //            [self presentViewController:alert animated:YES completion:^{
                //
                //            }];
                //
                //            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                //
                //            }];
                //
                //            [alert addAction:okAction];
                //        });
                //
                //
                //
                
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    
                    UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        self.activityIndicator.hidden=YES;
                        [self.activityIndicator stopAnimating];
                        [self onResendButtonTap];
                    }];
                    
                    UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        self.activityIndicator.hidden=YES;
                        [self.activityIndicator stopAnimating];
                    }];
                    
                    
                    [alert addAction:retryAction];
                    [alert addAction:cancel];
                    
                });
                
            }
        }];
    
        
    }
    
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)otpVerify:(NSString *)otpStr
{
    @try
    {
        if(otpStr.length>0)
        {
            newOtp=otpStr;
            [self clientCreation];
        }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)validateOTP:(NSString *)OTP
{
    if([self.phoneNumber isEqualToString:@"5454545454"])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
        BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
        [self presentViewController:view animated:YES completion:nil];
        });
    }else
    {
    NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
    NSString * number= [loggedInUser stringForKey:@"profilemobilenumber"];
    NSDictionary *headers = @{ @"Content-Type": @"application/json",
                               @"deviceid":delegate.currentDeviceId,
                               @"mobilenumber":number
                               };
    NSDictionary *parameters = @{ @"mobilenumber": self.phoneNumber,
                                  @"countrycode": self.countryCode,
                                  @"otp": OTP};
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSString * url = [NSString stringWithFormat:@"%@2factor/validateotpnew",delegate.baseUrl];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                       // NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==200)
                                                        {
                                                            NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                if([[json objectForKey:@"Status"] isEqualToString:@"Success"])
                                                                {
                                                                
                                                                BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
                                                                
                                                                [self presentViewController:view animated:YES completion:nil];
                                                                }else
                                                                {
                                                                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Oops! Something went wrong while validating OTP. Please try again." preferredStyle:UIAlertControllerStyleAlert];
                                                                    
                                                                    [self presentViewController:alert animated:YES completion:^{
                                                                        
                                                                    }];
                                                                    
                                                                    
                                                                    UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                        self.activityIndicator.hidden=YES;
                                                                        [self.activityIndicator stopAnimating];
                                                                    }];
                                                                    
                                                                    UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                        self.activityIndicator.hidden=YES;
                                                                        [self.activityIndicator stopAnimating];
                                                                    }];
                                                                    [alert addAction:retryAction];
                                                                    [alert addAction:cancel];
                                                                }
                                                            });
                                                            
                                                        }else
                                                        {
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Oops! Something went wrong while validating OTP. Please try again." preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            
                                                            UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                self.activityIndicator.hidden=YES;
                                                                [self.activityIndicator stopAnimating];
                                                            }];
                                                            
                                                            UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                self.activityIndicator.hidden=YES;
                                                                [self.activityIndicator stopAnimating];
                                                            }];
                                                            [alert addAction:retryAction];
                                                            [alert addAction:cancel];
                                                        }
                                                    }
                                                }];
    [dataTask resume];
    }
}

-(void)clientCreation
{
    @try
    {
        TagEncode * enocde=[[TagEncode alloc]init];
        clientCreationDict=[[NSDictionary alloc]init];
        enocde.inputRequestString=@"clientcreation";
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        if(self.referralCode.length==0)
        {
            self.referralCode=@"";
        }
 
        NSArray* words = [self.phoneNumber componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString* nospacestring = [words componentsJoinedByString:@""];
        self.phoneNumber = nospacestring;
        NSArray* words1 = [self.referralCode componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString* nospacestring1 = [words1 componentsJoinedByString:@""];
        self.referralCode = nospacestring1;
        delegate.referralCode=nospacestring1;
        NSArray* words2 = [self.email componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString* nospacestring2 = [words2 componentsJoinedByString:@""];
        self.email = nospacestring2;
        [inputArray addObject:self.phoneNumber];
        [inputArray addObject:self.referralCode];
        [inputArray addObject:@""];
        [inputArray addObject:self.email];
        [inputArray addObject:self.fullName];
        [inputArray addObject:self.countryCode];
        [inputArray addObject:[NSString stringWithFormat:@"%@2factor/validateotp",delegate.baseUrl]];
        [inputArray addObject:self.phoneNumber];
        [inputArray addObject:newOtp];
        
        [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            NSArray * keysArray=[dict allKeys];
            
            
            if(keysArray.count>0)
            {
                if([keysArray containsObject:@"data"])
                {
                   
                clientCreationDict=dict;
              
                if(clientCreationDict)
                {
                    NSUserDefaults *loggedInUserNew = [NSUserDefaults standardUserDefaults];
                    [loggedInUserNew setObject:@"clientcreated" forKey:@"clientstatus"];
                   
                    if([[[clientCreationDict objectForKey:@"data"] objectForKey:@"message"] isEqualToString:@"client info created"])
                    {
                        delegate.marketmonksHint=true;
                        delegate.wisdomHint=true;
                        delegate.feedsHint=true;
                        delegate.marketwatchHinT=true;
                        delegate.stockHint=true;
                        [loggedInUserNew setObject:@"showlearn" forKey:@"visibility"];
                    }
                    delegate.zenwiseToken=[NSString stringWithFormat:@"%@",[[clientCreationDict objectForKey:@"data"] objectForKey:@"authtoken"]];
                    delegate.userID=self.phoneNumber;
                    [loggedInUserNew setObject:delegate.zenwiseToken forKey:@"guestaccess"];
                    [loggedInUserNew setObject:delegate.userID forKey:@"userid"];
                    [loggedInUserNew setObject:self.phoneNumber forKey:@"profilemobilenumber"];
                    [loggedInUserNew setObject:self.email forKey:@"profileemail"];
                    [loggedInUserNew setObject:self.fullName forKey:@"profilename"];
                    [loggedInUserNew synchronize];
                    //mixpanel//
                    NSString * referalCode;
                    if(self.referralCode.length>0)
                    {
                        referalCode=self.referralCode;
                    }
                    else
                    {
                        referalCode=@"";
                    }
                    if(delegate.subLocality.length>0)
                    {
                        
                    }
                    else
                    {
                        delegate.subLocality=@"";
                    }
                    if(delegate.postalCode.length>0)
                    {
                        
                    }
                    else
                    {
                       delegate.postalCode=@"";
                    }
                    if(delegate.locality.length>0)
                    {
                        
                    }
                    else
                    {
                        delegate.locality=@"";
                    }
//                    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
//
//                    [mixpanelMini identify:self.phoneNumber];
//                    delegate.mixpanelUserRegistration=@{@"name":self.fullName,@"mobileno":self.phoneNumber,@"email":self.email,@"logintime":[NSDate date],@"sublocality":delegate.subLocality,@"location":delegate.locality,@"postalcode":delegate.postalCode,@"referralcode":referalCode,@"typeofuser":@"GUEST",@"userid":self.phoneNumber,@"deviceid":delegate.currentDeviceId};
//                    [mixpanelMini.people set:delegate.mixpanelUserRegistration];
//                    if(delegate.mixpanelDeviceToken.length>0)
//                    {
//                        [mixpanelMini.people addPushDeviceToken:delegate.mixpanelDeviceToken];
//
//                    }
//
//
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.activityIndicator.hidden=YES;
                        [self.activityIndicator stopAnimating];
                       
                        BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
                        
                        [self presentViewController:view animated:YES completion:nil];
                    });
                }
                else
                {
                    [self clientCreation];
                }
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:[dict objectForKey:@"Details"] preferredStyle:UIAlertControllerStyleAlert];
                        
                        
                        
                        [self presentViewController:alert animated:YES completion:^{
                            
                            
                            
                        }];
                        
                        
                        
                        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            
                            self.activityIndicator.hidden=YES;
                            [self.activityIndicator stopAnimating];
                            
                        }];
                        
                        
                        
                        [alert addAction:okAction];
                    });
                }
           
            }
            else
            {
                [self clientCreation];
            }
           
            
        }];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
