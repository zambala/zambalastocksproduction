//
//  FollowCell.m
//  testing
//
//  Created by zenwise technologies on 20/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "FollowCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation FollowCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
      self.profileimage.layer.cornerRadius=self.profileimage.frame.size.width / 2;
      self.profileimage.clipsToBounds = YES;
    self.profileimage.layer.borderWidth = 0.5f;
    self.profileimage.layer.borderColor = [[UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0] CGColor];
//    self.winnerLbl.layer.cornerRadius=3.2;
//    self.winnerLbl.layer.masksToBounds = YES;
//    self.winnerLbl.layer.borderWidth=0.5;
//    self.winnerLbl.layer.borderColor=[[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:0.2]CGColor];
//    self.winnerLbl.layer.backgroundColor=[[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:0.2]CGColor];
//    
//    self.looserLbl.layer.cornerRadius=3.2;
//    self.looserLbl.layer.masksToBounds = YES;
//    self.looserLbl.layer.borderWidth=0.5;
//    self.looserLbl.layer.borderColor=[[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:0.2]CGColor];
//    self.looserLbl.layer.backgroundColor=[[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:0.2]CGColor];
    
//    self.followBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
//    self.followBtn.layer.shadowOffset = CGSizeMake(0, 5.0f);
//    self.followBtn.layer.shadowOpacity = 5.0f;
//    self.followBtn.layer.shadowRadius = 4.0f;
//    self.followBtn.layer.cornerRadius=7.8f;
//    self.followBtn.layer.masksToBounds = NO;
    
//    self.bgView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
//    self.bgView.layer.shadowOffset = CGSizeMake(0, 5.0f);
//    self.bgView.layer.shadowOpacity = 5.0f;
//    self.bgView.layer.shadowRadius = 4.0f;
//    self.bgView.layer.cornerRadius=10.0f;
//    self.bgView.layer.masksToBounds = NO;
    self.followBtn.layer.cornerRadius=10.0f;
    self.followBtn.layer.masksToBounds = YES;
//    self.publicLbl.layer.cornerRadius=10.0f;
//    self.publicLbl.layer.masksToBounds = YES;
    self.publicLbl.clipsToBounds = YES;

    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect: self.publicLbl.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight ) cornerRadii:CGSizeMake(10.0, 10.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.publicLbl.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.publicLbl.layer.mask = maskLayer;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
