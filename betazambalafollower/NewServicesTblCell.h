//
//  NewServicesTblCell.h
//  PremiumServices
//
//  Created by zenwise mac 2 on 7/19/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewServicesTblCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *serviceName;

@property (weak, nonatomic) IBOutlet UIButton *subscribeBtn;
@property (weak, nonatomic) IBOutlet UILabel *subscriberCount;
@property (weak, nonatomic) IBOutlet UILabel *premiumServiceDetailsLbl;

@property (weak, nonatomic) IBOutlet UILabel *brandLbl;

@property (weak, nonatomic) IBOutlet UIView *detailsView;

@property (weak, nonatomic) IBOutlet UIButton *infoBtn;

@end
