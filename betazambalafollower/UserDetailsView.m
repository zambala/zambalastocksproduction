//
//  UserDetailsView.m
//  PremiumServices
//
//  Created by zenwise mac 2 on 7/21/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "UserDetailsView.h"
#import "PaymentSelectionView.h"
#import "AppDelegate.h"
#import "ViewController.h"

@interface UserDetailsView ()
{
    AppDelegate * delegate1;
}

@end

@implementation UserDetailsView

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.emailTxt.delegate=self;
    self.mobileTxt.delegate=self;
    self.navigationItem.title = @"View your details";
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    if([self.typeOfPayment isEqualToString:@"payin"])
    {
        self.topView.hidden=NO;
        self.topViewHeightConstraint.constant=88;
    }else
    {
        self.topView.hidden=YES;
        self.topViewHeightConstraint.constant=0;
    }
    
    [self.backButton addTarget:self action:@selector(onBackButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    
    [self.view addGestureRecognizer:tap];
    self.userDetailsView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.userDetailsView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.userDetailsView.layer.shadowOpacity = 1.0f;
    self.userDetailsView.layer.shadowRadius = 1.0f;
    self.userDetailsView.layer.cornerRadius=2.1f;
    self.userDetailsView.layer.masksToBounds = NO;
    
    self.continueOutlet.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.continueOutlet.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.continueOutlet.layer.shadowOpacity = 1.0f;
    self.continueOutlet.layer.shadowRadius = 1.0f;
    self.continueOutlet.layer.cornerRadius=2.1f;
    self.continueOutlet.layer.masksToBounds = NO;
    
//    self..layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
//    self.userDetailsView.layer.shadowOffset = CGSizeMake(0, 2.0f);
//    self.userDetailsView.layer.shadowOpacity = 1.0f;
//    self.userDetailsView.layer.shadowRadius = 1.0f;
//    self.userDetailsView.layer.cornerRadius=1.0f;
//    self.userDetailsView.layer.masksToBounds = NO;
//
    
    self.nameTxt.delegate=self;
    self.emailTxt.delegate=self;
    
    
    if(delegate1.userName.length>0)
    {
        self.nameTxt.text=delegate1.userName;
        
    }
    
    
    if(delegate1.emailId.length>0)
    {
        self.emailTxt.text=delegate1.emailId;
    }
   
    
    // Do any additional setup after loading the view.
}

-(void)onBackButtonTap
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)continueAction:(id)sender {
    @try
    {
    if(self.emailTxt.text.length>0&&self.mobileTxt.text.length>0&&self.nameTxt.text.length>0)
    {
        
        delegate1.emailId=self.emailTxt.text;
        delegate1.mobileNumber=self.mobileTxt.text;
        delegate1.userName1=self.nameTxt.text;
        
        if([self.typeOfPayment isEqualToString:@"payin"])
        {
            if([delegate1.brokerNameStr isEqualToString:@"Upstox"])
            {
                ViewController * view=[self.storyboard instantiateViewControllerWithIdentifier:@"view1"];
                view.segment= self.segment;
                view.txnAmount= self.txnAmount;
                view.payInCheck=@"payin";
                view.upiAlias = self.upiAlias;
                
                [self presentViewController:view animated:YES completion:nil];
            }
            else
            {
                ViewController * view=[self.storyboard instantiateViewControllerWithIdentifier:@"view1"];
                view.payInCheck=@"payin";
            view.accountIFSC = self.accountIFSC;
            view.accountNo = self.accountNo;
            view.segment = self.segment;
            view.txnAmount = self.txnAmount;
            view.upiAlias = self.upiAlias;
            view.payInCheck=self.typeOfPayment;
            view.bankAccount = self.bankName;
//            [self.navigationController pushViewController:view animated:YES];
            [self presentViewController:view animated:YES completion:nil];
            }
        }else
        {
        PaymentSelectionView * view=[self.storyboard instantiateViewControllerWithIdentifier:@"PaymentSelectionView"];
        
        [self.navigationController pushViewController:view animated:YES];
        }
        
    }
    else
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Please fill the empty fields" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            // Ok action example
        }];
              [alert addAction:okAction];
       
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

   
}



-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

-(void)dismissKeyboard
{
    [self.emailTxt resignFirstResponder];
     [self.mobileTxt resignFirstResponder];
    [self.nameTxt resignFirstResponder];
   
    //    [self.backView setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    //
    
}
@end
