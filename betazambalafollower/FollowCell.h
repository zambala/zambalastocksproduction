//
//  FollowCell.h
//  testing
//
//  Created by zenwise technologies on 20/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface FollowCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *profileimage;

@property (weak, nonatomic) IBOutlet UILabel *organizationName;

@property (weak, nonatomic) IBOutlet UILabel *profileName;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (strong, nonatomic) IBOutlet UIView *starView;
@property (strong, nonatomic) IBOutlet UILabel *followeCountLabel;
@property (strong, nonatomic) IBOutlet UIButton *followBtn;
@property (strong, nonatomic) IBOutlet HCSStarRatingView *starRatingView;
@property (strong, nonatomic) IBOutlet UILabel *aboutLbl;
@property (strong, nonatomic) IBOutlet UILabel *leaderActiveSts;
@property (strong, nonatomic) IBOutlet UILabel *gainPer;
@property (weak, nonatomic) IBOutlet UILabel *looserLbl;

@property (weak, nonatomic) IBOutlet UILabel *successRateLbl;
@property (weak, nonatomic) IBOutlet UILabel *publicLbl;
@property (weak, nonatomic) IBOutlet UIImageView *publicImgView;


@property (weak, nonatomic) IBOutlet UILabel *orgNameLbl;
@property (weak, nonatomic) IBOutlet UIImageView *bgImgView;

@property (weak, nonatomic) IBOutlet UILabel *winnerLbl;
@end
