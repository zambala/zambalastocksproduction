//
//  WalletHistoryCell.h
//  NewWallet
//
//  Created by Zenwise Technologies on 28/06/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WalletHistoryCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLbl;
@property (weak, nonatomic) IBOutlet UILabel *txnNoLbl;
@property (weak, nonatomic) IBOutlet UILabel *messageLbl;

@end
