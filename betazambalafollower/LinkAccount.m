//
//  LinkAccount.m
//  partofZambalaFollower
//
//  Created by zenwise mac 2 on 3/17/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "LinkAccount.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>

@interface LinkAccount ()

@end

@implementation LinkAccount
{
    NSArray *pickerData;
    NSString * check;
    AppDelegate *appDelegate;
    BOOL buttonCheck,buttonCheck1;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    buttonCheck = false;
    buttonCheck1 = false;
    
    
    self.bankButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.bankButton.contentEdgeInsets=UIEdgeInsetsMake(0, 10, 0, 0);
    
    self.bankBranchButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.bankBranchButton.contentEdgeInsets=UIEdgeInsetsMake(0, 0, 0, 0);
    
    self.pickerView.hidden=YES;
    
    self.submitButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    self.submitButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.submitButton.layer.shadowOpacity = 1.0f;
    self.submitButton.layer.shadowRadius = 1.0f;
    self.submitButton.layer.cornerRadius=2.1f;
    self.submitButton.layer.masksToBounds = NO;
    [self.submitButton addTarget:self action:@selector(onSubmitButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    [[self.skipButton layer]setCornerRadius:2.1f];
    [[self.skipButton layer]setMasksToBounds:YES];
    [[self.skipButton layer] setBorderWidth:1.0f];
    [[self.skipButton layer] setBorderColor:[[UIColor colorWithRed:0/255 green:42/255 blue:58/255 alpha:1]CGColor ]];
    
    
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return pickerData.count;
}
- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return pickerData[row];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if([check isEqualToString:@"bank"])
    {
        [self.bankButton setTitle:[pickerData objectAtIndex:row] forState:UIControlStateNormal];
        self.pickerView.hidden=YES;
        [self.backgroundView setBackgroundColor:[UIColor whiteColor]];
        self.backgroundView.alpha=1;

    }
    else if ([check isEqualToString:@"branch"])
    {
    [self.bankBranchButton setTitle:[pickerData objectAtIndex:row] forState:UIControlStateNormal];
        self.pickerView.hidden=YES;
        [self.backgroundView setBackgroundColor:[UIColor whiteColor]];
        self.backgroundView.alpha=1;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onChooseBankTap:(id)sender {
    
    self.pickerView.hidden=NO;
    pickerData =  @[@"HDFC",@"SBI",@"ICICI",@"Bank of Baroda",@"AxisBank"];
    [self.backgroundView setBackgroundColor:[UIColor blackColor]];
    self.backgroundView.alpha=0.5;
    self.pickerView.delegate=self;
    self.pickerView.dataSource=self;
    check=@"bank";
    buttonCheck = true;
    
    
   
    
}
-(void)onSubmitButtonTap
{
    
    [self validation];
    
}
-(void)validation
{
    if(buttonCheck==true&&buttonCheck==true&&self.ifscCodeTF.text.length>0&&self.micsCodeTF.text.length>0&&self.acoountNumberTF.text.length>0&&self.reEnterAccountNumberTF.text.length>0)
    {
        appDelegate.ifscCodeString = [[self.ifscCodeTF text]mutableCopy];
        appDelegate.mcirString= [[self.micsCodeTF text]mutableCopy];
        appDelegate.bankNameString = [self.bankButton.titleLabel.text mutableCopy];
        appDelegate.bankBranchString = [self.bankBranchButton.titleLabel.text mutableCopy];
        appDelegate.accountNumberString = [self.acoountNumberTF.text mutableCopy];
        
        //NSLog(@"%@,%@,%@,%@,%@",appDelegate.ifscCodeString,appDelegate.mcirString,appDelegate.bankNameString,appDelegate.bankBranchString,appDelegate.accountNumberString);
    }else
    {
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Missing Details" message:@"Please check the details" preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alert animated:YES completion:nil];
        UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alert addAction:okAction];
        
    }
}

- (IBAction)onChooseBranchTap:(id)sender {
    self.pickerView.hidden=NO;
    pickerData =  @[@"Hyderabad",@"Mumbai",@"Chennai",@"Banglore",@"Ahamadabad"];
    [self.backgroundView setBackgroundColor:[UIColor blackColor]];
    self.backgroundView.alpha=0.5;
    self.pickerView.delegate=self;
    self.pickerView.dataSource=self;
    check=@"branch";
    buttonCheck1 = true;
    

    
}
@end
