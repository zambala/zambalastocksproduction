//
//  MainView.h
//  ZambalaUSA
//
//  Created by guna on 17/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewUSA : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *marketMonksButton;
@property (weak, nonatomic) IBOutlet UIButton *wisdomGardenButton;
@property (weak, nonatomic) IBOutlet UIButton *marketWatchButton;
@property (weak, nonatomic) IBOutlet UIButton *premiumServiceButton;
@property (weak, nonatomic) IBOutlet UIButton *openAccountButton;
@property (weak, nonatomic) IBOutlet UIButton *indianButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *USMarketBarButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *zambalaBarButton;
@property (weak, nonatomic) IBOutlet UILabel *nasdaqLTPLabel;
@property (weak, nonatomic) IBOutlet UILabel *nasdaqChangePercentLabel;
@property NSMutableDictionary * nasdaqResponseDictionary;
@property (weak, nonatomic) IBOutlet UILabel *nasdaqCompositeLTPLabel;
@property (weak, nonatomic) IBOutlet UILabel *nasdaqCompositeChangePercentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *tickerImageView;
@property (weak, nonatomic) IBOutlet UIImageView *marketMonksImageView;
@property (weak, nonatomic) IBOutlet UIImageView *marketWatchImageView;
@property (weak, nonatomic) IBOutlet UIImageView *equityImageView;
@property (weak, nonatomic) IBOutlet UIImageView *derivativesImageView;
@property (weak, nonatomic) IBOutlet UIImageView *wealthCreatorImageView;
@property (weak, nonatomic) IBOutlet UIImageView *ordersImageView;
@property (weak, nonatomic) IBOutlet UIImageView *premiumServicesImageView;
@property (weak, nonatomic) IBOutlet UIImageView *indianMarketsImageView;

@end
