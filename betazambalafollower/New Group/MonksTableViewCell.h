//
//  MonksTableViewCell.h
//  ZambalaUSA
//
//  Created by guna on 17/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RateView.h"
#import "HCSStarRatingView.h"

@interface MonksTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *analystNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *sectorLabel;
@property (strong, nonatomic) IBOutlet UIImageView *profilePictureImageView;
@property (strong, nonatomic) IBOutlet UIImageView *star1;
@property (strong, nonatomic) IBOutlet UIImageView *star2;
@property (strong, nonatomic) IBOutlet UIImageView *star3;
@property (strong, nonatomic) IBOutlet UIImageView *star4;
@property (strong, nonatomic) IBOutlet UIImageView *star5;
@property RateView *rateView;
@property (weak, nonatomic) IBOutlet UIButton *detailsButton;

@property (strong, nonatomic) IBOutlet HCSStarRatingView *starRatingView;


@end
