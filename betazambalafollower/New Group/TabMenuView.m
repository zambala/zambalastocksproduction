//
//  TabMenuView.m
//  ZambalaUSA
//
//  Created by guna on 20/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "TabMenuView.h"
#import "PortFolioViewController.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "MainOrdersUSA.h"
#import "NewSelectLoginViewController.h"
#import "TradesViewController.h"
#import "HelpshiftSupport.h"
#import "HelpshiftCampaigns.h"
#import "BrokerViewNavigation.h"
#import "KnowledgeHomeViewController.h"
#import "TabBar.h"

@interface TabMenuView ()

@end

@implementation TabMenuView
{
    AppDelegate * delegate;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.dummyTable = [[UITableView alloc]init];
//    self.dummyTable.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
//    [self.view addSubview:self.dummyTable];
    self.tabBar1.delegate=self;
    self.delegate=self;
    //[[UITabBar appearance] setTintColor:[UIColor redColor]];
    
    
   
   // float X_Co = (self.view.frame.size.width - 70)/2;
   // self.addButton = [[VCFloatingActionButton alloc]initWithFrame:CGRectMake(X_Co,(self.view.frame.size.height-60), 70, 70) normalImage:[UIImage imageNamed:@"icon10Plus"] andPressedImage:[UIImage imageNamed:@"icon10PlusRotated"] withScrollview:_dummyTable];
    

    float X_Co = (self.view.frame.size.width - 85)/2;
    float Y_Co;
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    if(screenHeight>736)
    {
        
        Y_Co = self.view.frame.size.height - 90;
    }
    
    else
    {
        Y_Co = self.view.frame.size.height - 53;
        
    }
    
    
    
    self.addButton = [[VCFloatingActionButton alloc]initWithFrame:CGRectMake(X_Co,Y_Co, 85, 50) normalImage:[UIImage imageNamed:@"more1"] andPressedImage:[UIImage imageNamed:@"icon10PlusRotated"] withScrollview:_dummyTable];
    
    
    //        NSDictionary *optionsDictionary = @{@"plusMenu10.png":@"plusMenu10",@"plusMenu10.png":@"plusMenu9",@"plusMenu9.png":@"plusMenu10",@"plusMenu10.png":@"plusMenu10",@"plusMenu10.png":@"plusMenu10",@"plusMenu10.png":@"plusMenu10",@"plusMenu10.png":@"plusMenu10",@"linkedin-icon":@"Linked in",@"linkedin-icon":@"Linked in",@"linkedin-icon":@"Linked in",@"linkedin-icon":@"Linked in"};
    //        self.addButton.menuItemSet = optionsDictionary;
    
    //    delegate1.imageArray=[[NSMutableArray alloc]initWithObjects:@"plusMenu10.png",@"plusMenu9.png",@"plusMenu8.png",@"plusMenu7.png",@"plusMenu6.png",@"plusMenu5.png",@"plusMenu4.png",@"plusMenu3@2x.png",@"plusMenu2.png",@"plusMenu1.png", nil];
    
   // self.addButton.imageArray = @[@"plusMenu10",@"plusMenu9",@"plusMenu8",@"plusMenu7",@"plusMenu5",@"plusMenu4",@"plusMenu3",@"plusMenu2"];
    self.addButton.imageArray = @[@"plusMenu10",@"plusMenu9",@"plusMenu8"];

    
    if(delegate.sessionID.length>0)
    {
   // self.addButton.labelArray = @[@"Log Out",@"Contact Support",@"Trades",@"Orders",@"Fund Transfer",@"Wallet",@"Messages",@"Profile Settings"];
        self.addButton.labelArray = @[@"Log Out",@"Contact Support",@"Trades"];
    }else
    {
         //self.addButton.labelArray = @[@"Log In",@"Contact Support",@"Trades",@"Orders",@"Fund Transfer",@"Wallet",@"Messages",@"Profile Settings"];
        self.addButton.labelArray = @[@"Log Out",@"Contact Support",@"Trades"];
    }
    self.tabBarController.tabBar.barTintColor=[UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:1];
    self.tabBarController.tabBar.tintColor=[UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:1];
    [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed:(43/255.0) green:(49/255.0) blue:(51/255.0) alpha:1]];
   // self.tabBarController.tabBar.backgroundImage= [UIImage imageNamed:@"bottomBg.png"];
    self.addButton.hideWhileScrolling = YES;
    self.addButton.delegate = self;
    _dummyTable.delegate = self;
    _dummyTable.dataSource = self;
    [self.view addSubview:self.addButton];
    
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    if(tabBarController.selectedIndex==1)
    {
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        TabBar * tabPage=[storyboard instantiateViewControllerWithIdentifier:@"tab"];
//        [self presentViewController:tabPage animated:YES completion:nil];
    }
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    //NSLog(@"Selected");
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    cell.textLabel.text = [NSString stringWithFormat:@"%tu",indexPath.row];
    
    
    
    cell.imageView.image =[UIImage imageNamed:[NSString stringWithFormat:@"%tu",indexPath.row]];

    return cell;
}


-(void)logOutMethod
{
   
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:@"Are you sure you want to logout" preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:^{
                
            }];
            
            
            UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                delegate.marketWatchBool=true;
                delegate.usCheck=@"";
                delegate.usNavigationCheck=@"";
                delegate.brokerLoginCheck=false;
                //        NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
                //
                //        loggedInUser=nil;
                //
                //        NSUserDefaults * loggedInUser = [NSUserDefaults standardUserDefaults];
                //        NSDictionary * dict = [loggedInUser dictionaryRepresentation];
                //        for (id key in dict) {
                //            [loggedInUser removeObjectForKey:key];
                ////        }
                //        [loggedInUser synchronize];
                
                if([delegate.loginActivityStr isEqualToString:@"OTP1"])
                {
                    NSUserDefaults *loggedInUserNew  = [NSUserDefaults standardUserDefaults];
                    [loggedInUserNew setObject:@"exit" forKey:@"guestflow"];
                    [loggedInUserNew synchronize];
                    //
                    //                delegate1.loginActivityStr=@"";
                    //                //             delegate1.loginActivityStr=@"";
                    //                [loggedInUserNew removeObjectForKey:@"loginActivityStr"];
                    //             [prefs removeObjectForKey:@"ID"];
                    //             [prefs removeObjectForKey:@"userid"];
                    //             [prefs removeObjectForKey:@"brokername"];
                    //
                    
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    BrokerViewNavigation * view=[storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
                    
                    [self presentViewController:view animated:YES completion:nil];
                    
                    
                }
                
            }];
            UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            
            [alert addAction:retryAction];
            [alert addAction:cancel];
            
        });
}
                      
                       
                                        


-(void) didSelectMenuOptionAtIndex:(NSInteger)row
{
    //NSLog(@"Floating action tapped index %tu",row);
    if(row==0)
    {
        if(delegate.sessionID.length>0)
        {
        [self logOutServer];
        }else
        {
           
//            LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
//            login.reLoginCheck=@"relogin";
//            [self presentViewController:login animated:YES completion:nil];
            
            [self logOutMethod];
        }
    }
    
    if(row==2)
    {
        TradesViewController * trades = [self.storyboard instantiateViewControllerWithIdentifier:@"TradesViewController"];
        [self presentViewController:trades animated:YES completion:nil];
    }
    
//    if(row==3)
//    {
//        MainOrdersUSA * mainOrders = [self.storyboard instantiateViewControllerWithIdentifier:@"MainOrdersUSA"];
//        [self presentViewController:mainOrders animated:YES completion:nil];
//    }
    if(row==3)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Coming Soon" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
    }
    if(row==1)
    {
        [HelpshiftCore setName:@"" andEmail:@""];
        NSString *   userDetals;
        if(delegate.sessionID.length>0)
        {
            userDetals =[NSString stringWithFormat:@"CHOICE TRADE(%@)",delegate.usaId];
        }else
        {
            userDetals =[NSString stringWithFormat:@"CHOICE TRADE(%@)",delegate.userID];
        }
        
    
        
        
        
        //        NSDictionary *metaDataWithTags = @{@"usertype": @"paid",
        //                                           @"level":@"7",
        //                                           @"score":@"12345",
        //                                           HelpshiftSupportTagsKey:@[@"feedback",@"paid user",@"v4.1"]};
        
        
        
        NSString * broker = [NSString stringWithFormat:@"%@%@",[[@"CHOICE TRADE" substringToIndex:1] lowercaseString],[@"CHOICE TRADE" substringFromIndex:1] ];
        
        //NSLog(@"%@",broker);
        
        
        [HelpshiftCore loginWithIdentifier:userDetals withName:@"" andEmail:@""];
        
        
        //        [HelpshiftSupport setMetadataBlock:^(void){
        //            return [NSDictionary dictionaryWithObjectsAndKeys:delegate1.userID, @"name",
        //                    delegate1.emailId, @"email",
        //                    [NSArray arrayWithObjects:broker,nil], HelpshiftSupportTagsKey, nil];
        //        }];
        //
        //
        //
        //
        //         [HelpshiftSupport showFAQs:self withConfig:nil];
        
      
        
        
        
        NSDictionary *metaDataWithTags = @{@"userid":userDetals,
                                           @"emailid":@"",
                                           HelpshiftSupportTagsKey:@[broker],
                                           @"country:":delegate.country,
                                           @"locality":delegate.locality,
                                           @"name":delegate.name,
                                           @"postalcode":delegate.postalCode,
                                           @"sublocality":delegate.subLocality,
                                           
                                           @"locatedAt":delegate.locatedAt
                                           };
        
        
        
        
        
        
        
        
        
        
        [HelpshiftSupport showFAQs:self
                       withOptions:@{@"gotoConversationAfterContactUs":@"YES",
                                     HelpshiftSupportCustomMetadataKey: metaDataWithTags}];
        
        
    }
    
    
//    if(row==6)
//    {
//        PortFolioViewController * portfolioView = [self.storyboard instantiateViewControllerWithIdentifier:@"PortFolioViewController"];
//        [self presentViewController:portfolioView animated:YES completion:nil];
//    }
}


-(void)logOutServer
{
    @try
    {
        NSString * str = [NSString stringWithFormat:@"api={\"name\":\"JSON_Logout\",\"client_id\":\"T7qqV91jR2\",\"session_id\":\"%@\",\"firm_code\":\"5ntJOPXqQb\",\"userID\":\"%@\"}",delegate.sessionID,delegate.USAuserID];
    
        
        NSData *dt = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]init];
    if([delegate.accountCheck isEqualToString:@"dummy"])
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.dummyAccountURL]
                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                      timeoutInterval:10.0];
    }else if ([delegate.accountCheck isEqualToString:@"real"])
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.realAccountURL]
                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                      timeoutInterval:10.0];
    }
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        [request setHTTPBody:dt];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            NSDictionary* json = [NSJSONSerialization
                                                                                  JSONObjectWithData:data
                                                                                  options:kNilOptions
                                                                                  error:&error];
                                                            //NSLog(@"jjson %@",json);
                                                            
                                                            
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    
                                                                    int success = [[json objectForKey:@"success"] intValue];
                                                                    if(success==1)
                                                                    {
                                                                        
                                                                        UIAlertController * alert = [UIAlertController
                                                                                                     alertControllerWithTitle:@"Success"
                                                                                                     message:@"You have successfully logged out."
                                                                                                     preferredStyle:UIAlertControllerStyleAlert];
                                                                        
                                                                        //Add Buttons
                                                                        
                                                                        UIAlertAction* okButton = [UIAlertAction
                                                                                                   actionWithTitle:@"Ok"
                                                                                                   style:UIAlertActionStyleDefault
                                                                                                   handler:^(UIAlertAction * action) {
                                                                                                       //Handle your yes please button action here
                                                                                                       delegate.sessionID=@"";
                                                                                                       [delegate.marketWatchLocalStoreDict removeAllObjects];
                                                                                                       delegate.usaId=@"";
                                                                                                       delegate.USAuserID=@"";
                                                                                                       if([delegate.usNavigationCheck isEqualToString:@"USA"])
                                                                                                       {
                                                                                                           delegate.usNavigationCheck=@"";
                                                                                                           UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                                                                                           BrokerViewNavigation * view=[storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
                                                                                                           
                                                                                                           [self presentViewController:view animated:YES completion:nil];
                                                                                                       }else
                                                                                                       {
                                                                                                           TabMenuView * tabbar = [self.storyboard instantiateViewControllerWithIdentifier:@"TabMenuView"];
                                                                                                           [self presentViewController:tabbar animated:YES completion:nil];
                                                                                                       }
                                                                                                       
                                                                                                   }]                                                                                                 ;
                                                                        //Add your buttons to alert controller
                                                                        
                                                                        [alert addAction:okButton];
                                                                        
                                                                        
                                                                        [self presentViewController:alert animated:YES completion:nil];
//                                                                    NewSelectLoginViewController * view=[self.storyboard instantiateViewControllerWithIdentifier:@"NewSelectLoginViewController"];
//                                                                        delegate.accountNumber=@"";
//                                                                    [self presentViewController:view animated:YES completion:nil];
                                                                    }
                                                                });
                                                            
                                                        }
                                                    }];
        [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
//{
//    if(tabBarController.selectedIndex==2)
//    {
//        return NO;
//    }
//    
//    return YES;
//}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
