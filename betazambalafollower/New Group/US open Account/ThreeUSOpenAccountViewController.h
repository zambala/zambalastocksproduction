//
//  ThreeUSOpenAccountViewController.h
//  ChoiceTradeAccountOpening
//
//  Created by Zenwise Technologies on 15/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThreeUSOpenAccountViewController : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource,UIImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *checkButton;
@property (weak, nonatomic) IBOutlet UIButton *clickHereButton;
@property (weak, nonatomic) IBOutlet UIButton *incomeSourceButton;
@property (weak, nonatomic) IBOutlet UIButton *annualImcomeSourceButton;
@property (weak, nonatomic) IBOutlet UIButton *netWorthButton;
@property (weak, nonatomic) IBOutlet UIButton *liquidAssetsButton;
@property (weak, nonatomic) IBOutlet UIButton *taxButton;
@property (weak, nonatomic) IBOutlet UIButton *sourceOfWealthButton;
@property (weak, nonatomic) IBOutlet UIView *foreignView;
@property (weak, nonatomic) IBOutlet UIButton *profileButton;
@property (weak, nonatomic) IBOutlet UIButton *dayTradingButton;
@property (weak, nonatomic) IBOutlet UIButton *activeTraderButton;
@property (weak, nonatomic) IBOutlet UIButton *financialButton;
@property (weak, nonatomic) IBOutlet UIButton *objectiveButto;
@property (weak, nonatomic) IBOutlet UIButton *stockExpButton;
@property (weak, nonatomic) IBOutlet UIButton *avgStockSize;
@property (weak, nonatomic) IBOutlet UIButton *transactionsStockButton;
@property (weak, nonatomic) IBOutlet UIButton *optionsExpButton;
@property (weak, nonatomic) IBOutlet UIButton *optionsSizeButton;
@property (weak, nonatomic) IBOutlet UIButton *transactionsOptionsButton;
@property (weak, nonatomic) IBOutlet UIButton *bondsExpButton;
@property (weak, nonatomic) IBOutlet UIButton *bondsSizeButton;
@property (weak, nonatomic) IBOutlet UIButton *bondsTransactionsButton;
@property (weak, nonatomic) IBOutlet UIButton *mfExpButton;
@property (weak, nonatomic) IBOutlet UIButton *mfAvgSize;
@property (weak, nonatomic) IBOutlet UIButton *mfTransactionsButton;
@property (weak, nonatomic) IBOutlet UIButton *optionsTradingLevelButton;
@property (weak, nonatomic) IBOutlet UIButton *TLButtonOne;
@property (weak, nonatomic) IBOutlet UIButton *TLButtonTwo;
@property (weak, nonatomic) IBOutlet UIButton *TLButtonThree;
@property (weak, nonatomic) IBOutlet UIButton *TLButtonFour;
@property (weak, nonatomic) IBOutlet UIButton *TLButtonFive;
@property (weak, nonatomic) IBOutlet UIButton *TLButtonSix;
@property (weak, nonatomic) IBOutlet UIButton *TLButtonSeven;
@property (weak, nonatomic) IBOutlet UIButton *TLButtonEight;
@property (weak, nonatomic) IBOutlet UIButton *TLButtonNine;
@property (weak, nonatomic) IBOutlet UIButton *TLButtonTen;
@property (weak, nonatomic) IBOutlet UIButton *TLButtonEleven;
@property (weak, nonatomic) IBOutlet UIButton *signatureButton;
@property (weak, nonatomic) IBOutlet UIView *optionTradingView;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *otherIncomeTF;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@property NSString * yesString;
@property NSString * objString;
@property NSString * alphabetString;
@property NSMutableArray * dataArray;
@property NSString * optionLevel;
@property UIAlertAction * camera;
@property UIAlertAction * gallery;
@property (weak, nonatomic) IBOutlet UIView *otherIncomeView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *otherIncomeViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *optionViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *otherIncomeTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *foreignViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UITextField *signatureTF;
@property (weak, nonatomic) IBOutlet UIButton *idProofButton;
@property (weak, nonatomic) IBOutlet UIButton *bankStatementButton;
@property NSString * multipleFilesString;
@property (weak, nonatomic) IBOutlet UIView *doneView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bgViewHeightConstraint;

@end
