//
//  TwoUSOpenAccountViewController.m
//  ChoiceTradeAccountOpening
//
//  Created by Zenwise Technologies on 14/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "TwoUSOpenAccountViewController.h"
#import "AppDelegate.h"
#import "AgreementViewController.h"
#import "ThreeUSOpenAccountViewController.h"
#import <UITextField_AutoSuggestion/UITextField+AutoSuggestion.h>
#import <Mixpanel/Mixpanel.h>
#import "DGActivityIndicatorView.h"

@interface TwoUSOpenAccountViewController ()<UITextFieldAutoSuggestionDataSource,UITextFieldDelegate>
{
    UIToolbar * toolbar;
    AppDelegate * delegate;
     UITextField * selectedTextField;
    NSString * typeofTF;
    DGActivityIndicatorView *activityIndicatorView;
    UIView * backView;
    UIButton * myButton;
     NSDateFormatter *dateFormatter;
}

#define Symbol_ID @"symbol_id"

#define WEEKS @[@"Monday", @"Tuesday", @"Wednesday", @"Thirsday", @"Friday", @"Saturday", @"Sunday"]

@end

@implementation TwoUSOpenAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
//    self.addressOneTF.text=@"Pune";
//    self.addressTwoTF.text=@"Pune";
//    self.mobileNumber=@"8143221217";
//    self.occupationTF.text=@"Developer";
//    self.yearsEmpTF.text=@"2";
//    self.companyNameTF.text=@"Zenwise Technologies Private Limited";
//    self.typeofBusinessTF.text=@"IT";
//    self.businessMobile.text=@"8143221217";
    
    self.navigationItem.title=@"STEP 2";
    self.datePicker.hidden=YES;
    self.continueButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.continueButton.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.continueButton.layer.shadowOpacity = 10.0f;
    self.continueButton.layer.shadowRadius = 3.0f;
    self.continueButton.layer.masksToBounds = NO;
   
    self.empStatusButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
   
    self.doneView.hidden=YES;
    self.pickerView.hidden=YES;
    self.dataArray = [[NSMutableArray alloc]init];
    [self.dataArray addObject:@"Employed"];
    [self.dataArray addObject:@"SelfEmployed"];
    [self.dataArray addObject:@"Unemployed"];
    self.unEmployedView.hidden=YES;
    self.employedView.hidden=NO;
    NSDate * date = [[NSDate alloc]init];
    date = [NSDate date];
    dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateStyle=NSDateFormatterLongStyle;
    [self.lastDateEmployedBtn setTitle:[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:date]] forState:UIControlStateNormal];
//    self.addressOneTF.text = [NSString stringWithFormat:@"%@",[delegate.addressDict objectForKey:@"Street"]];
//
//    self.addressTwoTF.text = [NSString stringWithFormat:@"%@",[delegate.addressDict objectForKey:@"Thoroughfare"]];
//
//    self.countryTF.text = [NSString stringWithFormat:@"%@",[delegate.addressDict objectForKey:@"Country"]];
//
//    self.stateTF.text = [NSString stringWithFormat:@"%@",[delegate.addressDict objectForKey:@"State"]];
//
//    self.cityTF.text = [NSString stringWithFormat:@"%@",[delegate.addressDict objectForKey:@"City"]];
//
//    self.postalCodeTF.text = [NSString stringWithFormat:@"%@",[delegate.addressDict objectForKey:@"ZIP"]];
//
//    self.businessAddressTF.text = [NSString stringWithFormat:@"%@",[delegate.addressDict objectForKey:@"Street"]];
//
//
//
//    self.businessCountry.text = [NSString stringWithFormat:@"%@",[delegate.addressDict objectForKey:@"Country"]];
//
//    self.businessState.text = [NSString stringWithFormat:@"%@",[delegate.addressDict objectForKey:@"State"]];
//
//    self.businessCity.text = [NSString stringWithFormat:@"%@",[delegate.addressDict objectForKey:@"City"]];
//
//    self.businessPostalCode.text = [NSString stringWithFormat:@"%@",[delegate.addressDict objectForKey:@"ZIP"]];
//
    
    
    [self.empStatusButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.legalButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.continueButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
     [self.lastDateEmployedBtn addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    
    self.symbols = [[NSMutableArray alloc]init];
    self.company = [[NSMutableArray alloc]init];
    self.selectedCompanyArray = [[NSMutableArray alloc]init];
    self.searchResponseArray = [[NSMutableArray alloc]init];
    self.selectedCountryArray = [[NSMutableArray alloc]init];
    self.removeCountriesArray = [[NSMutableArray alloc]initWithObjects:@"Balkans",@"Belarus",@"Balkans",@"Burma",@"Cote D''Ivoire (Ivory Coast)",@"Cuba",@"Congo",@"Iran",@"Iraq",@"Liberia",@"Korea North",@"Sudan",@"Syria",@"Zimbabwe",@"United States", nil];
    //     ['Balkans', 'Belarus', 'Burma', "Cote D''Ivoire (Ivory Coast)", 'Cuba', 'Congo', 'Iran', 'Iraq', 'Liberia', 'Korea North', 'Sudan', 'Syria', 'Zimbabwe', 'United States']
    
    // self.searchTableView.hidden=YES;
    
    [self searchServer];
    self.countryTF.delegate = self;
    self.countryTF.autoSuggestionDataSource = self;
    self.countryTF.fieldIdentifier = Symbol_ID;
    [self.countryTF observeTextFieldChanges];
    
    
    self.stateTF.delegate = self;
    self.stateTF.autoSuggestionDataSource = self;
    self.stateTF.fieldIdentifier = Symbol_ID;
    [self.stateTF observeTextFieldChanges];
    
    self.cityTF.delegate = self;
    self.cityTF.autoSuggestionDataSource = self;
    self.cityTF.fieldIdentifier = Symbol_ID;
    [self.cityTF observeTextFieldChanges];
    
    self.businessCountry.delegate = self;
    self.businessCountry.autoSuggestionDataSource = self;
    self.businessCountry.fieldIdentifier = Symbol_ID;
    [self.businessCountry observeTextFieldChanges];
    
    self.businessState.delegate = self;
    self.businessState.autoSuggestionDataSource = self;
    self.businessState.fieldIdentifier = Symbol_ID;
    [self.businessState observeTextFieldChanges];
    
    self.businessCity.delegate = self;
    self.businessCity.autoSuggestionDataSource = self;
    self.businessCity.fieldIdentifier = Symbol_ID;
    [self.businessCity observeTextFieldChanges];
    
    self.postalCodeTF.delegate = self;
    self.mobileNOTF.delegate=self;
    self.occupationTF.delegate=self;
    self.yearsEmpTF.delegate=self;
    self.companyNameTF.delegate=self;
    self.typeofBusinessTF.delegate=self;
    self.businessAddressTF.delegate=self;
    self.businessCountry.delegate=self;
    self.businessState.delegate=self;
    self.businessPostalCode.delegate=self;
    self.businessMobile.delegate=self;
    self.addressOneTF.delegate = self;
    
    // Do any additional setup after loading the view.
}

- (void)loadWeekDays {
    // cancel previous requests
    @try {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(loadWeekDaysInBackground) object:self];
        //change
        [selectedTextField setLoading:false];
        
        // clear previous results
        [self.symbols removeAllObjects];
        
        //change
        [selectedTextField reloadContents];
        
        // start loading
        
        //change
        [selectedTextField setLoading:true];
        [self performSelector:@selector(loadWeekDaysInBackground) withObject:self afterDelay:2.0f];
        
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (void)loadWeekDaysInBackground {
    // finish loading
    @try {
        
        //change
        [selectedTextField setLoading:false];
        
        
        [self.symbols addObjectsFromArray:self.company];
        
        //change
        [selectedTextField reloadContents];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}

- (UITableViewCell *)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
    
    static NSString *cellIdentifier = @"WeekAutoSuggestionCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF BEGINSWITH[c] %@", text];
    NSArray *weeks = [self.symbols filteredArrayUsingPredicate:filterPredictate];
    
    cell.textLabel.text = weeks[indexPath.row];
    
    return cell;
}


- (NSInteger)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section forText:(NSString *)text {
    
    NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF BEGINSWITH[c] %@", text];
    
    @try {
        NSInteger count = [self.symbols filteredArrayUsingPredicate:filterPredictate].count;
        return count;
    }
    @catch (NSException *exception) {
        //NSLog(@"%@", exception.reason);
    }
    
    return 0;
    
}

- (void)autoSuggestionField:(UITextField *)field textChanged:(NSString *)text {
    [self loadWeekDays];
}


- (CGFloat)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
    return 50;
}

- (void)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    //NSLog(@"Selected suggestion at index row - %ld", (long)indexPath.row);
    
    
    
    NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF BEGINSWITH[c] %@", text];
    NSArray *weeks = [self.symbols filteredArrayUsingPredicate:filterPredictate];
    //change
    selectedTextField.text = weeks[indexPath.row];
    
//    if(self.stateIDString.length>0)
//    {
//        self.stateIDString=@"";
//    }
    
//    if(self.selectedCompanyArray.count>0)
//    {
//        [self.selectedCompanyArray removeAllObjects];
//    }
//
    
    //NSLog(@"Selected Company Array:%@",[self.selectedCountryArray objectAtIndex:indexPath.row]);
    if(selectedTextField==self.countryTF || selectedTextField == self.businessCountry)
    {
    for (int i=0; i<self.searchResponseArray.count; i++) {
        NSString * string1 = [NSString stringWithFormat:@"%@",[[[self.responseDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"]];
        if([string1 isEqualToString:selectedTextField.text])
        {
            self.stateIDString = [[[self.responseDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"id"];
            self.selectedISO = [[[self.responseDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"iso3code"];
        }
    }
    }else if (selectedTextField==self.stateTF || selectedTextField == self.businessState)
    {
        for (int i=0; i<self.statesResponseArray.count; i++) {
            NSString * string1 = [NSString stringWithFormat:@"%@",[[[self.statesResponseDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"]];
            if([string1 isEqualToString:selectedTextField.text])
            {
                self.cityIDString = [[[self.statesResponseDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"id"];
            }
        }
    }
    [self parametersMethod:selectedTextField];
//    self.stateIDString = [NSString stringWithFormat:@"%@",[self.searchResponseArray objectAtIndex:indexPath.row]];
//    [self.selectedCompanyArray addObject:[self.searchResponseArray objectAtIndex:indexPath.row]];
//    self.stateIDString = [NSString stringWithFormat:@"%@",[self.selectedCountryArray objectAtIndex:indexPath.row]];
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == self.countryTF || textField == self.businessCountry)
    {
         [self searchServer];
    }
   // [self searchServerState];
    selectedTextField = textField;
    NSString * searchStr = [selectedTextField.text stringByReplacingCharactersInRange:range withString:string];
   
    if(textField==self.stateTF || textField==self.businessState)
    {
        [self searchServerState];
        return YES;
    }else if(textField==self.cityTF || textField == self.businessCity)
    {
        [self searchServerState];
        return YES;
    }else
    {
        if(self.company.count>0)
        {
            [self.company removeAllObjects];
        }
    for (int i=0; i<self.searchResponseArray.count; i++) {
        NSString * string1 = [[self.searchResponseArray objectAtIndex:i] objectForKey:@"name"];
        NSString * string3 = [[self.searchResponseArray objectAtIndex:i] objectForKey:@"id"];
        NSString * string2 = [string1 lowercaseString];
        searchStr = [searchStr lowercaseString];
        if([string2 containsString:searchStr])
        {
            [self.company addObject:string1];
            [self.selectedCountryArray addObject:string3];
        }
    }
    return YES;
    }
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    selectedTextField = textField;
    if(textField == self.stateTF ||textField == self.businessState)
    {
        typeofTF=@"state";
         [self searchServerState];
        
    }else if (textField==self.cityTF || textField == self.businessCity)
    {
        typeofTF=@"city";
         [self searchServerState];
    }
    
        if(textField == self.stateTF)
        {
            if(self.countryTF.text.length>0)
            {
                return YES;
            }else
            {
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Alert" message:@"Please select country first." preferredStyle:UIAlertControllerStyleAlert];
    
                        [self presentViewController:alert animated:YES completion:^{
    
                        }];
    
                        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [self.countryTF becomeFirstResponder];
                        }];
    
                        [alert addAction:okAction];
                        return YES;
            }
        }
    
        if(textField == self.cityTF)
        {
            if(self.countryTF.text.length>0&&self.stateTF.text.length>0)
            {
                return YES;
            }else
            {
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Alert" message:@"Please select country and state first." preferredStyle:UIAlertControllerStyleAlert];
    
                [self presentViewController:alert animated:YES completion:^{
    
                }];
    
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    if(self.countryTF.text.length==0)
                    {
                    [self.countryTF becomeFirstResponder];
                    }else if (self.stateTF.text.length==0)
                    {
                        [self.stateTF becomeFirstResponder];
                    }
                }];
    
                [alert addAction:okAction];
                return YES;
            }
        }
    
    
    if(textField == self.businessState)
    {
        if(self.businessCountry.text.length>0)
        {
            return YES;
        }else
        {
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Alert" message:@"Please select country first." preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:^{
                
            }];
            
            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.businessCountry becomeFirstResponder];
            }];
            
            [alert addAction:okAction];
            return YES;
        }
    }
    
    if(textField == self.businessCity)
    {
        if(self.businessCountry.text.length>0&&self.businessState.text.length>0)
        {
            return YES;
        }else
        {
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Alert" message:@"Please select country and state first." preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:^{
                
            }];
            
            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                if(self.businessCountry.text.length==0)
                {
                    [self.businessCountry becomeFirstResponder];
                }else if (self.businessState.text.length==0)
                {
                    [self.businessState becomeFirstResponder];
                }
            }];
            
            [alert addAction:okAction];
            return YES;
        }
    }
   
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}

- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    // [self.view setFrame:CGRectMake(0,-100,self.view.frame.size.width,self.view.frame.size.height)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
   // [self parametersMethod:selectedTextField];
//    if(textField == self.stateTF)
//    {
//        if(self.countryTF.text.length>0)
//        {
//            return YES;
//        }else
//        {
//            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Alert" message:@"Please select country first." preferredStyle:UIAlertControllerStyleAlert];
//
//                    [self presentViewController:alert animated:YES completion:^{
//
//                    }];
//
//                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                        [self.countryTF becomeFirstResponder];
//                    }];
//
//                    [alert addAction:okAction];
//                    return YES;
//        }
//    }
//
//    if(textField == self.cityTF)
//    {
//        if(self.countryTF.text.length>0&&self.stateTF.text.length>0)
//        {
//            return YES;
//        }else
//        {
//            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Alert" message:@"Please select country and state first." preferredStyle:UIAlertControllerStyleAlert];
//
//            [self presentViewController:alert animated:YES completion:^{
//
//            }];
//
//            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                if(self.countryTF.text.length==0)
//                {
//                [self.countryTF becomeFirstResponder];
//                }else if (self.stateTF.text.length==0)
//                {
//                    [self.stateTF becomeFirstResponder];
//                }
//            }];
//
//            [alert addAction:okAction];
//            return YES;
//        }
//    }
//    if(textField.text.length==0)
//    {
//        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Alert" message:@"This field is mandatory. Please fill the detail." preferredStyle:UIAlertControllerStyleAlert];
//
//        [self presentViewController:alert animated:YES completion:^{
//
//        }];
//
//        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//        }];
//
//        [alert addAction:okAction];
//        return YES;
//    }else
//    {
//        return YES;
//    }
    
//    if(textField.text.length>0)
//    {
//        return YES;
//    }else
//    {
//        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Please fill previous detail." preferredStyle:UIAlertControllerStyleAlert];
//
//        [self presentViewController:alert animated:YES completion:^{
//
//        }];
//
//        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//        }];
//
//        [alert addAction:okAction];
//        return NO;
//    }
    //[self.view endEditing:YES];
return YES;
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    //[self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
}

-(void)searchServer
{
    
    NSDictionary *headers = @{ @"Cache-Control": @"no-cache",
                               @"authtoken":delegate.zenwiseToken,
                                @"deviceid":delegate.currentDeviceId
                               };
    NSString * str=[NSString stringWithFormat:@"%@countryinfo/countrystatecity?type=country",delegate.baseUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:str]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==403)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                            
                                                        }
                                                        else if([httpResponse statusCode]==401)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                        self.responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        self.searchResponseArray= [[NSMutableArray alloc]initWithArray:[self.responseDictionary objectForKey:@"data"]];
                                                        //NSLog(@"Search:%@",self.searchResponseArray);
                                                        
                                                        NSArray * localArray = [self.searchResponseArray mutableCopy];
                                                        [self.searchResponseArray removeAllObjects];
                                                        
                                                        
                                                        
                                                        for (int i=0; i<localArray.count; i++) {
                                                            NSString * string1 = [NSString stringWithFormat:@"%@",[[localArray objectAtIndex:i]objectForKey:@"name"]];
                                                
                                                            BOOL check=true;
                                                            for (int j=0; j<self.removeCountriesArray.count; j++) {
                                                                NSString * string2 = [self.removeCountriesArray objectAtIndex:j];
                                                                string2 = [string2 lowercaseString];
                                                                string1 = [string1 lowercaseString];
                                                                
                                                                if([string1 isEqualToString:string2])
                                                                {
                                                                    NSMutableArray * localArray=[[NSMutableArray alloc]init];
                                                                    for(int k=0;k<self.searchResponseArray.count;k++)
                                                                    {
                                                                        [localArray addObject:[[[self.searchResponseArray objectAtIndex:k]objectForKey:@"name"]lowercaseString]];
                                                                        
                                                                    }
                                                                    if([localArray containsObject:string1])
                                                                    {
                                                                        int index=(int)[localArray indexOfObject:string1];
                                                                        
                                                                        [self.searchResponseArray removeObjectAtIndex:index];
                                                                    }
                                                                    
                                                                }
                                                                
                                                                else if(check==true)
                                                                    
                                                                {
                                                                    check=false;
                                                                    [self.searchResponseArray addObject:[localArray objectAtIndex:i]];
                                                                    
                                                                }
                                                            }
                                                            
                                                            
                                                        }
                                                        
                                                        }
                                                    }
                                                }];
    [dataTask resume];
    
    
   
}

-(void)searchServerState
{
   // self.stateIDString=@"101";
   // NSString * city=@"22";
    NSDictionary *headers = @{ @"Cache-Control": @"no-cache",
                               @"authtoken":delegate.zenwiseToken,
                                @"deviceid":delegate.currentDeviceId
                            };
    
    NSString * url;
    if([typeofTF isEqualToString:@"state"])
    {
        
       url = [NSString stringWithFormat:@"%@countryinfo/countrystatecity?type=state&id=%@",delegate.baseUrl,self.stateIDString];
    }else if ([typeofTF isEqualToString:@"city"])
    {
       url = [NSString stringWithFormat:@"%@countryinfo/countrystatecity?type=city&id=%@",delegate.baseUrl,self.cityIDString];
    }
        
  
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==403)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                            
                                                        }
                                                        else if([httpResponse statusCode]==401)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                        self.statesResponseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        self.statesResponseArray = [[NSMutableArray alloc]initWithArray:[self.statesResponseDictionary objectForKey:@"data"]];
                                                        
                                                        //NSLog(@"States:%@",self.statesResponseArray);
                                                        if(self.company.count>0)
                                                        {
                                                            [self.company removeAllObjects];
                                                        }
                                                        for (int i=0; i<self.statesResponseArray.count; i++) {
                                                            [self.company addObject:[[self.statesResponseArray objectAtIndex:i]objectForKey:@"name"]];
                                                        }
                                                        }
                                                    }
                                                }];
    [dataTask resume];
}

-(void)onButtonTap:(UIButton *)sender
{
   
    myButton = (UIButton*)sender;
    [selectedTextField resignFirstResponder];
    if(sender == self.empStatusButton)
    {
    self.pickerView.hidden=NO;
        self.doneView.hidden=NO;
    self.pickerView.delegate=self;
    self.pickerView.dataSource=self;
    
    self.pickerView.backgroundColor=[UIColor lightGrayColor];
    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(onPickerDoneButtonTap)];
    
    toolbar= [[UIToolbar alloc]initWithFrame:CGRectMake(0,0,self.view.frame.size.width,50)];
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton,nil];
    [toolbar setItems:toolbarItems];
    [self.doneView addSubview:toolbar];
    CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
    [self.scrollView setContentOffset:bottomOffset animated:YES];
    }else if (sender == self.legalButton)
    {
        AgreementViewController * agrement = [self.storyboard instantiateViewControllerWithIdentifier:@"AgreementViewController"];
        [self.navigationController pushViewController:agrement animated:YES];
    }else if (sender == self.continueButton)
    {
        if(self.addressOneTF.text.length>0&&self.addressTwoTF.text.length>0&&self.countryTF.text.length>0&&self.stateTF.text.length>0&&self.cityTF.text.length>0&&self.postalCodeTF.text.length>0&&self.mobileNumber.length>0&&self.occupationTF.text.length>0&&self.yearsEmpTF.text.length>0&&self.companyNameTF.text.length>0&&self.typeofBusinessTF.text.length>0&&self.businessAddressTF.text.length>0&&self.businessCountry.text.length>0&&self.businessState.text.length>0&&self.businessCity.text.length>0&&self.businessPostalCode.text.length>0&&self.businessMobile.text.length>0&&self.selectedISO.length>0)
        {
       [self parametersMethod:self.continueButton];
       [self choiceTradeServer];
    //    ThreeUSOpenAccountViewController *three = [self.storyboard instantiateViewControllerWithIdentifier:@"ThreeUSOpenAccountViewController"];
      //  [self.navigationController pushViewController:three animated:YES];
        }
        else if(self.previousEmployerLbl.text.length>0&&self.unemployedTypeOfBusiness.text.length>0&&self.occupationOfPreviousEmployementLbl.text.length>0&&self.unemployedYearsEmployedLbl.text.length>0&&self.lastDateEmployedBtn.titleLabel.text.length>0)
        {
            [self parametersMethod:self.continueButton];
            [self choiceTradeServer];
        }
        else
        {
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Missing Fields. Please Verify" preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:^{
                
            }];
            
            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            [alert addAction:okAction];
        }
    }
    else if (sender == self.lastDateEmployedBtn)
    {
        CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
        [self.scrollView setContentOffset:bottomOffset animated:YES];
        self.pickerView.hidden=YES;
        self.datePicker.hidden=NO;
        self.doneView.hidden=NO;
        self.datePicker.backgroundColor=[UIColor lightGrayColor];
        [self.datePicker addTarget:self action:@selector(LabelTitle) forControlEvents:UIControlEventAllEvents];
        UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(onDOBDoneButtonTap)];
        
        toolbar= [[UIToolbar alloc]initWithFrame:CGRectMake(0,0,self.view.frame.size.width,50)];
        NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton,nil];
        [toolbar setItems:toolbarItems];
        [self.doneView addSubview:toolbar];
        
    }
}
-(void)onDOBDoneButtonTap
{
 
    
        //[self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        self.datePicker.hidden=YES;
        self.doneView.hidden=YES;
        [toolbar removeFromSuperview];
   
        [self.scrollView setContentOffset:CGPointMake(0, myButton.frame.origin.y-100) animated:YES];
}

-(void)LabelTitle
{
    dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateStyle=NSDateFormatterLongStyle;
    [self.lastDateEmployedBtn setTitle:[NSString stringWithFormat:@"%@",[dateFormatter  stringFromDate:self.datePicker.date]] forState:UIControlStateNormal];
}

-(void)onPickerDoneButtonTap
{
    self.doneView.hidden=YES;
    self.pickerView.hidden=YES;
    [toolbar removeFromSuperview];
   // [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    
    [self.scrollView setContentOffset:CGPointMake(0, myButton.frame.origin.y-100) animated:YES];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView
numberOfRowsInComponent:(NSInteger)component {
    return self.dataArray.count;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [NSString stringWithFormat:@"%@",[self.dataArray objectAtIndex:row]];//Or, your suitable title; like Choice-a, etc.
}

- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    
    [self.empStatusButton setTitle:[self.dataArray objectAtIndex:row] forState:UIControlStateNormal];
    
    if([self.empStatusButton.titleLabel.text isEqualToString:@"Unemployed"])
    {
        self.unEmployedView.hidden=NO;
        self.employedView.hidden=YES;
        self.employedViewHgt.constant=500;
    }
    else
    {
        self.unEmployedView.hidden=YES;
         self.employedView.hidden=NO;
        self.employedViewHgt.constant=962;
    }
}

-(void)parametersMethod:(id)sender
{
    [delegate.finalChoiceTradeDict setObject:self.empStatusButton.titleLabel.text forKey:@"empstatus"];
    if([self.empStatusButton.titleLabel.text isEqualToString:@"Unemployed"])
    {
        
        [delegate.finalChoiceTradeDict setObject:self.previousEmployerLbl.text forKey:@"previousemployer"];
        [delegate.finalChoiceTradeDict setObject:self.unemployedTypeOfBusiness.text forKey:@"typeofbussiness"];
        [delegate.finalChoiceTradeDict setObject:self.occupationOfPreviousEmployementLbl.text forKey:@"occupationofprevoiusemployemnt"];
        [delegate.finalChoiceTradeDict setObject:self.unemployedYearsEmployedLbl.text forKey:@"yearsemployed"];
        NSString *str1 = self.lastDateEmployedBtn.titleLabel.text; /// here this is your date with format yyyy-MM-dd
        dateFormatter.dateStyle=NSDateFormatterLongStyle;
        //// here set format of date which is in your output date (means above str with format)
        
        NSDate *date1 = [dateFormatter dateFromString: str1]; // here you can fetch date from string with define format
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"YYYY-MM-dd"];// here set format which you want...
        
        NSString *convertedString1 = [dateFormatter stringFromDate:date1]; //here convert date in NSString
        //NSLog(@"Converted String : %@",convertedString1);
        [delegate.finalChoiceTradeDict setObject:convertedString1 forKey:@"lastdateemployed"];
    }
    else
    {
    [delegate.finalChoiceTradeDict setObject:self.addressOneTF.text forKey:@"addressone"];
    [delegate.finalChoiceTradeDict setObject:self.addressTwoTF.text forKey:@"addresstwo"];
    [delegate.finalChoiceTradeDict setObject:self.selectedISO forKey:@"country"];
    [delegate.finalChoiceTradeDict setObject:self.stateTF.text forKey:@"state"];
    [delegate.finalChoiceTradeDict setObject:self.cityTF.text forKey:@"city"];
    [delegate.finalChoiceTradeDict setObject:self.postalCodeTF.text forKey:@"postalcode"];
    [delegate.finalChoiceTradeDict setObject:self.mobileNumber forKey:@"mobile"];
   
    [delegate.finalChoiceTradeDict setObject:self.occupationTF.text forKey:@"occupation"];
    [delegate.finalChoiceTradeDict setObject:self.yearsEmpTF.text forKey:@"yearsemp"];
    [delegate.finalChoiceTradeDict setObject:self.companyNameTF.text forKey:@"companyname"];
    [delegate.finalChoiceTradeDict setObject:self.typeofBusinessTF.text forKey:@"busineestype"];
    [delegate.finalChoiceTradeDict setObject:self.businessAddressTF.text forKey:@"businessaddress"];
    [delegate.finalChoiceTradeDict setObject:self.selectedISO forKey:@"businesscountry"];
    [delegate.finalChoiceTradeDict setObject:self.businessState.text forKey:@"businessstate"];
    [delegate.finalChoiceTradeDict setObject:self.businessCity.text forKey:@"businessCity"];
    [delegate.finalChoiceTradeDict setObject:self.businessPostalCode.text forKey:@"businesspostal"];
    [delegate.finalChoiceTradeDict setObject:self.businessMobile.text forKey:@"businessmobile"];
    }
}

-(void)choiceTradeServer
{
    backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.view addSubview:backView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeFiveDots tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [backView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    NSString * apiKey = [NSString stringWithFormat:@"Bearer %@",delegate.apiKey];
    NSDictionary *headers = @{ @"Content-Type": @"application/x-www-form-urlencoded",
                               @"Authorization": apiKey,
                               };
    
    NSString * title = [NSString stringWithFormat:@"Title=%@",[delegate.finalChoiceTradeDict objectForKey:@"title"]];
    
    NSString * firstName = [NSString stringWithFormat:@"&FirstName=%@",[delegate.finalChoiceTradeDict objectForKey:@"firstname"]];
    
    NSString * middlename = [NSString stringWithFormat:@"&MiddleInitial=%@",[delegate.finalChoiceTradeDict objectForKey:@"middlename"]];
    
    NSString * lastname = [NSString stringWithFormat:@"&LastName=%@",[delegate.finalChoiceTradeDict objectForKey:@"lastname"]];
    
    NSString * govtid = [NSString stringWithFormat:@"&GovtID=%@",[delegate.finalChoiceTradeDict objectForKey:@"govtid"]];
    
    NSString * dob = [NSString stringWithFormat:@"&DOB=%@",[delegate.finalChoiceTradeDict objectForKey:@"dob"]];
    
    NSString * maritalstatus = [NSString stringWithFormat:@"&MaritalStatus=%@",[delegate.finalChoiceTradeDict objectForKey:@"maritalstatus"]];
    
    
    NSString * gender = [NSString stringWithFormat:@"&Gender=%@",[delegate.finalChoiceTradeDict objectForKey:@"gender"]];
    
    NSString * noofdep = [NSString stringWithFormat:@"&NumberDependents=%@",@"2"];
    
    NSString * paddressone = [NSString stringWithFormat:@"&Paddress1=%@",[delegate.finalChoiceTradeDict objectForKey:@"addressone"]];
    
    NSString * paddresstwo = [NSString stringWithFormat:@"&Paddress2=%@",[delegate.finalChoiceTradeDict objectForKey:@"addresstwo"]];
    
    NSString * city = [NSString stringWithFormat:@"&Pcity=%@",[delegate.finalChoiceTradeDict objectForKey:@"city"]];
    
    NSString * state = [NSString stringWithFormat:@"&Pstate=%@",[delegate.finalChoiceTradeDict objectForKey:@"state"]];
    state=@"&Pstate=AE";
    
    NSString * zipcode = [NSString stringWithFormat:@"&Pzip=%@",[delegate.finalChoiceTradeDict objectForKey:@"postalcode"]];
    
    NSString * country = [NSString stringWithFormat:@"&Pcountry=%@",[delegate.finalChoiceTradeDict objectForKey:@"country"]];
   // country=@"&Pcountry=IND";
    
    NSString * mobile = [NSString stringWithFormat:@"&MobilePhone=%@",[delegate.finalChoiceTradeDict objectForKey:@"mobile"]];
    
    NSString * home = [NSString stringWithFormat:@"&HomePhone=%@",[delegate.finalChoiceTradeDict objectForKey:@"mobile"]];
    
    
    NSMutableData *postData = [[NSMutableData alloc] initWithData:[title dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[firstName dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[middlename dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[lastname dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[govtid dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[dob dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[maritalstatus dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[gender dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[noofdep dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[paddressone dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[paddresstwo dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[city dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[state dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[zipcode dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[country dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"&MailingDiff=N" dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[mobile dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[home dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"&OwnerDisclosure=N" dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"&Step=1" dataUsingEncoding:NSUTF8StringEncoding]];
    
   // NSString * url = [NSString stringWithFormat:@"http://newacctapi-dev.letsgotrade.com/api/accounts/step1/%@",delegate.appIDString];
    NSString * url = [NSString stringWithFormat:@"%@api/accounts/step1/%@",delegate.producationURL,delegate.appIDString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    [request setHTTPMethod:@"PUT"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        NSMutableDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        //NSLog(@"%@",responseDict);
                                                        
                                                        if([[responseDict objectForKey:@"status"]isEqualToString:@"200"])
                                                        {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                [activityIndicatorView stopAnimating];
                                                                [backView removeFromSuperview];
                                                                NSUserDefaults * openAccountPref = [NSUserDefaults standardUserDefaults];
                                                                [openAccountPref setObject:@"step3" forKey:@"openAccountStep"];
                                                                [openAccountPref synchronize];
                                                                
                                                                NSUserDefaults * openFinalDict = [NSUserDefaults standardUserDefaults];
                                                                [openFinalDict setObject:delegate.finalChoiceTradeDict forKey:@"openDict"];
                                                                [openFinalDict synchronize];
                                                                ThreeUSOpenAccountViewController *three = [self.storyboard instantiateViewControllerWithIdentifier:@"ThreeUSOpenAccountViewController"];
                                                                [self.navigationController pushViewController:three animated:YES];
                                                            });
                                                            
                                                            Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
                                                            [mixpanelMini track:@"usa_open_account_step2_completed_page"];
                                                            
                                                        }else
                                                        {
                                                            NSString * errorText = [NSString stringWithFormat:@"%@.Please check the inputs given again.",[responseDict objectForKey:@"message"]];
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:errorText preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                [activityIndicatorView stopAnimating];
                                                                [backView removeFromSuperview];
                                                            }];
                                                            
                                                            [alert addAction:okAction];
                                                        }
                                            
                                                    }
                                                }];
    [dataTask resume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
