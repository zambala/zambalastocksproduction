//
//  ThreeUSOpenAccountViewController.m
//  ChoiceTradeAccountOpening
//
//  Created by Zenwise Technologies on 15/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "ThreeUSOpenAccountViewController.h"
#import "PolicyViewController.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AFNetworking/AFNetworking.h>
#import <Mixpanel/Mixpanel.h>
#import "OpenAccountViewController.h"
#import "MainViewUSA.h"
#import "DGActivityIndicatorView.h"



@interface ThreeUSOpenAccountViewController ()
{
    AppDelegate * delegate;
    UIToolbar * toolbar;
    UIButton * myButton;
    CGPoint coordinates;
    NSString * checkString;
    UIImagePickerController * pickerController;
    UIImage *image;
    UIActivityIndicatorView *activityView;
    NSString * apiKey;
    NSString * signatureName;
    NSString * signature;
    NSMutableURLRequest *request1;
    NSString *str_localFilePath;
    NSData * data;
    DGActivityIndicatorView *activityIndicatorView;
    UIView * backView;
    NSString * bankStatementCheck;
    NSString * photoCheck;
    NSString * signatureCheck;
}



@end

@implementation ThreeUSOpenAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title=@"STEP 1";
    delegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    self.dataArray = [[NSMutableArray alloc]init];
    
    self.navigationItem.title=@"STEP 3";
    
    self.clickHereButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    
    self.incomeSourceButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.incomeSourceButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.netWorthButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.netWorthButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.liquidAssetsButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.liquidAssetsButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.taxButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.taxButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.sourceOfWealthButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.sourceOfWealthButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.profileButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.profileButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.dayTradingButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.dayTradingButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.activeTraderButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.activeTraderButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.financialButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.financialButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.objectiveButto.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.objectiveButto addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.stockExpButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.stockExpButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.avgStockSize.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.avgStockSize addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.transactionsStockButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.transactionsStockButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.optionsExpButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.optionsExpButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.optionsSizeButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.optionsSizeButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.transactionsOptionsButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.transactionsOptionsButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.bondsExpButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.bondsExpButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.bondsSizeButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.bondsSizeButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.bondsTransactionsButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.bondsTransactionsButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.mfExpButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.mfExpButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.mfAvgSize.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.mfAvgSize addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.mfTransactionsButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.mfTransactionsButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.optionsTradingLevelButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.optionsTradingLevelButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.TLButtonOne.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.TLButtonOne addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.TLButtonTwo.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.TLButtonTwo addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.TLButtonThree.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.TLButtonThree addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.TLButtonFour.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.TLButtonFour addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.TLButtonFive.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.TLButtonFive addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.TLButtonSix.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.TLButtonSix addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    
    self.TLButtonSeven.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.TLButtonSeven addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.TLButtonEight.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.TLButtonEight addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.TLButtonNine.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.TLButtonNine addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.TLButtonTen.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.TLButtonTen addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.TLButtonEleven.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.TLButtonEleven addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.signatureButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.signatureButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.idProofButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.idProofButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.bankStatementButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.bankStatementButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.annualImcomeSourceButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.annualImcomeSourceButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.submitButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
   
    
    self.pickerView.hidden=YES;
    
    self.optionTradingView.hidden=YES;
    self.optionViewHeightConstraint.constant=0;
    self.foreignViewHeightConstraint.constant=1700;
    
    self.otherIncomeView.hidden=YES;
    self.otherIncomeViewHeightConstraint.constant=0;
    self.otherIncomeTopConstraint.constant=0;
    self.doneView.hidden=YES;
    
    if([[delegate.finalChoiceTradeDict objectForKey:@"countryofbirth"]isEqualToString:@"India"])
    {
    self.foreignView.hidden=YES;
    self.foreignViewHeightConstraint.constant=0;
    }else
    {
        self.foreignView.hidden=NO;
        self.foreignViewHeightConstraint.constant=2900;
    }
    [self.checkButton setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    [self.checkButton setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    [self.checkButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.clickHereButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    
    
    checkString=@"No Options";
    self.yesString=@"N";
        [delegate.finalChoiceTradeDict setObject:@"A" forKey:@"annualincome"];
   
        [delegate.finalChoiceTradeDict setObject:@"A" forKey:@"liquid"];
   
        [delegate.finalChoiceTradeDict setObject:@"A" forKey:@"networth"];
   

        [delegate.finalChoiceTradeDict setObject:@"Personal" forKey:@"wealth"];
    [delegate.finalChoiceTradeDict setObject:@"N" forKey:@"button1"];
    [delegate.finalChoiceTradeDict setObject:@"N" forKey:@"button2"];
    [delegate.finalChoiceTradeDict setObject:@"N" forKey:@"button3"];
    [delegate.finalChoiceTradeDict setObject:@"N" forKey:@"button4"];
    [delegate.finalChoiceTradeDict setObject:@"N" forKey:@"button5"];
    [delegate.finalChoiceTradeDict setObject:@"N" forKey:@"button6"];
    [delegate.finalChoiceTradeDict setObject:@"N" forKey:@"button8"];
    [delegate.finalChoiceTradeDict setObject:@"N" forKey:@"button9"];
    [delegate.finalChoiceTradeDict setObject:@"N" forKey:@"button10"];
    [delegate.finalChoiceTradeDict setObject:@"N" forKey:@"button11"];
   // CGRectMake(75, 155, 170, 170)
    
  

    
  
    
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    if([delegate.tickCheck isEqualToString:@"tick"])
    {
    self.checkButton.selected=YES;
    }else
    {
        self.checkButton.selected=NO;
        delegate.tickCheck=@"";
    }
}

-(void)onButtonTap:(UIButton*)sender
{
    myButton = (UIButton*)sender;
    if(self.dataArray.count>0)
    {
        [self.dataArray removeAllObjects];
    }
   
    
    
    coordinates = sender.frame.origin;
    if(sender == self.checkButton||sender==self.submitButton||sender==self.signatureButton||sender==self.idProofButton||sender==self.bankStatementButton)
    {
        
    }else
    {
        self.doneView.hidden=NO;
        self.pickerView.hidden=NO;
        self.pickerView.delegate=self;
        self.pickerView.dataSource=self;
        self.pickerView.backgroundColor=[UIColor lightGrayColor];
        UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(onPickerDoneButtonTap)];
       // toolbar= [[UIToolbar alloc]initWithFrame:CGRectMake(0,0,self.view.frame.size.width,50)];
        toolbar= [[UIToolbar alloc]initWithFrame:CGRectMake(0,0,self.doneView.frame.size.width,50)];
        NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton,nil];
        [toolbar setItems:toolbarItems];
        [self.doneView addSubview:toolbar];
    //CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
    //[self.scrollView setContentOffset:bottomOffset animated:YES];
    }
    if(sender == self.checkButton)
    {
        self.pickerView.hidden=YES;
        [toolbar removeFromSuperview];
        if(sender.selected==YES)
        {
            self.checkButton.selected=NO;
            delegate.tickCheck=@"";
        }else if(sender.selected==NO)
        {
            self.checkButton.selected=YES;
            delegate.tickCheck=@"tick";
        }
    }else if (sender == self.clickHereButton)
    {
        self.pickerView.hidden=YES;
        [toolbar removeFromSuperview];
        PolicyViewController * policy = [self.storyboard instantiateViewControllerWithIdentifier:@"PolicyViewController"];
        [self.navigationController pushViewController:policy animated:YES];
    }else if (sender == self.incomeSourceButton)
    {
        [self.dataArray addObject:@"Job"];
        [self.dataArray addObject:@"Personal Savings"];
        [self.dataArray addObject:@"Spouse/Parent"];
        [self.dataArray addObject:@"Social Security/Pension"];
        [self.dataArray addObject:@"Inheritance"];
        [self.dataArray addObject:@"Investment"];
        [self.dataArray addObject:@"Retirement Plan"];
        [self.dataArray addObject:@"Real estate investment"];
        [self.dataArray addObject:@"Stock retirement annuity"];
        [self.dataArray addObject:@"Family wealth"];
        [self.dataArray addObject:@"Passive income"];
        [self.dataArray addObject:@"Royalties"];
        [self.dataArray addObject:@"Property Income"];
        [self.dataArray addObject:@"Portfolio income"];
        [self.dataArray addObject:@"Other"];
    }else if (sender == self.annualImcomeSourceButton)
    {
        [self.dataArray addObject:@"$0 - $24,999"];
        [self.dataArray addObject:@"$25,000 - $74,999"];
        [self.dataArray addObject:@"$75,000 - $199,999"];
        [self.dataArray addObject:@"$200,000 - $499,999"];
        [self.dataArray addObject:@"$500,000 - $999,999"];
        [self.dataArray addObject:@"Over $1,000,000"];
    }else if (sender == self.netWorthButton)
    {
        [self.dataArray addObject:@"$0 - $49,999"];
        [self.dataArray addObject:@"$50,000 - $149,999"];
        [self.dataArray addObject:@"$150,000 - $499,999"];
        [self.dataArray addObject:@"$500,000 - $999,999"];
        [self.dataArray addObject:@"$1,000,000 - $4,999,999"];
        [self.dataArray addObject:@"Over $5,000,000"];
    }else if (sender== self.liquidAssetsButton)
    {
        [self.dataArray addObject:@"$0 - $19,999"];
        [self.dataArray addObject:@"$20,000 - $49,999"];
        [self.dataArray addObject:@"$50,000 - $149,999"];
        [self.dataArray addObject:@"$150,000 - $499,999"];
        [self.dataArray addObject:@"$500,000 - $999,999"];
        [self.dataArray addObject:@"Over $1,000,000"];
    }else if (sender == self.taxButton)
    {
        [self.dataArray addObject:@"Unknown"];
        [self.dataArray addObject:@"10%"];
        [self.dataArray addObject:@"15%"];
        [self.dataArray addObject:@"27%"];
        [self.dataArray addObject:@"30%"];
        [self.dataArray addObject:@"35%"];
        [self.dataArray addObject:@"38%"];
    }else if (sender == self.sourceOfWealthButton)
    {
        [self.dataArray addObject:@"Personal Income/Savings"];
        [self.dataArray addObject:@"Inheritance"];
        [self.dataArray addObject:@"Sale of Business"];
        
    }else if (sender == self.profileButton)
    {
        [self.dataArray addObject:@"Cash"];
        [self.dataArray addObject:@"Margin"];
       
    }else if (sender == self.dayTradingButton)
    {
        [self.dataArray addObject:@"NO"];
        [self.dataArray addObject:@"YES"];
    }else if (sender == self.activeTraderButton)
    {
        [self.dataArray addObject:@"NO"];
        [self.dataArray addObject:@"YES"];
    }else if (sender == self.financialButton)
    {
        [self.dataArray addObject:@"NO"];
        [self.dataArray addObject:@"YES"];
    }else if (sender == self.objectiveButto)
    {
        [self.dataArray addObject:@"Income"];
        [self.dataArray addObject:@"Capital Appreciation"];
        [self.dataArray addObject:@"Speculation"];
    }else if (sender == self.stockExpButton)
    {
        [self.dataArray addObject:@"0"];
        [self.dataArray addObject:@"1-2"];
        [self.dataArray addObject:@"3-5"];
        [self.dataArray addObject:@"6-10"];
        [self.dataArray addObject:@"10+"];
    }else if (sender == self.avgStockSize)
    {
        [self.dataArray addObject:@"0"];
        [self.dataArray addObject:@"100-500"];
        [self.dataArray addObject:@"501-1000"];
        [self.dataArray addObject:@"1001-5000"];
        [self.dataArray addObject:@"5000+"];
    }else if (sender == self.transactionsStockButton)
    {
        [self.dataArray addObject:@"0"];
        [self.dataArray addObject:@"1-50"];
        [self.dataArray addObject:@"51-100"];
        [self.dataArray addObject:@"101-500"];
        [self.dataArray addObject:@"501+"];
    }else if (sender == self.optionsExpButton)
    {
        [self.dataArray addObject:@"0"];
        [self.dataArray addObject:@"1-2"];
        [self.dataArray addObject:@"3-5"];
        [self.dataArray addObject:@"6-10"];
        [self.dataArray addObject:@"10+"];
    }else if (sender == self.optionsSizeButton)
    {
        [self.dataArray addObject:@"0"];
        [self.dataArray addObject:@"1-10"];
        [self.dataArray addObject:@"11-20"];
        [self.dataArray addObject:@"21-100"];
        [self.dataArray addObject:@"101+"];
    }else if (sender == self.transactionsOptionsButton)
    {
        [self.dataArray addObject:@"0"];
        [self.dataArray addObject:@"1-50"];
        [self.dataArray addObject:@"51-100"];
        [self.dataArray addObject:@"101-500"];
        [self.dataArray addObject:@"501+"];
    }else if (sender == self.bondsExpButton)
    {
        [self.dataArray addObject:@"0"];
        [self.dataArray addObject:@"1-2"];
        [self.dataArray addObject:@"3-5"];
        [self.dataArray addObject:@"6-10"];
        [self.dataArray addObject:@"10+"];
    }else if (sender == self.bondsSizeButton)
    {
        [self.dataArray addObject:@"0"];
        [self.dataArray addObject:@"1-10"];
        [self.dataArray addObject:@"11-20"];
        [self.dataArray addObject:@"21-100"];
        [self.dataArray addObject:@"101+"];
    }else if (sender == self.bondsTransactionsButton)
    {
        [self.dataArray addObject:@"0"];
        [self.dataArray addObject:@"1-50"];
        [self.dataArray addObject:@"51-100"];
        [self.dataArray addObject:@"101-500"];
        [self.dataArray addObject:@"501+"];
    }else if (sender == self.mfExpButton)
    {
        [self.dataArray addObject:@"0"];
        [self.dataArray addObject:@"1-2"];
        [self.dataArray addObject:@"3-5"];
        [self.dataArray addObject:@"6-10"];
        [self.dataArray addObject:@"10+"];
    }else if (sender == self.mfAvgSize)
    {
        [self.dataArray addObject:@"0"];
        [self.dataArray addObject:@"1-10"];
        [self.dataArray addObject:@"11-20"];
        [self.dataArray addObject:@"21-100"];
        [self.dataArray addObject:@"101+"];
    }else if (sender == self.mfTransactionsButton)
    {
        [self.dataArray addObject:@"0"];
        [self.dataArray addObject:@"1-50"];
        [self.dataArray addObject:@"51-100"];
        [self.dataArray addObject:@"101-500"];
        [self.dataArray addObject:@"501+"];
    }else if (sender == self.optionsTradingLevelButton)
    {
        [self.dataArray addObject:@"No Options"];
        [self.dataArray addObject:@"Write covered calls"];
        [self.dataArray addObject:@"Level1 + Buy Puts and Calls"];
        [self.dataArray addObject:@"Level2 + Debit Spreads"];
        [self.dataArray addObject:@"Level3 + Credit Spreads"];
        [self.dataArray addObject:@"Level4 + Uncovered Equity Options"];
    }else if (sender == self.TLButtonOne)
    {
        [self.dataArray addObject:@"NO"];
        [self.dataArray addObject:@"YES"];
    }else if (sender == self.TLButtonTwo)
    {
        [self.dataArray addObject:@"NO"];
        [self.dataArray addObject:@"YES"];
    }else if (sender == self.TLButtonThree)
    {
        [self.dataArray addObject:@"NO"];
        [self.dataArray addObject:@"YES"];
    }else if (sender == self.TLButtonFour)
    {
        [self.dataArray addObject:@"NO"];
        [self.dataArray addObject:@"YES"];
    }else if (sender == self.TLButtonFive)
    {
        [self.dataArray addObject:@"NO"];
        [self.dataArray addObject:@"YES"];
    }else if (sender == self.TLButtonSix)
    {
        [self.dataArray addObject:@"NO"];
        [self.dataArray addObject:@"YES"];
    }else if (sender == self.TLButtonSeven)
    {
        [self.dataArray addObject:@"NO"];
        [self.dataArray addObject:@"YES"];
    }else if (sender == self.TLButtonSeven)
    {
        [self.dataArray addObject:@"NO"];
        [self.dataArray addObject:@"YES"];
    }else if (sender == self.TLButtonEight)
    {
        [self.dataArray addObject:@"NO"];
        [self.dataArray addObject:@"YES"];
    }else if (sender == self.TLButtonNine)
    {
        [self.dataArray addObject:@"NO"];
        [self.dataArray addObject:@"YES"];
    }else if (sender == self.TLButtonTen)
    {
        [self.dataArray addObject:@"NO"];
        [self.dataArray addObject:@"YES"];
    }else if (sender == self.TLButtonEleven)
    {
        [self.dataArray addObject:@"NO"];
        [self.dataArray addObject:@"YES"];
    }else if (sender == self.signatureButton)
    {
        if([self.signatureButton.titleLabel.text containsString:@"uploaded"])
        {
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Signature uploaded already." preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:^{
                
            }];
            
            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            }];
            
            [alert addAction:okAction];
        }else
        {
        [self cameraInvokeMethod];
        }
        
    }else if (sender == self.idProofButton)
    {
        [self.signatureTF resignFirstResponder];
        if([self.idProofButton.titleLabel.text containsString:@"Uploaded"])
        {
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"ID Proof uploaded already." preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:^{
                
            }];
            
            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            }];
            
            [alert addAction:okAction];
        }else
        {
        NSString * message = [NSString stringWithFormat:@"You must submit the following required document(s). To avoid delays with your application, we prefer that you scan and upload your documents in PDF or image (jpg/img) format below.Once a new account number is issued to you, you will receive your trading log in ID and password in a separate email"];
        
        NSString * message1 = [NSString stringWithFormat:@"Non-residents: Please submit a copy of your passport. Only if you do not have a passport, please submit a government-issued national identity card or driver's license. Please also send the first page of your latest bank statement (the same bank from which you will be wiring funds into your ChoiceTrade account). The address on your bank statement must match the address you entered on your account application. If it does not, then please also submit a current utility bill such as a gas, electric or water bill that contains your name and current address. Please note that if you send money into your ChoiceTrade account at any time in the future from a bank account that is not the same one as the bank statement you sent us, your funds may be returned to you and you will incur bank and other charges. So please only send funds from the bank account for which you have provided us a statement.Please do not send expired IDs as we will not be able to accept them."];
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Please Read the below instruction carefully." message:message preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Next" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Please Read the below instruction carefully." message:message1 preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:^{
                
            }];
            
            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self cameraInvokeMethod];
            }];
            
            [alert addAction:okAction];
            
        }];
        
        [alert addAction:okAction];
        }
        
    }else if (sender == self.bankStatementButton)
    {
        [self.signatureTF resignFirstResponder];
        NSString * message = [NSString stringWithFormat:@"You must submit the following required document(s). To avoid delays with your application, we prefer that you scan and upload your documents in PDF or image (jpg/img) format below.Once a new account number is issued to you, you will receive your trading log in ID and password in a separate email"];
        
        NSString * message1 = [NSString stringWithFormat:@"Non-residents: Please submit a copy of your passport. Only if you do not have a passport, please submit a government-issued national identity card or driver's license. Please also send the first page of your latest bank statement (the same bank from which you will be wiring funds into your ChoiceTrade account). The address on your bank statement must match the address you entered on your account application. If it does not, then please also submit a current utility bill such as a gas, electric or water bill that contains your name and current address. Please note that if you send money into your ChoiceTrade account at any time in the future from a bank account that is not the same one as the bank statement you sent us, your funds may be returned to you and you will incur bank and other charges. So please only send funds from the bank account for which you have provided us a statement.Please do not send expired IDs as we will not be able to accept them."];
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Please Read the below instruction carefully." message:message preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Next" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Please Read the below instruction carefully." message:message1 preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:^{
                
            }];
            
            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self pdfMethod];
            }];
            
            [alert addAction:okAction];
            
        }];
        
        [alert addAction:okAction];
        
        //[self cameraInvokeMethod];
        //[self pdfMethod];
    }
    else if (sender == self.submitButton)
    {
        [self.signatureTF resignFirstResponder];
        self.pickerView.hidden=YES;
        [toolbar removeFromSuperview];
        [self parametersMethod];
//         [self choiceTradeServer];
        //  [self choiceTradeServerStepThree];
        //[self choiceTradeStepFour];
       // [self choiceTradeStepFive];
      //  [self choiceTradeStepSeven];
        if([delegate.tickCheck isEqualToString:@"tick"])
        {
            if([photoCheck isEqualToString:@"Done"]&&[bankStatementCheck isEqualToString:@"Done"]&&[signatureCheck isEqualToString:@"Done"])
            {
                [self.checkButton setSelected:YES];
                [self choiceTradeServer];
                // [self choiceTradeStepSeven];
                // [self choiceTradeAccountRetrieve];
            }else
            {
            NSString * errorText;
            if([bankStatementCheck isEqualToString:@""]||[bankStatementCheck isEqual:[NSNull null]]||bankStatementCheck.length==0)
            {
                errorText = @"Please upload Bank Statement";
            }
                if([photoCheck isEqualToString:@""]||[photoCheck isEqual:[NSNull null]]||photoCheck.length==0)
                {
                    errorText = @"Please upload any ID proof";
                }
                if([signatureCheck isEqualToString:@""]||[signatureCheck isEqual:[NSNull null]]||signatureCheck.length==0)
                {
                    errorText =@"Please upload signature";
                }
           
//            if(errorText.length==0)
//            {
//                errorText =@"Something went wrong. Please try again. Also make sure you have uploaded required documents.";
//            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [activityIndicatorView stopAnimating];
                [backView removeFromSuperview];
            });
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:errorText preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:^{
                
            }];
            
            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [activityIndicatorView stopAnimating];
                [backView removeFromSuperview];
                
            }];
            
            [alert addAction:okAction];
            }
            
        }else if(self.signatureTF.text.length==0)
        {
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:@"Please fill signature name." preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:^{
                
            }];
            
            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [activityIndicatorView stopAnimating];
                [backView removeFromSuperview];
            }];
            
            [alert addAction:okAction];
        }else
        {
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:@"You need to Accept Terms and Conditions before you submit." preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:^{
                
            }];
            
            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.checkButton setSelected:YES];
                delegate.tickCheck=@"tick";
                [activityIndicatorView stopAnimating];
                [backView removeFromSuperview];
            }];
            
            [alert addAction:okAction];
        }
        
    }
}

-(void)onPickerDoneButtonTap
{
    if([myButton isEqual:self.sourceOfWealthButton])
    {
       
    }
    if([myButton isEqual:@"YES"])
    {
        self.yesString=@"Y";
    }else if([myButton isEqual:@"NO"])
    {
        self.yesString=@"N";
    }
    self.doneView.hidden=YES;
    self.pickerView.hidden=YES;
    [toolbar removeFromSuperview];
    //[self.scrollView setContentOffset:CGPointMake(0, myButton.frame.origin.y-100) animated:YES];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView
numberOfRowsInComponent:(NSInteger)component {
    return self.dataArray.count;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [NSString stringWithFormat:@"%@",[self.dataArray objectAtIndex:row]];//Or, your suitable title; like Choice-a, etc.
}

- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    
    [myButton setTitle:[self.dataArray objectAtIndex:row] forState:UIControlStateNormal];
   checkString = [NSString stringWithFormat:@"%@",myButton.titleLabel.text];
    if([myButton isEqual:self.optionsTradingLevelButton])
    {
    if([checkString isEqualToString:@"No Options"])
    {
        self.optionTradingView.hidden=YES;
        self.optionViewHeightConstraint.constant=0;
        self.foreignViewHeightConstraint.constant=1700;
    }else
    {
        self.optionTradingView.hidden=NO;
        self.optionViewHeightConstraint.constant=1150;
        self.foreignViewHeightConstraint.constant=2900;
    }
    }
    
    if([myButton isEqual:self.incomeSourceButton])
    {
        if([checkString isEqualToString:@"Other"])
        {
            self.otherIncomeView.hidden=NO;
            self.otherIncomeViewHeightConstraint.constant=60;
            self.otherIncomeTopConstraint.constant=41.5;
            
        }else
        {
            self.otherIncomeView.hidden=YES;
            self.otherIncomeViewHeightConstraint.constant=0;
            self.otherIncomeTopConstraint.constant=0;
        }
    }
    
    if([myButton isEqual:self.annualImcomeSourceButton]||[myButton isEqual:self.liquidAssetsButton]||[myButton isEqual:self.netWorthButton])
    {
        if(row==0)
        {
            self.alphabetString=@"A";
        }else if (row ==1)
        {
            self.alphabetString=@"B";
        }else if (row ==2)
        {
            self.alphabetString=@"C";
        }else if (row ==3)
        {
            self.alphabetString=@"D";
        }else if (row ==4)
        {
            self.alphabetString=@"E";
        }else if (row ==5)
        {
            self.alphabetString=@"F";
        }
        if([myButton isEqual:self.annualImcomeSourceButton])
        {
            [delegate.finalChoiceTradeDict setObject:self.alphabetString forKey:@"annualincome"];
        }else if ([myButton isEqual:self.liquidAssetsButton])
        {
            [delegate.finalChoiceTradeDict setObject:self.alphabetString forKey:@"liquid"];
        }else if ([myButton isEqual:self.netWorthButton])
        {
            [delegate.finalChoiceTradeDict setObject:self.alphabetString forKey:@"networth"];
        }
    }
    
    if([myButton isEqual:self.sourceOfWealthButton])
    {
        if(row ==0)
        {
             [delegate.finalChoiceTradeDict setObject:@"Personal" forKey:@"wealth"];
        }else if (row ==1)
        {
             [delegate.finalChoiceTradeDict setObject:@"Inheritance" forKey:@"wealth"];
        }else if (row ==2)
        {
            [delegate.finalChoiceTradeDict setObject:@"BusinessSale" forKey:@"wealth"];
        }
    }
}

-(void)parametersMethod
{
    [delegate.finalChoiceTradeDict setObject:self.incomeSourceButton.titleLabel.text forKey:@"income"];
    [delegate.finalChoiceTradeDict setObject:self.otherIncomeTF.text forKey:@"otherincome"];
//    [delegate.finalChoiceTradeDict setObject:self.annualImcomeSourceButton.titleLabel.text forKey:@"annualincome"];
//    [delegate.finalChoiceTradeDict setObject:self.netWorthButton.titleLabel.text forKey:@"networth"];
//    [delegate.finalChoiceTradeDict setObject:self.liquidAssetsButton.titleLabel.text forKey:@"liquid"];
    [delegate.finalChoiceTradeDict setObject:self.taxButton.titleLabel.text forKey:@"tax"];
   // [delegate.finalChoiceTradeDict setObject:self.sourceOfWealthButton.titleLabel.text forKey:@"wealth"];
    
    if([self.profileButton.titleLabel.text isEqualToString:@"Cash"])
    {
         [delegate.finalChoiceTradeDict setObject:@"cashAcc" forKey:@"accountype"];
    }else if([self.profileButton.titleLabel.text isEqualToString:@"Margin"])
    {
        [delegate.finalChoiceTradeDict setObject:@"marginAcc" forKey:@"accounttype"];
    }
   
    [delegate.finalChoiceTradeDict setObject:self.yesString forKey:@"daytrade"];
    [delegate.finalChoiceTradeDict setObject:self.yesString forKey:@"activetrader"];
    [delegate.finalChoiceTradeDict setObject:self.yesString forKey:@"financial"];
    
    
    [self.dataArray addObject:@"Income"];
    [self.dataArray addObject:@"Capital Appreciation"];
    [self.dataArray addObject:@"Speculation"];
    
    if([self.objectiveButto.titleLabel.text isEqualToString:@"Income"])
    {
        self.objString =@"B";
    }else if ([self.objectiveButto.titleLabel.text isEqualToString:@"Capital Appreciation"])
    {
        self.objString=@"C";
    }else if ([self.objectiveButto.titleLabel.text isEqualToString:@"Speculation"])
    {
        self.objString=@"D";
    }
    
    [delegate.finalChoiceTradeDict setObject:self.objString forKey:@"objective"];
    [delegate.finalChoiceTradeDict setObject:self.stockExpButton.titleLabel.text forKey:@"stockexp"];
    [delegate.finalChoiceTradeDict setObject:self.avgStockSize.titleLabel.text forKey:@"stocksize"];
    [delegate.finalChoiceTradeDict setObject:self.transactionsStockButton.titleLabel.text forKey:@"stocktrans"];
    [delegate.finalChoiceTradeDict setObject:self.optionsExpButton.titleLabel.text forKey:@"optionsexp"];
    [delegate.finalChoiceTradeDict setObject:self.optionsSizeButton.titleLabel.text forKey:@"optionssize"];
    [delegate.finalChoiceTradeDict setObject:self.transactionsOptionsButton.titleLabel.text forKey:@"optionstrans"];
    [delegate.finalChoiceTradeDict setObject:self.bondsExpButton.titleLabel.text forKey:@"bondsexp"];
    [delegate.finalChoiceTradeDict setObject:self.bondsSizeButton.titleLabel.text forKey:@"bondssize"];
    [delegate.finalChoiceTradeDict setObject:self.bondsTransactionsButton.titleLabel.text forKey:@"bondstrans"];
    [delegate.finalChoiceTradeDict setObject:self.mfExpButton.titleLabel.text forKey:@"mfexp"];
    [delegate.finalChoiceTradeDict setObject:self.mfAvgSize.titleLabel.text forKey:@"mfavgsize"];
    [delegate.finalChoiceTradeDict setObject:self.mfTransactionsButton.titleLabel.text forKey:@"mftrans"];
//    "level0 = No Options
//    level1 = Write covered calls
//    level2 = Level1 + Buy Puts and Calls
//    level3 = Level2 + Debit Spreads
//    level4 = Level3 + Credit Spreads
//    level5 = Level4 + Uncovered Equity Options"
    if([self.optionsTradingLevelButton.titleLabel.text isEqualToString:@"No Options"])
    {
        self.optionLevel=@"level0";
    }else if ([self.optionsTradingLevelButton.titleLabel.text isEqualToString:@"Write covered calls"])
    {
        self.optionLevel=@"level1";
    }else if ([self.optionsTradingLevelButton.titleLabel.text isEqualToString:@"Level1 + Buy Puts and Calls"])
    {
        self.optionLevel=@"level2";
    }else if ([self.optionsTradingLevelButton.titleLabel.text isEqualToString:@"Level2 + Debit Spreads"])
    {
        self.optionLevel=@"level3";
    }else if ([self.optionsTradingLevelButton.titleLabel.text isEqualToString:@"Level3 + Credit Spreads"])
    {
        self.optionLevel=@"level4";
    }else if ([self.optionsTradingLevelButton.titleLabel.text isEqualToString:@"Level4 + Uncovered Equity Options"])
    {
        self.optionLevel=@"level5";
    }
    
    [delegate.finalChoiceTradeDict setObject:self.optionLevel forKey:@"optionslevel"];
    [delegate.finalChoiceTradeDict setObject:self.yesString forKey:@"button1"];
     [delegate.finalChoiceTradeDict setObject:self.yesString forKey:@"button2"];
     [delegate.finalChoiceTradeDict setObject:self.yesString forKey:@"button3"];
     [delegate.finalChoiceTradeDict setObject:self.yesString forKey:@"button4"];
     [delegate.finalChoiceTradeDict setObject:self.yesString forKey:@"button5"];
     [delegate.finalChoiceTradeDict setObject:self.yesString forKey:@"button6"];
     [delegate.finalChoiceTradeDict setObject:self.yesString forKey:@"button8"];
     [delegate.finalChoiceTradeDict setObject:self.yesString forKey:@"button9"];
     [delegate.finalChoiceTradeDict setObject:self.yesString forKey:@"button10"];
     [delegate.finalChoiceTradeDict setObject:self.yesString forKey:@"button11"];

    
    
}

-(void)choiceTradeServer
{
    //    [postData appendData:[@"&OwnerDisclosure=N" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&EmployStatus=Employed" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&Occupation=Software Engineer" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&YearsEmployed=7" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&CompanyName=Zenwise Technologies Private Limited." dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&TypeBusiness=IT" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&BusinessAddress=Level 6, Pentagon 2, Magarpatta city." dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&BusinessCity=Pune" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&BusinessZip=411013" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&BusinessState=Maharastra" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&BusinessCountry=IND" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&BusinessPhone=02040147878" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&PrimaryIncomeSrc=Job" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AnnualIncome=A" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&LiquidAssets=A" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&NetWorth=A" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&TaxBracket=10" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&SourceWealth=Personal" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AffiliationMember=N" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AffiliationMemberFirm=N" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AffiliationMemberName=N" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AffiliationMemberPosition=N" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AffiliationBroker=N" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AffiliationBrokerFirm=N" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AffiliationBrokerName=N" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AffiliationBrokerPosition=N" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AffiliationForShellBank=N" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AffiliationForShellBankAgent=N" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AffiliationShellBank=N" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AffiliationShellBankService=N" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AffiliationOtherFunds=N" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AffiliationFFI=N" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AffiliationPoliOfficial=N" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AffiliationBankrupt=N" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AffiliationLegal=N" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AffiliationUnpaid=N" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AffiliationPoliOfficialName=N" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AffiliationPoliOfficialOrg=N" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AccountTypeInvest=cashAcc" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&DayTrade=N" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&SweepCash=N" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&FundInvest=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&TradeOTC=N" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&ActiveTrader=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AccountTransfer=N" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&InvestmentObj=B" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&StockYearsExp=1-2" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&StockAvgSize=100-500" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&StockAvgTrans=1-50" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&OptionYears=0" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&OptionSize=0" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&OptionAvgTrans=0" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&BondYears=0" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&BondSize=0" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&BondAvgTrans=0" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&FundYears=1-2" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&FundSize=1-10" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&FundAvgTrans=1-50" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&OptionTradeLevel=level0" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&NonProNYSE=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&NonProNasdaq=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&NonProOpra=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&NonProPink=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AgreeNyse=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AgreeNasdaq=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AgreeOpra=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AgreePink=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AgreeCust=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AgreeMargin=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AgreeOption=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AgreeJoint=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AgreePrivacy=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AgreeEConfirm=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AgreeEstatement=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AgreeEProxy=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AgreeEProspectus=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AgreeETax=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AgreeEReorg=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&AgreeForeignFinder=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&SignatureName=Parag Bhosale" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&SignatureIP=192.168.0.1" dataUsingEncoding:NSUTF8StringEncoding]];
    
   
    //[activityView startAnimating];
   // loadingLabel.text =@"Submitting Details of step2";
    
    backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.view addSubview:backView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeFiveDots tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [backView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    
     apiKey = [NSString stringWithFormat:@"Bearer %@",delegate.apiKey];
    NSMutableData *postData;
        NSDictionary *headers = @{ @"Content-Type": @"application/x-www-form-urlencoded",
                                   @"Authorization": apiKey,
                                   };
        
        NSString * empStatus = [NSString stringWithFormat:@"EmployStatus=%@",[delegate.finalChoiceTradeDict objectForKey:@"empstatus"]];
        if([empStatus containsString:@"Unemployed"])
        {
            NSString * previousEmployer = [NSString stringWithFormat:@"&UnemployedPrevEmployer=%@",[delegate.finalChoiceTradeDict objectForKey:@"previousemployer"]];
            
            NSString * typeOfBussiness = [NSString stringWithFormat:@"&UnemployedBusinessType=%@",[delegate.finalChoiceTradeDict objectForKey:@"typeofbussiness"]];
            
            NSString * previousEmployment = [NSString stringWithFormat:@"&UnemployedOccupation=%@",[delegate.finalChoiceTradeDict objectForKey:@"occupationofprevoiusemployemnt"]];
            
            NSString * yearsEmployed = [NSString stringWithFormat:@"&UnemployedYears=%@",[delegate.finalChoiceTradeDict objectForKey:@"yearsemployed"]];
            
            NSString * lastDateEmployed = [NSString stringWithFormat:@"&UnemployedLastDate=%@",[delegate.finalChoiceTradeDict objectForKey:@"lastdateemployed"]];
            
            NSString * incomeSource = [NSString stringWithFormat:@"&PrimaryIncomeSrc=%@",[delegate.finalChoiceTradeDict objectForKey:@"income"]];
            
            NSString * incomeSourceOther = [NSString stringWithFormat:@"&PrimaryIncomeSrcOther=%@",[delegate.finalChoiceTradeDict objectForKey:@"otherincome"]];
            
            NSString * annualIncome = [NSString stringWithFormat:@"&AnnualIncome=%@",[delegate.finalChoiceTradeDict objectForKey:@"annualincome"]];
            
            NSString * networth = [NSString stringWithFormat:@"&LiquidAssets=%@",[delegate.finalChoiceTradeDict objectForKey:@"networth"]];
            
            NSString * liquidAssets = [NSString stringWithFormat:@"&NetWorth=%@",[delegate.finalChoiceTradeDict objectForKey:@"liquid"]];
            
            NSString * tax = [NSString stringWithFormat:@"&TaxBracket=%@",[delegate.finalChoiceTradeDict objectForKey:@"tax"]];
            
            NSString * wealth = [NSString stringWithFormat:@"&SourceWealth=%@",[delegate.finalChoiceTradeDict objectForKey:@"wealth"]];
            
            postData = [[NSMutableData alloc] initWithData:[empStatus dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[previousEmployer dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[typeOfBussiness dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[previousEmployment dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[yearsEmployed dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[lastDateEmployed dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[incomeSource dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[incomeSourceOther dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[annualIncome dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[networth dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[liquidAssets dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[tax dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[wealth dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[@"&Step=2" dataUsingEncoding:NSUTF8StringEncoding]];
        }
    else
    {
        NSString * occupation = [NSString stringWithFormat:@"&Occupation=%@",[delegate.finalChoiceTradeDict objectForKey:@"occupation"]];
        
        NSString * yearsemp = [NSString stringWithFormat:@"&YearsEmployed=%@",[delegate.finalChoiceTradeDict objectForKey:@"yearsemp"]];
        
        NSString * companyname = [NSString stringWithFormat:@"&CompanyName=%@",[delegate.finalChoiceTradeDict objectForKey:@"companyname"]];
        
        NSString * busineestype = [NSString stringWithFormat:@"&TypeBusiness=%@",[delegate.finalChoiceTradeDict objectForKey:@"busineestype"]];
        
        NSString * businessaddress = [NSString stringWithFormat:@"&BusinessAddress=%@",[delegate.finalChoiceTradeDict objectForKey:@"businessaddress"]];
        
        NSString * businessCity = [NSString stringWithFormat:@"&BusinessCity=%@",[delegate.finalChoiceTradeDict objectForKey:@"businessCity"]];
        
        
        NSString * businesspostal = [NSString stringWithFormat:@"&BusinessZip=%@",[delegate.finalChoiceTradeDict objectForKey:@"businesspostal"]];
        
        NSString * businessstate = [NSString stringWithFormat:@"&BusinessState=%@",[delegate.finalChoiceTradeDict objectForKey:@"businessstate"]];
    
        businessstate=@"&BusinessState=AE";
        
        NSString * businesscountry = [NSString stringWithFormat:@"&BusinessCountry=%@",[delegate.finalChoiceTradeDict objectForKey:@"businesscountry"]];
    
      //  businesscountry =@"&BusinessCountry=IND";
        
        NSString * businessmobile = [NSString stringWithFormat:@"&BusinessPhone=%@",[delegate.finalChoiceTradeDict objectForKey:@"businessmobile"]];
        
        NSString * incomeSource = [NSString stringWithFormat:@"&PrimaryIncomeSrc=%@",[delegate.finalChoiceTradeDict objectForKey:@"income"]];
        
        NSString * incomeSourceOther = [NSString stringWithFormat:@"&PrimaryIncomeSrcOther=%@",[delegate.finalChoiceTradeDict objectForKey:@"otherincome"]];
        
        NSString * annualIncome = [NSString stringWithFormat:@"&AnnualIncome=%@",[delegate.finalChoiceTradeDict objectForKey:@"annualincome"]];
        
        NSString * networth = [NSString stringWithFormat:@"&LiquidAssets=%@",[delegate.finalChoiceTradeDict objectForKey:@"networth"]];
        
        NSString * liquidAssets = [NSString stringWithFormat:@"&NetWorth=%@",[delegate.finalChoiceTradeDict objectForKey:@"liquid"]];
        
        NSString * tax = [NSString stringWithFormat:@"&TaxBracket=%@",[delegate.finalChoiceTradeDict objectForKey:@"tax"]];
    
    NSString * wealth = [NSString stringWithFormat:@"&SourceWealth=%@",[delegate.finalChoiceTradeDict objectForKey:@"wealth"]];
    
   // NSString * step = [NSString stringWithFormat:@"&Step=2"];
        
        
        postData = [[NSMutableData alloc] initWithData:[empStatus dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[occupation dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[yearsemp dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[companyname dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[busineestype dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[businessaddress dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[businessCity dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[businesspostal dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[businessstate dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[businesscountry dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[businessmobile dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[incomeSource dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[incomeSourceOther dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[annualIncome dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[networth dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[liquidAssets dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[tax dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[wealth dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[@"&Step=2" dataUsingEncoding:NSUTF8StringEncoding]];
    }
        NSString * url = [NSString stringWithFormat:@"%@api/accounts/step2/%@",delegate.producationURL,delegate.appIDString];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:30.0];
        [request setHTTPMethod:@"PUT"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            
                                                            NSMutableDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            //NSLog(@"%@",responseDict);
                                                            
                                                            if([[responseDict objectForKey:@"status"]isEqualToString:@"200"])
                                                            {
                                                                Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
                                                                [mixpanelMini track:@"usa_open_account_step3_completed_page"];
                                                                [self choiceTradeServerStepThree];
                                                            }else
                                                            {
                                                                NSString * errorText = [NSString stringWithFormat:@"%@.Please check the inputs given again.",[responseDict objectForKey:@"message"]];
                                                                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:errorText preferredStyle:UIAlertControllerStyleAlert];
                                                                
                                                                [self presentViewController:alert animated:YES completion:^{
                                                                    
                                                                }];
                                                                
                                                                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                    [activityIndicatorView stopAnimating];
                                                                    [backView removeFromSuperview];
                                                                }];
                                                                
                                                                [alert addAction:okAction];
                                                            }
                                                            
                                                        }
                                                    }];
        [dataTask resume];
}

-(void)choiceTradeServerStepThree
{
   //  loadingLabel.text =@"Submitting Details of step3";
    NSDictionary *headers = @{ @"Content-Type": @"application/x-www-form-urlencoded",
                               @"Authorization": apiKey,
                               };
    
    NSString * string1 = [NSString stringWithFormat:@"AffiliationMember=N"];
    NSString * string2 = [NSString stringWithFormat:@"&AffiliationMemberFirm=N"];
    NSString * string3 = [NSString stringWithFormat:@"&AffiliationMemberName=N"];
    NSString * string4 = [NSString stringWithFormat:@"&AffiliationMemberPosition=N"];
    NSString * string5 = [NSString stringWithFormat:@"&AffiliationBroker=N"];
    NSString * string6 = [NSString stringWithFormat:@"&AffiliationBrokerFirm=N"];
    NSString * string7 = [NSString stringWithFormat:@"&AffiliationBrokerName=N"];
    NSString * string8 = [NSString stringWithFormat:@"&AffiliationBrokerPosition=N"];
    NSString * string9 = [NSString stringWithFormat:@"&AffiliationForShellBank=N"];
    NSString * string10 = [NSString stringWithFormat:@"&AffiliationForShellBankAgent=N"];
    NSString * string11 = [NSString stringWithFormat:@"&AffiliationShellBank=N"];
    NSString * string12 = [NSString stringWithFormat:@"&AffiliationShellBankService=N"];
    NSString * string13 = [NSString stringWithFormat:@"&AffiliationOtherFunds=N"];
    NSString * string14 = [NSString stringWithFormat:@"&AffiliationFFI=N"];
    NSString * string15 = [NSString stringWithFormat:@"&AffiliationPoliOfficial=N"];
    NSString * string16 = [NSString stringWithFormat:@"&AffiliationBankrupt=N"];
    NSString * string17 = [NSString stringWithFormat:@"&AffiliationLegal=N"];
    NSString * string18 = [NSString stringWithFormat:@"&AffiliationUnpaid=N"];
    NSString * string19 = [NSString stringWithFormat:@"&AffiliationPoliOfficialName=N"];
    NSString * string20 = [NSString stringWithFormat:@"&AffiliationPoliOfficialOrg=N"];
    
    
    // NSString * step = [NSString stringWithFormat:@"&Step=2"];
    
    
    NSMutableData *postData = [[NSMutableData alloc] initWithData:[string1 dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[string2 dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[string3 dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[string4 dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[string5 dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[string6 dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[string7 dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[string8 dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[string9 dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[string10 dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[string11 dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[string12 dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[string13 dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[string14 dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[string15 dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[string16 dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[string17 dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[string18 dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[string19 dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[string20 dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"&Step=3" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString * url = [NSString stringWithFormat:@"%@api/accounts/step3/%@",delegate.producationURL,delegate.appIDString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    [request setHTTPMethod:@"PUT"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        NSMutableDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        //NSLog(@"%@",responseDict);
                                                        
                                                        if([[responseDict objectForKey:@"status"]isEqualToString:@"200"])
                                                        {
                                                            Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
                                                            [mixpanelMini track:@"usa_open_account_step4_completed_page"];
                                                            [self choiceTradeStepFour];
                                                        }else
                                                        {
                                                            NSString * errorText = [NSString stringWithFormat:@"%@.Please check the inputs given again.",[responseDict objectForKey:@"message"]];
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:errorText preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    [activityIndicatorView stopAnimating];
                                                                    [backView removeFromSuperview];
                                                                });
                                                            }];
                                                            
                                                            [alert addAction:okAction];
                                                        }
                                                        
                                                    }
                                                }];
    [dataTask resume];
}

-(void)choiceTradeStepFour
{
  //   loadingLabel.text =@"Submitting Details of step4";
    NSDictionary *headers = @{ @"Content-Type": @"application/x-www-form-urlencoded",
                               @"Authorization": apiKey,
                               };
    
    NSString * AccountTypeInvest = [NSString stringWithFormat:@"AccountTypeInvest=%@",[delegate.finalChoiceTradeDict objectForKey:@"accountype"]];
    
    NSString * DayTrade = [NSString stringWithFormat:@"&DayTrade=%@",[delegate.finalChoiceTradeDict objectForKey:@"daytrade"]];
    
    NSString * TradeOTC = [NSString stringWithFormat:@"&TradeOTC=N"];
    
    NSString * ActiveTrader = [NSString stringWithFormat:@"&ActiveTrader=%@",[delegate.finalChoiceTradeDict objectForKey:@"activetrader"]];
    
    NSString * AccountTransfer = [NSString stringWithFormat:@"&AccountTransfer=%@",[delegate.finalChoiceTradeDict objectForKey:@"financial"]];
    
    NSString * InvestmentObj = [NSString stringWithFormat:@"&InvestmentObj=%@",[delegate.finalChoiceTradeDict objectForKey:@"objective"]];
    
    NSString * StockYearsExp = [NSString stringWithFormat:@"&StockYearsExp=%@",[delegate.finalChoiceTradeDict objectForKey:@"stockexp"]];
    
    
    NSString * StockAvgSize = [NSString stringWithFormat:@"&StockAvgSize=%@",[delegate.finalChoiceTradeDict objectForKey:@"stocksize"]];
    
    NSString * StockAvgTrans = [NSString stringWithFormat:@"&StockAvgTrans=%@",[delegate.finalChoiceTradeDict objectForKey:@"stocktrans"]];
    
    NSString * OptionYears = [NSString stringWithFormat:@"&OptionYears=%@",[delegate.finalChoiceTradeDict objectForKey:@"optionsexp"]];
    
    
    NSString * OptionSize = [NSString stringWithFormat:@"&OptionSize=%@",[delegate.finalChoiceTradeDict objectForKey:@"optionssize"]];
    
    NSString * OptionAvgTrans = [NSString stringWithFormat:@"&OptionAvgTrans=%@",[delegate.finalChoiceTradeDict objectForKey:@"optionstrans"]];
    
    NSString * BondYears = [NSString stringWithFormat:@"&BondYears=%@",[delegate.finalChoiceTradeDict objectForKey:@"bondsexp"]];
    
    NSString * BondSize = [NSString stringWithFormat:@"&BondSize=%@",[delegate.finalChoiceTradeDict objectForKey:@"bondssize"]];
    
    NSString * BondAvgTrans = [NSString stringWithFormat:@"&BondAvgTrans=%@",[delegate.finalChoiceTradeDict objectForKey:@"bondstrans"]];
    
    NSString * FundYears = [NSString stringWithFormat:@"&FundYears=%@",[delegate.finalChoiceTradeDict objectForKey:@"mfexp"]];
    
    NSString * FundSize = [NSString stringWithFormat:@"&FundSize=%@",[delegate.finalChoiceTradeDict objectForKey:@"mfavgsize"]];
    
    NSString * FundAvgTrans = [NSString stringWithFormat:@"&FundAvgTrans=%@",[delegate.finalChoiceTradeDict objectForKey:@"mftrans"]];
    
    NSString * OptionTradeLevel = [NSString stringWithFormat:@"&OptionTradeLevel=%@",[delegate.finalChoiceTradeDict objectForKey:@"optionslevel"]];
    
    NSString * button1,* button2,* button3,* button4,* button5,* button6,* button8,* button9,* button10,*button11;
    NSMutableData *postData;
    if([checkString isEqualToString:@"No Options"])
    {
        
    }else
    {
         button1 = [NSString stringWithFormat:@"&ExpStockOption=%@",[delegate.finalChoiceTradeDict objectForKey:@"button1"]];
         button2 = [NSString stringWithFormat:@"&ExpIndexOption=%@",[delegate.finalChoiceTradeDict objectForKey:@"button2"]];
         button3 = [NSString stringWithFormat:@"&ExCovCallEquity=%@",[delegate.finalChoiceTradeDict objectForKey:@"button3"]];
         button4 = [NSString stringWithFormat:@"&ExCovCallIndex=%@",[delegate.finalChoiceTradeDict objectForKey:@"button4"]];
         button5 = [NSString stringWithFormat:@"&ExSpreadStock=%@",[delegate.finalChoiceTradeDict objectForKey:@"button5"]];
         button6 = [NSString stringWithFormat:@"&ExSpreadIndex=%@",[delegate.finalChoiceTradeDict objectForKey:@"button6"]];
         button8 = [NSString stringWithFormat:@"&ExNakedStock=%@",[delegate.finalChoiceTradeDict objectForKey:@"button8"]];
        button9 = [NSString stringWithFormat:@"&ExNakedIndex=%@",[delegate.finalChoiceTradeDict objectForKey:@"button9"]];
        button10 = [NSString stringWithFormat:@"&ExNoStock=%@",[delegate.finalChoiceTradeDict objectForKey:@"button10"]];
        button11 = [NSString stringWithFormat:@"&ExNoOption=%@",[delegate.finalChoiceTradeDict objectForKey:@"button11"]];
        
    }
    
    
    // NSString * step = [NSString stringWithFormat:@"&Step=2"];
    
    
   postData = [[NSMutableData alloc] initWithData:[AccountTypeInvest dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[DayTrade dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[TradeOTC dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[ActiveTrader dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[AccountTransfer dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[InvestmentObj dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[StockYearsExp dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[StockAvgSize dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[StockAvgTrans dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[OptionYears dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[OptionSize dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[OptionAvgTrans dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[BondYears dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[BondSize dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[BondAvgTrans dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[FundYears dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[FundSize dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[FundAvgTrans dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[OptionTradeLevel dataUsingEncoding:NSUTF8StringEncoding]];
    
    if([checkString isEqualToString:@"No Options"])
    {
        
    }else
    {
        [postData appendData:[button1 dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[button2 dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[button3 dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[button4 dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[button5 dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[button6 dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[button8 dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[button9 dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[button10 dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[button11 dataUsingEncoding:NSUTF8StringEncoding]];
        
    }
    [postData appendData:[@"&Step=4" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString * url = [NSString stringWithFormat:@"%@api/accounts/step4/%@",delegate.producationURL,delegate.appIDString];
    
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    [request setHTTPMethod:@"PUT"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        NSMutableDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        //NSLog(@"%@",responseDict);
                                                        
                                                        if([[responseDict objectForKey:@"status"]isEqualToString:@"200"])
                                                        {
                                                            Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
                                                            [mixpanelMini track:@"usa_open_account_step5_completed_page"];
                                                            [self choiceTradeStepFive];
                                                        }else
                                                        {
                                                            NSString * errorText = [NSString stringWithFormat:@"%@.Please check the inputs given again.",[responseDict objectForKey:@"message"]];
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:errorText preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    [activityIndicatorView stopAnimating];
                                                                    [backView removeFromSuperview];
                                                                });
                                                            }];
                                                            
                                                            [alert addAction:okAction];
                                                        }
                                                        
                                                    }
                                                }];
    [dataTask resume];
}

-(void)choiceTradeStepFive
{
   //  loadingLabel.text =@"Submitting Details of step5";
    NSDictionary *headers = @{ @"Content-Type": @"application/x-www-form-urlencoded",
                               @"Authorization": apiKey,
                               };
    
   
    
    
    // NSString * step = [NSString stringWithFormat:@"&Step=2"];
    
    
    NSMutableData * postData = [[NSMutableData alloc] initWithData:[@"NonProNYSE=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"&NonProNasdaq=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"&NonProOpra=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"&NonProPink=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"&AgreeNyse=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"&AgreeNasdaq=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"&AgreeOpra=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"&AgreePink=Y" dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"&Step=5" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
   
    
    NSString * url = [NSString stringWithFormat:@"%@api/accounts/step5/%@",delegate.producationURL,delegate.appIDString];
    
   
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    [request setHTTPMethod:@"PUT"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        NSMutableDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        //NSLog(@"%@",responseDict);
                                                        
                                                        if([[responseDict objectForKey:@"status"]isEqualToString:@"200"])
                                                        {
                                                            Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
                                                            [mixpanelMini track:@"usa_open_account_step6_completed_page"];
                                                            [self choiceTradeStepSix];
                                                        }else
                                                        {
                                                            NSString * errorText = [NSString stringWithFormat:@"%@.Please check the inputs given again.",[responseDict objectForKey:@"message"]];
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:errorText preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    [activityIndicatorView stopAnimating];
                                                                    [backView removeFromSuperview];
                                                                });
                                                            }];
                                                            
                                                            [alert addAction:okAction];
                                                        }
                                                        
                                                    }
                                                }];
    [dataTask resume];
}

-(void)choiceTradeStepSix
{
 //    loadingLabel.text =@"Submitting Details of step6";
    NSDictionary *headers = @{ @"Content-Type": @"application/x-www-form-urlencoded",
                               @"Authorization":apiKey,
                               };
    
    
    
    
    // NSString * step = [NSString stringWithFormat:@"&Step=2"];
    
    
    NSMutableData * postData = [[NSMutableData alloc] initWithData:[@"AgreeCust=Y" dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[@"&AgreeMargin=Y" dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[@"&AgreeOption=Y" dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[@"&AgreeJoint=Y" dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[@"&AgreePrivacy=Y" dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[@"&AgreeEConfirm=Y" dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[@"&AgreeEstatement=Y" dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[@"&AgreeEProxy=Y" dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[@"&AgreeEProspectus=Y" dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[@"&AgreeETax=Y" dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[@"&AgreeEReorg=Y" dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[@"&AgreeForeignFinder=Y" dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[@"&ExemptW9=Y" dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[@"&AgreeW8BEN=Y" dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[@"&Step=6" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    
    NSString * url = [NSString stringWithFormat:@"%@api/accounts/step6/%@",delegate.producationURL,delegate.appIDString];
    
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    [request setHTTPMethod:@"PUT"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        NSMutableDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        //NSLog(@"%@",responseDict);
                                                        
                                                        if([[responseDict objectForKey:@"status"]isEqualToString:@"200"])
                                                        {
                                                            Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
                                                            [mixpanelMini track:@"usa_open_account_step7_completed_page"];
                                                            [self choiceTradeStepSeven];
                                                        }else
                                                        {
                                                            NSString * errorText = [NSString stringWithFormat:@"%@.Please check the inputs given again.",[responseDict objectForKey:@"message"]];
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:errorText preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    [activityIndicatorView stopAnimating];
                                                                    [backView removeFromSuperview];
                                                                });
                                                            }];
                                                            
                                                            [alert addAction:okAction];
                                                        }
                                                        
                                                    }
                                                }];
    [dataTask resume];
}

-(void)pdfMethod
{
    
    UIDocumentPickerViewController *documentPicker = [[UIDocumentPickerViewController alloc]initWithDocumentTypes:@[@"public.data"] inMode:UIDocumentPickerModeImport];
    documentPicker.delegate = self;
    
    documentPicker.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:documentPicker animated:YES completion:nil];
}

-(void)cameraInvokeMethod
{
    dispatch_async(dispatch_get_main_queue(), ^{
    
   
    
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Choose one" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    [self presentViewController:alert animated:YES completion:nil];
    self.camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        UIImagePickerController * cameraPicker = [[UIImagePickerController alloc]init];
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        }
        cameraPicker.delegate=self;
        [self presentViewController:cameraPicker animated:YES completion:nil];
        
        
        
        cameraPicker.mediaTypes = [NSArray arrayWithObjects:
                                   (NSString *) kUTTypeImage,
                                   nil];
        cameraPicker.allowsEditing = NO;
        // [self presentViewController:cameraPicker animated:YES completion:nil];
//        self.newMedia = YES;
        
        
    }];
    
    self.gallery = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];

        
        
    }];
    
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:self.camera];
    [alert addAction:self.gallery];
    [alert addAction:cancel];
        
         });
}

- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url{
    if (controller.documentPickerMode== UIDocumentPickerModeImport) {
        //taking the path of the file selected
        NSString *path = [url path];
        //converting to NSData
        data = [[NSFileManager defaultManager] contentsAtPath:path];
        //converting to base64 string
       // base64String = [data base64EncodedStringWithOptions:kNilOptions];
        [self postFiles];
        
        
    }
}

//- (void)documentPicker:(UIDocumentPickerViewController* )controller didPickDocumentAtURL:(NSURL* )url {
//    if (controller.documentPickerMode == UIDocumentPickerModeImport) {
//         // NSString *alertMessage = [NSString stringWithFormat"Successfully imported %@", [url lastPathComponent]];
//        //do stuff
//
//        self.multipleFilesString = [NSString stringWithFormat:@"%@",url];
//        //NSLog(@"URL:%@",[url lastPathComponent]);
//        data = [NSData dataWithContentsOfFile:self.multipleFilesString];
//        [self postFiles];
//
//    }
//}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    
    
    image = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    
    NSData *png_Data = UIImageJPEGRepresentation(image, 0);
    
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    str_localFilePath = [documentsDirectory stringByAppendingPathComponent:@"One.png"];
    [png_Data writeToFile:str_localFilePath atomically:YES];
    if(myButton==self.idProofButton)
    {
    [self postFiles];
    }else if (myButton == self.signatureButton)
    {
       // signature = [NSString stringWithFormat:@"Signature=%@",[self encodeToBase64String:image]];
       // signature = [NSString stringWithFormat:@"Signature=%@",[self base64String:image]];
        [self.signatureButton setTitle:@"Signature uploaded" forState:UIControlStateNormal];
        signatureCheck=@"Done";
    }
   // [self postToS3];
    //[self postImage];
    
    [self dismissViewControllerAnimated:YES completion:nil];

    
}
- (NSString *)documentsPathForFileName:(NSString *)name {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
    return [documentsPath stringByAppendingPathComponent:name];
}

-(void)image:(UIImage *)image
finishedSavingWithError:(NSError *)error
 contextInfo:(void *)contextInfo
{
    if (error) {
        
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Save failed" message:@"Failed to save image" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
    }
}

-(void)postFiles
{
    backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.view addSubview:backView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeFiveDots tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [backView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    // name of the image
    
   // convert your image into data
    NSString *urlString = [NSString stringWithFormat:@"%@api/accounts/upload/%@",delegate.producationURL,delegate.appIDString];  // enter your url to upload
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];  // allocate AFHTTPManager
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSString * apiKey = [NSString stringWithFormat:@"Bearer %@",delegate.apiKey];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:apiKey forHTTPHeaderField:@"Authorization"];
    
    if(myButton==self.idProofButton)
    {
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        CGFloat halfButtonHeight = self.idProofButton.bounds.size.height / 2;
        CGFloat buttonWidth = self.idProofButton.bounds.size.width;
        indicator.center = CGPointMake(buttonWidth - halfButtonHeight , halfButtonHeight);
        [self.idProofButton addSubview:indicator];
        [indicator startAnimating];
         Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
        [mixpanelMini track:@"usa_open_account_id_proof_upload_initiated"];
         NSData *imageData =  UIImageJPEGRepresentation(image, 0);
    
    [manager POST:urlString parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {  // POST DATA USING MULTIPART CONTENT TYPE
        [formData appendPartWithFileData:imageData
                                    name:@"files[]"
                                fileName:@"image.jpg" mimeType:@"image/jpeg"];   // add image to formData
        
        
//        [formData appendPartWithFormData:[@"sdsd" dataUsingEncoding:NSUTF8StringEncoding]
//                                    name:@"key1"];    // add your other keys !!!
        
    } progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        //NSLog(@"Response: %@", responseObject);    // Get response from the server
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Uploaded successfully. Do not upload again." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            photoCheck =@"Done";
        }];
        
        [alert addAction:okAction];
        
        [self.idProofButton setTitle:@"Uploaded SucessFully" forState:UIControlStateNormal];
        [indicator stopAnimating];
        [activityIndicatorView stopAnimating];
        [backView removeFromSuperview];
        indicator.hidden=YES;
         Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
        [mixpanelMini track:@"usa_open_account_id_proof_upload_completed"];
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        //NSLog(@"Error: %@", error);// Gives Error
        [activityIndicatorView stopAnimating];
        [backView removeFromSuperview];
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:@"Internal server error! Please try uploading again" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [indicator stopAnimating];
            indicator.hidden=YES;
            [activityIndicatorView stopAnimating];
            [backView removeFromSuperview];
            photoCheck=@"";
             photoCheck =@"Done";
            
        }];
        
        [alert addAction:okAction];
        
    }];
    }else if (myButton == self.bankStatementButton)
    {
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        CGFloat halfButtonHeight = self.bankStatementButton.bounds.size.height / 2;
        CGFloat buttonWidth = self.bankStatementButton.bounds.size.width;
        indicator.center = CGPointMake(buttonWidth - halfButtonHeight , halfButtonHeight);
        [self.bankStatementButton addSubview:indicator];
        [indicator startAnimating];
        Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
        [mixpanelMini track:@"usa_open_account_bank_statement_upload_initiated"];
        [manager POST:urlString parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {  // POST DATA USING MULTIPART CONTENT TYPE
            [formData appendPartWithFileData:data
                                        name:@"files[]"
                                    fileName:@"image.pdf" mimeType:@"application/pdf"];   // add image to formData
            
            //        [formData appendPartWithFormData:[@"sdsd" dataUsingEncoding:NSUTF8StringEncoding]
            //                                    name:@"key1"];    // add your other keys !!!
            
        } progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            //NSLog(@"Response: %@", responseObject);    // Get response from the server
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Uploaded successfully. Do not upload again." preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:^{
                
            }];
            
            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                 bankStatementCheck=@"Done";
            }];
           
            [alert addAction:okAction];
            [activityIndicatorView stopAnimating];
            [backView removeFromSuperview];
             [self.bankStatementButton setTitle:@"Uploaded SucessFully" forState:UIControlStateNormal];
            [indicator stopAnimating];
            indicator.hidden=YES;
            Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
            [mixpanelMini track:@"usa_open_account_bank_statement_upload_completed"];
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            //NSLog(@"Error: %@", error);   // Gives Error
            [activityIndicatorView stopAnimating];
            [backView removeFromSuperview];
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:@"Internal server error occured! Please try uploading again." preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:^{
                
            }];
            
            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [indicator stopAnimating];
                indicator.hidden=YES;
                [activityIndicatorView stopAnimating];
                [backView removeFromSuperview];
                bankStatementCheck=@"";
                 bankStatementCheck =@"Done";
            }];
            
            [alert addAction:okAction];
        }];
    }
}




- (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

- (NSString *)base64String:(UIImage *)image {
    NSData * data = [UIImagePNGRepresentation(image) base64EncodedDataWithOptions:NSDataBase64Encoding64CharacterLineLength];
    return [NSString stringWithUTF8String:[data bytes]];
}

-(void)choiceTradeStepSeven
{
    dispatch_async(dispatch_get_main_queue(), ^{
      //  signature = [NSString stringWithFormat:@"Signature=%@",[self encodeToBase64String:image]];
        signatureName  = [NSString stringWithFormat:@"&SignatureName=%@",self.signatureTF.text];
    });
    
     NSString * apiKey1 = [NSString stringWithFormat:@"Bearer %@",delegate.apiKey];
    
  //  loadingLabel.text =@"Submitting Details of step7";
    NSDictionary *headers = @{ @"Content-Type": @"application/x-www-form-urlencoded",
                               @"Authorization": apiKey1,
                               };
    
    
    
    
    // NSString * step = [NSString stringWithFormat:@"&Step=2"];
    
    
  
   
    NSString * testString = [NSString stringWithFormat:@"Signature=%@",image];
    NSMutableData * postData = [[NSMutableData alloc] initWithData:[testString dataUsingEncoding:NSDataBase64Encoding64CharacterLineLength]];
    
  //  NSMutableData * postData = [[NSMutableData alloc] initWithData:[signature dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[signatureName dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"&SignatureIP=192.168.0.1" dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"&Step=7" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
   
    
    NSString * url = [NSString stringWithFormat:@"%@api/accounts/step7/%@",delegate.producationURL,delegate.appIDString];
        
   //     NSString * url = [NSString stringWithFormat:@"%@api/accounts/step7/%@",delegate.producationURL,@"358598682909"];
    
    
    
    
    
        request1 = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
        
    [request1 setHTTPMethod:@"PUT"];
    [request1 setAllHTTPHeaderFields:headers];
    [request1 setHTTPBody:postData];
   
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request1
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        NSMutableDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        //NSLog(@"%@",responseDict);
                                                        
                                                        if([[responseDict objectForKey:@"status"]isEqualToString:@"200"])
                                                        {
                                                            Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
                                                            [mixpanelMini track:@"usa_open_account_step8_completed_page"];
                                                            [self choiceTradeAccountRetrieve];
                                                            
                                                        }else
                                                        {
                                                            NSString * errorText = [NSString stringWithFormat:@"%@",[responseDict objectForKey:@"message"]];
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:errorText preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    [activityIndicatorView stopAnimating];
                                                                    [backView removeFromSuperview];
                                                                });
                                                            }];
                                                            
                                                            [alert addAction:okAction];
                                                            
                                                        }
                                                        
                                                    }
                                                }];
    [dataTask resume];
}

-(void)choiceTradeAccountRetrieve
{
    NSDictionary *headers = @{ @"Authorization": apiKey,
                               @"Cache-Control": @"no-cache",
                            };
    
    
    NSString * url = [NSString stringWithFormat:@"%@api/accounts/%@",delegate.producationURL,delegate.appIDString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:20.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        NSMutableDictionary * responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        //NSLog(@"%@",responseDictionary);
                                                        if([[responseDictionary objectForKey:@"Status"]isEqualToString:@"Completed"])
                                                        {
                                                            Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
                                                            [mixpanelMini track:@"usa_open_account_application_submitted"];
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                [activityIndicatorView stopAnimating];
                                                                [backView removeFromSuperview];
                                                                //loadingLabel.text =@"Application Completed";
                                                                //[activityView stopAnimating];
                                                                
                                                            });
                                                            
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Congrats" message:@"You have successfully completed your application. You will receive an email from ChoiceTrade regarding account details shortly." preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                MainViewUSA * openAccount = [self.storyboard instantiateViewControllerWithIdentifier:@"MainViewUSA"];
                                                                [self.navigationController pushViewController:openAccount animated:YES];
                                                                //[self presentViewController:openAccount animated:YES completion:nil];
                                                                NSUserDefaults *openAccountPref  = [NSUserDefaults standardUserDefaults];
                                                                [openAccountPref removeObjectForKey:@"openAccountStep"];
                                                                
                                                                NSUserDefaults * openAppIDPref = [NSUserDefaults standardUserDefaults];
                                                                [openAppIDPref removeObjectForKey:@"openAppID"];
                                                                
                                                                NSUserDefaults * openMobPref = [NSUserDefaults standardUserDefaults];
                                                                [openMobPref removeObjectForKey:@"openMob"];
                                                                
                                                                NSUserDefaults * openFinalDict = [NSUserDefaults standardUserDefaults];
                                                                [openFinalDict removeObjectForKey:@"openDict"];
                                                            }];
                                                            
                                                            [alert addAction:okAction];
                                                        }else
                                                        {
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:@"Error Occured.Please try again." preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    [activityIndicatorView stopAnimating];
                                                                    [backView removeFromSuperview];
                                                                });
                                                            }];
                                                            
                                                            [alert addAction:okAction];
                                                        }
                                                        
                                                    }
                                                }];
    [dataTask resume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
