//
//  AgreementWebViewController.h
//  ChoiceTradeAccountOpening
//
//  Created by Zenwise Technologies on 14/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgreementWebViewController : UIViewController<UIWebViewDelegate>

@property NSString * buttonType;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property NSString * url;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end
