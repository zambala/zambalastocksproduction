//
//  AgreementWebViewController.m
//  ChoiceTradeAccountOpening
//
//  Created by Zenwise Technologies on 14/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "AgreementWebViewController.h"

@interface AgreementWebViewController ()

@end

@implementation AgreementWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.webView.delegate=self;
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    
    if([self.buttonType isEqualToString:@"user"])
    {
        self.url=@"http://www.choicetrade.com/legal.php?page=UserAgreement";
    }else if ([self.buttonType isEqualToString:@"customer"])
    {
        self.url=@"http://www.choicetrade.com/legal.php?page=custagree";
    }else if ([self.buttonType isEqualToString:@"risk"])
    {
        self.url=@"http://www.choicetrade.com/legal.php?page=RiskDisclosure";
    }else if ([self.buttonType isEqualToString:@"business"])
    {
        self.url=@"http://www.choicetrade.com/legal.php?page=bizcont";
    }else if ([self.buttonType isEqualToString:@"privacy"])
    {
        self.url=@"http://www.choicetrade.com/legal.php?page=privacy";
    }else if ([self.buttonType isEqualToString:@"margin"])
    {
        self.url=@"http://www.choicetrade.com/legal.php?page=disclosures";
    }else if ([self.buttonType isEqualToString:@"order"])
    {
        self.url=@"http://www.choicetrade.com/orderrouting.php";
    }else if ([self.buttonType isEqualToString:@"ccheck"])
    {
        self.url=@"http://www.choicetrade.com/legal.php?page=custagree";
    }else if ([self.buttonType isEqualToString:@"acheck"])
    {
        self.url=@"http://www.choicetrade.com/forms/vision/ctnaf.pdf";
    }else if ([self.buttonType isEqualToString:@"ocheck"])
    {
        self.url=@"http://www.choicetrade.com/forms/vision/ctoption.pdf";
    }else if([self.buttonType isEqualToString:@"cread"])
    {
        self.url=@"http://www.choicetrade.com/legal.php?page=custagree";
    }else if ([self.buttonType isEqualToString:@"aread"])
    {
        self.url=@"http://www.choicetrade.com/forms/vision/ctnaf.pdf";
    }else if ([self.buttonType isEqualToString:@"oread"])
    {
        self.url=@"http://www.choicetrade.com/forms/vision/ctoption.pdf";
    }else if ([self.buttonType isEqualToString:@"char"])
    {
         self.url=@"https://www.theocc.com/components/docs/riskstoc.pdf";
    }else if ([self.buttonType isEqualToString:@"spec"])
    {
        self.url=@"http://www.choicetrade.com/forms/Options-UncoverdStmt.pdf";
       
    }
    NSURL *url=[NSURL URLWithString:self.url];
    
    NSURLRequest *urlReq=[NSURLRequest requestWithURL: url];
    //NSLog(@"URl:%@",urlReq);
    [self.webView loadRequest:urlReq];
    
    // Do any additional setup after loading the view.
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.activityIndicator.hidden=YES;
        [self.activityIndicator stopAnimating];
    });
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
