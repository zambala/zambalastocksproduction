//
//  NewPolicyDetailViewController.m
//  ChoiceTradeAccountOpening
//
//  Created by Zenwise Technologies on 16/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "NewPolicyDetailViewController.h"
#import "AgreementWebViewController.h"

@interface NewPolicyDetailViewController ()

@end

@implementation NewPolicyDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.buttonOne.hidden=YES;
    self.buttonTwo.hidden=YES;
    self.readButton.hidden=NO;
    
    if([self.buttonType isEqualToString:@"cread"])
    {
        self.topTitleLabel.text=@"Customer Agreement";
        self.contentLabel.text=@"I represent that I have read, understand and agree to the Terms and Conditions governing this account as contained in the Customer Agreement. I agree to be bound by such Terms and Conditions as currently in effect and as may be amended from time to time without prior notice to me. I warrant that all the information I have entered on my application is correct to the best of my knowledge and belief. If there is a change in the future, I agree to promptly notify ChoiceTrade in writing.I UNDERSTAND THAT THIS ACCOUNT IS GOVERNED BY AN ARBITRATION AGREEMENT WHICH IS SET FORTH IN PARAGRAPHS 18 AND 19 OF THE CUSTOMER AGREEMENT. I ACKNOWLEDGE RECEIPT OF THE ARBITRATION AGREEMENT AND THE CUSTOMER ACCOUNT AGREEMENT GOVERNING MY ACCOUNT. I acknowledge that I have read and understand the Active Trading Risk Disclosure Statement, the Margin Disclosure Statement and the ChoiceTrade Privacy Statement. I understand and acknowledge that (i) this account application is not made pursuant to any solicitation, (ii) ChoiceTrade is not affiliated with or an agent of its clearing firm, and (iii) nothing herein is an offer or solicitation of any security, product or service in any jurisdiction where their offer or sale would be contrary to local law or regulation. I acknowledge that ChoiceTrade does not give investment, tax, legal or accounting advice with respect to the potential value or suitability of a particular security or investment strategy.Fee acknowledgement: Some platforms involve a monthly Service Fee (please review the Commission and Fee Schedule). I acknowledge and agree to pay the Service Fee by authorizing ChoiceTrade to debit my account. I acknowledge that a finder's fee may be paid pursuant to agreement.";
        self.buttonOne.hidden=YES;
        self.buttonTwo.hidden=YES;
        self.readButton.hidden=NO;
    }else if ([self.buttonType isEqualToString:@"aread"])
    {
//        AgreementWebViewController * webView = [self.storyboard instantiateViewControllerWithIdentifier:@"AgreementWebViewController"];
//        webView.buttonType=@"aread";
//        self.topTitleLabel.text=@"New Account Application";
//        self.contentLabel.text=@"";
//        self.buttonOne.hidden=YES;
//        self.buttonTwo.hidden=YES;
//        self.readButton.hidden=NO;
    //    [self.navigationController pushViewController:webView animated:YES];
    }else if ([self.buttonType isEqualToString:@"oread"])
    {
        self.topTitleLabel.text=@"Options Agreement";
        self.contentLabel.text=@"I AGREE TO ADVISE CHOICETRADE OF ANY MATERIAL CHANGE IN MY FINANCIAL STATUS AND/OR INVESTMENT OBJECTIVES. I AGREE TO ALL TERMS OF THE OPTIONS SUPPLEMENT. I ACKNOWLEDGE THAT I HAVE RECEIVED AND READ THE DISCLOSURE DOCUMENT, “CHARACTERISTICS AND RISKS OF STANDARDIZED OPTIONS” AND AM AWARE OF THE SPECIAL RISKS INHERENT IN OPTIONS TRADING.";
        self.buttonOne.hidden=NO;
        self.buttonTwo.hidden=NO;
        self.readButton.hidden=NO;
    }else if ([self.buttonType isEqualToString:@"tread"])
    {
        self.topTitleLabel.text=@"Certification of Tax Payer";
        self.contentLabel.text=@"I agree with the above certification. I acknowledge that I must make the declaration contained in the perjury statement of the IRS form W8-BEN and that I make this declaration by my signature below. I hereby agree to submit my form W8-BEN electronically and permit ChoiceTrade to share it with its clearing firm.";
        self.buttonOne.hidden=YES;
        self.buttonTwo.hidden=YES;
        self.readButton.hidden=YES;
        
    }else if ([self.buttonType isEqualToString:@"fread"])
    {
        self.readButton.hidden=YES;
    }
    
    [self.readButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
     [self.buttonOne addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
     [self.buttonTwo addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

-(void)onButtonTap:(UIButton*)sender
{
    AgreementWebViewController * webView = [self.storyboard instantiateViewControllerWithIdentifier:@"AgreementWebViewController"];
    
    if(sender==self.readButton)
    {
        if([self.buttonType isEqualToString:@"cread"])
        {
            webView.buttonType=@"cread";
        }else if ([self.buttonType isEqualToString:@"aread"])
        {
            webView.buttonType=@"aread";
        }else if ([self.buttonType isEqualToString:@"oread"])
        {
            webView.buttonType=@"oread";
        }else if ([self.buttonType isEqualToString:@"tread"])
        {
            webView.buttonType=@"tread";
        }else if ([self.buttonType isEqualToString:@"fread"])
        {
            webView.buttonType=@"fread";
        }
    }else if (sender==self.buttonOne)
    {
        webView.buttonType=@"char";
    }else if (sender == self.buttonTwo)
    {
        webView.buttonType=@"spec";
    }
    [self.navigationController pushViewController:webView animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
