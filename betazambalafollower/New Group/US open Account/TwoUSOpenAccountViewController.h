//
//  TwoUSOpenAccountViewController.h
//  ChoiceTradeAccountOpening
//
//  Created by Zenwise Technologies on 14/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TwoUSOpenAccountViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *addressOneTF;
@property (weak, nonatomic) IBOutlet UITextField *addressTwoTF;
@property (weak, nonatomic) IBOutlet UITextField *countryTF;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UITextField *stateTF;
@property (weak, nonatomic) IBOutlet UITextField *cityTF;
@property (weak, nonatomic) IBOutlet UITextField *postalCodeTF;
@property (weak, nonatomic) IBOutlet UITextField *mobileNOTF;
@property (weak, nonatomic) IBOutlet UITextField *occupationTF;
@property (weak, nonatomic) IBOutlet UIButton *empStatusButton;
@property (weak, nonatomic) IBOutlet UITextField *yearsEmpTF;
@property (weak, nonatomic) IBOutlet UITextField *companyNameTF;
@property (weak, nonatomic) IBOutlet UITextField *typeofBusinessTF;
@property (weak, nonatomic) IBOutlet UITextField *businessAddressTF;
@property (weak, nonatomic) IBOutlet UITextField *businessCountry;
@property (weak, nonatomic) IBOutlet UITextField *businessState;
@property (weak, nonatomic) IBOutlet UITextField *businessCity;
@property (weak, nonatomic) IBOutlet UITextField *businessPostalCode;
@property (weak, nonatomic) IBOutlet UITextField *businessMobile;

@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property (weak, nonatomic) IBOutlet UIButton *legalButton;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UIView *employedView;
@property (weak, nonatomic) IBOutlet UIView *unEmployedView;
@property (weak, nonatomic) IBOutlet UITextField *previousEmployerLbl;
@property (weak, nonatomic) IBOutlet UITextField *unemployedTypeOfBusiness;
@property (weak, nonatomic) IBOutlet UITextField *occupationOfPreviousEmployementLbl;
@property (weak, nonatomic) IBOutlet UITextField *unemployedYearsEmployedLbl;
@property (weak, nonatomic) IBOutlet UIButton *lastDateEmployedBtn;

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property NSMutableArray * dataArray;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *employedViewHgt;

@property NSMutableArray * symbols;
@property NSMutableArray * company;
@property NSMutableArray * selectedCompanyArray;
@property NSMutableArray * searchResponseArray;
@property NSMutableArray * removeCountriesArray;
@property NSMutableDictionary * responseDictionary;
@property NSString * stateIDString;
@property NSString * cityIDString;
@property NSMutableArray * selectedCountryArray;
@property NSMutableDictionary * statesResponseDictionary;
@property NSMutableArray * statesResponseArray;
@property NSString * selectedISO;
@property NSString * mobileNumber;
@property (weak, nonatomic) IBOutlet UIView *doneView;

@property (weak, nonatomic) IBOutlet UIView *bgView;


@end
