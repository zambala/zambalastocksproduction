//
//  USOpenAccountViewController.m
//  ChoiceTradeAccountOpening
//
//  Created by Zenwise Technologies on 14/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "USOpenAccountViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "AgreementViewController.h"
#import "TwoUSOpenAccountViewController.h"
#import "AppDelegate.h"
#import <UITextField_AutoSuggestion/UITextField+AutoSuggestion.h>
#import <Mixpanel/Mixpanel.h>
#import "DGActivityIndicatorView.h"

@interface USOpenAccountViewController ()<CLLocationManagerDelegate,UITextFieldAutoSuggestionDataSource,UITextFieldDelegate>

#define Symbol_ID @"symbol_id"

#define WEEKS @[@"Monday", @"Tuesday", @"Wednesday", @"Thirsday", @"Friday", @"Saturday", @"Sunday"]

{
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLGeocoder * geocoder;
    UIToolbar * toolbar;
    UIToolbar * dateToolBar;
    NSDateFormatter *dateFormatter;
    UIButton * myButton;
    CGPoint coordinates;
    AppDelegate * delegate;
     NSString * searchSymbol;
    UITextField * selectedTextField;
    int age;
    NSString * CountryBirth;
    NSString * CountryResidence;
    NSString * CountryCitizenship;
    DGActivityIndicatorView *activityIndicatorView;
    UIView * backView;
}

@end

@implementation USOpenAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self CurrentLocationIdentifier];
    
    self.dataArray = [[NSMutableArray alloc]init];
    
//    self.emailTF.text=@"sandeep.magantis@gmail.com";
//    self.firstNameTF.text=@"Sandeep";
//    self.middleTF.text=@"M";
//    self.lastNameTF.text=@"Maganti";
//    self.govtIDTF.text=@"DDMPM4590P";
    
    self.countryOfBirthTF.text = @"India";
    self.countryResidence.text = @"India";
    self.citizenshipTF.text = @"India";
    //IND
    
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"usa_open_account_step1_initiated_page"];
   
    self.navigationItem.title=@"STEP 1";
    self.citizenshipButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
     self.accountTypeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
     self.subAccountTypeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
     self.titleButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.dobButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
     self.maritalStatusButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.genderButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.maritalStatusButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    self.doneView.hidden=YES;
    self.pickerView.hidden=YES;
    self.datePicker.hidden=YES;
    
    self.accountTypeView.hidden=YES;
    self.accountTypeTopConstraint.constant=0;
    self.accountTypeHeightConstraint.constant=0;
    self.subAccountTypeTopLayout.constant=0;
    
    
    
    NSString * message = [NSString stringWithFormat:@" %C All the fields mentioned in this form are mandatory. \n %C Please put your signature on a white paper to upload further.\n %C Please download the latest bank statement which contains the address you will be giving in the form. After downloading save the PDF to files. The same bank account will be used to transfer funds.\n %C Please keep any one of the Government ID like PAN card, Voter card, Passport handy. The same should be passed in GovernmentID field.", (unichar) 0x2022, (unichar) 0x2022, (unichar) 0x2022, (unichar) 0x2022];
    
    self.middleTF.placeholder=@"Only one character should be entered.";
    
    
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Instructions" message:message preferredStyle:UIAlertControllerStyleAlert];

    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.alignment = NSTextAlignmentLeft;
    
    NSMutableAttributedString *atrStr = [[NSMutableAttributedString alloc] initWithString:message attributes:@{NSParagraphStyleAttributeName:paraStyle,NSFontAttributeName:[UIFont systemFontOfSize:15.0]}];
    
    [alert setValue:atrStr forKey:@"attributedMessage"];

    [self presentViewController:alert animated:YES completion:^{

    }];

    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"I have read the instructions carefully." style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

    }];

    [alert addAction:okAction];
    
  //  self.continueButton.layer.cornerRadius=20.0f;
    self.continueButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.continueButton.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.continueButton.layer.shadowOpacity = 10.0f;
    self.continueButton.layer.shadowRadius = 3.0f;
    self.continueButton.layer.masksToBounds = NO;
    
    
    [self.legalButton addTarget:self action:@selector(onLegalButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    [self.citizenshipButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.accountTypeButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.subAccountTypeButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.titleButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.dobButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.maritalStatusButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.genderButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.continueButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    
    
    NSDate * date = [[NSDate alloc]init];
    date = [NSDate date];
    dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateStyle=NSDateFormatterLongStyle;
    [self.dobButton setTitle:[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:date]] forState:UIControlStateNormal];
    
    self.datePicker.maximumDate = [NSDate date];
    
//    self.accountTypeView.hidden=YES;
//    self.accountTypeHeightConstraint.constant=0;
//    self.accountTypeTopConstraint=0;
    
    self.subAccountTypeView.hidden=YES;
    self.subAccountTypeViewHeightConstraint.constant=0;
    self.subAccountTypeTopLayout.constant=0;
    self.symbols = [[NSMutableArray alloc]init];
    self.company = [[NSMutableArray alloc]init];
    self.selectedCompanyArray = [[NSMutableArray alloc]init];
    self.searchResponseArray = [[NSMutableArray alloc]init];
    self.finalCTDictonary = [[NSMutableDictionary alloc]init];
    self.removeCountriesArray = [[NSMutableArray alloc]initWithObjects:@"Balkans",@"Belarus",@"Balkans",@"Burma",@"Cote D''Ivoire (Ivory Coast)",@"Cuba",@"Congo",@"Iran",@"Iraq",@"Liberia",@"Korea North",@"Sudan",@"Syria",@"Zimbabwe",@"United States", nil];
//     ['Balkans', 'Belarus', 'Burma', "Cote D''Ivoire (Ivory Coast)", 'Cuba', 'Congo', 'Iran', 'Iraq', 'Liberia', 'Korea North', 'Sudan', 'Syria', 'Zimbabwe', 'United States']
    
    // self.searchTableView.hidden=YES;
    
    [self searchServer];
    self.countryOfBirthTF.delegate = self;
    self.countryOfBirthTF.autoSuggestionDataSource = self;
    self.countryOfBirthTF.fieldIdentifier = Symbol_ID;
    [self.countryOfBirthTF observeTextFieldChanges];
    
    
    self.countryResidence.delegate = self;
    self.countryResidence.autoSuggestionDataSource = self;
    self.countryResidence.fieldIdentifier = Symbol_ID;
    [self.countryResidence observeTextFieldChanges];
    
    self.citizenshipTF.delegate = self;
    self.citizenshipTF.autoSuggestionDataSource = self;
    self.citizenshipTF.fieldIdentifier = Symbol_ID;
    [self.citizenshipTF observeTextFieldChanges];
    
    self.emailTF.delegate=self;
    self.firstNameTF.delegate=self;
    self.middleTF.delegate=self;
    self.lastNameTF.delegate=self;
    self.govtIDTF.delegate=self;
    self.mobileNumberTF.delegate=self;
    
    
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
}

- (void)loadWeekDays {
    // cancel previous requests
    @try {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(loadWeekDaysInBackground) object:self];
        //change
        [selectedTextField setLoading:false];
        
        // clear previous results
        [self.symbols removeAllObjects];
        
        //change
        [selectedTextField reloadContents];
        
        // start loading
        
        //change
        [selectedTextField setLoading:true];
        [self performSelector:@selector(loadWeekDaysInBackground) withObject:self afterDelay:2.0f];
        
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (void)loadWeekDaysInBackground {
    // finish loading
    @try {
        
        //change
        [selectedTextField setLoading:false];
        
        
        [self.symbols addObjectsFromArray:self.company];
        
        //change
        [selectedTextField reloadContents];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}

- (UITableViewCell *)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
    
    static NSString *cellIdentifier = @"WeekAutoSuggestionCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF BEGINSWITH[c] %@", text];
    NSArray *weeks = [self.symbols filteredArrayUsingPredicate:filterPredictate];
    
    cell.textLabel.text = weeks[indexPath.row];
    
    return cell;
}


- (NSInteger)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section forText:(NSString *)text {
    
    NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF BEGINSWITH[c] %@", text];
    
    @try {
        NSInteger count = [self.symbols filteredArrayUsingPredicate:filterPredictate].count;
        return count;
    }
    @catch (NSException *exception) {
        //NSLog(@"%@", exception.reason);
    }
    
    return 0;
    
}

- (void)autoSuggestionField:(UITextField *)field textChanged:(NSString *)text {
    [self loadWeekDays];
}


- (CGFloat)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
    return 50;
}

- (void)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    //NSLog(@"Selected suggestion at index row - %ld", (long)indexPath.row);
    
    
    
    NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF BEGINSWITH[c] %@", text];
    NSArray *weeks = [self.symbols filteredArrayUsingPredicate:filterPredictate];
    //change
    selectedTextField.text = weeks[indexPath.row];
    [self parametersMethod:selectedTextField];
    if(self.selectedCompanyArray.count>0)
    {
        [self.selectedCompanyArray removeAllObjects];
    }
    
    if([selectedTextField.text isEqualToString:@"India"])
    {
        self.accountTypeView.hidden=YES;
        self.accountTypeTopConstraint.constant=0;
        self.accountTypeHeightConstraint.constant=0;
        self.subAccountTypeTopLayout.constant=0;
    }else
    {
        self.accountTypeView.hidden=YES;
        self.accountTypeTopConstraint.constant=60;
        self.accountTypeHeightConstraint.constant=41.2;
        self.subAccountTypeTopLayout.constant=41.2;
    }
    for(int i=0;i<self.searchResponseArray.count;i++)
    {
        NSString * name=[NSString stringWithFormat:@"%@",[[self.searchResponseArray objectAtIndex:i]objectForKey:@"name"]];
        if([name isEqualToString:selectedTextField.text])
        {
            [self.selectedCompanyArray addObject:[self.searchResponseArray objectAtIndex:i]];
        }
    }
    
    if([selectedTextField.text isEqualToString:@"India"])
    {
        self.accountTypeView.hidden=YES;
        self.accountTypeHeightConstraint.constant=0;
        self.accountTypeTopConstraint.constant=0;
    }else
    {
        self.accountTypeView.hidden=NO;
        self.accountTypeHeightConstraint.constant=60;
        self.accountTypeTopConstraint.constant=41.2;
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
     selectedTextField = textField;
     NSString * searchStr = [selectedTextField.text stringByReplacingCharactersInRange:range withString:string];
    if(self.company.count>0)
    {
        [self.company removeAllObjects];
    }
    
   
    for (int i=0; i<self.searchResponseArray.count; i++) {
        NSString * string1 = [[self.searchResponseArray objectAtIndex:i] objectForKey:@"name"];
        NSString * string2 = [string1 lowercaseString];
        searchStr = [searchStr lowercaseString];
        if([string2 containsString:searchStr])
        {
            [self.company addObject:string1];
        }
    }
    
//    if(textField== self.middleTF)
//    {
//        if(textField.text.length>0)
//        {
//
//        }else
//        {
//            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Alert" message:@"Middle initial can have only one character." preferredStyle:UIAlertControllerStyleAlert];
//
//            [self presentViewController:alert animated:YES completion:^{
//
//            }];
//
//            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//            }];
//            
//            [alert addAction:okAction];
//        }
//    }
    
   
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    selectedTextField = textField;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}

- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    // [self.view setFrame:CGRectMake(0,-100,self.view.frame.size.width,self.view.frame.size.height)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    if(textField==self.countryOfBirthTF||self.countryResidence||self.citizenshipTF)
    {
        
    }else
    {
        [self parametersMethod:selectedTextField];
    }
    
    if(textField==self.middleTF)
    {
        //NSLog(@"Nothing");
    }
//    else if(textField.text.length==0)
//    {
//        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Alert" message:@"This field is mandatory. Please fill the detail." preferredStyle:UIAlertControllerStyleAlert];
//
//        [self presentViewController:alert animated:YES completion:^{
//
//        }];
//
//        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//        }];
//
//        [alert addAction:okAction];
//        return YES;
//    }
    else
    {
        return YES;
    }
    
//    if(textField.text.length>0)
//    {
//        return YES;
//    }else
//    {
//        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Please fill previous detail." preferredStyle:UIAlertControllerStyleAlert];
//
//        [self presentViewController:alert animated:YES completion:^{
//
//        }];
//
//        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//        }];
//
//        [alert addAction:okAction];
//        return NO;
//    }
    
    return YES;
   
    
   // [self.view endEditing:YES];
    
   
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    //[self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
}

-(void)searchServer
{
    
    NSDictionary *headers = @{ @"Cache-Control": @"no-cache",
                               @"authtoken":delegate.zenwiseToken,
                                @"deviceid":delegate.currentDeviceId
                            };
    NSString * str=[NSString stringWithFormat:@"%@countryinfo/countrystatecity?type=country",delegate.baseUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:str]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==403)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                            
                                                        }
                                                        else if([httpResponse statusCode]==401)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                        self.responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        self.searchResponseArray= [[NSMutableArray alloc]initWithArray:[self.responseDictionary objectForKey:@"data"]];
                                                        //NSLog(@"Search:%@",self.searchResponseArray);
                                                        
                                                        NSArray * localArray = [self.searchResponseArray mutableCopy];
                                                        [self.searchResponseArray removeAllObjects];
                                                        
                                                        for (int i=0; i<localArray.count; i++) {
                                                            NSString * string1 = [NSString stringWithFormat:@"%@",[[localArray objectAtIndex:i]objectForKey:@"name"]];
                                                            BOOL check=true;
                                                            for (int j=0; j<self.removeCountriesArray.count; j++) {
                                                                NSString * string2 = [self.removeCountriesArray objectAtIndex:j];
                                                                string2 = [string2 lowercaseString];
                                                                string1 = [string1 lowercaseString];
                                                                
                                                                if([string1 isEqualToString:string2])
                                                                {
                                                                    NSMutableArray * localArray=[[NSMutableArray alloc]init];
                                                                    for(int k=0;k<self.searchResponseArray.count;k++)
                                                                    {
                                                                        [localArray addObject:[[[self.searchResponseArray objectAtIndex:k]objectForKey:@"name"]lowercaseString]];
                                                                    }
                                                                    if([localArray containsObject:string1])
                                                                    {
                                                                    int index=(int)[localArray indexOfObject:string1];
                                                                    
                                                                    [self.searchResponseArray removeObjectAtIndex:index];
                                                                    
                                                                    }
                                                                    
                                                                    
                                                                }
                                                                
                                                            else if(check==true)
                            
                                                                {
                                                                    check=false;
                                                                     [self.searchResponseArray addObject:[localArray objectAtIndex:i]];
                                                                }
                                                            }
                                            
                                                            
                                                        }
                                                        
                                                        }
                                                    }
                                                }];
    [dataTask resume];
    
    
//    if(self.company.count>0)
//    {
//        [self.company removeAllObjects];
//    }
    
    
    
    
    
//    //NSLog(@"%@",self.removeCountriesArray);
//
//    [self.searchResponseArray addObject:@"Japan"];
//    [self.searchResponseArray addObject:@"Germany"];
//    [self.searchResponseArray addObject:@"Chicago"];
//    [self.searchResponseArray addObject:@"India"];
//    [self.searchResponseArray addObject:@"Belarus"];
//    [self.searchResponseArray addObject:@"Burma"];
//    for (int i=0; i<self.searchResponseArray.count; i++) {
//        NSString * string1 = [NSString stringWithFormat:@"%@",[self.searchResponseArray objectAtIndex:i]];
//        NSString * string2 = [NSString stringWithFormat:@"%@",[self.removeCountriesArray objectAtIndex:i]];
//        if([string1 isEqualToString:string2])
//        {
//            [self.searchResponseArray removeObjectAtIndex:i];
//        }
//    }
//
//    for (int i=0; i<self.searchResponseArray.count; i++) {
//        [self.company addObject:[self.searchResponseArray objectAtIndex:i]];
//    }
//    //NSLog(@"Compnay Array:%@",self.company);
    
//    [self.company addObject:@"Japan"];
//    [self.company addObject:@"Germany"];
//    [self.company addObject:@"Chicago"];
//    [self.company addObject:@"India"];
}


-(void)onButtonTap:(UIButton*)sender
{
    myButton = (UIButton*)sender;
    
    [selectedTextField resignFirstResponder];
    
    if(sender == self.continueButton)
    {
        
    }else
    {
    self.doneView.hidden=NO;
    self.pickerView.hidden=NO;
    self.datePicker.hidden=YES;
    self.pickerView.backgroundColor=[UIColor lightGrayColor];
    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(onPickerDoneButtonTap)];
    toolbar= [[UIToolbar alloc]initWithFrame:CGRectMake(0,0,self.view.frame.size.width,50)];
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton,nil];
    [toolbar setItems:toolbarItems];
    [self.doneView addSubview:toolbar];
    coordinates = sender.frame.origin;
    CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
    [self.scrollView setContentOffset:bottomOffset animated:YES];
    }
    if(sender == self.citizenshipButton)
    {
        if(self.dataArray.count>0)
        {
            [self.dataArray removeAllObjects];
        }
        [self.dataArray addObject:@"US Citizen"];
        [self.dataArray addObject:@"Resident Alien"];
        [self.dataArray addObject:@"Non Resident"];
        self.pickerView.delegate=self;
        self.pickerView.dataSource=self;
        
    }else if (sender == self.accountTypeButton)
    {
        if(self.dataArray.count>0)
        {
            [self.dataArray removeAllObjects];
        }
        [self.dataArray addObject:@"Individual"];
        [self.dataArray addObject:@"Joint"];
        self.pickerView.delegate=self;
        self.pickerView.dataSource=self;
    }else if (sender == self.subAccountTypeButton)
    {
        if(self.dataArray.count>0)
        {
            [self.dataArray removeAllObjects];
        }
        [self.dataArray addObject:@"Joint Tenants with Rights of Survivorship"];
        [self.dataArray addObject:@"Joint Tenants in Common"];
        [self.dataArray addObject:@"Joint Tenants by Entirety"];
        [self.dataArray addObject:@"Joint Community Property"];
        self.pickerView.delegate=self;
        self.pickerView.dataSource=self;
    }else if (sender == self.titleButton)
    {
        if(self.dataArray.count>0)
        {
            [self.dataArray removeAllObjects];
        }
        
        [self.dataArray addObject:@"Mr."];
        [self.dataArray addObject:@"Mrs."];
        [self.dataArray addObject:@"Ms."];
        [self.dataArray addObject:@"Dr."];
        self.pickerView.delegate=self;
        self.pickerView.dataSource=self;
    }else if (sender == self.dobButton)
    {
        self.pickerView.hidden=YES;
        self.datePicker.hidden=NO;
        self.doneView.hidden=NO;
        self.datePicker.backgroundColor=[UIColor lightGrayColor];
        [self.datePicker addTarget:self action:@selector(LabelTitle) forControlEvents:UIControlEventAllEvents];
        UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(onDOBDoneButtonTap)];
        
        dateToolBar= [[UIToolbar alloc]initWithFrame:CGRectMake(0,0,self.view.frame.size.width,50)];
        NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton,nil];
        [dateToolBar setItems:toolbarItems];
        [self.doneView addSubview:dateToolBar];
        
    }else if (sender == self.maritalStatusButton)
    {
        if(self.dataArray.count>0)
        {
            [self.dataArray removeAllObjects];
        }
        
        [self.dataArray addObject:@"Single"];
        [self.dataArray addObject:@"Married"];
        [self.dataArray addObject:@"Divorced"];
        [self.dataArray addObject:@"Widowed"];
        self.pickerView.delegate=self;
        self.pickerView.dataSource=self;
        
    }else if (sender == self.genderButton)
    {
        if(self.dataArray.count>0)
        {
            [self.dataArray removeAllObjects];
        }
        [self.dataArray addObject:@"Male"];
        [self.dataArray addObject:@"Female"];
        self.pickerView.delegate=self;
        self.pickerView.dataSource=self;
    }else if (sender == self.continueButton)
    {
        [self parametersMethod:myButton];
        //NSLog(@"Final:%@",self.finalCTDictonary);
        self.pickerView.hidden=YES;
        self.datePicker.hidden=YES;
        [toolbar removeFromSuperview];
        [dateToolBar removeFromSuperview];
        if(self.countryOfBirthTF.text.length>0&&self.countryResidence.text.length>0&&self.citizenshipTF.text.length>0&&self.accountTypeButton.titleLabel.text>0&&self.emailTF.text.length>0&&self.titleButton.titleLabel.text.length>0&&self.firstNameTF.text.length>0&&self.lastNameTF.text.length>0&&self.govtIDTF.text.length>0&&self.dobButton.titleLabel.text.length>0&&self.maritalStatusButton.titleLabel.text.length>0&&self.genderButton.titleLabel.text>0)
        {
            [self createAccountServer];
        //TwoUSOpenAccountViewController * two = [self.storyboard instantiateViewControllerWithIdentifier:@"TwoUSOpenAccountViewController"];
        //[self.navigationController pushViewController:two animated:YES];
        }else
        {
            
            
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Missing Fields. Please Verify" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                
                [alert addAction:okAction];
            
            

        }
        
     //   [self createAccountServer];
        
//        TwoUSOpenAccountViewController * two = [self.storyboard instantiateViewControllerWithIdentifier:@"TwoUSOpenAccountViewController"];
//        [self.navigationController pushViewController:two animated:YES];
        
    }
}

-(void)LabelTitle
{
    dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateStyle=NSDateFormatterLongStyle;
    self.dobString = [NSString stringWithFormat:@"%@",[dateFormatter  stringFromDate:self.datePicker.date]];
    [self.dobButton setTitle:self.dobString forState:UIControlStateNormal];
}

-(void)onDOBDoneButtonTap
{
    NSDate * date = [dateFormatter dateFromString:self.dobString];
    [self ageFromBirthday:date];
    [self ageFromBirthDate:self.dobString];
    age = [[self ageFromBirthDate:self.dobString]intValue];
    if(age>18)
    {
        [self.scrollView setContentOffset:CGPointMake(0, myButton.frame.origin.y-100) animated:YES];
        //[self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        self.datePicker.hidden=YES;
        self.doneView.hidden=YES;
        [dateToolBar removeFromSuperview];
        [toolbar removeFromSuperview];
    }else
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:@"Age must be greater than 18 years." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
           
        }];
        
        [alert addAction:okAction];
    }
    
}
- (NSString *) ageFromBirthDate:(NSString *)birthDate{
    dateFormatter= [[NSDateFormatter alloc]init];
    dateFormatter.dateStyle=NSDateFormatterLongStyle;
    NSDate *myDate = [dateFormatter dateFromString:birthDate];
    
    //NSLog(@"%@",[NSString stringWithFormat:@"%ld ans", (long)[[[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:myDate toDate:[NSDate date] options:0] year]]);
    
    return [NSString stringWithFormat:@"%ld ans", (long)[[[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:myDate toDate:[NSDate date] options:0] year]];
}


- (NSInteger)ageFromBirthday:(NSDate *)birthdate {
    NSDate *today = [NSDate date];
    NSDateComponents *ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                       fromDate:birthdate
                                       toDate:today
                                       options:0];
    
    //NSLog(@"%ld",(long)ageComponents.year);
    return ageComponents.year;
}

-(void)onPickerDoneButtonTap
{
     [self.scrollView setContentOffset:CGPointMake(0, myButton.frame.origin.y-100) animated:YES];
    //[self.scrollView setContentOffset:coordinates animated:YES];
    
    //CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
    //[self.scrollView setContentOffset:bottomOffset animated:YES];
    self.pickerView.hidden=YES;
    self.doneView.hidden=YES;
    [toolbar removeFromSuperview];
    
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView
numberOfRowsInComponent:(NSInteger)component {
    return self.dataArray.count;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [NSString stringWithFormat:@"%@",[self.dataArray objectAtIndex:row]];//Or, your suitable title; like Choice-a, etc.
}

- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {

    [myButton setTitle:[self.dataArray objectAtIndex:row] forState:UIControlStateNormal];
    if([myButton.titleLabel.text isEqualToString:@"Joint"])
    {
        self.subAccountTypeView.hidden=NO;
        self.subAccountTypeViewHeightConstraint.constant=60;
        self.subAccountTypeTopLayout.constant=41.2;
    }else
    {
        self.subAccountTypeView.hidden=YES;
        self.subAccountTypeViewHeightConstraint.constant=0;
        self.subAccountTypeTopLayout.constant=0;
    }
}


-(void)parametersMethod:(id)sender
{
    [self.finalCTDictonary setObject:@"non_resident" forKey:@"citizenship"];
    [self.finalCTDictonary setObject:@"zambala" forKey:@"password"];
    [self.finalCTDictonary setObject:self.countryOfBirthTF.text forKey:@"countryofbirth"];
    [self.finalCTDictonary setObject:self.countryResidence.text forKey:@"countryresidence"];
    [self.finalCTDictonary setObject:self.citizenshipTF.text forKey:@"countrycitizenship"];
    [self.finalCTDictonary setObject:self.emailTF.text forKey:@"email"];
    [self.finalCTDictonary setObject:self.firstNameTF.text forKey:@"firstname"];
    if(self.middleTF.text.length>0)
    {
    [self.finalCTDictonary setObject:[self.middleTF.text substringToIndex:1] forKey:@"middlename"];
    }
    [self.finalCTDictonary setObject:self.lastNameTF.text forKey:@"lastname"];
    [self.finalCTDictonary setObject:self.govtIDTF.text forKey:@"govtid"];
    if([self.countryResidence.text isEqualToString:@"India"])
    {
        [self.finalCTDictonary setObject:@"individual" forKey:@"accounttype"];
    }else
    {
        [self.finalCTDictonary setObject:@"joint" forKey:@"accounttype"];
        [self.finalCTDictonary setObject:self.subAccountTypeButton.titleLabel.text forKey:@"subaccount"];
    }
    [self.finalCTDictonary setObject:self.titleButton.titleLabel.text forKey:@"title"];
    [self.finalCTDictonary setObject:@"N" forKey:@"optln"];
    [self.finalCTDictonary setObject:@"192.168.0.1" forKey:@"hostreferer"];
    [self.finalCTDictonary setObject:@"192.168.0.1" forKey:@"ipaddress"];
    [self.finalCTDictonary setObject:@"MobileBrowser" forKey:@"useragent"];
    
    
    NSString *str = self.dobButton.titleLabel.text; /// here this is your date with format yyyy-MM-dd
     dateFormatter.dateStyle=NSDateFormatterLongStyle;
    //// here set format of date which is in your output date (means above str with format)
    
    NSDate *date = [dateFormatter dateFromString: str]; // here you can fetch date from string with define format
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd"];// here set format which you want...
    
    NSString *convertedString = [dateFormatter stringFromDate:date]; //here convert date in NSString
    //NSLog(@"Converted String : %@",convertedString);
   
        //need to set date format to YYYY-DD-MM
    [self.finalCTDictonary setObject:convertedString forKey:@"dob"];
    
    [self.finalCTDictonary setObject:self.maritalStatusButton.titleLabel.text forKey:@"maritalstatus"];
    
    if([self.genderButton.titleLabel.text isEqualToString:@"Male"])
    {
        [self.finalCTDictonary setObject:@"M" forKey:@"gender"];
    }else
    {
        [self.finalCTDictonary setObject:@"F" forKey:@"gender"];
    }
    //Delegate
    
    
    [delegate.finalChoiceTradeDict setObject:@"non_resident" forKey:@"citizenship"];
    [delegate.finalChoiceTradeDict setObject:@"zambala" forKey:@"password"];
    [delegate.finalChoiceTradeDict setObject:self.countryOfBirthTF.text forKey:@"countryofbirth"];
    [delegate.finalChoiceTradeDict setObject:self.countryResidence.text forKey:@"countryresidence"];
    [delegate.finalChoiceTradeDict setObject:self.citizenshipTF.text forKey:@"countrycitizenship"];
    [delegate.finalChoiceTradeDict setObject:self.emailTF.text forKey:@"email"];
    [delegate.finalChoiceTradeDict setObject:self.firstNameTF.text forKey:@"firstname"];
    if(self.middleTF.text.length>0)
    {
    [delegate.finalChoiceTradeDict setObject:[self.middleTF.text substringToIndex:1] forKey:@"middlename"];
    }
    [delegate.finalChoiceTradeDict setObject:self.lastNameTF.text forKey:@"lastname"];
    [delegate.finalChoiceTradeDict setObject:self.govtIDTF.text forKey:@"govtid"];
    if([self.countryResidence.text isEqualToString:@"India"])
    {
        [delegate.finalChoiceTradeDict setObject:@"individual" forKey:@"accounttype"];
    }else
    {
        [delegate.finalChoiceTradeDict setObject:@"joint" forKey:@"accounttype"];
        [delegate.finalChoiceTradeDict setObject:self.subAccountTypeButton.titleLabel.text forKey:@"subaccount"];
    }
    [delegate.finalChoiceTradeDict setObject:self.titleButton.titleLabel.text forKey:@"title"];
    [delegate.finalChoiceTradeDict setObject:@"N" forKey:@"optln"];
    [delegate.finalChoiceTradeDict setObject:@"192.168.0.1" forKey:@"hostreferer"];
    [delegate.finalChoiceTradeDict setObject:@"192.168.0.1" forKey:@"ipaddress"];
    [delegate.finalChoiceTradeDict setObject:@"MobileBrowser" forKey:@"useragent"];
    
    
    NSString *str1 = self.dobButton.titleLabel.text; /// here this is your date with format yyyy-MM-dd
    dateFormatter.dateStyle=NSDateFormatterLongStyle;
    //// here set format of date which is in your output date (means above str with format)
    
    NSDate *date1 = [dateFormatter dateFromString: str1]; // here you can fetch date from string with define format
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd"];// here set format which you want...
    
    NSString *convertedString1 = [dateFormatter stringFromDate:date1]; //here convert date in NSString
    //NSLog(@"Converted String : %@",convertedString1);
    
    //need to set date format to YYYY-DD-MM
    [delegate.finalChoiceTradeDict setObject:convertedString1 forKey:@"dob"];
    
    [delegate.finalChoiceTradeDict setObject:self.maritalStatusButton.titleLabel.text forKey:@"maritalstatus"];
    
    if([self.genderButton.titleLabel.text isEqualToString:@"Male"])
    {
        [delegate.finalChoiceTradeDict setObject:@"M" forKey:@"gender"];
    }else
    {
        [delegate.finalChoiceTradeDict setObject:@"F" forKey:@"gender"];
    }
}

-(void)sendToZambalaServer
{
   NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@userlogininfo/notifyopenaccount",delegate.baseUrl]]
                                      cachePolicy:NSURLRequestUseProtocolCachePolicy
                                  timeoutInterval:10.0];
    
  NSDictionary*headers = @{ @"cache-control": @"no-cache",
                 @"content-type": @"application/json",
                 @"authtoken":delegate.zenwiseToken,
                @"deviceid":delegate.currentDeviceId
                 };
    NSString * name = [self.firstNameTF.text stringByAppendingString:self.lastNameTF.text];
  NSDictionary *parameters = @{
                               @"userid":delegate.userID,
                               @"name":name,
                               @"mobileno":self.mobileNumberTF.text,
                               @"email":self.emailTF.text,
                               @"referenceno":delegate.appIDString
                   };
    
    NSData * postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    
                                                    if(data!=nil)
                                                    {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            if([httpResponse statusCode]==403)
                                                            {
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                
                                                            }
                                                            else if([httpResponse statusCode]==401)
                                                            {
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                
                                                            }
                                                            else
                                                            {
                                                                
                                                            NSMutableDictionary * sampleDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            //NSLog(@"Sample Dict:%@",sampleDict);
                                                            NSString * check = [sampleDict objectForKey:@"message"];
                                                            if([check containsString:@"fowarded"])
                                                            {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    [activityIndicatorView stopAnimating];
                                                                    [backView removeFromSuperview];
                                                                    NSUserDefaults * openAccountPref = [NSUserDefaults standardUserDefaults];
                                                                    [openAccountPref setObject:@"step2" forKey:@"openAccountStep"];
                                                                    [openAccountPref synchronize];
                                                                    
                                                                    NSUserDefaults * openAppIDPref = [NSUserDefaults standardUserDefaults];
                                                                    [openAppIDPref setObject:delegate.appIDString forKey:@"openAppID"];
                                                                    [openAppIDPref synchronize];
                                                                    
                                                                    NSUserDefaults * openMobPref = [NSUserDefaults standardUserDefaults];
                                                                    [openMobPref setObject:self.mobileNumberTF.text forKey:@"openMob"];
                                                                    [openMobPref synchronize];
                                                                    
                                                                    NSUserDefaults * openFinalDict = [NSUserDefaults standardUserDefaults];
                                                                    [openFinalDict setObject:delegate.finalChoiceTradeDict forKey:@"openDict"];
                                                                    [openFinalDict synchronize];
                                                                    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
                                                                    [mixpanelMini track:@"usa_open_account_step1_completed_page"];
                                                                    TwoUSOpenAccountViewController * two = [self.storyboard instantiateViewControllerWithIdentifier:@"TwoUSOpenAccountViewController"];
                                                                    two.mobileNumber=self.mobileNumberTF.text;
                                                                    [self.navigationController pushViewController:two animated:YES];
                                                                });
                                                            }else
                                                            {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! Something went wrong." preferredStyle:UIAlertControllerStyleAlert];
                                                                    
                                                                    [self presentViewController:alert animated:YES completion:^{
                                                                        
                                                                    }];
                                                                    
                                                                    
                                                                    UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                        [self sendToZambalaServer];
                                                                    }];
                                                                    
                                                                    UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                        
                                                                    }];
                                                                    
                                                                    
                                                                    [alert addAction:retryAction];
                                                                    [alert addAction:cancel];
                                                                    
                                                                });
                                                            }
                                                            }
                                                            
                                                    }
                                                    }
                                                    
                                                    else
                                                    {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            
                                                            UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                [self sendToZambalaServer];
                                                            }];
                                                            
                                                            UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                
                                                            }];
                                                            
                                                            
                                                            [alert addAction:retryAction];
                                                            [alert addAction:cancel];
                                                            
                                                        });
                                                        
                                                        
                                                    }
                                                    
                                                }];
    
    [dataTask resume];
}

-(void)createAccountServer
{
    backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.view addSubview:backView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeFiveDots tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [backView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    
    NSString * apiKey = [NSString stringWithFormat:@"Bearer %@",delegate.apiKey];
    
    //PvyKTESA2t-NzGR1IjF74
    NSDictionary *headers = @{ @"Content-Type": @"application/x-www-form-urlencoded",
                               @"Authorization": apiKey,
                                };
//    NSString * citizenship = [NSString stringWithFormat:@"Citizenship=%@",[self.finalCTDictonary objectForKey:@"citizenship"]];
    NSString * accountType = [NSString stringWithFormat:@"&AccountType=%@",[self.finalCTDictonary objectForKey:@"accounttype"]];
    NSString * email = [NSString stringWithFormat:@"&Email=%@",[self.finalCTDictonary objectForKey:@"email"]];
//    NSString * dob = [NSString stringWithFormat:@"&DOB=%@",[self.finalCTDictonary objectForKey:@"dob"]];
//    NSString * firstName = [NSString stringWithFormat:@"&FirstName=%@",[self.finalCTDictonary objectForKey:@"firstname"]];
//
//    NSString * middleName = [NSString stringWithFormat:@"&MiddleInitial=%@",[self.finalCTDictonary objectForKey:@"middlename"]];
//    NSString * lastName = [NSString stringWithFormat:@"&LastName=%@",[self.finalCTDictonary objectForKey:@"lastname"]];
//    NSString * govtID = [NSString stringWithFormat:@"&GovtID=%@",[self.finalCTDictonary objectForKey:@"govtid"]];
//    NSString * maritalStatus = [NSString stringWithFormat:@"&MaritalStatus=%@",[self.finalCTDictonary objectForKey:@"maritalstatus"]];
//    NSString * gender = [NSString stringWithFormat:@"&Gender=%@",[self.finalCTDictonary objectForKey:@"gender"]];
    if([self.countryResidence.text isEqualToString:@"India"])
    {
        CountryBirth = @"&CountryBirth=IND";
        CountryResidence= @"&CountryResidence=IND";
        CountryCitizenship = @"&CountryCitizenship=IND";
    }else
    {
    @try
    {
        //iso3code
        CountryBirth = [NSString stringWithFormat:@"&CountryBirth=%@",[[self.selectedCompanyArray objectAtIndex:0] objectForKey:@"iso3code"]];
         CountryResidence = [NSString stringWithFormat:@"&CountryResidence=%@",[[self.selectedCompanyArray objectAtIndex:0] objectForKey:@"iso3code"]];
        CountryCitizenship = [NSString stringWithFormat:@"&CountryCitizenship=%@",[[self.selectedCompanyArray objectAtIndex:0] objectForKey:@"iso3code"]];
    }
    @catch (NSException * e) {
        CountryBirth = [NSString stringWithFormat:@"&CountryBirth=%@",[[self.selectedCompanyArray objectAtIndex:0] objectForKey:@"sortname"]];
        CountryResidence = [NSString stringWithFormat:@"&CountryResidence=%@",[[self.selectedCompanyArray objectAtIndex:0] objectForKey:@"sortname"]];
        CountryCitizenship = [NSString stringWithFormat:@"&CountryCitizenship=%@",[[self.selectedCompanyArray objectAtIndex:0] objectForKey:@"sortname"]];
    }
    @finally {
        //NSLog(@"finally");
    }
    }
    
    
    NSMutableData *postData = [[NSMutableData alloc] initWithData:[@"Citizenship=non_resident" dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[CountryBirth dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[CountryResidence dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[CountryCitizenship dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[accountType dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"&Source=ZW483" dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[email dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"&Password=Zambala" dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"&OptIn=N" dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"&HostReferer=192.168.0.1" dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"&IPAddress=192.168.0.1" dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"&UserAgent=mobilebrowser" dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"&Step=0" dataUsingEncoding:NSUTF8StringEncoding]];
   
//    [postData appendData:[@"&Title=Mr" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[firstName dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[middleName dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[lastName dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[govtID dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[dob dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[maritalStatus dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[gender dataUsingEncoding:NSUTF8StringEncoding]];
    
//    [postData appendData:[@"&NumberDependents=2" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&Paddress1=SURVEY NO 31, ROAD NO 14 LANE NO 03, CHOUDHARY NAGAR" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&Paddress2=TINGRE NAGAR DHANORI" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&Pcity=Pune" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&Pstate=Maharastra" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&Pzip=411015" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&Pcountry=India" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&MailingDiff=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&MobilePhone=9867987243" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&HomePhone=9867987243" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&OwnerDisclosure=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&EmployStatus=Employed" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&Occupation=Software Engineer" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&YearsEmployed=7" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&CompanyName=Zenwise Technologies Private Limited." dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&TypeBusiness=IT" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&BusinessAddress=Level 6, Pentagon 2, Magarpatta city." dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&BusinessCity=Pune" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&BusinessZip=411013" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&BusinessState=Maharastra" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&BusinessCountry=IND" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&BusinessPhone=02040147878" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&PrimaryIncomeSrc=Job" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AnnualIncome=A" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&LiquidAssets=A" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&NetWorth=A" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&TaxBracket=10" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&SourceWealth=Personal" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AffiliationMember=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AffiliationMemberFirm=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AffiliationMemberName=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AffiliationMemberPosition=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AffiliationBroker=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AffiliationBrokerFirm=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AffiliationBrokerName=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AffiliationBrokerPosition=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AffiliationForShellBank=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AffiliationForShellBankAgent=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AffiliationShellBank=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AffiliationShellBankService=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AffiliationOtherFunds=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AffiliationFFI=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AffiliationPoliOfficial=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AffiliationBankrupt=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AffiliationLegal=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AffiliationUnpaid=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AffiliationPoliOfficialName=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AffiliationPoliOfficialOrg=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AccountTypeInvest=cashAcc" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&DayTrade=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&SweepCash=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&FundInvest=Y" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&TradeOTC=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&ActiveTrader=Y" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AccountTransfer=N" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&InvestmentObj=B" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&StockYearsExp=1-2" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&StockAvgSize=100-500" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&StockAvgTrans=1-50" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&OptionYears=0" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&OptionSize=0" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&OptionAvgTrans=0" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&BondYears=0" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&BondSize=0" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&BondAvgTrans=0" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&FundYears=1-2" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&FundSize=1-10" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&FundAvgTrans=1-50" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&OptionTradeLevel=level0" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&NonProNYSE=Y" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&NonProNasdaq=Y" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&NonProOpra=Y" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&NonProPink=Y" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AgreeNyse=Y" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AgreeNasdaq=Y" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AgreeOpra=Y" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AgreePink=Y" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AgreeCust=Y" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AgreeMargin=Y" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AgreeOption=Y" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AgreeJoint=Y" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AgreePrivacy=Y" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AgreeEConfirm=Y" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AgreeEstatement=Y" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AgreeEProxy=Y" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AgreeEProspectus=Y" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AgreeETax=Y" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AgreeEReorg=Y" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&AgreeForeignFinder=Y" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&SignatureName=Parag Bhosale" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&SignatureIP=192.168.0.1" dataUsingEncoding:NSUTF8StringEncoding]];
    
     NSString * str = [[NSString alloc]initWithData:postData encoding:NSASCIIStringEncoding];
    
    //NSLog(@"Data:%@",str);
    
    NSString * url = [NSString stringWithFormat:@"%@api/accounts",delegate.producationURL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        NSMutableDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        //NSLog(@"Response Dict:%@",responseDict);
                                                        
                                                        if([[responseDict objectForKey:@"status"]isEqualToString:@"201"])
                                                        {
                                                            delegate.appIDString =[NSString stringWithFormat:@"%@",[responseDict objectForKey:@"app_id"]];
                                                            //NSLog(@"AppID:%@",delegate.appIDString);
                                                            
                                                            NSString * errorText = [NSString stringWithFormat:@"Your App ID is:%@. Please note it somewhere for further assistance.",delegate.appIDString];
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:errorText preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                
                                                              
                                                                [self sendToZambalaServer];
                                                                
                                                                
                                                            }];
                                                            
                                                            [alert addAction:okAction];
                                                            
                                                           
                                                           
                                                        }else
                                                        {
                                                            NSString * errorText = [NSString stringWithFormat:@"%@.Please check the inputs given.",[responseDict objectForKey:@"message"]];
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:errorText preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [self presentViewController:alert animated:YES completion:^{
                                                                
                                                            }];
                                                            
                                                            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                [activityIndicatorView stopAnimating];
                                                                [backView removeFromSuperview];
                                                            }];
                                                            
                                                            [alert addAction:okAction];
                                                        }
                                                        
                                                    }
                                                }];
    [dataTask resume];
}


-(void)onLegalButtonTap
{
    AgreementViewController * agree = [self.storyboard instantiateViewControllerWithIdentifier:@"AgreementViewController"];
    [self.navigationController pushViewController:agree animated:YES];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    // after we have current coordinates, we use this method to fetch the information data of fetched coordinate
    [geocoder reverseGeocodeLocation:[locations lastObject] completionHandler:^(NSArray *placemarks, NSError *error) {
        CLPlacemark *placemark = [placemarks lastObject];
        
        NSString *street = placemark.thoroughfare;
        NSString *city = placemark.locality;
        NSString *posCode = placemark.postalCode;
        NSString *country = placemark.country;
        
        //NSLog(@"we live in %@", country);
        //NSLog(@"we live in %@", street);
        //NSLog(@"we live in %@", city);
        //NSLog(@"we live in %@", posCode);
        
        // stopping locationManager from fetching again
        [locationManager stopUpdatingLocation];
    }];
}


-(void)CurrentLocationIdentifier
{
    //---- For getting current gps location
    CLGeocoder *ceo;
    
    locationManager = [[CLLocationManager alloc]init];
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
    
    //------
    
    ceo= [[CLGeocoder alloc]init];
    [locationManager requestWhenInUseAuthorization];
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
    }
    CLLocationCoordinate2D coordinate;
    
    coordinate.latitude=locationManager.location.coordinate.latitude;
    coordinate.longitude=locationManager.location.coordinate.longitude;
    //CLLocationCoordinate2D  ctrpoint;
    //  ctrpoint.latitude = ;
    //ctrpoint.longitude =f1;
    //coordinate.latitude=23.6999;
    //coordinate.longitude=75.000;
    //    MKPointAnnotation *marker = [MKPointAnnotation new];
    //    marker.coordinate = coordinate;
    //    //NSLog(@"%f",coordinate.latitude);
    //[self.mapView addAnnotation:marker];
    
    
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:coordinate.latitude longitude:coordinate.longitude
                       ];
    [ceo reverseGeocodeLocation:loc
              completionHandler:^(NSArray *placemarks, NSError *error) {
                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
                  //NSLog(@"placemark %@",placemark);
                  //String to hold address
                  NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                  //NSLog(@"addressDictionary %@", placemark.addressDictionary);
                  delegate.addressDict = placemark.addressDictionary;
//                  
//                  self.countryOfBirthTF.text = @"India";
//                  
//                  self.countryResidence.text = @"India";
//                  
//                  self.citizenshipTF.text = @"India";
                  
                  //NSLog(@"%@",delegate.addressDict);
                  //NSLog(@"placemark %@",placemark.region);
                  //NSLog(@"placemark %@",placemark.country);  // Give Country Name
                  //NSLog(@"placemark %@",placemark.locality); // Extract the city name
                  //NSLog(@"location %@",placemark.name);
                  //NSLog(@"location %@",placemark.ocean);
                  //NSLog(@"location %@",placemark.postalCode);
                  //NSLog(@"location %@",placemark.subLocality);
                  
                  //NSLog(@"location %@",placemark.location);
                  //Print the location to console
                  //NSLog(@"I am currently at %@",locatedAt);
                  
                  self.region=[NSString stringWithFormat:@"%@",placemark.region];
                  
                  self.country=[NSString stringWithFormat:@"%@",placemark.country];
                  
                  
                  self.locality=[NSString stringWithFormat:@"%@",placemark.locality];
                  
                  
                  self.name=[NSString stringWithFormat:@"%@",placemark.name];
                  
                  
                  self.ocean=[NSString stringWithFormat:@"%@",placemark.ocean];
                  
                  
                  self.postalCode=[NSString stringWithFormat:@"%@",placemark.postalCode];
                  
                  
                  self.subLocality=[NSString stringWithFormat:@"%@",placemark.subLocality];
                  
                  
                  self.location=[NSString stringWithFormat:@"%@",placemark.location];
                  
                  
                  self.locatedAtt=[NSString stringWithFormat:@"%@",locatedAt];
                  
                  
                  
                  
                  
                  [locationManager stopUpdatingLocation];
              }
     
     ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Check network
-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
            
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    
    
}

-(BOOL)networkStatus
{
    
    
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    
    if(netStatus==NotReachable)
    {
        
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    return YES;
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
