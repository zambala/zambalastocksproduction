//
//  USOpenAccountViewController.h
//  ChoiceTradeAccountOpening
//
//  Created by Zenwise Technologies on 14/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface USOpenAccountViewController : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *citizenshipButton;
@property (weak, nonatomic) IBOutlet UITextField *countryOfBirthTF;
@property (weak, nonatomic) IBOutlet UITextField *countryResidence;
@property (weak, nonatomic) IBOutlet UITextField *citizenshipTF;
@property (weak, nonatomic) IBOutlet UIView *accountTypeView;
@property (weak, nonatomic) IBOutlet UIButton *accountTypeButton;
@property (weak, nonatomic) IBOutlet UIView *subAccountTypeView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *subAccountTypeViewHeightConstraint;

@property (weak, nonatomic) IBOutlet UIButton *subAccountTypeButton;
@property (weak, nonatomic) IBOutlet UITextField *emailTF;
@property (weak, nonatomic) IBOutlet UIButton *titleButton;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTF;
@property (weak, nonatomic) IBOutlet UITextField *middleTF;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTF;
@property (weak, nonatomic) IBOutlet UIButton *dobButton;
@property (weak, nonatomic) IBOutlet UITextField *govtIDTF;
@property (weak, nonatomic) IBOutlet UIButton *maritalStatusButton;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIButton *genderButton;
@property (weak, nonatomic) IBOutlet UIButton *legalButton;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *subAccountTypeTopLayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *accountTypeHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *accountTypeTopConstraint;
@property (weak, nonatomic) IBOutlet UITextField *mobileNumberTF;

@property NSString* region;
@property NSString*country;
@property NSString*locality;
@property NSString*name;
@property NSString*ocean;
@property NSString*postalCode;
@property NSString*subLocality;
@property NSString*location;
@property NSString* locatedAtt;

@property NSMutableArray * dataArray;
@property (weak, nonatomic) IBOutlet UIView *doneView;

@property NSMutableArray * symbols;
@property NSMutableArray * company;
@property NSMutableArray * selectedCompanyArray;
@property NSMutableArray * searchResponseArray;
@property NSMutableArray * removeCountriesArray;
@property NSMutableDictionary * responseDictionary;
@property   NSString * dobString;

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property NSMutableDictionary * finalCTDictonary;
@property Reachability * reach;

@end
