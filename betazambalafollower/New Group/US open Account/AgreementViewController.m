//
//  AgreementViewController.m
//  ChoiceTradeAccountOpening
//
//  Created by Zenwise Technologies on 14/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "AgreementViewController.h"
#import "AgreementWebViewController.h"

@interface AgreementViewController ()

@end

@implementation AgreementViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.userButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.customerButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.riskButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.businessButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.privacyButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.marginButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.orderRoutingButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [self.userButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.customerButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.riskButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.businessButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.privacyButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.marginButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.orderRoutingButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    
    // Do any additional setup after loading the view.
}

-(void)onButtonTap:(UIButton*)sender
{
    AgreementWebViewController * webView = [self.storyboard instantiateViewControllerWithIdentifier:@"AgreementWebViewController"];
    if(sender == self.userButton)
    {
        webView.buttonType=@"user";
    }else if (sender == self.customerButton)
    {
        webView.buttonType=@"customer";
    }else if (sender == self.riskButton)
    {
        webView.buttonType=@"risk";
    }else if (sender == self.businessButton)
    {
        webView.buttonType=@"business";
    }else if (sender == self.privacyButton)
    {
        webView.buttonType=@"privacy";
    }else if (sender == self.marginButton)
    {
        webView.buttonType=@"margin";
    }else if (sender == self.orderRoutingButton)
    {
        webView.buttonType=@"order";
    }
    [self.navigationController pushViewController:webView animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
