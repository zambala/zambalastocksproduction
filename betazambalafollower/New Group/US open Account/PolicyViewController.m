//
//  PolicyViewController.m
//  ChoiceTradeAccountOpening
//
//  Created by Zenwise Technologies on 15/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "PolicyViewController.h"
#import "AppDelegate.h"
#import "ThreeUSOpenAccountViewController.h"
#import "PolicyDetailViewController.h"
#import "AgreementWebViewController.h"
#import "NewPolicyDetailViewController.h"

@interface PolicyViewController ()
{
    AppDelegate * delegate;
}

@end

@implementation PolicyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.customerCheck.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.customerRead.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.accountCheck.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.accountRead.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.optionsCheck.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.optionsRead.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.taxPayerCheck.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.taxpayerRead.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.finderCheck.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.finderRead.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    
   self.customerTitle.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.accountTitle.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.optionsTitle.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.taxpayerTitle.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.agreementTitle.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    
    [self.customerCheck setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    [self.customerCheck setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    
    [self.accountCheck setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    [self.accountCheck setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    
    [self.optionsCheck setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    [self.optionsCheck setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    
    [self.taxPayerCheck setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    [self.taxPayerCheck setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    
    [self.finderCheck setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    [self.finderCheck setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    
    
    [self.customerCheck addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.accountCheck addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.optionsCheck addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.taxPayerCheck addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.finderCheck addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.customerRead addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.accountRead addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.optionsRead addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.taxpayerRead addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.finderRead addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.acceptButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if([delegate.tickCheck isEqualToString:@"tick"])
    {
        self.customerCheck.selected=YES;
        self.accountCheck.selected=YES;
        self.optionsCheck.selected=YES;
        self.taxPayerCheck.selected=YES;
        self.finderCheck.selected=YES;
    }else
    {
        self.customerCheck.selected=NO;
        self.accountCheck.selected=NO;
        self.optionsCheck.selected=NO;
        self.taxPayerCheck.selected=NO;
        self.finderCheck.selected=NO;
    }
    
    // Do any additional setup after loading the view.
}

-(void)onButtonTap:(UIButton*)sender
{
    if(sender == self.customerCheck)
    {
        if(sender.selected==YES)
        {
            self.customerCheck.selected=NO;
        }else if (sender.selected==NO)
        {
            self.customerCheck.selected=YES;
        }
    }else if (sender == self.accountCheck)
    {
        if(sender.selected==YES)
        {
            self.accountCheck.selected=NO;
        }else if (sender.selected==NO)
        {
            self.accountCheck.selected=YES;
        }
    }else if (sender == self.optionsCheck)
    {
        if(sender.selected==YES)
        {
            self.optionsCheck.selected=NO;
        }else if (sender.selected==NO)
        {
            self.optionsCheck.selected=YES;
        }
    }else if (sender == self.taxPayerCheck)
    {
        if(sender.selected==YES)
        {
            self.taxPayerCheck.selected=NO;
        }else if (sender.selected==NO)
        {
            self.taxPayerCheck.selected=YES;
        }
    }else if (sender == self.finderCheck)
    {
        if(sender.selected==YES)
        {
            self.finderCheck.selected=NO;
        }else if (sender.selected==NO)
        {
            self.finderCheck.selected=YES;
        }
    }else if (sender == self.acceptButton)
    {
        
    if(self.customerCheck.selected==YES&&self.accountCheck.selected==YES&&self.optionsCheck.selected==YES&&self.taxPayerCheck.selected==YES&&self.finderCheck.selected==YES)
        {
            delegate.tickCheck=@"tick";
        }else
        {
            delegate.tickCheck=@"";
        }
        ThreeUSOpenAccountViewController * three = [self.storyboard instantiateViewControllerWithIdentifier:@"ThreeUSOpenAccountViewController"];
        [self.navigationController pushViewController:three animated:YES];
        
       // [self dismissViewControllerAnimated:YES completion:nil];
       // [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }else if (sender == self.customerRead)
    {
        NewPolicyDetailViewController * policy = [self.storyboard instantiateViewControllerWithIdentifier:@"NewPolicyDetailViewController"];
        policy.buttonType=@"cread";
        [self.navigationController pushViewController:policy animated:YES];
//        AgreementWebViewController * webView = [self.storyboard instantiateViewControllerWithIdentifier:@"AgreementWebViewController"];
//        webView.buttonType=@"ccheck";
//        [self.navigationController pushViewController:webView animated:YES];
    }else if (sender == self.accountRead)
    {
//        NewPolicyDetailViewController * policy = [self.storyboard instantiateViewControllerWithIdentifier:@"NewPolicyDetailViewController"];
//        policy.buttonType=@"aread";
//        [self.navigationController pushViewController:policy animated:YES];
        AgreementWebViewController * webView = [self.storyboard instantiateViewControllerWithIdentifier:@"AgreementWebViewController"];
        webView.buttonType=@"acheck";
        [self.navigationController pushViewController:webView animated:YES];
    }else if (sender == self.optionsRead)
    {
        NewPolicyDetailViewController * policy = [self.storyboard instantiateViewControllerWithIdentifier:@"NewPolicyDetailViewController"];
        policy.buttonType=@"oread";
        [self.navigationController pushViewController:policy animated:YES];
//        AgreementWebViewController * webView = [self.storyboard instantiateViewControllerWithIdentifier:@"AgreementWebViewController"];
//        webView.buttonType=@"ocheck";
//        [self.navigationController pushViewController:webView animated:YES];
    }else if (sender == self.taxpayerRead)
    {
        NewPolicyDetailViewController * policy = [self.storyboard instantiateViewControllerWithIdentifier:@"NewPolicyDetailViewController"];
        policy.buttonType=@"tread";
        [self.navigationController pushViewController:policy animated:YES];
        
    }else if (sender == self.finderRead)
    {
        NewPolicyDetailViewController * policy = [self.storyboard instantiateViewControllerWithIdentifier:@"NewPolicyDetailViewController"];
        policy.buttonType=@"fread";
        [self.navigationController pushViewController:policy animated:YES];
//        PolicyDetailViewController * policy = [self.storyboard instantiateViewControllerWithIdentifier:@"PolicyDetailViewController"];
//        [self.navigationController pushViewController:policy animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
