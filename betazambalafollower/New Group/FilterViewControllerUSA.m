//
//  FilterViewController.m
//  ZambalaUSA
//
//  Created by guna on 19/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "FilterViewControllerUSA.h"
#import "MarketMonksViewController.h"

@interface FilterViewControllerUSA ()

@end

@implementation FilterViewControllerUSA

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Filter Market Monks";

    self.navigationController.navigationBarHidden=NO;
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *backBtnImage = [UIImage imageNamed:@"screenClose.png"]  ;
    [backBtn setBackgroundImage:backBtnImage forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(goback) forControlEvents:UIControlEventTouchUpInside];
    backBtn.frame = CGRectMake(3,12,36, 36);
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn] ;
    self.navigationItem.leftBarButtonItem = backButton;
    
//    UIImage *NavigationPortraitBackground= [UIImage imageNamed:@"screenClose.png"];
//    [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
    
    self.sectorButton.layer.shadowOffset = CGSizeMake(0, 2);
    self.sectorButton.layer.shadowOpacity =0.1;
    self.sectorButton.layer.shadowRadius = 3.1;
    self.sectorButton.clipsToBounds = NO;
    
    
    self.ratingView.layer.shadowOffset = CGSizeMake(0, 2);
    self.ratingView.layer.shadowOpacity = 0.1;
    self.ratingView.layer.shadowRadius = 3.1;
    self.ratingView.clipsToBounds = NO;
    
    self.rankingView.layer.shadowOffset = CGSizeMake(0, 2);
    self.rankingView.layer.shadowOpacity = 0.1;
    self.rankingView.layer.shadowRadius = 3.1;
    self.rankingView.clipsToBounds = NO;
    
    self.compareView.layer.shadowOffset = CGSizeMake(0, 2);
    self.compareView.layer.shadowOpacity = 0.1;
    self.compareView.layer.shadowRadius = 3.1;
    self.compareView.clipsToBounds = NO;


    
    // Do any additional setup after loading the view.
}
- (void)goback
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backButtonAction:(id)sender {
//    MarketMonksViewController * marketVC=[self.storyboard instantiateViewControllerWithIdentifier:@"market"];
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
