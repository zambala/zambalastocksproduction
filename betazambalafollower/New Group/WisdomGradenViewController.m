//
//  WisdomGradenViewController.m
//  ZambalaUSA
//
//  Created by guna on 19/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "WisdomGradenViewController.h"
#import "HMSegmentedControl.h"
#import "EquitiesBuyTableViewCell.h"
#import "EquitiesSellTableViewCell.h"
#import "TreadingStocksTableViewCell.h"
#import "testTableViewCell.h"
#import "OrderViewController.h"
#import "AppDelegate.h"
#import "NewStockViewController.h"
#import "NewOrderViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <Mixpanel/Mixpanel.h>


@interface WisdomGradenViewController (){
    HMSegmentedControl * segmentedControl;
    UIVisualEffectView *blurView;
    UIVisualEffectView *vibrantView;
    BOOL check;
    AppDelegate * delegate;
    UIRefreshControl * refreshControl1;
    NSMutableString * tickerStoreString;
    NSURL*url;
   
}

@end

@implementation WisdomGradenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    check=true;
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"usa_equity_page"];
    self.trendingStocksResponseArray = [[NSMutableArray alloc]init];
    tickerStoreString = [[NSMutableString alloc]init];
    self.navigationItem.title = @"Equity";
    self.navigationItem.leftBarButtonItem.title=@" ";
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"ANALYST ADVISE"]];
    segmentedControl.frame = CGRectMake(0, 0, self.view.frame.size.width, 54);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    //    segmentedControl.verticalDividerEnabled = YES;
    //    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(251/255.0) green:(196/255.0) blue:(12/255.0) alpha:1];
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
    segmentedControl.backgroundColor=[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1];
    segmentedControl.tintColor=[UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:0.5];
    [self.view addSubview:segmentedControl];
    [segmentedControl removeFromSuperview];
    self.tickerStoreArray = [[NSMutableArray alloc]init];
    self.analystHeaderView.hidden=NO;
    self.analystHeaderViewHeight.constant=37;
    self.tredingHeaderView.hidden=YES;
    self.tredingHeaderViewHeight.constant=0;
    
    self.view2.hidden=YES;
    self.view2Hgt.constant=0;
    
    self.view1.hidden=NO;
    self.view1Hgt.constant=37;
    [self analystAdvice];
    
//    refreshControl = [[UIRefreshControl alloc]init];
//    [self.wisdomGradenTableView addSubview:refreshControl];
//    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    
    refreshControl1 = [UIRefreshControl new];
    [refreshControl1 addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [self.wisdomGradenTableView addSubview:refreshControl1];
    [self.wisdomGradenTableView sendSubviewToBack:refreshControl1];

//    self.WGTableViewHeight.constant=94;
    
    // Do any additional setup after loading the view.
}

- (void)handleRefresh:(UIRefreshControl *)refreshControl {
    
    [self analystAdvice];
    [self.wisdomGradenTableView reloadData];
    [self.wisdomGradenTableView layoutIfNeeded];
    [refreshControl1 endRefreshing];
}

//-(void)refreshTable
//{
//    [self analystAdvice];
//    [refreshControl1 endRefreshing];
//    [self.wisdomGradenTableView reloadData];
//}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}
-(void)segmentedControlChangedValue
{
    if (segmentedControl.selectedSegmentIndex==0) {
        
        self.analystHeaderView.hidden=NO;
        self.analystHeaderViewHeight.constant=37;
        self.tredingHeaderView.hidden=YES;
        self.tredingHeaderViewHeight.constant=0;
//         self.WGTableViewHeight.constant=94;
        
        self.view2.hidden=YES;
        self.view2Hgt.constant=0;
        self.navigationItem.rightBarButtonItem.enabled=NO;
        self.view1.hidden=NO;
        self.view1Hgt.constant=37;
        
        [self analystAdvice];

        [self.wisdomGradenTableView reloadData];
        

    }
    if (segmentedControl.selectedSegmentIndex==1) {
        
        self.analystHeaderView.hidden=YES;
        self.analystHeaderViewHeight.constant=0;
        self.tredingHeaderView.hidden=NO;
        self.tredingHeaderViewHeight.constant=37;
        self.navigationItem.rightBarButtonItem.enabled=NO;
        self.view2.hidden=NO;
        self.view2Hgt.constant=37;
        
        self.view1.hidden=YES;
        self.view1Hgt.constant=0;

//         self.WGTableViewHeight.constant=94;
        [self trendingStocksServerHit];
       
        [self.wisdomGradenTableView reloadData];
        
    }
    if (segmentedControl.selectedSegmentIndex==2) {
        self.analystHeaderView.hidden=YES;
        self.analystHeaderViewHeight.constant=0;
        self.tredingHeaderView.hidden=YES;
        self.tredingHeaderViewHeight.constant=0;
//         self.WGTableViewHeight.constant=94;
        [self.wisdomGradenTableView reloadData];
    }
    
    
    //NSLog(@"hai");
}
-(void)trendingStocksServerHit
{
    @try
    {
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    self.wisdomGradenTableView.hidden=YES;
    NSDictionary *headers = @{ @"x-apikey": @"TR_Zenwise",
                               @"x-apitoken": @"8a342d1a-1bha-21a2-d1d9-3a5cd0fac442",
                                };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://api.tipranks.com/api/Stocks/TrendingStocks?num=20000&daysAgo=30"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        self.trendingStocksResponseArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        //NSLog(@"Trending Response:%@",self.trendingStocksResponseArray);
                                                        if(self.tickerStoreArray.count>0)
                                                        {
                                                            [self.tickerStoreArray removeAllObjects];
                                                        }
                                                        if(tickerStoreString.length>0)
                                                        {
                                                            tickerStoreString=[[NSString stringWithFormat:@""] mutableCopy];
                                                        }
                                                        
                                            for (int i=0; i<self.trendingStocksResponseArray.count; i++) {
                                                [self.tickerStoreArray addObject:[[self.trendingStocksResponseArray objectAtIndex:i] objectForKey:@"ticker"]];
                                                
                                                            
                                                        }
                                                        tickerStoreString = [[self.tickerStoreArray componentsJoinedByString:@","] mutableCopy];
                                                        
                                                    }
                                                    //NSLog(@"Ticker Store string:%@",tickerStoreString);
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        
                                                         [self LTPServer];
                                                        
                                                       
                                                        
                                                        
                                                        
                                                    });
                                                }];
    [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
-(void)analystAdvice
{
    @try
    {
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    self.wisdomGradenTableView.hidden=YES;
    NSDictionary *headers = @{ @"x-apikey": @"TR_Zenwise",
                               @"x-apitoken": @"8a342d1a-1bha-21a2-d1d9-3a5cd0fac442",
                               @"cache-control": @"no-cache",
                               };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://api.tipranks.com/api/LiveFeed?num=30"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        self.analystAdviceArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                
                                                        //NSLog(@"Analyst Advice:%@",self.analystAdviceArray);
                                                        
                                                        if(self.tickerStoreArray.count>0)
                                                        {
                                                            [self.tickerStoreArray removeAllObjects];
                                                        }
                                                        if(tickerStoreString.length>0)
                                                        {
                                                            tickerStoreString=[[NSString stringWithFormat:@""] mutableCopy];
                                                        }
                                                    for (int i=0; i<self.analystAdviceArray.count; i++) {
                                                        [self.tickerStoreArray addObject:[[self.analystAdviceArray objectAtIndex:i] objectForKey:@"stockTicker"]];
                                                        }
                                                        tickerStoreString = [[self.tickerStoreArray componentsJoinedByString:@","] mutableCopy];
                                                        //NSLog(@"Ticker store array:%@",self.tickerStoreArray);
                                                        //NSLog(@"Ticker store string:%@",tickerStoreString);
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                         [self LTPServer];
                                                        
                                                     
                                                        
                                                        
                                                        
                                                        
                                                    });
                                                }];
    [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}


-(void)LTPServer
{
    @try
    {
        NSDictionary *headers = @{ @"cache-control": @"no-cache",
                                   @"deviceid":delegate.currentDeviceId,
                                   @"authtoken":delegate.zenwiseToken
                                   };
    
    NSString * url1 = [NSString stringWithFormat:@"%@%@",delegate.usLTPURL,tickerStoreString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url1]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        self.ltpResponseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        unsigned long a = [[[self.ltpResponseDictionary objectForKey:@"results"] objectForKey:@"quote"] count];
                                                        self.ltpResponseArray = [[NSMutableArray alloc]init];
                                                        for (int i=0; i<a; i++) {
                                                            [self.ltpResponseArray addObject:[[[self.ltpResponseDictionary objectForKey:@"results"]objectForKey:@"quote"]objectAtIndex:i]];
                                                        }
                                                        
                                                        
                                                        //NSLog(@"LTP:%@",self.ltpResponseArray);

                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            self.wisdomGradenTableView.hidden=NO;
                                                            self.wisdomGradenTableView.delegate=self;
                                                            self.wisdomGradenTableView.dataSource=self;
                                                            [self.wisdomGradenTableView reloadData];
                                                            self.activityIndicator.hidden=YES;
                                                            [self.activityIndicator stopAnimating];
                                                        });
                                                    }
                                                }];
    [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}





- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(segmentedControl.selectedSegmentIndex==1)
    {
        return self.trendingStocksResponseArray.count;
    }
    
    else if(segmentedControl.selectedSegmentIndex==0)
    {
        return self.analystAdviceArray.count;
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @try
    {
    if (segmentedControl.selectedSegmentIndex==0) {
        
     //   if([[self.ltpResponseArray objectAtIndex:indexPath.row] objectForKey:@"ticker"] isEqualToString:[NSString stringWithFormat:@"%@",[[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"stockTicker"]])
        
            
//        if (indexPath.row%2==0) {
            EquitiesBuyTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"buy" forIndexPath:indexPath];
//        cell.layer.shadowRadius  = 1.5f;
//        cell.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
//        cell.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
//        cell.layer.shadowOpacity = 0.9f;
//        cell.layer.masksToBounds = NO;
        
        cell.buySellBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
        cell.buySellBtn.layer.shadowOffset = CGSizeMake(0, 2.0f);
        cell.buySellBtn.layer.shadowOpacity = 1.0f;
        cell.buySellBtn.layer.shadowRadius = 2.0f;
        cell.buySellBtn.layer.cornerRadius=1.0f;
        cell.buySellBtn.layer.masksToBounds = NO;
        
        UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
        UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(cell.bounds, shadowInsets)];
        cell.layer.shadowPath    = shadowPath.CGPath;
        float priceTargetFloat;
        
        for (int i=0; i<self.ltpResponseArray.count; i++) {
            NSString * testString = [[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"stockTicker"];
            NSString * testString1 = [[[self.ltpResponseArray objectAtIndex:i] objectForKey:@"equityinfo"] objectForKey:@"shortname"];
            if([testString isEqualToString:testString1])
            {
                //NSLog(@"Teststring:%@",testString);
                //NSLog(@"Teststring1:%@",testString1);
                NSString * ltp = [NSString stringWithFormat:@"%@",[[[[self.ltpResponseArray objectAtIndex:indexPath.row] objectForKey:@"pricedata"] objectForKey:@"last"] stringValue]];
                
                
                float ltpInt = [ltp floatValue];
                cell.LtpLbl.text = [NSString stringWithFormat:@"%.2f",ltpInt];
                
                NSString * priceTarget = [NSString stringWithFormat:@"%@",[[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"priceTarget"]];
                if([priceTarget isEqual:[NSNull null]]||[priceTarget isEqualToString:@"<null>"])
                {
                    priceTargetFloat = 0;
                }else
                {
                    priceTargetFloat = [[NSString stringWithFormat:@"%@",[[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"priceTarget"]] floatValue];
                }
                float upsideCalculation = ((priceTargetFloat-ltpInt)/(ltpInt))*100;
                NSString * upsideString = [NSString stringWithFormat:@"%.2f",upsideCalculation];
                
                //NSLog(@"Upside calculation:%f",upsideCalculation);
                NSString * percent1 = @"%";
                
                if([upsideString containsString:@"-"])
                {
                    if(priceTargetFloat==0)
                    {
                        cell.percentageLbl.text = @"0";
                    }
                    else
                    {
                    cell.percentageLbl.textColor = [UIColor colorWithRed:(242.0)/255 green:(30.0)/255 blue:(51.0)/255 alpha:1];
                    cell.percentageLbl.text =[NSString stringWithFormat:@"%.2f%@(%@)",upsideCalculation,percent1,@"upside"];
                    }
                }else
                {
                    if(priceTargetFloat==0)
                    {
                        cell.percentageLbl.text= @"0%(Upside)";
                    }else
                    {
                    cell.percentageLbl.textColor = [UIColor colorWithRed:(27.0)/255 green:(160.0)/255 blue:(33.0)/255 alpha:1];
                    cell.percentageLbl.text =[NSString stringWithFormat:@"%.2f%@(%@)",upsideCalculation,percent1,@"upside"];
                    }
                }
                
                //from here
                
                //Image
                NSString *nulll=[[self.analystAdviceArray objectAtIndex:indexPath.row]objectForKey:@"expertPictureUrl"];
                
                
                if(![nulll isEqual:[NSNull null]])
                {
                    NSString *ImageURL = [NSString stringWithFormat:@"https://az712682.vo.msecnd.net/expert-pictures/%@",[[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"expertPictureUrl"]];
                    
                    @try {
                        url = [NSURL URLWithString:ImageURL];
                    } @catch (NSException *exception) {
                        
                        
                        
                    } @finally {
                        
                    }
                    
                    
                    
                    
                    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                        if (data) {
                            UIImage *image = [UIImage imageWithData:data];
                            if (image) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    //                            EquitiesCell1 *updateCell = (id)[tableView cellForRowAtIndexPath:indexPath];
                                    //                            if (updateCell)
                                    cell.analystImg.image = image;
                                });
                            }
                        }
                    }];
                    [task resume];
                    
                    
                    //            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:ImageURL]];
                    //            cell.PIV.image = [UIImage imageWithData:imageData];
                }else
                {
                    NSString *ImageURL = @"https://www.tipranks.com/images/analyst-fallback_tsqr.png";
                    
                    @try {
                        url = [NSURL URLWithString:ImageURL];
                    } @catch (NSException *exception) {
                        
                        
                        
                    } @finally {
                        
                    }
                    
                    
                    
                    
                    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                        if (data) {
                            UIImage *image = [UIImage imageWithData:data];
                            if (image) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    //                            EquitiesCell1 *updateCell = (id)[tableView cellForRowAtIndexPath:indexPath];
                                    //                            if (updateCell)
                                    cell.analystImg.image = image;
                                });
                            }
                        }
                    }];
                    [task resume];
                    
                    
                    //            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:ImageURL]];
                    //            cell.PIV.image = [UIImage imageWithData:imageData];
                }

                
                
                
                
                NSString * analystName=[NSString stringWithFormat:@"%@",[[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"analystName"]];
                
                NSString * analystName1=[NSString stringWithFormat:@",%@",[[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"firmName"]];
                
                cell.analystName.text=[NSString stringWithFormat:@"%@%@",analystName,analystName1];
                
                NSString * sector=[NSString stringWithFormat:@"Sector: %@",[[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"sector"]];
                
                cell.sectorLbl.text=sector;
                
                cell.companyName.text=[[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"companyName"];
                
                cell.tradingSymbol.text=[[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"stockTicker"];
                
                NSString * price=[NSString stringWithFormat:@"%@",[[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"priceTarget"]];
                
                if([price isEqual:[NSNull null]]||[price isEqualToString:@"<null>"])
                {
                    cell.priceTrgetLbl.text= @"N/A";
                }else
                {
                    cell.priceTrgetLbl.text=[NSString stringWithFormat:@"$%@",price];
                }
                
                cell.adviceDate.text=[NSString stringWithFormat:@"%@",[[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"recommendationDate"]];
                
                cell.transactionTypeLbl.text=[NSString stringWithFormat:@"%@",[[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"recommendation"]];
                
                NSString * percent = @"%";
                
                
                NSString * success = [NSString stringWithFormat:@"%@",[[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"successRate"]];
                float successFloat = [success floatValue]*100;
                NSString * successRate = [NSString stringWithFormat:@"%.f%@",successFloat,percent];
                
                cell.successRateLbl.text=successRate;
                
                
                NSString * averageReturn =[NSString stringWithFormat:@"%.2f%@",[[[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"excessReturn"] floatValue],percent];
                
                cell.averageLbl.text=averageReturn;
                
                NSString * transaction=[[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"recommendation"];
                
                //NSLog(@"%@",transaction);
                
                if([transaction isEqualToString:@"Hold"])
                {
                    
                    transaction=@"HOLD";
                }
                else if([transaction isEqualToString:@"Buy"])
                {
                    
                    transaction=@"BUY";
                }
                
                else if([transaction isEqualToString:@"Sell"])
                {
                    
                    transaction=@"SELL";
                }
                
                
                //Upside calculation
                
                
                // cell.changePercentageLbl.text = [NSString stringWithFormat:@"%.2f",[[[self.ltpResponseArray objectAtIndex:indexPath.row] objectForKey:@"changePercent"] floatValue]];
                
                
                NSString * numOfStars = [[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"numOfStars"];
                float floatStar= [numOfStars floatValue];
                //NSLog(@"Float Star:%f",floatStar);
                cell.starRatingView.minimumValue=0;
                cell.starRatingView.maximumValue=5;
                cell.starRatingView.value=floatStar;
                cell.starRatingView.allowsHalfStars=YES;
                cell.starRatingView.allowsHalfStars=YES;
                cell.starRatingView.emptyStarImage = [UIImage imageNamed:@"ratingstarEmpty"];
                cell.starRatingView.halfStarImage = [UIImage imageNamed:@"starsmallHalf"]; // optional
                cell.starRatingView.filledStarImage = [UIImage imageNamed:@"ratingstarFilled"];
                [cell.starRatingView setUserInteractionEnabled:NO];
                cell.buySellBtn.titleLabel.text=[NSString stringWithFormat:@"%@",[[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"recommendation"]];
                
                [cell.buySellBtn setTitle:transaction forState:UIControlStateNormal];
                
                if([cell.buySellBtn.currentTitle isEqualToString:@"HOLD"])
                {
                    [cell.buySellBtn setBackgroundColor:[UIColor colorWithRed:(85/255.0) green:(90/255.0) blue:(92/255.0) alpha:1.0f]];
                    
                }
                
                else if([cell.buySellBtn.currentTitle isEqualToString:@"BUY"])
                {
                    [cell.buySellBtn setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1.0f]];
                    
                }
                
                else if([cell.buySellBtn.currentTitle isEqualToString:@"SELL"])
                {
                    [cell.buySellBtn setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1.0f]];
                    
                }

  
            }
        }
        
       
        
        
        [cell.buySellBtn addTarget:self action:@selector(onBuyorSellButtonTap:) forControlEvents:UIControlEventTouchUpInside];

            
            return cell;
//        }
//       else
//       {
//           if (indexPath.row%3==0 && check==true) {
//               EquitiesSellTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"sell" forIndexPath:indexPath];
//               
////               UIView * clearView=[[UIView alloc]initWithFrame:CGRectMake(0, 50, cell.contentView.frame.size.width, 110)];
////               clearView.backgroundColor=[UIColor colorWithRed:247/255.0 green:241/255.0 blue:221/255.0 alpha:0.7f];
////               [cell.contentView addSubview:clearView];
//               
////               
//               
//                       UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
//               
//                                   blurView = [[UIVisualEffectView alloc]initWithEffect:blur];
//                                   blurView.frame = CGRectMake(0, 50, cell.contentView.frame.size.width, 110);
//                                   blurView.layer.opacity=0.8f;
//                                   [cell addSubview:blurView];
//               check=false;
////                           vibrantView = [[UIVisualEffectView alloc]initWithEffect:[UIVibrancyEffect effectForBlurEffect:blur]];
////                           vibrantView.frame = blurView.bounds;
////                           vibrantView.layer.opacity=0.5f;
////                           
////                           [blurView.contentView addSubview:vibrantView];
//
//               UIButton * subscribetoPremiumButton=[[UIButton alloc]initWithFrame:CGRectMake(75, 48, cell.contentView.frame.size.width-150, 41.6)];
//               subscribetoPremiumButton.backgroundColor=[UIColor colorWithRed:241/255.0 green:165/255.0 blue:75/255.0 alpha:1.0f];
//               [subscribetoPremiumButton setTitle:@"Subscribe to Premium" forState:UIControlStateNormal];
//               subscribetoPremiumButton.tintColor=[UIColor whiteColor];
//               subscribetoPremiumButton.layer.shadowOffset = CGSizeMake(2,3.5);
//               subscribetoPremiumButton.layer.shadowOpacity = 0.1;
//               subscribetoPremiumButton.layer.shadowRadius = 4.2;
//               subscribetoPremiumButton.clipsToBounds = NO;
//               
//               [blurView addSubview:subscribetoPremiumButton];
//               
//               
//               
//               
//               cell.layer.shadowOffset = CGSizeMake(0,2);
//               cell.layer.shadowOpacity = 0.1;
//               cell.layer.shadowRadius = 3.1;
//               cell.clipsToBounds = NO;
//               
//               return cell;
//
           }
//           else
//           {
//               EquitiesSellTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"sell" forIndexPath:indexPath];
//               
//               cell.layer.shadowOffset = CGSizeMake(0,2);
//               cell.layer.shadowOpacity = 0.1;
//               cell.layer.shadowRadius = 3.1;
//               cell.clipsToBounds = NO;
//               
//               return cell;
//               
//
//  
//           }
    
           
//       }
    
        
//    }
    if (segmentedControl.selectedSegmentIndex==1) {
        
//        TreadingStocksTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"treding" forIndexPath:indexPath];
//        
//        
//        cell.layer.shadowOffset = CGSizeMake(0,2);
//        cell.layer.shadowOpacity = 0.1;
//        cell.layer.shadowRadius = 3.1;
//        cell.clipsToBounds = NO;
//        
//        return cell;
        
      
        TreadingStocksTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"hai" forIndexPath:indexPath];
        
        
//        cell.layer.shadowRadius  = 1.5f;
//        cell.layer.shadowColor   = [UIColor colorWithRed:0.f/255.f green:0.f/255.f blue:0.f/0.f alpha:1.f].CGColor;
//        cell.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
//        cell.layer.shadowOpacity = 0.9f;
//        cell.layer.masksToBounds = NO;
        
        cell.consensusLabel.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
        cell.consensusLabel.layer.shadowOffset = CGSizeMake(0, 2.0f);
        cell.consensusLabel.layer.shadowOpacity = 1.0f;
        cell.consensusLabel.layer.shadowRadius = 2.0f;
        cell.consensusLabel.layer.cornerRadius=1.0f;
        cell.consensusLabel.layer.masksToBounds = NO;
        
        cell.tickerNameLabel.text = [[self.trendingStocksResponseArray objectAtIndex:indexPath.row] objectForKey:@"ticker"];
         NSString * percent = @"%";
        NSString * changePercent = [NSString stringWithFormat:@"%.2f%@",[[[[self.ltpResponseArray objectAtIndex:indexPath.row] objectForKey:@"pricedata"] objectForKey:@"changepercent"] floatValue],percent];
    
        cell.changePercentLabel.text = changePercent;
        
        if([changePercent containsString:@"-"])
        {
            cell.changePercentLabel.textColor = [UIColor colorWithRed:(242.0)/255 green:(30.0)/255 blue:(51.0)/255 alpha:1];
        }else
        {
            cell.changePercentLabel.textColor = [UIColor colorWithRed:(27.0)/255 green:(160.0)/255 blue:(33.0)/255 alpha:1];
        }
        
        cell.dateLabel.text = [[self.trendingStocksResponseArray objectAtIndex:indexPath.row] objectForKey:@"lastRatingDate"];
        
        cell.LTPLabel.text = [NSString stringWithFormat:@"%.2f",[[[[self.ltpResponseArray objectAtIndex:indexPath.row] objectForKey:@"pricedata"] objectForKey:@"last"] floatValue]];
        NSString * priceTarget = [NSString stringWithFormat:@"%@",[[self.trendingStocksResponseArray objectAtIndex:indexPath.row] objectForKey:@"averagePriceTarget"]];
        if([priceTarget isEqual:[NSNull null]])
        {
            priceTarget =@"0";
        }else
        {
            priceTarget = [NSString stringWithFormat:@"%@",[[self.trendingStocksResponseArray objectAtIndex:indexPath.row] objectForKey:@"averagePriceTarget"]];
        }
        
        float priceInt = [priceTarget floatValue];
        
        NSString * ltp = [NSString stringWithFormat:@"%.2f",[[[[self.ltpResponseArray objectAtIndex:indexPath.row] objectForKey:@"pricedata"] objectForKey:@"last"] floatValue]];
        float ltpInt = [ltp floatValue];
        
        float upsideCalculation = ((priceInt-ltpInt)/(ltpInt))*100;
        NSString * upsideString = [NSString stringWithFormat:@"%.2f",upsideCalculation];
        
        //NSLog(@"Upside calculation:%f",upsideCalculation);
        cell.upSidePercentLabel.text =[NSString stringWithFormat:@"%.2f%@",upsideCalculation,percent];
        
        if([upsideString containsString:@"-"])
        {
            cell.upSidePercentLabel.textColor = [UIColor colorWithRed:(242.0)/255 green:(30.0)/255 blue:(51.0)/255 alpha:1];
            cell.upSideLabel.text = @"(Downside)";
        }else
        {
            cell.upSidePercentLabel.textColor = [UIColor colorWithRed:(27.0)/255 green:(160.0)/255 blue:(33.0)/255 alpha:1];
            cell.upSideLabel.text = @"(Upside)";
        }
        

        
        
        
        
        NSMutableString * titleString =  [[self.trendingStocksResponseArray objectAtIndex:indexPath.row] objectForKey:@"consensus"];
        
        if([titleString containsString:@"StrongBuy"])
        {
            NSString * Buy = [titleString substringFromIndex:6];
            //NSLog(@"buy:%@",Buy);
            NSString * strong = [titleString substringToIndex:6];
            //NSLog(@"strong:%@",strong);
            NSString * title2 = [[strong stringByAppendingString:@" "]stringByAppendingString:Buy];
            //NSLog(@"Title2:%@",title2);
            [cell.consensusLabel setTitle:title2 forState:UIControlStateNormal];
            
        }else
        {
        
        [cell.consensusLabel setTitle:titleString forState:UIControlStateNormal];
        }
        
        
        
        
      
        if([titleString containsString:@"StrongBuy"]||[titleString containsString:@"Buy"])
        {
            [cell.consensusLabel setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1.0f]];
        }else if ([titleString containsString:@"Sell"])
        {
            [cell.consensusLabel setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1.0f]];
        }else if ([titleString containsString:@"Neutral"]||[titleString containsString:@"Hold"])
        {
            [cell.consensusLabel setBackgroundColor:[UIColor colorWithRed:(85/255.0) green:(90/255.0) blue:(92/255.0) alpha:1.0f]];
        }
        NSString * avgPrice = [NSString stringWithFormat:@"%.2f",[[[self.trendingStocksResponseArray objectAtIndex:indexPath.row] objectForKey:@"averagePriceTarget"]floatValue]];
        NSString * dollar = @"$";
        NSString * avgPriceFinal = [dollar stringByAppendingString:avgPrice];
        
       // //NSLog(@"Average Target:%@",avgPriceFinal);
        cell.averagePriceTarget.text=avgPriceFinal;
        NSString * buy = [NSString stringWithFormat:@"%@",[[self.trendingStocksResponseArray objectAtIndex:indexPath.row] objectForKey:@"buy"]];
        NSString * buy1 = @" Buy";
        NSString * buyFinal = [buy stringByAppendingString:buy1];
        cell.buyLabel.text=buyFinal;
        
        NSString * sell = [NSString stringWithFormat:@"%@",[[self.trendingStocksResponseArray objectAtIndex:indexPath.row] objectForKey:@"sell"]];
        NSString * sell1 = @" Sell";
        NSString * sellFinal = [sell stringByAppendingString:sell1];
        cell.sellLabel.text=sellFinal;
        
        NSString * hold = [NSString stringWithFormat:@"%@",[[self.trendingStocksResponseArray objectAtIndex:indexPath.row] objectForKey:@"sell"]];
        NSString * hold1 = @" Hold";
        NSString * holdFinal = [hold stringByAppendingString:hold1];
        cell.holdLabel.text=holdFinal;
        
        
        cell.companyNameLabel.text= [[self.trendingStocksResponseArray objectAtIndex:indexPath.row] objectForKey:@"companyName"];
        
        
        [cell.consensusLabel addTarget:self action:@selector(onBuySellTapTrending:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
    if (segmentedControl.selectedSegmentIndex==2) {
        testTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"sell1" forIndexPath:indexPath];
        
        
        cell.layer.shadowOffset = CGSizeMake(0,2);
        cell.layer.shadowOpacity = 0.1;
        cell.layer.shadowRadius = 3.1;
        cell.clipsToBounds = NO;
        
        return cell;

        
    }
    return nil;
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
    if(segmentedControl.selectedSegmentIndex==0)
    {
        //NSLog(@"%@",self.analystAdviceArray);
        NewStockViewController * stockDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"NewStockViewController"];
        
        NSString * price=[NSString stringWithFormat:@"%@",[[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"priceTarget"]];
        
        if([price isEqual:[NSNull null]]||[price isEqualToString:@"<null>"])
        {
            stockDetail.priceTargetString=@"0";
        }else
        {
            stockDetail.priceTargetString=price;
        }
        [self.navigationController pushViewController:stockDetail animated:YES];
        
        //NSLog(@"Trendig Stock:%@",[[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"stockTicker"]);
        stockDetail.tickerNameString = [[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"stockTicker"];
        
    }else if(segmentedControl.selectedSegmentIndex==1)
    {
        //NSLog(@"%@",self.trendingStocksResponseArray);
        NewStockViewController * stockDetailTrending = [self.storyboard instantiateViewControllerWithIdentifier:@"NewStockViewController"];
        stockDetailTrending.tickerNameString = [[self.trendingStocksResponseArray objectAtIndex:indexPath.row] objectForKey:@"ticker"];
        [self.navigationController pushViewController:stockDetailTrending animated:YES];
        
        
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (segmentedControl.selectedSegmentIndex==1) {
        return 89;
    }
    else{
        return 168;
    }
}

-(void)onBuySellTapTrending:(UIButton*)sender
{
    @try
    {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.wisdomGradenTableView];
    NSIndexPath *indexPath = [self.wisdomGradenTableView indexPathForRowAtPoint:buttonPosition];
    //NSLog(@"Index path:%ld",(long)indexPath.row);
    NewOrderViewController * trendingOrders = [self.storyboard instantiateViewControllerWithIdentifier:@"NewOrderViewController"];
    delegate.stockTickerString = [[self.trendingStocksResponseArray objectAtIndex:indexPath.row] objectForKey:@"ticker"];
    delegate.orderRecomdationType = [[self.trendingStocksResponseArray objectAtIndex:indexPath.row] objectForKey:@"consensus"];
    trendingOrders.recomondation =  [[self.trendingStocksResponseArray objectAtIndex:indexPath.row] objectForKey:@"consensus"];
    
    trendingOrders.tickerName = [[self.trendingStocksResponseArray objectAtIndex:indexPath.row] objectForKey:@"ticker"];
    trendingOrders.companyName = [[self.trendingStocksResponseArray objectAtIndex:indexPath.row] objectForKey:@"companyName"];
    
    [self.navigationController pushViewController:trendingOrders animated:YES];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
-(void)onBuyorSellButtonTap:(UIButton*)sender
{
    @try
    {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.wisdomGradenTableView];
    NSIndexPath *indexPath = [self.wisdomGradenTableView indexPathForRowAtPoint:buttonPosition];
    //NSLog(@"Index path:%ld",(long)indexPath.row);
//    EquitiesBuyTableViewCell *cell = [self.wisdomGradenTableView cellForRowAtIndexPath:indexPath];
    delegate.stockTickerString = [[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"stockTicker"];
    delegate.orderRecomdationType = [[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"recommendation"];
    //NSLog(@"%@",delegate.stockTickerString);
    NewOrderViewController * orders = [self.storyboard instantiateViewControllerWithIdentifier:@"NewOrderViewController"];
    orders.recomondation =  [[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"recommendation"];
    orders.tickerName = [[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"stockTicker"];
    orders.companyName = [[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"companyName"];
    [self.navigationController pushViewController:orders animated:YES];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}


    // Do any additional setup after loading the view.


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
