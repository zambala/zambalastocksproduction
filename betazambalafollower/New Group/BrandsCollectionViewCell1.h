//
//  BrandsCollectionViewCell1.h
//  ZambalaUSA
//
//  Created by Zenwise Technologies on 14/09/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrandsCollectionViewCell1 : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *titleButton;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *gradientImageView;

@end
