//
//  ReLoginViewController.h
//  betazambalafollower
//
//  Created by Zenwise Technologies on 01/01/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import "ViewController.h"

@interface ReLoginViewController : ViewController
@property (weak, nonatomic) IBOutlet UIView *popUPView;
@property (weak, nonatomic) IBOutlet UIView *realAccountView;
@property (weak, nonatomic) IBOutlet UIButton *dummyAccountButton;
@property (weak, nonatomic) IBOutlet UIButton *realAccountButton;
@property (weak, nonatomic) IBOutlet UITextField *userIDTF;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIView *dummyAccountView;
@property (weak, nonatomic) IBOutlet UITextField *emailIDTF;
- (void)showInView:(UIView *)aView animated:(BOOL)animated;
@property NSString * dummyUserID;
@property NSString *dummyPassword;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property NSString * fromWhere;

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@end
