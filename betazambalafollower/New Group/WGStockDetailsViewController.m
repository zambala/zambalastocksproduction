//
//  WGStockDetailsViewController.m
//  ZambalaUSA
//
//  Created by guna on 25/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "WGStockDetailsViewController.h"
#import "HMSegmentedControl.h"
#import "WGOptionTableViewCell.h"
#import "WGNewsTableViewCell.h"

@interface WGStockDetailsViewController ()
{
    HMSegmentedControl * segmentedControl;
}
@end

@implementation WGStockDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.optionView.hidden=NO;
    self.newsView.hidden=YES;

    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"RESEARCH",@"OPTIONS",@"NEWS"]];
    segmentedControl.frame = CGRectMake(0, 52, self.mainView.frame.size.width, 54);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    //    segmentedControl.verticalDividerEnabled = YES;
    //    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(251/255.0) green:(196/255.0) blue:(12/255.0) alpha:1];
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
    segmentedControl.backgroundColor=[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1];
    segmentedControl.tintColor=[UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:1];
    [self.mainView addSubview:segmentedControl];
    self.optionTableView.delegate=self;
    self.optionTableView.dataSource=self;
    self.newsTableView.delegate=self;
    self.newsTableView.dataSource=self;


    
    
    // Do any additional setup after loading the view.
}
-(void)segmentedControlChangedValue
{
    if (segmentedControl.selectedSegmentIndex==0) {
        self.optionView.hidden=NO;
        self.newsView.hidden=YES;
    }
    if (segmentedControl.selectedSegmentIndex==1) {
        self.optionView.hidden=NO;
        self.newsView.hidden=YES;
       
       [self.optionTableView reloadData];
    }
    if (segmentedControl.selectedSegmentIndex==2) {
        
         [self.newsTableView reloadData];
        self.optionView.hidden=YES;
        self.newsView.hidden=NO;
       
    }
    
    
    //NSLog(@"hai");
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView==self.newsTableView) {
        return 4;
    }
    else
    {
    self.optionTableViewHeight.constant=6*63;
        return 6;}
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    @try
    {
   
    
    if (tableView==self.newsTableView) {
      
        WGNewsTableViewCell * cell=[self.newsTableView dequeueReusableCellWithIdentifier:@"news" forIndexPath:indexPath];
        return cell;
    }
    if (tableView==self.optionTableView) {
    
    
        WGOptionTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"option" forIndexPath:indexPath];
    cell.button1.layer.borderWidth=1;
    cell.button1.layer.borderColor=([UIColor colorWithRed:247/255.0 green:248/255.0 blue:249/255.0 alpha:1.0f].CGColor);
    cell.button2.layer.borderWidth=1;
    cell.button2.layer.borderColor=([UIColor colorWithRed:247/255.0 green:248/255.0 blue:249/255.0 alpha:1.0f].CGColor);
    cell.button3.layer.borderWidth=1;
    cell.button3.layer.borderColor=([UIColor colorWithRed:247/255.0 green:248/255.0 blue:249/255.0 alpha:1.0f].CGColor);
    cell.button4.layer.borderWidth=1;
    cell.button4.layer.borderColor=([UIColor colorWithRed:247/255.0 green:248/255.0 blue:249/255.0 alpha:1.0f].CGColor);
    cell.button5.layer.borderWidth=1;
    cell.button5.layer.borderColor=([UIColor colorWithRed:247/255.0 green:248/255.0 blue:249/255.0 alpha:1.0f].CGColor);
        
        
        return cell;
    }
    if (segmentedControl.selectedSegmentIndex==0) {
        
        
//        WGOptionTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"option" forIndexPath:indexPath];
//        cell.button1.layer.borderWidth=1;
//        cell.button1.layer.borderColor=([UIColor colorWithRed:247/255.0 green:248/255.0 blue:249/255.0 alpha:1.0f].CGColor);
//        cell.button2.layer.borderWidth=1;
//        cell.button2.layer.borderColor=([UIColor colorWithRed:247/255.0 green:248/255.0 blue:249/255.0 alpha:1.0f].CGColor);
//        cell.button3.layer.borderWidth=1;
//        cell.button3.layer.borderColor=([UIColor colorWithRed:247/255.0 green:248/255.0 blue:249/255.0 alpha:1.0f].CGColor);
//        cell.button4.layer.borderWidth=1;
//        cell.button4.layer.borderColor=([UIColor colorWithRed:247/255.0 green:248/255.0 blue:249/255.0 alpha:1.0f].CGColor);
//        cell.button5.layer.borderWidth=1;
//        cell.button5.layer.borderColor=([UIColor colorWithRed:247/255.0 green:248/255.0 blue:249/255.0 alpha:1.0f].CGColor);
//        
        
//        return cell;
    }

    
    return 0;
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
