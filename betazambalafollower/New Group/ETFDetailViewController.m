//
//  ETFDetailViewController.m
//  
//
//  Created by Zenwise Technologies on 26/09/17.
//
//

#import "ETFDetailViewController.h"
#import "HMSegmentedControl.h"
#import "ETFDetailTableViewCell.h"
#import "NewOrderViewController.h"
#import "AppDelegate.h"
#import <Mixpanel/Mixpanel.h>

@interface ETFDetailViewController ()

@end

@implementation ETFDetailViewController
{
    HMSegmentedControl * segmentedControl;
    AppDelegate * delegate;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"RESEARCH",@"OPTIONS",@"NEWS"]];
//    segmentedControl.frame = CGRectMake(0, 0, self.view.frame.size.width, 54);
//    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
//    //    segmentedControl.verticalDividerEnabled = YES;
//    //    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(251/255.0) green:(196/255.0) blue:(12/255.0) alpha:1];
//    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
//    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
//    segmentedControl.backgroundColor=[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1];
//    segmentedControl.tintColor=[UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:0.5];
//    [self.view addSubview:segmentedControl]
//    ;
    self.navigationItem.title = @"ETF Detail";
    
    
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"usa_etf_detail_page"];
    self.topView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.topView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.topView.layer.shadowOpacity = 1.0f;
    self.topView.layer.shadowRadius = 1.0f;
    self.topView.layer.cornerRadius=1.0f;
    self.topView.layer.masksToBounds = NO;
    
    self.topTenHoldingsView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.topTenHoldingsView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.topTenHoldingsView.layer.shadowOpacity = 1.0f;
    self.topTenHoldingsView.layer.shadowRadius = 1.0f;
    self.topTenHoldingsView.layer.cornerRadius=1.0f;
    self.topTenHoldingsView.layer.masksToBounds = NO;
    [self LTPServer];
    [self ETFDetailServer];
    [self.buySellButtonLabel addTarget:self action:@selector(onBuyButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)segmentedControlChangedValue
{
    
}
-(void)ETFDetailServer
{
   @try
    {
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               @"authtoken":delegate.zenwiseToken,
                                @"deviceid":delegate.currentDeviceId
                            };
    
    NSString * url = [NSString stringWithFormat:@"%@usstockinfo/etfdetails?symbol=%@",delegate.baseUrl,self.etfCompanyNameString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==403)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                            
                                                        }
                                                        else if([httpResponse statusCode]==401)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                        self.etfDetailResponseArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        //NSLog(@"ETF Details:%@",self.etfDetailResponseArray);
                                                        }
                                                    }
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        self.topTenHoldingsTableView.delegate=self;
                                                        self.topTenHoldingsTableView.dataSource=self;
                                                        [self.topTenHoldingsTableView reloadData];

                                                        
                                                        [self ETFUpdateUI];
                                                        
                                                        
                                                        self.activityIndicator.hidden=YES;
                                                        [self.activityIndicator stopAnimating];
                                                        
                                                    });
                                                }];
    [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
-(void)ETFUpdateUI
{
    @try
    {
    NSString * percent =@"%";
    self.tickerNameLabel.text = [[self.etfDetailResponseArray objectAtIndex:0] objectForKey:@"symbol"];
    self.companyNameLabel.text = [[self.etfDetailResponseArray objectAtIndex:0] objectForKey:@"name"];
    self.ETFNameLabel.text = [[self.etfDetailResponseArray objectAtIndex:0] objectForKey:@"fundfamily"];
    NSString * region = [[self.etfDetailResponseArray objectAtIndex:0] objectForKey:@"region"];
    if([region isEqual:[NSNull null]])
    {
        self.regionLabel.text=@"N/A";
    }else
    {
        self.regionLabel.text = region;
    }
    NSString * country = [[self.etfDetailResponseArray objectAtIndex:0] objectForKey:@"country"];
    if([country isEqual:[NSNull null]])
    {
        self.countryLabel.text =@"N/A";
    }else
    {
    self.countryLabel.text = country;
    }
    NSString * sectorLabel1 = [NSString stringWithFormat:@"%@,%@",[[self.etfDetailResponseArray objectAtIndex:0] objectForKey:@"assetclass"],[[self.etfDetailResponseArray objectAtIndex:0] objectForKey:@"sector"]];
    self.sectorLabel.text = sectorLabel1;
    self.navLabel.text = [[self.etfDetailResponseArray objectAtIndex:0] objectForKey:@"netassetvalue"];
    
    self.dividendLabel.text = [NSString stringWithFormat:@"%@%@",[[self.etfDetailResponseArray objectAtIndex:0] objectForKey:@"annualdividendyield"],percent]; //Need to be given
    
    self.YTDReturnLabel.text = [NSString stringWithFormat:@"%@%@",[[self.etfDetailResponseArray objectAtIndex:0] objectForKey:@"ytdreturn"],percent];
    
    self.descriptionLabel.text = [[self.etfDetailResponseArray objectAtIndex:0] objectForKey:@"description"];
    
    self.oneYRReturnLabel.text = [NSString stringWithFormat:@"%.2f%@",[[[self.etfDetailResponseArray objectAtIndex:0] objectForKey:@"oneyearreturn"] floatValue],percent];
    self.twoYRReturnLabel.text = [NSString stringWithFormat:@"%.2f%@",[[[self.etfDetailResponseArray objectAtIndex:0] objectForKey:@"threeyearreturn"] floatValue],percent];
    self.fiveYearReturnLabel.text = [NSString stringWithFormat:@"%.2f%@",[[[self.etfDetailResponseArray objectAtIndex:0] objectForKey:@"fiveyearreturn"] floatValue],percent];
    self.tenYRReturnLabel.text = [NSString stringWithFormat:@"%.2f%@",[[[self.etfDetailResponseArray objectAtIndex:0] objectForKey:@"tenyearreturn"] floatValue],percent];
    NSString *managementFee = [[self.etfDetailResponseArray objectAtIndex:0] objectForKey:@"managementfee"];
    self.managementFeeLabel.text =[NSString stringWithFormat:@"%@%@",managementFee,percent];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    }

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    self.tableViewHeightContraint.constant = 10*60;
    //self.topHoldingsViewHeightConstraint.constant = (10*60)+30;
    return 10;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
    ETFDetailTableViewCell * top10Holdings = [tableView dequeueReusableCellWithIdentifier:@"ETFDetailTableViewCell" forIndexPath:indexPath];
    
    NSString * tickerLabel = [NSString stringWithFormat:@"%@",[[self.etfDetailResponseArray objectAtIndex:0] objectForKey:[NSString stringWithFormat:@"topholdings%ld",indexPath.row+1]]];
    
    if([tickerLabel isEqual:[NSNull null]])
    {
        top10Holdings.holdingsTickerLabel.text=@"--";
    }else
    {
    top10Holdings.holdingsTickerLabel.text = tickerLabel;
    }
    
    NSString * percent = @"%";
    NSString * percentString = [[self.etfDetailResponseArray objectAtIndex:0] objectForKey:[NSString stringWithFormat:@"topholdings%ldpercent",indexPath.row+1]];
    if([percentString isEqual:[NSNull null]])
    {
        top10Holdings.holdingsPercentLabel.text=@"--";
    }else
    {
    
    float percentFloat = [percentString floatValue];
    NSString * percentFinalString = [NSString stringWithFormat:@"%.2f%@",percentFloat,percent];
    
    top10Holdings.holdingsPercentLabel.text =percentFinalString;
    }
    
    NSString * companyNameLabel = [[self.etfDetailResponseArray objectAtIndex:0] objectForKey:[NSString stringWithFormat:@"topholdings%ldname",indexPath.row+1]];
    if([companyNameLabel isEqual:[NSNull null]])
    {
        top10Holdings.companyNameLabel.text=@"--";
    }else
    {
    top10Holdings.companyNameLabel.text = companyNameLabel;
    }
    return top10Holdings;
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(void)LTPServer
{
    @try
    {
    self.ltpLabel.text=@"Loading";
        NSDictionary *headers = @{ @"cache-control": @"no-cache",
                                   @"deviceid":delegate.currentDeviceId,
                                   @"authtoken":delegate.zenwiseToken
                                   };
    
    NSString * url1 = [NSString stringWithFormat:@"%@%@",delegate.usLTPURL,self.etfCompanyNameString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url1]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        self.ltpResponseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        //                                                        unsigned long a = [[[self.ltpResponseDictionary objectForKey:@"results"] objectForKey:@"quote"] count];
                                                        //                                                        self.ltpResponseArray = [[NSMutableArray alloc]init];
                                                        //                                                        for (int i=0; i<a; i++) {
                                                        //                                                            [self.ltpResponseArray addObject:[[[self.ltpResponseDictionary objectForKey:@"results"]objectForKey:@"quote"]objectAtIndex:i]];
                                                        //                                                        }
                                                        //
                                                        //
                                                        //                                                        //NSLog(@"LTP:%@",self.ltpResponseArray);
                                                        //
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            self.ltpLabel.text = [NSString stringWithFormat:@"%@",[[[[[self.ltpResponseDictionary objectForKey:@"results"]objectForKey:@"quote"]objectAtIndex:0]objectForKey:@"pricedata"]objectForKey:@"last"]];
                                                            
                                                        });
                                                    }
                                                }];
    [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)onBuyButtonTap
{
    NewOrderViewController * trendingOrders = [self.storyboard instantiateViewControllerWithIdentifier:@"NewOrderViewController"];
    delegate.stockTickerString = self.etfCompanyNameString;
    delegate.orderRecomdationType =@"Buy";
    trendingOrders.recomondation =@"Buy";
    
    trendingOrders.tickerName = self.etfCompanyNameString;
    trendingOrders.companyName = [[self.etfDetailResponseArray objectAtIndex:0] objectForKey:@"name"];
    
    [self.navigationController pushViewController:trendingOrders animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
