//
//  TabMenuView.h
//  ZambalaUSA
//
//  Created by guna on 20/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCFloatingActionButton.h"

@interface TabMenuView : UITabBarController<UITableViewDelegate,UITableViewDataSource,floatMenuDelegate,UITabBarDelegate,UITabBarControllerDelegate>


@property UITableView * dummyTable;
@property UITabBarController * tabBar1;

@property (strong, nonatomic) VCFloatingActionButton *addButton;
@property (weak, nonatomic) IBOutlet UITabBar *bottomTabBar;

@end
