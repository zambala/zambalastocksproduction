//
//  TopPicksTableViewCell.h
//  ZambalaUSA
//
//  Created by guna on 26/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopPicksTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *tickerName;
@property (strong, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (strong, nonatomic) IBOutlet UIButton *consesusButton;
@property (strong, nonatomic) IBOutlet UILabel *priceTargetLabel;
@property (weak, nonatomic) IBOutlet UILabel *ltpLabel;
@property (weak, nonatomic) IBOutlet UILabel *changePercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *upsideLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end
