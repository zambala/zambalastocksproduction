//
//  MonksTableViewCell.m
//  ZambalaUSA
//
//  Created by guna on 17/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "MonksTableViewCell.h"
#import "HCSStarRatingView.h"
#import "RateView.h"

@implementation MonksTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.rateView = [[RateView alloc]initWithFrame:CGRectMake(10, 10, 1000, 200)];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)startView1:(id)sender {
}
@end
