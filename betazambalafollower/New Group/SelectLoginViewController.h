//
//  SelectLoginViewController.h
//  ZambalaUSA
//
//  Created by Zenwise Technologies on 30/10/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectLoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *dummyAccountButton;
@property (weak, nonatomic) IBOutlet UIButton *realAccountButton;
@property (weak, nonatomic) IBOutlet UITextField *emailIDTF;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIView *dummyAccountView;
@property (weak, nonatomic) IBOutlet UIView *realAccountView;
@property (weak, nonatomic) IBOutlet UITextField *userIDTF;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;

@end
