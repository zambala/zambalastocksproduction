//
//  ETFDetailViewController.h
//  
//
//  Created by Zenwise Technologies on 26/09/17.
//
//

#import <UIKit/UIKit.h>

@interface ETFDetailViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *tickerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *ltpLabel;
@property (weak, nonatomic) IBOutlet UILabel *ETFNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *regionLabel;
@property (weak, nonatomic) IBOutlet UILabel *countryLabel;
@property (weak, nonatomic) IBOutlet UILabel *sectorLabel;
@property (weak, nonatomic) IBOutlet UILabel *navLabel;
@property (weak, nonatomic) IBOutlet UILabel *dividendLabel;
@property (weak, nonatomic) IBOutlet UILabel *YTDReturnLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *buySellButtonLabel;
@property (weak, nonatomic) IBOutlet UILabel *oneYRReturnLabel;
@property (weak, nonatomic) IBOutlet UILabel *twoYRReturnLabel;
@property (weak, nonatomic) IBOutlet UILabel *fiveYearReturnLabel;
@property (weak, nonatomic) IBOutlet UILabel *tenYRReturnLabel;
@property (weak, nonatomic) IBOutlet UIView *topTenHoldingsView;
@property (weak, nonatomic) IBOutlet UITableView *topTenHoldingsTableView;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UILabel *managementFeeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightContraint;
@property NSMutableDictionary * etfDetailResponseDictionary;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topHoldingsViewHeightConstraint;
@property NSMutableDictionary * ltpResponseDictionary;
@property NSString *etfCompanyNameString;
@property NSMutableArray * etfDetailResponseArray;

@end
