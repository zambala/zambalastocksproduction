//
//  LoginViewController.h
//  ZambalaUSA
//
//  Created by zenwise mac 2 on 9/5/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *clientIDTF;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
- (IBAction)onContinueButtonTap:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property NSMutableDictionary * loginResponseDictionary;
@property (weak, nonatomic) IBOutlet UIButton *backButtonTap;
@property NSString * reLoginCheck;
@property NSMutableDictionary * clientResponseDictionary;
@property NSString * accountName;
@property (weak, nonatomic) IBOutlet UIImageView *ctImageView;
@property (weak, nonatomic) IBOutlet UITextField *tempPasswordTF;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tempHeightConstraint;

@end
