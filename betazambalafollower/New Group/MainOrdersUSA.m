//
//  MainOrdersUSA.m
//  
//
//  Created by Zenwise Technologies on 16/10/17.
//
//

#import "MainOrdersUSA.h"
#import "PendingOrdersUSATableViewCell.h"
#import "CancelledOrdersTableViewCell.h"
#import "CompletedOrdersUSATableViewCell.h"
#import "NoOrdersTableViewCell.h"
#import "HMSegmentedControl.h"
#import "AppDelegate.h"
#import "TabMenuView.h"
#import "NewOrderViewController.h"
#import "Mixpanel/Mixpanel.h"
#import "BrokersWebViewControllerUSA.h"
#import "LoginViewController.h"


@interface MainOrdersUSA ()

@end

@implementation MainOrdersUSA
{
    HMSegmentedControl * segmentedControl;
    AppDelegate * delegate;
    NSString*sortByString;
    NSMutableArray * mixpanelOrdersArray;
    NSString * mixPanelOrdersString;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    mixpanelOrdersArray = [[NSMutableArray alloc]init];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"usa_order_status_page"];
    [mixpanelMini track:@"usa_order_pending_page"];
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"PENDING",@"COMPLETED",@"CANCELLED"]];
    segmentedControl.frame = CGRectMake(0,88, self.view.frame.size.width, 54.1);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.verticalDividerEnabled = YES;
    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1];
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
    if([delegate.accountCheck isEqualToString:@"dummy"])
    {
        sortByString=@"24";
    }else if ([delegate.accountCheck isEqualToString:@"real"])
    {
        sortByString=@"21";
    }
    //sortByString=@"2";
    [segmentedControl setSelectedSegmentIndex:0];
    
    [self.view addSubview:segmentedControl];
    self.ordersResponseDictionary = [[NSMutableDictionary alloc]init];
    
    if([self.orderStatusString isEqualToString:@"ORDEXE"])
    {
        [segmentedControl setSelectedSegmentIndex:1];
        sortByString=@"23";
        //sortByString=@"2";
    }else if ([self.orderStatusString isEqualToString:@"ORDREQ"]||[self.orderStatusString isEqualToString:@"ORDSNT"])
    {
        [segmentedControl setSelectedSegmentIndex:0];
        if([delegate.accountCheck isEqualToString:@"dummy"])
        {
            sortByString=@"24";
        }else if ([delegate.accountCheck isEqualToString:@"real"])
        {
            sortByString=@"21";
        }
        //sortByString=@"2";
    }else if ([self.orderStatusString isEqualToString:@"ORDREJ"])
    {
        [segmentedControl setSelectedSegmentIndex:2];
        sortByString=@"22";
        //sortByString=@"2";
    }
    
    self.completedView.hidden=YES;
    self.cancelView.hidden=YES;
    self.pendingView.hidden=NO;
    
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
   self.dateString = [dateFormat stringFromDate:today];
    //NSLog(@"date: %@", self.dateString);
    if(delegate.sessionID.length>0)
    {
        [self ordersListServer];
    }else
    {
        [self loginCheck];
    }
    
    
    
    
    [self.backButton addTarget:self action:@selector(onBackButtonTap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}
-(void)loginCheck
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Please choose an option." message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
//        UIAlertAction * OpenAccount=[UIAlertAction actionWithTitle:@"Open E-Account" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            //Navigate to open E- Account page
//            BrokersWebViewControllerUSA * openAccountWebView = [self.storyboard instantiateViewControllerWithIdentifier:@"BrokersWebViewControllerUSA"];
//            [self.navigationController pushViewController:openAccountWebView animated:YES];
//        }];
        UIAlertAction * Login=[UIAlertAction actionWithTitle:@"Login" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            //Navigate to login page
            LoginViewController *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
            login.reLoginCheck=@"relogin";
            [self presentViewController:login animated:YES completion:nil];
        }];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
//        [alert addAction:OpenAccount];
        [alert addAction:Login];
        [alert addAction:cancel];
        self.mainOrdersTableView.delegate=self;
        self.mainOrdersTableView.dataSource=self;
        [self.mainOrdersTableView reloadData];
        
    });
}

-(void)onBackButtonTap
{
    TabMenuView * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"TabMenuView"];
    [self presentViewController:tabPage animated:YES completion:nil];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)segmentedControlChangedValue
{
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    if(segmentedControl.selectedSegmentIndex==0)
    {
         [mixpanelMini track:@"usa_order_pending_page"];
        mixPanelOrdersString=@"";
        if(mixpanelOrdersArray.count>0)
        {
            [mixpanelOrdersArray removeAllObjects];
        }
        if([delegate.accountCheck isEqualToString:@"dummy"])
        {
        sortByString=@"24";
        }else if ([delegate.accountCheck isEqualToString:@"real"])
        {
            sortByString=@"21";
        }
        //sortByString=@"2";
        self.completedView.hidden=YES;
        self.cancelView.hidden=YES;
        self.pendingView.hidden=NO;
        [self ordersListServer];
    }else if (segmentedControl.selectedSegmentIndex==1)
    {
         [mixpanelMini track:@"usa_order_completed_page"];
        mixPanelOrdersString=@"";
        if(mixpanelOrdersArray.count>0)
        {
            [mixpanelOrdersArray removeAllObjects];
        }
        sortByString=@"23";
        //sortByString=@"2";
        self.completedView.hidden=NO;
        self.cancelView.hidden=YES;
        self.pendingView.hidden=YES;
        [self ordersListServer];
    }else if (segmentedControl.selectedSegmentIndex==2)
    {
         [mixpanelMini track:@"usa_order_cancelled_page"];
        mixPanelOrdersString=@"";
        if(mixpanelOrdersArray.count>0)
        {
            [mixpanelOrdersArray removeAllObjects];
        }
        sortByString=@"22";
        //sortByString=@"2";
        self.completedView.hidden=YES;
        self.cancelView.hidden=NO;
        self.pendingView.hidden=YES;
        [self ordersListServer];
    }
}

-(void)ordersListServer
{
    @try
    {
        NSString * str = [NSString stringWithFormat:@"api={\"name\":\"JSON_GetOrderListPaginated\",\"client_id\":\"T7qqV91jR2\",\"session_id\":\"%@\",\"firm_code\":\"5ntJOPXqQb\",\"account\":\"%@\",\"sortBy\":\"%@\",\"page\":\"1\",\"startDate\":\"%@\",\"endDate\":\"%@\"}",delegate.sessionID,delegate.accountNumber,sortByString,self.dateString,self.dateString];
    
    //NSString * str = [NSString stringWithFormat:@"api={\"name\":\"JSON_GetOrderListPaginated\",\"client_id\":\"T7qqV91jR2\",\"session_id\":\"%@\",\"firm_code\":\"5ntJOPXqQb\",\"account\":\"%@\",\"sortBy\":\"%@\",\"page\":\"1\",\"startDate\":\"%@\",\"endDate\":\"%@\"}",delegate.sessionID,delegate.accountNumber,sortByString,@"2017-01-01",@"2017-12-04"];
    
        NSData *dt = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]init];
    if([delegate.accountCheck isEqualToString:@"dummy"])
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.dummyAccountURL]
                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                      timeoutInterval:10.0];
    }else if ([delegate.accountCheck isEqualToString:@"real"])
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.realAccountURL]
                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                      timeoutInterval:10.0];
    }
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        [request setHTTPBody:dt];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            NSDictionary* json = [NSJSONSerialization
                                                                                  JSONObjectWithData:data
                                                                                  options:kNilOptions
                                                                                  error:&error];
                                                            //NSLog(@"jjson %@",json);
                                                            
                                                            if(self.ordersResponseDictionary.count>0)
                                                            {
                                                                [self.ordersResponseDictionary removeAllObjects];
                                                            }
                                                            [self.ordersResponseDictionary setObject:json forKey:@"orders"];
                                                            
                                                            //NSLog(@"orders:%@",self.ordersResponseDictionary);
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                self.mainOrdersTableView.delegate=self;
                                                                self.mainOrdersTableView.dataSource=self;
                                                                [self.mainOrdersTableView reloadData];
                                                            });
                                                        }
                                                    }];
        [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"]isEqual:[NSNull null]])
    {
        return 1;
    }else
    {
        return [[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] count];
    }
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
        if([[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"]isEqual:[NSNull null]]||self.ordersResponseDictionary.count==0)
        {
            NoOrdersTableViewCell * noorders = [tableView dequeueReusableCellWithIdentifier:@"NoOrdersTableViewCell" forIndexPath:indexPath];
            Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
            [mixpanelMini identify:delegate.USAuserID];
            if(segmentedControl.selectedSegmentIndex==0)
            {
            [mixpanelMini.people set:@{@"uspendingorders":@""}];
            }else if (segmentedControl.selectedSegmentIndex==1)
            {
            [mixpanelMini.people set:@{@"uscompletedorders":@""}];
            }else if (segmentedControl.selectedSegmentIndex==2)
            {
            [mixpanelMini.people set:@{@"uscancelledorders":@""}];
            }
            
            return noorders;
        }else
        {
            if(segmentedControl.selectedSegmentIndex==0)
            {
    PendingOrdersUSATableViewCell * pending = [tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
                pending.orderIDLabel.text=[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"orderID"];
                pending.dateAndTimeLabel.text=[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"timeplaced"];
                pending.tickerNameLabel.text =[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"symbol"];
                int action =[[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"action"] intValue];
                if(action==1)
                {
                    pending.transactionTypeLabel.text =@"Buy";
                }else if (action==2)
                {
                    pending.transactionTypeLabel.text =@"Sell";
                }
                pending.priceLabel.text=[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"limitPriceDeb"];
                pending.totalQuantityLabel.text=[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"quantity"];
                pending.filledQuantityLabel.text=[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"fillQty"];
                pending.pendingQuantityLabel.text=[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"remainingQty"];
                [pending.cancelButton addTarget:self action:@selector(onCancelButtonTap:) forControlEvents:UIControlEventTouchUpInside];
                [pending.editButton addTarget:self action:@selector(onEditButtonTap:) forControlEvents:UIControlEventTouchUpInside];
                
                [mixpanelOrdersArray addObject:[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"symbol"]];
                mixPanelOrdersString = [mixpanelOrdersArray componentsJoinedByString:@","];
                Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
                [mixpanelMini identify:delegate.USAuserID];
                [mixpanelMini.people set:@{@"uspendingorders":mixPanelOrdersString}];
                
    return pending;
            }else if (segmentedControl.selectedSegmentIndex==1)
            {
                CompletedOrdersUSATableViewCell * completed = [tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
                completed.orderIDLabel.text=[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"orderID"];
                completed.timeAndDateLabel.text=[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"timeplaced"];
                completed.tickerNameLabel.text=[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"symbol"];
                int action =[[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"action"] intValue];
                if(action==1)
                {
                    completed.transactionTypeLabel.text =@"Buy";
                }else if (action==2)
                {
                   completed.transactionTypeLabel.text =@"Sell";
                }
                completed.priceLabel.text=[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"fillPrice"];
                completed.quantityLabel.text=[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"fillQty"];
                completed.fillQuantityLabel.text=[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"fillQty"];
                completed.pendingQuantityLabel.text=[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"fillQty"];
                
                
                [mixpanelOrdersArray addObject:[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"symbol"]];
                mixPanelOrdersString = [mixpanelOrdersArray componentsJoinedByString:@","];
                Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
                [mixpanelMini identify:delegate.USAuserID];
                [mixpanelMini.people set:@{@"uscompletedorders":mixPanelOrdersString}];
                return completed;
            }else if (segmentedControl.selectedSegmentIndex==2)
            {
                CompletedOrdersUSATableViewCell * completed = [tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
                completed.orderIDLabel.text=[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"orderID"];
                completed.timeAndDateLabel.text=[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"timeplaced"];
                completed.tickerNameLabel.text=[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"symbol"];
                int action =[[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"action"] intValue];
                if(action==1)
                {
                    completed.transactionTypeLabel.text =@"Buy";
                }else if (action==2)
                {
                    completed.transactionTypeLabel.text =@"Sell";
                }
                completed.priceLabel.hidden=YES;
//                completed.priceLabel.text=[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"limitPriceDeb"];
                completed.quantityLabel.text=[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"quantity"];
                completed.fillQuantityLabel.text=[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"fillQty"];
                completed.pendingQuantityLabel.text=[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"fillQty"];
                completed.dummyLabel.text=[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"limitPriceDeb"];
                [mixpanelOrdersArray addObject:[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"symbol"]];
                mixPanelOrdersString = [mixpanelOrdersArray componentsJoinedByString:@","];
                Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
                [mixpanelMini identify:delegate.USAuserID];
                [mixpanelMini.people set:@{@"uscancelledorders":mixPanelOrdersString}];
                return completed;
            }
        }
    return 0;
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"]isEqual:[NSNull null]]||self.ordersResponseDictionary.count==0)
    {
        return 400;
    }else
    {
        return 125;
    }
    return 0;
}
-(void)cancelOrderServer
{
   @try
    {
    NSString * str = [NSString stringWithFormat:@"api={\"name\":\"JSON_CancelOrder\",\"client_id\":\"T7qqV91jR2\",\"session_id\":\"%@\",\"firm_code\":\"5ntJOPXqQb\",\"order_id\":\"%@\"}",delegate.sessionID,self.OrderID];
    
    NSData *dt = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]init];
    if([delegate.accountCheck isEqualToString:@"dummy"])
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.dummyAccountURL]
                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                      timeoutInterval:10.0];
    }else if ([delegate.accountCheck isEqualToString:@"real"])
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.realAccountURL]
                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                      timeoutInterval:10.0];
    }
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody:dt];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        NSDictionary* json = [NSJSONSerialization
                                                                              JSONObjectWithData:data
                                                                              options:kNilOptions
                                                                              error:&error];
                                                        //NSLog(@"jjson %@",json);
                                                        if([[json objectForKey:@"message"]containsString:@"Request Successful"])
                                                        {
                                                            UIAlertController * alert = [UIAlertController
                                                                                         alertControllerWithTitle:@"Order Status"
                                                                                         message:@"Cancelled Order"
                                                                                         preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            //Add Buttons
                                                            
                                                            UIAlertAction* okButton = [UIAlertAction
                                                                                       actionWithTitle:@"Ok"
                                                                                       style:UIAlertActionStyleDefault
                                                                                       handler:^(UIAlertAction * action) {
                                                                                           //Handle your yes please button action here
                                                                                           sortByString=@"22";
                                                                                           //sortByString=@"2";
                                                                                           [segmentedControl setSelectedSegmentIndex:2];
                                                                       [self ordersListServer];
                                                                                           
                                                                                       }];
                                                            //Add your buttons to alert controller
                                                            
                                                            [alert addAction:okButton];
                                                            
                                                            
                                                            [self presentViewController:alert animated:YES completion:nil];
                                                        }
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                        });
                                                    }
                                                }];
    [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}

-(void)onCancelButtonTap:(UIButton*)sender
{
    @try
    {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.mainOrdersTableView];
    NSIndexPath *indexPath = [self.mainOrdersTableView indexPathForRowAtPoint:buttonPosition];
    //NSLog(@"Index path:%ld",(long)indexPath.row);
    self.OrderID = [[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"orderID"];
    [self cancelOrderServer];
    [self ordersListServer];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)onEditButtonTap:(UIButton*)sender
{
    @try
    {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.mainOrdersTableView];
    NSIndexPath *indexPath = [self.mainOrdersTableView indexPathForRowAtPoint:buttonPosition];
    //NSLog(@"Index path:%ld",(long)indexPath.row);
    NewOrderViewController * editOrder = [self.storyboard instantiateViewControllerWithIdentifier:@"NewOrderViewController"];
    editOrder.editOrderCheck=@"edit";
    editOrder.editQuantity = [[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"quantity"];
    editOrder.editLimitPrice= [[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"limitPriceDeb"];
    editOrder.tickerName=[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"symbol"];
    editOrder.editOrderID=[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"orderID"];
    delegate.stockTickerString = [[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"symbol"];
    editOrder.companyName = [[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"symbol"];
    [self presentViewController:editOrder animated:YES completion:nil];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
