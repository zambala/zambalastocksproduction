//
//  PortFolioViewController.h
//  
//
//  Created by Zenwise Technologies on 28/09/17.
//
//

#import <UIKit/UIKit.h>

@interface PortFolioViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *balancesView;
@property (weak, nonatomic) IBOutlet UILabel *cashBalanceView;
@property (weak, nonatomic) IBOutlet UILabel *securitiesValue;
@property (weak, nonatomic) IBOutlet UILabel *totalValueLabel;
@property (weak, nonatomic) IBOutlet UIView *noPortFolioView;
@property (weak, nonatomic) IBOutlet UIView *dematView;
@property (weak, nonatomic) IBOutlet UITableView *dematTableView;
@property NSMutableDictionary * positionsDictionary;
@property NSMutableArray * positionsArray;
@property NSMutableArray * groupArray;

@end
