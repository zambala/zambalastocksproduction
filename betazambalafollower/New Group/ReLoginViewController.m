//
//  ReLoginViewController.m
//  betazambalafollower
//
//  Created by Zenwise Technologies on 01/01/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import "ReLoginViewController.h"
#import "AppDelegate.h"
#import "Mixpanel/Mixpanel.h"


@interface ReLoginViewController ()

@end

@implementation ReLoginViewController
{
    AppDelegate * delegate;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.dummyAccountView.hidden=YES;
    self.dummyAccountButton.hidden=YES;
    self.realAccountView.hidden=NO;
    self.realAccountButton.hidden=YES;
    self.submitButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.submitButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.submitButton.layer.shadowOpacity = 1.0f;
    self.submitButton.layer.shadowRadius = 1.0f;
    self.submitButton.layer.cornerRadius=1.0f;
    self.submitButton.layer.masksToBounds = NO;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.mainView addGestureRecognizer:tap];
    [self.cancelButton addTarget:self action:@selector(onCancelButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.submitButton addTarget:self action:@selector(onSubmitButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    // Do any additional setup after loading the view.
}
-(void)onCancelButtonTap
{
    
}
-(void)onSubmitButtonTap
{
    if(self.userIDTF.text.length>0&&self.passwordTF.text.length>0)
    {
        [self loginServer];
    }else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Error"
                                         message:@"Please fill the missing details"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            //Add Buttons
            UIAlertAction* okButton = [UIAlertAction
                                       actionWithTitle:@"Ok"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           //Handle your yes please button action here
                                           
                                       }];
            //Add your buttons to alert controller
            
            [alert addAction:okButton];
            
            
            [self presentViewController:alert animated:YES completion:nil];
            
        });
    }
}
-(void)dismissKeyboard
{
    [self.userIDTF resignFirstResponder];
    [self.passwordTF resignFirstResponder];
}

-(void)loginServer
{
    @try
    {
    
    NSString * str = [NSString stringWithFormat:@"api={\"name\":\"JSON_Login\",\"client_id\":\"T7qqV91jR2\",\"firm_code\":\"5ntJOPXqQb\",\"type\":\"C\",\"userID\":\"%@\",\"password\":\"%@\",\"fromIP\":\"192.168.12.26\",\"browser\":\"mobilebrowser\"}",self.userIDTF.text,self.passwordTF.text];
    // NSString* str = @"api={\"name\":\"JSON_Login\",\"client_id\":\"T7qqV91jR2\",\"firm_code\":\"5ntJOPXqQb\",\"type\":\"C\",\"userID\":\"VT070971\",\"password\":\"EQI387\",\"fromIP\":\"192.168.12.26\",\"browser\":\"mobilebrowser\"}";
    
    NSString * url = [NSString stringWithFormat:@"%@",delegate.realAccountURL];
    
    NSData *dt = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    //    NSDictionary *cookieProperties = [NSDictionary dictionaryWithObjectsAndKeys:
    //                                      @"domain.com", NSHTTPCookieDomain,
    //                                      @"\\", NSHTTPCookiePath,
    //                                      @"myCookie", NSHTTPCookieName,
    //                                      @"1234", NSHTTPCookieValue,
    //                                      nil];
    //    NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
    //    NSArray* cookieArray = [NSArray arrayWithObject:cookie];
    //    NSDictionary * headers = [NSHTTPCookie requestHeaderFieldsWithCookies:cookieArray];
    //    [request setAllHTTPHeaderFields:headers];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody:dt];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        NSDictionary* json = [NSJSONSerialization
                                                                              JSONObjectWithData:data
                                                                              options:kNilOptions
                                                                              error:&error];
                                                        //NSLog(@"jjson %@",json);
                                                        int successCheck = [[json objectForKey:@"success"] intValue];
                                                        
                                                        if(successCheck==1)
                                                        {
                                                            delegate.accountCheck=@"real";
                                                            delegate.USAuserID = self.userIDTF.text;
                                                            delegate.sessionID = [json objectForKey:@"session_id"];
                                                            //NSLog(@"Session ID:%@",delegate.sessionID);
//                                                            Mixpanel *mixpanel = [Mixpanel sharedInstance];
//
//                                                            [mixpanel identify:delegate.USAuserID];
//                                                            NSString * name = [NSString stringWithFormat:@"%@",[[json objectForKey:@"accounts"]objectForKey:@"name"]];
//                                                            [mixpanel.people set:@{@"first_name":name}];
//
//                                                            [mixpanel.people set:@{@"brokername":@"ChoiceTrade"}];
//
                                                            
                                                            [self getAccountNumberServer];
                                                        }else if ([[json objectForKey:@"message"]isEqualToString:@"Error: Firm is Invalid."])
                                                        {
                                                            
                                                            [self loginServer];
                                                        }else if (successCheck==0)
                                                        {
                                                            NSString * message = [NSString stringWithFormat:@"%@",[json objectForKey:@"message"]];
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                UIAlertController * alert = [UIAlertController
                                                                                             alertControllerWithTitle:@"Error"
                                                                                             message:message
                                                                                             preferredStyle:UIAlertControllerStyleAlert];
                                                                
                                                                //Add Buttons
                                                                
                                                                UIAlertAction* okButton = [UIAlertAction
                                                                                           actionWithTitle:@"Ok"
                                                                                           style:UIAlertActionStyleDefault
                                                                                           handler:^(UIAlertAction * action) {
                                                                                               //Handle your yes please button action here
                                                                                               
                                                                                           }];
                                                                //Add your buttons to alert controller
                                                                
                                                                [alert addAction:okButton];
                                                                
                                                                
                                                                [self presentViewController:alert animated:YES completion:nil];
                                                                
                                                            });
                                                        }
                                                    }
                                                }];
    [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)getAccountNumberServer
{
    @try
    {
    NSString * str = [NSString stringWithFormat:@"api={\"name\":\"JSON_GetAccountList\",\"client_id\":\"T7qqV91jR2\",\"session_id\":\"%@\",\"firm_code\":\"5ntJOPXqQb\",\"byCust\":\"0\",\"userID\":\"%@\",\"accountMode\":\"A\"}",delegate.sessionID,delegate.USAuserID];
    // NSString* str = @"api={\"name\":\"JSON_Login\",\"client_id\":\"T7qqV91jR2\",\"firm_code\":\"5ntJOPXqQb\",\"type\":\"C\",\"userID\":\"VT070971\",\"password\":\"EQI387\",\"fromIP\":\"192.168.12.26\",\"browser\":\"mobilebrowser\"}";
    
    NSData *dt = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.realAccountURL]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody:dt];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        NSDictionary* json = [NSJSONSerialization
                                                                              JSONObjectWithData:data
                                                                              options:kNilOptions
                                                                              error:&error];
                                                        //NSLog(@"jjson %@",json);
                                                        int successCheck = [[json objectForKey:@"success"] intValue];
                                                        
                                                        if(successCheck==1)
                                                        {
                                                            delegate.accountNumber = [[json objectForKey:@"accounts"] objectForKey:@"accountNo"];
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                if(delegate.accountCheck.length>0)
                                                                {
                                                                    [self dismissViewControllerAnimated:YES completion:nil];
                                                                }else
                                                                {
                                                                    UIAlertController * alert = [UIAlertController
                                                                                                 alertControllerWithTitle:@"Error"
                                                                                                 message:@"Error while retriving Account number. Login Again."
                                                                                                 preferredStyle:UIAlertControllerStyleAlert];
                                                                    
                                                                    //Add Buttons
                                                                    
                                                                    UIAlertAction* okButton = [UIAlertAction
                                                                                               actionWithTitle:@"Ok"
                                                                                               style:UIAlertActionStyleDefault
                                                                                               handler:^(UIAlertAction * action) {
                                                                                                   //Handle your yes please button action here
                                                                                                   
                                                                                               }];
                                                                    //Add your buttons to alert controller
                                                                    
                                                                    [alert addAction:okButton];
                                                                    
                                                                    
                                                                    [self presentViewController:alert animated:YES completion:nil];
                                                                }
                                                            });
                                                        }
                                                    }
                                                }];
    [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)showAnimate
{
    self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.view.alpha = 0.5;
    [UIView animateWithDuration:.25 animations:^{
        self.view.alpha = 1;
        self.view.transform = CGAffineTransformMakeScale(1, 1);
        
    }];
}


- (UIView *)applyBlurToView:(UIView *)view withEffectStyle:(UIBlurEffectStyle)style andConstraints:(BOOL)addConstraints
{
    //only apply the blur if the user hasn't disabled transparency effects
    if(!UIAccessibilityIsReduceTransparencyEnabled())
    {
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:style];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = view.bounds;
        
        [view addSubview:blurEffectView];
        
        if(addConstraints)
        {
            //add auto layout constraints so that the blur fills the screen upon rotating device
            [blurEffectView setTranslatesAutoresizingMaskIntoConstraints:NO];
            
            [view addConstraint:[NSLayoutConstraint constraintWithItem:blurEffectView
                                                             attribute:NSLayoutAttributeTop
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:view
                                                             attribute:NSLayoutAttributeTop
                                                            multiplier:1
                                                              constant:0]];
            
            [view addConstraint:[NSLayoutConstraint constraintWithItem:blurEffectView
                                                             attribute:NSLayoutAttributeBottom
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:view
                                                             attribute:NSLayoutAttributeBottom
                                                            multiplier:1
                                                              constant:0]];
            
            [view addConstraint:[NSLayoutConstraint constraintWithItem:blurEffectView
                                                             attribute:NSLayoutAttributeLeading
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:view
                                                             attribute:NSLayoutAttributeLeading
                                                            multiplier:1
                                                              constant:0]];
            
            [view addConstraint:[NSLayoutConstraint constraintWithItem:blurEffectView
                                                             attribute:NSLayoutAttributeTrailing
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:view
                                                             attribute:NSLayoutAttributeTrailing
                                                            multiplier:1
                                                              constant:0]];
        }
    }
    else
    {
        view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
    }
    
    return view;
}

- (void)removeAnimate
{
    [UIView animateWithDuration:.25 animations:^{
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.view.alpha = 0.5;
    } completion:^(BOOL finished) {
        if (finished) {
            [self.view removeFromSuperview];
            
            
        }
    }];
}
- (void)showInView:(UIView *)aView animated:(BOOL)animated
{
    [aView addSubview:self.view];
    if (animated) {
        [self showAnimate];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
