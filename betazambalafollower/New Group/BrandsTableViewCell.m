//
//  BrandsTableViewCell.m
//  ZambalaUSA
//
//  Created by Zenwise Technologies on 14/09/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "BrandsTableViewCell.h"
#import "BrandsCollectionViewCell1.h"
#import "BrandsCollectionViewCell2.h"
#import "NewStockViewController.h"
#import "AppDelegate.h"
#import "NewOrderViewController.h"
#import "MainViewUSA.h"
#import "MarketWatchViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+WebCache.h"

@implementation BrandsTableViewCell
{
    NSMutableArray * companyNamesArray;
    NSMutableArray * symbolsArray;
    NSMutableArray * imagesArray;
    NSMutableString * symbolsString;
    AppDelegate * delegate;
    NSString * catergoryNameString;
    CAGradientLayer *theViewGradient;
    BOOL selectedCheck;
    NSIndexPath * storedIndex;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    companyNamesArray = [[NSMutableArray alloc]initWithObjects:@"Apple",@"Adobe",@"Alphabet",@"Amazon",@"Blackberry",@"Twitter", nil];
    symbolsArray = [[NSMutableArray alloc]initWithObjects:@"AAPL",@"ADBE",@"GOOGL",@"AMZN",@"FB",@"TWTR", nil];
    symbolsString = [[NSMutableString alloc]init];
    theViewGradient = [CAGradientLayer layer];
    imagesArray = [[NSMutableArray alloc]initWithObjects:@"aapl.png",@"adbe.png",@"alpha.png",@"amzn.png",@"bbry.png",@"twtr.png", nil];
    selectedCheck = true;
    self.brandsCollectionView.allowsMultipleSelection=NO;
    //    NSIndexPath *indexPathForFirstRow = [NSIndexPath indexPathForRow:0 inSection:0];
    //    [self.brandsCollectionView selectItemAtIndexPath:indexPathForFirstRow animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    ////    [self collectionView:self.brandsCollectionView didSelectItemAtIndexPath:indexPathForFirstRow];
    //    //[self.brandsCollectionView selectItemAtIndexPath:indexPathForFirstRow animated:YES scrollPosition:UICollectionViewScrollPositionNone];
    [self categoriesServer];
    //    NSIndexPath *indexPathForFirstRow = [NSIndexPath indexPathForRow:0 inSection:0];
    //    [self.brandsCollectionView selectItemAtIndexPath:indexPathForFirstRow animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    //    [self collectionView:self.brandsCollectionView didSelectItemAtIndexPath:indexPathForFirstRow];
    
    
    
}

-(void)ltpServerHit
{
    @try
    {
        NSDictionary *headers = @{ @"cache-control": @"no-cache",
                                   @"deviceid":delegate.currentDeviceId,
                                   @"authtoken":delegate.zenwiseToken
                                   };
    NSString * url = [NSString stringWithFormat:@"%@%@",delegate.usLTPURL,symbolsString];
    
    //NSLog(@"url:%@",url);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        self.ltpResponseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        //NSLog(@"LTP Response:%@",self.ltpResponseDictionary);
                                                        unsigned long a = [[[self.ltpResponseDictionary objectForKey:@"results"] objectForKey:@"quote"] count];
                                                        self.ltpResponseArray = [[NSMutableArray alloc]init];
                                                        for (int i=0; i<a; i++) {
                                                            [self.ltpResponseArray addObject:[[[self.ltpResponseDictionary objectForKey:@"results"]objectForKey:@"quote"]objectAtIndex:i]];
                                                        }
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            
                                                            //  self.brandsCollectionView.delegate=self;
                                                            //  self.brandsCollectionView.dataSource=self;
                                                            //  self.userInteractionEnabled=YES;
                                                            self.brandsCollectionView2.delegate=self;
                                                            self.brandsCollectionView2.dataSource=self;
                                                            //[self.brandsCollectionView reloadData];
                                                            [self.brandsCollectionView2 reloadData];
                                                            
                                                        });
                                                        
                                                        
                                                        
                                                        
                                                    }
                                                }];
    [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)categoriesServer
{
    @try
    {
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               @"authtoken":delegate.zenwiseToken,
                               @"deviceid":delegate.currentDeviceId
                               };
    
    NSString * url = [NSString stringWithFormat:@"%@usstockinfo/categories",delegate.baseUrl];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==403)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                            
                                                        }
                                                        else if([httpResponse statusCode]==401)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                            self.categoriesResponseArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            //NSLog(@"Categories:%@",self.categoriesResponseArray);
                                                            catergoryNameString = [NSString stringWithFormat:@"%@",[[self.categoriesResponseArray objectAtIndex:0]objectForKey:@"category"]];
                                                            
                                                        }
                                                    }
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        if(catergoryNameString.length>0)
                                                        {
                                                            self.brandsCollectionView.delegate=self;
                                                            self.brandsCollectionView.dataSource=self;
                                                            [self.brandsCollectionView reloadData];
                                                            [self categoriesListServer];
                                                        }else
                                                        {
                                                            
                                                        }
                                                        
                                                    });
                                                    
                                                }];
    [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}
-(void)categoriesListServer
{
    @try
    {
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               @"authtoken":delegate.zenwiseToken,
                               @"deviceid":delegate.currentDeviceId
                               };
    
    NSString * url = [NSString stringWithFormat:@"%@usstockinfo/brands?category=%@&limit=1000",delegate.baseUrl,catergoryNameString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==403)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                            
                                                        }
                                                        else if([httpResponse statusCode]==401)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                            self.categoriesListResponseArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            //NSLog(@"Categories list:%@",self.categoriesListResponseArray);
                                                            
                                                            NSMutableArray * symbolArray = [[NSMutableArray alloc] init];
                                                            for (int i=0; i<self.categoriesListResponseArray.count; i++) {
                                                                [symbolArray addObject:[[self.categoriesListResponseArray objectAtIndex:i]objectForKey:@"symbol"]];
                                                            }
                                                            symbolsString = [[symbolArray componentsJoinedByString:@","] mutableCopy];
                                                            //NSLog(@"SymbolsString:%@",symbolsString);
                                                            
                                                        }
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            //                                                        self.brandsCollectionView.delegate=self;
                                                            //                                                        self.brandsCollectionView2.dataSource=self;
                                                            [self ltpServerHit];
                                                            
                                                        });
                                                        
                                                    }
                                                    
                                                }];
    [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(collectionView == self.brandsCollectionView)
    {
        return self.categoriesResponseArray.count;
    }else if (collectionView == self.brandsCollectionView2)
    {
        return self.categoriesListResponseArray.count;
    }
    
    return 0;
}

//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
//{
//    if(collectionView == self.brandsCollectionView)
//    {
//    return 300;
//    }
//    return 30;
//}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
    if(collectionView == self.brandsCollectionView)
    {
        BrandsCollectionViewCell1 *brands1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"brands1" forIndexPath:indexPath];
        //        brands1.contentView.layer.cornerRadius = 2.0f;
        //        brands1.contentView.layer.borderWidth = 1.0f;
        //        brands1.contentView.layer.borderColor = [UIColor colorWithRed:2.0/255.0 green:30.0/255.0 blue:41.0/255.0 alpha:1.0].CGColor;
        //        brands1.contentView.layer.masksToBounds = YES;
        //
        //        brands1.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        //        brands1.layer.shadowOffset = CGSizeMake(0, 4.0f);
        //        brands1.layer.shadowRadius = 4.0f;
        //        brands1.layer.shadowOpacity = 1.0f;
        //        brands1.layer.masksToBounds = NO;
        //brands1.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:brands1.bounds cornerRadius:brands1.contentView.layer.cornerRadius].CGPath;
        brands1.bgView.layer.cornerRadius = 5.0f;
        brands1.titleButton.layer.cornerRadius = 5.0f;
//        brands1.gradientImageView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:1.0] CGColor];
//        brands1.gradientImageView.layer.shadowOffset = CGSizeMake(0, 2.0f);
//        brands1.gradientImageView.layer.shadowOpacity = 2.0f;
//        brands1.gradientImageView.layer.shadowRadius = 3.2f;
//        brands1.gradientImageView.layer.masksToBounds = YES;
//        if(storedIndex)
//        {
//            BrandsCollectionViewCell1 *cell = (BrandsCollectionViewCell1 *)[self.brandsCollectionView cellForItemAtIndexPath:storedIndex];
//            cell.titleButton.layer.borderWidth = 1.0f;
//            cell.titleButton.layer.borderColor = [UIColor colorWithRed:2.0/255.0 green:30.0/255.0 blue:41.0/255.0 alpha:1.0].CGColor;
//            cell.bgView.layer.cornerRadius = 2.0f;
//            cell.titleButton.layer.cornerRadius = 2.0f;
//            cell.titleLabel.textColor = [UIColor colorWithRed:65.0/255.0 green:65.0/255.0 blue:65.0/255.0 alpha:1.0];
//            cell.titleImageView.image = [cell.titleImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//            [cell.titleImageView setTintColor:[UIColor colorWithRed:65.0/255.0 green:65.0/255.0 blue:65.0/255.0 alpha:1.0]];
//        }
       
        NSString * capital;
        NSString * category = [NSString stringWithFormat:@"%@",[[self.categoriesResponseArray objectAtIndex:indexPath.row]objectForKey:@"category"]];
        capital = [category stringByReplacingCharactersInRange:NSMakeRange(0,1)
                                                    withString:[[category substringToIndex:1] capitalizedString]];
        
        
        brands1.titleLabel.text = capital;
        
        
        
       
        //        if(check==true)
        //        {
        //            NSIndexPath *indexPathForFirstRow = [NSIndexPath indexPathForRow:0 inSection:0];
        //            [self.brandsCollectionView selectItemAtIndexPath:indexPathForFirstRow animated:NO scrollPosition:UICollectionViewScrollPositionNone];
        //            check=false;
        //        }else
        //        {
        //
        //        }
        //        brands1.titleButton.tag=0;
        
        
        //        [self performSelectDeselectBorderColor:brands1.titleButton isSelected:check view:brands1.contentView label:brands1.titleLabel image:brands1.titleImageView];
        
        [brands1.titleButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
        
        
        if(storedIndex==indexPath)
        {
            //             BrandsCollectionViewCell1 *brands2 = [collectionView dequeueReusableCellWithReuseIdentifier:@"brands1" forIndexPath:storedIndex];
            //            if(brands1.titleButton.layer.borderWidth==0.0f)
            //            {
            //                brands1.titleButton.layer.borderWidth = 1.0f;
            //                brands1.titleButton.layer.borderColor = [UIColor colorWithRed:2.0/255.0 green:30.0/255.0 blue:41.0/255.0 alpha:1.0].CGColor;
            //                brands1.bgView.layer.cornerRadius = 2.0f;
            //                brands1.titleButton.layer.cornerRadius = 2.0f;
            //                brands1.titleLabel.textColor = [UIColor colorWithRed:65.0/255.0 green:65.0/255.0 blue:65.0/255.0 alpha:1.0];
            //                brands1.titleImageView.image = [brands1.titleImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            //                [brands1.titleImageView setTintColor:[UIColor colorWithRed:65.0/255.0 green:65.0/255.0 blue:65.0/255.0 alpha:1.0]];
            //
            //            }else if(brands1.titleButton.layer.borderWidth==1.0f)
            //            {
            
            UIColor *startColor = [UIColor colorWithRed:31.0/255.0 green:144.0/255.0 blue:188.0/255.0 alpha:1.0];
            UIColor *endColor = [UIColor colorWithRed:16.0/255.0 green:83.0/255.0 blue:110.0/255.0 alpha:1.0];
            theViewGradient.colors = [NSArray arrayWithObjects: (id)startColor.CGColor, (id)endColor.CGColor, nil];
            theViewGradient.startPoint = CGPointMake(0.0, 0.5);
            theViewGradient.endPoint = CGPointMake(1.0, 0.5);
            theViewGradient.frame = brands1.bounds;
            //[brands1.bgView.layer insertSublayer:theViewGradient atIndex:0];
           // brands1.bgView.backgroundColor = [UIColor colorWithRed:2.0/255.0 green:30.0/255.0 blue:41.0/255.0 alpha:1.0];
            brands1.titleButton.layer.borderWidth= 0.0f;
            brands1.titleLabel.textColor = [UIColor whiteColor];
            brands1.gradientImageView.image = [UIImage imageNamed:@"brandsGradient"];
//            brands1.titleImageView.image = [brands1.titleImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            
            NSString * etfImage = [NSString stringWithFormat:@"%@",[[self.categoriesResponseArray objectAtIndex:indexPath.row]objectForKey:@"categoryurl"]];
            NSURL *url = [NSURL URLWithString:etfImage];
            
//            NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//                if (data) {
//                    UIImage *image = [[UIImage imageWithData:data]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//                    if (image) {
//                        dispatch_async(dispatch_get_main_queue(), ^{
//
//                            if (brands1)
//                                brands1.titleImageView.image = image;
//                                [brands1.titleImageView setTintColor:[UIColor whiteColor]];
//                        });
//                    }
//                }
//            }];
//            [task resume];
            // }
            [brands1.titleImageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
            brands1.titleImageView.image = [brands1.titleImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            [brands1.titleImageView setTintColor:[UIColor whiteColor]];
        }
        else
            
        {
            //            [theViewGradient removeFromSuperlayer];
            //            UIColor *startColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];
            //            UIColor *endColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];
            //            theViewGradient.colors = [NSArray arrayWithObjects: (id)startColor.CGColor, (id)endColor.CGColor, nil];
            //            theViewGradient.startPoint = CGPointMake(0.0, 0.5);
            //            theViewGradient.endPoint = CGPointMake(1.0, 0.5);
            //            theViewGradient.frame = brands1.bounds;
            //            [brands1.bgView.layer insertSublayer:theViewGradient atIndex:0];
            //            brands1.titleLabel.textColor = [UIColor colorWithRed:65.0/255.0 green:65.0/255.0 blue:65.0/255.0 alpha:1.0];
            //
            //            brands1.titleButton.layer.borderWidth = 1.0f;
            //            brands1.titleButton.layer.borderColor = [UIColor colorWithRed:2.0/255.0 green:30.0/255.0 blue:41.0/255.0 alpha:1.0].CGColor;
            //
            //            brands1.titleImageView.image = [brands1.titleImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            //            [brands1.titleImageView setTintColor:[UIColor colorWithRed:65.0/255.0 green:65.0/255.0 blue:65.0/255.0 alpha:1.0]];
            
            brands1.titleButton.layer.borderWidth = 1.0f;
            brands1.titleButton.layer.borderColor = [UIColor colorWithRed:2.0/255.0 green:30.0/255.0 blue:41.0/255.0 alpha:1.0].CGColor;
            brands1.bgView.layer.cornerRadius = 5.0f;
          //  brands1.bgView.backgroundColor = [UIColor whiteColor];
            brands1.titleButton.layer.cornerRadius = 5.0f;
            brands1.titleLabel.textColor = [UIColor colorWithRed:65.0/255.0 green:65.0/255.0 blue:65.0/255.0 alpha:1.0];
//            brands1.titleImageView.image = [brands1.titleImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
             brands1.gradientImageView.image = nil;
            
            NSString * etfImage = [NSString stringWithFormat:@"%@",[[self.categoriesResponseArray objectAtIndex:indexPath.row]objectForKey:@"categoryurl"]];
            NSURL *url = [NSURL URLWithString:etfImage];
            
//            NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//                if (data) {
//                    UIImage *image = [[UIImage imageWithData:data]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//                    if (image) {
//                        dispatch_async(dispatch_get_main_queue(), ^{
//
//                            if (brands1)
//                                brands1.titleImageView.image = image;
//                            [brands1.titleImageView setTintColor:[UIColor colorWithRed:65.0/255.0 green:65.0/255.0 blue:65.0/255.0 alpha:1.0]];
//                        });
//                    }
//                }
//            }];
//            [task resume];
             [brands1.titleImageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
            brands1.titleImageView.image = [brands1.titleImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            [brands1.titleImageView setTintColor:[UIColor colorWithRed:65.0/255.0 green:65.0/255.0 blue:65.0/255.0 alpha:1.0]];
            
        }
        if(indexPath.row==0&&selectedCheck==true)
        {
            storedIndex=indexPath;
            UIColor *startColor = [UIColor colorWithRed:31.0/255.0 green:144.0/255.0 blue:188.0/255.0 alpha:1.0];
            UIColor *endColor = [UIColor colorWithRed:16.0/255.0 green:83.0/255.0 blue:110.0/255.0 alpha:1.0];
            theViewGradient.colors = [NSArray arrayWithObjects: (id)startColor.CGColor, (id)endColor.CGColor, nil];
            theViewGradient.startPoint = CGPointMake(0.0, 0.5);
            theViewGradient.endPoint = CGPointMake(1.0, 0.5);
            theViewGradient.frame = brands1.bounds;
            //[brands1.bgView.layer insertSublayer:theViewGradient atIndex:0];
           // brands1.bgView.backgroundColor = [UIColor colorWithRed:2.0/255.0 green:30.0/255.0 blue:41.0/255.0 alpha:1.0];
            brands1.titleButton.layer.borderWidth= 0.0f;
            brands1.titleLabel.textColor = [UIColor whiteColor];
            brands1.gradientImageView.image = [UIImage imageNamed:@"brandsGradient"];
//
          
            
            NSString * etfImage = [NSString stringWithFormat:@"%@",[[self.categoriesResponseArray objectAtIndex:indexPath.row]objectForKey:@"categoryurl"]];
            NSURL *url = [NSURL URLWithString:etfImage];
            
//            NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//                if (data) {
//                    UIImage *image = [[UIImage imageWithData:data]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//                    if (image) {
//                        dispatch_async(dispatch_get_main_queue(), ^{
//
//                            if (brands1)
//
////                                brands1.titleImageView.image = [brands1.titleImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//                                brands1.titleImageView.image = image;
//                              [brands1.titleImageView setTintColor:[UIColor whiteColor]];
//                        });
//                    }
//                }
//            }];
//            [task resume];
            [brands1.titleImageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
            brands1.titleImageView.image = [brands1.titleImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [brands1.titleImageView setTintColor:[UIColor whiteColor]];
            selectedCheck=false;
           
        }
       
        return brands1;
    }else if (collectionView == self.brandsCollectionView2)
    {
        @try
        {
        BrandsCollectionViewCell2 * brands2 = [collectionView dequeueReusableCellWithReuseIdentifier:@"brands2" forIndexPath:indexPath];
        
        brands2.activityIndicator.hidden=NO;
        [brands2.activityIndicator startAnimating];
        brands2.brandsBackView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:1.0] CGColor];
        brands2.brandsBackView.layer.shadowOffset = CGSizeMake(0, 2.0f);
        brands2.brandsBackView.layer.shadowOpacity = 2.0f;
        brands2.brandsBackView.layer.shadowRadius = 3.2f;
        brands2.brandsBackView.layer.cornerRadius=4.0f;
        brands2.brandsBackView.layer.masksToBounds = YES;
        brands2.layer.shadowColor =[[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:1.0] CGColor];
        brands2.layer.shadowOffset = CGSizeMake(0, 1.0f);
        brands2.layer.shadowRadius = 1.0f;
        brands2.layer.shadowOpacity = 0.5f;
        brands2.layer.masksToBounds = NO;
        // brands2.brandsBackView.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:brands2.bounds cornerRadius:brands2.contentView.layer.cornerRadius].CGPath;
        
        NSString * categoryImage = [NSString stringWithFormat:@"%@",[[self.categoriesListResponseArray objectAtIndex:indexPath.row]objectForKey:@"logourl"]];
        
        
        NSURL *url = [NSURL URLWithString:categoryImage];
        
//        NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//            if (data) {
//                UIImage *image = [UIImage imageWithData:data];
//                if (image) {
//                    dispatch_async(dispatch_get_main_queue(), ^{
//
//                        if (brands2)
//                            brands2.brands2ImageView.image = image;
//                    });
//                }
//            }
//        }];
//        [task resume];
        [brands2.brands2ImageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
       
        // brands2.brands2ImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[imagesArray objectAtIndex:indexPath.row]]];
        brands2.symbolNameLabel.text = [NSString stringWithFormat:@"%@",[[self.categoriesListResponseArray objectAtIndex:indexPath.row]objectForKey:@"symbol"]];
        brands2.companyNameLabel.text = [NSString stringWithFormat:@"%@",[[self.categoriesListResponseArray objectAtIndex:indexPath.row]objectForKey:@"name"]];
        
        brands2.ltpLabel.text = [NSString stringWithFormat:@"$ %.2f",[[[[self.ltpResponseArray objectAtIndex:indexPath.row] objectForKey:@"pricedata"] objectForKey:@"last"]floatValue]];
        [brands2.navigateButton addTarget:self action:@selector(onNavigateButtonTap:) forControlEvents:UIControlEventTouchUpInside];
        brands2.activityIndicator.hidden=YES;
        [brands2.activityIndicator stopAnimating];
        
        return brands2;
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
    }
    
    return 0;
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)onButtonTap:(UIButton*)sender
{
    @try
    {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.brandsCollectionView];
    NSIndexPath *indexPath = [self.brandsCollectionView indexPathForItemAtPoint:buttonPosition];
    //NSLog(@"Index path:%ld",(long)indexPath.row);
//    if(check==true)
//    {
//        check=false;
//    }
    // if(storedIndex!=indexPath)
    if(storedIndex)
    {
        BrandsCollectionViewCell1 *cell = (BrandsCollectionViewCell1 *)[self.brandsCollectionView cellForItemAtIndexPath:indexPath];
        cell.titleButton.layer.borderWidth = 1.0f;
        cell.titleButton.layer.borderColor = [UIColor colorWithRed:2.0/255.0 green:30.0/255.0 blue:41.0/255.0 alpha:1.0].CGColor;
        cell.bgView.layer.cornerRadius = 5.0f;
        cell.titleButton.layer.cornerRadius = 5.0f;
        cell.titleLabel.textColor = [UIColor colorWithRed:65.0/255.0 green:65.0/255.0 blue:65.0/255.0 alpha:1.0];
//        cell.titleImageView.image = [cell.titleImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.titleImageView setTintColor:[UIColor colorWithRed:65.0/255.0 green:65.0/255.0 blue:65.0/255.0 alpha:1.0]];
        //  cell.bgView.backgroundColor = [UIColor whiteColor];
    }
    storedIndex=indexPath;
    BrandsCollectionViewCell1 *cell = (BrandsCollectionViewCell1 *)[self.brandsCollectionView cellForItemAtIndexPath:indexPath];
    if(sender.layer.borderWidth==0.0f)
    {
        storedIndex=nil;
        cell.titleButton.layer.borderWidth = 1.0f;
        cell.titleButton.layer.borderColor = [UIColor colorWithRed:2.0/255.0 green:30.0/255.0 blue:41.0/255.0 alpha:1.0].CGColor;
        cell.bgView.layer.cornerRadius = 5.0f;
        cell.titleButton.layer.cornerRadius = 5.0f;
        cell.titleLabel.textColor = [UIColor colorWithRed:65.0/255.0 green:65.0/255.0 blue:65.0/255.0 alpha:1.0];
//        cell.titleImageView.image = [cell.titleImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.titleImageView setTintColor:[UIColor colorWithRed:65.0/255.0 green:65.0/255.0 blue:65.0/255.0 alpha:1.0]];
         // cell.bgView.backgroundColor = [UIColor whiteColor];
        cell.gradientImageView.image = nil;
        
    }else if(sender.layer.borderWidth==1.0f)
    {
        UIColor *startColor = [UIColor colorWithRed:31.0/255.0 green:144.0/255.0 blue:188.0/255.0 alpha:1.0];
        UIColor *endColor = [UIColor colorWithRed:16.0/255.0 green:83.0/255.0 blue:110.0/255.0 alpha:1.0];
        theViewGradient.colors = [NSArray arrayWithObjects: (id)startColor.CGColor, (id)endColor.CGColor, nil];
        theViewGradient.startPoint = CGPointMake(0.0, 0.5);
        theViewGradient.endPoint = CGPointMake(1.0, 0.5);
        theViewGradient.frame = cell.bounds;
      //  [cell.bgView.layer insertSublayer:theViewGradient atIndex:0];
        cell.gradientImageView.image = [UIImage imageNamed:@"brandsGradient"];
         //cell.bgView.backgroundColor = [UIColor colorWithRed:2.0/255.0 green:30.0/255.0 blue:41.0/255.0 alpha:1.0];
        cell.titleButton.layer.borderWidth= 0.0f;
        cell.titleLabel.textColor = [UIColor whiteColor];
//        cell.titleImageView.image = [cell.titleImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.titleImageView setTintColor:[UIColor whiteColor]];
    }
    catergoryNameString = [NSString stringWithFormat:@"%@",[[self.categoriesResponseArray objectAtIndex:indexPath.row] objectForKey:@"category"]];
    cell.userInteractionEnabled=NO;
    [self.brandsCollectionView reloadData];
    [self categoriesListServer];
    cell.userInteractionEnabled=YES;
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)performSelectDeselectBorderColor:(UIButton *)cellButton isSelected:(BOOL)isSelect view:(UIView *)brandsView label:(UILabel *)titleLabel image:(UIImageView *)titleImageView
{
    //    BrandsCollectionViewCell1 *cell = (BrandsCollectionViewCell1 *)[self.brandsCollectionView cellForItemAtIndexPath:storedIndex];
    if(!isSelect)
    {
        cellButton.layer.borderWidth = 1.0f;
        cellButton.layer.borderColor = [UIColor colorWithRed:2.0/255.0 green:30.0/255.0 blue:41.0/255.0 alpha:1.0].CGColor;
        brandsView.layer.cornerRadius = 2.0f;
        cellButton.layer.cornerRadius = 2.0f;
        titleLabel.textColor = [UIColor colorWithRed:65.0/255.0 green:65.0/255.0 blue:65.0/255.0 alpha:1.0];
//        titleImageView.image = [titleImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [titleImageView setTintColor:[UIColor colorWithRed:65.0/255.0 green:65.0/255.0 blue:65.0/255.0 alpha:1.0]];
    }
    else
    {
        
        UIColor *startColor = [UIColor colorWithRed:31.0/255.0 green:144.0/255.0 blue:188.0/255.0 alpha:1.0];
        UIColor *endColor = [UIColor colorWithRed:16.0/255.0 green:83.0/255.0 blue:110.0/255.0 alpha:1.0];
        theViewGradient.colors = [NSArray arrayWithObjects: (id)startColor.CGColor, (id)endColor.CGColor, nil];
        theViewGradient.startPoint = CGPointMake(0.0, 0.5);
        theViewGradient.endPoint = CGPointMake(1.0, 0.5);
        theViewGradient.frame = brandsView.bounds;
        [brandsView.layer insertSublayer:theViewGradient atIndex:0];
        cellButton.layer.borderWidth= 0.0f;
        titleLabel.textColor = [UIColor whiteColor];
//        titleImageView.image = [titleImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [titleImageView setTintColor:[UIColor whiteColor]];
    }
}
-(void)onNavigateButtonTap:(UIButton*)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"USA" bundle: nil];
    
    
    //    MarketWatchViewController *viewController = (MarketWatchViewController*)[[(AppDelegate*)
    //                                                                [[UIApplication sharedApplication]delegate] window] rootViewController];
    
    // UIViewController * viewController = rootController;
    
    // MainViewUSA *viewController = [[MainViewUSA alloc] init];
    //    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController1];
    //    [self.window setRootViewController:navigationController];
    //    [self.window makeKeyAndVisible];
    UIViewController *viewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    if ( viewController.presentedViewController && !viewController.presentedViewController.isBeingDismissed ) {
        viewController = viewController.presentedViewController;
    }
    
    //NSLog(@"keyWindow --------> %@",[UIApplication sharedApplication].keyWindow.rootViewController);
    //NSLog(@"delegate.window --> %@",[UIApplication sharedApplication].delegate.window.rootViewController);
    // //NSLog(@"self.view.window -> %@",self.view.window.rootViewController);
    
    
    //   // NSLayoutConstraint *constraint = [NSLayoutConstraint
    //                                      constraintWithItem:alert.view
    //                                      attribute:NSLayoutAttributeHeight
    //                                      relatedBy:NSLayoutRelationLessThanOrEqual
    //                                      toItem:nil
    //                                      attribute:NSLayoutAttributeNotAnAttribute
    //                                      multiplier:1
    //                                      constant:viewController.view.frame.size.height*2.0f];
    //
    // [alert.view addConstraint:constraint];
    
    
    
    
    
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.brandsCollectionView2];
    NSIndexPath *indexPath = [self.brandsCollectionView2 indexPathForItemAtPoint:buttonPosition];
    //NSLog(@"Index path:%ld",(long)indexPath.row);
    //   NewOrderViewController * trendingOrders = [storyboard instantiateViewControllerWithIdentifier:@"NewOrderViewController"];
    delegate.stockTickerString = [NSString stringWithFormat:@"%@",[[self.categoriesListResponseArray objectAtIndex:indexPath.row]objectForKey:@"symbol"]];
    // delegate.orderRecomdationType = [[[self.responseDict objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"rating"];
    //  trendingOrders.recomondation =   [[[self.responseDict objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"rating"];
    
    delegate.tickerName=[NSString stringWithFormat:@"%@",[[self.categoriesListResponseArray objectAtIndex:indexPath.row]objectForKey:@"symbol"]];
    delegate.companyName =[NSString stringWithFormat:@"%@",[[self.categoriesListResponseArray objectAtIndex:indexPath.row]objectForKey:@"symbol"]];;
    //    trendingOrders.tickerName = [NSString stringWithFormat:@"%@",[[self.categoriesListResponseArray objectAtIndex:indexPath.row]objectForKey:@"symbol"]];
    //    trendingOrders.companyName =[NSString stringWithFormat:@"%@",[[self.categoriesListResponseArray objectAtIndex:indexPath.row]objectForKey:@"name"]];
    //  delegate.etfOrderCheck=@"etf";
    
    //     [UINavigationController pushViewController:trendingOrders animated:YES];
    // [viewController presentViewController:trendingOrders animated:YES completion:nil];
    //  [viewController presentViewController:trendingOrders animated:YES completion:nil];
    delegate.brandsNavigationCheck=true;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"brands" object:nil];
    
    
    
    
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //    if(collectionView == self.brandsCollectionView)
    //    {
    //        UICollectionViewCell *brands1 = [collectionView cellForItemAtIndexPath:indexPath];
    //        BrandsCollectionViewCell1 *brands2 = [collectionView dequeueReusableCellWithReuseIdentifier:@"brands1" forIndexPath:indexPath];
    //        if (brands1.selected) {
    ////            if(indexPath.row==0)
    ////            {
    ////                brands1.backgroundColor = [UIColor whiteColor];
    ////                brands1.layer.cornerRadius = 4.0f;
    ////                brands1.contentView.layer.borderWidth = 1.0f;
    ////                brands1.contentView.layer.borderColor = [UIColor colorWithRed:2.0/255.0 green:30.0/255.0 blue:41.0/255.0 alpha:1.0].CGColor;
    ////                [theViewGradient removeFromSuperlayer];
    ////            }
    //           // brands1.backgroundColor = [UIColor colorWithRed:255/255.0 green:205/255.0 blue:3/255.0 alpha:1]; // highlight selection
    //            UIColor *startColor = [UIColor colorWithRed:31.0/255.0 green:144.0/255.0 blue:188.0/255.0 alpha:1.0];
    //            UIColor *endColor = [UIColor colorWithRed:16.0/255.0 green:83.0/255.0 blue:110.0/255.0 alpha:1.0];
    //            theViewGradient.colors = [NSArray arrayWithObjects: (id)startColor.CGColor, (id)endColor.CGColor, nil];
    //            theViewGradient.startPoint = CGPointMake(0.0, 0.5);
    //            theViewGradient.endPoint = CGPointMake(1.0, 0.5);
    //            theViewGradient.frame = brands1.bounds;
    //            [brands1.contentView.layer insertSublayer:theViewGradient atIndex:0];
    //            brands1.layer.borderWidth=0;
    //            brands2.titleImageView.image = [brands2.titleImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    //            [brands2.titleImageView setTintColor:[UIColor whiteColor]];
    //            brands2.titleLabel.textColor = [UIColor whiteColor];
    //            //NSIndexPath *indexPaths = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
    ////            NSMutableArray * indexPathArray = [[NSMutableArray alloc]init];
    ////            [indexPathArray addObject:indexPath];
    ////            [self.brandsCollectionView reloadItemsAtIndexPaths:indexPathArray];
    //        }
    //        else
    //        {
    ////           // brands1.backgroundColor = [UIColor colorWithRed:2/255.0 green:30/255.0 blue:41/255.0 alpha:1]; // Default color
    //           brands1.backgroundColor = [UIColor whiteColor];
    ////           // [theViewGradient removeFromSuperlayer];
    //        }
    ////        UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    //        catergoryNameString = [NSString stringWithFormat:@"%@",[[self.categoriesResponseArray objectAtIndex:indexPath.row] objectForKey:@"category"]];
    ////        cell.backgroundColor = [UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:1];
    //        brands1.userInteractionEnabled=NO;
    //        [self categoriesListServer];
    //        brands1.userInteractionEnabled=YES;
    //       // [collectionView deselectItemAtIndexPath:indexPath animated:NO];
    //    }else if (collectionView == self.brandsCollectionView2)
    //    {
    //
    //    }
}


//- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    if(collectionView==self.brandsCollectionView)
//    {
//        UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
//        cell.contentView.backgroundColor = [UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1];
//    }
//}
//- (void)collectionView:(UICollectionView *)collectionView didhighlightItemAtIndexPath:(NSIndexPath *)indexPath
//{
//
//    if(collectionView==self.brandsCollectionView)
//    {
//        UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
//       cell.contentView.backgroundColor = [UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:1];
//        [collectionView deselectItemAtIndexPath:indexPath animated:NO];
//
//
//    }
//}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //    if(collectionView == self.brandsCollectionView)
    //    {
    //    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    //    //cell.backgroundColor = [UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1];
    //
    //        cell.backgroundColor = [UIColor whiteColor];
    //    }
}


//- (CGSize)collectionView:(UICollectionView *)collectionView
//                  layout:(UICollectionViewLayout*)collectionViewLayout
//  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    return CGSizeMake(self.brandsCollectionView.frame.size.width, 80);
//    //return CGSizeMake(50, 50);
//}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:YES];
    
    // Configure the view for the selected state
}

@end
