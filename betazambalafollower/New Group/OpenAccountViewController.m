//
//  OpenAccountViewController.m
//  ZambalaUSA
//
//  Created by Zenwise Technologies on 13/11/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "OpenAccountViewController.h"
#import "BrokersWebViewControllerUSA.h"
#import "LoginViewController.h"
#import "USOpenAccountViewController.h"
#import "TwoUSOpenAccountViewController.h"
#import "ThreeUSOpenAccountViewController.h"
#import "AppDelegate.h"
#import <Mixpanel/Mixpanel.h>

@interface OpenAccountViewController ()

@end

@implementation OpenAccountViewController
{
    NSString * typeCheck;
    AppDelegate * delegate;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.nextButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.nextButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.nextButton.layer.shadowOpacity = 1.0f;
    self.nextButton.layer.shadowRadius = 1.0f;
    self.nextButton.layer.cornerRadius=1.0f;
    self.nextButton.layer.masksToBounds = NO;
    self.alreadyHaveButton.hidden=YES;
    self.enterEmailLabel.hidden=YES;
    self.enterEmailHeight.constant=0;
    self.emailTF.hidden=YES;
    self.emailTFHeight.constant=0;
    [self.virtualAccountButton setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    [self.virtualAccountButton setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    
    
    [self.openAccountButton setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    [self.openAccountButton setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    typeCheck=@"Real";
    
    self.virtualAccountButton.selected=NO;
    self.openAccountButton.selected=NO;
    
    [self.virtualAccountButton addTarget:self action:@selector(onVirtualButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.openAccountButton addTarget:self action:@selector(onOpenAccountTap) forControlEvents:UIControlEventTouchUpInside];
    
    [self.nextButton addTarget:self action:@selector(onNextButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.alreadyHaveButton addTarget:self action:@selector(onAlreadyHaveButton) forControlEvents:UIControlEventTouchUpInside];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
     [self.navigationController setNavigationBarHidden:NO animated:YES];
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"usa_open_account_page"];
}
-(void)dismissKeyboard
{
    [self.emailTF resignFirstResponder];
}
-(void)onVirtualButtonTap
{
    [self.nextButton setTitle:@"Continue" forState:UIControlStateNormal];
    // self.enterEmailLabel.hidden=NO;
    // self.enterEmailHeight.constant=22;
    self.virtualAccountButton.selected=YES;
    self.openAccountButton.selected=NO;
    //self.emailTF.hidden=NO;
    //self.emailTFHeight.constant=30;
    typeCheck=@"Login";
}
-(void)onOpenAccountTap
{
    [self.nextButton setTitle:@"Next" forState:UIControlStateNormal];
    self.enterEmailLabel.hidden=YES;
    self.enterEmailHeight.constant=0;
    self.virtualAccountButton.selected=NO;
    self.openAccountButton.selected=YES;
    self.emailTF.hidden=YES;
    self.emailTFHeight.constant=0;
    typeCheck=@"Open";
}
-(void)onNextButtonTap
{
    @try
    {
    if([typeCheck isEqualToString:@"Open"])
    {
        NSUserDefaults * openAccountPref = [NSUserDefaults standardUserDefaults];
        NSString * checkString = [openAccountPref stringForKey:@"openAccountStep"];
        if(checkString.length>0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Please choose an option" message:@"You have already filled a part of the application" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                
                UIAlertAction * countinue=[UIAlertAction actionWithTitle:@"Continue" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    NSUserDefaults * openAppIDPref = [NSUserDefaults standardUserDefaults];
                    NSString * appIDString = [openAppIDPref objectForKey:@"openAppID"];
                    if(appIDString.length>0)
                    {
                        delegate.appIDString = appIDString;
                    }else
                    {
                        delegate.appIDString = @"";
                    }
                    
                    NSUserDefaults * openFinalDict = [NSUserDefaults standardUserDefaults];
                    delegate.finalChoiceTradeDict = [[openFinalDict objectForKey:@"openDict"] mutableCopy];
                    
                    NSUserDefaults * openMobPref = [NSUserDefaults standardUserDefaults];
                    NSString * mobileNumber = [openMobPref objectForKey:@"openMob"];
                    if([checkString isEqualToString:@"step2"])
                    {
                        TwoUSOpenAccountViewController * openAccountTwo = [self.storyboard instantiateViewControllerWithIdentifier:@"TwoUSOpenAccountViewController"];
                        openAccountTwo.mobileNumber = mobileNumber;
                        [self.navigationController pushViewController:openAccountTwo animated:YES];
                    }else if ([checkString isEqualToString:@"step3"])
                    {
                        ThreeUSOpenAccountViewController * threeOpenAccount = [self.storyboard instantiateViewControllerWithIdentifier:@"ThreeUSOpenAccountViewController"];
                        [self.navigationController pushViewController:threeOpenAccount animated:YES];
                    }
                }];
                
                UIAlertAction * startOver=[UIAlertAction actionWithTitle:@"Startover" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    NSUserDefaults *openAccountPref  = [NSUserDefaults standardUserDefaults];
                    [openAccountPref removeObjectForKey:@"openAccountStep"];
                    
                    NSUserDefaults * openAppIDPref = [NSUserDefaults standardUserDefaults];
                    [openAppIDPref removeObjectForKey:@"openAppID"];
                    
                    NSUserDefaults * openMobPref = [NSUserDefaults standardUserDefaults];
                    [openMobPref removeObjectForKey:@"openMob"];
                    
                    NSUserDefaults * openFinalDict = [NSUserDefaults standardUserDefaults];
                    [openFinalDict removeObjectForKey:@"openDict"];
                    USOpenAccountViewController * openAccount = [self.storyboard instantiateViewControllerWithIdentifier:@"USOpenAccountViewController"];
                    [self.navigationController pushViewController:openAccount animated:YES];
                }];
                
                
                [alert addAction:countinue];
                [alert addAction:startOver];
                
            });
        }else
        {
        USOpenAccountViewController * openAccount = [self.storyboard instantiateViewControllerWithIdentifier:@"USOpenAccountViewController"];
        [self.navigationController pushViewController:openAccount animated:YES];
        }
        
        //    BrokersWebViewControllerUSA * webView = [self.storyboard instantiateViewControllerWithIdentifier:@"BrokersWebViewControllerUSA"];
        //    [self.navigationController pushViewController:webView animated:YES];
    }else if ([typeCheck isEqualToString:@"Login"])
    {
        
        LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        login.reLoginCheck=@"relogin";
        [self presentViewController:login animated:YES completion:nil];
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
-(void)onAlreadyHaveButton
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
