//
//  MarketMonksViewController.h
//  ZambalaUSA
//
//  Created by guna on 17/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarketMonksViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *monksTableView;
@property NSMutableDictionary *allAnalystsDict;
@property NSMutableArray *allAnalystArray,*topAnalysts,*allAnalystsNames;


@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
