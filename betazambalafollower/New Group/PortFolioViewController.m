//
//  PortFolioViewController.m
//  
//
//  Created by Zenwise Technologies on 28/09/17.
//
//

#import "PortFolioViewController.h"
#import "HMSegmentedControl.h"
#import "AppDelegate.h"
#import "DematTableViewCell.h"
#import "NewOrderViewController.h"
#import "Mixpanel/Mixpanel.h"
#import "BrokersWebViewControllerUSA.h"
#import "LoginViewController.h"


@interface PortFolioViewController ()

@end

@implementation PortFolioViewController
{
    HMSegmentedControl * segmentedControl;
    AppDelegate * delegate;
    NSString * allSymbolsString;
    float unRealisedPL;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.positionsArray = [[NSMutableArray alloc]init];
    self.groupArray = [[NSMutableArray alloc]init];
    
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"HOLDINGS",@"BALANCES"]];
    self.navigationItem.title = @"Portfolio";
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"usa_portfolio_page"];
    [mixpanelMini track:@"usa_holdings_page"];
    
        if([delegate.accountCheck isEqualToString:@"dummy"])
        {
            segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"HOLDINGS",@"BALANCES"]];
        }else if ([delegate.accountCheck isEqualToString:@"real"])
        {
            segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"HOLDINGS",@"BALANCES"]];
        }
        segmentedControl.frame = CGRectMake(0, 0, self.view.frame.size.width, 56);
        segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
        //    segmentedControl.verticalDividerEnabled = YES;
        //    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(251/255.0) green:(196/255.0) blue:(12/255.0) alpha:1];
        segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
        [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
        segmentedControl.backgroundColor=[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1];
        segmentedControl.tintColor=[UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:0.5];
    [self.view addSubview:segmentedControl];
    
   
    self.noPortFolioView.hidden=YES;
    self.balancesView.hidden=YES;
    self.dematView.hidden=NO;
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated
{
    if(delegate.sessionID.length>0)
    {
        [self getPositionsList];
    }else
    {
        [self loginCheck];
    }
}
-(void)loginCheck
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Please choose an option." message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [self presentViewController:alert animated:YES completion:^{
        
    }];
    UIAlertAction * OpenAccount=[UIAlertAction actionWithTitle:@"Open Broking Account" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //Navigate to open E- Account page
        BrokersWebViewControllerUSA * openAccountWebView = [self.storyboard instantiateViewControllerWithIdentifier:@"BrokersWebViewControllerUSA"];
        [self.navigationController pushViewController:openAccountWebView animated:YES];
    }];
    UIAlertAction * Login=[UIAlertAction actionWithTitle:@"Login" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //Navigate to login page
        LoginViewController *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        login.reLoginCheck=@"relogin";
        [self presentViewController:login animated:YES completion:nil];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        self.noPortFolioView.hidden=NO;
    }];
    [alert addAction:OpenAccount];
    [alert addAction:Login];
    [alert addAction:cancel];
}



-(void)getPositionsList
{
    @try
    {
    self.dematView.hidden=NO;
    NSString * str = [NSString stringWithFormat:@"api={\"name\":\"JSON_GetPositionList\",\"client_id\":\"T7qqV91jR2\",\"session_id\":\"%@\",\"firm_code\":\"5ntJOPXqQb\",\"account\":\"%@\",\"sortBy\":\"1\"}",delegate.sessionID,delegate.accountNumber];
    NSData *dt = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]init];
    if([delegate.accountCheck isEqualToString:@"dummy"])
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.dummyAccountURL]
                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                      timeoutInterval:10.0];
    }else if ([delegate.accountCheck isEqualToString:@"real"])
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.realAccountURL]
                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                      timeoutInterval:10.0];
    }
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody:dt];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        self.positionsDictionary = [NSJSONSerialization
                                                                              JSONObjectWithData:data
                                                                              options:kNilOptions
                                                                              error:&error];
                                                        //NSLog(@"Positions:%@",self.positionsDictionary);
                                                        
                                                        if(self.positionsArray.count>0)
                                                        {
                                                            [self.positionsArray removeAllObjects];
                                                        }
                                                       
                                                        self.positionsArray = [[NSMutableArray alloc]initWithArray:[self.positionsDictionary objectForKey:@"records"]];
                                                        float amount;
                                                        float ltp;
                                                        float totalValue;
                                                        NSMutableArray * array = [[NSMutableArray alloc]init];
                                                        NSMutableArray * array1 = [[NSMutableArray alloc]init];
                                                        for (int i=0; i<self.positionsArray.count; i++) {
                                                            [array addObject:[[self.positionsArray objectAtIndex:i] objectForKey:@"symbol"]];
                                                            amount = [[[self.positionsArray objectAtIndex:i] objectForKey:@"amount"] floatValue];
                                                            ltp = [[[self.positionsArray objectAtIndex:i] objectForKey:@"tradePrice"] floatValue];
                                                            totalValue = amount*ltp;
                                                            [array1 addObject:[NSNumber numberWithFloat:totalValue]];
                                                        }
                                                        NSNumber* sum = [array1 valueForKeyPath:@"@sum.self"];
                                                        NSString * totalholdingsValue = [NSString stringWithFormat:@"%@",sum];
                                                        //NSLog(@"Totalholdingsvalue:%@",totalholdingsValue);

                                                        allSymbolsString = [array componentsJoinedByString:@","];
                                                        
                                                        Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
                                                        [mixpanelMini identify:delegate.USAuserID];
                                                        
                                                        
                                                        [mixpanelMini.people set:@{@"holdingslist":allSymbolsString}];
                                                        [mixpanelMini.people set:@{@"totalholdingsvalue":totalholdingsValue}];
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                self.dematTableView.delegate=self;
                                                                self.dematTableView.dataSource=self;
                                                                [self.dematTableView reloadData];
                                                                
                                                                if([[self.positionsDictionary objectForKey:@"records"] count]==0)
                                                                {
                                                                    self.noPortFolioView.hidden=NO;
                                                                }
                                                               
                                                                
                                                            });
                                                        
                                                    }
                                                }];
    [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)segmentedControlChangedValue
{
     Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    if(segmentedControl.selectedSegmentIndex==0)
    {
        [mixpanelMini track:@"usa_holdings_page"];
        self.balancesView.hidden=YES;
        self.dematView.hidden=NO;
        if(delegate.sessionID.length>0)
        {
        [self getPositionsList];
        }else
        {
            [self loginCheck];
        }
    }else if (segmentedControl.selectedSegmentIndex==1)
    {
        [mixpanelMini track:@"usa_balances_page"];
        self.dematView.hidden=YES;
        self.balancesView.hidden=NO;
        if(delegate.sessionID.length>0)
        {
        [self balancesServerHit];
        }else
        {
            [self loginCheck];
        }
    }
}

-(void)balancesServerHit
{
    @try
    {
    //CTA20759
        NSString * str = [NSString stringWithFormat:@"api={\"name\":\"JSON_RetrieveAccount\",\"client_id\":\"T7qqV91jR2\",\"session_id\":\"%@\",\"firm_code\":\"5ntJOPXqQb\",\"account\":\"%@\"}",delegate.sessionID,delegate.accountNumber];
    
        NSData *dt = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]init];
    if([delegate.accountCheck isEqualToString:@"dummy"])
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.dummyAccountURL]
                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                      timeoutInterval:10.0];
    }else if ([delegate.accountCheck isEqualToString:@"real"])
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.realAccountURL]
                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                      timeoutInterval:10.0];
    }
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        [request setHTTPBody:dt];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            NSDictionary* json = [NSJSONSerialization
                                                                                  JSONObjectWithData:data
                                                                                  options:kNilOptions
                                                                                  error:&error];
                                                            //NSLog(@"jjson %@",json);
                                                            NSMutableArray * sumArray = [[NSMutableArray alloc]init];
                                                            for (int i=0; i<self.positionsArray.count; i++) {
                                                                NSString * amount = [NSString stringWithFormat:@"%.2f",[[[self.positionsArray objectAtIndex:i]objectForKey:@"amount"]floatValue]];
                                                                NSString * ltp = [NSString stringWithFormat:@"%.2f",[[[self.positionsArray objectAtIndex:i]objectForKey:@"lastPrice"]floatValue]];
                                                                float a = [amount floatValue]*[ltp floatValue];
                                                                [sumArray addObject:[NSNumber numberWithFloat:a]];
                                                            }
                                                            NSNumber *sum = [sumArray valueForKeyPath:@"@sum.floatValue"];
                                                            NSString * total = [NSString stringWithFormat:@"%.2f",[[json objectForKey:@"ActualCash_CashBal"]floatValue]];
                                                            float bTotal = [sum floatValue]+[total floatValue];
                                                                
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    
                                                                    self.cashBalanceView.text= [NSString stringWithFormat:@"$%@",total];
                                                                    self.securitiesValue.text = [NSString stringWithFormat:@"$%.2f",[sum floatValue]];
                                                                    self.totalValueLabel.text=[NSString stringWithFormat:@"$%.2f",bTotal];
                                                                    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
                                                                    
                                                                    [mixpanelMini identify:delegate.USAuserID];
                                                                    [mixpanelMini.people set:@{@"ussecurityvalue":self.securitiesValue.text}];
                                                                    [mixpanelMini.people set:@{@"ustotalvalue":self.totalValueLabel.text}];
                                                                   
                                                                });
                                                            
                                                        }
                                                    }];
        [dataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.positionsArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
    DematTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"DematTableViewCell" forIndexPath:indexPath];
    NSString * find = [[self.positionsArray objectAtIndex:indexPath.row] objectForKey:@"symbol"];
    NSInteger strCount = [allSymbolsString length] - [[allSymbolsString stringByReplacingOccurrencesOfString:find withString:@""] length];
    strCount /= [find length];
    NSString * tickerName = [[[self.positionsDictionary objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"symbol"];
    
    cell.tickerNameLabel.text = tickerName;
    
    NSString * companyName = [[[self.positionsDictionary objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"longName"];
    cell.companyNameLabel.text= companyName;
    
    cell.quantityLabel.text = [[[[self.positionsDictionary objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"amount"] stringValue];
    NSString * realisedPL = [[[[self.positionsDictionary objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"realizedPL"] stringValue];
    cell.realisedPandL.text = [NSString stringWithFormat:@"%.2f",[realisedPL floatValue]];
    
    [cell.tradeButton addTarget:self action:@selector(onTradeButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    float amount = [[[[self.positionsDictionary objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"amount"] floatValue];
//    float bid = [[[[self.positionsDictionary objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"bid"] floatValue];
//    float ask = [[[[self.positionsDictionary objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"ask"] floatValue];
    float ltp = [[[[self.positionsDictionary objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"lastPrice"] floatValue];
    float tradePrice = [[[[self.positionsDictionary objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"tradePrice"] floatValue];
//    NSString * amountString =[NSString stringWithFormat:@"%@",[[[[self.positionsDictionary objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"amount"] stringValue]];
//    if([amountString containsString:@"-"])
//    {
//        unRealisedPL =(amount*(bid-ltp));
//
//    }else
//    {
//        unRealisedPL =(amount*(ask-ltp));
//    }
    unRealisedPL =(amount*(ltp-tradePrice));
    cell.unRealisedPandL.text = [NSString stringWithFormat:@"%.2f",unRealisedPL];
    return cell;
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(void)onTradeButtonTap:(UIButton*)sender
{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.dematTableView];
    NSIndexPath *indexPath = [self.dematTableView indexPathForRowAtPoint:buttonPosition];
    //NSLog(@"Index path:%ld",(long)indexPath.row);
//    NewOrderViewController * Trade = [self.storyboard instantiateViewControllerWithIdentifier:@"NewOrderViewController"];
//
//    editOrder.editQuantity = [[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"quantity"];
//    editOrder.editLimitPrice= [[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"limitPriceDeb"];
//    editOrder.tickerName=[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"symbol"];
//    editOrder.editOrderID=[[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"orderID"];
//    delegate.stockTickerString = [[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"symbol"];
//    editOrder.companyName = [[[[self.ordersResponseDictionary objectForKey:@"orders"] objectForKey:@"records"] objectAtIndex:indexPath.row] objectForKey:@"symbol"];
//    [self presentViewController:editOrder animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
