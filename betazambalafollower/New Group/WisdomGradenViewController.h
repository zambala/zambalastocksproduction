//
//  WisdomGradenViewController.h
//  ZambalaUSA
//
//  Created by guna on 19/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WisdomGradenViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *wisdomGradenTableView;
@property (weak, nonatomic) IBOutlet UIView *tredingHeaderView;
@property (weak, nonatomic) IBOutlet UIView *analystHeaderView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *analystHeaderViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tredingHeaderViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *WGTableViewHeight;

@property NSMutableArray *trendingStocksResponseArray;
@property (weak, nonatomic) IBOutlet UIScrollView *scroll;

@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UIView *view2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view1Hgt;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view2Hgt;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property NSMutableArray * analystAdviceArray;
@property NSMutableArray *ltpResponseArray;
@property NSMutableArray * tickerStoreArray;
@property NSMutableDictionary * ltpResponseDictionary;



@end
