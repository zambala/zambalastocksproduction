//
//  TradesViewController.m
//  ZambalaUSA
//
//  Created by Zenwise Technologies on 04/12/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "TradesViewController.h"
#import "AppDelegate.h"
#import "TradesTableViewCell.h"
#import "TabMenuView.h"
#import "NoTradesTableViewCell.h"
#import "BrokersWebViewControllerUSA.h"
#import "LoginViewController.h"
#import <Mixpanel/Mixpanel.h>

@interface TradesViewController ()

@end

@implementation TradesViewController
{
    AppDelegate * delegate;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tradesResponseArray = [[NSMutableArray alloc]init];
    self.activityIndicator.hidden=YES;
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"usa_trade_page"];
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    self.dateString = [dateFormat stringFromDate:today];
    //NSLog(@"date: %@", self.dateString);
    if(delegate.sessionID.length>0)
    {
    [self tradesServer];
    }else
    {
        [self loginCheck];
    }
    
    // Do any additional setup after loading the view.
}
-(void)loginCheck
{
    @try
    {
    dispatch_async(dispatch_get_main_queue(), ^{
       
    
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Please choose an option." message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [self presentViewController:alert animated:YES completion:^{
        
    }];
//    UIAlertAction * OpenAccount=[UIAlertAction actionWithTitle:@"Open E-Account" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        //Navigate to open E- Account page
//        BrokersWebViewControllerUSA * openAccountWebView = [self.storyboard instantiateViewControllerWithIdentifier:@"BrokersWebViewControllerUSA"];
//        [self.navigationController pushViewController:openAccountWebView animated:YES];
//    }];
    UIAlertAction * Login=[UIAlertAction actionWithTitle:@"Login" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //Navigate to login page
        LoginViewController *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        login.reLoginCheck=@"relogin";
        [self presentViewController:login animated:YES completion:nil];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
       [self dismissViewControllerAnimated:YES completion:nil];
    }];
//    [alert addAction:OpenAccount];
    [alert addAction:Login];
    [alert addAction:cancel];
        self.tradesTableView.delegate=self;
        self.tradesTableView.dataSource=self;
        [self.tradesTableView reloadData];
   
        });
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)tradesServer
{
    @try
    {
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
        //NSString * str = [NSString stringWithFormat:@"api={\"name\":\"JSON_GetOrderListPaginated\",\"client_id\":\"T7qqV91jR2\",\"session_id\":\"%@\",\"firm_code\":\"5ntJOPXqQb\",\"account\":\"%@\",\"sortBy\":\"%@\",\"page\":\"1\",\"startDate\":\"%@\",\"endDate\":\"%@\"}",delegate.sessionID,delegate.accountNumber,@"23",self.dateString,self.dateString];
        
        NSString * str = [NSString stringWithFormat:@"api={\"name\":\"JSON_GetOrderListPaginated\",\"client_id\":\"T7qqV91jR2\",\"session_id\":\"%@\",\"firm_code\":\"5ntJOPXqQb\",\"account\":\"%@\",\"sortBy\":\"%@\",\"page\":\"1\",\"startDate\":\"%@\",\"endDate\":\"%@\"}",delegate.sessionID,delegate.accountNumber,@"23",@"2017-01-01",@"2017-12-04"];
        
        NSData *dt = [str dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableURLRequest *request=[[NSMutableURLRequest alloc]init];
        if([delegate.accountCheck isEqualToString:@"dummy"])
        {
            request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.dummyAccountURL]
                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                          timeoutInterval:10.0];
        }else if ([delegate.accountCheck isEqualToString:@"real"])
        {
            request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.realAccountURL]
                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                          timeoutInterval:10.0];
        }
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        [request setHTTPBody:dt];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            NSDictionary* json = [NSJSONSerialization
                                                                                  JSONObjectWithData:data
                                                                                  options:kNilOptions
                                                                                  error:&error];
                                                            //NSLog(@"jjson %@",json);
                                                            if(self.tradesResponseArray.count>0)
                                                            {
                                                                [self.tradesResponseArray removeAllObjects];
                                                            }
                                                            
                                                            if([[json objectForKey:@"records"]isEqual:[NSNull null]])
                                                            {
                                                                
                                                            }else
                                                            {
                                                            [self.tradesResponseArray addObjectsFromArray:[json objectForKey:@"records"]];
                                                            }
                                                            //NSLog(@"Trades:%@",self.tradesResponseArray);
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                self.activityIndicator.hidden=YES;
                                                                [self.activityIndicator stopAnimating];
                                                                self.tradesTableView.delegate=self;
                                                                self.tradesTableView.dataSource=self;
                                                                [self.tradesTableView reloadData];
                                                            });
                                                        }
                                                    }];
        [dataTask resume];
}
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.tradesResponseArray.count>0)
    {
    return self.tradesResponseArray.count;
    }else
    {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
    if(self.tradesResponseArray.count>0)
    {
    TradesTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"TradesTableViewCell" forIndexPath:indexPath];
    cell.orderIDLabel.text = [NSString stringWithFormat:@"%@",[[self.tradesResponseArray objectAtIndex:indexPath.row]objectForKey:@"orderID"]];
    cell.symbolLabel.text = [NSString stringWithFormat:@"%@",[[self.tradesResponseArray objectAtIndex:indexPath.row]objectForKey:@"symbol"]];
    cell.dateAndTimeLabel.text = [NSString stringWithFormat:@"%@",[[self.tradesResponseArray objectAtIndex:indexPath.row]objectForKey:@"timeplaced"]];
    NSString * transactionType = [NSString stringWithFormat:@"%@",[[self.tradesResponseArray objectAtIndex:indexPath.row]objectForKey:@"action"]];
    if([transactionType isEqualToString:@"1"])
    {
        cell.buySellLabel.text = @"Buy";
    }else if ([transactionType isEqualToString:@"2"])
    {
        cell.buySellLabel.text = @"Sell";
    }
    cell.totalQuantityLabel.text=[NSString stringWithFormat:@"%@",[[self.tradesResponseArray objectAtIndex:indexPath.row]objectForKey:@"quantity"]];
    cell.filledQuantityLabel.text= [NSString stringWithFormat:@"%@",[[self.tradesResponseArray objectAtIndex:indexPath.row]objectForKey:@"fillQty"]];
     cell.pendingQuantityLabel.text= [NSString stringWithFormat:@"%@",[[self.tradesResponseArray objectAtIndex:indexPath.row]objectForKey:@"remainingQty"]];
    cell.priceLabel.text= [NSString stringWithFormat:@"%@",[[self.tradesResponseArray objectAtIndex:indexPath.row]objectForKey:@"fillPrice"]];
    cell.mtomPandLLabel.text=@"--";
    return cell;
    }else
    {
        NoTradesTableViewCell * noTrades = [tableView dequeueReusableCellWithIdentifier:@"NoTradesTableViewCell" forIndexPath:indexPath];
        return noTrades;
    }
    return nil;
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onBackButtonTap:(id)sender {
    TabMenuView * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"TabMenuView"];
    [self presentViewController:tabPage animated:YES completion:nil];
}
@end
