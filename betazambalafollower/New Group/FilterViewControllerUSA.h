//
//  FilterViewController.h
//  ZambalaUSA
//
//  Created by guna on 19/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterViewControllerUSA : UIViewController
- (IBAction)backButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *sectorButton;
@property (weak, nonatomic) IBOutlet UIView *rankingView;
@property (weak, nonatomic) IBOutlet UIView *compareView;
@property (weak, nonatomic) IBOutlet UIView *ratingView;

@end
