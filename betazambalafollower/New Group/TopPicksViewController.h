//
//  TopPicksViewController.h
//  ZambalaUSA
//
//  Created by guna on 17/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopPicksViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *topPicksTableView;
@property NSMutableDictionary *topPicksResponseDictionary;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property NSMutableArray *ltpResponseArray;
@property NSMutableArray *tickerStoreArray;
@property NSMutableDictionary * ltpResponseDictionary;
@property (weak, nonatomic) IBOutlet UIView *segmentedControlView;
@property NSMutableArray *trendingStocksResponseArray;

@end
