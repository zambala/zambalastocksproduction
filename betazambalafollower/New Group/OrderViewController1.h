//
//  OrderViewController1.h
//  ZambalaUSA
//
//  Created by guna on 20/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderViewController1 : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *orderTableView;
@property (weak, nonatomic) IBOutlet UIView *orderTypeView;
@property (weak, nonatomic) IBOutlet UIButton *submitOrderButton;

@end
