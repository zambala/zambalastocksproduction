//
//  VoucherTermsPopupViewController.m
//  betazambalafollower
//
//  Created by Zenwise Technologies Private Limited on 25/08/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import "VoucherTermsPopupViewController.h"
#import "UIViewController+MJPopupViewController.h"

@interface VoucherTermsPopupViewController ()

@end

@implementation VoucherTermsPopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.termsAndConditions.text = self.termsAndConditionsString;
    [self.okayButton addTarget:self action:@selector(onButtonTap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view from its nib.
}

-(void)onButtonTap
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
