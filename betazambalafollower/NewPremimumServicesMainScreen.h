//
//  NewPremimumServicesMainScreen.h
//  PremiumServices
//
//  Created by zenwise mac 2 on 7/13/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "ViewController.h"

@interface NewPremimumServicesMainScreen : ViewController
@property (weak, nonatomic) IBOutlet UIView *premiumAdvicesView;
@property (weak, nonatomic) IBOutlet UIView *signalsView;
@property (weak, nonatomic) IBOutlet UIView *dealerModeView;
- (IBAction)onPremiumAdvicesButtonTap:(id)sender;
- (IBAction)onSignalsButtonTap:(id)sender;

- (IBAction)onDealerModeButtonTap:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *premiumAdvicesButton;
@property (weak, nonatomic) IBOutlet UIButton *signalsButton;
@property (weak, nonatomic) IBOutlet UIButton *dealerModeButton;

@end
