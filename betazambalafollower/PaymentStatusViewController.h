//
//  PaymentStatusViewController.h
//  betazambalafollower
//
//  Created by Zenwise Technologies on 02/01/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import "ViewController.h"

@interface PaymentStatusViewController : ViewController
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *paymentLabel;
@property NSString * txnNo;
@property NSMutableDictionary * responseDictionary;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property NSString * payinCheck;
@property (weak, nonatomic) IBOutlet UIButton *statusDescriptionButton;
@property (weak, nonatomic) IBOutlet UILabel *paymentAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *transactionIDLabel;
@property (weak, nonatomic) IBOutlet UIButton *bottomButton;

@end
