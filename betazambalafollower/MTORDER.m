//
//  MTORDER.m
//  
//
//  Created by zenwise technologies on 27/07/17.
//
//

#import "MTORDER.h"

@implementation MTORDER


- (id)init {
    if (self = [super init]) {
            self.inOrderSeqNo = 0;
            self.stUserID = @"";
            self.inATINOrderID = 0;
            self.inOrderID = 0;
            self.inExchangeClientOrderID = 0;
            self.stExchangeOrderID =@"";
            self.stOrderTime =@"";
            self.stExchangeOrderTime =@"";
            self.stExchange =@"";
            self.stSecurityID =@"";
            self.stSecurityType =@"";
            self.stSymbol =@"";
            self.stExpiryDate =@"";
            self.stOptionType =@"";
            self.dbStrikePrice = 0;
            self.stOrderExpiryDateTime =@"";
            self.stSide =@"";
            self.inQty = 0;
            self.inPendingQty = 0;
            self.inExeQty = 0;
            self.inDiscloseQty = 0;
            self.dbPrice = 0;
            self.dbTriggerPrice = 0;
            self.stClientID =@"";
            self.stMemberID =@"";
            self.stTraderID =@"";
            self.stCTCLID =@"";
            self.stOrderType =@"";
            self.stOrderSituation =@"";
            self.stOrderStatus =@"";
            self.stTimeinForce =@"";
            self.stErrorText =@"";
            self.shErrorCode = 0;
            self.inOrderCount = 0;
            self.stTerminalInfo =@"";
            self.stRefText =@"";

        
    }
    return self;
}



+ (id)Mt2 {
    static MTORDER *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}


@end
