//
//  TradeOrders.m
//  testing
//
//  Created by zenwise technologies on 03/01/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "TradeOrders.h"

@interface TradeOrders ()

@end

@implementation TradeOrders

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.limitView.hidden=YES;
    self.stopLossView.hidden=YES;
   
    [self.marketBtn addTarget:self action:@selector(marketAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.limitBtn addTarget:self action:@selector(limitAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.stopLossBtn addTarget:self action:@selector(stopLossAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.buySellSegment addTarget:self action:@selector(segmentAction) forControlEvents:UIControlEventValueChanged];
    
    self.buySellSegment.tintColor=[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)marketAction
{
    self.limitView.hidden=YES;
    self.stopLossView.hidden=YES;
    
    self.marketBtn.selected=YES;
    self.limitBtn.selected=NO;
    self.stopLossBtn.selected=NO;
    self.marketBtn.backgroundColor=[UIColor colorWithRed:(206/255.0) green:(212/255.0) blue:(215/255.0) alpha:1];
    self.stopLossBtn.backgroundColor=[UIColor clearColor];
    self.limitBtn.backgroundColor=[UIColor clearColor];
}

-(void)limitAction
{
    self.limitView.hidden=NO;
    self.stopLossView.hidden=YES;
    
    self.marketBtn.selected=NO;
    self.limitBtn.selected=YES;
    self.stopLossBtn.selected=NO;
    self.limitBtn.backgroundColor=[UIColor colorWithRed:(206/255.0) green:(212/255.0) blue:(215/255.0) alpha:1];
    self.stopLossBtn.backgroundColor=[UIColor clearColor];
    self.marketBtn.backgroundColor=[UIColor clearColor];
}

-(void)stopLossAction
{
    self.limitView.hidden=YES;
    self.stopLossView.hidden=NO;
    self.marketBtn.selected=NO;
    self.limitBtn.selected=NO;
    self.stopLossBtn.selected=YES;
    self.stopLossBtn.backgroundColor=[UIColor colorWithRed:(206/255.0) green:(212/255.0) blue:(215/255.0) alpha:1];
    self.limitBtn.backgroundColor=[UIColor clearColor];
    self.marketBtn.backgroundColor=[UIColor clearColor];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)segmentAction
{
    if(_buySellSegment.selectedSegmentIndex==0)
    {
        self.buySellSegment.tintColor=[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1];
    }
    
    else if(_buySellSegment.selectedSegmentIndex==1)
    {
        self.buySellSegment.tintColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
    }
    
}

- (IBAction)onSubmitOrderTap:(id)sender {
}
@end
