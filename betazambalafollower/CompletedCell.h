//
//  CompletedCell.h
//  testing
//
//  Created by zenwise technologies on 26/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompletedCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *orderIdLbl;

@property (strong, nonatomic) IBOutlet UILabel *timeLbl;


@property (strong, nonatomic) IBOutlet UILabel *tradingSymLbl;

@property (strong, nonatomic) IBOutlet UILabel *transactionLbl;
@property (strong, nonatomic) IBOutlet UILabel *priceLbl;

@property (strong, nonatomic) IBOutlet UILabel *exchangeLbl;

@property (strong, nonatomic) IBOutlet UILabel *qtyLbl;

@property (strong, nonatomic) IBOutlet UILabel *orderTypeLbl;
@property (strong, nonatomic) IBOutlet UILabel *fillQtyLbl;
@property (strong, nonatomic) IBOutlet UILabel *pendingQtyLbl;
@property (weak, nonatomic) IBOutlet UILabel *productTypeLbl;

@property (weak, nonatomic) IBOutlet UIImageView *expandImgView;
@end
