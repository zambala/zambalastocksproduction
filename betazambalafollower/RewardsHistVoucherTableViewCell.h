//
//  RewardsHistVoucherTableViewCell.h
//  betazambalafollower
//
//  Created by Zenwise Technologies Private Limited on 23/08/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RewardsHistVoucherTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *voucherImageView;
@property (weak, nonatomic) IBOutlet UILabel *voucherNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *voucherValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *referenceIDLabel;
@property (weak, nonatomic) IBOutlet UIButton *showVoucherButton;
@property (weak, nonatomic) IBOutlet UILabel *redeemDateLabel;
@property (weak, nonatomic) IBOutlet UIView *backview;

@end
