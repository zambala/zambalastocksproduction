//
//  KnowledgeTextImageViewController.m
//  KnowledgeSection
//
//  Created by Zenwise Technologies on 13/06/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "KnowledgeTextImageViewController.h"
#import "AppDelegate.h"

@interface KnowledgeTextImageViewController ()<UIWebViewDelegate>
{
    AppDelegate * delegate1;
}

@end

@implementation KnowledgeTextImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.backButton addTarget:self action:@selector(onBackButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.webView.delegate=self;
   self.webView.scrollView.scrollEnabled = NO;
    NSString * shortStr=[NSString stringWithFormat:@"%@",[delegate1.resourceDetails objectForKey:@"shortdescription"]];
    NSString * urlStr=[NSString stringWithFormat:@"%@",[delegate1.resourceDetails objectForKey:@"url"]];
    if([urlStr isEqualToString:@"null"])
    {
        
    }
    else
    {
        NSURL *url;
        @try {
            url =  [NSURL URLWithString:urlStr];
        } @catch (NSException *exception) {
            
            
            
        } @finally {
            
        }
        
        
        
        
        
        
        NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if (data) {
                UIImage *image = [UIImage imageWithData:data];
                if (image) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                       
                            self.bgImgView.image = image;
                    });
                }
            }
        }];
        [task resume];
    }
    if([shortStr isEqualToString:@"<null>"])
    {
        self.shortDescLbl.text=[NSString stringWithFormat:@"%@",[delegate1.resourceDetails objectForKey:@"heading"]];
    }
    else
    {
        self.shortDescLbl.text=shortStr;
    }
    
    NSString * htmlStr=[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[delegate1.resourceDetails objectForKey:@"longdescription"]]];
    
    [self.webView loadHTMLString:htmlStr baseURL:nil];
    // Do any additional setup after loading the view.
}
- (void)webViewDidFinishLoad:(UIWebView *)aWebView
{
//     NSString *string = [_webView stringByEvaluatingJavaScriptFromString:@"document.getElementById(\"body\").offsetHeight;"];
//    NSString *string =[NSString stringWithFormat:@"document.getElementById(\"%@\").offsetHeight;",[delegate1.resourceDetails objectForKey:@"longdescription"]];
//    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
//    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
//
//    float value = [numberFormatter numberFromString:string].floatValue;
    
    CGFloat height = aWebView.scrollView.contentSize.height + 245;
    CGRect frame = [_webView frame];
    frame.size.height = height;
    [_webView setFrame:frame];
//    CGRect scrollFrame = CGRectMake(0,0,self.view.frame.size.width,height);
//    self.scrollView.frame = scrollFrame;
    self.scrollView.contentSize=CGSizeMake(0, height);
    //    self.scrollView.size.content = height;
//    [aWebView setFrame:frame];
}


-(void)onBackButtonTap
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
