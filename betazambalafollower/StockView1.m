//
//  StockView1.m
//  testing
//
//  Created by zenwise technologies on 01/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "StockView1.h"
#import "HMSegmentedControl.h"
#import "AppDelegate.h"
#import "PSWebSocket.h"
#import "MarketWatchNews.h"
#import "OPTCell.h"
#import "StockOrderView.h"
#import "CallPutTrade.h"
#import "NewProfile.h"
#import "FeedListDetail.h"
#import "TagEncode.h"
#import "MT.h"
#import<malloc/malloc.h>
#import <Mixpanel/Mixpanel.h>
@import SocketIO;


@interface StockView1 ()<PSWebSocketDelegate,NSStreamDelegate,SocketEngineSpec,SocketManagerSpec,SocketEngineClient>
{
    
    AppDelegate * delegate2;
    HMSegmentedControl * segmentedControl;
    NSDictionary * dict1;
    NSString * string1;
    NSMutableArray * LtpArray;
    NSMutableArray * changePerArray;
    NSMutableArray * shareListArray;
    NSMutableArray * shareListArrayCopy;
    NSMutableArray * instrumentTokens;
    NSMutableArray * filteredArray;
    
    NSMutableArray *allKeysList;
    NSMutableArray * symbol;
    NSMutableArray * symbol1;
    NSMutableArray * symbol2;
    NSMutableArray * symbol3;
    NSMutableArray * instrumentToken;
    NSString * localExchange;
    NSString * localFutExchange;
    NSMutableArray * expiry;
    NSMutableArray * strike;
    NSMutableArray * title;
    NSString * titleStr;
    NSMutableArray * sortedArray;
    NSMutableArray *  newSortArray;
    NSMutableArray * newInstrumentToken;
    NSMutableArray * testArray;
    NSMutableArray * CEArray,*PEArray;
    NSMutableDictionary * testDictionary;
    NSArray * newSortedStrike;
    BOOL BIDCECheck;
    BOOL BIDPECheck;
    BOOL ASKCECheck;
    BOOL ASKPECheck;
    MT * sharedManager;
    bool exchangeTokenCheck;;
    TagEncode * tag;
    BOOL optionExchangetokenCheck;
    NSTimer * timer;
    NSMutableArray * upStoxEQSymbolArray;
    NSMutableArray * upStoxFUTSymbolArray;
    NSMutableArray * upStoxEQSymbolArrayToken;
    NSMutableArray * upStoxFUTSymbolArrayToken;
    NSUInteger location;
    NSMutableArray *array_isSelected;
    NSString * expiryDateFilter;
    NSString * localStrikesCheck;
    NSUInteger  objectAtIndex;
    BOOL opt;
    NSMutableDictionary * companyNews;
    
    NSMutableArray * exchangeTokenArray;
    
    NSMutableArray *array_SelectedIndexPostion;
    
    BOOL check;
    BOOL optCheck;
    NSMutableArray * globalAllKeys;
    NSMutableDictionary *callPutDictionary;
    NSMutableArray * CEDetailArray;
    NSMutableArray * PEDetailArray;
    NSMutableArray * sortedStrike;
    float sortValue;
    float sortValue1;
    BOOL FUTCheck;
    bool temp;
    int value;
    int packetsLength;
    int packetsNumber;
//    int instrumentToken;
    float lastPriceFlaotValue;
    float closePriceFlaotValue;
    NSString * bankNiftyCheck;
    
    float changeFloatValue;
    
    float changeFloatPercentageValue;
    
    float BIDFloatValue ;
     float marketDepthValueBIDPrice1 ;
    BOOL nseCheck;
    BOOL nfoCheck;

    
    float ASKFloatValue ;
    
    BOOL dismissCheck;
    
    float openFloatValue ;
     float marketDepthValueASKPrice1 ;
    
    
    
    
    float highFloatValue ;
    
    
    float lowFloatValue  ;
    NSString * fincode;
    
    
    NSString * exchange;
    NSString * FUTexchange;
    
    
    NSMutableArray * newCEDetailArray;
    NSMutableArray * newPEDetailArray;
    NSMutableArray * bankNiftyOptionArray;
    
    
    


}
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *view1_Height;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *view2_Height;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *view3_Height;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *view4_Height;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *contentHeight;

@end

@implementation StockView1
@synthesize instrumentTokensDict;

- (void)viewDidLoad {
    @try
    {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //segmented controll//
    delegate2=(AppDelegate *)[[UIApplication sharedApplication]delegate];
        self.wisdomProfileImg.layer.borderWidth = 0.5f;
        self.wisdomProfileImg.layer.borderColor = [[UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0] CGColor];
        Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
        [mixpanelMini track:@"stock_details_page"];
    if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
//        [self.socket close];
        [self  websocket];
    }
    
    else
    {
        
    
    }
//    delegate2.marketWatch1=[[NSMutableArray alloc]init];
        bankNiftyCheck = @"FUT";
    sharedManager=[[MT alloc]init];
    array_isSelected =[[NSMutableArray alloc]init];
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"DETAILS",@"NEWS"]];
    
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    //    segmentedControl.verticalDividerEnabled = YES;
    //    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(251/255.0) green:(196/255.0) blue:(12/255.0) alpha:1];
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
    
    
    
   
    
    
    
    delegate2.btnCountArray=[[NSMutableArray alloc]init];
    self.localWisdomCheck = delegate2.depth;

    
    if([delegate2.depth isEqualToString:@"MARKETWATCH"])
    {
        self.wisdomViewHgt.constant=0;
        segmentedControl.frame = CGRectMake(0,0, self.view.frame.size.width, 40);
        self.view1Top.constant=0;
        
//        [self.view addSubview:segmentedControl];
        self.wisdomProfileImg.hidden=YES;
        self.wisdomProfileName.hidden=YES;
        self.wisdomMsg.hidden=YES;
        self.wisdomDate.hidden=YES;
        self.wisdomLTP.hidden=YES;
        self.wisdomCHG.hidden=YES;
        self.wisdomActedBy.hidden=YES;
        self.wisdomShares.hidden=YES;
        self.wisdomLTPValue.hidden=YES;
        self.wisdomChgValue.hidden=YES;
        self.wisdomActedValue.hidden=YES;
        self.wisdomShareValue.hidden=YES;
        self.wisdomOLbl.hidden=YES;
        self.wisdomStLbl.hidden=YES;
        self.wisdomLtpValue1.hidden=YES;
    }
    
    else if([delegate2.depth isEqualToString:@"WISDOM"])
    {
        
         self.contentHeight.constant=self.view5.frame.origin.y+self.view5Hgt.constant-40;
        
        self.wisdomProfileImg.layer.cornerRadius=self.wisdomProfileImg.frame.size.width / 2;
        self.wisdomProfileImg.clipsToBounds = YES;
        
        self.wisdomOLbl.layer.cornerRadius=self.wisdomOLbl.frame.size.width / 2;
        self.wisdomOLbl.clipsToBounds = YES;
        
        self.wisdomStLbl.layer.cornerRadius=self.wisdomStLbl.frame.size.width / 2;
        self.wisdomStLbl.clipsToBounds = YES;
        
        
        self.wisdomViewHgt.constant=130;
//        segmentedControl.frame = CGRectMake(0,190, self.view.frame.size.width, 40);
         segmentedControl.frame = CGRectMake(0,0, self.view.frame.size.width, 40);
        self.view1Top.constant=20;
//        [self.view addSubview:segmentedControl];
        self.wisdomProfileImg.hidden=NO;
        self.wisdomProfileName.hidden=NO;
        self.wisdomLtpValue1.hidden=NO;
        self.wisdomProfileName.text=[delegate2.leaderAdviceDetails objectAtIndex:0];
        self.wisdomMsg.hidden=NO;
        self.wisdomDate.hidden=NO;
        
        NSString * dateStr=[delegate2.leaderAdviceDetails objectAtIndex:1];
        
      
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        NSDate *date = [dateFormatter dateFromString:dateStr]; // create date from string
        
        // change to a readable time format and change to local time zone
        [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        NSString *finalDate = [dateFormatter stringFromDate:date];
        
        
        
        
        
    
        
        
       
        
        self.wisdomDate.text=finalDate;
        self.wisdomLTP.hidden=NO;
        self.wisdomLtpValue1.text=[delegate2.leaderAdviceDetails objectAtIndex:2];
        self.wisdomCHG.hidden=NO;
        self.wisdomChgValue.text=[delegate2.leaderAdviceDetails objectAtIndex:3];
//        self.wisdomMsg.text=[delegate2.leaderAdviceDetails objectAtIndex:4];
        self.wisdomActedBy.hidden=NO;
        self.wisdomShares.hidden=NO;
        self.wisdomLTPValue.hidden=NO;
        self.wisdomChgValue.hidden=NO;
        self.wisdomActedValue.hidden=NO;
        self.wisdomShareValue.hidden=NO;
        self.wisdomOLbl.hidden=NO;
        self.wisdomStLbl.hidden=NO;
        @try {
        NSString * logo=[delegate2.leaderAdviceDetails objectAtIndex:8];
        
        if([logo isEqualToString:@"no"])
        {
            self.wisdomProfileImg.image = nil;
        }
        
        else
        {
            
            NSURL *url = [NSURL URLWithString:logo];
            
            NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (data) {
                    UIImage *image = [UIImage imageWithData:data];
                    if (image) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            self.wisdomProfileImg.image = image;
                            
                        });
                    }
                }
            }];
            [task resume];
            
        }
        }
            @catch (NSException * e) {
                //NSLog(@"Exception: %@", e);
            }
            @finally {
                //NSLog(@"finally");
            }
            
            
        
        
        @try {
            
            NSString * message=[NSString stringWithFormat:@"%@",[delegate2.leaderAdviceDetails objectAtIndex:4]];
            if([message containsString:@"@"])
            {
                
                 self.detailMsg.text=[message uppercaseString];
            }
            
            else
            {
            NSArray * mainArray=[[delegate2.leaderAdviceDetails objectAtIndex:4] componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            
            
            //NSLog(@"array  %@",[mainArray objectAtIndex:0]);
            
            //NSLog(@"array  %@",[mainArray objectAtIndex:1]);
            
            NSString * buySell=[NSString stringWithFormat:@"%@",[mainArray objectAtIndex:0]];
            
            
            int buysell1=[[delegate2.leaderAdviceDetails objectAtIndex:7] intValue];
            
            if(buysell1==1)
            {
                buySell=@"BUY";
            }
            else if(buysell1==2)
            {
                buySell=@"SELL";
            }
            
            NSString * string=[NSString stringWithFormat:@"%@",[mainArray objectAtIndex:1]];
            
            
            
            
            
            NSArray * companyArray=[string componentsSeparatedByString:@";"];
            
            
            
            //NSLog(@" array1 %@",companyArray);
            
            
            
            //               for (int i=1; i<[companyArray count];i++) {
            
            
            
            //                   NSString * string11=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:0]];
            //
            //
            //
            //                   NSArray * array2=[string11 componentsSeparatedByString:@";"];
            //
            //                   //NSLog(@"%@",array2);
            
            NSString * companyName=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:0]];
            
            
            NSString * EP1=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:1]];
            NSString * TP=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:2]];
            NSString * SL;
            
             
                if([message containsString:@"SL"])
                {
                   SL=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:3]];
                    
                }
                else
                {
                    SL=@"";
                  
                }
            
            
            
            
            
            
            
            
            NSString * sell = [NSString stringWithFormat:@"<html><body><center><div style='display:inline-block;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'>%@ <b>&nbsp;%@</b></div></center> </body></html>",buySell,companyName];
            //  NSString * muthootfin =[NSString stringWithFormat:@"<html><body><center><b><div style='display:inline-block;font-size:12.5px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></center></body></html>",companyName];
            
            
            
            
            NSString * msg=[NSString stringWithFormat:@"%@",sell];
            
            NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[msg dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
            
            
            
            self.wisdomMsg.attributedText=attrStr;
            
            NSString * EPFinal =[EP1 stringByReplacingOccurrencesOfString:@"EP=" withString:@" "];
            NSString * TPFinal =[TP stringByReplacingOccurrencesOfString:@"TP=" withString:@" "];
            NSString * SLFinal =[SL stringByReplacingOccurrencesOfString:@"SL=" withString:@" "];
            
                NSString * htmlString;
                if([message containsString:@"SL"])
                {
                      htmlString = [NSString stringWithFormat:@"<html><body><center><div style='display:inline-block;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'>@&nbsp;EP<b>%@</b>&nbsp;TP<b>%@</b>&nbsp;SL<b>%@</b></div><center></html></body>",EPFinal,TPFinal,SLFinal];
                    
                }
                else
                {
                   htmlString = [NSString stringWithFormat:@"<html><body><center><div style='display:inline-block;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'>@&nbsp;EP<b>%@</b>&nbsp;TP<b>%@</b>&nbsp</div><center></html></body>",EPFinal,TPFinal];
                    
                }
            
            //        NSString * ep = @"<html><body><center><div style='display:inline-block;text-align:center;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'>@ EP </div><center></html></body>";
            //        NSString * value1 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:center;font-size:12.5px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",EPFinal];
            //        NSString * tp = @"<html><body><div style='display:inline-block;text-align:center;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'>   TP</div></html></body>";
            //        NSString * value2 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:center;font-size:12.5px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",TPFinal];
            //        NSString * sl = @"<html><body><div style='display:inline-block;text-align:center;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'>   SL</div></html></body>";
            //        NSString * value3 =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:center;font-size:12.5px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",SLFinal] ;
            
            
            //NSString * msg1=[NSString stringWithFormat:@"%@%@  %@%@  %@%@",ep,value1,tp,value2,sl,value3];
            
            
            NSAttributedString * attrStr1 = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
            
            self.detailMsg.attributedText=attrStr1;
            
            
            }
            
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
            
        
        
        
    }
    

    //NSLog(@"%@",delegate2.symbolDepthStr);
    self.companyTxt1.text=delegate2.symbolDepthStr;
    self.companyTxt2.text=delegate2.symbolDepthStr;
    self.optionCompanyName.text=delegate2.symbolDepthStr;
    FUTCheck=true;
    check=true;
    BIDPECheck=true;
    BIDCECheck=true;
    ASKCECheck=true;
    ASKPECheck=true;
    
    
    
    self.view1.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.view1.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.view1.layer.shadowOpacity = 1.0f;
    self.view1.layer.shadowRadius = 1.0f;
    self.view1.layer.cornerRadius=2.1f;
    self.view1.layer.masksToBounds = NO;
    
    
    self.view2.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.view2.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.view2.layer.shadowOpacity = 1.0f;
    self.view2.layer.shadowRadius = 1.0f;
    self.view2.layer.cornerRadius=2.1f;
    self.view2.layer.masksToBounds = NO;
    
    
    
    self.view3.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.view3.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.view3.layer.shadowOpacity = 1.0f;
    self.view3.layer.shadowRadius = 1.0f;
    self.view3.layer.cornerRadius=2.1f;
    self.view3.layer.masksToBounds = NO;
    
    self.view4.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.view4.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.view4.layer.shadowOpacity = 1.0f;
    self.view4.layer.shadowRadius = 1.0f;
    self.view4.layer.cornerRadius=2.1f;
    self.view4.layer.masksToBounds = NO;
    
    self.cashBuyButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.cashBuyButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.cashBuyButton.layer.shadowOpacity = 1.0f;
    self.cashBuyButton.layer.shadowRadius = 1.0f;
    self.cashBuyButton.layer.cornerRadius=2.1f;
    self.cashBuyButton.layer.masksToBounds = NO;
    
    
    self.cashSellButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.cashSellButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.cashSellButton.layer.shadowOpacity = 1.0f;
    self.cashSellButton.layer.shadowRadius = 1.0f;
    self.cashSellButton.layer.cornerRadius=2.1f;
    self.cashSellButton.layer.masksToBounds = NO;
    
    
    self.futBuyButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.futBuyButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.futBuyButton.layer.shadowOpacity = 1.0f;
    self.futBuyButton.layer.shadowRadius = 1.0f;
    self.futBuyButton.layer.cornerRadius=2.1f;
    self.futBuyButton.layer.masksToBounds = NO;
    
    
    self.futSell.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.futSell.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.futSell.layer.shadowOpacity = 1.0f;
    self.futSell.layer.shadowRadius = 1.0f;
    self.futSell.layer.cornerRadius=2.1f;
    self.futSell.layer.masksToBounds = NO;
    
    
    //instrument token//
    instrumentToken=[[NSMutableArray alloc]init];
    exchangeTokenArray=[[NSMutableArray alloc]init];
    expiry=[[NSMutableArray alloc]init];
    title=[[NSMutableArray alloc]init];
     strike=[[NSMutableArray alloc]init];
    instrumentTokensDict=[[NSMutableDictionary alloc]init];
    array_SelectedIndexPostion = [[NSMutableArray alloc]init];
    self.pickerView.hidden=YES;
     self.doneView.hidden=YES;
    self.view5.hidden=YES;
    self.view3.hidden=YES;
    self.view4.hidden=YES;
    self.view3_Height.constant = 0;
    
    self.view5Hgt.constant = 0;
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    
    
    _view1_Height.constant=0;
    _view2_Height.constant=0;
    self.view1.hidden=YES;
    self.view2.hidden=YES;
    

    
   
    
//     instrumentToken=[delegate2.instrumentDepthStr intValue];
    
    //DEPTH//
    
   [self uiMethod];
//    allKeysList=[[NSMutableArray alloc]init];
    
    if([delegate2.exchaneLblStr containsString:@"CDS"])
    {
        self.futureSegment.text=@"CDS-FUT";
        self.optionSegment.text=@"CDS-OPT";
                [self faoMethod];
        
        
    }
    
    else if([delegate2.symbolDepthStr containsString:@"NIFTY"]&&([delegate2.exchaneLblStr containsString:@"NFO-OPT"]||[delegate2.exchaneLblStr containsString:@"NFO-FUT"]))
    {
        self.futureSegment.text=@"NFO-FUT";
        self.optionSegment.text=@"NFO-OPT";
        //mt//
        exchange=@"NSEFO";
        FUTexchange=@"NSEFO";
        [self faoMethod];
    }
    
    else if([delegate2.symbolDepthStr containsString:@"NIFTY"]&&([delegate2.exchaneLblStr containsString:@"BFO-FUT"]||[delegate2.exchaneLblStr containsString:@"BFO-OPT"]))
    {
        self.futureSegment.text=@"BFO-FUT";
        self.optionSegment.text=@"BFO-OPT";
       //mt//
        exchange=@"BSEFO";
        FUTexchange=@"BSEFO";
        [self faoMethod];
        
    }
    
    else if([delegate2.exchaneLblStr containsString:@"MCX"])
    {
        self.futureSegment.text=@"MCX";
        self.optionSegment.text=@"MCX";
        //mt//
//        exchange=@"BSEFO";
//        FUTexchange=@"BSEFO";
        [self faoMethod];
        
    }
    else
    {
        _view1_Height.constant=170;
        _view2_Height.constant=135;
        self.view1.hidden=NO;
        self.view2.hidden=NO;
        
        @try {
            NSDictionary *headers = @{ @"content-type": @"application/x-www-form-urlencoded",
                                       @"cache-control": @"no-cache",
                                       @"authtoken":delegate2.zenwiseToken,
                                          @"deviceid":delegate2.currentDeviceId
                                       };
            
            if([delegate2.exchaneLblStr containsString:@"NSE"]||[delegate2.exchaneLblStr containsString:@"FUT"]||[delegate2.exchaneLblStr containsString:@"OPT"])
            {
                localExchange=@"NSE";
                self.cashSegment.text=@"NSE CASH";
                self.futureSegment.text=@"NFO-FUT";
                self.optionSegment.text=@"NFO-OPT";
            }
            
            else if([delegate2.exchaneLblStr containsString:@"BSE"]||[delegate2.exchaneLblStr containsString:@"FUT"]||[delegate2.exchaneLblStr containsString:@"OPT"])
            {
                localExchange=@"BSE";
                self.cashSegment.text=@"BSE CASH";
                self.futureSegment.text=@"BFO-FUT";
                self.optionSegment.text=@"BFO-OPT";
            }
            
            
            
            NSString * depthStr=[NSString stringWithFormat:@"%@stock/%@?exchange=%@",delegate2.baseUrl,delegate2.symbolDepthStr,localExchange];
            
            
            
            //NSLog(@"%@",depthStr);
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:depthStr]
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:10.0];
            [request setHTTPMethod:@"GET"];
            [request setAllHTTPHeaderFields:headers];
            
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            if (error) {
                                                                //NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                //NSLog(@"%@", httpResponse);
                                                                if([httpResponse statusCode]==403)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                    
                                                                }
                                                                else if([httpResponse statusCode]==401)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    
                                                                upStoxEQSymbolArray=[[NSMutableArray alloc]init];
                                                                upStoxEQSymbolArrayToken=[[NSMutableArray alloc]init];
                                                                
                                                                symbol = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil]mutableCopy];
                                                                [instrumentToken removeAllObjects];
                                                                [exchangeTokenArray removeAllObjects];
                                                                //NSLog(@"%@",symbol);
                                                                if(symbol.count>0)
                                                                {
                                                                    NSString * string=[symbol valueForKey:@"instrument_token"];
                                                                    [upStoxEQSymbolArrayToken addObject:string];
                                                                     fincode=[symbol valueForKey:@"fincode"];
                                                                    
                                                                    //NSLog(@"%@",string);
                                                                    
                                                                    
                                                                    int myInt = [string intValue];
                                                                    
                                                                    //NSLog(@"token%i",myInt);
                                                                    
                                                                    [instrumentToken addObject:[NSNumber numberWithInt:myInt]];
                                                                    
                                                                     NSString * exchangeTokenString=[symbol valueForKey:@"exchange_token"];
                                                                    
                                                                    int myInt1 = [exchangeTokenString intValue];
                                                                    
                                                                    //NSLog(@"token%i",myInt1);
                                                                    
                                                                    [exchangeTokenArray addObject:exchangeTokenString];
                                                                    
                                                                    //NSLog(@"%@",instrumentToken);
                                                                }
                                                                
                                                                @try
                                                                {
                                                                [upStoxEQSymbolArray addObject:[symbol valueForKey:@"symbol"]];
                                                                }
                                                                @catch (NSException * e) {
                                                                    //NSLog(@"Exception: %@", e);
                                                                }
                                                                @finally {
                                                                    //NSLog(@"finally");
                                                                }
                                                                }
                                                                
                                                                
                                                            }
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                
                                                                [self faoMethod];
                                                                
                                                            });
                                                            
                                                            
                                                            
                                                            
                                                        }];
            [dataTask resume];
            

            
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
       }
    
     if(delegate2.optionOrderArray.count>0)
     {[delegate2.optionOrderArray removeAllObjects];
         [delegate2.optionLimitPriceArray removeAllObjects];}
    if(delegate2.optionOrderTransactionArray.count>0){[delegate2.optionOrderTransactionArray removeAllObjects];}
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
   }

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
   
//    [self.depthTableView reloadData];
}
-(void)viewWillDisappear:(BOOL)animated
{
    
    
    [self.socketio disconnect];
    delegate2.depthMt=false;
//   [[NSNotificationCenter defaultCenter] postNotificationName:@"broad" object:nil];
//     delegate2.searchToWatch=true;
}

-(void)hideMethod
{
    [self.activityIndicator stopAnimating];
    self.activityIndicator.hidden=YES;
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    self.socket=nil;
   
    if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
        dismissCheck=true;
        [self unSubMethod];
        [self unSubMethod1];
    }
    else  if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
    {
        
    }
    
    else
        
    {
        [self.socketio disconnect];
        self.socketio=nil;
    }
}
-(void)faoMethod
{
    
    @try {
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"cache-control": @"no-cache",
                                   @"authtoken":delegate2.zenwiseToken,
                                      @"deviceid":delegate2.currentDeviceId
                                   };
        
        if([delegate2.exchaneLblStr containsString:@"NSE"]||[delegate2.exchaneLblStr containsString:@"NFO-FUT"]||[delegate2.exchaneLblStr containsString:@"NFO-OPT"])
        {
            localFutExchange=@"NFO-FUT";
        }
        
        else if([delegate2.exchaneLblStr containsString:@"BSE"]||[delegate2.exchaneLblStr containsString:@"BFO-FUT"]||[delegate2.exchaneLblStr containsString:@"BFO-OPT"])
        {
            localFutExchange=@"BFO-FUT";
        }
        
       else if([delegate2.exchaneLblStr containsString:@"CDS"])
        {
            
            
            localFutExchange=@"CDS-FUT";
        }
        
       else if([delegate2.exchaneLblStr containsString:@"MCX"])
       {
           
           
           localFutExchange=@"MCX";
       }
        
        NSString * depthStr=[NSString stringWithFormat:@"%@stock/fao/%@?segment=%@&limit=150&orderby=ASC",delegate2.baseUrl,delegate2.symbolDepthStr,localFutExchange];
        
        //NSLog(@"%@",depthStr);
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:depthStr]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            if([httpResponse statusCode]==403)
                                                            {
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                
                                                            }
                                                            else if([httpResponse statusCode]==401)
                                                            {
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                
                                                            }
                                                            else
                                                            {
                                                            if([bankNiftyCheck isEqualToString:@"FUT"])
                                                            {
                                                            [expiry removeAllObjects];
                                                            }
                                                            [delegate2.depthLotSize removeAllObjects];
                                                            upStoxFUTSymbolArray=[[NSMutableArray alloc]init];
                                                            upStoxFUTSymbolArrayToken=[[NSMutableArray alloc]init];
                                                            symbol1 = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil]mutableCopy];
                                                            
                                                            //NSLog(@"%@",symbol1);
                                                            if(symbol1.count>0)
                                                            {
                                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                                 self.view3.hidden=NO;
                                                                self.view4.hidden=NO;
                                                                self.view4_Height.constant = 170;
                                                                
                                                                 });
                                                                
                                                                if([delegate2.exchaneLblStr containsString:@"MCX"])
                                                                {
                                                             if(self.view.frame.size.width<375)
                                                             {
                                                                 
                                                             }
                                                                    
                                                            else
                                                            {
                                                                       self.scrollView.scrollEnabled=false;
                                                            }
                                                                    
                                                                   
                                                                        
                                                                    self.view5.hidden=YES;
                                                                     self.view5Hgt.constant = 0;
                                                                    
                                                                    self.upDownButton.enabled=NO;
                                                                    
                                                                }
                                                                
                                                                else
                                                                {
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                    self.view5.hidden=NO;
                                                                     self.view5Hgt.constant = 500;
                                                                    self.upDownButton.enabled=YES;
                                                                    });
                                                                }
                                                               
                                                                self.view3_Height.constant = 170;
                                                                self.upDownButton.enabled=YES;
                                                                
                                                               
                                                                
                                                               
                                                                for(int i=0;i<symbol1.count;i++)
                                                                {
                               NSString * exchangeTokenString=[[symbol1 objectAtIndex:i]valueForKey:@"exchange_token"];
                                    if([delegate2.instrumentDepthStr isEqualToString:exchangeTokenString])
                                                      {
                                                                    
                                                      }
                                                }
                                                                
                                                                NSString * exchangeTokenString=[[symbol1 objectAtIndex:0]valueForKey:@"exchange_token"];
                                                                
                                                                //NSLog(@"%@",exchangeTokenString);
                                                                
                                                                
                                                                
                                                                for(int i=0;i<symbol1.count;i++)
                                                                {
                                                                    
                                                                    NSString * string=[[symbol1 objectAtIndex:i]valueForKey:@"instrument_token"];
                                                                    NSString * exchangeTokenString=[[symbol1 objectAtIndex:i]valueForKey:@"exchange_token"];
                                                                    
                                                                    
                                                                    NSString *myString = [[symbol1 objectAtIndex:i]objectForKey:@"expiryseries"];
                                                                    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                                                                    dateFormatter.dateFormat = @"yyyy-MM-dd";
                                                                    NSDate *yourDate = [dateFormatter dateFromString:myString];
                                                                    dateFormatter.dateFormat = @"dd MMM yyyy";
                                                                    //NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
                                                                    
                                                                    NSString * finalDate=[[dateFormatter stringFromDate:yourDate] uppercaseString];
                                                                    
                                                                    
                                                        
                                                                    [expiry addObject:finalDate];
                                                                    
                                                                    
                                                                   
 if(([delegate2.exchaneLblStr containsString:@"CDS-FUT"]||[delegate2.exchaneLblStr containsString:@"FUT"]||[delegate2.exchaneLblStr containsString:@"MCX"]) &&(delegate2.expirySeriesValue.length>0))
                                                                 
                                                             {
                                                            
                                                                    if([delegate2.instrumentDepthStr intValue]==[exchangeTokenString intValue]||[delegate2.instrumentDepthStr intValue]==[string intValue])
                                                                    {
                                                                        
                                                                      
                                                                    expiryDateFilter=[[symbol1 objectAtIndex:i]objectForKey:@"expiryseries"];
                                                                        
                                                                        [upStoxFUTSymbolArrayToken addObject:string];
                                                                        NSString * symbol = [NSString stringWithFormat:@"%@",[[symbol1 objectAtIndex:i]objectForKey:@"securitydesc"]];
                                                                        if([delegate2.brokerNameStr isEqualToString:@"Upstox"]&&[symbol containsString:@"BANKNIFTY"])
                                                                        {
                                                                           [upStoxFUTSymbolArray addObject:[[symbol1 objectAtIndex:i]objectForKey:@"securitydescupstox"]];
                                                                        }
                                                                        else
                                                                        {
                                                                           [upStoxFUTSymbolArray addObject:[[symbol1 objectAtIndex:i]objectForKey:@"securitydesc"]];
                                                                        }
                                                                        //NSLog(@"%@",string);
                                                                        
                                                                        
                                                                        int myInt = [string intValue];
                                                                        
                                                                        //NSLog(@"token%i",myInt);
                                                                        
                                                                          int myInt2 = [exchangeTokenString intValue];
                                                                        
                                                                        //NSLog(@"token%i",myInt2);
                                                                        
                                                                        if([expiryDateFilter isEqualToString:delegate2.expirySeriesValue])
                                                                        {
                                                                        
                                                                        [instrumentToken addObject:[NSNumber numberWithInt:myInt]];
                                                                        
                                                                        //NSLog(@"%@",instrumentToken);
                                                                            
                                                                            [exchangeTokenArray addObject:exchangeTokenString];
                                                                        }
                                                                        
                                                                      
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        NSString * lot=[[symbol1 objectAtIndex:0]valueForKey:@"lotsize"];
                                                                        
                                                                        [delegate2.depthLotSize addObject:lot];
                                                                        
                                                                        
                                                                        location=i;
                                                                        
                                                                        
                                                                        
                                                        }
                                                                    
                                                                    
                                                                    
                                                                    
                                                             }
                                                                    
                                else if([delegate2.exchaneLblStr containsString:@"OPT"]&&delegate2.expirySeriesValue.length>0)
                                                {
                                                    NSString * series=[NSString stringWithFormat:@"%@",[[symbol1 objectAtIndex:i]objectForKey:@"expiryseries"]];
                                                    if([self.companyTxt1.text containsString:@"BANKNIFTY"])
                                                    {
                                                        expiryDateFilter=[[symbol1 objectAtIndex:0]objectForKey:@"expiryseries"];
                                                        
                                                        
                                                        //NSLog(@"%@",string);
                                                        
                                                        
                                                        int myInt = [string intValue];
                                                        
                                                        int myInt2 = [exchangeTokenString intValue];
                                                        
                                                        //NSLog(@"token%i",myInt2);
                                                        
                                                        
                                                        //NSLog(@"token%i",myInt);
                                                        
                                                        
                                                        
                                                        if(i==0)
                                                        {
                                                            
                                                            [instrumentToken addObject:[NSNumber numberWithInt:myInt]];
                                                            
                                                            //NSLog(@"%@",instrumentToken);
                                                            
                                                            [exchangeTokenArray addObject:exchangeTokenString];
                                                            [upStoxFUTSymbolArrayToken addObject:string];
                                                            
                                                            NSString * symbol = [NSString stringWithFormat:@"%@",[[symbol1 objectAtIndex:i]objectForKey:@"securitydesc"]];
                                                            if([delegate2.brokerNameStr isEqualToString:@"Upstox"]&&[symbol containsString:@"BANKNIFTY"])
                                                            {
                                                                [upStoxFUTSymbolArray addObject:[[symbol1 objectAtIndex:i]objectForKey:@"securitydescupstox"]];
                                                            }
                                                            else
                                                            {
                                                                [upStoxFUTSymbolArray addObject:[[symbol1 objectAtIndex:i]objectForKey:@"securitydesc"]];
                                                            }
                                                            
                                                            
                                                        }
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        NSString * lot=[[symbol1 objectAtIndex:0]valueForKey:@"lotsize"];
                                                        
                                                        [delegate2.depthLotSize addObject:lot];
                                                        
                                                        
                                                        location=0;
                                                        
                                                    }
                                                    else
                                                    {
                                                    if([series isEqualToString:delegate2.expirySeriesValue])
                                                    {
                                                        
                                                        expiryDateFilter=[[symbol1 objectAtIndex:i]objectForKey:@"expiryseries"];
                                                        [upStoxFUTSymbolArrayToken addObject:string];
                                                        
                                                        NSString * symbol = [NSString stringWithFormat:@"%@",[[symbol1 objectAtIndex:i]objectForKey:@"securitydesc"]];
                                                        if([delegate2.brokerNameStr isEqualToString:@"Upstox"]&&[symbol containsString:@"BANKNIFTY"])
                                                        {
                                                            [upStoxFUTSymbolArray addObject:[[symbol1 objectAtIndex:i]objectForKey:@"securitydescupstox"]];
                                                        }
                                                        else
                                                        {
                                                            [upStoxFUTSymbolArray addObject:[[symbol1 objectAtIndex:i]objectForKey:@"securitydesc"]];
                                                        }
                                                        
                                                        //NSLog(@"%@",string);
                                                        
                                                        
                                                        int myInt = [string intValue];
                                                        
                                                        //NSLog(@"token%i",myInt);
                                                        
                                                        [instrumentToken addObject:[NSNumber numberWithInt:myInt]];
                                                        
                                                        //NSLog(@"%@",instrumentToken);
                                                        
                                                        
                                                        int myInt2 = [exchangeTokenString intValue];
                                                        
                                                        //NSLog(@"token%i",myInt2);
                                                        
                                                        [exchangeTokenArray addObject:exchangeTokenString];
                                                        
                                                        NSString * lot=[[symbol1 objectAtIndex:0]valueForKey:@"lotsize"];
                                                        
                                                        [delegate2.depthLotSize addObject:lot];
                                                        
                                                        
                                                        location=i;
                                                        
                                                        
                                                    }
                                                    }
                                                    
                                                                        
                                                }
                                                                    
                                                            else
                                                            {
                                                                        
                                                                expiryDateFilter=[[symbol1 objectAtIndex:0]objectForKey:@"expiryseries"];
                                                                
                                                                
                                                                //NSLog(@"%@",string);
                                                                
                                                                
                                                                int myInt = [string intValue];
                                                                
                                                                int myInt2 = [exchangeTokenString intValue];
                                                                
                                                                //NSLog(@"token%i",myInt2);
                                                                
                                                                
                                                                //NSLog(@"token%i",myInt);
                                                                
                                                                
                                                                
                                                                if(i==0)
                                                                {
                                                                    
                                                                    [instrumentToken addObject:[NSNumber numberWithInt:myInt]];
                                                                    
                                                                    //NSLog(@"%@",instrumentToken);
                                                                    
                                                                    [exchangeTokenArray addObject:exchangeTokenString];
                                                                    [upStoxFUTSymbolArrayToken addObject:string];
                                                                    
                                                                    NSString * symbol = [NSString stringWithFormat:@"%@",[[symbol1 objectAtIndex:i]objectForKey:@"securitydesc"]];
                                                                    if([delegate2.brokerNameStr isEqualToString:@"Upstox"]&&[symbol containsString:@"BANKNIFTY"])
                                                                    {
                                                                        [upStoxFUTSymbolArray addObject:[[symbol1 objectAtIndex:i]objectForKey:@"securitydescupstox"]];
                                                                    }
                                                                    else
                                                                    {
                                                                        [upStoxFUTSymbolArray addObject:[[symbol1 objectAtIndex:i]objectForKey:@"securitydesc"]];
                                                                    }
                                                                    
                                                                    
                                                                }
                                                                
                                                                
                                                              
                                                             
                                                                
                                                                NSString * lot=[[symbol1 objectAtIndex:0]valueForKey:@"lotsize"];
                                                                
                                                                [delegate2.depthLotSize addObject:lot];
                                                                
                                                                
                                                                location=0;
                                                                
                                                                
                                                                        
                                                                        
                                                        }
                                                                    
                                                 
                                                                    
                                                                    
                                                                }
                                                                
                                                                //NSLog(@"%@",expiry);
                                                                
                                                    
                                                            }
                                                            
                                                            
                                                            else
                                                            {
                                                                self.view3.hidden=YES;
                                                                self.view4.hidden=YES;
                                                                self.view5.hidden=YES;
                                                                self.upDownButton.enabled=NO;
                                                            }
                                                            
                                                        }
                                                        }
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                           
                                                            if(symbol1.count>0)
                                                            {
                                                                
                                                                
                                                                    
                                                                
                                                                    
                                                                
                                                                NSString * dateStr=[expiry objectAtIndex:location];
                                                                
                                                                delegate2.tradingSecurityDes=[[symbol1 objectAtIndex:location]valueForKey:@"securitydesc"];
                                                              
                                                                if([delegate2.brokerNameStr isEqualToString:@"Upstox"]&&[delegate2.tradingSecurityDes containsString:@"BANKNIFTY"])
                                                                {
                                                                    delegate2.tradingSecurityDes=[[symbol1 objectAtIndex:location]valueForKey:@"securitydescupstox"];
                                                                }
                                                                else
                                                                {
                                                                     delegate2.tradingSecurityDes=[[symbol1 objectAtIndex:location]valueForKey:@"securitydesc"];
                                                                }
                                                                
                                                                [self.expiryBtnOut setTitle:dateStr forState:UIControlStateNormal];
                                                                
//                                                                delegate2.marketWatch1=[[NSMutableArray alloc]init];
                                                                if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                                                                {
                                                                [self websocket];
                                                                }
                                                                
                                                                else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
                                                                {
                                                                    nseCheck=true;
                                                                    nfoCheck=true;
                                                                  
                                                                    [self subMethod];
                                                                    
                                                                }
                                                                
                                                                else
                                                                    
                                                                {
                                                                    
                                                                   [self websocket];
                                                                }
                                                                
                                                                
                                                            }
                                                            
                                                            else
                                                                
                                                            {
                                                                if(self.view.frame.size.width<370)
                                                                {
                                                                    
                                                                }
                                                                
                                                                else
                                                                {
                                                                    self.scrollView.scrollEnabled=false;
                                                                }
//                                                                 SEdelegate2.marketWatch1=[[NSMutableArray alloc]init];
                                                                if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                                                                {
                                                                    [self websocket];
                                                                }
                                                                
                                                               else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
                                                                {
                                                                    nseCheck=true;
//                                                                    [self websocket];
                                                                    [self subMethod];
                                                                    
                                                                }
                                                               else
                                                               {
                                                                   [self websocket];
                                                                   
                                                               }
                                                                
                                                                
                                                            }
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                        });
                                                        
                                                        
                                                    }];
        [dataTask resume];
        

        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
   
   
}

-(void)optionCEMethod
{
    @try
    {
     callPutDictionary = [[NSMutableDictionary alloc]init];
     FUTCheck=false;
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"authtoken":delegate2.zenwiseToken,
                                  @"deviceid":delegate2.currentDeviceId
                               };
    
    if([delegate2.exchaneLblStr containsString:@"NSE"]||[delegate2.exchaneLblStr containsString:@"NFO-FUT"]||[delegate2.exchaneLblStr containsString:@"NFO-OPT"])
    {
        localFutExchange=@"NFO-OPT";
        
        
    }
    
    else if([delegate2.exchaneLblStr containsString:@"BSE"]||[delegate2.exchaneLblStr containsString:@"BFO-FUT"]||[delegate2.exchaneLblStr containsString:@"BFO-OPT"])
    {
        localFutExchange=@"BFO-OPT";
    }
    
    else if([delegate2.exchaneLblStr containsString:@"CDS"])
    {
        
        
        localFutExchange=@"CDS-OPT";
    }
    
    else if([delegate2.exchaneLblStr containsString:@"MCX"])
    {
        
        
        localFutExchange=@"MCX";
    }
    
    @try {
//      
        NSString * depthStr;
        
       
        
         depthStr=[NSString stringWithFormat:@"%@stock/faostrikes/%@?segment=%@&ltp=%@&limit=2000&optiontype=CE&orderby=ASC&expiry=%@",delegate2.baseUrl,delegate2.symbolDepthStr,localFutExchange,self.FutLtpLbl.text,expiryDateFilter];
            
    
        
        
        //NSLog(@"%@",depthStr);
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:depthStr]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            if([httpResponse statusCode]==403)
                                                            {
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                
                                                            }
                                                            else if([httpResponse statusCode]==401)
                                                            {
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                
                                                            }
                                                            else
                                                            {
                                                                
                                                            if([httpResponse statusCode]==200)
                                                                
                                                            {
                                                            if(strike.count>0)
                                                            {
                                                                [strike removeAllObjects];
                                                            }
                                                            NSMutableArray * newSymbol = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil]mutableCopy];
                                                            
                                                            symbol2=[[NSMutableArray alloc]init];
                                                            
                                                                bankNiftyOptionArray = [[NSMutableArray alloc]init];
                                                            for(int i=0;i<newSymbol.count;i++)
                                                            {
                                                                [symbol2 addObject:[newSymbol objectAtIndex:i]];
                                                                
                                                                NSString *myString = [[newSymbol objectAtIndex:i]objectForKey:@"expiryseries"];
                                                                NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                                                                dateFormatter.dateFormat = @"yyyy-MM-dd";
                                                                NSDate *yourDate = [dateFormatter dateFromString:myString];
                                                                dateFormatter.dateFormat = @"dd MMM yyyy";
                                                                //NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
                                                                
                                                                NSString * finalDate=[[dateFormatter stringFromDate:yourDate] uppercaseString];
                                                                //                                                                        rtrt
                                                                //                                                                        if(expiry.count==0)
                                                                //                                                                        {
                                                                //                                                                               [expiry addObject:finalDate];
                                                                //                                                                        }
                                                                //
                                                                //                                                                        else
                                                                //                                                                        {
                                                                //                                                                            if([expiry containsObject:finalDate])
                                                                //                                                                            {
                                                                //
                                                                //                                                                            }
                                                                //                                                                            else
                                                                //                                                                            {
                                                             
                                                                //                                                                            }
                                                                
                                                                
                                                            }
                                                                
                                                              
                                                               
                                                            //NSLog(@"%@",newSymbol);
                                                            //NSLog(@"%@",symbol2);
                                                            
                                                            
                                                            
                                                            
                                                            if(symbol2.count>0)
                                                            {
                                                                
                                                                for(int i = 0; i< symbol2.count;i++)
                                                                {
                                                                    
                                                                    @autoreleasepool {
                                                                        
                                                                        
                                                                        NSMutableArray *array_Temp = [[NSMutableArray alloc]init];
                                                                        for ( int  j=0;j<=3;j++)
                                                                        {
                                                                            [array_Temp addObject:@(NO)];
                                                                        }
                                                                        
                                                                        [array_isSelected addObject:array_Temp];
                                                                        
                                                                    }
                                                                    
                                                                }

                                                                
                                                                //                                                            NSString * string=[[symbol2 objectAtIndex:0]valueForKey:@"instrument_token"];
                                                                //
                                                                //                                                            //NSLog(@"%@",string);
                                                                //
                                                                //
                                                                //                                                            int myInt = [string intValue];
                                                                //
                                                                //                                                            //NSLog(@"token%i",myInt);
                                                                //
                                                                //                                                            [instrumentToken addObject:[NSNumber numberWithInt:myInt]];
                                                                //
                                                                //                                                            //NSLog(@"%@",instrumentToken);
                                                                
                                                                //                                                             [strike addObject:[[symbol2 objectAtIndex:0]valueForKey:@"strikeprice"]];
                                                                
                                                               
                                                                [self performCallPutDepth:false and:symbol2];
                                                                
                                                                
                                                            }
                                                                
                                                            }
                                                            
                                                            else
                                                            {
                                                                [self optionCEMethod];
                                                            }
                                                            }
                                                        }
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            if([self.companyTxt1.text containsString:@"BANKNIFTY"])
                                                            {
                                                                [self bankNiftyOptionExp];
                                                            }
                                                            [self optionPEMethod];
                                                            
                                                            [self.OPTBtn setTitle:expiryDateFilter forState:UIControlStateNormal];
                                                            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                                                            dateFormatter.dateFormat = @"yyyy-MM-dd";
                                                            NSDate *yourDate = [dateFormatter dateFromString:expiryDateFilter];
                                                            dateFormatter.dateFormat = @"dd MMM yyyy";
                                                            //NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
                                                            
                                                            NSString * finalDate=[[dateFormatter stringFromDate:yourDate] uppercaseString];
                                                            if(finalDate !=nil)
                                                            {
                                                             [self.OPTBtn setTitle:finalDate forState:UIControlStateNormal];
                                                            }
                                                            
                                                        });
                                                        
                                                        
                                                    }];
        [dataTask resume];
        

        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}

-(void)bankNiftyOptionExp
{
    
    @try {
        NSDictionary *headers = @{
                                  @"cache-control": @"no-cache",
                                  @"authtoken":delegate2.zenwiseToken,
                                  @"deviceid":delegate2.currentDeviceId
                                  };
        NSString * urlStr=[NSString stringWithFormat:@"%@stock/fao/%@?segment=NFO-OPT&optiontype=CE&limit=20000&orderby=ASC",delegate2.baseUrl,self.companyTxt1.text];
        
        //NSLog(@"url %@",urlStr);
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            
                                                            //                                                        [self.company removeAllObjects];
                                                            
                                                            //
                                                            if([httpResponse statusCode]==403)
                                                            {
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                
                                                            }
                                                            else if([httpResponse statusCode]==401)
                                                            {
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                
                                                            }
                                                            else
                                                            {
                                                                if([bankNiftyCheck isEqualToString:@"FUT"])
                                                                {
                                                                    [bankNiftyOptionArray removeAllObjects];
                                                                }
                                                                if(strike.count>0)
                                                                {
                                                                    [strike removeAllObjects];
                                                                }
                                                                
                                                                if(filteredArray.count>0)
                                                                {
                                                                    [filteredArray removeAllObjects];
                                                                }
                                                                
                                                                NSArray * responseArray1=[[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] mutableCopy];
                                                                
                                                                NSLog(@"%@",responseArray1);
                                                                
                                                                if(responseArray1.count>0)
                                                                {
                                                                    
                                                                    for(int i=0;i<responseArray1.count;i++)
                                                                    {
                                                                    NSString *myString = [[responseArray1 objectAtIndex:i]objectForKey:@"expiryseries"];
                                                                    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                                                                    dateFormatter.dateFormat = @"yyyy-MM-dd";
                                                                    NSDate *yourDate = [dateFormatter dateFromString:myString];
                                                                    dateFormatter.dateFormat = @"dd MMM yyyy";
                                                                    //NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
                                                                    
                                                                    NSString * finalDate=[[dateFormatter stringFromDate:yourDate] uppercaseString];
                                                                    //                                                                        rtrt
                                                                    //                                                                        if(expiry.count==0)
                                                                    //                                                                        {
                                                                    //                                                                               [expiry addObject:finalDate];
                                                                    //                                                                        }
                                                                    //
                                                                    //                                                                        else
                                                                    //                                                                        {
                                                                    //                                                                            if([expiry containsObject:finalDate])
                                                                    //                                                                            {
                                                                    //
                                                                    //                                                                            }
                                                                    //                                                                            else
                                                                    //                                                                            {
                                                                    [bankNiftyOptionArray addObject:finalDate];
                                                                    
                                                                    }
                                                                    
                                                                    
                                                                    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:bankNiftyOptionArray];
                                                                    bankNiftyOptionArray = [[orderedSet array]mutableCopy];
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
//                                                               [self.OPTBtn setTitle:[bankNiftyOptionArray objectAtIndex:0] forState:UIControlStateNormal];
                                                                    });
                                                                }
                                                            }
                                                        }
                                                        
                                                    
                                                        
                                                        
                                                    }];
        [dataTask resume];
        
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}

-(void)performCallPutDepth:(BOOL)checkCE_PE and:(NSArray *)CE_PEArray
{
    @try {
        for (int i=0 ; i<[CE_PEArray count] ;i++)
        {
            
            @autoreleasepool {
                
                NSMutableDictionary *tmp = [CE_PEArray objectAtIndex:i];
                
                NSString *dateDescription = [tmp valueForKey:@"securitydesc"];
//                if([delegate2.brokerNameStr isEqualToString:@"Upstox"]&&[dateDescription containsString:@"BANKNIFTY"])
//                {
//                   dateDescription = [tmp valueForKey:@"securitydescupstox"];
//                }
//                else
//                {
//                   dateDescription = [tmp valueForKey:@"securitydesc"];
//                }
                
//                int totalStringLength = (int)[dateDescription length];
//                int strikePriceLength = (int)[[tmp valueForKey:@"strikeprice"]length];
//                int trimedLength = totalStringLength - 2 - strikePriceLength;
//
                NSString *month =@"";
//                if([[tmp valueForKey:@"strikeprice"] containsString:@".0"])
//                {
//                    if([dateDescription containsString:@"BANKNIFTY"])
//                    {
//                          month = [dateDescription substringWithRange:NSMakeRange(trimedLength+2 -3, 3)];
//                    }
//                    else
//                    {
//                    month = [dateDescription substringWithRange:NSMakeRange(trimedLength+2 -3, 3)];
//                    }
//                }
//
//                else{
//                    if([dateDescription containsString:@"BANKNIFTY"])
//                    {
//                        month = [dateDescription substringWithRange:NSMakeRange(trimedLength+2 -3, 3)];
//                    }
//                    else
//                    {
//                    month = [dateDescription substringWithRange:NSMakeRange(trimedLength -3, 3)];
//                    }
//                }
                
                NSString *myString =[tmp valueForKey:@"expiryseries"];
                NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                 dateFormatter.dateFormat = @"yyyy-MM-dd";
                NSDate *yourDate = [dateFormatter dateFromString:myString];
                dateFormatter.dateFormat = @"MMM";
                NSString * finalDate=[dateFormatter stringFromDate:yourDate];
                month = finalDate;
                //NSLog(@"%@",month);
                
                
                
                if([callPutDictionary objectForKey:month])
                {
                    
                    
                    if(checkCE_PE) //PE
                    {
                        
                        NSMutableArray *tmpMonth = [callPutDictionary objectForKey:month];
                        
                        if([[tmpMonth objectAtIndex:1] count]>=1)
                        {
                            
                            NSMutableArray *PEArrayTemp = [tmpMonth objectAtIndex:1];
                            [PEArrayTemp addObject:tmp];
                            [tmpMonth replaceObjectAtIndex:1 withObject:PEArrayTemp];
                            [callPutDictionary setObject:tmpMonth forKey:month];
                        }
                        else
                        {
                            [self performPECommonFunction:tmp chekCE_PEValue:checkCE_PE mon:month and:tmpMonth];
                            
                            
                        }
                        
                        
                    }
                    else    //CE
                    {
                        NSMutableArray *tmpMonth = [callPutDictionary objectForKey:month];
                        NSMutableArray *CEArrayTemp = [tmpMonth objectAtIndex:0];
                        [CEArrayTemp addObject:tmp];
                        [tmpMonth replaceObjectAtIndex:0 withObject:CEArrayTemp];
                        [callPutDictionary setObject:tmpMonth forKey:month];
                    }
                    
                }
                else
                {
                    
                    [self performPECommonFunction:tmp chekCE_PEValue:checkCE_PE mon:month and:nil];
                }
                
            }
            
            
        }
        
        //NSLog(@"***********************%@",callPutDictionary);
        
        title=[[callPutDictionary allKeys]mutableCopy];
        
        //NSLog(@"t%@",title);
        if(title.count>0)
        {
            titleStr=[title objectAtIndex:0];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                
//                [self.OPTBtn setTitle:titleStr forState:UIControlStateNormal];
                
                
            });
            
            
            long c=[[callPutDictionary objectForKey:titleStr]count];
            //NSLog(@"%ld",c);
            if([[[callPutDictionary objectForKey:titleStr]objectAtIndex:1]count]>0)
            {
                long j=[[[callPutDictionary objectForKey:titleStr]objectAtIndex:0]count];
                for(int i=0;i<j;i++)
                {
                    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
                    {
                    NSString * CEToken=[[[[callPutDictionary objectForKey:titleStr]objectAtIndex:0]objectAtIndex:i]objectForKey:@"instrument_token"];
                    int myInt = [CEToken intValue];
                    
                    //NSLog(@"token%i",myInt);
                        
                        NSString * symbol=[[[[callPutDictionary objectForKey:titleStr]objectAtIndex:0]objectAtIndex:i]objectForKey:@"securitydesc"];
                                        
                      
                        if([delegate2.brokerNameStr isEqualToString:@"Upstox"]&&[symbol containsString:@"BANKNIFTY"])
                        {
                           symbol=[[[[callPutDictionary objectForKey:titleStr]objectAtIndex:0]objectAtIndex:i]objectForKey:@"securitydescupstox"];
                        }
                        else
                        {
                            symbol=[[[[callPutDictionary objectForKey:titleStr]objectAtIndex:0]objectAtIndex:i]objectForKey:@"securitydesc"];
                        }
                      
                          
                          [upStoxFUTSymbolArray addObject:symbol];
                          [upStoxFUTSymbolArrayToken addObject:CEToken];
                     
                    
                    [instrumentToken addObject:[NSNumber numberWithInt:myInt]];
                    
                    NSString * PEToken=[[[[callPutDictionary objectForKey:titleStr]objectAtIndex:1]objectAtIndex:i]objectForKey:@"instrument_token"];
                    int myInt1 = [PEToken intValue];
                    
                    //NSLog(@"token%i",myInt1);
                        
                        
                        NSString * symbol1=[[[[callPutDictionary objectForKey:titleStr]objectAtIndex:1]objectAtIndex:i]objectForKey:@"securitydesc"];
                        
                        if([delegate2.brokerNameStr isEqualToString:@"Upstox"]&&[symbol1 containsString:@"BANKNIFTY"])
                        {
                            symbol1=[[[[callPutDictionary objectForKey:titleStr]objectAtIndex:1]objectAtIndex:i]objectForKey:@"securitydescupstox"];
                        }
                        else
                        {
                            symbol1=[[[[callPutDictionary objectForKey:titleStr]objectAtIndex:1]objectAtIndex:i]objectForKey:@"securitydesc"];
                        }
                        
                        
                            
                            [upStoxFUTSymbolArray addObject:symbol1];
                            [upStoxFUTSymbolArrayToken addObject:PEToken];
                       
                    
                    [instrumentToken addObject:[NSNumber numberWithInt:myInt1]];
                    }
                    
                    else
                    {
                        
                        NSString * CEToken=[NSString stringWithFormat:@"%@",[[[[callPutDictionary objectForKey:titleStr]objectAtIndex:0]objectAtIndex:i]objectForKey:@"exchange_token"]];
//                        int myInt = [CEToken intValue];
                        
//                        //NSLog(@"token%i",myInt);
                        
                        [exchangeTokenArray addObject:CEToken];
                        
                        NSString * PEToken=[NSString stringWithFormat:@"%@",[[[[callPutDictionary objectForKey:titleStr]objectAtIndex:1]objectAtIndex:i]objectForKey:@"exchange_token"]];
//                        int myInt1 = [PEToken intValue];
//                        
//                        //NSLog(@"token%i",myInt1);
                        
                         [exchangeTokenArray addObject:PEToken];
                    }
                }
                
                
                for(int i=0;i<[[[callPutDictionary objectForKey:titleStr] objectAtIndex:0] count];i++)
                {
                    sortedStrike=[[NSMutableArray alloc]init];
                    
                    sortedStrike=[[[[callPutDictionary objectForKey:titleStr] objectAtIndex:0] objectAtIndex:i]objectForKey:@"strikeprice"];
                    
                }
                newSortedStrike=[[NSArray alloc]init];
                newSortedStrike = sortedStrike;
                
                //NSLog(@"%@",newSortedStrike);
                
                //            NSArray* data = @[@"Grapes", @"Apples", @"Oranges"];
                //                             
                //                              
                //                              NSSortDescriptor *sd = [[NSSortDescriptor alloc] initWithKey:nil ascending:NO];
                //                              newSortedStrike = [newSortedStrike sortedArrayUsingDescriptors:@[sd]];
                //            //NSLog(@"%@",newSortedStrike);
                //
                if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                {
                    
                [self.socket close];
                }
                
                
                optionExchangetokenCheck=true;
                
                if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                {
                
                [self websocket];
                }
                else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
                {
                    [self websocket];
                    [self futSubMethod];
                    
                }
                
                else
                {
                     [self websocket];
                    
                }
                
            }
        }
        

        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
   
}

-(void)performPECommonFunction:(NSDictionary *)CE_PE chekCE_PEValue:(BOOL)chekCE_PE mon:(NSString *)month and:(NSArray *)orginalMonthDetails
{
    @try {
        NSMutableArray *CEPEArray = [[NSMutableArray alloc]init];
        
        NSMutableArray *CEArray1 = [[NSMutableArray alloc]init];
        
        NSMutableArray *PEArray1 = [[NSMutableArray alloc]init];
        
        if(chekCE_PE) //PE
        {
            
            [CEArray1 addObjectsFromArray:[orginalMonthDetails objectAtIndex:0]];
            [PEArray1 addObject:CE_PE];
            [CEPEArray addObject:CEArray1];
            [CEPEArray addObject:PEArray1];
            
            
            
        }
        else   //CE
        {
            
            [CEArray1 addObject:CE_PE];
            [CEPEArray addObject:CEArray1];
            [CEPEArray addObject:[NSArray new]];
            
        }
        
        
        
        [callPutDictionary setObject:CEPEArray forKey:month];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    

}





-(void)optionPEMethod
{
    @try
    {
    NSDictionary *headers = @{ @"content-type": @"application/x-www-form-urlencoded",
                               @"cache-control": @"no-cache",
                               @"authtoken":delegate2.zenwiseToken,
                                  @"deviceid":delegate2.currentDeviceId
                               };
    
    if([delegate2.exchaneLblStr isEqualToString:@"NSE"]||[delegate2.exchaneLblStr containsString:@"NFO-FUT"]||[delegate2.exchaneLblStr containsString:@"NFO-OPT"])
    {
        localFutExchange=@"NFO-OPT";
    }
    
    else if([delegate2.exchaneLblStr isEqualToString:@"BSE"]||[delegate2.exchaneLblStr containsString:@"BFO-FUT"]||[delegate2.exchaneLblStr containsString:@"BFO-OPT"])
    {
        localFutExchange=@"BFO-OPT";
    }
    
   else if([delegate2.exchaneLblStr containsString:@"CDS"])
    {
        
        
        localFutExchange=@"CDS-OPT";
    }
    
   else if([delegate2.exchaneLblStr containsString:@"MCX"])
   {
       
       
       localFutExchange=@"MCX";
   }
    
    @try {
          NSString * depthStr=[NSString stringWithFormat:@"%@stock/faostrikes/%@?segment=%@&ltp=%@&limit=2000&optiontype=PE&orderby=ASC&expiry=%@",delegate2.baseUrl,delegate2.symbolDepthStr,localFutExchange,self.FutLtpLbl.text,expiryDateFilter];
        
        //NSLog(@"%@",depthStr);
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:depthStr]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            if([httpResponse statusCode]==403)
                                                            {
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                
                                                            }
                                                            else if([httpResponse statusCode]==401)
                                                            {
                                                                
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                
                                                            }
                                                            else
                                                            {
                                                                
                                                            if([httpResponse statusCode]==200)
                                                                
                                                            {
//                                                            if(strike.count>0)
//                                                            {
//
//                                                            }
                                                            NSMutableArray * newSymbol1 = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil]mutableCopy];
                                                            
                                                            symbol3=[[NSMutableArray alloc]init];
                                                            
                                                            //NSLog(@"%@",newSymbol1);
                                                            
                                                            for(int i=0;i<newSymbol1.count;i++)
                                                            {
                                                                [symbol3 addObject:[newSymbol1 objectAtIndex:i]];
                                                                
                                                            }
                                                            
                                                            //NSLog(@"%@",newSymbol1);
                                                            //NSLog(@"%@",symbol3);
                                                            
                                                            if(symbol3.count>0)
                                                            {
                                                                //                                                            NSString * string=[[symbol3 objectAtIndex:0]valueForKey:@"instrument_token"];
                                                                //
                                                                //                                                            //NSLog(@"%@",string);
                                                                //
                                                                //
                                                                //                                                            int myInt = [string intValue];
                                                                //
                                                                //                                                            //NSLog(@"token%i",myInt);
                                                                //
                                                                //                                                            [instrumentToken addObject:[NSNumber numberWithInt:myInt]];
                                                                //
                                                                //                                                            //NSLog(@"%@",instrumentToken);
                                                                
                                                              
                                                                
                                                                [self performCallPutDepth:true and:symbol3];
                                                            }
                                                                
                                                                 }
                                                                
                                                                else
                                                                {
                                                                    [self optionPEMethod];
                                                                }
                                                                
                                                            }
                                                            
                                                        }
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            if([self.companyTxt1.text containsString:@"BANKNIFTY"])
                                                            {
                                                                [self bankNiftyOptionExp];
                                                            }
                                                            
                                                            
                                                        });
                                                        
                                                        
                                                    }];
        [dataTask resume];

        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}




- (void)viewDidAppear:(BOOL)animated
{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(hideMethod) userInfo:nil repeats:YES];
    delegate2.depthMt=true;
 
    globalAllKeys=[[NSMutableArray alloc]init];
    
    delegate2.broadcastOpen=true;
    
   if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
   {


   }
    
    else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
//         [self websocket];
    
       
    }

    else
    {
        timer=  [NSTimer scheduledTimerWithTimeInterval:0.5f
                                                 target:self selector:@selector(reloadMethod) userInfo:nil repeats:YES];
    }
    
    
//    delegate2.tradeCompletedArray =[NSMutableArray alloc]
    
    
    
//     [[NSNotificationCenter defaultCenter] postNotificationName:@"broad" object:nil];
//     [[NSNotificationCenter defaultCenter] postNotificationName:@"broad" object:nil];

//    
//   if(delegate2.optionLimitPriceArray.count>0)
//    
//   {
//       [delegate2.optionLimitPriceArray removeAllObjects];
//       [delegate2.optionOrderArray removeAllObjects];
//       [delegate2.optionOrderTransactionArray removeAllObjects];
//       
//       [self.depthTableView reloadData];
//       
//    
//       
//   }
   
}

-(void)reloadMethod
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{

                       self.depthTableView.delegate=self;
                       self.depthTableView.dataSource=self;
                       [self.depthTableView reloadData];

                   });
    
}

//-(void)viewWillDisappear:(BOOL)animated
//{
//    delegate2.depthMt=false;
//}
//-(void)filterMethod
//
//{
//    [instrumentToken removeAllObjects];
//    for (int i = 0; i<[symbol count]; i++)
//    {
//        NSString * status=[[symbol objectAtIndex:i]valueForKey:@"segment"];
//        
//        //NSLog(@"segment %@",status);
//        //NSLog(@"%@",delegate2.exchaneLblStr);
//        
//        if([status isEqualToString:delegate2.exchaneLblStr])
//        {
//            
//            
//            NSString * string=[[symbol objectAtIndex:i]valueForKey:@"instrument_token"];
//            
//            //NSLog(@"%@",string);
//            
//            
//            int myInt = [string intValue];
//            
//            //NSLog(@"token%i",myInt);
//
//            [instrumentToken addObject:[NSNumber numberWithInt:myInt]];
//            
//            //NSLog(@"%@",instrumentToken);
//            
//           
//
//        }
//    }
//    
//    
//}

-(void)uiMethod
{
    
    ///drop down//
    
    dataArray=[[NSArray alloc]initWithObjects:@"Zerodha",@"Spinner",@"Broker not available",nil];
    pickerView=[[UIPickerView alloc]init];
    ViewContainer=[[UIView alloc]init];
    
    [self.strikeBtn1 addTarget:self action:@selector(strikeDropDown) forControlEvents:UIControlEventTouchUpInside];
    
    [self.strike2 addTarget:self action:@selector(strikeDropDown) forControlEvents:UIControlEventTouchUpInside];
    
    dropDwnTableView=[[UITableView alloc]init];
    dropDwnTableView.frame = CGRectMake(self.strikeBtn1.frame.origin.x,self.strikeBtn1.frame.origin.y+ self.strikeBtn1.frame.size.height,self.strikeBtn1.frame.size.width,200);
   
    dropDwnTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth
    ;
    dropDwnTableView.layer.borderWidth = 0.1f;
    dropDwnTableView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    [dropDwnTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    [dropDwnTableView reloadData];
    [self.view addSubview:dropDwnTableView];
    
    
    dropDwnTableView.hidden=YES;
    dropDwnTableView.layer.cornerRadius=3;
    
    //    [_chkImgBtn setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
    //    [_chkImgBtn setImage:[UIImage imageNamed:@"checkSel-box.png"] forState:UIControlStateSelected];
    //    _chkImgBtn.selected = NO;
    
      self.view2_Height.constant = 0;
    self.view4_Height.constant = 0;
    _view2.hidden = true;
    _view4.hidden=true;
    [_view2 layoutIfNeeded];
    [_view4 layoutIfNeeded];
    self.tableView.hidden=YES;
    
//    self.contentHeight.constant=1000;
    
    self.titlesArray=[[NSMutableArray alloc]init];
    
    [self.bidbtn1 addTarget:self action:@selector(onClickingbidBtn1) forControlEvents:UIControlEventTouchUpInside];
    
    [self.bidBtn2 addTarget:self action:@selector(onClickingbidBtn2) forControlEvents:UIControlEventTouchUpInside];
    
    [self.bidBtn3 addTarget:self action:@selector(onClickingbidBtn3) forControlEvents:UIControlEventTouchUpInside];
    
    [self.bidBtn4 addTarget:self action:@selector(onClickingbidBtn4) forControlEvents:UIControlEventTouchUpInside];
    
    [self.bidBtn5 addTarget:self action:@selector(onClickingbidBtn5) forControlEvents:UIControlEventTouchUpInside];
    
    [self.bidBtn6 addTarget:self action:@selector(onClickingbidBtn6) forControlEvents:UIControlEventTouchUpInside];
    
    [self.bidBtn7 addTarget:self action:@selector(onClickingbidBtn7) forControlEvents:UIControlEventTouchUpInside];
    
    [self.bidBtn8 addTarget:self action:@selector(onClickingbidBtn8) forControlEvents:UIControlEventTouchUpInside];
    
    [self.bidBtn9 addTarget:self action:@selector(onClickingbidBtn9) forControlEvents:UIControlEventTouchUpInside];
    
    [self.bidBtn10 addTarget:self action:@selector(onClickingbidBtn10) forControlEvents:UIControlEventTouchUpInside];
    
    [self.askBtn1 addTarget:self action:@selector(onClickingaskBtn1) forControlEvents:UIControlEventTouchUpInside];
    
    [self.askBtn2 addTarget:self action:@selector(onClickingaskBtn2) forControlEvents:UIControlEventTouchUpInside];
    
    [self.askBtn3 addTarget:self action:@selector(onClickingaskBtn3) forControlEvents:UIControlEventTouchUpInside];
    
    [self.askBtn4 addTarget:self action:@selector(onClickingaskBtn4) forControlEvents:UIControlEventTouchUpInside];
    
    [self.askBtn5 addTarget:self action:@selector(onClickingaskBtn5) forControlEvents:UIControlEventTouchUpInside];
    
    [self.askBtn6 addTarget:self action:@selector(onClickingaskBtn6) forControlEvents:UIControlEventTouchUpInside];
    
    [self.askBtn7 addTarget:self action:@selector(onClickingaskBtn7) forControlEvents:UIControlEventTouchUpInside];
    
    [self.askBtn8 addTarget:self action:@selector(onClickingaskBtn8) forControlEvents:UIControlEventTouchUpInside];
    
    [self.askBtn9 addTarget:self action:@selector(onClickingaskBtn9) forControlEvents:UIControlEventTouchUpInside];
    
    [self.askBtn10 addTarget:self action:@selector(onClickingaskBtn10) forControlEvents:UIControlEventTouchUpInside];
    
  
}
-(void)websocket
{
    @try {
        
        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
        {
        //pocket socket//
//        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"wss://websocket.kite.trade/?api_key=%@&user_id=%@&public_token=%@",delegate2.APIKey,delegate2.userID,delegate2.publicToken]];
         NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"wss://ws.kite.trade/?api_key=%@&access_token=%@",delegate2.APIKey,delegate2.accessToken]];
        //NSLog(@"user %@",delegate2.userID);
        //NSLog(@"user %@",delegate2.publicToken);
        
        
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        
        //     create the socket and assign delegate
        self.socket = [PSWebSocket clientSocketWithRequest:request];
        self.socket.delegate = self;
        //
        //    self.socket.delegateQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        //     open socket
        [self.socket open];
            
        }
        
        else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {@try {
           
            NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"wss://ws-api.upstox.com/?apiKey=%@&token=%@",delegate2.APIKey,delegate2.accessToken]];
            
            
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            
            
            //     create the socket and assign delegate
            self.socket = [PSWebSocket clientSocketWithRequest:request];
            self.socket.delegate = self;
             [self.socket open];
            //
            //    self.socket.delegateQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            
            //     open socket
            
            //        if(delegate2.instrumentToken.count>0)
            //    {
            //
            //    }
            
            
            
        }
            @catch (NSException * e) {
                //NSLog(@"Exception: %@", e);
            }
            @finally {
                //NSLog(@"finally");
            }
            
            
        }
        
        
        
        else
        {
            
            
            
           
            
            if(symbol.count>0)
            {
            if([delegate2.exchaneLblStr containsString:@"NSE"]||[delegate2.exchaneLblStr containsString:@"NFO"])
            {
                exchange=@"NSECM";
                FUTexchange=@"NSEFO";
            }
            
            else if([delegate2.exchaneLblStr containsString:@"BSE"]||[delegate2.exchaneLblStr containsString:@"BFO"])
            {
                exchange=@"BSECM";
                FUTexchange=@"BSEFO";
            }
                
            }
            
            else
            {
                
                if([delegate2.exchaneLblStr containsString:@"NSE"]||[delegate2.exchaneLblStr containsString:@"NFO"])
                {
                    exchange=@"NSEFO";
                    FUTexchange=@"NSEFO";
                }
                
                else if([delegate2.exchaneLblStr containsString:@"BSE"]||[delegate2.exchaneLblStr containsString:@"BFO"])
                {
                    exchange=@"BSEFO";
                    FUTexchange=@"BSEFO";
                }
                
            }
            
            
           
            
            
            
//            for(int i=0;i<exchangeTokenArray.count;i++)
//            {
//
//                TagEncode * tag1 = [[TagEncode alloc]init];
//                delegate2.mtCheck=true;
//
//
//                //  [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.Watch];
//
//
//                [tag1 TagData:sharedManager.shMsgCode shValueMethod:sharedManager.Broadcast];
//                //        [tag TagData:sharedManager.stSecurityID stringMethod:[localInstrumentToken objectAtIndex:i]]; //2885
//                [tag1 TagData:sharedManager.stSecurityID stringMethod:[exchangeTokenArray objectAtIndex:i]];
//
//                if(i==0)
//                {
//                [tag1 TagData:sharedManager.stExchange stringMethod:exchange];
//                }
//
//                else
//                {
//
//                    [tag1 TagData:sharedManager.stExchange stringMethod:FUTexchange];
//                }
//
//
//
//                [tag1 GetBuffer];
//
//                if([delegate2.depth isEqualToString:@"WISDOM"])
//                {
//                    [self newMessage];
//                }
//
//              else if([delegate2.depth isEqualToString:@"MARKETWATCH"])
//              {
//
//
//                [[NSNotificationCenter defaultCenter] postNotificationName:@"ordertowatch" object:nil];
//              }
//
//
//            }
            delegate2.marketWatch1=[[NSMutableArray alloc]init];
                
            NSURL* url = [[NSURL alloc] initWithString:delegate2.ltpServerURL];
         
            
            self.manager = [[SocketManager alloc] initWithSocketURL:url config:@{@"log": @YES}];
            
            self.socketio=self.manager.defaultSocket;
            
            [self.socketio on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
                //NSLog(@"socket connected");
                
                
                for(int i=0;i<exchangeTokenArray.count;i++)
                {
                    NSMutableArray * subscriptionList=[[NSMutableArray alloc]init];
                    NSMutableDictionary * tmpDict = [[NSMutableDictionary alloc]init];
                    
                    [tmpDict setValue:[exchangeTokenArray objectAtIndex:i] forKey:@"securityToken"];
                    if(i==0)
                    {
                        
                         [tmpDict setValue:exchange forKey:@"exchange"];
                    }
                    
                    else
                    {
                        
                        
                         [tmpDict setValue:FUTexchange forKey:@"exchange"];
                    }
                    
                   
                    [subscriptionList addObject:tmpDict];
                    
                    [self.socketio emit:@"symbol" with:subscriptionList];
//                    [self.socketio emit:@"initltp" with:subscriptionList];
                    
                }
                
                
            }];
            
            [self.socketio on:@"heartbeat" callback:^(NSArray* data, SocketAckEmitter* ack) {
                //NSLog(@"%@",data);
                
                
                
                
            }];
            
            [self.socketio on:@"marketclose" callback:^(NSArray* data, SocketAckEmitter* ack) {
                //NSLog(@"%@",data);
                
                if(data.count>0)
                {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSString * message=[NSString stringWithFormat:@"%@",[data objectAtIndex:0]];
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:message preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                    
                    [alert addAction:okAction];
                    
                });
                }
                
            }];
            
            [self.socketio on:@"initltp" callback:^(NSArray* data, SocketAckEmitter* ack) {
                //NSLog(@"%@",data);


                //                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                if(data.count>0)
                {
                 [self performSelector:@selector(mtbroad:) withObject:data afterDelay:1.0f];
                }
                //                });


            }];
            
            
            [self.socketio on:@"ltp" callback:^(NSArray* data, SocketAckEmitter* ack) {
                //NSLog(@"%@",data);
                if(data.count>0)
                {
                [self performSelector:@selector(mtbroad:) withObject:data afterDelay:1.0f];
                }
                
                
                
                
            }];
            
            [self.socketio connect];

            
        }
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
   

}

//-(void)liveStreaming
//{
//    //    [delegate2.lastPriceArray removeAllObjects];
//
//    //    for(int i=0;i<self.companyArray1.count;i++)
//    //    {
//    //        NSString * name=[NSString stringWithFormat:@"%@",[self.companyArray1 objectAtIndex:i ]];
//
//
//    self.session2=[NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//    NSURL *url1 = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.kite.trade/instruments/NSE/IDFC/?api_key=%@&access_token=%@",delegate2.APIKey,delegate2.accessToken]];
//
//    self.urlRequest2=[[NSMutableURLRequest alloc]initWithURL:url1];
//
//    self.task2=[self.session2 dataTaskWithRequest:self.urlRequest2 completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
//                {
//
//                    delegate2.cashDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//                    //NSLog(@"%@",delegate2.cashDic);
//
//                    delegate2.lastPriceStr = [NSString stringWithFormat:@"%@",[[delegate2.cashDic objectForKey:@"data"]objectForKey:@"last_price"]];
//
//                    //                    [self.arr addObject:[[delegate2.cashDic objectForKey:@"data"]objectForKey:@"last_price"]];
//                    //
//                    //                    //NSLog(@"c%@",[delegate2.lastPriceArray objectAtIndex:0]);
//
//                                         delegate2.lastTimeStr = [NSString stringWithFormat:@"%@",[[delegate2.cashDic objectForKey:@"data"]objectForKey:@"last_time"]];
//
//                    delegate2.changePercentageStr = [NSString stringWithFormat:@"%@",[[delegate2.cashDic objectForKey:@"data"]objectForKey:@"change_percent"]];
//                    //
//                    delegate2.changeString=[NSString stringWithFormat:@"%@",[[delegate2.cashDic objectForKey:@"data"]objectForKey:@"change"]];
//
//                    delegate2.volumeString=[NSString stringWithFormat:@"%@",[[delegate2.cashDic objectForKey:@"data"]objectForKey:@"volume"]];
//
//                    delegate2.openString=[NSString stringWithFormat:@"%@",[[[delegate2.cashDic objectForKey:@"data"] objectForKey:@"ohlc"] objectForKey:@"open"]];
//
//                    delegate2.highString=[NSString stringWithFormat:@"%@",[[[delegate2.cashDic objectForKey:@"data"] objectForKey:@"ohlc"] objectForKey:@"high"]];
//
//                    delegate2.lowString=[NSString stringWithFormat:@"%@",[[[delegate2.cashDic objectForKey:@"data"] objectForKey:@"ohlc"] objectForKey:@"low"]];
//
//                    delegate2.closeStr=[NSString stringWithFormat:@"%@",[[[delegate2.cashDic objectForKey:@"data"] objectForKey:@"ohlc"] objectForKey:@"close"]];
//
//                    delegate2.bidSizeStr=[NSString stringWithFormat:@"%@",[[[[[delegate2.cashDic objectForKey:@"data"] objectForKey:@"depth"] objectForKey:@"buy"] objectAtIndex:0] objectForKey:@"price"]];
//
//                    delegate2.askSizeStr=[NSString stringWithFormat:@"%@",[[[[[delegate2.cashDic objectForKey:@"data"] objectForKey:@"depth"] objectForKey:@"sell"] objectAtIndex:0] objectForKey:@"price"]];
//
//
//
//
//                    //
//                    //                    //NSLog(@"delegate2.changePercentageStr----%@", delegate2.changePercentageStr);
//                    //
//                    //                    //NSLog(@"delegate2.lastTimeStr----%@", delegate2.lastTimeStr);
//                    //
//                    //                    //NSLog(@"delegate2.lastPriceStr ----%@", delegate2.lastPriceStr );
//                    //
//                    [delegate2.lastPriceArray addObject:[[delegate2.cashDic objectForKey:@"data"]objectForKey:@"last_price"]];
//
//                    [delegate2.percentageChangeArray addObject:[[delegate2.cashDic objectForKey:@"data"]objectForKey:@"change_percent"]];
//
//                    [delegate2.timeArray addObject:[[delegate2.cashDic objectForKey:@"data"]objectForKey:@"last_time"]];
//
//                    //NSLog(@"sx %@",[delegate2.timeArray objectAtIndex:0]);
//                    //NSLog(@"sx %@",[delegate2.lastPriceArray objectAtIndex:0]);
//                    //NSLog(@"sx %@",[delegate2.percentageChangeArray objectAtIndex:0]);
//
//
//
//                    dispatch_async(dispatch_get_main_queue(), ^{
//
//                        self.cashOpen.text=delegate2.openString;
//                        self.cashHigh.text=delegate2.highString;
//                        self.cashLow.text=delegate2.lowString;
//                        self.cashVolume.text=delegate2.volumeString;
//                        self.cashBidSize.text=delegate2.bidSizeStr;
//                        self.askBidSize.text=delegate2.askSizeStr;
//                        self.ltpLbl.text=delegate2.lastPriceStr;
//                        self.chngLbl.text=delegate2.changeString;
//                        self.timeLbl.text=delegate2.lastTimeStr;
//                        self.chngperLbl.text=delegate2.changePercentageStr;
//                        self.cashClose.text=delegate2.closeStr;
//                        self.bidLbl.text=delegate2.bidSizeStr;
//                        self.askLbl.text=delegate2.askSizeStr;
//
//                        if([delegate2.changePercentageStr containsString:@"-"])
//                        {
//                            NSString * per=@"%";
//                            self.chngperLbl.text = [NSString stringWithFormat:@"(%@%@)",delegate2.changePercentageStr,per];
//
//                            //        cell.percentLbl.text = [NSString stringWithFormat:@"(%@%@)",delegate2.changePercentageStr,str];
//                            self.chngperLbl.textColor=[UIColor redColor];
//
//                        }
//                        else
//                        {
//                            NSString * per=@"%";
//                            self.chngperLbl.text = [NSString stringWithFormat:@"(%@%@)",delegate2.changePercentageStr,per];
//
//                            //        cell.percentLbl.text = [NSString stringWithFormat:@"(%@%@)",delegate2.changePercentageStr,str];
//                            self.chngperLbl.textColor=[UIColor greenColor];                        }
//
//
//
//                    });
//
//                }];
//
//    [self.task2 resume];
//}
//


-(void)segmentedControlChangedValue
{
    @try
    {
    if(segmentedControl.selectedSegmentIndex==0)
    {
        self.tableView.hidden=YES;
       
       
        self.contentView.hidden=NO;
        self.scrollView.hidden=NO;
        
        
        if(self.view1.hidden==YES)
        {
             self.view1.hidden=YES;
        }
        
        else
        {
             self.view1.hidden=NO;
            
        }
        
        if(self.view2.hidden==YES)
        {
            self.view2.hidden=YES;
        }
        
        else
        {
            self.view2.hidden=NO;
            
        }
        
        if(self.view3.hidden==YES)
        {
            self.view3.hidden=YES;
        }
        
        else
        {
            self.view3.hidden=NO;
            
        }
        
        if(self.view4.hidden==YES)
        {
            self.view4.hidden=YES;
        }
        
        else
        {
            self.view4.hidden=NO;
            
        }


    }
    else{
        
        self.tableView.hidden=NO;
        self.contentView.hidden=YES;
        self.scrollView.hidden=YES;
        
        NSDictionary *headers = @{ @"cache-control": @"no-cache",
                                   };
        
        
        NSString * url=[NSString stringWithFormat:@"http://news.accordwebservices.com/News/GetNewsSection?Top=&PageNo=1&PageSize=200&SecId=7&SubSecId=15&NewsID=&FromDate=&ToDate=&Fincode=%@&token=Qva3eKigDe1_jmSec2WYOWIWZt_F7l4m",fincode];
        
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            
                                                            companyNews=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            
                                                            //NSLog(@"%@",companyNews);
                                                            
                                                            
                                                        }
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                           
                                                            
                                                            self.tableView.delegate=self;
                                                            self.tableView.dataSource=self;
                                                            [self.tableView reloadData];
                                                        });
                                                        
                                                    }];
        [dataTask resume];
        

       
     
        
       
        
    }
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillLayoutSubviews{
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)dropDownBtn:(id)sender {
    @try
    {
    if (_startStopButtonIsActive) {
        self.startStopButtonIsActive = !self.startStopButtonIsActive;  //toggle!
        
//        self.contentHeight.constant=800;
        
        [UIView animateWithDuration:2
                              delay:0.0
                            options:UIViewAnimationCurveEaseOut
                         animations:^(void) {
                             
                             _view2_Height.constant = 0;
                             _view2.hidden = true;
                             self.imgView1.image=[UIImage imageNamed:@"expandCard.png"];
//                             self.contentHeight.constant=1000;
                             self.contentHeight.constant=self.view5.frame.origin.y+self.view5Hgt.constant-135;
                             //  self.view2.hidden=YES;
                             
                             //                             self.headerView.frame=CGRectMake(0, 176, 375, 32);
                             //                            self.view3.frame=CGRectMake(0, 209, 375, 520);
                             
                             
                         }
                         completion:NULL];
        
        
        
        
        // do your stuff here
    } else {
        self.startStopButtonIsActive = !self.startStopButtonIsActive;  //toggle!
        // do your stuff here
        
        
        
//        self.contentHeight.constant=1400;
//        self.contentHeight.constant=self.view5.frame.origin.y+self.view5Hgt.constant;
        [UIView animateWithDuration:2
                              delay:0.0
                            options:UIViewAnimationCurveEaseOut
                         animations:^(void) {
                             
                             _view2_Height.constant = 135;
                             self.imgView1.image=[UIImage imageNamed:@"collapseCard.png"];
                             _view2.hidden = false;
//                             self.contentHeight.constant=1200;
                             self.contentHeight.constant=self.view5.frame.origin.y+self.view5Hgt.constant+135;
                             
                             //                             self.headerView.frame=CGRectMake(0, 345, 375, 32);
                             //                             self.view3.frame=CGRectMake(0, 377, 375, 520);
                             
                         }
                         completion:NULL];
        
        
    }
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}
- (IBAction)dropDownBtn1:(id)sender {
    @try
    {
    if (_startStopButtonIsActive1) {
        self.startStopButtonIsActive1 = !self.startStopButtonIsActive1;  //toggle!
        
//        self.contentHeight.constant=800;
        [UIView animateWithDuration:2
                              delay:0.0
                            options:UIViewAnimationCurveEaseOut
                         animations:^(void) {
                             
                             
                             
                             
                             
                             self.view4_Height.constant=0;
                             _view4.hidden = true;
                             self.imgView2.image=[UIImage imageNamed:@"expandCard.png"];
//                             self.contentHeight.constant=1000;
                             self.contentHeight.constant=self.view5.frame.origin.y+self.view5Hgt.constant-135;

                             
                             
                             //  self.view2.hidden=YES;
                             
                             //                             self.headerView.frame=CGRectMake(0, 176, 375, 32);
                             //                            self.view3.frame=CGRectMake(0, 209, 375, 520);
                             
                             
                         }
                         completion:NULL];
        
        
        
        
        // do your stuff here
    } else {
        self.startStopButtonIsActive1 = !self.startStopButtonIsActive1;  //toggle!
        // do your stuff here
        
//        self.contentHeight.constant=1400;
//        self.contentHeight.constant=self.view5.frame.origin.y+self.view5Hgt.constant;
        [UIView animateWithDuration:2
                              delay:0.0
                            options:UIViewAnimationCurveEaseOut
                         animations:^(void) {
                             
                             
                             
                             
                             self.view4.hidden=false;
                             self.imgView2.image=[UIImage imageNamed:@"collapseCard.png"];
                             self.view4_Height.constant = 135;
//                             self.contentHeight.constant=1200;
                             
                             self.contentHeight.constant=self.view5.frame.origin.y+self.view5Hgt.constant+135;

                             
                             
                             
                             
                             //                             self.headerView.frame=CGRectMake(0, 345, 375, 32);
                             //                             self.view3.frame=CGRectMake(0, 377, 375, 520);
                             
                         }
                         completion:NULL];
        
        
    }
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==_depthTableView)
    {
//        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
//        {
        if(titleStr.length>0)
        {
            long i=[[[callPutDictionary objectForKey:titleStr] objectAtIndex:0] count];
            
           
                return i;
        
//            //NSLog(@"%ld",i);
//            self.depthTableViewHeight.constant=i*53;
//            self.view5Hgt.constant=i*60+100;
//            
//            self.contentHeight.constant=self.view5.frame.origin.y+self.view5Hgt.constant;
            
            
        }
//        }
        
//        else
//        {
//            
//            
//            if(CEDetailArray.count==PEDetailArray.count)
//            {
//                long i=[[[callPutDictionary objectForKey:titleStr] objectAtIndex:0] count];
//                return i;
//            }
//        }
    }
    
    else
    {
      
        return [[companyNews objectForKey:@"Table"] count];
       
       
    }

    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
    if(tableView==_depthTableView)
    {
        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
            OPTCell * cell1=[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            
            if(callPutDictionary.count>0)
            {
                NSString * strikeStr=[[[[callPutDictionary objectForKey:titleStr] objectAtIndex:0] objectAtIndex:indexPath.row]objectForKey:@"strikeprice"];
                
                //NSLog(@"%@",strikeStr);
                [cell1.strikeBtn setTitle:strikeStr  forState:UIControlStateNormal];
            }
            NSString * instrument;
            NSString * instrument1;
        if(CEArray.count>0)
        {
        NSNumber * number=[CEArray objectAtIndex:indexPath.row] ;
        
         instrument=[number stringValue];
        }
            if(PEArray.count>0)
            {
        
        NSNumber * number1=[PEArray objectAtIndex:indexPath.row] ;
        
        instrument1=[number1 stringValue];
            }
        
        //NSLog(@"%@",CEDetailArray);
         //NSLog(@"%@",PEDetailArray);

        
        cell1.btn_BID_CE.tag = 0;
        cell1.btn_BID_PE.tag = 1;
        cell1.btn_ASK_CE.tag = 2;
        cell1.btn_ASK_PE.tag = 3;
            
           
            
          
            
        
        @try {
            if([[[CEDetailArray objectAtIndex:indexPath.row] objectForKey:instrument]count]>0)
            {
                
                
                cell1.bidQtyCE.text=[NSString stringWithFormat:@"%@",[[[CEDetailArray objectAtIndex:indexPath.row] objectForKey:instrument] objectForKey:@"BIDValue"]];
                cell1.askQtyCE.text=[NSString stringWithFormat:@"%@",[[[CEDetailArray objectAtIndex:indexPath.row] objectForKey:instrument] objectForKey:@"ASKValue"]];
                
               
                cell1.bidPriceCE.text=[NSString stringWithFormat:@"%@",[[[CEDetailArray objectAtIndex:indexPath.row] objectForKey:instrument] objectForKey:@"BIDPrice"]];
                cell1.askPriceCE.text=[NSString stringWithFormat:@"%@",[[[CEDetailArray objectAtIndex:indexPath.row] objectForKey:instrument] objectForKey:@"ASKPrice"]];
                
                
                
            }
            
            else
                
            {
                cell1.bidQtyCE.text=@"0.00";
                cell1.askQtyCE.text=@"0.00";
                cell1.bidPriceCE.text=@"0.00";
                cell1.askPriceCE.text=@"0.00";
                
            }
            
            
            if([[[PEDetailArray objectAtIndex:indexPath.row] objectForKey:instrument1]count]>0)
                
            {
                
                cell1.bidQtyPE.text=[NSString stringWithFormat:@"%@",[[[PEDetailArray objectAtIndex:indexPath.row] objectForKey:instrument1] objectForKey:@"BIDValue"]];
                cell1.askQtyPE.text=[NSString stringWithFormat:@"%@",[[[PEDetailArray objectAtIndex:indexPath.row] objectForKey:instrument1] objectForKey:@"ASKValue"]];
                
                
                cell1.bidPricePE.text=[NSString stringWithFormat:@"%@",[[[PEDetailArray objectAtIndex:indexPath.row] objectForKey:instrument1] objectForKey:@"BIDPrice"]];
                cell1.askPricePE.text=[NSString stringWithFormat:@"%@",[[[PEDetailArray objectAtIndex:indexPath.row] objectForKey:instrument1] objectForKey:@"ASKPrice"]];
                
                
                
            }
            
            else
                
            {
                cell1.bidQtyPE.text=@"0.00";
                cell1.askQtyPE.text=@"0.00";
                cell1.bidPricePE.text=@"0.00";
                cell1.askPricePE.text=@"0.00";
                
            }
            
            [cell1.btn_BID_CE addTarget:self action:@selector(performSelectCallPut:) forControlEvents:UIControlEventTouchUpInside];
            //  cell1.btn_BID_CE.layer.borderWidth=0.0f;
            //cell1.btn_BID_CE.layer.borderColor=[[UIColor clearColor]CGColor];
            [cell1.btn_BID_PE addTarget:self action:@selector(performSelectCallPut:) forControlEvents:UIControlEventTouchUpInside];
            // cell1.btn_BID_PE.layer.borderWidth=0.0f;
            //  cell1.btn_BID_PE.layer.borderColor=[[UIColor clearColor]CGColor];
            [cell1.btn_ASK_CE addTarget:self action:@selector(performSelectCallPut:) forControlEvents:UIControlEventTouchUpInside];
            //  cell1.btn_ASK_CE.layer.borderWidth=0.0f;
            //  cell1.btn_ASK_CE.layer.borderColor=[[UIColor clearColor]CGColor];
            [cell1.btn_ASK_PE addTarget:self action:@selector(performSelectCallPut:) forControlEvents:UIControlEventTouchUpInside];
            //   cell1.btn_ASK_PE.layer.borderWidth=0.0f;
            //  cell1.btn_ASK_PE.layer.borderColor=[[UIColor clearColor]CGColor];
            
          
            
            
            //        BOOL tmpBool = [self performIsIndexPathAvaliable:indexPath];
            
    //venk//
            
            NSMutableArray *tmp_array = [[NSMutableArray alloc]initWithArray:[array_isSelected objectAtIndex:indexPath.row]];
            
            
            for(int i =0 ;i<=3;i++)
            {
                if(i == 0)
                {
                    if([[tmp_array objectAtIndex:0] isEqual:@(YES)])
                    {
                        [self performSelectDeselectBorderColor:cell1.btn_BID_CE isSelected:true];

                    }
                    else
                    {
                        [self performSelectDeselectBorderColor:cell1.btn_BID_CE isSelected:false];

                    }

                    
                }else if(i ==1)
                {
                    if([[tmp_array objectAtIndex:1] isEqual:@(YES)])
                    {
                        [self performSelectDeselectBorderColor:cell1.btn_BID_PE isSelected:true];
                        
                    }
                    else
                    {
                        [self performSelectDeselectBorderColor:cell1.btn_BID_PE isSelected:false];
                        
                    }

                }
                else if(i ==2)
                {
                    if([[tmp_array objectAtIndex:2] isEqual:@(YES)])
                    {
                        [self performSelectDeselectBorderColor:cell1.btn_ASK_CE isSelected:true];
                        
                    }
                    else
                    {
                        [self performSelectDeselectBorderColor:cell1.btn_ASK_CE isSelected:false];
                        
                    }

                }
                else if(i == 3)
                {
                    if([[tmp_array objectAtIndex:3] isEqual:@(YES)])
                    {
                        [self performSelectDeselectBorderColor:cell1.btn_ASK_PE isSelected:true];
                        
                    }
                    else
                    {
                        [self performSelectDeselectBorderColor:cell1.btn_ASK_PE isSelected:false];
                        
                    }

                }
                
                
            }
            
            return cell1;
            
        
        
        
            
        
            
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
        
        
    }
    
    else
    {
//         long i=[[[callPutDictionary objectForKey:titleStr] objectAtIndex:0] count];
        
        
       
       // dispatch_async(dispatch_get_main_queue(), ^{

        OPTCell * cell1=[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
       // [self.tableView registerClass:[UITableViewCell self] forCellReuseIdentifier:@"CELL1"];

        //    });
        if(cell1 !=nil)
        {
        
        
        
        if(callPutDictionary.count>0)
        {
            NSString * strikeStr=[[[[callPutDictionary objectForKey:titleStr] objectAtIndex:0] objectAtIndex:indexPath.row]objectForKey:@"strikeprice"];
            
            //NSLog(@"%@",strikeStr);
            [cell1.strikeBtn setTitle:strikeStr  forState:UIControlStateNormal];
        }
        
            NSString * instrument;
            NSString * instrument1;
       @try
            {
        
        instrument=[NSString stringWithFormat:@"%@",[CEArray objectAtIndex:indexPath.row]];
        
        
        instrument1=[NSString stringWithFormat:@"%@",[PEArray objectAtIndex:indexPath.row]];
        
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
        
        
        cell1.btn_BID_CE.tag = 0;
        cell1.btn_BID_PE.tag = 1;
        cell1.btn_ASK_CE.tag = 2;
        cell1.btn_ASK_PE.tag = 3;
        
        @try {
           
            if([[[newCEDetailArray objectAtIndex:indexPath.row] objectForKey:instrument]count]>0)
            {
                
                
                cell1.bidQtyCE.text=[NSString stringWithFormat:@"%@",[[[newCEDetailArray objectAtIndex:indexPath.row] objectForKey:instrument] objectForKey:@"inBuy_Qty_0"]];
                cell1.askQtyCE.text=[NSString stringWithFormat:@"%@",[[[newCEDetailArray objectAtIndex:indexPath.row] objectForKey:instrument] objectForKey:@"inSell_Qty_0"]];
                
            
                cell1.bidPriceCE.text=[NSString stringWithFormat:@"%.2f",[[[[newCEDetailArray objectAtIndex:indexPath.row] objectForKey:instrument] objectForKey:@"dbBuy_Price_1"]floatValue]];
                cell1.askPriceCE.text=[NSString stringWithFormat:@"%.2f",[[[[newCEDetailArray objectAtIndex:indexPath.row] objectForKey:instrument] objectForKey:@"dbSell_Price_0"]floatValue]];
                
                
                
            }
            
            else
            {
                cell1.bidQtyCE.text=@"0.00";
                cell1.askQtyCE.text=@"0.00";
                
              
                cell1.bidPriceCE.text=@"0.00";
                cell1.askPriceCE.text=@"0.00";
                
                
                
            }
        }
            @catch (NSException * e) {
                //NSLog(@"Exception: %@", e);
            }
            @finally {
                //NSLog(@"finally");
            }
            
          @try
            {
            if([[[newPEDetailArray objectAtIndex:indexPath.row] objectForKey:instrument1]count]>0)
                
            {
                
                cell1.bidQtyPE.text=[NSString stringWithFormat:@"%@",[[[newPEDetailArray objectAtIndex:indexPath.row] objectForKey:instrument1] objectForKey:@"inBuy_Qty_0"]];
                cell1.askQtyPE.text=[NSString stringWithFormat:@"%@",[[[newPEDetailArray objectAtIndex:indexPath.row] objectForKey:instrument1] objectForKey:@"inSell_Qty_0"]];
                
                
                cell1.bidPricePE.text=[NSString stringWithFormat:@"%.2f",[[[[newPEDetailArray objectAtIndex:indexPath.row] objectForKey:instrument1] objectForKey:@"dbBuy_Price_0"]floatValue]];
                cell1.askPricePE.text=[NSString stringWithFormat:@"%.2f",[[[[newPEDetailArray objectAtIndex:indexPath.row] objectForKey:instrument1] objectForKey:@"dbSell_Price_0"]floatValue]];
                
                
                
            }
                
                else
                {
                    
                    cell1.bidQtyPE.text=@"0.00";
                    cell1.askQtyPE.text=@"0.00";
                    
                    
                    cell1.bidPricePE.text=@"0.00";
                    cell1.askPricePE.text=@"0.00";
                    
                    
                }
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
            
            [cell1.btn_BID_CE addTarget:self action:@selector(performSelectCallPut:) forControlEvents:UIControlEventTouchUpInside];
            //  cell1.btn_BID_CE.layer.borderWidth=0.0f;
            //cell1.btn_BID_CE.layer.borderColor=[[UIColor clearColor]CGColor];
            [cell1.btn_BID_PE addTarget:self action:@selector(performSelectCallPut:) forControlEvents:UIControlEventTouchUpInside];
            // cell1.btn_BID_PE.layer.borderWidth=0.0f;
            //  cell1.btn_BID_PE.layer.borderColor=[[UIColor clearColor]CGColor];
            [cell1.btn_ASK_CE addTarget:self action:@selector(performSelectCallPut:) forControlEvents:UIControlEventTouchUpInside];
            //  cell1.btn_ASK_CE.layer.borderWidth=0.0f;
            //  cell1.btn_ASK_CE.layer.borderColor=[[UIColor clearColor]CGColor];
            [cell1.btn_ASK_PE addTarget:self action:@selector(performSelectCallPut:) forControlEvents:UIControlEventTouchUpInside];
            //   cell1.btn_ASK_PE.layer.borderWidth=0.0f;
            //  cell1.btn_ASK_PE.layer.borderColor=[[UIColor clearColor]CGColor];
            
            
            
            
            //        BOOL tmpBool = [self performIsIndexPathAvaliable:indexPath];
            
            //venk//
            
            NSMutableArray *tmp_array = [[NSMutableArray alloc]initWithArray:[array_isSelected objectAtIndex:indexPath.row]];
            
            
            for(int i =0 ;i<=3;i++)
            {
                if(i == 0)
                {
                    if([[tmp_array objectAtIndex:0] isEqual:@(YES)])
                    {
                        [self performSelectDeselectBorderColor:cell1.btn_BID_CE isSelected:true];
                        
                    }
                    else
                    {
                        [self performSelectDeselectBorderColor:cell1.btn_BID_CE isSelected:false];
                        
                    }
                    
                    
                }else if(i ==1)
                {
                    if([[tmp_array objectAtIndex:1] isEqual:@(YES)])
                    {
                        [self performSelectDeselectBorderColor:cell1.btn_BID_PE isSelected:true];
                        
                    }
                    else
                    {
                        [self performSelectDeselectBorderColor:cell1.btn_BID_PE isSelected:false];
                        
                    }
                    
                }
                else if(i ==2)
                {
                    if([[tmp_array objectAtIndex:2] isEqual:@(YES)])
                    {
                        [self performSelectDeselectBorderColor:cell1.btn_ASK_CE isSelected:true];
                        
                    }
                    else
                    {
                        [self performSelectDeselectBorderColor:cell1.btn_ASK_CE isSelected:false];
                        
                    }
                    
                }
                else if(i == 3)
                {
                    if([[tmp_array objectAtIndex:3] isEqual:@(YES)])
                    {
                        [self performSelectDeselectBorderColor:cell1.btn_ASK_PE isSelected:true];
                        
                    }
                    else
                    {
                        [self performSelectDeselectBorderColor:cell1.btn_ASK_PE isSelected:false];
                        
                    }
                    
                }
                
                
            }
            
            return cell1;
            
            
            
            
            
            
            
       
        
    

    }
    }
    
    }
    
    
    
            else
            {
                @try {

                if([[companyNews objectForKey:@"Table"]count]>0)
                {
                
                MarketWatchNews * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
                
                cell.newsHeading.text = [[[companyNews objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Heading"];;
                cell.newsTime.text =[[[companyNews objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Newsdate"];
                 return cell;
                    
                }
                    
                    else
                    {
//                        NoNewsCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
//                        return cell;
                        
                    }
                    
                }
                @catch (NSException * e) {
                    //NSLog(@"Exception: %@", e);
                }
                @finally {
                    //NSLog(@"finally");
                }
                
                
            }

    
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
   
}

-(void)performSelectDeselectBorderColor:(UIButton *)cellButton isSelected:(BOOL)isSelect
{
    if(!isSelect)
    {
        
        cellButton.layer.borderWidth=0.0f;
        cellButton.layer.borderColor=[[UIColor whiteColor]CGColor];
        [cellButton setBackgroundColor:[UIColor whiteColor]];
        
    }

        
    
    else
    {
        cellButton.layer.borderWidth=2.0f;
        cellButton.layer.borderColor=[[UIColor colorWithRed:(254/255.0) green:(217/255.0) blue:(65/255.0) alpha:1]CGColor];
        [cellButton setBackgroundColor:[UIColor colorWithRed:(254/255.0) green:(217/255.0) blue:(65/255.0) alpha:1]];
      
    }
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Changing button label
//    [self.strikeBtn1 setTitle:[dataArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
//    
//    // Hiding table view
//    dropDwnTableView.hidden=YES;
    @try
    {
    if(tableView==self.depthTableView)
    {
        
    }
    
    else
    {
        @try {
            if([[companyNews objectForKey:@"Table"]count]>0)
            {
            FeedListDetail *view = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedListDetail"];
            
            [self.navigationController pushViewController:view animated:YES];
            
            
            view.descriptionString = [[[companyNews objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Heading"];
            view.timeString = [[[companyNews objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Newsdate"];
            view.detailDesString = [[[companyNews objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Details"];
            }
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
       
    }
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}






- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if(scrollView.tag == 1000)
        return;

    
    float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (bottomEdge >= scrollView.contentSize.height) {
        // we are at the end
        
        
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options:UIViewAnimationCurveEaseOut
                         animations:^(void) {
//                            self.headerView.hidden=NO;
                             
                         }
                         completion:NULL];

    }
    else
    {
       
    }
    
    
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if(scrollView.tag == 1000)
        return;

         [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationCurveEaseOut
                     animations:^(void) {
                                                  
                     }
                     completion:NULL];

}



-(void)onClickingbidBtn1
{
    if (_startStopButtonIsActive2) {
        self.startStopButtonIsActive2 = !self.startStopButtonIsActive2;
        [self.titlesArray removeObjectAtIndex:0];
        self.bidbtn1.layer.borderWidth=0;
        [self.bidbtn1.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
        
        
            
            self.startStopButtonIsActive2 = !self.startStopButtonIsActive2;
            [self.bidbtn1.layer setBorderColor:[[UIColor orangeColor] CGColor]];
            NSString * title =[NSString stringWithFormat:@"%@",self.bidbtn1.titleLabel];
            self.bidbtn1.layer.borderWidth=4;
            
            [self.titlesArray addObject:title];
            
            
        }
    }



-(void)onClickingbidBtn2
{
    if (_startStopButtonIsActive3) {
        self.startStopButtonIsActive3 = !self.startStopButtonIsActive3;
        [self.titlesArray removeObjectAtIndex:0];
        self.bidBtn2.layer.borderWidth=0;
        [self.bidBtn2.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
   else if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
    
    
        
        self.startStopButtonIsActive3 = !self.startStopButtonIsActive3;
         [self.bidBtn2.layer setBorderColor:[[UIColor orangeColor] CGColor]];
        NSString * title =[NSString stringWithFormat:@"%@",self.bidBtn2.titleLabel];
        self.bidBtn2.layer.borderWidth=4;
        
        [self.titlesArray addObject:title];
        
        
    }
    }


-(void)onClickingbidBtn3
{
    if (_startStopButtonIsActive4) {
        self.startStopButtonIsActive4 = !self.startStopButtonIsActive4;
        [self.titlesArray removeObjectAtIndex:0];
        self.bidBtn3.layer.borderWidth=0;
        [self.bidBtn3.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
        
        
            self.startStopButtonIsActive4 = !self.startStopButtonIsActive4;
            [self.bidBtn3.layer setBorderColor:[[UIColor orangeColor] CGColor]];
            NSString * title =[NSString stringWithFormat:@"%@",self.bidBtn3.titleLabel];
            self.bidBtn3.layer.borderWidth=4;
            
            [self.titlesArray addObject:title];
            
            
        }
    }



-(void)onClickingbidBtn4
{
    if (_startStopButtonIsActive5) {
        self.startStopButtonIsActive5 = !self.startStopButtonIsActive5;
        [self.titlesArray removeObjectAtIndex:0];
        self.bidBtn4.layer.borderWidth=0;
        [self.bidBtn4.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
   else if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
       
        
            self.startStopButtonIsActive5 = !self.startStopButtonIsActive5;
            [self.bidBtn4.layer setBorderColor:[[UIColor orangeColor] CGColor]];
            self.bidBtn4.layer.borderWidth=4;
            NSString * title =[NSString stringWithFormat:@"%@",self.bidBtn4.titleLabel];
            
            [self.titlesArray addObject:title];
            
            
        }
    }





-(void)onClickingbidBtn5
{
    if (_startStopButtonIsActive6) {
        self.startStopButtonIsActive6 = !self.startStopButtonIsActive6;
        [self.titlesArray removeObjectAtIndex:0];
        self.bidBtn5.layer.borderWidth=0;
        [self.bidBtn5.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
        
       
            
            self.startStopButtonIsActive6 = !self.startStopButtonIsActive6;
            [self.bidBtn5.layer setBorderColor:[[UIColor orangeColor] CGColor]];
            self.bidBtn5.layer.borderWidth=4;
            NSString * title =[NSString stringWithFormat:@"%@",self.bidBtn5.titleLabel];
            
            [self.titlesArray addObject:title];
            
            
        }
    }


-(void)onClickingbidBtn6
{
    if (_startStopButtonIsActive7) {
        self.startStopButtonIsActive7 = !self.startStopButtonIsActive7;
        [self.titlesArray removeObjectAtIndex:0];
        self.bidBtn6.layer.borderWidth=0;
        [self.bidBtn6.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
   else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
        
       
            
            self.startStopButtonIsActive7 = !self.startStopButtonIsActive7;
            [self.bidBtn6.layer setBorderColor:[[UIColor orangeColor] CGColor]];
            self.bidBtn6.layer.borderWidth=4;
            NSString * title =[NSString stringWithFormat:@"%@",self.bidBtn6.titleLabel];
            
            [self.titlesArray addObject:title];
            
            
        }
    }



-(void)onClickingbidBtn7
{
    if (_startStopButtonIsActive8) {
        self.startStopButtonIsActive8 = !self.startStopButtonIsActive8;
        [self.titlesArray removeObjectAtIndex:0];
        self.bidBtn7.layer.borderWidth=0;
        [self.bidBtn7.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
        
       
            
            self.startStopButtonIsActive8 = !self.startStopButtonIsActive8;
            [self.bidBtn7.layer setBorderColor:[[UIColor orangeColor] CGColor]];
            self.bidBtn7.layer.borderWidth=4;
            NSString * title =[NSString stringWithFormat:@"%@",self.bidBtn7.titleLabel];
            
            [self.titlesArray addObject:title];
            
            
        }
    }



-(void)onClickingbidBtn8
{
    if (_startStopButtonIsActive9) {
        self.startStopButtonIsActive9 = !self.startStopButtonIsActive9;
        [self.titlesArray removeObjectAtIndex:0];
        self.bidBtn8.layer.borderWidth=0;
        [self.bidBtn8.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
        
        
            self.startStopButtonIsActive9 = !self.startStopButtonIsActive9;
            [self.bidBtn8.layer setBorderColor:[[UIColor orangeColor] CGColor]];
            self.bidBtn8.layer.borderWidth=4;
            NSString * title =[NSString stringWithFormat:@"%@",self.bidBtn8.titleLabel];
            
            [self.titlesArray addObject:title];
            
            
        }
    }



-(void)onClickingbidBtn9
{
    if (_startStopButtonIsActive10) {
        self.startStopButtonIsActive10 = !self.startStopButtonIsActive10;
        [self.titlesArray removeObjectAtIndex:0];
        self.bidBtn9.layer.borderWidth=0;
        [self.bidBtn9.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
        
        
            
            self.startStopButtonIsActive10 = !self.startStopButtonIsActive10;
            [self.bidBtn9.layer setBorderColor:[[UIColor orangeColor] CGColor]];
            self.bidBtn9.layer.borderWidth=4;
            NSString * title =[NSString stringWithFormat:@"%@",self.bidBtn9.titleLabel];
            
            [self.titlesArray addObject:title];
            
            
        }
    }



-(void)onClickingbidBtn10
{
    if (_startStopButtonIsActive11) {
        self.startStopButtonIsActive11 = !self.startStopButtonIsActive11;
        [self.titlesArray removeObjectAtIndex:0];
        self.bidBtn10.layer.borderWidth=0;
        [self.bidBtn10.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
   else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )  {
       
       
            
            self.startStopButtonIsActive11 = !self.startStopButtonIsActive11;
            [self.bidBtn10.layer setBorderColor:[[UIColor orangeColor] CGColor]];
            self.bidBtn10.layer.borderWidth=4;
            NSString * title =[NSString stringWithFormat:@"%@",self.bidBtn10.titleLabel];
            
            [self.titlesArray addObject:title];
            
            
        }
    }


-(void)onClickingaskBtn1
{
    if (_startStopButtonIsActive12) {
        self.startStopButtonIsActive12 = !self.startStopButtonIsActive12;
        [self.titlesArray removeObjectAtIndex:0];
        self.askBtn1.layer.borderWidth=0;
        [self.askBtn1.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
        
        
            self.startStopButtonIsActive12 = !self.startStopButtonIsActive12;
            [self.askBtn1.layer setBorderColor:[[UIColor orangeColor] CGColor]];
            self.askBtn1.layer.borderWidth=4;
            NSString * title =[NSString stringWithFormat:@"%@",self.askBtn1.titleLabel];
            
            [self.titlesArray addObject:title];
            
            
        }
    }


-(void)onClickingaskBtn2
{
    if (_startStopButtonIsActive13) {
        self.startStopButtonIsActive13 = !self.startStopButtonIsActive13;
        [self.titlesArray removeObjectAtIndex:0];
        self.askBtn2.layer.borderWidth=0;
        [self.askBtn2.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
   else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
        
        
            self.startStopButtonIsActive13 = !self.startStopButtonIsActive13;
            [self.askBtn2.layer setBorderColor:[[UIColor orangeColor] CGColor]];
            self.askBtn2.layer.borderWidth=4;
            NSString * title =[NSString stringWithFormat:@"%@",self.askBtn2.titleLabel];
            
            [self.titlesArray addObject:title];
            
            
        }
    }


-(void)onClickingaskBtn3
{
    if (_startStopButtonIsActive14) {
        self.startStopButtonIsActive14 = !self.startStopButtonIsActive14;
        [self.titlesArray removeObjectAtIndex:0];
        self.askBtn3.layer.borderWidth=0;
        [self.askBtn3.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }

    else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )   {
        
            
            self.startStopButtonIsActive14 = !self.startStopButtonIsActive14;
            [self.askBtn3.layer setBorderColor:[[UIColor orangeColor] CGColor]];
            self.askBtn3.layer.borderWidth=4;
            NSString * title =[NSString stringWithFormat:@"%@",self.askBtn3.titleLabel];
            
            [self.titlesArray addObject:title];
            
            
        }
    }


-(void)onClickingaskBtn4
{
    if (_startStopButtonIsActive15) {
        self.startStopButtonIsActive15 = !self.startStopButtonIsActive15;
        [self.titlesArray removeObjectAtIndex:0];
        self.askBtn4.layer.borderWidth=0;
        [self.askBtn4.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
 else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
        
        
            self.startStopButtonIsActive15 = !self.startStopButtonIsActive15;
            [self.askBtn4.layer setBorderColor:[[UIColor orangeColor] CGColor]];
            self.askBtn4.layer.borderWidth=4;
            NSString * title =[NSString stringWithFormat:@"%@",self.askBtn4.titleLabel];
            
            [self.titlesArray addObject:title];
            
            
        }
    }


-(void)onClickingaskBtn5
{
    if (_startStopButtonIsActive16) {
        self.startStopButtonIsActive16 = !self.startStopButtonIsActive16;
        [self.titlesArray removeObjectAtIndex:0];
        self.askBtn5.layer.borderWidth=0;
        [self.askBtn5.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
   else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )   {
        
        
            
            self.startStopButtonIsActive16 = !self.startStopButtonIsActive16;
            [self.askBtn5.layer setBorderColor:[[UIColor orangeColor] CGColor]];
            self.askBtn5.layer.borderWidth=4;
            NSString * title =[NSString stringWithFormat:@"%@",self.askBtn5.titleLabel];
            
            [self.titlesArray addObject:title];
            
            
        }
    }


-(void)onClickingaskBtn6
{
    if (_startStopButtonIsActive17) {
        self.startStopButtonIsActive17 = !self.startStopButtonIsActive17;
        [self.titlesArray removeObjectAtIndex:0];
        self.askBtn6.layer.borderWidth=0;
        [self.askBtn6.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
   else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
        
       
            
            self.startStopButtonIsActive17 = !self.startStopButtonIsActive17;
            [self.askBtn6.layer setBorderColor:[[UIColor orangeColor] CGColor]];
            self.askBtn6.layer.borderWidth=4;
            NSString * title =[NSString stringWithFormat:@"%@",self.askBtn6.titleLabel];
            
            [self.titlesArray addObject:title];
            
            
        }
    }


-(void)onClickingaskBtn7
{if (_startStopButtonIsActive18) {
    self.startStopButtonIsActive18 = !self.startStopButtonIsActive18;
    [self.titlesArray removeObjectAtIndex:0];
    self.askBtn7.layer.borderWidth=0;
    [self.askBtn7.layer setBorderColor:[[UIColor whiteColor] CGColor]];
}
   else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )
    {
        
        
            self.startStopButtonIsActive18 = !self.startStopButtonIsActive18;
            [self.askBtn7.layer setBorderColor:[[UIColor orangeColor] CGColor]];
            self.askBtn7.layer.borderWidth=4;
            NSString * title =[NSString stringWithFormat:@"%@",self.askBtn7.titleLabel];
            
            [self.titlesArray addObject:title];
            
            
        }
    }


-(void)onClickingaskBtn8
{
    if (_startStopButtonIsActive19) {
        self.startStopButtonIsActive19 = !self.startStopButtonIsActive19;
        [self.titlesArray removeObjectAtIndex:0];
        self.askBtn8.layer.borderWidth=0;
        [self.askBtn8.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
   else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )    {
       
       
            self.startStopButtonIsActive19 = !self.startStopButtonIsActive19;
            [self.askBtn8.layer setBorderColor:[[UIColor orangeColor] CGColor]];
            self.askBtn8.layer.borderWidth=4;
            NSString * title =[NSString stringWithFormat:@"%@",self.askBtn8.titleLabel];
            
            [self.titlesArray addObject:title];
            
            
        }
    }


-(void)onClickingaskBtn9
{
    if (_startStopButtonIsActive20) {
        self.startStopButtonIsActive20 = !self.startStopButtonIsActive20;
        [self.titlesArray removeObjectAtIndex:0];
        self.askBtn9.layer.borderWidth=0;
        [self.askBtn9.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )  {
       
       
            
            self.startStopButtonIsActive20 = !self.startStopButtonIsActive20;
            [self.askBtn9.layer setBorderColor:[[UIColor orangeColor] CGColor]];
            self.askBtn9.layer.borderWidth=4;
            NSString * title =[NSString stringWithFormat:@"%@",self.askBtn9.titleLabel];
            
            [self.titlesArray addObject:title];
            
            
        }
    }


-(void)onClickingaskBtn10
{
    if (_startStopButtonIsActive21) {
        self.startStopButtonIsActive21 = !self.startStopButtonIsActive21;
        [self.titlesArray removeObjectAtIndex:0];
        self.askBtn10.layer.borderWidth=0;
        [self.askBtn10.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    }
   else  if(self.titlesArray.count==0 || (self.titlesArray.count>=1 && self.titlesArray.count<4) )   {
        
       
            
            self.startStopButtonIsActive21 = !self.startStopButtonIsActive21;
            [self.askBtn10.layer setBorderColor:[[UIColor orangeColor] CGColor]];
            self.askBtn10.layer.borderWidth=4;
            NSString * title =[NSString stringWithFormat:@"%@",self.askBtn10.titleLabel];
            
            [self.titlesArray addObject:title];
            
            
        }
    }




//drop down text field//
-(void)outFocusTextField
{
    [self addBorderToButton:self.strikeBtn1];
}

- (void)addBorderToButton:(UIButton *)button
{
    button.titleLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:12];
    [button setTitleColor:RGB(0,0,0) forState:UIControlStateNormal];
    button.backgroundColor = RGB(255,255,255);
    
    CALayer *border = [CALayer layer];
    border.backgroundColor = RGB(192,192,192).CGColor;
    button.backgroundColor = [UIColor clearColor];
    border.frame = CGRectMake(0.0f, button.frame.size.height - 1, button.frame.size.width, 1.0f);
    [button.layer addSublayer:border];
}


-(void)strikeDropDown
{
    if (dropFlag == false)
    {
        [dropDwnTableView setHidden:NO];
        dropFlag=true;
    }
    else
    {
        [dropDwnTableView setHidden:YES];
        dropFlag=false;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


//websockets//


- (void)webSocketDidOpen:(PSWebSocket *)webSocket {
    @try
    {
    //NSLog(@"The websocket handshake completed and is now open!");
    
    
    
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
    {
    
    //     instrumentTokens=[[NSMutableArray alloc]init];
    
    
    
    //   [instrumentTokens addObject:[NSNumber numberWithInt:11547906]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:4911105]];
    //   [instrumentTokens addObject:[NSNumber numberWithInt:11508226]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:11535618]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:24026370]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:12344066]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:4954113]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:20517634]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:20517890]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:217138949]];
    //NSLog(@"%@",instrumentToken);
    if(instrumentToken!=0)
    {
        
        NSDictionary * subscribeDict=@{@"a": @"subscribe", @"v":instrumentToken};
        
        
        NSData *json;
        
        NSError * error;
        
        
        
        
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:subscribeDict])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:subscribeDict options:NSJSONWritingPrettyPrinted error:&error];
            
            // If no errors, let's view the JSON
            if (json != nil && error == nil)
            {
                string1 = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                
                //            delegate2.message=[NSString stringWithFormat:@"%@",str];
                //
                
                //NSLog(@"JSON: %@",string1);
                
            }
        }
        
        
        [self.socket send:string1];
        
        
        
        
        //NSLog(@"%@",instrumentToken);
        
        NSArray * mode=@[@"full",instrumentToken];
        
        subscribeDict=@{@"a": @"mode", @"v": mode};
        
        
        
        //     Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:subscribeDict])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:subscribeDict options:NSJSONWritingPrettyPrinted error:&error];
            
            
            // If no errors, let's view the JSON
            if (json != nil && error == nil)
            {
                _message = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                
                //            delegate2.message=[NSString stringWithFormat:@"%@",str];
                
                //NSLog(@"JSON: %@",_message);
                
            }
        }
        [self.socket send:_message];
        
    }
        
        
    }
    
    else if ([delegate2.brokerNameStr isEqualToString:@"Upstox"])
        
    {
//            [self subMethod];
    }
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}




- (void)webSocket:(PSWebSocket *)webSocket didReceiveMessage:(id)message {
    @try {
        //NSLog(@"The websocket received a message: %@",message);
        
        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
        {
        
        NSData * data=[NSKeyedArchiver archivedDataWithRootObject:message];
        
        
        if(data.length > 247)
        {
            
            
            //NSLog(@"The websocket received a message: %@",message);
            
            
            id packets = [message subdataWithRange:NSMakeRange(0,2)];
            packetsNumber = CFSwapInt16BigToHost(*(int16_t*)([packets bytes]));
            //NSLog(@" number of packets %i",packetsNumber);
            
            int startingPosition = 2;
            
            
//            if([instrumentTokensDict allKeys])
//                [instrumentTokensDict removeAllObjects];
            
            for( int i =0; i< packetsNumber ; i++)
            {
                
                
                NSData * packetsLengthData = [message subdataWithRange:NSMakeRange(startingPosition,2)];
                packetsLength = CFSwapInt16BigToHost(*(int16_t*)([packetsLengthData bytes]));
                startingPosition = startingPosition + 2;
                
                id packetQuote = [message subdataWithRange:NSMakeRange(startingPosition,packetsLength)];
                
                
                id instrumentTokenData = [packetQuote subdataWithRange:NSMakeRange(0, 4)];
                int32_t instrumentTokenValue = CFSwapInt32BigToHost(*(int32_t*)([instrumentTokenData bytes]));
                
                id lastPrice = [packetQuote subdataWithRange:NSMakeRange(4,4)];
                int32_t lastPriceValue = CFSwapInt32BigToHost(*(int32_t*)([lastPrice bytes]));
                
                id BID = [packetQuote subdataWithRange:NSMakeRange(20,4)];
                int32_t BIDValue = CFSwapInt32BigToHost(*(int32_t*)([BID bytes]));
                
                
                
                
                id ASK = [packetQuote subdataWithRange:NSMakeRange(24,4)];
                int32_t ASLValue = CFSwapInt32BigToHost(*(int32_t*)([ASK bytes]));
                
                
                
                
                id open = [packetQuote subdataWithRange:NSMakeRange(28,4)];
                int32_t openValue = CFSwapInt32BigToHost(*(int32_t*)([open bytes]));
                
                
                
                
                id high = [packetQuote subdataWithRange:NSMakeRange(32,4)];
                int32_t highValue = CFSwapInt32BigToHost(*(int32_t*)([high bytes]));
                
                
                
                
                id low = [packetQuote subdataWithRange:NSMakeRange(36,4)];
                int32_t lowValue = CFSwapInt32BigToHost(*(int32_t*)([low bytes]));
                
                
                
                
                
                id volume = [packetQuote subdataWithRange:NSMakeRange(16,4)];
                int32_t volumeValue = CFSwapInt32BigToHost(*(int32_t*)([volume bytes]));
                
                
                float volumeFloatValue = (float)volumeValue;
                
                float shortenedAmount = volumeFloatValue;
                NSString *suffix = @"";
                if(volumeFloatValue >= 10000000.0f) {
                    suffix = @"Cr";
                    shortenedAmount /= 10000000.0f;
                }
                else if(volumeFloatValue >= 1000000.0f) {
                    suffix = @"L";
                    shortenedAmount /= 100000.0f;
                }
                else if(volumeFloatValue >= 100000.0f) {
                    suffix = @"L";
                    shortenedAmount /= 100000.0f;
                }
                else if(volumeFloatValue >= 1000.0f) {
                    suffix = @"K";
                    shortenedAmount /= 1000.0f;
                }
                
                NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
                [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
                [numberFormatter setCurrencySymbol:@""];
                NSString *numberAsString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:shortenedAmount]];
                
                NSString *requiredString = [NSString stringWithFormat:@"%@%@", numberAsString, suffix];
                
                
                
                
                id closingPrice = [packetQuote subdataWithRange:NSMakeRange(40,4)];
                int32_t closePriceValue = CFSwapInt32BigToHost(*(int32_t*)([closingPrice bytes]));
                
                
                id marketDepthBID = [packetQuote subdataWithRange:NSMakeRange(64,60)];
                int32_t marketDepthValueBID = CFSwapInt32BigToHost(*(int32_t*)([marketDepthBID bytes]));
                
                //NSLog(@"%d",marketDepthValueBID);
                
                id marketDepthBIDQuantity = [marketDepthBID subdataWithRange:NSMakeRange(0,4)];
                int32_t marketDepthValueBIDQuantity = CFSwapInt32BigToHost(*(int32_t*)([marketDepthBIDQuantity bytes]));
                
                //NSLog(@"%i",marketDepthValueBIDQuantity);
                
                float shortenedAmount1 = marketDepthValueBIDQuantity;
                NSString *suffix1 = @"";
                if(marketDepthValueBIDQuantity >= 10000000.0f) {
                    suffix1 = @"Cr";
                    shortenedAmount1 /= 10000000.0f;
                }
                else if(marketDepthValueBIDQuantity >= 1000000.0f) {
                    suffix1 = @"L";
                    shortenedAmount1 /= 100000.0f;
                }
                else if(marketDepthValueBIDQuantity >= 100000.0f) {
                    suffix1 = @"L";
                    shortenedAmount1 /= 100000.0f;
                }
                else if(marketDepthValueBIDQuantity >= 1000.0f) {
                    suffix1 = @"K";
                    shortenedAmount1 /= 1000.0f;
                }
                
                NSNumberFormatter *numberFormatter1 = [[NSNumberFormatter alloc] init];
                [numberFormatter1 setNumberStyle: NSNumberFormatterCurrencyStyle];
                [numberFormatter1 setCurrencySymbol:@""];
                NSString *numberAsString1 = [numberFormatter1 stringFromNumber:[NSNumber numberWithFloat:shortenedAmount1]];
                
                NSString *requiredString1 = [NSString stringWithFormat:@"%@%@", numberAsString1, suffix1];
                
                
                
                
                
                id marketDepthBIDPrice = [marketDepthBID subdataWithRange:NSMakeRange(4,4)];
                int32_t marketDepthValueBIDPrice = CFSwapInt32BigToHost(*(int32_t*)([marketDepthBIDPrice bytes]));
                
                
                
                
                
                
                id marketDepthASK = [packetQuote subdataWithRange:NSMakeRange(124,60)];
                int32_t marketDepthValueASK = CFSwapInt32BigToHost(*(int32_t*)([marketDepthASK bytes]));
                
                //NSLog(@"%d",marketDepthValueASK);
                
                id marketDepthASKQuantity = [marketDepthASK subdataWithRange:NSMakeRange(0,4)];
                int32_t marketDepthValueASKQuantity = CFSwapInt32BigToHost(*(int32_t*)([marketDepthASKQuantity bytes]));
                
                
                //NSLog(@"%i",marketDepthValueASKQuantity);
                
                float shortenedAmount2 = marketDepthValueASKQuantity;
                NSString *suffix2 = @"";
                if(marketDepthValueASKQuantity >= 10000000.0f) {
                    suffix2 = @"Cr";
                    shortenedAmount2 /= 10000000.0f;
                }
                else if(marketDepthValueASKQuantity >= 1000000.0f) {
                    suffix2 = @"L";
                    shortenedAmount2 /= 100000.0f;
                }
                else if(marketDepthValueASKQuantity >= 100000.0f) {
                    suffix2 = @"L";
                    shortenedAmount2 /= 100000.0f;
                }
                else if(marketDepthValueASKQuantity >= 1000.0f) {
                    suffix2 = @"K";
                    shortenedAmount2 /= 1000.0f;
                }
                
                NSNumberFormatter *numberFormatter2 = [[NSNumberFormatter alloc] init];
                [numberFormatter2 setNumberStyle: NSNumberFormatterCurrencyStyle];
                [numberFormatter2 setCurrencySymbol:@""];
                NSString *numberAsString2 = [numberFormatter2 stringFromNumber:[NSNumber numberWithFloat:shortenedAmount2]];
                
                NSString *requiredString2 = [NSString stringWithFormat:@"%@%@", numberAsString2, suffix2];
                
                
                
                
                
                id marketDepthASKPrice = [marketDepthASK subdataWithRange:NSMakeRange(4,4)];
                int32_t marketDepthValueASKPrice = CFSwapInt32BigToHost(*(int32_t*)([marketDepthASKPrice bytes]));
                
                
                
                
                
                //                id marketDepth = [packetQuote subdataWithRange:NSMakeRange(44,120)];
                //                int32_t marketDepthValue = CFSwapInt32BigToHost(*(int32_t*)([marketDepth bytes]));
                //
                //                //NSLog(@"%@",marketDepth);
                
                if([delegate2.exchaneLblStr containsString:@"CDS"])
                {
                    
                    lastPriceFlaotValue = (float)lastPriceValue/10000000;
                    
                    closePriceFlaotValue = (float)closePriceValue/10000000;
                    
                    //NSLog(@"CLOSE %f",closePriceFlaotValue);
                    
                    changeFloatValue = lastPriceFlaotValue-closePriceFlaotValue;
                    
                   changeFloatPercentageValue = ((lastPriceFlaotValue-closePriceFlaotValue) *100/closePriceFlaotValue);
                    BIDFloatValue = (float)BIDValue/10000000;
                    ASKFloatValue = (float)ASLValue/10000000;
                    openFloatValue = (float)openValue/10000000;
                    highFloatValue = (float)highValue/10000000;
                    lowFloatValue = (float)lowValue/10000000;
                    marketDepthValueBIDPrice1 = (float)marketDepthValueBIDPrice/10000000;
                    marketDepthValueASKPrice1 = (float)marketDepthValueASKPrice/10000000;
                    
                    
                }
                else
                {
                    lastPriceFlaotValue = (float)lastPriceValue/100;
                    
                    closePriceFlaotValue = (float)closePriceValue/100;
                    
                    //NSLog(@"CLOSE %f",closePriceFlaotValue);
                    
                    changeFloatValue = lastPriceFlaotValue-closePriceFlaotValue;
                    
                    changeFloatPercentageValue = ((lastPriceFlaotValue-closePriceFlaotValue) *100/closePriceFlaotValue);
                    
                    
                    
                    BIDFloatValue = (float)BIDValue/100;
                    ASKFloatValue = (float)ASLValue/100;
                    openFloatValue = (float)openValue/100;
                    highFloatValue = (float)highValue/100;
                    lowFloatValue = (float)lowValue/100;
                    marketDepthValueBIDPrice1 = (float)marketDepthValueBIDPrice/100;
                    marketDepthValueASKPrice1 = (float)marketDepthValueASKPrice/100;
                    
                    
                }
                startingPosition = startingPosition + packetsLength;
                
                
                @autoreleasepool {
                    
                    NSMutableDictionary *tmpDict = [[NSMutableDictionary alloc]init];
                    [tmpDict setValue:[NSString stringWithFormat:@"%d",instrumentTokenValue] forKey:@"InstrumentToken"];
                    
                    NSString *tmpInstrument = [NSString stringWithFormat:@"%d",instrumentTokenValue];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%.2f",closePriceFlaotValue] forKey:@"ClosePriceValue"];
                    
                    if([delegate2.exchaneLblStr containsString:@"CDS"])
                    {
                    [tmpDict setValue:[NSString stringWithFormat:@"%.4f",lastPriceFlaotValue] forKey:@"LastPriceValue"];
                        
                        [tmpDict setValue:[NSString stringWithFormat:@"%.4f",openFloatValue] forKey:@"OpenValue"];
                        
                        [tmpDict setValue:[NSString stringWithFormat:@"%.4f",highFloatValue] forKey:@"HighValue"];
                        
                        [tmpDict setValue:[NSString stringWithFormat:@"%.4f",lowFloatValue] forKey:@"LowValue"];
                        
                        [tmpDict setValue:[NSString stringWithFormat:@"%@",requiredString] forKey:@"VolumeValue"];
                        
                        [tmpDict setValue:[NSString stringWithFormat:@"%.4f",marketDepthValueBIDPrice1] forKey:@"BIDPrice"];
                        
                        
                        [tmpDict setValue:[NSString stringWithFormat:@"%.4f",marketDepthValueASKPrice1] forKey:@"ASKPrice"];
                        
                        [tmpDict setValue:[NSString stringWithFormat:@"%.2f",changeFloatValue] forKey:@"ChangeValue"];
                        
                        [tmpDict setValue:[NSString stringWithFormat:@"%.2f",changeFloatPercentageValue] forKey:@"ChangePercentageValue"];
                        //NSLog(@"%@",tmpDict);
                        
                        [tmpDict setValue:[NSString stringWithFormat:@"%@",requiredString1] forKey:@"BIDValue"];
                        
                        [tmpDict setValue:[NSString stringWithFormat:@"%@",requiredString2] forKey:@"ASKValue"];
                        
                    }
                    
                    else
                    {
                        [tmpDict setValue:[NSString stringWithFormat:@"%.2f",changeFloatValue] forKey:@"ChangeValue"];
                        
                        [tmpDict setValue:[NSString stringWithFormat:@"%.2f",changeFloatPercentageValue] forKey:@"ChangePercentageValue"];
                        //NSLog(@"%@",tmpDict);
                        
                        [tmpDict setValue:[NSString stringWithFormat:@"%@",requiredString1] forKey:@"BIDValue"];
                        
                        [tmpDict setValue:[NSString stringWithFormat:@"%@",requiredString2] forKey:@"ASKValue"];
                        
                        
                        
                        
                        [tmpDict setValue:[NSString stringWithFormat:@"%.2f",lastPriceFlaotValue] forKey:@"LastPriceValue"];
                        
                        [tmpDict setValue:[NSString stringWithFormat:@"%.2f",openFloatValue] forKey:@"OpenValue"];
                        
                        [tmpDict setValue:[NSString stringWithFormat:@"%.2f",highFloatValue] forKey:@"HighValue"];
                        
                        [tmpDict setValue:[NSString stringWithFormat:@"%.2f",lowFloatValue] forKey:@"LowValue"];
                        
                        [tmpDict setValue:[NSString stringWithFormat:@"%@",requiredString] forKey:@"VolumeValue"];
                        
                        if([delegate2.exchaneLblStr containsString:@"CDS"])
                        {
                            [tmpDict setValue:[NSString stringWithFormat:@"%.4f",marketDepthValueBIDPrice1] forKey:@"BIDPrice"];
                            
                            
                            [tmpDict setValue:[NSString stringWithFormat:@"%.4f",marketDepthValueASKPrice1] forKey:@"ASKPrice"];
                        }
                        
                        else
                        {
                            [tmpDict setValue:[NSString stringWithFormat:@"%.2f",marketDepthValueBIDPrice1] forKey:@"BIDPrice"];
                            
                            
                            [tmpDict setValue:[NSString stringWithFormat:@"%.2f",marketDepthValueASKPrice1] forKey:@"ASKPrice"];
                        }
                        
                        
                        
                    }
                    
                        
                    
                   
                    
                    
                    
                  
                    
                   
                    
                    if([instrumentTokensDict allKeys])
                    {
                         NSArray * arrayKeys=[instrumentTokensDict allKeys];
                    
                    if([arrayKeys containsObject:tmpInstrument])
                    {
                        if(tmpDict.count>0)
                        {
                            
                            [instrumentTokensDict removeObjectForKey:tmpInstrument];
                            
                           [instrumentTokensDict setValue:tmpDict forKeyPath:tmpInstrument];
                        }
                        
                        else
                        {
                            
                            
                        }
                        
                    }
                        
                        else
                        {
                            [instrumentTokensDict setValue:tmpDict forKeyPath:tmpInstrument];
                            
                        }
                        
                        
                    }
                    
                    else
                    {
                          [instrumentTokensDict setValue:tmpDict forKeyPath:tmpInstrument];
                        
                    }
                    
                    //NSLog(@"%@",instrumentTokensDict);
                    
                    
                    
                    //  [shareListArray addObject:tmpDict];
                    
                }
                
            }
            
            
        }
            
        }
        
        else if ([delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
            NSString *detailsString = [[NSString alloc] initWithData:message encoding:NSASCIIStringEncoding];
            
            
            
            //NSLog(@"%@",detailsString);
            
            NSArray * completeArray=[[NSArray alloc]init];
            
            completeArray= [detailsString componentsSeparatedByString:@";"] ;
            //NSLog(@"%@",completeArray);
            
            
            
            for (int i=0; i<completeArray.count; i++) {
                
                
                
                
                NSMutableArray * individualCompanyArray=[[NSMutableArray alloc]init];
                NSString * innerStr=[NSString stringWithFormat:@"%@",[completeArray objectAtIndex:i]];
                
                individualCompanyArray=[[innerStr componentsSeparatedByString:@","] mutableCopy];
                
                
                NSString * symbolName=[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:2]];
                
                if(individualCompanyArray.count>5)
                {
                    
                    
                    if([upStoxEQSymbolArray containsObject:symbolName]||[upStoxFUTSymbolArray containsObject:symbolName])
                    {
                        
                        NSUInteger index;
                        NSString * token;
                       
                            
                           if([upStoxEQSymbolArray containsObject:symbolName])
                           {
                            index=[upStoxEQSymbolArray indexOfObject:symbolName];
                               
                               token=[upStoxEQSymbolArrayToken objectAtIndex:index];
                           }
                        
                         else if([upStoxFUTSymbolArray containsObject:symbolName])
                         {
                             
                             index=[upStoxFUTSymbolArray indexOfObject:symbolName];
                             
                             token=[upStoxFUTSymbolArrayToken objectAtIndex:index];
                         }
                            
                      
                        
                        
                        
                        @autoreleasepool {
                            
                            
                            
                            NSString * instrument1=[NSString stringWithFormat:@"%@",token];
                            
                            NSMutableDictionary *tmpDict = [[NSMutableDictionary alloc]init];
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",instrument1] forKey:@"InstrumentToken"];
                            
                            NSString *tmpInstrument = [NSString stringWithFormat:@"%@",instrument1];
                            
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:4]] forKey:@"ClosePriceValue"];
                            
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:3]] forKey:@"LastPriceValue"];
                            
                            
                            float close=[[individualCompanyArray objectAtIndex:7] floatValue];
                            float ltp=[[individualCompanyArray objectAtIndex:3] floatValue];
                            
                            float change=ltp-close;
                            
                            
                            [tmpDict setValue:[NSString stringWithFormat:@"%.2f",change] forKey:@"ChangeValue"];
                            
                            float changePer = ((ltp-close) *100/close);
                            
                            [tmpDict setValue:[NSString stringWithFormat:@"%.2f",changePer] forKey:@"ChangePercentageValue"];
                            
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:18]] forKey:@"BIDValue"];
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:21]] forKey:@"BIDValue1"];
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:24]] forKey:@"BIDValue2"];
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:27]] forKey:@"BIDValue3"];
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:30]] forKey:@"BIDValue4"];
                            
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:33]] forKey:@"ASKValue"];
                            
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:36]] forKey:@"ASKValue1"];
                            
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:39]] forKey:@"ASKValue2"];
                            
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:42]] forKey:@"ASKValue3"];
                            
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:45]] forKey:@"ASKValue4"];
                            
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:5]] forKey:@"OpenValue"];
                            
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:6]] forKey:@"HighValue"];
                            
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:7]] forKey:@"LowValue"];
                            
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:8]] forKey:@"VolumeValue"];
                            
                            
                            
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:19]] forKey:@"BIDPrice"];
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:22]] forKey:@"BIDPrice2"];
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:25]] forKey:@"BIDPrice3"];
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:28]] forKey:@"BIDPrice4"];
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:31]] forKey:@"BIDPrice5"];
                            
                            
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:34]] forKey:@"ASKPrice"];
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:37]] forKey:@"ASKPrice2"];
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:40]] forKey:@"ASKPrice3"];
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:43]] forKey:@"ASKPrice4"];
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:46]] forKey:@"ASKPrice5"];
                            
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:12]] forKey:@"TotalBid"];
                            [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:13]] forKey:@"TotalAsk"];
                            
                            
                           
                            
                            [instrumentTokensDict setValue:tmpDict forKeyPath:tmpInstrument];
                            
                            //NSLog(@"%@",instrumentTokensDict);
                            
                            
                            
                            
                            
                            //  [shareListArray addObject:tmpDict];
                            
                        }
                        
                    }
                }
            }
        }
                        
           if(instrumentTokensDict.count>0)
           {
            if(allKeysList.count>0)
            {
                [allKeysList removeAllObjects];
            }
            //NSLog(@"%@",instrumentTokensDict);
            allKeysList = [[instrumentTokensDict allKeys]mutableCopy];
            
            //NSLog(@"%@",allKeysList);
            
            NSMutableDictionary *tmp = [[NSMutableDictionary alloc]initWithDictionary:[instrumentTokensDict valueForKey:[allKeysList objectAtIndex:0]]];
            
            //NSLog(@"%@",tmp);
            
            
            sortedArray = [[NSMutableArray alloc]init];
            
            //NSLog(@"%@",instrumentToken);
            
            for( int i =0 ;i <[instrumentToken count];i++)
            {
                
                
                NSNumber *string_InstrumentToken = [instrumentToken objectAtIndex:i];
                
                NSString *string_InstrumentTokenStr=[string_InstrumentToken stringValue];
                
                //NSLog(@"%@",string_InstrumentTokenStr);
                
                
                if([allKeysList containsObject:string_InstrumentTokenStr])
                {
                    @autoreleasepool {
                        
                        
                        NSMutableDictionary *tmp_Dict = [[NSMutableDictionary alloc]init];
                        
                        [tmp_Dict setObject:[instrumentTokensDict valueForKey:string_InstrumentTokenStr] forKey:string_InstrumentTokenStr];
                        [sortedArray addObject:tmp_Dict];
                        
                    }
                    
                }
                else
                {
                    @autoreleasepool {
                        
                        NSMutableDictionary *tmp_Dict = [[NSMutableDictionary alloc]init];
                        
                        [tmp_Dict setObject:[NSDictionary new] forKey:string_InstrumentTokenStr];
                        [sortedArray addObject:tmp_Dict];
                        
                    }
                    
                }
                
                
            }
            
            //NSLog(@"%@",instrumentToken);
            
            
            //NSLog(@"%@",sortedArray);
            
            
            if(symbol1.count>0)
            {
                
                
                
                
                
                //        NSMutableDictionary *tmp1 = [[NSMutableDictionary alloc]initWithDictionary:[instrumentTokensDict valueForKey:[sortedArray objectAtIndex:1]]];
                if([delegate2.exchaneLblStr containsString:@"CDS"]||[delegate2.symbolDepthStr containsString:@"NIFTY"]||[delegate2.exchaneLblStr containsString:@"MCX"])
                {
                    
                    NSNumber * number=[instrumentToken objectAtIndex:0] ;
                    
                    NSString * instrument=[number stringValue];
                    
                    //NSLog(@"%lu",[[[sortedArray objectAtIndex:0] objectForKey:instrument]count]);
                    
                    
                    
                    if([[[sortedArray objectAtIndex:0] objectForKey:instrument]count]>0)
                    {
                        
                        self.FutLtpLbl.text=[[[sortedArray objectAtIndex:0] objectForKey:instrument] objectForKey:@"LastPriceValue"];
                        
                     if(self.FutLtpLbl.text.length>0&&![self.FutLtpLbl.text isEqualToString:@"0.00"])
                     {
                        
                        if(FUTCheck==true)
                        {
                            if([delegate2.exchaneLblStr containsString:@"MCX"])
                            {
                                
                            }
                            
                            else
                            {
                            [self optionCEMethod];
                            }
                        }
                        
                        
                        
                     }
                        
                        
                        
                        NSString * changeStrPer=[[[sortedArray objectAtIndex:0] objectForKey:instrument] objectForKey:@"ChangePercentageValue"];
                        NSString * str=@"%";
                        
                        if([changeStrPer containsString:@"-"])
                        {
                            self.futChgPerLbl.text = [NSString stringWithFormat:@"(%@%@)",changeStrPer,str];
                            self.futChgPerLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                            
                            self.futChgLbl.text=[[[sortedArray objectAtIndex:0] objectForKey:instrument] objectForKey:@"ChangeValue"];
                            
                            self.futChgLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                        }
                        else{
                            self.futChgPerLbl.text = [NSString stringWithFormat:@"(%@%@)",changeStrPer,str];
                            self.futChgPerLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                            
                            self.futChgLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                            self.futChgLbl.text=[[[sortedArray objectAtIndex:0] objectForKey:instrument] objectForKey:@"ChangeValue"];
                        }
                        
                        
                        
                        
                        self.openLbl.text=[[[sortedArray objectAtIndex:0] objectForKey:instrument] objectForKey:@"OpenValue"];
                        self.highLbl.text=[[[sortedArray objectAtIndex:0] objectForKey:instrument] objectForKey:@"HighValue"];
                        self.lowLbl.text=[[[sortedArray objectAtIndex:0] objectForKey:instrument] objectForKey:@"LowValue"];
                        self.volumeLbl.text=[[[sortedArray objectAtIndex:0] objectForKey:instrument] objectForKey:@"VolumeValue"];
                        self.bidQtyLbl.text=[[[sortedArray objectAtIndex:0] objectForKey:instrument] objectForKey:@"BIDValue"];
                        self.askQtyLbl.text=[[[sortedArray objectAtIndex:0] objectForKey:instrument] objectForKey:@"ASKValue"];
                        //        self.chngperLbl.text=delegate2.changePercentageStr;
                        self.closeLbl.text=[[[sortedArray objectAtIndex:0] objectForKey:instrument] objectForKey:@"ClosePriceValue"];
                        self.bidPriceLbl.text=[[[sortedArray objectAtIndex:0] objectForKey:instrument] objectForKey:@"BIDPrice"];
                        self.askPriceLbl.text=[[[sortedArray objectAtIndex:0] objectForKey:instrument] objectForKey:@"ASKPrice"];
                        
                        
                        
                        //                //NSLog(@"%@",tmp1);
                    }
                    
                }
                
                
                else
                {
                    
                    NSNumber * number=[instrumentToken objectAtIndex:1] ;
                    
                    NSString * instrument=[number stringValue];
                    
                    //NSLog(@"%lu",[[[sortedArray objectAtIndex:1] objectForKey:instrument]count]);
                    
                    
                    
                    if([[[sortedArray objectAtIndex:1] objectForKey:instrument]count]>0)
                    {
                        
                        self.FutLtpLbl.text=[[[sortedArray objectAtIndex:1] objectForKey:instrument] objectForKey:@"LastPriceValue"];
                        
                           if(self.FutLtpLbl.text.length>0&&![self.FutLtpLbl.text isEqualToString:@"0.00"])
                        {
                        if(FUTCheck==true)
                        {
                            [self optionCEMethod];
                        }
                        
                        
                        }
                        
                        
                        
                        
                        NSString * chg=[[[sortedArray objectAtIndex:1] objectForKey:instrument] objectForKey:@"ChangeValue"];
                        
                        if([chg containsString:@"-"])
                        {
                            self.futChgLbl.text=chg;
                            
                            self.futChgLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                        }
                        
                        else
                        { self.futChgLbl.text=chg;
                            
                            self.futChgLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                            
                            
                        }
                        
                        NSString * chgPer=[[[sortedArray objectAtIndex:1] objectForKey:instrument] objectForKey:@"ChangePercentageValue"];
                        NSString * str=@"%";
                        
                        if([chgPer containsString:@"-"])
                        {
                            self.futChgPerLbl.text=[NSString stringWithFormat:@"%@%@",chgPer,str];
                            
                            self.futChgPerLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                        }
                        
                        else
                        {
                            self.futChgPerLbl.text=[NSString stringWithFormat:@"%@%@",chgPer,str];
                            
                            self.futChgPerLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                            
                            
                        }
                        
                        
                        
                        
                        
                        
                        
                        
                        self.openLbl.text=[[[sortedArray objectAtIndex:1] objectForKey:instrument] objectForKey:@"OpenValue"];
                        self.highLbl.text=[[[sortedArray objectAtIndex:1] objectForKey:instrument] objectForKey:@"HighValue"];
                        self.lowLbl.text=[[[sortedArray objectAtIndex:1] objectForKey:instrument] objectForKey:@"LowValue"];
                        self.volumeLbl.text=[[[sortedArray objectAtIndex:1] objectForKey:instrument] objectForKey:@"VolumeValue"];
                        self.bidQtyLbl.text=[[[sortedArray objectAtIndex:1] objectForKey:instrument] objectForKey:@"BIDValue"];
                        self.askQtyLbl.text=[[[sortedArray objectAtIndex:1] objectForKey:instrument] objectForKey:@"ASKValue"];
                        //        self.chngperLbl.text=delegate2.changePercentageStr;
                        self.closeLbl.text=[[[sortedArray objectAtIndex:1] objectForKey:instrument] objectForKey:@"ClosePriceValue"];
                        self.bidPriceLbl.text=[[[sortedArray objectAtIndex:1] objectForKey:instrument] objectForKey:@"BIDPrice"];
                        self.askPriceLbl.text=[[[sortedArray objectAtIndex:1] objectForKey:instrument] objectForKey:@"ASKPrice"];
                        
                    }
                    
                    
                    
                }
                
            }
            if(symbol.count>0)
            {
                
                
                
                NSNumber * number=[instrumentToken objectAtIndex:0] ;
                
                NSString * instrument=[number stringValue];
                
                //NSLog(@"%lu",[[[sortedArray objectAtIndex:0] objectForKey:instrument]count]);
                
                
                if([[[sortedArray objectAtIndex:0] objectForKey:instrument]count]>0)
                {
                    
                    self.ltpLbl.text=[[[sortedArray objectAtIndex:0] objectForKey:instrument] objectForKey:@"LastPriceValue"];
                    
                    
                    NSString * chg=[[[sortedArray objectAtIndex:0] objectForKey:instrument] objectForKey:@"ChangeValue"];
                    
                    if([chg containsString:@"-"])
                    {
                        self.chngLbl.text=chg;
                        
                        self.chngLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                    }
                    
                    else
                    { self.chngLbl.text=chg;
                        
                        self.chngLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                        
                        
                    }
                    
                    NSString * chgPer=[[[sortedArray objectAtIndex:0] objectForKey:instrument] objectForKey:@"ChangePercentageValue"];
                    NSString * str=@"%";
                    
                    if([chgPer containsString:@"-"])
                    {
                        self.chngperLbl.text=[NSString stringWithFormat:@"%@%@",chgPer,str];
                        
                        self.chngperLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                    }
                    
                    else
                    { self.chngperLbl.text=[NSString stringWithFormat:@"%@%@",chgPer,str];
                        
                        self.chngperLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                        
                        
                    }
                    
                    
                    
                    self.cashOpen.text=[[[sortedArray objectAtIndex:0] objectForKey:instrument] objectForKey:@"OpenValue"];
                    self.cashHigh.text=[[[sortedArray objectAtIndex:0] objectForKey:instrument] objectForKey:@"HighValue"];
                    self.cashLow.text=[[[sortedArray objectAtIndex:0] objectForKey:instrument] objectForKey:@"LowValue"];
                    self.cashVolume.text=[[[sortedArray objectAtIndex:0] objectForKey:instrument] objectForKey:@"VolumeValue"];
                    self.cashBidSize.text=[[[sortedArray objectAtIndex:0] objectForKey:instrument] objectForKey:@"BIDValue"];
                    self.askBidSize.text=[[[sortedArray objectAtIndex:0] objectForKey:instrument] objectForKey:@"ASKValue"];        //        self.chngperLbl.text=delegate2.changePercentageStr;
                    self.cashClose.text=[[[sortedArray objectAtIndex:0] objectForKey:instrument] objectForKey:@"ClosePriceValue"];
                    self.bidLbl.text=[[[sortedArray objectAtIndex:0] objectForKey:instrument] objectForKey:@"BIDPrice"];
                    self.askLbl.text=[[[sortedArray objectAtIndex:0] objectForKey:instrument] objectForKey:@"ASKPrice"];
                }
                
                
            }
            
            
            //         NSMutableDictionary *tmp2 = [[NSMutableDictionary alloc]initWithDictionary:[instrumentTokensDict valueForKey:[allKeysList objectAtIndex:2]]];
            //
            //        self.bidPriceCE1.text=[tmp2 objectForKey:@"BIDPrice"];
            //        self.askPriceCE1.text=[tmp2 objectForKey:@"ASKPrice"];
            //
            //        self.bidQtyCE1.text=[tmp2 objectForKey:@"BIDValue"];
            //        self.askQtyCE1.text=[tmp2 objectForKey:@"ASKValue"];
            //
            //        NSString * strikeStr=[NSString stringWithFormat:@"%@",[strike objectAtIndex:0]];
            //
            //        [self.strike1 setTitle:strikeStr forState:UIControlStateNormal];
            
            if(symbol2.count>0&&symbol3.count>0)
            {
                
                newSortArray=[[NSMutableArray alloc]init];
                newInstrumentToken=[[NSMutableArray alloc]init];
                
                newSortArray=[sortedArray mutableCopy];
                newInstrumentToken=[instrumentToken mutableCopy];
                
                //        if(check==true)
                //        {
                if([delegate2.exchaneLblStr containsString:@"CDS"]||[delegate2.symbolDepthStr containsString:@"NIFTY"])
                {
                    [newSortArray removeObjectAtIndex:0];
                    
                    [newInstrumentToken removeObjectAtIndex:0];
                    
                }
                
                else
                {
                    [newSortArray removeObjectAtIndex:0];
                    [newSortArray removeObjectAtIndex:0];
                    [newInstrumentToken removeObjectAtIndex:0];
                    [newInstrumentToken removeObjectAtIndex:0];
                }
                
                
                //            check=false;
                //        }
                
                //NSLog(@"%@",newInstrumentToken);
                //NSLog(@"%lu",newSortArray.count);
                
                testArray = [[NSMutableArray alloc]init];
                testDictionary = [[NSMutableDictionary alloc]init];
                
                
                CEArray = [[NSMutableArray alloc]init];
                PEArray = [[NSMutableArray alloc]init];
                
                for(int i=0;i<newInstrumentToken.count;i++)
                {
                    
                    if(i%2==0)
                    {
                        [CEArray addObject:[newInstrumentToken objectAtIndex:i]];
                    }else
                    {
                        [PEArray addObject:[newInstrumentToken objectAtIndex:i]];
                    }
                }
                
                //NSLog(@"CE array:%lu",(unsigned long)CEArray.count);
                //NSLog(@"PE Array:%lu",(unsigned long)PEArray.count);
                
                CEDetailArray =[[NSMutableArray alloc]init];
                PEDetailArray=[[NSMutableArray alloc]init];
                for(int i=0;i<CEArray.count;i++)
                {
                    
                    for(int j=0;j<newSortArray.count;j++)
                    {
                        NSMutableDictionary * filterDict=[[NSMutableDictionary alloc]init];
                        filterDict=[newSortArray objectAtIndex:j];
                        
                        NSArray * filterArray=[[NSArray alloc]init];
                        
                        filterArray=[filterDict allKeys];
                        
                        NSString * string3=[filterArray objectAtIndex:0];
                        //NSLog(@"%@",string3);
                        
                        NSNumber * number2=[CEArray objectAtIndex:i];
                        
                        NSString * string2=[number2 stringValue];
                        //NSLog(@"%@",string2);
                        
                        NSNumber * number4=[PEArray objectAtIndex:i];
                        
                        NSString * string4=[number4 stringValue];
                        //NSLog(@"%@",string4);
                        
                        
                        if([string3 isEqualToString:string2])
                        {
                            [CEDetailArray addObject:[newSortArray objectAtIndex:j]];
                        }
                        else if([string3 isEqualToString:string4])
                        {
                            [PEDetailArray addObject:[newSortArray objectAtIndex:j]];
                        }
                    }
                }
                
                //NSLog(@"ce%@",CEDetailArray);
                //NSLog(@"pe%@",PEDetailArray);
                
                //        NSMutableArray *selectedObjects = [NSMutableArray array];
                //        
                //        for(int i=0;i<newSortArray.count;i++)
                //        {
                //            if(newSortArray.count/2)
                //            {
                //        testDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:newSortArray[i],@"one", nil];
                //               // [testDictionary dictionaryWithValuesForKeys:newSortArray[i]];
                //                [testDictionary setObject:newSortArray[i] forKey:@"one"];
                //            }
                //            
                //        }
                //        //NSLog(@"test Dicitionary:%@",testDictionary );
                //        [testArray addObject:[testDictionary allValues]];
                //        //NSLog(@"Test array0:%@",[testArray objectAtIndex:0]);
                //        //NSLog(@"Test array1:%@",[testArray objectAtIndex:1]);
                //        NSIndexSet *index = [newSortArray indexesOfObjectsPassingTest:^BOOL(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                //            return [testArray containsObject:obj];
                //        }];
                //       NSArray* testArray1 = @[newSortArray[0,1]];
                //        
                //        //NSLog(@"Test array1:%@",testArray1);
                //        //NSLog(@"Index: %@",index);
                //        //NSLog(@"test Array:%@",testArray);
                
                
                
                
                
            }
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.depthTableView.delegate=self;
            self.depthTableView.dataSource=self;
            [self.depthTableView reloadData];
            
        });
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            
            
            
            
            
            
            [self.activityIndicator performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:YES];
            
            self.activityIndicator .hidden=YES;
            
            
            
            
            
        });
        
        
    }

        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    
    
    
}


- (void)webSocket:(PSWebSocket *)webSocket didFailWithError:(NSError *)error {
    //NSLog(@"The websocket handshake/connection failed with an error: %@", error);
}
- (void)webSocket:(PSWebSocket *)webSocket didReceiveMessageWithData:(NSData *)data
{
    //NSLog(@"The websocket received a message: %@", data);
}
- (void)webSocket:(PSWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    //NSLog(@"The websocket closed with code: %@, reason: %@, wasClean: %@", @(code), reason, (wasClean) ? @"YES" : @"NO");
}


-(void)depth
{
    

}





- (IBAction)futDate:(id)sender {
    
    self.pickerView.hidden=NO;
     self.doneView.hidden=NO;
    
    CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
    [self.scrollView setContentOffset:bottomOffset animated:YES];
    bankNiftyCheck =@"FUT";
    self.pickerView.delegate=self;
    self.pickerView.dataSource=self;
}


//picker view//

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if([self.companyTxt1.text containsString:@"BANKNIFTY"])
    {
        if([bankNiftyCheck isEqualToString:@"OPT"])
        {
        return bankNiftyOptionArray.count;
        }
        else
        {
        return symbol1.count;
            
        }
    }
    else
    {
    return symbol1.count;
    }
    
}
- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    //NSLog(@"%@",expiry);
    if([self.companyTxt1.text containsString:@"BANKNIFTY"])
    {
        if([bankNiftyCheck isEqualToString:@"OPT"])
        {
         return bankNiftyOptionArray[row];
        }
        else
        {
            return expiry[row];
            
        }
    }
    else
    {
         return expiry[row];
    }
    
   
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
   if(optCheck==true)
   {
      
       
       NSInteger selectRow = [self.pickerView selectedRowInComponent:0];
       
       objectAtIndex = selectRow;

      
    }
       
   
       else
       {
           
           
           NSInteger selectRow = [self.pickerView selectedRowInComponent:0];
           
           objectAtIndex = selectRow;
           
           
       }
    
   

      
       
       
    
   
}


//tableview//






- (IBAction)OPTBtnAction:(id)sender {
    
    self.pickerView.hidden=NO;
    self.doneView.hidden=NO;
    bankNiftyCheck =@"OPT";
    
    CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
    [self.scrollView setContentOffset:bottomOffset animated:YES];
    
    self.pickerView.delegate=self;
    self.pickerView.dataSource=self;
    
    optCheck=true;
    
    

}
- (IBAction)cashBuy:(id)sender {
    
    @try {
        
            
            
            delegate2.orderBuySell=@"BUY";
            if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
            {
                
                if(instrumentToken.count>0)
                {
                    delegate2.orderinstrument=[instrumentToken objectAtIndex:0];
                }
                
            }
            
            else
            {
                delegate2.orderinstrument=[exchangeTokenArray objectAtIndex:0];
                
            }
            delegate2.orderSegment=self.cashSegment.text;
            
            delegate2.symbolDepthStr=[symbol valueForKey:@"symbol"];
            
            if([self.localWisdomCheck isEqualToString:@"WISDOM"])
            {
                self.localWisdomCheck=@"";
                delegate2.wisdomGardemTickIDString = [delegate2.leaderAdviceDetails objectAtIndex:6];
            }
            
            StockOrderView * orderView=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
            
            [self.navigationController pushViewController:orderView animated:YES];
            
        

        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
}

- (IBAction)cashSell:(id)sender {
    @try {
        
            
            
            delegate2.orderBuySell=@"SELL";
            if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
            {
                
                if(instrumentToken.count>0)
                {
                    delegate2.orderinstrument=[instrumentToken objectAtIndex:0];
                }
                
            }
            
            else
            {
                delegate2.orderinstrument=[exchangeTokenArray objectAtIndex:0];
                
            }
           
            delegate2.orderSegment=self.cashSegment.text;
            delegate2.symbolDepthStr=[symbol valueForKey:@"symbol"];
            
            
            if([self.localWisdomCheck isEqualToString:@"WISDOM"])
            {
                self.localWisdomCheck=@"";
                delegate2.wisdomGardemTickIDString = [delegate2.leaderAdviceDetails objectAtIndex:6];
            }
            
            
            StockOrderView * orderView=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
            
            [self.navigationController pushViewController:orderView animated:YES];
        

    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
//

}
- (IBAction)futBuy:(id)sender {
    
    @try {
        
            if(symbol.count>0)
            {
            
            delegate2.orderBuySell=@"BUY";
                
                if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                {
                
            if(instrumentToken.count>0)
            {
            delegate2.orderinstrument=[instrumentToken objectAtIndex:1];
            }
                    
                }
                
                else
                {
                     delegate2.orderinstrument=[exchangeTokenArray objectAtIndex:1];
                    
                }
            delegate2.orderSegment=self.futureSegment.text;
            
            
            if([self.localWisdomCheck isEqualToString:@"WISDOM"])
            {
                self.localWisdomCheck=@"";
                if(delegate2.leaderAdviceDetails.count>0)
                {
                delegate2.wisdomGardemTickIDString = [delegate2.leaderAdviceDetails objectAtIndex:6];
                }
            }
            //NSLog(@"%@",delegate2.tradingSecurityDes);
            
            
            StockOrderView * orderView=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
                
                orderView.upstoxCheckString=@"fut";
            [self.navigationController pushViewController:orderView animated:YES];
                
            }
            
            else
            {
                delegate2.orderBuySell=@"BUY";
                
                if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                {
                if(instrumentToken.count>0)
                {
                    delegate2.orderinstrument=[instrumentToken objectAtIndex:0];
                }
                    
                }
                
                else
                {
                    
                    delegate2.orderinstrument=[exchangeTokenArray objectAtIndex:0];
                    
                    
                }
                delegate2.orderSegment=self.futureSegment.text;
                
                
                if([self.localWisdomCheck isEqualToString:@"WISDOM"])
                {
                    if(delegate2.leaderAdviceDetails.count>0)
                    {
                        delegate2.wisdomGardemTickIDString = [delegate2.leaderAdviceDetails objectAtIndex:6];
                    }
                }
                //NSLog(@"%@",delegate2.tradingSecurityDes);
                
                
                StockOrderView * orderView=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
                orderView.upstoxCheckString=@"fut";
                
                [self.navigationController pushViewController:orderView animated:YES];
            }
        

        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    

}

- (IBAction)futSell:(id)sender {
    
    @try {
       
            
            if(symbol.count>0)
            {
            delegate2.orderBuySell=@"SELL";
                
         if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
         {
            if(instrumentToken.count>0)
            {
            delegate2.orderinstrument=[instrumentToken objectAtIndex:1];
            }
             
         }
                
        else
        {
        delegate2.orderinstrument=[exchangeTokenArray objectAtIndex:1];
                    
        }
            delegate2.orderSegment=self.futureSegment.text;
            
            
            if([self.localWisdomCheck isEqualToString:@"WISDOM"])
            {
                if(delegate2.leaderAdviceDetails.count>0)
                {
                delegate2.wisdomGardemTickIDString = [delegate2.leaderAdviceDetails objectAtIndex:6];
                }
            }
            //NSLog(@"%@",delegate2.tradingSecurityDes);
            
            
            StockOrderView * orderView=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
                orderView.upstoxCheckString=@"fut";

            
            [self.navigationController pushViewController:orderView animated:YES];
                
            }
            
            else
            {
                
                delegate2.orderBuySell=@"SELL";
                if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                {
                    if(instrumentToken.count>0)
                    {
                        delegate2.orderinstrument=[instrumentToken objectAtIndex:0];
                    }
                    
                }
                
                else
                {
                    
                    delegate2.orderinstrument=[exchangeTokenArray objectAtIndex:0];
                    
                    
                }
                delegate2.orderSegment=self.futureSegment.text;
                
                
                if([self.localWisdomCheck isEqualToString:@"WISDOM"])
                {
                    if(delegate2.leaderAdviceDetails.count>0)
                    {
                        delegate2.wisdomGardemTickIDString = [delegate2.leaderAdviceDetails objectAtIndex:6];
                    }
                }
                //NSLog(@"%@",delegate2.tradingSecurityDes);
                
                
                StockOrderView * orderView=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
                orderView.upstoxCheckString=@"fut";

                
                [self.navigationController pushViewController:orderView animated:YES];
                
            }
            
            
            
        

    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
    

}



-(void)perfromDeselectBID_CE
{
    @try {
        [delegate2.optionOrderArray removeObjectAtIndex: delegate2.anIndex];
        [delegate2.optionLimitPriceArray removeObjectAtIndex: delegate2.anIndex];
        [delegate2.optionOrderTransactionArray removeObjectAtIndex: delegate2.anIndex];
        
        
        
        [delegate2.optionOrderArray insertObject:@"check" atIndex: delegate2.anIndex];
        [delegate2.optionLimitPriceArray insertObject:@"check" atIndex: delegate2.anIndex];
        [delegate2.optionOrderTransactionArray insertObject:@"check" atIndex: delegate2.anIndex];
        
        
        //NSLog(@"%@",delegate2.optionLimitPriceArray );
        
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

}

-(void)perfomDeselectBID_PE
{
    @try {
        [delegate2.optionOrderArray removeObjectAtIndex:delegate2.anIndex1];
        [delegate2.optionLimitPriceArray removeObjectAtIndex:delegate2.anIndex1];
        [delegate2.optionOrderTransactionArray removeObjectAtIndex:delegate2.anIndex1];
        
        [delegate2.optionOrderArray insertObject:@"check" atIndex:delegate2.anIndex1];
        [delegate2.optionLimitPriceArray insertObject:@"check" atIndex:delegate2.anIndex1];
        [delegate2.optionOrderTransactionArray insertObject:@"check" atIndex:delegate2.anIndex1];
        
        //NSLog(@"%@",delegate2.optionLimitPriceArray );
        
        
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

}






//Tableview Btn selection//

-(void)performSelectCallPut:(UIButton *) sender
{
//    NSMutableDictionary *dict_Temp = [[NSMutableDictionary alloc]init];
@try
    {
    
    CGPoint center= sender.center;
    CGPoint rootViewPoint = [sender.superview convertPoint:center toView:self.depthTableView];
    NSIndexPath *indexPath = [self.depthTableView indexPathForRowAtPoint:rootViewPoint];
    
    OPTCell *cell = [self.depthTableView cellForRowAtIndexPath:indexPath];
    //NSLog(@"%@",indexPath);
    
    
    //venk//
    
//    if(delegate2.btnCountArray.count<4)
//    {
//        NSMutableArray *tmp_array = [[NSMutableArray alloc]initWithArray:[array_isSelected objectAtIndex:indexPath.row]];
//        
//        
//        
//        
//        if([[tmp_array objectAtIndex:sender.tag] isEqual:@(YES)])
//            [tmp_array replaceObjectAtIndex:sender.tag withObject:@(NO)];
//        else
//            [tmp_array replaceObjectAtIndex:sender.tag withObject:@(YES)];
//        
//        [array_isSelected replaceObjectAtIndex:indexPath.row withObject:tmp_array];
//        
//        
//        
//        
//    }
//
//      else{
//
//
//        NSMutableArray *tmp_array = [[NSMutableArray alloc]initWithArray:[array_isSelected objectAtIndex:indexPath.row]];
//    
//        
//        if([[tmp_array objectAtIndex:sender.tag] isEqual:@(YES)])
//        {
//            [tmp_array replaceObjectAtIndex:sender.tag withObject:@(NO)];
//            if(delegate2.btnCountArray.count >0)
//                [delegate2.btnCountArray removeObjectAtIndex:0];
//        
//        }
//        else{
//            
//            return;
//        }
//        
//    
//        
//        [array_isSelected replaceObjectAtIndex:indexPath.row withObject:tmp_array];
//    }


    
    
    if(sender.tag == 0&&cell.btn_BID_CE.layer.borderWidth==0.0f&&delegate2.btnCountArray.count<4) // CE _ Buy
    {
//        [dict_Temp setValue:@"CE" forKey:@"CallPut"];
//        [dict_Temp setValue:@"BUY" forKey:@"Options"];
//        [dict_Temp setValue:[NSNumber numberWithInt:sender.tag] forKey:@"TagID"];
        
        cell.btn_BID_CE.layer.borderWidth=2.0f;
        cell.btn_BID_CE.layer.borderColor=[[UIColor colorWithRed:(254/255.0) green:(217/255.0) blue:(65/255.0) alpha:1]CGColor];
        
      
        [cell.btn_BID_CE setBackgroundColor:[UIColor colorWithRed:(254/255.0) green:(217/255.0) blue:(65/255.0) alpha:1]];
        
        [delegate2.btnCountArray addObject:@"1"];
        
        NSString * localStr=[[[callPutDictionary objectForKey:titleStr] objectAtIndex:0] objectAtIndex:indexPath.row];
        
        [delegate2.optionOrderArray addObject:localStr];
        
        NSString * priceStr=cell.bidPriceCE.text;
        
        [delegate2.optionLimitPriceArray addObject:priceStr];
        
        //NSLog(@"%@",delegate2.optionOrderArray);
        
        [delegate2.optionOrderTransactionArray addObject:@"BUY"];
        
         delegate2.anIndex=[delegate2.optionOrderArray indexOfObject:localStr];
        
        
        
        //NSLog(@"%lu",delegate2.anIndex);
        
        
        
        
        
        
        
        
    }
    
    else if(sender.tag == 0&&cell.btn_BID_CE.layer.borderWidth==2.0f) // CE _ Buy
    {
        //        [dict_Temp setValue:@"CE" forKey:@"CallPut"];
        //        [dict_Temp setValue:@"BUY" forKey:@"Options"];
        //        [dict_Temp setValue:[NSNumber numberWithInt:sender.tag] forKey:@"TagID"];
        
        cell.btn_BID_CE.layer.borderWidth=0.0f;
        cell.btn_BID_CE.layer.borderColor=[[UIColor whiteColor]CGColor];
         [cell.btn_BID_CE setBackgroundColor:[UIColor whiteColor]];
        
        [delegate2.btnCountArray removeObject:@"1"];
        
        
        //NSLog(@"%lu",delegate2.anIndex);
        
        [self perfromDeselectBID_CE];
        @try {
            [delegate2.optionOrderArray removeObjectAtIndex: delegate2.anIndex];
            [delegate2.optionLimitPriceArray removeObjectAtIndex: delegate2.anIndex];
            [delegate2.optionOrderTransactionArray removeObjectAtIndex: delegate2.anIndex];
            
           
            
            [delegate2.optionOrderArray insertObject:@"check" atIndex: delegate2.anIndex];
            [delegate2.optionLimitPriceArray insertObject:@"check" atIndex: delegate2.anIndex];
            [delegate2.optionOrderTransactionArray insertObject:@"check" atIndex: delegate2.anIndex];
            
            
            //NSLog(@"%@",delegate2.optionLimitPriceArray );
            

        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
        
    }

    
    else if(sender.tag == 1&&cell.btn_BID_PE.layer.borderWidth==0.0f&&delegate2.btnCountArray.count<4) //PE _ Buy
    {
//        [dict_Temp setValue:@"PE" forKey:@"CallPut"];
//        [dict_Temp setValue:@"BUY" forKey:@"Options"];
        
        cell.btn_BID_PE.layer.borderWidth=2.0f;
        cell.btn_BID_PE.layer.borderColor=[[UIColor colorWithRed:(254/255.0) green:(217/255.0) blue:(65/255.0) alpha:1]CGColor];
         [cell.btn_BID_PE setBackgroundColor:[UIColor colorWithRed:(254/255.0) green:(217/255.0) blue:(65/255.0) alpha:1]];
        
        [delegate2.btnCountArray addObject:@"2"];
        
        NSString * localStr=[[[callPutDictionary objectForKey:titleStr] objectAtIndex:1] objectAtIndex:indexPath.row];
        
        [delegate2.optionOrderArray addObject:localStr];
        
        NSString * priceStr=cell.bidPricePE.text;
        
        [delegate2.optionLimitPriceArray addObject:priceStr];
        
        //NSLog(@"%@",delegate2.optionOrderArray);
         [delegate2.optionOrderTransactionArray addObject:@"BUY"];
        
         delegate2.anIndex1=[delegate2.optionOrderArray indexOfObject:localStr];
        
        //NSLog(@"%lu",delegate2.anIndex1);
        
    }
    
    else if(sender.tag == 1&&cell.btn_BID_PE.layer.borderWidth==2.0f) //PE _ Buy
    {
        //        [dict_Temp setValue:@"PE" forKey:@"CallPut"];
        //        [dict_Temp setValue:@"BUY" forKey:@"Options"];
        
        cell.btn_BID_PE.layer.borderWidth=0.0f;
        cell.btn_BID_PE.layer.borderColor=[[UIColor whiteColor]CGColor];
          [cell.btn_BID_PE setBackgroundColor:[UIColor whiteColor]];
       [delegate2.btnCountArray removeObject:@"2"];
        //NSLog(@"%lu",delegate2.anIndex1);
        @try {
            [delegate2.optionOrderArray removeObjectAtIndex:delegate2.anIndex1];
            [delegate2.optionLimitPriceArray removeObjectAtIndex:delegate2.anIndex1];
            [delegate2.optionOrderTransactionArray removeObjectAtIndex:delegate2.anIndex1];
            
            [delegate2.optionOrderArray insertObject:@"check" atIndex:delegate2.anIndex1];
            [delegate2.optionLimitPriceArray insertObject:@"check" atIndex:delegate2.anIndex1];
            [delegate2.optionOrderTransactionArray insertObject:@"check" atIndex:delegate2.anIndex1];
            
             //NSLog(@"%@",delegate2.optionLimitPriceArray );
            

            
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
    }
    else if (sender.tag == 2&&cell.btn_ASK_CE.layer.borderWidth==0.0f&&delegate2.btnCountArray.count<4) // CE _ SELL
    {
//        [dict_Temp setValue:@"CE" forKey:@"CallPut"];
//        [dict_Temp setValue:@"SELL" forKey:@"Options"];
        cell.btn_ASK_CE.layer.borderWidth=2.0f;
        cell.btn_ASK_CE.layer.borderColor=[[UIColor colorWithRed:(254/255.0) green:(217/255.0) blue:(65/255.0) alpha:1]CGColor];
         [cell.btn_ASK_CE setBackgroundColor:[UIColor colorWithRed:(254/255.0) green:(217/255.0) blue:(65/255.0) alpha:1]];
         [delegate2.btnCountArray addObject:@"3"];
        
        NSString * localStr=[[[callPutDictionary objectForKey:titleStr] objectAtIndex:0] objectAtIndex:indexPath.row];
        
        NSString * priceStr=cell.askPriceCE.text;
        
        [delegate2.optionLimitPriceArray addObject:priceStr];
        
        [delegate2.optionOrderArray addObject:localStr];
        
        //NSLog(@"%@",delegate2.optionOrderArray);
        [delegate2.optionOrderTransactionArray addObject:@"SELL"];
        
         delegate2.anIndex2=[delegate2.optionOrderArray indexOfObject:localStr];
        
        //NSLog(@"%lu",delegate2.anIndex2);
        
    }
    
    else if (sender.tag == 2&&cell.btn_ASK_CE.layer.borderWidth==2.0f) // CE _ SELL
    {
        //        [dict_Temp setValue:@"CE" forKey:@"CallPut"];
        //        [dict_Temp setValue:@"SELL" forKey:@"Options"];
        cell.btn_ASK_CE.layer.borderWidth=0.0f;
        cell.btn_ASK_CE.layer.borderColor=[[UIColor whiteColor]CGColor];
         [cell.btn_ASK_CE setBackgroundColor:[UIColor whiteColor]];
         [delegate2.btnCountArray removeObject:@"3"];
        //NSLog(@"%lu",delegate2.anIndex2);
        @try {
            [delegate2.optionOrderArray removeObjectAtIndex:delegate2.anIndex2];
            [delegate2.optionLimitPriceArray removeObjectAtIndex:delegate2.anIndex2];
            [delegate2.optionOrderTransactionArray removeObjectAtIndex:delegate2.anIndex2];
            
            [delegate2.optionOrderArray insertObject:@"check" atIndex:delegate2.anIndex2];
            [delegate2.optionLimitPriceArray insertObject:@"check" atIndex:delegate2.anIndex2];
            [delegate2.optionOrderTransactionArray insertObject:@"check" atIndex:delegate2.anIndex2];
            
            
             //NSLog(@"%@",delegate2.optionLimitPriceArray );
            

            
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }

       
    }
    else if (sender.tag == 3&&cell.btn_ASK_PE.layer.borderWidth==0.0f&&delegate2.btnCountArray.count<4) // PE _SeLL
    {
//        [dict_Temp setValue:@"PE" forKey:@"CallPut"];
//        [dict_Temp setValue:@"SELL" forKey:@"Options"];
        cell.btn_ASK_PE.layer.borderWidth=2.0f;
        cell.btn_ASK_PE.layer.borderColor=[[UIColor colorWithRed:(254/255.0) green:(217/255.0) blue:(65/255.0) alpha:1]CGColor];
          [cell.btn_ASK_PE setBackgroundColor:[UIColor colorWithRed:(254/255.0) green:(217/255.0) blue:(65/255.0) alpha:1]];
         [delegate2.btnCountArray addObject:@"4"];
        
        NSString * localStr=[[[callPutDictionary objectForKey:titleStr] objectAtIndex:1] objectAtIndex:indexPath.row];
        
        NSString * priceStr=cell.askPricePE.text;
        
        [delegate2.optionLimitPriceArray addObject:priceStr];

        
        [delegate2.optionOrderArray addObject:localStr];
        
        //NSLog(@"%@",delegate2.optionOrderArray);
[delegate2.optionOrderTransactionArray addObject:@"SELL"];
         delegate2.anIndex3=[delegate2.optionOrderArray indexOfObject:localStr];
        
         //NSLog(@"%lu",delegate2.anIndex3);
       
    }
    
    else if (sender.tag == 3&&cell.btn_ASK_PE.layer.borderWidth==2.0f) // PE _SeLL
    {
        //        [dict_Temp setValue:@"PE" forKey:@"CallPut"];
        //        [dict_Temp setValue:@"SELL" forKey:@"Options"];
        cell.btn_ASK_PE.layer.borderWidth=0.0f;
        cell.btn_ASK_PE.layer.borderColor=[[UIColor whiteColor]CGColor];
         [cell.btn_ASK_PE setBackgroundColor:[UIColor whiteColor]];
         [delegate2.btnCountArray removeObject:@"4"];
        
        //NSLog(@"%lu",delegate2.anIndex3);
        @try {
            [delegate2.optionOrderArray removeObjectAtIndex:delegate2.anIndex3];
            [delegate2.optionLimitPriceArray removeObjectAtIndex:delegate2.anIndex3];
            [delegate2.optionOrderTransactionArray removeObjectAtIndex:delegate2.anIndex3];
            
            
            [delegate2.optionOrderArray insertObject:@"check" atIndex:delegate2.anIndex3];
            [delegate2.optionLimitPriceArray insertObject:@"check" atIndex:delegate2.anIndex3];
            [delegate2.optionOrderTransactionArray insertObject:@"check" atIndex:delegate2.anIndex3];
            
             //NSLog(@"%@",delegate2.optionLimitPriceArray );
            

            
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
    }
    
    
//    [dict_Temp setObject:indexPath forKey:@"IndexPath"];
//    [dict_Temp setValue:[[[[callPutDictionary objectForKey:titleStr] objectAtIndex:0] objectAtIndex:indexPath.row] valueForKey:@"strikeprice"] forKey:@"StrikePrice"];
//    [dict_Temp setValue:self.OPTBtn.titleLabel.text forKey:@"Date"];
//    [dict_Temp setValue:self.optionCompanyName.text forKey:@"CompanyTitle"];
//    
//    [array_SelectedIndexPostion addObject:dict_Temp];
    
    
    
    if(delegate2.btnCountArray.count<4)
    {
       
        
            NSMutableArray *tmp_array = [[NSMutableArray alloc]initWithArray:[array_isSelected objectAtIndex:indexPath.row]];
            
            
            
            
            if([[tmp_array objectAtIndex:sender.tag] isEqual:@(YES)])
                [tmp_array replaceObjectAtIndex:sender.tag withObject:@(NO)];
            else
                [tmp_array replaceObjectAtIndex:sender.tag withObject:@(YES)];
            
            [array_isSelected replaceObjectAtIndex:indexPath.row withObject:tmp_array];
        
        
            
            
        if(delegate2.btnCountArray.count==3)
        {
            opt=true;
        }
        

    }
    
    else
    {
        if(opt==true)
        {
            opt=false;
        
            NSMutableArray *tmp_array = [[NSMutableArray alloc]initWithArray:[array_isSelected objectAtIndex:indexPath.row]];
            
            
            
            
            if([[tmp_array objectAtIndex:sender.tag] isEqual:@(YES)])
                [tmp_array replaceObjectAtIndex:sender.tag withObject:@(NO)];
            else
                [tmp_array replaceObjectAtIndex:sender.tag withObject:@(YES)];
            
            [array_isSelected replaceObjectAtIndex:indexPath.row withObject:tmp_array];
            
            
            
        }
        
        

    }
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}


-(BOOL)performIsIndexPathAvaliable:(NSIndexPath *)indexpath
{
    
    BOOL isAvailable = false;
    
    
    
    return isAvailable;
}

- (IBAction)tradeBtn:(id)sender {
    
   @try
    {
        if([delegate2.brokerNameStr isEqualToString:@"Proficient"])
        {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:@"Message"
                                             message:@"Sorry, your broker doesn't support this exchange type currently."
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                //Add Buttons
                
                UIAlertAction* okButton = [UIAlertAction
                                           actionWithTitle:@"Ok"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
                                               
                                               //                                    delegate2.holdingCheck=@"hold";
                                               //Handle your yes please button action here
                                               
                                               
                                           }];
                //Add your buttons to alert controller
                
                [alert addAction:okButton];
                
                
                [self presentViewController:alert animated:YES completion:nil];
                
            });
        }
        else
        {
            if(delegate2.btnCountArray.count>0)
            {
                CallPutTrade * callPut=[self.storyboard instantiateViewControllerWithIdentifier:@"CallPutTrade"];
                
                [self.navigationController pushViewController:callPut animated:YES];
            }
            
            else
            {
                  dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Please select your trade options." preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                
                [alert addAction:okAction];

                  });
            }
            
            }
        }
        
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}
- (IBAction)doneAction:(id)sender {
    @try
    {
    self.pickerView.hidden=YES;
    self.doneView.hidden=YES;
    
    if(optCheck==true)
    {
        
        
//        [self.OPTBtn setTitle:[expiry objectAtIndex:objectAtIndex] forState:UIControlStateNormal];
        self.pickerView.hidden=YES;
        self.doneView.hidden=YES;
        
        
        if([delegate2.exchaneLblStr containsString:@"CDS"]||[delegate2.symbolDepthStr containsString:@"NIFTY"]||[delegate2.exchaneLblStr containsString:@"MCX"])
        {
            
//            if(allKeysList.count>=1)
//            {
            
                NSNumber * futInstrument=[instrumentToken objectAtIndex:0];
                NSString * exchangeTokenStr=[NSString stringWithFormat:@"%@",[exchangeTokenArray objectAtIndex:0]];
                
                //NSLog(@"%@",instrumentToken);
                
               
                
                
                
                
                title=[[callPutDictionary allKeys]mutableCopy];
                if(title.count>0)
                {
                    if([self.companyTxt1.text containsString:@"BANKNIFTY"])
                    {
                        if([bankNiftyCheck isEqualToString:@"OPT"])
                        {
                            NSString *myString = [bankNiftyOptionArray objectAtIndex:objectAtIndex];
                            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                            dateFormatter.dateFormat = @"dd MMM yyyy";
                            NSDate *yourDate = [dateFormatter dateFromString:myString];
                            dateFormatter.dateFormat = @"yyyy-MM-dd";
                            NSString * finalDate=[dateFormatter stringFromDate:yourDate];
                            expiryDateFilter=finalDate;
                        }
                        else
                        {
                            
                             expiryDateFilter=[[symbol1 objectAtIndex:objectAtIndex]objectForKey:@"expiryseries"];
                        }
                    }
                    else
                    {
                        
                        expiryDateFilter=[[symbol1 objectAtIndex:objectAtIndex]objectForKey:@"expiryseries"];
                    }
                    
                    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
                    {
                        [instrumentToken removeAllObjects];
                    [instrumentToken insertObject:futInstrument atIndex:0];
                         [self.socket close];
                    }
                    else
                    {
                        [exchangeTokenArray removeAllObjects];
                    [exchangeTokenArray insertObject:exchangeTokenStr atIndex:0];
                    }
                    optCheck=false;
                    
                   
                    
                    [self optionCEMethod];
                }
                
           
            
      
            
        }
        
        else
        {
            
            
//            if(allKeysList.count>=1)
//            {
                localStrikesCheck=@"strikes";
                NSNumber * cashInstrument=[instrumentToken objectAtIndex:0];
                NSNumber * futInstrument=[instrumentToken objectAtIndex:1];
                
                NSString * cashExchangeToken=[NSString stringWithFormat:@"%@",[exchangeTokenArray objectAtIndex:0]];
                
                NSString * futExchangeToken=[NSString stringWithFormat:@"%@",[exchangeTokenArray objectAtIndex:1]];
                
                //NSLog(@"%@",instrumentToken);
                if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                {
                [instrumentToken removeAllObjects];
                }
                else
                {
                [exchangeTokenArray removeAllObjects];
                }
                //NSLog(@"%@",instrumentToken);
                
                
                
                
                title=[[callPutDictionary allKeys]mutableCopy];
                if(title.count>0)
                {
                    
                    NSString *string=[expiry objectAtIndex:objectAtIndex
                                      ];
//                    NSArray *components = [string componentsSeparatedByString:@" "];
                    
                    expiryDateFilter=[[symbol1 objectAtIndex:objectAtIndex]objectForKey:@"expiryseries"];
                    
                    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                    {
                    [instrumentToken insertObject:cashInstrument atIndex:0];
                    [instrumentToken insertObject:futInstrument atIndex:1];
                       [self.socket close];
                    }
                    
                    else
                    {
                    [exchangeTokenArray insertObject:cashExchangeToken atIndex:0];
                    [exchangeTokenArray insertObject:futExchangeToken atIndex:1];
                    }
                    optCheck=false;
                    
                   
                    
                    [self optionCEMethod];
                    
                    
                }
                
//            }
            
            
        }

             [self.scrollView setContentOffset:CGPointMake(0, 300) animated:YES];
    }
    
    else
    {
    

    [self.expiryBtnOut setTitle:[expiry objectAtIndex:objectAtIndex] forState:UIControlStateNormal];
    
    
    self.pickerView.hidden=YES;
    self.doneView.hidden=YES;
    //        [self.backgroundView setBackgroundColor:[UIColor whiteColor]];
    //        self.backgroundView.alpha=1;
    //        NSInteger selectRow = [self.pickerView selectedRowInComponent:0];
    //
    //        objectAtIndex = selectRow;
    
    
    
    if(allKeysList.count>=1)
    {
        
        if([delegate2.exchaneLblStr containsString:@"CDS"]||[delegate2.symbolDepthStr containsString:@"NIFTY"]||[delegate2.exchaneLblStr containsString:@"MCX"])
        {
            if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
            {
            [instrumentToken removeObjectAtIndex:0];
                
            }
            
            else
            {
               [exchangeTokenArray removeObjectAtIndex:0];
            }
        }
        else
        {
            if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
            {
                [instrumentToken removeObjectAtIndex:1];
                
            }
            
            else
            {
                [exchangeTokenArray removeObjectAtIndex:1];
            }
        }
        
       
        
        NSString * string=[[symbol1 objectAtIndex:objectAtIndex] valueForKey:@"instrument_token"];
        
          NSString * exchangeToken=[[symbol1 objectAtIndex:objectAtIndex] valueForKey:@"exchange_token"];
        
        
        
        delegate2.tradingSecurityDes=[[symbol1 objectAtIndex:objectAtIndex]valueForKey:@"securitydesc"];
        if([delegate2.brokerNameStr isEqualToString:@"Upstox"]&&[delegate2.tradingSecurityDes containsString:@"BANKNIFTY"])
        {
           delegate2.tradingSecurityDes=[[symbol1 objectAtIndex:objectAtIndex]valueForKey:@"securitydescupstox"];
        }
        else
        {
           delegate2.tradingSecurityDes=[[symbol1 objectAtIndex:objectAtIndex]valueForKey:@"securitydesc"];
        }
        
        //NSLog(@"%@",string);
        
        
        int myInt = [string intValue];
        
        //NSLog(@"token%i",myInt);
        
        if([delegate2.exchaneLblStr containsString:@"CDS"]||[delegate2.symbolDepthStr containsString:@"NIFTY"]||[delegate2.exchaneLblStr containsString:@"MCX"])
        {
            if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
            {
            [instrumentToken insertObject:[NSNumber numberWithInt:myInt] atIndex:0];
                 [self.socket close];
            }
            
            else
            {
                 [exchangeTokenArray insertObject:exchangeToken atIndex:0];
            }
        }
        else
        {
            if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
            {
                [instrumentToken insertObject:[NSNumber numberWithInt:myInt] atIndex:1];
                 [self.socket close];
            }
            
            else
            {
                [exchangeTokenArray insertObject:exchangeToken atIndex:1];
            }
        }
        
        
        
       
        
        
       
        [self websocket];
    }
    
    
    
    [self.scrollView setContentOffset:CGPointZero animated:YES];
        
    }
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}
- (IBAction)onProfileButtonTap:(id)sender {
    @try {
        
        delegate2.leaderIDWhoToFollow = [delegate2.leaderAdviceDetails objectAtIndex:5];
        //
        //    if([[[self.responseArray objectAtIndex:indexPath.row] objectForKey:@"portfoliojson"] isEqual:[NSNull null]])
        //    {
        //
        //        delegate1.leaderIDWhoToFollow=[[self.responseArray objectAtIndex:indexPath.row]objectForKey:@"leaderid"];
        //
        //
        //
        //
        //    }
        //
        NewProfile * new=[self.storyboard instantiateViewControllerWithIdentifier:@"NewProfile"];
        
        [self.navigationController pushViewController:new animated:YES];

        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
  
}

- (void)mtbroad:(NSArray *)data
{
   
   
        // here: start the loop
        @try {
            
            //        dispatch_async(dispatch_get_main_queue(), ^{
            
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
           if(data.count>0)
           {
            NSString * newString=[NSString stringWithFormat:@"%@",[[data objectAtIndex:0] objectForKey:@"stSecurityID"]];
           
            
            NSMutableArray * localArray=[[NSMutableArray alloc]init];
            
            if(delegate2.marketWatch1.count>0)
            {
                for(int i=0;i<delegate2.marketWatch1.count;i++)
                {
                    NSString * localStr=[NSString stringWithFormat:@"%@",[[delegate2.marketWatch1 objectAtIndex:i] objectForKey:@"stSecurityID"]];
                    
                    if([localStr containsString:newString]||[newString containsString:localStr])
                    {
                        exchangeTokenCheck=true;
                        [delegate2.marketWatch1 replaceObjectAtIndex:i withObject:[data objectAtIndex:0]];
                    }
                    
                    
                    
                }
                
                if(exchangeTokenCheck==true)
                {
                    
                    exchangeTokenCheck=false;
                }
                
                else
                {
                    [delegate2.marketWatch1 addObject:[data objectAtIndex:0]];
                }
                
                
                
                
                
            }
            
            else
            {
                [delegate2.marketWatch1 addObject:[data objectAtIndex:0]];
            }
            
            if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
            {
                
            }
            
            else
            {
                
                instrumentTokensDict=[[NSMutableDictionary alloc]init];
                NSString * newExchangeToken;         
                
                
                for(int i=0;i<delegate2.marketWatch1.count;i++)
                {
                    NSString * tmpInstrument=[NSString stringWithFormat:@"%@",[[delegate2.marketWatch1 objectAtIndex:i] objectForKey:@"stSecurityID"]];
                    
                    for(int j=0;j<exchangeTokenArray.count;j++)
                    {
                        NSString * exchangeToken=[NSString stringWithFormat:@"%@",[exchangeTokenArray objectAtIndex:j]];
                        
                        if([tmpInstrument containsString:exchangeToken])
                        {
                            newExchangeToken=exchangeToken;
                            [instrumentTokensDict setValue:[delegate2.marketWatch1 objectAtIndex:i] forKeyPath:newExchangeToken];
                        }
                        
                        
                    }
                    
                    
                    
                    
                    
                }
                
                
               
                
                
                
                //  [shareListArray addObject:tmpDict];
                
                
                
//                if(allKeysList.count>0)
//                {
//                    [allKeysList removeAllObjects];
//                }
                
              NSArray *  allKeysListNew = [[instrumentTokensDict allKeys]mutableCopy];
                NSMutableArray * allKeysList = [[NSMutableArray alloc]init];
                
                for(int i=0;i<allKeysListNew.count;i++)
                {
                    [globalAllKeys addObject:[allKeysListNew objectAtIndex:i]];
                    [allKeysList addObject:[allKeysListNew objectAtIndex:i]];
                }
                
//                NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:globalAllKeys];
            
//                NSMutableDictionary *tmp = [[NSMutableDictionary alloc]initWithDictionary:[instrumentTokensDict valueForKey:[allKeysList objectAtIndex:0]]];
                
               
                
                
                sortedArray = [[NSMutableArray alloc]init];
                
              
                
                for( int i =0 ;i <[exchangeTokenArray count];i++)
                {
                    
                    
                    
                    
                    NSString *string_InstrumentTokenStr=[NSString stringWithFormat:@"%@",[exchangeTokenArray objectAtIndex:i]];
                    
                   
                    
                    
                    if([allKeysList containsObject:string_InstrumentTokenStr])
                    {
                        @autoreleasepool {
                        
                            
                            NSMutableDictionary *tmp_Dict = [[NSMutableDictionary alloc]init];
                            
                            [tmp_Dict setObject:[instrumentTokensDict valueForKey:string_InstrumentTokenStr] forKey:string_InstrumentTokenStr];
                            [sortedArray addObject:tmp_Dict];
                            
                        }
                        
                    }
                    else
                    {
                        @autoreleasepool {
                        
                            NSMutableDictionary *tmp_Dict = [[NSMutableDictionary alloc]init];
                            NSDictionary * local= @{
                                @"btDataType" : @"0.00",
                                @"btNull1" : @"0.00",
                                @"btNull2" :@"0.00",
                                @"btSessionIndex" : @"0.00",
                                @"dbBuy_Price_0" : @"0.00",
                                @"dbBuy_Price_1" : @"0.00",
                                @"dbBuy_Price_2" :  @"0.00",
                                @"dbBuy_Price_3" : @"0.00",
                               @"dbBuy_Price_4" :  @"0.00",
                                @"dbClose" :@"0.00",
                                @"dbHigh" :@"0.00",
                                @"dbLTP" :  @"0.00",
                                @"dbLifeHigh " :  @"0.00",
                                @"dbLifeLow " :  @"0.00",
                                @"dbLow " : @"0.00",
                                @"dbOpen " : @"0.00",
                                @"dbSell_Price_0" :  @"0.00",
                                @"dbSell_Price_1" : @"0.00",
                                @"dbSell_Price_2" : @"0.00",
                               @"dbSell_Price_3" : @"0.00",
                               @"dbSell_Price_4" : @"0.00",
                                @"dbTotalValue " :  @"0.00",
                                @"dbVWap " :  @"0.00",
                                @"inBroadCastTime " :  @"0.00",
                                @"inBuy_Order_0" : @"0.00",
                                @"inBuy_Order_1" :  @"0.00",
                                @"inBuy_Order_2" : @"0.00",
                               @"inBuy_Order_3" :  @"0.00",
                                @"inBuy_Order_4" :  @"0.00",
                              @"inBuy_Qty_0" :  @"0.00",
                               @"inBuy_Qty_1" : @"0.00",
                               @"inBuy_Qty_2" : @"0.00",
                               @"inBuy_Qty_3" : @"0.00",
                               @"inBuy_Qty_4" : @"0.00",
                                @"inLTQ " : @"0.00",
                               @" inOpenInterest " : @"0.00",
                                @"inSell_Order_0" : @"0.00",
                                @"inSell_Order_1" : @"0.00",
                                @"inSell_Order_2" :@"0.00",
                               @"inSell_Order_3" : @"0.00",
                                @"inSell_Order_4" : @"0.00",
                                @"inSell_Qty_0" : @"0.00",
                                @"inSell_Qty_1" : @"0.00",
                                @"inSell_Qty_2" : @"0.00",
                                @"inSell_Qty_3" :  @"0.00",
                                @"inSell_Qty_4" :  @"0.00",
                               @"inSeqID " :  @"0.00",
                               @"inTotalBuyQty " :  @"0.00",
                                @"inTotalQtyTraded " :@"0.00",
                                @"inTotalSellQty " :  @"0.00",
                                @"inTradeTime " :  @"0.00",
                                @"stExchange " :  @"0.00",
                                @"stSecurityID " : string_InstrumentTokenStr,
                                @"usAT " :  @"0.00",
                                @"usMsgCode " :  @"0.00",
                                @"usSize " : @"0.00",
                            };
                            [tmp_Dict setObject:local forKey:string_InstrumentTokenStr];
                            [sortedArray addObject:tmp_Dict];
                            
                        }
                        
                    }
                    
                    
                }
                
                
                
                //      sortedArray=[[NSMutableArray alloc]init];
                
                //    for(int i=0;i<exchangeTokenArray.count;i++)
                //    {
                //        NSString * first=[exchangeTokenArray objectAtIndex:i];
                //
                //
                //
                //        for(int j=0;j<delegate2.marketWatch1.count;j++)
                //        {
                //            NSString * second=[NSString stringWithFormat:@"%@",[[delegate2.marketWatch1 objectAtIndex:j] objectForKey:@"stSecurityID"]];
                //
                //            if([second containsString:first])
                //            {
                //
                //
                //
                //                NSMutableDictionary * newDict=[[NSMutableDictionary alloc]init];
                //
                //                [newDict setObject:[delegate2.marketWatch1 objectAtIndex:j] forKey:first];
                //
                //
                //
                //                [sortedArray addObject:newDict];
                //
                //            }
                //
                //            else
                //
                //            {
                //                NSMutableArray * keyArray=[[NSMutableArray alloc]init];
                //                for(int k=0;k<sortedArray.count;k++)
                //                {
                //                    NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
                //
                //                    dict=[sortedArray objectAtIndex:k];
                //
                //                    NSMutableArray * testArray1=[[NSMutableArray alloc]initWithArray:[dict allKeys]];
                //
                //
                //                    [keyArray addObject:[testArray1 objectAtIndex:0]];
                //
                //
                //                }
                //
                //                if([keyArray  containsObject:first])
                //                {
                //
                //                }
                //
                //                else
                //                {
                //
                //                @autoreleasepool {
                //
                //                    NSMutableDictionary * newDict=[[NSMutableDictionary alloc]init];
                //
                //                    [newDict setObject:[NSDictionary new] forKey:first];
                //
                //
                //
                //                    [sortedArray addObject:newDict];
                //
                //                }
                //
                //                }
                //
                //
                //
                //            }
                //        }
                //
                //
                //
                //    }
                
                
                
                
                //    if(sortedArray.count==exchangeTokenArray.count)
                //    {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                
                    
                    if(symbol.count>0&&sortedArray.count>0)
                    {
                        
                        NSString * ltp=[NSString stringWithFormat:@"%.2f",[[[[sortedArray  objectAtIndex:0] objectForKey:[exchangeTokenArray objectAtIndex:0]]objectForKey:@"dbLTP"]floatValue]];
                        NSString * close=[NSString stringWithFormat:@"%.2f",[[[[sortedArray  objectAtIndex:0] objectForKey:[exchangeTokenArray objectAtIndex:0]]objectForKey:@"dbClose"]floatValue]];
                        NSString * open=[NSString stringWithFormat:@"%.2f",[[[[sortedArray  objectAtIndex:0] objectForKey:[exchangeTokenArray objectAtIndex:0]]objectForKey:@"dbOpen"]floatValue]];
                        NSString * high=[NSString stringWithFormat:@"%.2f",[[[[sortedArray  objectAtIndex:0] objectForKey:[exchangeTokenArray objectAtIndex:0]]objectForKey:@"dbHigh"]floatValue]];
                        NSString * low=[NSString stringWithFormat:@"%.2f",[[[[sortedArray  objectAtIndex:0] objectForKey:[exchangeTokenArray objectAtIndex:0]]objectForKey:@"dbLow"]floatValue]];
                        
                        changeFloatValue = [ltp floatValue]-[close floatValue];
                        changeFloatPercentageValue = (([ltp floatValue]-[close floatValue]) *100/[close floatValue]);
                        
                        self.ltpLbl.text=ltp;
                        if([[NSString stringWithFormat:@"%.2f",changeFloatValue] containsString:@"-"])
                        {
                            
                            
                            self.chngLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                        }
                        
                        else
                        {
                            
                            self.chngLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                            
                        }
                        if([[NSString stringWithFormat:@"%.2f",changeFloatPercentageValue] containsString:@"-"])
                        {
                            
                            
                            self.chngperLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                        }
                        
                        else
                        {
                            
                            self.chngperLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                            
                        }
                        self.chngLbl.text=[NSString stringWithFormat:@"%.2f",changeFloatValue];
                        NSString * per=@"%";
                        self.chngperLbl.text=[NSString stringWithFormat:@"%.2f%@",changeFloatPercentageValue,per];
                        self.cashOpen.text=open;
                        self.cashHigh.text=high;
                        self.cashLow.text=low;
                        self.cashClose.text=close;
                        self.cashBidSize.text=[NSString stringWithFormat:@"%@",[[[sortedArray  objectAtIndex:0] objectForKey:[exchangeTokenArray objectAtIndex:0]]objectForKey:@"inBuy_Qty_0"]];
                        self.askBidSize.text=[NSString stringWithFormat:@"%@",[[[sortedArray  objectAtIndex:0] objectForKey:[exchangeTokenArray objectAtIndex:0]]objectForKey:@"inSell_Qty_0"]];
                        self.bidLbl.text=[NSString stringWithFormat:@"%.2f",[[[[sortedArray  objectAtIndex:0] objectForKey:[exchangeTokenArray objectAtIndex:0]]objectForKey:@"dbBuy_Price_0"]floatValue]];
                        self.askLbl.text=[NSString stringWithFormat:@"%.2f",[[[[sortedArray  objectAtIndex:0] objectForKey:[exchangeTokenArray objectAtIndex:0]]objectForKey:@"dbSell_Price_0"]floatValue]];
                        
                        
                    }
                    
                    @try {
                        
                        if(symbol1.count>0&&sortedArray.count>0)
                        {
                            if([delegate2.exchaneLblStr containsString:@"CDS"])
                            {
                            
                            
                            }
                            else
                            {
                                if(symbol.count>0&&sortedArray.count>0)
                                {
                                    
                                    NSString * futltp=[NSString stringWithFormat:@"%.2f",[[[[sortedArray  objectAtIndex:1] objectForKey:[exchangeTokenArray objectAtIndex:1]]objectForKey:@"dbLTP"]floatValue]];
                                    
                                    NSString * futclose=[NSString stringWithFormat:@"%.2f",[[[[sortedArray  objectAtIndex:1] objectForKey:[exchangeTokenArray objectAtIndex:1]]objectForKey:@"dbClose"]floatValue]];
                                    
                                    
                                    
                                    
                                    float futchangeFloatValue = [futltp floatValue]-[futclose floatValue];
                                    float futchangeFloatPercentageValue = (([futltp floatValue]-[futclose floatValue]) *100/[futclose floatValue]);
                                    self.FutLtpLbl.text=futltp;
                                    NSString * percent = @"%";
                                    NSString * per=@"%";
                                    if([[NSString stringWithFormat:@"%.2f",futchangeFloatValue] containsString:@"-"])
                                    {
                                        
                                        
                                        self.futChgLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                                    }
                                    
                                    else
                                    {
                                        
                                        self.futChgLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                                        
                                    }
                                    if([[NSString stringWithFormat:@"%.2f",futchangeFloatPercentageValue] containsString:@"-"])
                                    {
                                        
                                        
                                        self.futChgPerLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                                    }
                                    
                                    else
                                    {
                                        
                                        self.futChgPerLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                                        
                                    }
                                    self.futChgLbl.text=[NSString stringWithFormat:@"%.2f",futchangeFloatValue];
                                    self.futChgPerLbl.text=[NSString stringWithFormat:@"%.2f%@",futchangeFloatPercentageValue,per];
                                    
                                    self.openLbl.text=[NSString stringWithFormat:@"%.2f",[[[[sortedArray  objectAtIndex:1] objectForKey:[exchangeTokenArray objectAtIndex:1]]objectForKey:@"dbOpen"]floatValue]];
                                    self.highLbl.text=[NSString stringWithFormat:@"%.2f",[[[[sortedArray  objectAtIndex:1] objectForKey:[exchangeTokenArray objectAtIndex:1]]objectForKey:@"dbHigh"]floatValue]];
                                    self.lowLbl.text=[NSString stringWithFormat:@"%.2f",[[[[sortedArray  objectAtIndex:1] objectForKey:[exchangeTokenArray objectAtIndex:1]]objectForKey:@"dbLow"]floatValue]];
                                    self.closeLbl.text=[NSString stringWithFormat:@"%.2f",[[[[sortedArray  objectAtIndex:1] objectForKey:[exchangeTokenArray objectAtIndex:1]]objectForKey:@"dbClose"]floatValue]];
                                    self.bidQtyLbl.text=[NSString stringWithFormat:@"%@",[[[sortedArray  objectAtIndex:1] objectForKey:[exchangeTokenArray objectAtIndex:1]]objectForKey:@"inBuy_Qty_0"]];
                                    self.askQtyLbl.text=[NSString stringWithFormat:@"%@",[[[sortedArray  objectAtIndex:1] objectForKey:[exchangeTokenArray objectAtIndex:1]]objectForKey:@"inSell_Qty_0"]];
                                    self.bidPriceLbl.text=[NSString stringWithFormat:@"%.2f",[[[[sortedArray  objectAtIndex:1] objectForKey:[exchangeTokenArray objectAtIndex:1]]objectForKey:@"dbBuy_Price_0"]floatValue] ];
                                    self.askPriceLbl.text=[NSString stringWithFormat:@"%.2f",[[[[sortedArray  objectAtIndex:1] objectForKey:[exchangeTokenArray objectAtIndex:1]]objectForKey:@"dbSell_Price_0"]floatValue]];
                                        if(futltp.length>0&&![futltp isEqualToString:@"0.00"])
                                   
                                    {
                                        if(FUTCheck==true)
                                        {
                                            [self optionCEMethod];
                                        }
                                        
                                    }
                                    
                                }
                                else
                                {
                                    NSString * futltp=[NSString stringWithFormat:@"%.2f",[[[[sortedArray  objectAtIndex:0] objectForKey:[exchangeTokenArray objectAtIndex:0]]objectForKey:@"dbLTP"]floatValue]];
                                    
                                    NSString * futclose=[NSString stringWithFormat:@"%.2f",[[[[sortedArray  objectAtIndex:0] objectForKey:[exchangeTokenArray objectAtIndex:0]]objectForKey:@"dbClose"]floatValue]];
                                    
                                    
                                    
                                    
                                    float futchangeFloatValue = [futltp floatValue]-[futclose floatValue];
                                    float futchangeFloatPercentageValue = (([futltp floatValue]-[futclose floatValue]) *100/[futclose floatValue]);
                                    self.FutLtpLbl.text=futltp;
                                    
                                    if([[NSString stringWithFormat:@"%.2f",futchangeFloatValue] containsString:@"-"])
                                    {
                                        
                                        
                                        self.futChgLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                                    }
                                    
                                    else
                                    {
                                        
                                        self.futChgLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                                        
                                    }
                                    if([[NSString stringWithFormat:@"%.2f",futchangeFloatPercentageValue] containsString:@"-"])
                                    {
                                        
                                        
                                        self.futChgPerLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                                    }
                                    
                                    else
                                    {
                                        
                                        self.futChgPerLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                                        
                                    }
                                    self.futChgLbl.text=[NSString stringWithFormat:@"%.2f",futchangeFloatValue];
                                    
                                    NSString * per=@"%";
                                    self.futChgPerLbl.text=[NSString stringWithFormat:@"%.2f%@",futchangeFloatPercentageValue,per];
                                    
                                    self.openLbl.text=[NSString stringWithFormat:@"%.2f",[[[[sortedArray  objectAtIndex:0] objectForKey:[exchangeTokenArray objectAtIndex:0]]objectForKey:@"dbOpen"]floatValue]];
                                    self.highLbl.text=[NSString stringWithFormat:@"%.2f",[[[[sortedArray  objectAtIndex:0] objectForKey:[exchangeTokenArray objectAtIndex:0]]objectForKey:@"dbHigh"]floatValue]];
                                    self.lowLbl.text=[NSString stringWithFormat:@"%.2f",[[[[sortedArray  objectAtIndex:0] objectForKey:[exchangeTokenArray objectAtIndex:0]]objectForKey:@"dbLow"]floatValue]];
                                    self.closeLbl.text=[NSString stringWithFormat:@"%.2f",[[[[sortedArray  objectAtIndex:0] objectForKey:[exchangeTokenArray objectAtIndex:0]]objectForKey:@"dbClose"]floatValue]];
                                    self.bidQtyLbl.text=[NSString stringWithFormat:@"%@",[[[sortedArray  objectAtIndex:0] objectForKey:[exchangeTokenArray objectAtIndex:0]]objectForKey:@"inBuy_Qty_0"]];
                                    self.askQtyLbl.text=[NSString stringWithFormat:@"%@",[[[sortedArray  objectAtIndex:0] objectForKey:[exchangeTokenArray objectAtIndex:0]]objectForKey:@"inSell_Qty_0"]];
                                    self.bidPriceLbl.text=[NSString stringWithFormat:@"%.2f",[[[[sortedArray  objectAtIndex:0] objectForKey:[exchangeTokenArray objectAtIndex:0]]objectForKey:@"dbBuy_Price_0"]floatValue]];
                                    self.askPriceLbl.text=[NSString stringWithFormat:@"%.2f",[[[[sortedArray  objectAtIndex:0] objectForKey:[exchangeTokenArray objectAtIndex:0]]objectForKey:@"dbSell_Price_0"]floatValue]];
                                    
                                   if(futltp.length>0&&![futltp isEqualToString:@"0.00"])
                                    {
                                        if(FUTCheck==true)
                                        {
                                            if([delegate2.exchaneLblStr containsString:@"MCX"])
                                            {
                                                
                                            }
                                            
                                            else
                                            {
                                                [self optionCEMethod];
                                            }
                                        }
                                        
                                    }
                                    
                                    
                                    
                                }
                            }
                        }
                        
                        
                        
                    } @catch (NSException *exception) {
                        
                    } @finally {
                        
                    }
                    
                    
                    
              
                
                //
                //
                //
                //
                
                
                if(symbol2.count>0&&symbol3.count>0)
                {
                 
                    if(sortedArray.count>3)
                    {
                        
                        //    if(sortedArray.count==exchangeTokenArray.count)
                        //    {
                        
                        @try
                        {
                        
                        
                        newSortArray=[[NSMutableArray alloc]init];
                        newInstrumentToken=[[NSMutableArray alloc]init];
                        
                        newSortArray=[sortedArray mutableCopy];
                        newInstrumentToken=[exchangeTokenArray mutableCopy];
                        
                        //        if(check==true)
                        //        {
                        if([delegate2.exchaneLblStr containsString:@"CDS"]||[delegate2.symbolDepthStr containsString:@"NIFTY"])
                        {
                            [newSortArray removeObjectAtIndex:0];
                            
                            [newInstrumentToken removeObjectAtIndex:0];
                            
                        }
                        
                        else
                        {
                            [newSortArray removeObjectAtIndex:0];
                            [newSortArray removeObjectAtIndex:0];
                            [newInstrumentToken removeObjectAtIndex:0];
                            [newInstrumentToken removeObjectAtIndex:0];
                        }
                        
                        
                        //            check=false;
                        //        }
                        
                      
                        
                        testArray = [[NSMutableArray alloc]init];
                        testDictionary = [[NSMutableDictionary alloc]init];
                        
                        
                        CEArray = [[NSMutableArray alloc]init];
                        PEArray = [[NSMutableArray alloc]init];
                            @try
                            {
                        for(int i=0;i<newInstrumentToken.count;i++)
                        {
                            
                            if(i%2==0)
                            {
                                [CEArray addObject:[newInstrumentToken objectAtIndex:i]];
                            }else
                            {
                                [PEArray addObject:[newInstrumentToken objectAtIndex:i]];
                            }
                        }
                            } @catch (NSException *exception) {
                                
                            } @finally {
                                
                            }
                            
                        
                        } @catch (NSException *exception) {
                            
                        } @finally {
                            
                        }
                        
                        
                        CEDetailArray =[[NSMutableArray alloc]init];
                        PEDetailArray=[[NSMutableArray alloc]init];
                        
                        @try
                        {
                        for(int i=0;i<CEArray.count;i++)
                        {
                            
                            for(int j=0;j<newSortArray.count;j++)
                            {
                                NSMutableDictionary * filterDict=[[NSMutableDictionary alloc]init];
                                filterDict=[newSortArray objectAtIndex:j];
                                
                                NSArray * filterArray=[[NSArray alloc]init];
                                
                                filterArray=[filterDict allKeys];
                                
                                NSString * string3=[filterArray objectAtIndex:0];
                                //NSLog(@"%@",string3);
                                
                                
                                
                                NSString * string2=[NSString stringWithFormat:@"%@",[CEArray objectAtIndex:i]];
                                //NSLog(@"%@",string2);
                                
                                
                                
                                NSString * string4=[NSString stringWithFormat:@"%@",[PEArray objectAtIndex:i]];
                                //NSLog(@"%@",string4);
                                
                                
                                if([string3 isEqualToString:string2])
                                {
                                    [CEDetailArray addObject:[newSortArray objectAtIndex:j]];
                                }
                                else if([string3 isEqualToString:string4])
                                {
                                    [PEDetailArray addObject:[newSortArray objectAtIndex:j]];
                                }
                            }
                        }
                        
                        } @catch (NSException *exception) {
                            
                        } @finally {
                            
                        }
                        
                        
                        
                        //        NSMutableArray *selectedObjects = [NSMutableArray array];
                        //
                        //        for(int i=0;i<newSortArray.count;i++)
                        //        {
                        //            if(newSortArray.count/2)
                        //            {
                        //        testDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:newSortArray[i],@"one", nil];
                        //               // [testDictionary dictionaryWithValuesForKeys:newSortArray[i]];
                        //                [testDictionary setObject:newSortArray[i] forKey:@"one"];
                        //            }
                        //
                        //        }
                        //        //NSLog(@"test Dicitionary:%@",testDictionary );
                        //        [testArray addObject:[testDictionary allValues]];
                        //        //NSLog(@"Test array0:%@",[testArray objectAtIndex:0]);
                        //        //NSLog(@"Test array1:%@",[testArray objectAtIndex:1]);
                        //        NSIndexSet *index = [newSortArray indexesOfObjectsPassingTest:^BOOL(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        //            return [testArray containsObject:obj];
                        //        }];
                        //       NSArray* testArray1 = @[newSortArray[0,1]];
                        //
                        //        //NSLog(@"Test array1:%@",testArray1);
                        //        //NSLog(@"Index: %@",index);
                        //        //NSLog(@"test Array:%@",testArray);
                        
                        
                        //        if(exchangeTokenArray.count-2==[[[callPutDictionary objectForKey:titleStr] objectAtIndex:0] count])
                        //         dispatch_async(dispatch_get_main_queue(), ^{
                        //
                        //        self.depthTableView.delegate=self;
                        //        self.depthTableView.dataSource=self;
                        //        [self.depthTableView reloadData];
                        //
                        //             self.activityIndicator.hidden=YES;
                        //             [self.activityIndicator stopAnimating];
                        //
                        //
                        //           });
                        
                        //        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0),
                        //                       ^{
                        //                           [self performSelector:@selector(methodThatloadYourData)];
                        //                           dispatch_async(dispatch_get_main_queue(),
                        //                                          ^{
                        //                                              self.depthTableView.delegate=self;
                        //                                              self.depthTableView.dataSource=self;
                        //                                              [self.depthTableView reloadData];
                        //
                        //                                              self.activityIndicator.hidden=YES;
                        //                                              [self.activityIndicator stopAnimating];
                        //                                          });
                        //                       });
                        //
                        
                        
//                        dispatch_async(dispatch_get_main_queue(),
//                                                                               ^{
//
//                        self.depthTableView.delegate=self;
//                        self.depthTableView.dataSource=self;
//                        [self.depthTableView reloadData];
//
//                                                                               });
//
                        
                    }
                    
                    
                    
                   
                    
                    
                   
                    long i=[[[callPutDictionary objectForKey:titleStr] objectAtIndex:0] count];
                   
                   
                    if(i==CEDetailArray.count)
                    {
                         newCEDetailArray=[[NSMutableArray alloc]init];
                    newCEDetailArray=CEDetailArray;
                    }
                    if(i==PEDetailArray.count)
                    {
                        
                         newPEDetailArray=[[NSMutableArray alloc]init];
                        newPEDetailArray=PEDetailArray;
                    }
                   
                    
                }
                                                        
                                                        
                                   
                
                else
                {
                    
                    self.activityIndicator.hidden=YES;
                    [self.activityIndicator stopAnimating];
                }
                
                
                
              
                 });
                
                
                
                
                
                
            }
//             });
           }
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        
        
   
    
   
   
        
    
}



- (void)methodThatloadYourData {
    
    
}


-(void)unSubMethod
{
    
    
    @try
    {
    
   if(upStoxEQSymbolArray.count>0)
   {
    
    
    NSString * result = [[upStoxEQSymbolArray valueForKey:@"description"] componentsJoinedByString:@","];
    
    NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
    NSDictionary *headers = @{ @"x-api-key": delegate2.APIKey,
                               @"authorization": access,
                               };
    
   
   
       
       NSString * urlString;
       if([self.cashSegment.text containsString:@"NSE"])
       {
           NSString * exchange=@"nse_eq";
           urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/%@/?symbol=%@",exchange,result];
       }
       
       
       else if([self.cashSegment.text containsString:@"BSE"])
       {
           
           NSString * exchange=@"bse_eq";
           urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/%@/?symbol=%@",exchange,result];
       }
       
        
   if(urlString.length>0)
   {
   
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:40.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData* data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSMutableArray * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        //NSLog(@"%@",json);
                                                    }
                                                    
                                                    
//                                                    dispatch_async(dispatch_get_main_queue(), ^{
//
//
//                                                        [self unSubMethod1];
//
//
//
//
//                                                    });
                                                }];
    [dataTask resume];
       
   }
   }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
-(void)unSubMethod1
{
    
    
    @try
    {
    
    if(upStoxFUTSymbolArray.count>0)
    
    {
    NSString * result = [[upStoxFUTSymbolArray valueForKey:@"description"] componentsJoinedByString:@","];
    
    NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
    NSDictionary *headers = @{ @"x-api-key": delegate2.APIKey,
                               @"authorization": access,
                               };
    
        NSString * urlString;
        
        if([self.futureSegment.text containsString:@"CDS"])
        {
            NSString * exchange=@"ncd_fo";
            urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/%@/?symbol=%@",exchange,result];
            
        }
        
        else  if([self.futureSegment.text containsString:@"NFO"])
        { NSString * exchange=@"nse_fo";
            urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/%@/?symbol=%@",exchange,result];
            
        }
        else  if([self.futureSegment.text containsString:@"BFO"])
        {
            NSString * exchange=@"bse_fo";
            urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/%@/?symbol=%@",exchange,result];
        }
        else  if([self.futureSegment.text containsString:@"MCX"])
        {
            NSString * exchange=@"mcx_fo";
            urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/%@/?symbol=%@",exchange,result];
        }
        
        
  
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:40.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData* data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSMutableArray * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        //NSLog(@"%@",json);
                                                    }
                                                    
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        [upStoxEQSymbolArray removeAllObjects];
                                                         [upStoxFUTSymbolArray removeAllObjects];
//                                                        if(dismissCheck==true)
//                                                        {
//
//                                                            dismissCheck=false;
//                                                        }
//
//                                                        else
//                                                        {
//
//                                                             [self subMethod];
//
//                                                        }
                                                       
                                                            
                                                        
                                                        
                                                        
                                                        
                                                        
                                                    });
                                                }];
    [dataTask resume];
        
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}



-(void)subMethod
{
    
  @try
    {
    if(upStoxEQSymbolArray.count>0)
    {
//      if([upStoxEQSymbolArray containsObject:delegate2.symbolDepthStr])
//      {
//          [upStoxEQSymbolArray removeObject:delegate2.symbolDepthStr];
//
//          if(upStoxFUTSymbolArray.count>0)
//          {
//
//              [self futSubMethod];
//
//
//          }
//
//          else
//          {
//
//              [self websocket];
//          }
//
//      }
//        else
//        {
            NSString * result = [[upStoxEQSymbolArray valueForKey:@"description"] componentsJoinedByString:@","];
        CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)result, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
        
        result=[NSString stringWithFormat:@"%@",newString];
            
            NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
            NSDictionary *headers = @{ @"x-api-key": delegate2.APIKey,
                                       @"authorization": access,
                                       };
            
            NSString * urlString;
            if([self.cashSegment.text containsString:@"NSE"])
            {
                NSString * exchange=@"nse_eq";
                urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/%@/?symbol=%@",exchange,result];
            }
            
            
            else if([self.cashSegment.text containsString:@"BSE"])
            {
                
                NSString * exchange=@"bse_eq";
                urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/%@/?symbol=%@",exchange,result];
            }
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:40.0];
            [request setHTTPMethod:@"GET"];
            [request setAllHTTPHeaderFields:headers];
            
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                        completionHandler:^(NSData* data, NSURLResponse *response, NSError *error) {
                                                            if (error) {
                                                                //NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *) response;
                                                                //NSLog(@"%@", httpResponse);
                                                                
                                                                
                                                                NSMutableArray * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                
                                                                //NSLog(@"%@",json);
                                                            }
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                if(upStoxFUTSymbolArray.count>0)
                                                                {
                                                                    
                                                                [self futSubMethod];
                                                                    
                                                                    
                                                                }
                                                                
                                                                else
                                                                {
                                                                    
                                                                    [self websocket];
                                                                }
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                            });
                                                            
                                                            
                                                            
                                                        }];
            [dataTask resume];
            
        }
        else
        {
             [self futSubMethod];
        }
   
//    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)futSubMethod
{@try
    {
    NSMutableArray * localArray=[[NSMutableArray alloc]init];
    
    
  if(upStoxFUTSymbolArray.count>0)
  {
//      if([upStoxFUTSymbolArray containsObject:delegate2.symbolDepthStr])
//      {
//          [upStoxFUTSymbolArray removeObject:delegate2.symbolDepthStr];
//      }
//      else
//      {
//
//      }
    for(int i=1;i<upStoxFUTSymbolArray.count;i++)
    {
        [localArray addObject:[upStoxFUTSymbolArray objectAtIndex:i]];
        
    }
    
    NSString * result;
    if(symbol2.count>0&&symbol3.count>0)
    {
    result = [[localArray valueForKey:@"description"] componentsJoinedByString:@","];
        
    }
    
    else
    {
         result = [[upStoxFUTSymbolArray valueForKey:@"description"] componentsJoinedByString:@","];
    }
      
      CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)result, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
      
      result=[NSString stringWithFormat:@"%@",newString];
    
    NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
    NSDictionary *headers = @{ @"x-api-key": delegate2.APIKey,
                               @"authorization": access,
                               };
      
       //self.futureSegment.text=@"CDS-FUT";
      
      NSString * urlString;
      
      if([self.futureSegment.text containsString:@"CDS"])
      {
          NSString * exchange=@"ncd_fo";
          urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/%@/?symbol=%@",exchange,result];
          
      }
      
      else  if([self.futureSegment.text containsString:@"NFO"])
      { NSString * exchange=@"nse_fo";
          urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/%@/?symbol=%@",exchange,result];
      }
      else  if([self.futureSegment.text containsString:@"BFO"])
      {
          NSString * exchange=@"bse_fo";
          urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/%@/?symbol=%@",exchange,result];
      }
      else  if([self.futureSegment.text containsString:@"MCX"])
      {
          NSString * exchange=@"mcx_fo";
          urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/%@/?symbol=%@",exchange,result];
      }
   
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:40.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData* data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSMutableArray * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        //NSLog(@"%@",json);
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                       
                                                        [self websocket];

                                                        
                                                        
                                                    });
                                                    
                                                    
                                                    
                                                }];
    [dataTask resume];
  }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}



@end
