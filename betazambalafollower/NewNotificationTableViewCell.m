//
//  NewNotificationTableViewCell.m
//  zambala leader
//
//  Created by zenwise mac 2 on 3/15/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "NewNotificationTableViewCell.h"

@implementation NewNotificationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.bulletView.layer.cornerRadius = self.bulletView.frame.size.width/2;
    self.bulletView.layer.masksToBounds = YES;
    self.bulletView.layer.borderWidth = 1.0f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
