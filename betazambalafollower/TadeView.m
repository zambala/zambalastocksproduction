//
//  TadeView.m
//  testing
//
//  Created by zenwise technologies on 23/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "TadeView.h"
#import "TradeCellTableViewCell.h"
#import "TabBar.h"
#import "AppDelegate.h"
#import "NoTradesCell.h"
#import "TagEncode.h"

#import "MT.h"

#import <Foundation/Foundation.h>
#import <CoreFoundation/CoreFoundation.h>
#import <Endian.h>
#import "MTORDER.h"
#import <Mixpanel/Mixpanel.h>
#import "BrokerViewNavigation.h"
#import "OpenAccountView.h"
#define buyColor [UIColor colorWithRed:37.0/255.0 green:114.0/255.0 blue:191.0/255.0 alpha:1.0]
#define sellColor [UIColor colorWithRed:206.0/255.0 green:38.0/255.0 blue:38.0/255.0 alpha:1.0]
@interface TadeView ()<NSStreamDelegate>
{
    AppDelegate *delegate2;
    NSArray * filterArray;
    NSMutableArray * symbolArray;
    NSMutableArray * newSymbolArray;
    NSMutableArray * detailsArray;
    NSString * filterString1;
    //mt//
    
    MT *sharedManager;
    MTORDER * sharedManagerOrder;
    NSMutableArray * newSymbolResponseArray;
    NSMutableArray * securityTokenArray;
    NSMutableArray * tradeOrderIDArray;
    NSMutableArray * mtExchangeArray;
    NSDictionary * loginReplyDict;
    NSMutableArray * tradeDetailsArray;
    NSIndexPath * selectedIndex;
     BOOL expandBool;
}

@end

@implementation TadeView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    expandBool = false;
    delegate2=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    filterArray=[[NSArray alloc]init];
    symbolArray=[[NSMutableArray alloc]init];
    newSymbolArray=[[NSMutableArray alloc]init];
    delegate2.tradeCompletedArray=[[NSMutableArray alloc]init];
    self.tradeTableView.hidden=YES;
    orderIDArray = [[NSMutableArray alloc]init];
    tradingSymArray = [[NSMutableArray alloc]init];
    orderTimeArray = [[NSMutableArray alloc]init];
    exchangeArray = [[NSMutableArray alloc]init];
    orderTypeArray = [[NSMutableArray alloc]init];
    transactionTypeArray = [[NSMutableArray alloc]init];
    priceArray = [[NSMutableArray alloc]init];
    totalQtyArray = [[NSMutableArray alloc]init];
    newSymbolResponseArray = [[NSMutableArray alloc]init];
    productArray = [[NSMutableArray alloc]init];
    self.controller.delegate=self;
    self.controller.searchResultsUpdater=self;
    //mt//
    sharedManager = [MT Mt1];
    sharedManagerOrder = [MTORDER Mt2];
    
}
-(void)loginCheck
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Please choose an option." message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [self presentViewController:alert animated:YES completion:^{
        
    }];
   
    UIAlertAction * Login=[UIAlertAction actionWithTitle:@"Login" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //Navigate to login page
        NSUserDefaults *loggedInUserNew  = [NSUserDefaults standardUserDefaults];
        //
        delegate2.loginActivityStr=@"";
        //             delegate1.loginActivityStr=@"";
        [loggedInUserNew removeObjectForKey:@"loginActivityStr"];
        
        
        
        BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
        
        [self presentViewController:view animated:YES completion:nil];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
   
    [alert addAction:Login];
    [alert addAction:cancel];
    
}



-(void)viewDidAppear:(BOOL)animated
{
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"trades_page"];
    [self.activityInd startAnimating];
    self.activityInd.hidden=NO;
     self.timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(hideMethod) userInfo:nil repeats:YES];
    if([delegate2.loginActivityStr isEqualToString:@"OTP1"])
    {
        [self loginCheck];
        
    }
    
    else
    {
        
    }
    
  
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
        [self GetTraders];
    }
    
    else
    {
//        delegate2.scripsMt=[[NSMutableArray alloc]init];
//        delegate2.allTradeArray=[[NSMutableArray alloc]init];
//        TagEncode * tag = [[TagEncode alloc]init];
//        delegate2.mtCheck=true;
//        [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.OrderDownloadRequest];
//        [tag GetBuffer];
//
//        [self newMessage];
//
//        delegate2.scripsMt=[[NSMutableArray alloc]init];
//        delegate2.allTradeArray=[[NSMutableArray alloc]init];
//        tag = [[TagEncode alloc]init];
//        delegate2.mtCheck=true;
//        [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.TradeDownloadRequest];
//        [tag GetBuffer];
//
//        [self newMessage];
        
        [self mttrades];
    
}

}
-(void)mttrades
{
    @try
    {
    TagEncode * enocde=[[TagEncode alloc]init];
    loginReplyDict=[[NSDictionary alloc]init];
    enocde.inputRequestString=@"trade";
    
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    tradeDetailsArray=[[NSMutableArray alloc]init];
    [inputArray addObject:delegate2.accessToken];
    [inputArray addObject:delegate2.userID];
    [inputArray addObject:[NSString stringWithFormat:@"%@multitradeapi/traderequest",delegate2.mtBaseUrl]];
    
    
    [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        NSArray * keysArray=[dict allKeys];
        
        
        if(keysArray.count>0)
        {
        loginReplyDict=dict;
        
        tradeDetailsArray =[[[loginReplyDict objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"trades"];
           tradeDetailsArray = [[[tradeDetailsArray reverseObjectEnumerator] allObjects] mutableCopy];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.tradeTableView.delegate=self;
            self.tradeTableView.dataSource=self;
            [self.tradeTableView reloadData];
        });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                
                UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self mttrades];
                }];
                
                UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                
                
                [alert addAction:retryAction];
                [alert addAction:cancel];
                
            });
            
        }
        
    }];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
-(void)hideMethod
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.tradeTableView.hidden=NO;
        self.activityInd.hidden=YES;
        [self.activityInd stopAnimating];
        
        self.tradeTableView.delegate=self;
        self.tradeTableView.dataSource=self;
        
        [self.tradeTableView reloadData];
        
    });
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)GetTraders
{
    
    @try
    {
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSURL *url;
    
    NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
    NSDictionary * headers;
    
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
    {
    url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.kite.trade/trades?api_key=%@&access_token=%@",delegate2.APIKey,delegate2.accessToken]];
        
    }
    
    else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.upstox.com/live/trade-book"]];
        
        headers  = @{ @"cache-control": @"no-cache",
                      @"Content-Type" : @"application/json",
                      @"authorization":access,
                      @"x-api-key":delegate2.APIKey
                      };
    }
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request setHTTPMethod:@"GET"];
    if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
        [request setAllHTTPHeaderFields:headers];
    }
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(data!=nil)
        {
        NSDictionary *traderResponse=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        //NSLog(@"orderDict----%@",traderResponse);
        
        
        tradeArray = [traderResponse valueForKey:@"data"];
        
        for (int i = 0; i<[tradeArray count]; i++)
        {
            if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
            {
            orderIdStr = [[tradeArray objectAtIndex:i]valueForKey:@"order_id"];
            tradingSymStr = [[tradeArray objectAtIndex:i]valueForKey:@"tradingsymbol"];
            orderTimeStr = [[tradeArray objectAtIndex:i]valueForKey:@"order_timestamp"];
            exchangeStr = [[tradeArray objectAtIndex:i]valueForKey:@"exchange"];
//            fillQtyStr = [[[tradeArray objectAtIndex:i]valueForKey:@"filled_quantity"]stringValue];
//            pendingQtyStr = [[[tradeArray objectAtIndex:i]valueForKey:@"pending_quantity"]stringValue];
//            orderTypeStr = [[tradeArray objectAtIndex:i]valueForKey:@"order_type"];
            transactionTypeStr = [[tradeArray objectAtIndex:i]valueForKey:@"transaction_type"];
            priceStr = [[[tradeArray objectAtIndex:i]valueForKey:@"average_price"]stringValue];
            totalQtyStr = [[[tradeArray objectAtIndex:i]valueForKey:@"quantity"]stringValue];
            productStr = [[tradeArray objectAtIndex:i]valueForKey:@"product"];
            if([productStr isEqualToString:@"CNC"])
            {
                productStr = @"Delivery";
            }
            else if([productStr isEqualToString:@"MIS"])
            {
                productStr = @"Intraday";
            }
                
                orderTypeStr=@"--";
                
//                if([orderTypeStr isEqualToString:@"L"])
//                {
//                    orderTypeStr = @"LIMIT";
//                }
//
//                else if([orderTypeStr isEqualToString:@"M"])
//                {
//                    orderTypeStr = @"MARKET";
//
//                }
//
//                else if([orderTypeStr isEqualToString:@"SL"])
//                {
//                    orderTypeStr = @"STOP LOSS";
//
//                }
                
                
            
            [orderIDArray addObject:orderIdStr];
            [tradingSymArray addObject:tradingSymStr];
            [orderTimeArray addObject:orderTimeStr];
            [exchangeArray addObject:exchangeStr];
//            [filledArray addObject:fillQtyStr];
//            [pendingArray addObject:pendingQtyStr];
//            [orderTypeArray addObject:orderTypeStr];
            [transactionTypeArray addObject:transactionTypeStr];
            [priceArray addObject:priceStr];
            [totalQtyArray addObject:totalQtyStr];
            [productArray addObject:productStr];
            [orderTypeArray addObject:orderTypeStr];
            
            }
            
            else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
            {
                
                orderIdStr = [[tradeArray objectAtIndex:i]valueForKey:@"trade_id"];
                tradingSymStr = [[tradeArray objectAtIndex:i]valueForKey:@"symbol"];
                orderTimeStr = [[tradeArray objectAtIndex:i]valueForKey:@"exchange_time"];
                exchangeStr = [[tradeArray objectAtIndex:i]valueForKey:@"exchange"];
                productStr = [[tradeArray objectAtIndex:i]valueForKey:@"product"];
                
                //            fillQtyStr = [[[tradeArray objectAtIndex:i]valueForKey:@"filled_quantity"]stringValue];
                //            pendingQtyStr = [[[tradeArray objectAtIndex:i]valueForKey:@"pending_quantity"]stringValue];
                //            orderTypeStr = [[tradeArray objectAtIndex:i]valueForKey:@"order_type"];
                
                NSString * string=[NSString stringWithFormat:@"%@",[[tradeArray objectAtIndex:i]valueForKey:@"transaction_type"]];
                
                if([string isEqualToString:@"B"])
                {
                     transactionTypeStr = @"BUY";
                }
                
                else if([string isEqualToString:@"S"])
                {
                    transactionTypeStr = @"SELL";
                    
                }
                
                
                orderTypeStr=[NSString stringWithFormat:@"%@",[[tradeArray objectAtIndex:i]valueForKey:@"order_type"]];
                
                if([orderTypeStr isEqualToString:@"L"])
                {
                    orderTypeStr = @"LIMIT";
                }
                
                else if([orderTypeStr isEqualToString:@"M"])
                {
                    orderTypeStr = @"MARKET";
                    
                }
                
                else if([orderTypeStr isEqualToString:@"SL"])
                {
                    orderTypeStr = @"STOP LOSS";
                    
                }
                
                
                
               
               
                priceStr = [[[tradeArray objectAtIndex:i]valueForKey:@"traded_price"]stringValue];
                totalQtyStr = [[[tradeArray objectAtIndex:i]valueForKey:@"traded_quantity"]stringValue];
                
                
                [orderIDArray addObject:orderIdStr];
                [tradingSymArray addObject:tradingSymStr];
                [orderTimeArray addObject:orderTimeStr];
                [exchangeArray addObject:exchangeStr];
                //            [filledArray addObject:fillQtyStr];
                //            [pendingArray addObject:pendingQtyStr];
                //            [orderTypeArray addObject:orderTypeStr];
                [transactionTypeArray addObject:transactionTypeStr];
                [priceArray addObject:priceStr];
                [totalQtyArray addObject:totalQtyStr];
                [productArray addObject:productStr];
                [orderTypeArray addObject:orderTypeStr];
            }
            
            
            
            Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
            
            [mixpanelMini identify:delegate2.userID];
            
//            NSMutableString * string=[[NSMutableString alloc]init];
//
//            for(int i=0;i<tradingSymArray.count;i++)
//            {
//                NSMutableString * string1=[[NSMutableString alloc]init];
//                string1=[NSMutableString stringWithFormat:@"%@,",[tradingSymArray objectAtIndex:i]];
//
//                [string appendString:string1];                                       }
//
//
//
//
            if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
            {
            
            [mixpanelMini.people set:@{@"tradeslist":tradeArray}];
            }
            
            else
            {
            [mixpanelMini.people set:@{@"tradeslist":delegate2.tradeCompletedArray}];
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.tradeTableView.hidden=NO;
            self.tradeTableView.delegate=self;
            self.tradeTableView.dataSource=self;

            [self.tradeTableView reloadData];
            
        });
            
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                
                UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self GetTraders];
                }];
                
                UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                
                
                [alert addAction:retryAction];
                [alert addAction:cancel];
                
            });
            
            
        }
        
    }];
        
    
    
    [postDataTask resume];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtn:(id)sender {
    
//    TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
//    
//    [self presentViewController:tabPage animated:YES completion:nil];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

//search//

- (IBAction)searchAction:(id)sender {
    
    self.controller = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.controller.searchResultsUpdater = self;
    self.controller.hidesNavigationBarDuringPresentation = YES;
    self.controller.dimsBackgroundDuringPresentation = false;
    self.definesPresentationContext = YES;
    //    self.controller.searchBar.scopeButtonTitles = @[@"Posts", @"Users", @"Subreddits"];
    
    
    
    
    
    [self presentViewController:self.controller animated:YES
                     completion:nil];

}

//-(void)updateSearchResultsForSearchController:(UISearchController *)searchController
//{
//
//        @try {
//            if(newSymbolArray.count>0)
//            {
//                [newSymbolArray removeAllObjects];
//
//            }
//            if(symbolArray.count>0)
//            {
//                [symbolArray removeAllObjects];
//            }
//            for(int i=0;i<tradeArray.count;i++)
//            {
//
//                [symbolArray addObject:[[tradeArray objectAtIndex:i]objectForKey:@"tradingsymbol"]];
//            }
//            NSPredicate * predicate=[NSPredicate predicateWithFormat:@"self beginswith[c] %@",searchController.searchBar.text];
//            //NSLog(@"%@",predicate);
//            if(self.controller.searchBar.text.length>0)
//            {
//                filterArray =[[NSArray alloc]init];
//
//
//                filterArray=[symbolArray  filteredArrayUsingPredicate:predicate];
//
//
//
//
//
//
//
//                for(int i=0;i<filterArray.count;i++)
//                {
//
//                    detailsArray=[[NSMutableArray alloc]init];
//
//                    if(newSymbolArray.count>0&&filterArray.count>0)
//                    {
//
//                        filterString1=[filterArray objectAtIndex:i];
//                        for(int k=0;k<newSymbolArray.count;k++)
//                        {
//                            [detailsArray addObject:[[newSymbolArray objectAtIndex:k]objectForKey:@"firstname"]];
//
//                        }
//
//                    }
//                    for(int j=0;j<tradeArray.count;j++)
//                    {
//                        NSString * filterString=[filterArray objectAtIndex:i];
//                        NSString * responseString=[[tradeArray  objectAtIndex:j]objectForKey:@"tradingsymbol"];
//
//
//
//
//                        if(detailsArray.count>0)
//                        {
//
//                            if([detailsArray containsObject:filterString1])
//                            {
//
//
//                            }
//
//
//                            else
//                            {
//                                if([filterString isEqualToString:responseString])
//                                {
//                                    [newSymbolArray addObject:[tradeArray objectAtIndex:j]];
//                                }
//                            }
//
//
//                        }
//
//                        else
//                        {
//                            if([filterString isEqualToString:responseString])
//                            {
//                                [newSymbolArray addObject:[tradeArray objectAtIndex:j]];
//                            }
//                        }
//
//                    }
//                }
//            }
//            [self.tradeTableView reloadData];
//
//        }
//        @catch (NSException * e) {
//            //NSLog(@"Exception: %@", e);
//        }
//        @finally {
//            //NSLog(@"finally");
//        }
//
//}

        
        
    



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(self.controller.active==YES)
    {
        return [newSymbolArray count];
    }
    
    else if(tradeArray.count>0||tradeDetailsArray.count>0)
    {
        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
            return [tradeArray count];
        }
        
        else
        {
            return [tradeDetailsArray count];
            
        }
        
    }
    else
    {
        return 1;
    }
    
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    @try
    {
    if(self.controller.active==YES)
    {
        
        
        if(newSymbolArray.count>0)
        {
            
        
        TradeCellTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
            if(expandBool == false)
            {
                cell.expandImg.image =[UIImage imageNamed:@"expandCard"];
            }
            else
            {
                cell.expandImg.image =[UIImage imageNamed:@"collapseCard"];
            }
        cell.orderIdLbl.text = [NSString stringWithFormat:@"%@",[[newSymbolArray objectAtIndex:indexPath.row]valueForKey:@"order_id"]];
        cell.timeLbl.text = [NSString stringWithFormat:@"%@", [[newSymbolArray objectAtIndex:indexPath.row]valueForKey:@"order_timestamp"]];
        cell.tradingSymLbl.text = [NSString stringWithFormat:@"%@", [[newSymbolArray objectAtIndex:indexPath.row]valueForKey:@"tradingsymbol"]];
        cell.exchangeLbl.text = [NSString stringWithFormat:@"%@", [[newSymbolArray objectAtIndex:indexPath.row]valueForKey:@"exchange"]];
        cell.transactionTypeLbl.text = [NSString stringWithFormat:@"%@", [[newSymbolArray objectAtIndex:indexPath.row]valueForKey:@"transaction_type"]];
        cell.priceLbl.text =[NSString stringWithFormat:@"%@",[[newSymbolArray objectAtIndex:indexPath.row]valueForKey:@"average_price"]];
        //    cell.orderTypeLbl.text = [NSString stringWithFormat:@"%@", [orderTypeArray objectAtIndex:indexPath.row]];
        //    cell.fillQtyLbl.text = [NSString stringWithFormat:@"%@", [filledArray objectAtIndex:indexPath.row]];
        //    cell.pedingQtyLbl.text = [NSString stringWithFormat:@"%@", [pendingArray objectAtIndex:indexPath.row]];
        cell.qtyLbl.text = [NSString stringWithFormat:@"%@", [[newSymbolArray objectAtIndex:indexPath.row]valueForKey:@"quantity"]];
        
        return cell;
            
        }
        
    }
    
    else
    {
    if(tradeArray.count>0||tradeDetailsArray.count>0)
    {
        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
        TradeCellTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    
    cell.orderIdLbl.text = [NSString stringWithFormat:@"%@", [orderIDArray objectAtIndex:indexPath.row]];
    cell.timeLbl.text = [NSString stringWithFormat:@"%@", [orderTimeArray objectAtIndex:indexPath.row]];
    cell.tradingSymLbl.text = [NSString stringWithFormat:@"%@", [tradingSymArray objectAtIndex:indexPath.row]];
    cell.exchangeLbl.text = [NSString stringWithFormat:@"%@", [exchangeArray objectAtIndex:indexPath.row]];
    cell.transactionTypeLbl.text = [NSString stringWithFormat:@"%@", [transactionTypeArray objectAtIndex:indexPath.row]];
            if([cell.transactionTypeLbl.text isEqualToString:@"BUY"])
            {
                
                cell.transactionTypeLbl.layer.borderColor = buyColor.CGColor;
                cell.transactionTypeLbl.textColor = buyColor;
            }
            else if([ cell.transactionTypeLbl.text isEqualToString:@"SELL"])
            {
                
                cell.transactionTypeLbl.layer.borderColor = sellColor.CGColor;
                cell.transactionTypeLbl.textColor = sellColor;
            }
            
            NSString * orderTypeString=[NSString stringWithFormat:@"Type: %@",[orderTypeArray objectAtIndex:indexPath.row]];
            
            cell.orderTypeLbl.text = orderTypeString;
            
            NSString * productStr =[NSString stringWithFormat:@"%@", [productArray objectAtIndex:indexPath.row]];
            if([productStr isEqualToString:@"D"]||[productStr isEqualToString:@"Delivery"])
            {
                cell.productTypeLbl.text = @"Product: Delivery";
            }
            else  if([productStr isEqualToString:@"I"]||[productStr isEqualToString:@"Intraday"])
            {
                cell.productTypeLbl.text = @"Product: Intraday";
            }
            
    cell.priceLbl.text =[NSString stringWithFormat:@"%@", [priceArray objectAtIndex:indexPath.row]];
//    cell.orderTypeLbl.text = [NSString stringWithFormat:@"%@", [orderTypeArray objectAtIndex:indexPath.row]];
//    cell.fillQtyLbl.text = [NSString stringWithFormat:@"%@", [filledArray objectAtIndex:indexPath.row]];
//    cell.pedingQtyLbl.text = [NSString stringWithFormat:@"%@", [pendingArray objectAtIndex:indexPath.row]];
    cell.qtyLbl.text = [NSString stringWithFormat:@"%@", [totalQtyArray objectAtIndex:indexPath.row]];
    cell.filledLabel.text = [NSString stringWithFormat:@"%@", [totalQtyArray objectAtIndex:indexPath.row]];
        return cell;
        }
        
        
        
        else
        {
            TradeCellTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
            
           
            cell.timeLbl.text = [NSString stringWithFormat:@"%@",[[tradeDetailsArray objectAtIndex:indexPath.row] objectForKey:@"order_timestamp"]];
            
            cell.exchangeLbl.text = [NSString stringWithFormat:@"%@",[[tradeDetailsArray objectAtIndex:indexPath.row] objectForKey:@"exchange"]];
            
            NSString * string1=[[tradeDetailsArray objectAtIndex:indexPath.row] objectForKey:@"transaction_type"];
           
            NSString * orderTypeString=[NSString stringWithFormat:@"Type: %@",[[tradeDetailsArray objectAtIndex:indexPath.row] objectForKey:@"order_type"]];
            if(![orderTypeString containsString:@"null"])
            {
            cell.orderTypeLbl.text = orderTypeString;
            }
            else
            {
                cell.orderTypeLbl.text=@"Type: --";
            }
            
            NSString * productStr =[NSString stringWithFormat:@"%@", [[tradeDetailsArray objectAtIndex:indexPath.row] objectForKey:@"product_type"]];
            if([productStr isEqualToString:@"1"])
            {
                cell.productTypeLbl.text = @"Product: Delivery";
            }
            else  if([productStr isEqualToString:@"2"])
            {
                cell.productTypeLbl.text = @"Product: Intraday";
            }
            
            
            
           cell.tradingSymLbl.text=[[tradeDetailsArray objectAtIndex:indexPath.row] objectForKey:@"tradingsymbol"];
            
            cell.transactionTypeLbl.text = [NSString stringWithFormat:@"%@",string1];
            if([cell.transactionTypeLbl.text isEqualToString:@"BUY"])
            {
                
                cell.transactionTypeLbl.layer.borderColor = buyColor.CGColor;
                cell.transactionTypeLbl.textColor = buyColor;
            }
            else if([ cell.transactionTypeLbl.text isEqualToString:@"SELL"])
            {
                
                cell.transactionTypeLbl.layer.borderColor = sellColor.CGColor;
                cell.transactionTypeLbl.textColor = sellColor;
            }
            
//            for(int i=0;i<delegate2.allTradeOrderArray.count;i++)
//            {
//                 int orderID=[[[delegate2.tradeCompletedArray objectAtIndex:indexPath.row] objectForKey:@"ATINOrderId"] intValue];
//
//                 int orderID1=[[[delegate2.allTradeOrderArray objectAtIndex:i] objectForKey:@"ATINOrderId"] intValue];
//
//                if(orderID==orderID1)
//                {
//                    float finalPrice=[[[delegate2.allTradeOrderArray objectAtIndex:i] objectForKey:@"dbPrice"] intValue];
//
//                     cell.priceLbl.text =[NSString stringWithFormat:@"%.2f",finalPrice];
//
//                     cell.orderIdLbl.text = [NSString stringWithFormat:@"%@",[[delegate2.allTradeOrderArray objectAtIndex:indexPath.row] objectForKey:@"stTradeID"]];
//
//                }
//
//
//
//            }
//
            cell.priceLbl.text =[NSString stringWithFormat:@"%@",[[tradeDetailsArray objectAtIndex:indexPath.row] objectForKey:@"trade_price"]];
            
            cell.orderIdLbl.text = [NSString stringWithFormat:@"%@",[[tradeDetailsArray objectAtIndex:indexPath.row] objectForKey:@"trade_id"]];
           

            //    cell.orderTypeLbl.text = [NSString stringWithFormat:@"%@", [orderTypeArray objectAtIndex:indexPath.row]];
                cell.filledLabel.text = [NSString stringWithFormat:@"%@", [[tradeDetailsArray objectAtIndex:indexPath.row] objectForKey:@"filled_quantity"]];
            
            NSString * pendingStr= [NSString stringWithFormat:@"%@", [[tradeDetailsArray objectAtIndex:indexPath.row] objectForKey:@"pending_quantity"]];
            if([pendingStr isEqualToString:@"(null)"])
            {
                 cell.pendingLabel.text =@"0";
            }
            
            else
            {
                cell.pendingLabel.text =pendingStr;
                
            }
            
            cell.qtyLbl.text = [NSString stringWithFormat:@"%@", [[tradeDetailsArray objectAtIndex:indexPath.row] objectForKey:@"quantity"]];
            
            
            
            return cell;

            
        }
        
        
    }
    
    else
    {
        NoTradesCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
        
        return cell;
    }
        
    }
    
    return nil;
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

}

- (void)tableView: (UITableView*)tableView willDisplayCell: (UITableViewCell*)cell forRowAtIndexPath: (NSIndexPath*)indexPath
{
    
    
//
//    if(indexPath.row % 2 == 0)
//    {
//
//        cell.backgroundColor = [UIColor whiteColor];
//    }
//           else
//           {
//
//               cell.backgroundColor = [UIColor colorWithRed:(245/255.0) green:(245/255.0) blue:(245/255.0) alpha:1];
//
//           }
//
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tradeArray.count>0||tradeDetailsArray.count>0)
    {
        if(indexPath == selectedIndex) {
            
            return 180; //Size you want to increase to
        }
        else
        {
            return 104;
        }
        
    }
    else
    {
        return 393 ;
    }
   
}

//mt//

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
        
        if(selectedIndex == nil)
        {
            selectedIndex = indexPath;
            
            expandBool = true;
        }
        else
        {
            if(selectedIndex == indexPath)
            {
                selectedIndex = nil;
                expandBool = false;
            }
            else
            {
                selectedIndex = indexPath;
                expandBool = true;
            }
        }
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    
    
}





@end
