//
//  MainOrdersViewController.h
//  testing
//
//  Created by zenwise technologies on 26/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface MainOrdersViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

    
    
    {
        NSMutableArray *orderIDArray, *tradingSymArray, *orderTimeArray, *exchangeArray, *filledArray, *comPendingArray, *orderTypeArray, *transactionTypeArray, *priceArray, *totalQtyArray,*pendingMessageArray, *completedProductArray;
        
        NSMutableArray *cancelledOrderIDArray, *cancelledTradingSymArray, *cancelledOrderTimeArray, *cancelledExchangeArray, *cancelledFilledArray, *cancelledPendingArray, *cancelledOrderTypeArray, *cancelledTransactionTypeArray, *cancelledPriceArray, *cancelledTotalQtyArray ,*pendingFilledArrayy,*pendingLabelArray, *cancelledProductArray;
        
        NSMutableArray *pendingOrderIDArray, *pendingTradingSymArray, *pendingOrderTimeArray, *pendingExchangeArray, *pendingFilledArray, *pendingPendingArray, *pendingOrderTypeArray, *pendingTransactionTypeArray, *pendingPriceArray, *pendingTotalQtyArray, *pendingProductArray;;
        
        NSString *orderIdStr, *tradingSymStr, *orderTimeStr, *exchangeStr, *fillQtyStr, *pendingQtyStr, *orderTypeStr, *transactionTypeStr, *priceStr, *totalQtyStr, *completedProductStr;
        
        NSString *cancelledOrderIdStr, *cancelledTradingSymStr, *cancelledOrderTimeStr, *cancelledExchangeStr, *cancelledFillQtyStr, *cancelledPendingQtyStr, *cancelledOrderTypeStr, *cancelledTransactionTypeStr, *cancelledPriceStr, *cancelledTotalQtyStr, *cancelledProductStr;
        
        NSString *pendingOrderIdStr, *pendingTradingSymStr, *pendingOrderTimeStr, *pendingExchangeStr, *pendingFillQtyStr, *pendingPendingQtyStr, *pendingOrderTypeStr, *pendingTransactionTypeStr, *pendingPriceStr, *pendingTotalQtyStr, *pendingTriggerStr, *pendingStopLimitPriceStr,*pendingMessageStr, *pendingProductStr;
        BOOL flagValue;
        NSArray *orderArray;
        NSMutableArray *instrumentTokenn;
    }

@property NSTimer * timer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewLayoutConstraint;
@property (weak, nonatomic) IBOutlet UILabel *topViewLabel;

@property (weak, nonatomic) IBOutlet UITableView *pendingOrdersTableView;

- (IBAction)backToMainView:(id)sender;

- (IBAction)searchAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *searchOutlet;

@property (weak, nonatomic) IBOutlet UIView *completedView;

@property (weak, nonatomic) IBOutlet UITableView *completedTableView;


@property (weak, nonatomic) IBOutlet UIView *pendingView;

@property (strong, nonatomic) IBOutlet UITableView *cancelTableView;

@property (strong, nonatomic) IBOutlet UIView *cancelView;
@property Reachability * reach;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityInd;
@property NSString * topViewCheck;

@end
