//
//  TadeView.h
//  testing
//
//  Created by zenwise technologies on 23/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TadeView : UIViewController<UITableViewDelegate,UITableViewDataSource, NSURLSessionDelegate,UISearchDisplayDelegate, UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating>

{
    NSArray *tradeArray;
    
    NSMutableArray *orderIDArray, *tradingSymArray, *orderTimeArray, *exchangeArray, *filledArray, *pendingArray, *orderTypeArray, *transactionTypeArray, *priceArray, *totalQtyArray, *productArray;
    
    NSString *orderIdStr, *tradingSymStr, *orderTimeStr, *exchangeStr, *fillQtyStr, *pendingQtyStr, *orderTypeStr, *transactionTypeStr, *priceStr, *totalQtyStr, *productStr;

}

@property (weak, nonatomic) IBOutlet UITableView *tradeTableView;

- (IBAction)backBtn:(id)sender;

- (IBAction)searchAction:(id)sender;
@property UISearchController * controller;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityInd;
@property NSTimer * timer;

@end



