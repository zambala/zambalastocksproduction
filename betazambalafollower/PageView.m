//
//  PageView.m
//  testing
//
//  Created by zenwise technologies on 24/11/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "PageView.h"
#import "TabBar.h"
#import "TagEncode.h"
#import "AppDelegate.h"
#import "ViewController1.h"


#define SCROLLWIDTH 375

@interface PageView ()
{
    AppDelegate * delegate2;
}

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *scrollview_ContentView_Height;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *scrollview_ContentView_Width;

@end

@implementation PageView


- (void)viewDidLoad
{
    delegate2=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    scrollView.contentSize=CGSizeMake(SCROLLWIDTH*3, scrollView.frame.size.height);
    scrollView.delegate = self;
    
    for (int i =0; i<3; i++)
    {
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(SCROLLWIDTH*i, 0, scrollView.frame.size.width, scrollView.frame.size.height)];
        if (i==0)
        {
//            imageView.backgroundColor = [UIColor redColor];
            imageView.image = [UIImage imageNamed:@"image1.jpg"];
            
        
            
        }
        else if (i==1)
        {
//            imageView.backgroundColor = [UIColor greenColor];
            imageView.image = [UIImage imageNamed:@"image2.jpg"];

            
        }
        else
        {
//            imageView.backgroundColor = [UIColor yellowColor];
            imageView.image = [UIImage imageNamed:@"kiteimg.png"];

        }
        [scrollView addSubview:imageView];
    }
    
    
       [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.getStartBtn.hidden=YES;
    self.activityInd.hidden=NO;
    [self.activityInd startAnimating];
    [self initCall];
}


- (void)viewWillAppear:(BOOL)animated
{
    [self performAdjustScrollviewHeightWidth];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if(prefs!=nil)
    {
        @try {
            NSString * local=[prefs stringForKey:@"logintype"];
            
            if([local isEqualToString:@"OTP1"])
            {
                
                TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
                
                [self presentViewController:tabPage animated:YES completion:nil];
                
                
                
                
            }
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
        
        
        
        
        
        
    }
    

    
    
}
//- (void)viewDidAppear:(BOOL)animated;     // Called when the view has been fully transitioned onto the screen. Default does nothing
//- (void)viewWillDisappear:(BOOL)animated; // Called when the view is dismissed, covered or otherwise hidden. Default does nothing
- (void)viewDidDisappear:(BOOL)animated
{
//    [self.navigationController setNavigationBarHidden:NO animated:YES];
}



-(void)performAdjustScrollviewHeightWidth
{
    CGFloat windowWidth = [[UIScreen mainScreen] bounds].size.width;
    CGFloat windowHeight =  [[UIScreen mainScreen] bounds].size.height;
    
    self.scrollview_ContentView_Height.constant = windowHeight - 127;
    self.scrollview_ContentView_Width.constant = windowWidth * 3;
    
    [self.view layoutIfNeeded];
    
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





#pragma mark changePage

-(IBAction)changePage:(id)sender
{
    [scrollView scrollRectToVisible:CGRectMake(SCROLLWIDTH*pageControl.currentPage, scrollView.frame.origin.y, SCROLLWIDTH, scrollView.frame.size.height) animated:YES];
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender {
    
    [self setIndiactorForCurrentPage];
    
}

-(void)setIndiactorForCurrentPage
{
    uint page = scrollView.contentOffset.x / SCROLLWIDTH;
    [pageControl setCurrentPage:page];
    
}

-(void)initCall
{
    NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
    
    TagEncode * enocde=[[TagEncode alloc]init];
    delegate2.accessToken=[loggedInUser objectForKey:@"acesstoken"];
    enocde.inputRequestString=@"init";
    NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString * appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString * versionBuildString = [NSString stringWithFormat:@"%@(%@)", appVersionString, appBuildString];
    
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    
    //[inputArray addObject:[NSString stringWithFormat:@"%@init",@"http://192.168.15.10:8013/api/"]];
    [inputArray addObject:[NSString stringWithFormat:@"%@init",delegate2.baseUrl]];
    //    [inputArray addObject:[NSString stringWithFormat:@"%@init",@"https://stagemainstock.zenwise.net/api/"]];
    
    [inputArray addObject:[NSString stringWithFormat:@"%@",delegate2.currentDeviceId]];
    [inputArray addObject:[NSString stringWithFormat:@"%@",versionBuildString]];
    [inputArray addObject:[NSString stringWithFormat:@"%@",@"ios"]];
    if([[loggedInUser objectForKey:@"loginActivityStr"] isEqualToString:@"OTP1"])
    {
        [inputArray addObject:[NSString stringWithFormat:@""]];
    }
    else
    {
        [inputArray addObject:[NSString stringWithFormat:@"%@",delegate2.accessToken]];
    }
    
    
    //NSLog(@"%@",inputArray);
    
    [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        
        //NSLog(@"%@",dict);
        if(dict.count>0)
        {
            NSMutableDictionary * localArray = [[NSMutableDictionary alloc]init];
            localArray = [[dict objectForKey:@"data"] objectForKey:@"data"];
            //NSLog(@"%@",localArray);
            delegate2.brokerDetailsDict=[[NSMutableDictionary alloc]init];
            delegate2.brokerDetailsDict=[dict mutableCopy];
            
            int checkInt = [[localArray objectForKey:@"alreadylogin"] intValue];
            // NSString * checkString = [NSString stringWithFormat:@"%@",[[[localArray objectAtIndex:0]objectForKey:@"alreadylogin"]stringValue]];
            if([[loggedInUser objectForKey:@"loginActivityStr"] isEqualToString:@"OTP1"])
            {
                
                delegate2.userID=[loggedInUser objectForKey:@"userid"];
                delegate2.loginActivityStr=@"OTP1";
                delegate2.brokerNameStr=@"GUEST";
                delegate2.usCheck=@"ind";
                delegate2.clientPhoneNumber=delegate2.userID;
                delegate2.zenwiseToken=[loggedInUser objectForKey:@"guestaccess"];
                delegate2.accessToken=delegate2.zenwiseToken;
                 dispatch_async(dispatch_get_main_queue(), ^{
                     UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                     TabBar *view = [storyboard instantiateViewControllerWithIdentifier:@"tab"];
                     //                                [self.window makeKeyAndVisible];
                     //                                [self.window.rootViewController presentViewController:view animated:YES completion:NULL];
                     [self presentViewController:view animated:YES completion:nil];
                delegate2.brokerInfoDict=[[NSMutableDictionary alloc]init];
                delegate2.brokerInfoDict=[[[localArray objectForKey:@"brokerinfo"]objectForKey:@"guestlogin"]objectAtIndex:0];
                 });
                
                
            }
            else
            {
                
                if(checkInt ==1)
                {
                    delegate2.userID=[loggedInUser objectForKey:@"userid"];
                    delegate2.accessToken = [NSString stringWithFormat:@"%@",[localArray objectForKey:@"token"]];
                    delegate2.brokerNameStr=[loggedInUser objectForKey:@"brokername"];
                    delegate2.loginActivityStr=[loggedInUser objectForKey:@"loginActivityStr"];
                    
                    int numberCount = (int)[[[localArray objectForKey:@"brokerinfo"] objectForKey:@"clientlogin"] count];
                    //NSLog(@"NumberCount:%d",numberCount);
                    for(int i=0;i<numberCount;i++)
                    {
                        
                        NSString * brokerName=[NSString stringWithFormat:@"%@",[[[[localArray objectForKey:@"brokerinfo"]objectForKey:@"clientlogin"]objectAtIndex:i]objectForKey:@"brokername"]];
                        if([delegate2.brokerNameStr isEqualToString:@"PhillipCapital"])
                        {
                            delegate2.brokerNameStr=@"Phillip";
                        }
                        
                        if([delegate2.brokerNameStr containsString:brokerName])
                        {
                            delegate2.brokerInfoDict=[[[localArray objectForKey:@"brokerinfo"]objectForKey:@"clientlogin"] objectAtIndex:i];
                        }
                    }
                    
                    if (([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Phillip"]||[delegate2.brokerNameStr isEqualToString:@"Arcadia"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"]
                         ||[delegate2.brokerNameStr isEqualToString:@"Sunidhi"]||[delegate2.brokerNameStr containsString:@"Choice Trade"]))
                    {
                        if(delegate2.brokerInfoDict.count>0)
                        {
                            if([delegate2.brokerNameStr isEqualToString:@"Arcadia"]||[delegate2.brokerNameStr isEqualToString:@"Phillip"]||[delegate2.brokerNameStr isEqualToString:@"Sunidhi"])
                            {
                                UIImage *NavigationPortraitBackground= [UIImage imageNamed:@"topBg1"];
                                [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
                                delegate2.usCheck=@"ind";
                                delegate2.brokerNameStr=delegate2.brokerNameStr;
                                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                TabBar *view = [storyboard instantiateViewControllerWithIdentifier:@"tab"];
//                                [self.window makeKeyAndVisible];
//                                [self.window.rootViewController presentViewController:view animated:YES completion:NULL];
                                [self presentViewController:view animated:YES completion:nil];
                                
                                
                            }else if ([delegate2.brokerNameStr containsString:@"Choice Trade"])
                            {
                                UIImage *NavigationPortraitBackground= [UIImage imageNamed:@"topUS-1"];
                                [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
                                
                                [[UINavigationBar appearance] setTranslucent:NO];
                                delegate2.usCheck=@"USA";
                                delegate2.usNavigationCheck=@"USA";
                                //                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"USA" bundle:nil];
                                //                            LoginViewController * usHome = [storyboard instantiateInitialViewController];
                                //                            [self presentViewController:usHome animated:YES completion:nil];
                            }
                            
                            else
                            {
                                ViewController1 *view = [self.storyboard instantiateViewControllerWithIdentifier:@"view"];
                                [self presentViewController:view animated:YES completion:nil];
                                
                                UIImage *NavigationPortraitBackground= [UIImage imageNamed:@"topBg1"];
                                [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
                                delegate2.usCheck=@"ind";
                                delegate2.brokerNameStr=delegate2.brokerNameStr;
                                //                            ViewController1 *view = [self.storyboard instantiateViewControllerWithIdentifier:@"view"];
                                //                            [self.navigationController pushViewController:view animated:YES];
                            }
                        }
                    }
                    
                }
                else
                {
                    delegate2.brokerNameStr=[loggedInUser objectForKey:@"brokername"];
                        delegate2.loginActivityStr=[loggedInUser objectForKey:@"loginActivityStr"];
                    
                    int numberCount = (int)[[[localArray objectForKey:@"brokerinfo"] objectForKey:@"clientlogin"] count];
                    //NSLog(@"NumberCount:%d",numberCount);
                    for(int i=0;i<numberCount;i++)
                    {
                        
                        NSString * brokerName=[NSString stringWithFormat:@"%@",[[[[localArray objectForKey:@"brokerinfo"]objectForKey:@"clientlogin"]objectAtIndex:i]objectForKey:@"brokername"]];
                        if([delegate2.brokerNameStr isEqualToString:@"PhillipCapital"])
                        {
                            delegate2.brokerNameStr=@"Phillip";
                        }
                        
                        if([delegate2.brokerNameStr containsString:brokerName])
                        {
                            delegate2.brokerInfoDict=[[[localArray objectForKey:@"brokerinfo"]objectForKey:@"clientlogin"] objectAtIndex:i];
                        }
                    }
                    
                    if (([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Phillip"]||[delegate2.brokerNameStr isEqualToString:@"Arcadia"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"]
                         ||[delegate2.brokerNameStr isEqualToString:@"Sunidhi"]||[delegate2.brokerNameStr containsString:@"Choice Trade"]))
                    {
                        if(delegate2.brokerInfoDict.count>0)
                        {
                            if([delegate2.brokerNameStr isEqualToString:@"Arcadia"]||[delegate2.brokerNameStr isEqualToString:@"Phillip"]||[delegate2.brokerNameStr isEqualToString:@"Sunidhi"])
                            {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                UIImage *NavigationPortraitBackground= [UIImage imageNamed:@"topBg1"];
                                [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
                                delegate2.usCheck=@"ind";
                                delegate2.brokerNameStr=delegate2.brokerNameStr;
                                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                TabBar *view = [storyboard instantiateViewControllerWithIdentifier:@"tab"];
                                //                                [self.window makeKeyAndVisible];
                                //                                [self.window.rootViewController presentViewController:view animated:YES completion:NULL];
                                [self presentViewController:view animated:YES completion:nil];
                                });
                                
                                
                            }else if ([delegate2.brokerNameStr containsString:@"Choice Trade"])
                            {
                                UIImage *NavigationPortraitBackground= [UIImage imageNamed:@"topUS-1"];
                                [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
                                
                                [[UINavigationBar appearance] setTranslucent:NO];
                                delegate2.usCheck=@"USA";
                                delegate2.usNavigationCheck=@"USA";
                                //                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"USA" bundle:nil];
                                //                            LoginViewController * usHome = [storyboard instantiateInitialViewController];
                                //                            [self presentViewController:usHome animated:YES completion:nil];
                            }
                            
                            else
                            {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                     UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                ViewController1 *view = [storyboard instantiateViewControllerWithIdentifier:@"view"];
                                [self presentViewController:view animated:YES completion:nil];
                                
                                UIImage *NavigationPortraitBackground= [UIImage imageNamed:@"topBg1"];
                                [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
                                delegate2.usCheck=@"ind";
                                delegate2.brokerNameStr=delegate2.brokerNameStr;
                                });
                                //                            ViewController1 *view = [self.storyboard instantiateViewControllerWithIdentifier:@"view"];
                                //                            [self.navigationController pushViewController:view animated:YES];
                            }
                        }
                    }
                    
                }
                
                
            }
              dispatch_async(dispatch_get_main_queue(), ^{
            [self.activityInd stopAnimating];
            self.activityInd.hidden=YES;
            self.getStartBtn.hidden=NO;
              });
        }
        else
        {
            [self initCall];
        }
        
    }];
    //    }
}


@end
