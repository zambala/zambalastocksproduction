//
//  CustomPortfolioView.m
//  betazambalafollower
//
//  Created by Zenwise Technologies on 13/06/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import "CustomPortfolioView.h"
#import <UITextField_AutoSuggestion/UITextField+AutoSuggestion.h>
#import "AppDelegate.h"
#import <Mixpanel/Mixpanel.h>

@interface CustomPortfolioView ()<UITextFieldAutoSuggestionDataSource,UITextFieldDelegate>
{
    AppDelegate * delegate1;
    NSUInteger  objectAtIndex;
    NSString * exchangeStr;
    NSMutableArray * companyName;
}
@property (strong, nonatomic) NSTimer * searchTimer;

@end

#define Symbol_ID @"symbol_id"
#define WEEKS @[@"Monday", @"Tuesday", @"Wednesday", @"Thirsday", @"Friday", @"Saturday", @"Sunday"]


@implementation CustomPortfolioView

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"add_portfolio_watch_symbol_page"];
    self.symbolSearch.delegate = self;
    self.symbolSearch.autoSuggestionDataSource = self;
    self.symbolSearch.fieldIdentifier = Symbol_ID;
    [self.symbolSearch observeTextFieldChanges];
    self.symbols=[[NSMutableArray alloc]init];
    self.company=[[NSMutableArray alloc]init];
    exchangeStr=@"NSE";
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadWeekDays {
    // cancel previous requests
    @try {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(loadWeekDaysInBackground) object:self];
        [self.symbolSearch setLoading:false];
        
        // clear previous results
        [self.symbols removeAllObjects];
        [self.symbolSearch reloadContents];
        
        // start loading
        [self.symbolSearch setLoading:true];
        [self performSelector:@selector(loadWeekDaysInBackground) withObject:self afterDelay:2.0f];
        
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (void)loadWeekDaysInBackground {
    // finish loading
    @try {
        [self.symbolSearch setLoading:false];
        
        
        [self.symbols addObjectsFromArray:self.company];
        
        [self.symbolSearch reloadContents];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}

- (UITableViewCell *)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
    static NSString *cellIdentifier = @"WeekAutoSuggestionCell";
    @try {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        
//        NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@", text];
        NSArray *weeks = self.symbols;
        NSString * final=[NSString stringWithFormat:@"%@(%@)",weeks[indexPath.row],[companyName objectAtIndex:indexPath.row]];
        cell.textLabel.text =final;
        
        return cell;
    }
    @catch (NSException *exception) {
        //NSLog(@"%@", exception.reason);
    }
}

- (NSInteger)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section forText:(NSString *)text {
    
//    NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@", text];
    
    @try {
        NSInteger count = self.symbols.count;
        return count;
    }
    @catch (NSException *exception) {
        //NSLog(@"%@", exception.reason);
    }
    
    return 0;
    
}

- (void) searchForKeyword:(NSTimer *)timer
{
    // retrieve the keyword from user info
    NSString *keyword = (NSString*)timer.userInfo;
    self.searchStr=keyword;
    [self searchServer:self.searchStr];
    [self loadWeekDays];
    // [self segmentedControlChangedValue];
    // perform your search (stubbed here using //NSLog)
    //NSLog(@"Searching for keyword %@", keyword);
}

- (void)autoSuggestionField:(UITextField *)field textChanged:(NSString *)text {
    if (self.searchTimer != nil) {
        [self.searchTimer invalidate];
        self.searchTimer = nil;
    }
    
    // reschedule the search: in 1.0 second, call the searchForKeyword: method on the new textfield content
    self.searchTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                        target: self
                                                      selector: @selector(searchForKeyword:)
                                                      userInfo: self.symbolSearch.text
                                                       repeats: NO];
}

-(void)searchServer:(NSString *)string
{
   
     @try {
    self.searchStr = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                           NULL,
                                                                                           (CFStringRef)string,
                                                                                           NULL,
                                                                                           (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                           kCFStringEncodingUTF8 ));
   
        NSDictionary *headers = @{
                                  @"cache-control": @"no-cache",
                                  @"authtoken":delegate1.zenwiseToken,
                                  @"deviceid":delegate1.currentDeviceId
                                  };
        
        NSString * urlStr=[NSString stringWithFormat:@"%@stock/searchsymbol/?searchsymbol=%@&exchange=%@&limit=20000",delegate1.baseUrl,self.searchStr,exchangeStr];
        
        
        
        //NSLog(@"url %@",urlStr);
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(data !=nil)
                                                        {
                                                            if (error) {
                                                                //NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                //NSLog(@"%@", httpResponse);
                                                                if([httpResponse statusCode]==403)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                    
                                                                }
                                                                else if([httpResponse statusCode]==401)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    
                                                                    [self.company removeAllObjects];
                                                                    companyName=[[NSMutableArray alloc]init];
                                                                    self.responseArray=[[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] mutableCopy];
                                                                    
                                                                    //                                                            [delegate1.searchSymbolDict setObject:self.responseArray forKey:@"details"];
                                                                    
                                                                    //NSLog(@"%@",self.responseArray);
                                                                    
                                                                    //                                                            NSString * string=[NSString stringWithFormat:@"%@",self.responseArray];
                                                                    
                                                                    self.detailsDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSArray arrayWithArray:self.responseArray],@"details", nil];
                                                                    //NSLog(@"%@",self.detailsDict);
                                                                    if(self.responseArray.count>0)
                                                                    {
                                                                        
                                                                        for(int i=0;i<self.responseArray.count;i++)
                                                                        {
                                                                            
                                                                            [self.company addObject:[[self.responseArray objectAtIndex:i]objectForKey:@"symbol"]];
                                                                            
                                                                            [companyName addObject:[[self.responseArray objectAtIndex:i]objectForKey:@"name"]];
                                                                            
                                                                        }
                                                                        
                                                                        [self.responseArray removeAllObjects];
                                                                    }
                                                                }
                                                            }
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                //                                                          HomePage * home1=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
                                                                //
                                                                //                                                          [self.navigationController pushViewController:home1 animated:YES];
                                                                
                                                                
                                                                
                                                            });
                                                        }
                                                    }];
        [dataTask resume];
        
        
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (CGFloat)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
    return 50;
}

- (void)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
    
    @try {
        [self.view endEditing:YES];
        
        //NSLog(@"Selected suggestion at index row - %ld", (long)indexPath.row);
        
      
//        NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@", text];
        NSArray *weeks = self.symbols;
        
        self.symbolSearch.text = weeks[indexPath.row];
        
        //    NSIndexPath *selectedIndexPath = [tableView indexPathForSelectedRow];
       
        objectAtIndex = indexPath.row;
       
        
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
//        self.searchStr = [self.symbolSearch.text stringByReplacingCharactersInRange:range withString:string];
//       self.searchStr = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
//                                                                                           NULL,
//                                                                                           (CFStringRef) self.searchStr,
//                                                                                           NULL,
//                                                                                           (CFStringRef)@"!*'();:@&=+$,/?%#[]",
//                                                                                           kCFStringEncodingUTF8 ));
//    @try {
//        NSDictionary *headers = @{
//                                  @"cache-control": @"no-cache",
//                                  @"authtoken":delegate1.zenwiseToken,
//                                   @"deviceid":delegate1.currentDeviceId
//                                  };
//
//        NSString * urlStr=[NSString stringWithFormat:@"%@stock/searchsymbol/?searchsymbol=%@&exchange=%@&limit=20000",delegate1.baseUrl,self.searchStr,exchangeStr];
//
//
//
//        //NSLog(@"url %@",urlStr);
//
//        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
//                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                           timeoutInterval:10.0];
//        [request setHTTPMethod:@"GET"];
//        [request setAllHTTPHeaderFields:headers];
//
//        NSURLSession *session = [NSURLSession sharedSession];
//        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
//                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                                                        if(data !=nil)
//                                                        {
//                                                        if (error) {
//                                                            //NSLog(@"%@", error);
//                                                        } else {
//                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
//                                                            //NSLog(@"%@", httpResponse);
//                                                            if([httpResponse statusCode]==403)
//                                                            {
//
//                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
//
//                                                            }
//                                                            else if([httpResponse statusCode]==401)
//                                                            {
//
//                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
//
//                                                            }
//                                                            else
//                                                            {
//
//                                                            [self.company removeAllObjects];
//
//                                                            self.responseArray=[[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] mutableCopy];
//
////                                                            [delegate1.searchSymbolDict setObject:self.responseArray forKey:@"details"];
//
//                                                            //NSLog(@"%@",self.responseArray);
//
//                                                            //                                                            NSString * string=[NSString stringWithFormat:@"%@",self.responseArray];
//
//                                                            self.detailsDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSArray arrayWithArray:self.responseArray],@"details", nil];
//                                                            //NSLog(@"%@",self.detailsDict);
//                                                            if(self.responseArray.count>0)
//                                                            {
//
//                                                                for(int i=0;i<self.responseArray.count;i++)
//                                                                {
//
//                                                                    [self.company addObject:[[self.responseArray objectAtIndex:i]objectForKey:@"symbol"]];
//
//
//
//                                                                }
//
//                                                                [self.responseArray removeAllObjects];
//                                                            }
//                                                            }
//                                                        }
//
//                                                        dispatch_async(dispatch_get_main_queue(), ^{
//
//                                                            //                                                          HomePage * home1=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
//                                                            //
//                                                            //                                                          [self.navigationController pushViewController:home1 animated:YES];
//
//
//
//                                                        });
//                                                        }
//                                                    }];
//        [dataTask resume];
//
//
//
//    }
//    @catch (NSException * e) {
//        //NSLog(@"Exception: %@", e);
//    }
//    @finally {
//        //NSLog(@"finally");
//    }
         return YES;
}
        
- (IBAction)exchangeSegmentAction:(id)sender {
    
    if(self.exchangeSegment.selectedSegmentIndex==0)
    {
        exchangeStr=@"NSE";
    }
    else if(self.exchangeSegment.selectedSegmentIndex==1)
    {
        exchangeStr=@"BSE";
    }
    
    
}
- (IBAction)onPortfolioTap:(id)sender {
    @try
    {
    
    //NSLog(@"%@",self.detailsDict);
    //NSLog(@"%lu",objectAtIndex);
    if(self.symbolSearch.text.length>0&&self.qtyFld.text.length>0&&self.avrgPriceFld.text.length>0&&[[self.detailsDict objectForKey:@"details"]count]>0)
    {
        NSMutableDictionary * customPortfolioDict=[[NSMutableDictionary alloc]init];
        customPortfolioDict=[[[self.detailsDict objectForKey:@"details"] objectAtIndex:objectAtIndex] mutableCopy];
        [customPortfolioDict setObject:self.qtyFld.text forKey:@"quantity"];
        [customPortfolioDict setObject:self.avrgPriceFld.text forKey:@"averageprice"];
        delegate1.customPortfolioArray=[[NSMutableArray alloc]init];
        [delegate1.customPortfolioArray addObject:customPortfolioDict];
        if(customPortfolioDict)
        {
        delegate1.searchToWatch=true;
        [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else
    {
      
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Please fill all fields." preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:^{
                
            }];
            
            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            [alert addAction:okAction];
            
            
        });
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
@end
