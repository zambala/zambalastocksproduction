//
//  MessageView.m
//  testing
//
//  Created by zenwise mac 2 on 11/23/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "MessageView.h"
#import "MessagesTableView.h"
#import "BrokerMessagesView.h"
#import "HMSegmentedControl.h"
#import "TabBar.h"
#import "MessagesTableCellTableViewCell.h"
#import "BrokerTableViewCell.h"
#import "MessagesCell.h"

@interface MessageView ()
{
    HMSegmentedControl *segmentedControl;
}

@end

@implementation MessageView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"MESSAGES";
    
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"Notofications",@"Broker Messages",@"Offers"]];
    segmentedControl.frame = CGRectMake(0, 58.2, 375, 40);
//    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
//    segmentedControl.verticalDividerEnabled = YES;
    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(251/255.0) green:(196/255.0) blue:(12/255.0) alpha:1];
    //    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
        [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segmentedControl];
    
    
    self.notoficationstableView.delegate=self;
    self.notoficationstableView.dataSource=self;
    
  
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)segmentedControlChangedValue
{
    [self.notoficationstableView reloadData];
}


- (IBAction)showingMainView:(id)sender
{
    TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
    
    [self presentViewController:tabPage animated:YES completion:nil];

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 10;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
   
//    if(segmentedControl.selectedSegmentIndex==0)
//    {
    
//        self.brokerMesssagesTableView.hidden=YES;
//        self.notoficationstableView.hidden=NO;
    
    MessagesCell * cell=[tableView dequeueReusableCellWithIdentifier:@"123" forIndexPath:indexPath];
//    cell.messageLable.text=@"Messages";
                return cell;
//    }
//    else if (segmentedControl.selectedSegmentIndex==1)
//    {
////        self.notoficationstableView.hidden=YES;
////        self.brokerMesssagesTableView.hidden=NO;
//        MessagesCell * cell=[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
//        
////        cell.messageLable.text=@"Broker Messages";
////       
//        return cell;
//    }
//    
//    else if (segmentedControl.selectedSegmentIndex==2)
//    {
//        //        self.notoficationstableView.hidden=YES;
//        //        self.brokerMesssagesTableView.hidden=NO;
//        MessagesCell * cell=[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
//        
////        cell.messageLable.text=@"Offers Messages";
//        
//        return cell;
//    }
//
//    
//    return 0;
//    
}

- (IBAction)showMenuActionSheet:(id)sender {
    actionSheet = [[UIActionSheet alloc] initWithTitle:@""
                                              delegate:self
                                     cancelButtonTitle:nil
                                destructiveButtonTitle:nil
                                     otherButtonTitles:@"All", @"Advices",@"Alerts", @"Order Status", @"Fund Transfer", @"Referrals", @"Rewards", nil];
    
    
    [actionSheet showInView:self.view];
    
    
   // actionSheet.tag = 100;
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == 0)
    {
        
    }
}



@end
