//
//  KnowledgeVideoTextViewController.m
//  KnowledgeSection
//
//  Created by Zenwise Technologies on 16/06/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "KnowledgeVideoTextViewController.h"

@interface KnowledgeVideoTextViewController ()

@end

@implementation KnowledgeVideoTextViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.backButton addTarget:self action:@selector(onBackButtonTap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

-(void)onBackButtonTap
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onPlayButtonTap:(id)sender {
}
@end
