//
//  StockAllocationView.m
//  testing
//
//  Created by zenwise technologies on 27/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "StockAllocationView.h"
#import "PortfolioDetailDepthTableViewCell.h"
#import "AppDelegate.h"
#import "PortfolioDepthNamesTableViewCell.h PortfolioDepthNamesTableViewCell.h"
#import "XYPieChart.h"

@interface StockAllocationView ()
{
    AppDelegate * delegate;
    NSDictionary *headers ;
    
    NSMutableURLRequest *request;
    
    NSDictionary *parameters ;
    
    NSData *postData ;
}

@end

@implementation StockAllocationView


- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    // Do any additional setup after loading the view.
    
    self.countArray = [[NSMutableArray alloc]init];
    self.sliceColors = [[NSMutableArray alloc]init];
    self.pieChartPercentageArray = [[NSMutableArray alloc]init];
    
    [self portfolioDepthServer];
    //NSLog(@"ID:%@",delegate.protFolioUserID);
    
    self.scrollView.contentSize=CGSizeMake(375, 790);
    self.profileimage.layer.cornerRadius=self.profileimage.frame.size.width / 2;
    self.profileimage.clipsToBounds = YES;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)design
{
    
    @try {
        
        self.leaderNameLabel.text = [[[self.portfolioDepthResponseDictionary objectForKey:@"data"] objectAtIndex:0] objectForKey:@"firstname"];
        self.dateAndTimeLabel.text = [[[[self.portfolioDepthResponseDictionary objectForKey:@"data"] objectAtIndex:0] objectForKey:@"createdon"] stringValue];
        self.profileNameLabel.text = [[[self.portfolioDepthResponseDictionary objectForKey:@"data"] objectAtIndex:0] objectForKey:@"messagename"];
        
        NSString * assumedInvestent = [NSString stringWithFormat:@"%@",[[[self.portfolioDepthResponseDictionary objectForKey:@"data"] objectAtIndex:0]objectForKey:@"amount"]];
        NSString * rupee = @"₹";
        NSString * assumedInvestmentFinal = [rupee stringByAppendingString:assumedInvestent];
        self.assumedInvestmentLabel.text= assumedInvestmentFinal;
        NSString * actedBy = [NSString stringWithFormat:@"%@",[[[self.portfolioDepthResponseDictionary objectForKey:@"data"] objectAtIndex:0]objectForKey:@"actedby"]];
        if([actedBy isEqual:[NSNull null]])
        {
            self.actedByLabel.text=@"0";
        }else
        {
            self.actedByLabel.text=actedBy;
        }
        NSString * sharesBought =[NSString stringWithFormat:@"%@",[[[self.portfolioDepthResponseDictionary objectForKey:@"data"] objectAtIndex:0]objectForKey:@"sharesold"]];
        if([sharesBought isEqual:[NSNull null]])
        {
            self.sharesBoughtLabel.text=@"0";
        }else
        {
            self.sharesBoughtLabel.text=sharesBought;
        }

        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
}

-(void)portfolioDepthServer
{
    headers = @{ @"cache-control": @"no-cache",
                 @"content-type": @"application/json",
                 @"authtoken":delegate.zenwiseToken,
                    @"deviceid":delegate.currentDeviceId
                 };
    
    request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower/advices",delegate.baseUrl]]
                                      cachePolicy:NSURLRequestUseProtocolCachePolicy
                                  timeoutInterval:10.0];
    
    parameters = @{
                   @"clientid":delegate.userID,
                   @"messagetypeid":@4,
                   @"active":@(true),
                   @"issubscribed":@(true),
                   @"messageid":delegate.protFolioUserID
                   };
    
    postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==403)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                            
                                                        }
                                                        else if([httpResponse statusCode]==401)
                                                        {
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                        self.portfolioDepthResponseDictionary= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            //NSLog(@"Portfolio Depth:%@",self.portfolioDepthResponseDictionary);
                                                        self.countArray = [[[[self.portfolioDepthResponseDictionary objectForKey:@"data"] objectAtIndex:0] objectForKey:@"portfoliojson"] objectForKey:@"stocks"];
                                                        for (int i=0; i<self.countArray.count; i++) {
                                                            [self.pieChartPercentageArray addObject:[[self.countArray objectAtIndex:i] objectForKey:@"percentage"]];
                                                        }
                                                        
                                                        
                                                        [self design];
                                                        }
                                                    }
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        self.portFolioTableView.delegate=self;
                                                        self.portFolioTableView.dataSource=self;
                                                        
                                                        [self.portFolioTableView reloadData];
                                                        
                                                        self.depthNameTableView.delegate=self;
                                                        self.depthNameTableView.dataSource=self;
                                                        
                                                        [self.depthNameTableView reloadData];
                                                        [self pieChart];

                                                        
                                                        
                                                        
                                                    });
                                                    
                                                }];
    [dataTask resume];
    

    
//    self.portfolioDepthResponseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//
//    //NSLog(@"Portfolio Depth:%@",self.portfolioDepthResponseDictionary);
}

-(void)pieChart
{
    [self.pieChartOutlet setDelegate:self];
    
    [self.pieChartOutlet setDataSource:self];
    [self.pieChartOutlet setStartPieAngle:M_PI_2];
    [self.pieChartOutlet setAnimationSpeed:1.0];
    [self.pieChartOutlet setLabelFont:[UIFont fontWithName:@"DBLCDTempBlack" size:18]];
    [self.pieChartOutlet setLabelRadius:160];
    [self.pieChartOutlet setShowPercentage:YES];
    [self.pieChartOutlet setPieRadius:self.pieChartOutlet.frame.size.height-50];
    [self.pieChartOutlet setUserInteractionEnabled:YES];
    [self.pieChartOutlet setLabelShadowColor:[UIColor blackColor]];
    [self.pieChartOutlet setShowLabel:NO];
    
    @try {
        self.sliceColors =[NSMutableArray arrayWithObjects:
                           [UIColor colorWithRed:(45.0)/255 green:(226.0)/255 blue:(127.0)/255 alpha:1],
                           [UIColor colorWithRed:(244.0)/(255.0) green:(100.0)/(255.0) blue:(231.0)/(255.0) alpha:1],
                           [UIColor colorWithRed:(94.0)/(255.0) green:(234.0)/(255.0) blue:(234.0)/(255.0) alpha:1],
                           [UIColor colorWithRed:(155.0)/(255.0) green:(96.0)/(255.0) blue:(198.0)/(255.0) alpha:1],nil];
                          ;
    } @catch (NSException *exception) {
        
        // [UIColor colorWithRed:(247.0)/(255.0) green:(186.0)/(255.0) blue:(65.0)/(255.0) alpha:1],
      //  [UIColor colorWithRed:136/255.0 green:115/255.0 blue:162/255.0 alpha:1],
      //  [UIColor colorWithRed:58/255.0 green:149/255.0 blue:179/255.0 alpha:1],nil]
        
    } @finally {
        
    }
    
   
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        [self.pieChartOutlet reloadData];
        
        
        
        
    });
}
- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart
{
    return self.pieChartPercentageArray.count;
}

- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
    return [[self.pieChartPercentageArray objectAtIndex:index] doubleValue];
}

- (UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
//    if(pieChart == self.pieChartOutlet) return nil;
    return [self.sliceColors objectAtIndex:(index % self.sliceColors.count)];
}

#pragma mark - XYPieChart Delegate
- (void)pieChart:(XYPieChart *)pieChart willSelectSliceAtIndex:(NSUInteger)index
{
    //NSLog(@"will select slice at index %lu",(unsigned long)index);
}
- (void)pieChart:(XYPieChart *)pieChart willDeselectSliceAtIndex:(NSUInteger)index
{
    //NSLog(@"will deselect slice at index %lu",(unsigned long)index);
}
- (void)pieChart:(XYPieChart *)pieChart didDeselectSliceAtIndex:(NSUInteger)index
{
    //NSLog(@"did deselect slice at index %lu",(unsigned long)index);
}
- (void)pieChart:(XYPieChart *)pieChart didSelectSliceAtIndex:(NSUInteger)index
{
//    //NSLog(@"did select slice at index %d",index);
//    self.selectedSliceLabel.text = [NSString stringWithFormat:@"$%@",[self.slices objectAtIndex:index]];
}
- (NSString *)pieChart:(XYPieChart *)pieChart textForSliceAtIndex:(NSUInteger)index//optional
{
    return @"test";
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 30;
    
//    if(self.countArray.count==1)
//    {
//        return 255;
//    }else if (self.countArray.count==2)
//    {
//        return 285;
//    }else if (self.countArray.count==3)
//    {
//        return 315;
//    }else if (self.countArray.count==4)
//    {
//        return 335;
//    }else if (self.countArray.count==5)
//    {
//        return 365;
//    }
//    return 255;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if(tableView==self.depthNameTableView)
    {
        return self.countArray.count;
    }
    
    
    return self.countArray.count;
    
    

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView==self.portFolioTableView)
    {
    
    PortfolioDetailDepthTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"PortfolioDetailDepthTableViewCell"];
    
        
        @try {
            
            cell.companyNamelabel.text = [[[[[[self.portfolioDepthResponseDictionary objectForKey:@"data"] objectAtIndex:0] objectForKey:@"portfoliojson"] objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"symbol"];
            cell.LTPLabel.text = [[[[[[self.portfolioDepthResponseDictionary objectForKey:@"data"] objectAtIndex:0] objectForKey:@"portfoliojson"] objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"LTP"];
            NSString * changePercent = [[[[[[self.portfolioDepthResponseDictionary objectForKey:@"data"] objectAtIndex:0]objectForKey:@"portfoliojson"] objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"ChangePercent"] ;
            
            
            
            
            cell.changePercentageLabel.text = changePercent;
            
            
            NSString * quantity = [[[[[[self.portfolioDepthResponseDictionary objectForKey:@"data"] objectAtIndex:0]objectForKey:@"portfoliojson"] objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"quantity"] ;
            cell.quantityLabel.text = quantity;
            
            if([changePercent containsString:@"-"])
            {
                cell.changePercentageLabel.textColor= [UIColor colorWithRed:(242.0/255.0) green:(30.0/255.0) blue:(51.0/255.0) alpha:1];
            }else
            {
                cell.changePercentageLabel.textColor= [UIColor colorWithRed:(27.0/255.0) green:(160.0/255.0) blue:(33.0/255.0) alpha:1];
            }
            cell.percentageLabel.text= [[[[[[self.portfolioDepthResponseDictionary objectForKey:@"data"] objectAtIndex:0]objectForKey:@"portfoliojson"] objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"percentage"] ;

            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        return cell;

    }
    
     if(tableView==self.depthNameTableView)
    {
        PortfolioDepthNamesTableViewCell_h_PortfolioDepthNamesTableViewCell * cell1 = [tableView dequeueReusableCellWithIdentifier:@"depth"];
        
        
        @try {
            
            if(indexPath.row==0)
            {
                [cell1.colorButton setBackgroundColor:[UIColor colorWithRed:(45.0)/(255.0) green:(226.0)/(255.0) blue:(127.0)/(255.0) alpha:1]];
            }else if (indexPath.row==1)
            {
                [cell1.colorButton setBackgroundColor:[UIColor colorWithRed:(244.0)/(255.0) green:(100.0)/(255.0) blue:(231.0)/(255.0) alpha:1]];
            }else if (indexPath.row==2)
            {
                [cell1.colorButton setBackgroundColor:[UIColor colorWithRed:(94.0)/(255.0) green:(234.0)/(255.0) blue:(234.0)/(255.0) alpha:1]];
                
            }else if (indexPath.row==3)
            {
                [cell1.colorButton setBackgroundColor:[UIColor colorWithRed:(155.0)/(255.0) green:(96.0)/(255.0) blue:(198.0)/(255.0) alpha:1]];
                
            }else if (indexPath.row==4)
            {
                [cell1.colorButton setBackgroundColor:[UIColor colorWithRed:(247.0)/(255.0) green:(186.0)/(255.0) blue:(65.0)/(255.0) alpha:1]];
                
            }
            
            
            cell1.DepthCompanyLabelName.text = [[self.countArray objectAtIndex:indexPath.row] objectForKey:@"symbol"];
            
            NSString * quantity = [[[[[[self.portfolioDepthResponseDictionary objectForKey:@"data"] objectAtIndex:0]objectForKey:@"portfoliojson"] objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"percentage"] ;
            NSString * finalQuantity = [NSString stringWithFormat:@"(%@)",quantity];
            
            NSString * percent = @"%";
            NSString * finalFinalQuantity = [finalQuantity stringByAppendingString:percent];
            cell1.allocationPercentLabel.text= finalFinalQuantity;

        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
        
        
        
        return cell1;
    }
    
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
