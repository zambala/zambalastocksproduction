//
//  NewTwoFAViewController.h
//  NewUI
//
//  Created by Zenwise Technologies on 23/05/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewTwoFAViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *brokerImageView;
@property (weak, nonatomic) IBOutlet UILabel *brokerNameLabel;
@property (weak, nonatomic) IBOutlet UIView *twoAnsView;
@property (weak, nonatomic) IBOutlet UIView *fourAnsView;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property (weak, nonatomic) IBOutlet UILabel *qusOneLabel;
@property (weak, nonatomic) IBOutlet UILabel *qusTwoLabel;
@property (weak, nonatomic) IBOutlet UILabel *qusThreeLabel;
@property (weak, nonatomic) IBOutlet UILabel *qusFourLabel;
@property (weak, nonatomic) IBOutlet UILabel *qusFiveLabel;
@property (weak, nonatomic) IBOutlet UILabel *qusSixLabel;
@property (weak, nonatomic) IBOutlet UITextField *quesOneTF;
@property (weak, nonatomic) IBOutlet UITextField *quesTwoTF;
@property (weak, nonatomic) IBOutlet UITextField *quesThreeTF;
@property (weak, nonatomic) IBOutlet UITextField *quesFourTF;
@property (weak, nonatomic) IBOutlet UITextField *quesFiveTF;
@property (weak, nonatomic) IBOutlet UITextField *quesSixTF;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *quesViewHgt;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityInd;

@end
