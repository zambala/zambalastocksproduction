//
//  FeedListDetail.m
//  testing
//
//  Created by zenwise mac 2 on 12/19/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "FeedListDetail.h"
#import "AppDelegate.h"

@interface FeedListDetail ()
{
    AppDelegate * delegate1;
}

@end

@implementation FeedListDetail

@synthesize imageDetailArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    [self.view addSubview:self.detailScrollView];
    
//        self.detailDesLbl.text = _detailDesString;
    
    self.DesLbl.text = _descriptionString;
    self.timeLbl.text = _timeString;

    
     self.detailDesLbl.numberOfLines = 0;
     self.detailDesLbl.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake( self.detailDesLbl.frame.size.width, CGFLOAT_MAX);
    CGSize expectSize = [self.detailDesLbl sizeThatFits:maximumLabelSize];
     self.detailDesLbl.frame = CGRectMake( self.detailDesLbl.frame.origin.x,  self.detailDesLbl.frame.origin.y, expectSize.width, expectSize.height);
    
    [self.detailScrollView setContentSize:CGSizeMake(self.detailScrollView.frame.size.width, self.detailDesLbl.frame.origin.y+self.detailDesLbl.frame.size.height)];
    
    //NSLog(@"_imgString----%@", _imgString);
    
//    self.detailImg =[[UIImageView alloc]initWithFrame:CGRectMake(44, 67, 281, 141)];
//    self.detailImg.image = [UIImage imageNamed:_imgString];
//    [self.detailScrollView addSubview:_detailImg];
//    [self.detailScrollView addSubview:_detailDesLbl];
    
    self.web.delegate=self;
    
    
    if(self.detailDesString.length>0)
    {
    
    [self.web loadHTMLString:[_detailDesString stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"] baseURL:nil];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}







- (BOOL)webView:(UIWebView *)wv shouldStartLoadWithRequest:(NSURLRequest *)rq

{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
//        
//        self.activityIndicator.hidden=NO;
//        
//        [self.activityIndicator startAnimating];
//        
        
        
        
        
    });
    
    
    
    return YES;
    
    
    
}

- (void)webViewDidFinishLoading:(UIWebView *)wv

{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
//        
//        [self.activityIndicator stopAnimating];
//        
//        self.activityIndicator.hidden=YES;
        
        
        
    });
    
    
    
    
    
}



- (void)webView:(UIWebView *)wv didFailLoadWithError:(NSError *)error

{
    
//    [self.activityIndicator stopAnimating];
//    
//    self.activityIndicator.hidden=YES;
    
}




@end
