//
//  CancelledOrderCell.m
//  testing
//
//  Created by zenwise technologies on 29/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "CancelledOrderCell.h"

@implementation CancelledOrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.transactionLbl.layer.borderWidth = 1.0f;
    self.transactionLbl.layer.cornerRadius = 2.0f;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
