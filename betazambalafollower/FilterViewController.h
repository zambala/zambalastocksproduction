//
//  FilterViewController.h
//  ZambalaUSA
//
//  Created by guna on 19/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface FilterViewController : UIViewController
- (IBAction)backButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *sectorButton;
@property (weak, nonatomic) IBOutlet UIView *rankingView;
@property (weak, nonatomic) IBOutlet UIView *compareView;
@property (weak, nonatomic) IBOutlet UIView *ratingView;
@property (weak, nonatomic) IBOutlet UIView *segmentView;
@property (weak, nonatomic) IBOutlet UIView *tradeView;
@property (strong, nonatomic) IBOutlet UIButton *equityButtonOutlet;
- (IBAction)equitiesButtonAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *currencyButtonOutlet;
- (IBAction)currencyButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *derivativeButtonOutlet;
- (IBAction)derivativeButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *commoditiesButtonOutlet;
- (IBAction)commoditiesButtonAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *dayTradeButtonOutlet;
- (IBAction)dayTradeButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *longTermButtonOutlet;
- (IBAction)longTermButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *shortTermButtonOutlet;
- (IBAction)shortTermButtonAction:(id)sender;
@property BOOL eqCheckFlag,drCheckFlag,crCheckFlag,commCheckFlag,dayTradeCheckFlag,longTermCheckFlag,shortTermCheckFlag;
@property (strong, nonatomic) IBOutlet HCSStarRatingView *starRatingView;
@property NSMutableDictionary *segmentDictionary;
@property NSMutableDictionary *tradeDictionary;
- (IBAction)onActionButtonTap:(id)sender;


@end
