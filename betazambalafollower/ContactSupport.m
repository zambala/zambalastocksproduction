//
//  ContactSupport.m
//  
//
//  Created by zenwise technologies on 13/02/17.
//
//

#import "ContactSupport.h"
#import "TabBar.h"
#import "HelpshiftSupport.h"
#import <Mixpanel/Mixpanel.h>


@interface ContactSupport ()

@end

@implementation ContactSupport

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"contact_support_page"];
   [HelpshiftSupport showFAQs:self withConfig:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtn:(id)sender {
    
    TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
    
    [self presentViewController:tabPage animated:YES completion:nil];

}
@end
