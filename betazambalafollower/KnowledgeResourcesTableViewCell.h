//
//  KnowledgeResourcesTableViewCell.h
//  KnowledgeSection
//
//  Created by Zenwise Technologies on 13/06/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KnowledgeResourcesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIImageView *playButton;
@property (weak, nonatomic) IBOutlet UILabel *shortDescriptionLbl;
@property (weak, nonatomic) IBOutlet UIImageView *thumbNailImgView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgviewHgt;

@end
