//
//  WisdomGrdenView.m
//  testing
//
//  Created by zenwise mac 2 on 11/23/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "WisdomGrdenView.h"
#import "WisdomSubView.h"
#import "UIViewController+MJPopupViewController.h"
#import "MJSecondDetailViewController.h"
#import "PopUpView.h"
#import "WisdomFilterPopup.h"
#import "Equities.h"
#import "HMSegmentedControl.h"
#import "EquitiesCell1.h"
#import "EquitiesCell2.h"
#import "DerivativesCell.h"
#import "WisdomGardenOrders.h"
#import "protofolioAdviceOrders.h"
#import "StockAllocationView.h"
#import "WisdomStockView1.h"
#import "WisdomViewController.h"
#import "StockView1.h"
#import "AppDelegate.h"
#import "StockOrderView.h"
#import "NewProfile.h"
#import "StockAllocationView.h"
#import "NewPortOrderView.h"
#import <Mixpanel/Mixpanel.h>
#import "MQTTKit.h"
#include <AudioToolbox/AudioToolbox.h>
#import "WGFilterViewController.h"
#import "UIImageView+WebCache.h"





@interface WisdomGrdenView () <MJSecondPopupDelegate>
{
    HMSegmentedControl * segmentedControl;
    NSString * EPString;/*!<it store entry price*/
    NSString * SLString;/*!<it store stop loss*/
    NSString * TPString;/*!<it store target price*/
    AppDelegate * delegate1;/*!<appdelegate reference*/
    NSString * EP;/*!<it store entry price*/
    NSString * buySell;/*!<it store transaction type*/
    NSMutableArray * derivativeResponse;/*!<it store derivative advices response*/
    NSDictionary *headers ;
    NSMutableURLRequest *request;
    NSArray * filterArray;/*!<it store filtered data*/
    NSMutableArray * adviceList;/*!<it store firstname of leader*/
    NSMutableArray * filterAdvice;/*!<it store filtered data*/
    UIImageView * myImageView;/*!<no advices image*/
    NSDictionary *parameters ;
    NSData *postData ;
    NSString * wisdomBuyString11;/*!<transaction type filter*/
    NSString * wisdomDurationTypeString11;/*!<duration type filter*/
    NSString * nameAddingCheck;/*!<unused*/
    NSMutableArray * detailsArray;/*!<it store firstname of leader*/
    NSString * filterString1;
    NSString * finalLederID;
    UIRefreshControl * refreshControl;
    UIRefreshControl * refreshControl1;
    NSNumber * offset;/*!<pagination*/
    NSMutableArray * finalResponseArray;
    UIWindow * window;
    NSArray * segmentsArray;
    NSMutableArray * followingUserId;
    BOOL wisdomRefreshCheck;
    UIButton *refreshButton;
    UILabel *badgeLbl;
    int badgeValueInt;
    NSString * wisdomSymbol;
    BOOL rehitBool;
    NSString * number;
    NSMutableArray * localSegmentsArray;
}
@end
@implementation WisdomGrdenView
- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
    
    number= [loggedInUser stringForKey:@"profilemobilenumber"];
    // Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationItem.backBarButtonItem=nil;
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    if([delegate1.equities_Derivatives isEqualToString:@"EQUITY"])
    {
        self.navigationItem.title =@"Equity";
    }else if([delegate1.equities_Derivatives isEqualToString:@"DERIVATIVES"])
    {
        self.navigationItem.title =@"Derivatives";
    }
    
    filterArray=[[NSArray alloc]init];
    adviceList=[[NSMutableArray alloc]init];
    filterAdvice=[[NSMutableArray alloc]init];
    self.equitiesTableView.hidden=YES;
    if(delegate1.wisdomHint==YES)
    {   delegate1.wisdomHint=NO;
        UIWindow *currentWindow = [UIApplication sharedApplication].keyWindow;
        myImageView =[[UIImageView alloc] initWithFrame:CGRectMake(0.0,0.0,currentWindow.frame.size.width,currentWindow.frame.size.height)];
        myImageView.image=[UIImage imageNamed:@"wisdomgarden"];
        [currentWindow addSubview:myImageView ];
        [currentWindow bringSubviewToFront:myImageView];}
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTap.numberOfTapsRequired = 1;
    [myImageView setUserInteractionEnabled:YES];
    [myImageView addGestureRecognizer:singleTap];
   localSegmentsArray=[[NSMutableArray alloc]init];
    self.controller.delegate=self;
    self.controller.searchResultsUpdater=self;
    segmentsArray=[[NSArray alloc]init];
    if([delegate1.loginActivityStr isEqualToString:@"OTP1"])
    {
  
    segmentsArray=[delegate1.brokerInfoDict objectForKey:@"guestsegment"];
    }
    
    else
    {
        segmentsArray=[delegate1.brokerInfoDict objectForKey:@"segment"];
    }
    
    if([delegate1.equities_Derivatives isEqualToString:@"DERIVATIVES"])
    {
    if(segmentsArray.count>0)
    {
    
    if([segmentsArray containsObject:@2])
    {
    [localSegmentsArray addObject:@"F&O"];
    }
    if([segmentsArray containsObject:@3])
    {
    [localSegmentsArray addObject:@"CURRENCY"];
    }
    if([segmentsArray containsObject:@4])
    {
    [localSegmentsArray addObject:@"COMMODITIES"];
    }
    }
    }
    else  if([delegate1.equities_Derivatives isEqualToString:@"EQUITY"])
    {
    [localSegmentsArray addObject:@"LONG TERM"];
    [localSegmentsArray addObject:@"SHORT TERM"];
    [localSegmentsArray addObject:@"DAY TRADE"];
//    [localSegmentsArray addObject:@"COMMODITIES"];
    }
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:localSegmentsArray];
    segmentedControl.frame = CGRectMake(0, 0, self.view.frame.size.width, 40);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
     [segmentedControl setEnabled:NO];
    [self.view addSubview:segmentedControl];
    [segmentedControl setSelectedSegmentIndex:0];
    self.equitiesTableView.delegate=self;
    self.equitiesTableView.dataSource=self;
    self.derivativesTableView.hidden=YES;
    
    
    badgeLbl = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2+130,self.view.frame.size.height/2+130,30,30)];
    badgeLbl.clipsToBounds = YES;
    badgeLbl.backgroundColor = [UIColor redColor];
    badgeLbl.textColor = [UIColor whiteColor];
    badgeLbl.textAlignment = NSTextAlignmentCenter;
    badgeLbl.hidden=YES;
//    [self.view addSubview:badgeLbl];
    
    refreshButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [refreshButton addTarget:self
               action:@selector(refreshButtonAction)
     forControlEvents:UIControlEventTouchUpInside];
//    UIImage *btnImage = [UIImage imageNamed:@"add"];
//    [refreshButton setImage:btnImage forState:UIControlStateNormal];
    
    [refreshButton setBackgroundColor:[UIColor colorWithRed:2/255.0 green:30/255.0 blue:41/255.0 alpha:1.0f]];
    refreshButton.frame = CGRectMake(self.view.frame.size.width/2+100,self.view.frame.size.height/2+100,60,60);
    refreshButton.layer.cornerRadius = refreshButton.frame.size.width/2; // this value vary as per your desire
    refreshButton.clipsToBounds = YES;
    [self.view addSubview:refreshButton];
    refreshButton.hidden=YES;
    
    refreshButton.layer.shadowColor = [[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.12f] CGColor];
   refreshButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    refreshButton.layer.shadowOpacity = 1.0f;
   refreshButton.layer.shadowRadius = refreshButton.frame.size.width/2;
   refreshButton.layer.masksToBounds = NO;

   
   

    
    
}

-(void)refreshButtonAction
{
    refreshButton.hidden=YES;
    badgeLbl.hidden=YES;
    badgeValueInt=0;
    finalResponseArray=[[NSMutableArray alloc]init];
    
    offset=[NSNumber numberWithInt:0];
    
    [self.equitiesTableView setContentOffset:CGPointZero animated:YES];
    
    [self serverHit];
    
}
/*!
 @discussion userd to refresh advice list.
 */
-(void)refreshMethod
{
    [self serverHit];
}
-(void)viewDidAppear:(BOOL)animated
{
    
    
    
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    if([delegate1.equities_Derivatives isEqualToString:@"EQUITY"])
    {
        [mixpanelMini track:@"equity_page"];
        [mixpanelMini track:@"equity_long_term_page"];
        
    }else if([delegate1.equities_Derivatives isEqualToString:@"DERIVATIVES"])
    {
        [mixpanelMini track:@"derivatives_page"];
        [mixpanelMini track:@"derivatives_future_options_page"];
    }
    
    self.activityIndicator.hidden=NO;
       [segmentedControl setEnabled:NO];
    [self.activityIndicator startAnimating];
    self.equitiesTableView.hidden=YES;
    rehitBool=true;
    badgeValueInt=0;
    self.equitiesTableView.delegate=self;
    self.equitiesTableView.dataSource=self;
    [self followerListMethod];
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
    finalResponseArray=[[NSMutableArray alloc]init];
    offset=[NSNumber numberWithInt:0];
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
   
    
    NSString * verify=[prefs stringForKey:@"profileDurationBool"];
    
    
    NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
    
  
    NSString *  verifyStr=[prefs stringForKey:@"profileDuration"];
    
    if([verifyStr containsString:@"1"])
    {
       [dict setObject:[NSNumber numberWithInt:1] forKey:@"day"];
    }
    
    if([verifyStr containsString:@"2"])
    {
       [dict setObject:[NSNumber numberWithInt:2] forKey:@"short"];
    }
    
    
    if([verifyStr containsString:@"3"])
    {
       [dict setObject:[NSNumber numberWithInt:3] forKey:@"long"];
    }
    
    
    if([verify containsString:@"0"])
    {
        delegate1.durationUpdateBool=false;
    }
   else if([verify containsString:@"1"])
    {
        delegate1.profileDuration=[NSString stringWithFormat:@"%@",[dict allValues]];
        delegate1.durationUpdateBool=true;
    }
    
 
    [self serverHit];
    
    if([delegate1.pushNotificationSection isEqualToString:@"EQ.DT"])
    {
        delegate1.pushNotificationSection=@"";
        [segmentedControl setSelectedSegmentIndex:2];
        [self segmentedControlChangedValue];
        
    }
    else if([delegate1.pushNotificationSection isEqualToString:@"EQ.LT"])
    {
        delegate1.pushNotificationSection=@"";
        [segmentedControl setSelectedSegmentIndex:0];
        [self segmentedControlChangedValue];
        
    }
    else if([delegate1.pushNotificationSection isEqualToString:@"EQ.ST"])
    {
        delegate1.pushNotificationSection=@"";
        [segmentedControl setSelectedSegmentIndex:1];
        [self segmentedControlChangedValue];
        
    }
    else if([delegate1.pushNotificationSection isEqualToString:@"DV.FO"])
    {
        delegate1.pushNotificationSection=@"";
        [segmentedControl setSelectedSegmentIndex:0];
        [self segmentedControlChangedValue];
        
    }
    else if([delegate1.pushNotificationSection isEqualToString:@"DV.CC"])
    {
        delegate1.pushNotificationSection=@"";
        [segmentedControl setSelectedSegmentIndex:1];
        [self segmentedControlChangedValue];
        
    }
    else if([delegate1.pushNotificationSection isEqualToString:@"DV.CD"])
    {
        delegate1.pushNotificationSection=@"";
        [segmentedControl setSelectedSegmentIndex:2];
        [self segmentedControlChangedValue];
        
    }
    
}




-(void)followerListMethod
{

        @try {
            NSDictionary *headers = @{ @"content-type": @"application/json",
                                       @"cache-control": @"no-cache",
                                       @"authtoken":delegate1.zenwiseToken,
                                       @"deviceid":delegate1.currentDeviceId
                                       };
            
            NSDictionary * params;
            
            @try {
                
                params=@{@"clientid":delegate1.userID,
                         @"limit":@1000,
                         @"mobilenumber":number,
                         @"brokerid":[delegate1.brokerInfoDict objectForKey:@"brokerid"]
                         
                         };
                
                
            }
            @catch (NSException * e) {
                //NSLog(@"Exception: %@", e);
            }
            @finally {
                //NSLog(@"finally");
            }
            
            NSData * postData=[NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
            
            
            NSString * localUrl=[NSString stringWithFormat:@"%@follower/allleaders",delegate1.baseUrl];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:localUrl]
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:10.0];
            [request setHTTPMethod:@"POST"];
            [request setAllHTTPHeaderFields:headers];
            [request setHTTPBody:postData];
            
            
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            if(data!=nil)
                                                            {
                                                                if (error) {
                                                                    //NSLog(@"%@", error);
                                                                } else {
                                                                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                    //NSLog(@"%@", httpResponse);
                                                                    if([httpResponse statusCode]==403)
                                                                    {
                                                                        
                                                                        [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                        
                                                                    }
                                                                    else if([httpResponse statusCode]==401)
                                                                    {
                                                                        
                                                                        [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                        
                                                                    }
                                                                    else
                                                                    {
                                                                        followingUserId=[[NSMutableArray alloc]init];
                                                                        NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                       
                                                                        for(int i=0;i<[[dict objectForKey:@"results"]count];i++)
                                                                        {
                                                                            int subscribed=[[[[dict objectForKey:@"results"] objectAtIndex:i] objectForKey:@"subscribed"] intValue];
                                                                            if(subscribed==1)
                                                                            {
                                                                                
                                                                                 [followingUserId addObject:[[[dict objectForKey:@"results"] objectAtIndex:i] objectForKey:@"userid"]];
                                                                            }
                                                                        }
                                                                    }
                                                                    
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        
                                                                        wisdomRefreshCheck=true;
//                                                                        [self refreshSocket];
                                                                        
                                                                        
                                                                    });
                                                                    
                                                                }
                                                            }
                                                            else
                                                            {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                                    
                                                                    [self presentViewController:alert animated:YES completion:^{
                                                                        
                                                                    }];
                                                                    
                                                                    
                                                                    UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                        [self followerListMethod];
                                                                    }];
                                                                    
                                                                    UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                        
                                                                    }];
                                                                    
                                                                    
                                                                    [alert addAction:retryAction];
                                                                    [alert addAction:cancel];
                                                                    
                                                                });
                                                                
                                                            }
                                                            
                                                        }];
            [dataTask resume];
            
            
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
        
    
    


}

-(void)serverHit
{
    
   
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:retryAction];
        
        
        
    }
    else
    {
        if(localSegmentsArray.count>0)
        {
        @try {
            request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower/wisdomgarden",delegate1.baseUrl]]
                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                          timeoutInterval:10.0];
            
            
            wisdomDurationTypeString11=@"";
            wisdomBuyString11=@"";
            finalLederID=@"";
            NSString * wisdomFromDate;
            NSString  * wisdomToDate;
         
            
            /*!
             @discussion used to check filter is applied or not.
             */
            
            if(delegate1.wisdomBool==true)
            {
                wisdomToDate=delegate1.wisdomaToStr;
                wisdomFromDate=delegate1.wisdomFromStr;
                wisdomSymbol=delegate1.wisdomSearchSymbol;
                NSArray* words = [delegate1.wisdomBuySell componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* wisdomBuyString = [words componentsJoinedByString:@""];
                NSString* wisdomBuyString1 = [wisdomBuyString stringByReplacingOccurrencesOfString:@"(" withString:@""];
                wisdomBuyString11 = [wisdomBuyString1 stringByReplacingOccurrencesOfString:@")" withString:@""];
                
                if(wisdomBuyString11.length==0)
                {
                    wisdomBuyString11=@"";
                }
                
                if(delegate1.durationUpdateBool==true)
                {
                    NSArray* words1 = [delegate1.profileDuration componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    NSString* wisdomDurationString = [words1 componentsJoinedByString:@""];
                    NSString* wisdomDurationString1 = [wisdomDurationString stringByReplacingOccurrencesOfString:@"(" withString:@""];
                    wisdomDurationTypeString11 = [wisdomDurationString1 stringByReplacingOccurrencesOfString:@")" withString:@""];
                }
                
                if(delegate1.wisdomDuration.length>0)
                {
                    NSArray* words1 = [delegate1.wisdomDuration componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    NSString* wisdomDurationString = [words1 componentsJoinedByString:@""];
                    NSString* wisdomDurationString1 = [wisdomDurationString stringByReplacingOccurrencesOfString:@"(" withString:@""];
                    wisdomDurationTypeString11 = [wisdomDurationString1 stringByReplacingOccurrencesOfString:@")" withString:@""];
                }
                else
                {
                    wisdomDurationTypeString11=@"";
                }
                
                
                
                if(delegate1.selectLeadersID.count>0)
                {
                    
                    NSString *leaderID = [NSString stringWithFormat:@"%@",delegate1.selectLeadersID];
                    NSString * leaderID1 = [leaderID stringByReplacingOccurrencesOfString:@"(" withString:@""];
                    NSString * leaderID2 = [leaderID1 stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                    finalLederID = [leaderID2 stringByReplacingOccurrencesOfString:@")" withString:@""];
                    finalLederID = [finalLederID stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    
                }else
                {
                    finalLederID=@"";
                }
                
//
                
            }else
            {
                if(delegate1.durationUpdateBool==true)
                {
                    NSArray* words1 = [delegate1.profileDuration componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    NSString* wisdomDurationString = [words1 componentsJoinedByString:@""];
                    NSString* wisdomDurationString1 = [wisdomDurationString stringByReplacingOccurrencesOfString:@"(" withString:@""];
                    wisdomDurationTypeString11 = [wisdomDurationString1 stringByReplacingOccurrencesOfString:@")" withString:@""];
                    wisdomBuyString11=@"";
                    finalLederID=@"";
                    wisdomFromDate=@"";
                    wisdomToDate=@"";
                    wisdomSymbol=@"";
                }
                else
                {
                    wisdomDurationTypeString11=@"";
                    wisdomBuyString11=@"";
                    finalLederID=@"";
                    wisdomFromDate=@"";
                    wisdomToDate=@"";
                    wisdomSymbol=@"";
                }
                
               
            }
            
            
            
            headers = @{ @"cache-control": @"no-cache",
                         @"content-type": @"application/json",
                          @"authtoken":delegate1.zenwiseToken,
                          @"deviceid":delegate1.currentDeviceId
                         };
            NSNumber *segment;
            if([delegate1.equities_Derivatives isEqualToString:@"EQUITY"])
            {
             segment=@1;
            
            if(segmentedControl.selectedSegmentIndex==0)
            {
                
                wisdomDurationTypeString11=@"3";
            }
            
            else if(segmentedControl.selectedSegmentIndex==1)
            {
               wisdomDurationTypeString11=@"2";
            }
            else if(segmentedControl.selectedSegmentIndex==2)
            {
               wisdomDurationTypeString11=@"1";
            }
//            else if(segmentedControl.selectedSegmentIndex==3)
//            {
//
//            }
                
            }
            
            else  if([delegate1.equities_Derivatives isEqualToString:@"DERIVATIVES"])
            {
                if(segmentedControl.selectedSegmentIndex==0)
                {
                    segment=@2;
                    
                }
                
                else if(segmentedControl.selectedSegmentIndex==1)
                {
                    segment=@3;
                }
                else if(segmentedControl.selectedSegmentIndex==2)
                {
                    segment=@4;
                }
            }
            if(delegate1.wisdomTopAdvices==true)
            {
                if([delegate1.adviceType isEqualToString:@"Public"])
                {
                parameters = @{
                               @"clientid":delegate1.userID,
                               @"segment":segment,
                               @"active":@(true),
                               @"issubscribed":@(true),
                               @"limit":@10,
                               @"offset":offset,
                               @"orderby":@"createdon",
                               @"ispublic":@(true),
                               @"durationtype":wisdomDurationTypeString11,
                               @"sortby":@"DESC",
                               @"mobilenumber":number,
                                 @"brokerid":[delegate1.brokerInfoDict objectForKey:@"brokerid"]
                               };
                
                }
              else if([delegate1.adviceType isEqualToString:@"Premium"])
              {
                  parameters = @{
                                 @"clientid":delegate1.userID,
                                 @"segment":segment,
                                 @"active":@(true),
                                 @"issubscribed":@(true),
                                 @"limit":@10,
                                 @"offset":offset,
                                 @"orderby":@"createdon",
                                 @"durationtype":wisdomDurationTypeString11,
                                 @"sortby":@"DESC",
                                 @"subscriptiontypeid":@"2",
                                  @"mobilenumber":number,
                                   @"brokerid":[delegate1.brokerInfoDict objectForKey:@"brokerid"]
                                 };
              }
                
            }else
            {
                if(wisdomDurationTypeString11.length==0)
                {
                    wisdomDurationTypeString11=@"";
                }
                if(wisdomFromDate.length==0)
                {
                    wisdomFromDate=@"";
                }
                
                if(wisdomToDate.length==0)
                {
                    wisdomToDate=@"";
                }
                if(wisdomSymbol.length==0)
                {
                    wisdomSymbol=@"";
                }
                parameters = @{
                               @"clientid":delegate1.userID,
                               @"segment":segment,
                               @"active":@(true),
                               @"issubscribed":@(true),
                               @"durationtype":wisdomDurationTypeString11,
                               @"buysell":wisdomBuyString11,
                               @"limit":@10,
                               @"offset":offset,
                               @"sortby":@"DESC",
                               @"orderby":@"createdon",
                               @"leaderid":finalLederID,
                               @"fromdate":wisdomFromDate,
                               @"todate":wisdomToDate,
                               @"symbol":wisdomSymbol,
                                @"mobilenumber":number,
                                 @"brokerid":[delegate1.brokerInfoDict objectForKey:@"brokerid"]
                               };
            }
            
            postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
            [request setHTTPMethod:@"POST"];
            [request setAllHTTPHeaderFields:headers];
            [request setHTTPBody:postData];
            
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            
                                                            if(data!=nil)
                                                            {
                                                                if (error) {
                                                                    //NSLog(@"%@", error);
                                                                } else {
                                                                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                    //NSLog(@"%@", httpResponse);
                                                                    if([httpResponse statusCode]==403)
                                                                    {
                                                                        
                                                                        [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                        
                                                                    }
                                                                    else if([httpResponse statusCode]==401)
                                                                    {
                                                                        
                                                                        [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                        
                                                                    }
                                                                    else
                                                                    {
                                                                        
                                                                    NSMutableDictionary * sampleDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                    
                                                                    
                                                                    
                                                                    for(int i=0;i<[[sampleDict objectForKey:@"data"] count];i++)
                                                                    {
                                                                        
                                                                        [finalResponseArray addObject:[[sampleDict objectForKey:@"data"]objectAtIndex:i]];
                                                                        
                                                                    }
                                                                    
                                                                    self.responseArray=[[NSMutableDictionary alloc]init];
                                                                    
                                                                    
                                                                    NSArray * userArray=[finalResponseArray mutableCopy];
                                                                    NSOrderedSet *set = [NSOrderedSet orderedSetWithArray:userArray];
                                                                    NSArray *uniqueArray = [set array];
                                                                    
                                                                    
                                                                    
                                                                    [self.responseArray setObject:uniqueArray forKey:@"data"];
                                                                    
                                                                    
                                                                    }
                                                                    
                                                                }
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    
                                                                   
                                                                    [self.equitiesTableView reloadData];
                                                                    
                                                                    self.activityIndicator.hidden=YES;
                                                                       [segmentedControl setEnabled:YES];
                                                                    [self.activityIndicator stopAnimating];      self.equitiesTableView.hidden=NO;
                                                                    
                                                                    
                                                                    
                                                                });
                                                            }
                                                            
                                                            else
                                                            {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                                    
                                                                    [self presentViewController:alert animated:YES completion:^{
                                                                        
                                                                    }];
                                                                    
                                                                    
                                                                    UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                        [self serverHit];
                                                                    }];
                                                                    
                                                                    UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                       
                                                                    }];
                                                                    
                                                                    
                                                                    [alert addAction:retryAction];
                                                                    [alert addAction:cancel];
                                                                    
                                                                });
                                                                
                                                              
                                                            }
                                                            
                                                        }];
            
            [dataTask resume];
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
    }
        else
            
        {
            self.activityIndicator.hidden = YES;
            [self.activityIndicator stopAnimating];
            
            [self.equitiesTableView reloadData];
         
        }
        
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
  
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    @try
    {
    
    if(self.controller.isActive==YES&&filterAdvice.count>0)
    {
        return filterAdvice.count;
    }
   else  if([[self.responseArray objectForKey:@"data"] count]&&self.controller.isActive==NO)
    {
         return [[self.responseArray objectForKey:@"data"] count];
    }
    else
    {
    return 1;
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    @try
    {
  
    if(self.controller.isActive==YES&&self.controller.searchBar.text.length>=1)
    {
    
    }
    
    else
    {
        
        [self arrayMethod];
    }
    
    if(filterAdvice.count>0)
        
    {
        NSDictionary * contra=[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"];
        NSDictionary * openAdvice=[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"];
        NSString * contraStr=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"]];
        
        if(contra && openAdvice && ![contraStr isEqualToString:@"<null>"])
        {
            
        int messageType=[[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"contra_advice"]objectForKey:@"messagetypeid"] intValue];
        
        if(messageType!=4)
        {
            //        self.brokerMesssagesTableView.hidden=YES;
            //        self.notoficationstableView.hidden=NO;
            
            EquitiesCell1 * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
            cell.originalAdviceView.hidden=NO;
            if([delegate1.equities_Derivatives isEqualToString:@"DERIVATIVES"])
            {
                cell.durationLbl.hidden=NO;
                int duration=[[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"contra_advice"] objectForKey:@"durationtype"] intValue];
                if(duration==1)
                {
                    cell.durationLbl.text=@"DT";
                    
                    cell.durationLbl.backgroundColor=[UIColor colorWithRed:(246/255.0) green:(195/255.0) blue:(117/255.0) alpha:1.0f];
                }
                else if(duration==2)
                {
                    cell.durationLbl.text=@"ST";
                    
                    cell.durationLbl.backgroundColor=[UIColor colorWithRed:(178/255.0) green:(224/255.0) blue:(239/255.0) alpha:1.0f];
                }
                else if(duration==3)
                {
                    cell.durationLbl.text=@"LT";
                    
                    cell.durationLbl.backgroundColor=[UIColor colorWithRed:(220/255.0) green:(172/255.0) blue:(241/255.0) alpha:1.0f];
                }
            }
            else
            {
                  cell.durationLbl.hidden=YES;
            }
            cell.leaderImg.image=[UIImage imageNamed:@"default"];
            cell.equitiesImg2.layer.cornerRadius=cell.equitiesImg2.frame.size.width / 2;
            cell.equitiesImg2.clipsToBounds = YES;
            
            cell.eqitiesImg1.layer.cornerRadius=cell.eqitiesImg1.frame.size.width / 2;
            cell.eqitiesImg1.clipsToBounds = YES;
            
            cell.leaderImg.layer.cornerRadius=cell.leaderImg.frame.size.width / 2;
            cell.leaderImg.clipsToBounds = YES;
            //NSMutableAttributedString *strText = [[NSMutableAttributedString alloc] initWithString:@"SELL MUTHOOTFIN @ EP 3350.00 TP 3390.00 SL 3300.00"];
            //NSString * test = @"SELL MUTHOOTFIN @ EP 3350.00 TP 3390.00 SL 3300.00";
            // [strText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Ubuntu-Medium" size:12.5] range:NSMakeRange(0,3)];
            // [strText addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, 3)];
            
            int tickCheck = [[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"isacted"] intValue];
            if(tickCheck==0)
            {
                cell.tickImageView.hidden=YES;
            }else if (tickCheck==1)
            {
                cell.tickImageView.hidden=NO;
                
            }
            
            
        
            
            for(int i=0;i<=1;i++)
            {
                NSString * mainString;
                NSNumber * buttonNumber;
                NSString * contraBuySell;
                int buttonTitle;
            if(i==0)
            {
                 mainString=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"message"]];
                 buttonNumber = [[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"buysell"];
                
               
                buttonTitle = [buttonNumber intValue];
                if(buttonTitle==1)
                {
                    contraBuySell=@"BUY";
                                        [cell.buySellButton setTitle:@"BUY" forState:UIControlStateNormal];
                    
                    
                    
                                        [cell.buySellButton setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1.0f]];
                    
                    
                    
                }else if (buttonTitle==2)
                {
                    contraBuySell=@"SELL";
                    [cell.buySellButton setTitle:@"SELL" forState:UIControlStateNormal];
                    
                    [cell.buySellButton setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1.0f]];
                    
                }
            }
              else  if(i==1)
                {
                    mainString=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"message"]];
                     buttonNumber = [[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"buysell"];
                    buttonTitle = [buttonNumber intValue];
                  
                }
                
                
            if([mainString containsString:@"@"])
            {
                NSString * sell = [NSString stringWithFormat:@"<html><body><div style='text-align:left;display:inline-block;font-size:14px;font-family:Ubuntu;color:#4b4d52'>  %@</div> </body></html>",contraBuySell];
                NSString * muthootfin =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"symbolname"]];
                NSString * ep = @"<html><body><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#4b4d52'> @</div></html></body>";
                NSString * tp = @"<html><body><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#4b4d52'> XP</div></html></body>";
                NSString * value2 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"exitprice"]];
                NSString * sl;
                if([mainString containsString:@"Stop Loss Hit"])
                {
                    sl = @"<html><body><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#4b4d52'> Stop Loss Hit</div></html></body>";
                }
                else if([mainString containsString:@"Target Achieved"])
                {
                    sl = @"<html><body><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#4b4d52'> Profit Target Achieved</div></html></body>";
                }
                else if([mainString containsString:@"Book Profit"])
                {
                    sl = @"<html><body><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#4b4d52'> Book Profit</div></html></body>";
                }
                else if([mainString containsString:@"Book Loss"])
                {
                    sl = @"<html><body><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#4b4d52'> Book Loss</div></html></body>";
                }
                
            

                NSString * string1 = [sell stringByAppendingString:muthootfin];
                NSString *string2 = [string1 stringByAppendingString:ep];
                NSString * string4 = [string2 stringByAppendingString:tp];
                NSString * string5 = [string4 stringByAppendingString:value2];
                NSString * string6 = [string5 stringByAppendingString:sl];
              
                NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[string6 dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];

                //UILabel * myLabel = [[UILabel alloc] init];
                // myLabel.attributedText = attrStr;


                cell.valueChangeLabel.attributedText=attrStr;
                
//                cell.valueChangeLabel.text=[mainString uppercaseString];
                
//                cell.oLbl.text=@"C";
            }
            
           
            
            else
            {
                
//                cell.oLbl.text=@"O";
                TPString=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"targetprice"]];
                
                if ([mainString isEqual:[NSNull null]]||mainString==nil||[mainString isEqualToString:@"<null>"] ||  [TPString isEqual:[NSNull null]]||TPString==nil||[TPString isEqualToString:@"<null>"]   ) {
                    cell.valueChangeLabel.text =@"<Null>";
                }
                
                else{
                    
                    @try {
                        
                        
                        //NSLog(@"main String %@",mainString);
                        
                        
                        
                        NSArray * mainArray=[mainString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                        
                        
                        
                        //NSLog(@"array  %@",[mainArray objectAtIndex:0]);
                        
                        //NSLog(@"array  %@",[mainArray objectAtIndex:1]);
                        
                        //                NSString * buySell=[NSString stringWithFormat:@"%@",[mainArray objectAtIndex:0]];
                        
                        if(buttonTitle==1)
                        {
                            buySell=@"BUY";
                            
                        }else if (buttonTitle==2)
                        {
                            buySell=@"SELL";
                            
                        }
                        
                        NSString * string=[NSString stringWithFormat:@"%@",[mainArray objectAtIndex:1]];
                        
                        
                        
                        NSArray * companyArray=[string componentsSeparatedByString:@";"];
                        
                        
                        
                        //NSLog(@" array1 %@",companyArray);
                        
                        
                        
                        //               for (int i=1; i<[companyArray count];i++) {
                        
                        
                        
                        //                   NSString * string11=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:0]];
                        //
                        //
                        //
                        //                   NSArray * array2=[string11 componentsSeparatedByString:@";"];
                        //
                        //                   //NSLog(@"%@",array2);
                        
                        NSString * companyName=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:0]];
                        NSString * EP1=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:1]];
                        NSString * TP=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:2]];
                        NSString * SL;
                        NSArray * SLArray;
                        if([mainString containsString:@"SL="])
                        {
                          SL=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:3]];
                          SLArray=[SL componentsSeparatedByString:@"="];
                          SLString=[NSString stringWithFormat:@"%@",[SLArray objectAtIndex:1]];
                            
                        }
                        else
                        {
                            SL=@"";
                            SLString=@"";
                        }
                       
                        NSArray * EPArray=[EP1 componentsSeparatedByString:@"="];
                        NSArray * TPArray=[TP componentsSeparatedByString:@"="];
                      
                        
                        EPString=[NSString stringWithFormat:@"%@",[EPArray objectAtIndex:1]];
                        TPString=[NSString stringWithFormat:@"%@",[TPArray objectAtIndex:1]];
                      
                        
                        
                        NSString * dummyfinalStr=[NSString stringWithFormat:@"%@ %@ @%@%@%@",buySell,companyName,EP1,TP,SL];
                        
                        NSString * finalStr = [dummyfinalStr stringByReplacingOccurrencesOfString:@"=" withString:@" "];
                        
                        //NSLog(@"%@",finalStr);
                        
                        //NSLog(@"COMPANY %@",companyName);
                        
                        
                        //                              if (i==1) {
                        //                                  EP=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                        //
                        //                                  //NSLog(@"EP %@",[array2 objectAtIndex:1]);
                        //
                        //                              }
                        
                        //                   if (i==2) {
                        //                       EPString=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                        //
                        //                       //NSLog(@"TP %@",[array2 objectAtIndex:1]);
                        //
                        //                   }
                        //
                        //                   if (i==3) {
                        //                       SLString=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                        //
                        //                       //NSLog(@"SL %@",[array2 objectAtIndex:1]);
                        //
                        //                   }
                        //               }
                        //
                        
                        
                        NSString * sell = [NSString stringWithFormat:@"<html><body><div style='text-align:left;display:inline-block;font-size:12px;font-family:Ubuntu;color:#4b4d52'>  %@</div> </body></html>",buySell];
                        NSString * muthootfin =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",[companyArray objectAtIndex:0]];
                        NSString * ep = @"<html><body><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#4b4d52'> @ EP</div></html></body>";
                        NSString * value1 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",EPString];
                        NSString * tp = @"<html><body><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#4b4d52'> TP</div></html></body>";
                        NSString * value2 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",TPString];
                        NSString * sl = @"<html><body><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#4b4d52'> SL</div></html></body>";
                        NSString * value3 =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",SLString] ;
                        
                        NSString * string1 = [sell stringByAppendingString:muthootfin];
                        NSString *string2 = [string1 stringByAppendingString:ep];
                        NSString * string3 = [string2 stringByAppendingString:value1];
                        NSString * string4 = [string3 stringByAppendingString:tp];
                        NSString * string5 = [string4 stringByAppendingString:value2];
                        NSString * string6 = [string5 stringByAppendingString:sl];
                        NSString * finalString;
                        if([mainString containsString:@"SL="])
                        {
                            finalString = [string6 stringByAppendingString:value3];
                        }
                        else
                        {
                            finalString = string5;
                        }
                       
                        
                        
                        NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[finalString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                        
                        //UILabel * myLabel = [[UILabel alloc] init];
                        // myLabel.attributedText = attrStr;
                   
                        
                        if(i==0)
                        {
                            cell.valueChangeLabel.attributedText=attrStr;
                        }
                        else  if(i==1)
                        {
                            cell.originalMessageLabel.attributedText=attrStr;
                        }
                    }
                    @catch (NSException * e) {
                        //NSLog(@"Exception: %@", e);
                    }
                    @finally {
                        //NSLog(@"finally");
                    }
                    
                }
                
                
                
            }
                
            }
            
           
            
            cell.ltpLabel.text =[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"lasttradedprice"]];
            
            NSString * changePercentlabel = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"contra_advice"]objectForKey:@"changepercent"]];
            
            NSString * percent =@"  ";
            NSString * finalChangePercentlabel = [changePercentlabel stringByAppendingString:percent];
            if([changePercentlabel isEqual:[NSNull null]]||changePercentlabel==nil||[changePercentlabel isEqualToString:@"<null>"])
            {
                
                
                
                NSString * percentage = @"";
                NSString * zero = @"  0";
                NSString* final = [zero stringByAppendingString:percentage];
                cell.changePercentlabel.text=final;
                cell.changePercentlabel.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
            }
            else
            {
                
                if([finalChangePercentlabel containsString:@"-"])
                {
                    cell.changePercentlabel.text=finalChangePercentlabel;
                    cell.changePercentlabel.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                    cell.adviceButton.layer.borderColor =[[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1] CGColor];
                    [cell.adviceButton setTitleColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1] forState:UIControlStateNormal];
                    [cell.adviceButton setTitle:@"Closing Advice" forState:UIControlStateNormal];
//                    cell.oLbl.backgroundColor=[UIColor colorWithRed:(238/255.0) green:(43/255.0) blue:(53/255.0) alpha:1];
                }
                else{
                    cell.changePercentlabel.text=finalChangePercentlabel;
                    cell.changePercentlabel.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                    cell.adviceButton.layer.borderColor =[[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1] CGColor];
                    [cell.adviceButton setTitleColor:[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1] forState:UIControlStateNormal];
                    [cell.adviceButton setTitle:@"Closing Advice" forState:UIControlStateNormal];
//                    cell.oLbl.backgroundColor=[UIColor colorWithRed:(86/255.0) green:(168/255.0) blue:(66/255.0) alpha:1];
                }
                
                
            }
            
            NSString * actedBy = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"contra_advice"]objectForKey:@"actedby"]];
            
            if([actedBy isEqual:[NSNull null]]||actedBy==nil||[actedBy isEqualToString:@"<null>"])
            {
                cell.actedByLabel.text=@"0";
            }else
            {
                cell.actedByLabel.text=actedBy;
            }
            NSString * sharesSold = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"sharesold"]];
            if([sharesSold isEqual:[NSNull null]]||sharesSold==nil||[sharesSold isEqualToString:@"<null>"])
            {
                cell.sharesSold.text=@"0";
            }else
            {
                cell.sharesSold.text=sharesSold;
            }
            
            
            
            
            NSString * dateStr=[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"createdon"];
            
            
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
            NSDate *date = [dateFormatter dateFromString:dateStr]; // create date from string
            
            // change to a readable time format and change to local time zone
            [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            NSString *finalDate = [dateFormatter stringFromDate:date];
            
            
            
            
            
            cell.dateAndTimeLabel.text=finalDate;
            NSString * firstName=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"firstname"]];
            NSString * lastName=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"lastname"]];
            
            if([firstName isEqualToString:@"<null>"])
            {
                firstName=@"";
            }
            
            if([lastName isEqualToString:@"<null>"])
            {
                lastName=@"";
            }
            
            cell.profileName.text=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
            
          
            
            // cell.valueChangeLabel.attributedText=strText;
            // [strText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Ubuntu-Medium" size:12.5] range:NSMakeRange(6, )];
            
            
       
            
            
            [cell.buySellButton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.leaderProfileAction addTarget:self action:@selector(leaderProfileMethod:) forControlEvents:UIControlEventTouchUpInside];
            //       dispatch_async(dispatch_get_main_queue(), ^{
            //           cell.profileImg.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"logourl"]]]];
            //
            //
            //       });
            
            
           
            
            
            @try {
                
                NSString * actedBy =[ [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"contra_advice"]objectForKey:@"actedby"];
                
                
                if([actedBy isEqual:[NSNull null]])
                {
                    cell.actedByLabel.text=@"0";
                }else
                {
                    int actedByInt=[actedBy intValue];
                    cell.actedByLabel.text = [NSString stringWithFormat:@"%i",actedByInt];
                }
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            @try {
                
                NSString * sharesSold =[ [[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"sharesold"];
                
                
                
                if([sharesSold isEqual:[NSNull null]])
                {
                    cell.sharesSold.text = @"0";
                }else
                {
                    int sharesSoldInt=[sharesSold intValue];
                    cell.sharesSold.text = [NSString stringWithFormat:@"%i",sharesSoldInt];
                }
                
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            @try {
                
                NSString * averageProfit =[ [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"contra_advice"]objectForKey:@"averageprofit"];
                
                
                
                if([averageProfit isEqual:[NSNull null]])
                {
                    
                }else
                {
                    int sharesSoldInt=[averageProfit intValue];
                   
                }
                
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            
            
            NSString * duration=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"durationtype"]];
            
            int durationInt=[duration intValue];
            
            if(durationInt==1)
            {
               
            }
            
            else if(durationInt==2)
            {
              
            }
            else if(durationInt==3)
            {
                
                
                
          
                
            }
            
          
            
            NSString * public=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"contra_advice"]objectForKey:@"ispublic"]];
            int publicInt=[public intValue];
            
            if(publicInt==1)
            {
                cell.premiumImg.hidden=NO;
               
                cell.premiumImg.image=[UIImage imageNamed:@"publicnew"];
              
            
                
                NSString * source = [NSString stringWithFormat:@"%@",[[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"]objectForKey:@"otherinfo"]objectForKey:@"source"]];
                cell.sourceLabel.hidden=NO;
                cell.sourceDataLabel.hidden=NO;
                cell.sourceDataLabel.text = source;
                cell.analystName.hidden=NO;
                cell.analystNameHeightConstant.constant=21;
                NSString * analystName = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"]objectForKey:@"leaderorgname"]];
               NSString * dummyAnalystName= [analystName stringByReplacingOccurrencesOfString:@" " withString:@""];
                dummyAnalystName=[dummyAnalystName lowercaseString];
                cell.profileName.text=analystName;
                NSString * firstName = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"]objectForKey:@"firstname"]];
                NSString * lastname = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"]objectForKey:@"lastname"]];
               
                NSString * dummyName;
             
                if([lastname isEqual:[NSNull null]]||[lastname containsString:@"<null>"])
                {
                    lastname=@"";
                    NSString * name = [NSString stringWithFormat:@"%@ %@",firstName,lastname];
                    dummyName = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
                    dummyName=[dummyName lowercaseString];
                    NSString * profileName=[cell.profileName.text stringByReplacingOccurrencesOfString:@" " withString:@""];
                    profileName=[profileName lowercaseString];
                    if(![dummyName isEqualToString:profileName])
                    {
                          cell.organizationNameLbl.text=[NSString stringWithFormat:@"%@",cell.profileName.text];
                        cell.profileName.text= [NSString stringWithFormat:@"%@ %@",firstName,lastname];
                         cell.expertXconstraint.constant=10;
                      
                    }
                    else
                    {
                         cell.expertXconstraint.constant=20;
                         cell.organizationNameLbl.text=@"";
                    }
                    
                    
                }else
                {
                    NSString * name = [NSString stringWithFormat:@"%@ %@",firstName,lastname];
                    dummyName = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
                    dummyName=[dummyName lowercaseString];
                    NSString * profileName=[cell.profileName.text stringByReplacingOccurrencesOfString:@" " withString:@""];
                    profileName=[profileName lowercaseString];
                    if(![dummyName isEqualToString:profileName])
                    {
                        cell.organizationNameLbl.text=[NSString stringWithFormat:@"%@",cell.profileName.text];
                        cell.profileName.text= [NSString stringWithFormat:@"%@ %@",firstName,lastname];
                        cell.expertXconstraint.constant=10;
                        
                    }
                    
                    else
                    {
                        cell.expertXconstraint.constant=20;
                         cell.organizationNameLbl.text=@"";
                    }
                    
                }
                if([dummyAnalystName containsString:dummyName])
                {
                    cell.analystName.hidden=YES;
                    cell.analystNameHeightConstant.constant=0;
                }else
                {
                    cell.analystName.hidden=NO;
                    cell.analystNameHeightConstant.constant=21;
//                    cell.profileName.text = analystName;
                }
                
               
               
               
            }

            else if(publicInt==0)
            {
                cell.premiumImg.hidden=YES;
                cell.expertXconstraint.constant=20;
                cell.sourceLabel.hidden=YES;
                 cell.organizationNameLbl.text=@"";
                cell.sourceDataLabel.hidden=YES;
                NSString * firstName=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"firstname"]];
                NSString * lastName=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"lastname"]];
                
                if([firstName isEqualToString:@"<null>"])
                {
                    firstName=@"";
                }
                
                if([lastName isEqualToString:@"<null>"])
                {
                    lastName=@"";
                }
                
                cell.profileName.text=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                cell.analystName.hidden=YES;
                cell.analystNameHeightConstant.constant=0;
                
                NSString * sub=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"contra_advice"] objectForKey:@"subscriptiontypeid"]];
                
                int subInt=[sub intValue];
                
                if(subInt==1)
                {
                    cell.premiumImg.hidden=YES;
                    
                }
                
                else if(subInt==2)
                {
                    cell.premiumImg.hidden=NO;
                    
                    cell.premiumImg.image=[UIImage imageNamed:@"premiumNew"];
                    
                }
                
                
            }
            
            
            cell.leaderImg.image=nil;
            
            if(publicInt==1)
            {
                NSURL *url;
                @try {
                     url =  [NSURL URLWithString:[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"logourl"]];
                } @catch (NSException *exception) {
                    
                    
                    
                } @finally {
                    
                }
                
                  [cell.leaderImg sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"dpDefault"]];
//                NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//                    if (data) {
//                        UIImage *image = [UIImage imageWithData:data];
//                        if (image) {
//                            dispatch_async(dispatch_get_main_queue(), ^{
//                                EquitiesCell1 *cell = [self.equitiesTableView cellForRowAtIndexPath:indexPath];
//                                if (cell)
//                                {
//                                    cell.leaderImg.image = image;
//                                }
//                            });
//                        }
//                    }
//                }];
//                [task resume];
            }else
            {
            
            NSURL *url;
            @try {
                url =  [NSURL URLWithString:[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"logourl"]];
            } @catch (NSException *exception) {
                
                
                
            } @finally {
                
            }
            
             [cell.leaderImg sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"dpDefault"]];
//            NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//                if (data) {
//                    UIImage *image = [UIImage imageWithData:data];
//                    if (image) {
//                        dispatch_async(dispatch_get_main_queue(), ^{
//
//                            EquitiesCell1 *cell = [self.equitiesTableView cellForRowAtIndexPath:indexPath];
//                            if (cell)
//                            {
//                                cell.leaderImg.image = image;
//                            }
//                        });
//                    }
//                }
//            }];
//            [task resume];
            }
            
            
            //original advice//
            
            NSString * originalLtp1=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"lasttradedprice"]];
            
            if([originalLtp1 isEqual:[NSNull null]]||[originalLtp1 isEqualToString:@"<null>"])
            {
                cell.originalLtp.text=@"0.00";
            }
            else
            {
                cell.originalLtp.text=originalLtp1;
                
            }
            
            NSString * originalChg=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"changepercent"]];
            
            if([originalChg isEqual:[NSNull null]]||[originalChg isEqualToString:@"<null>"])
            {
                cell.originalPL.text=@"0.00";
            }
            else
            {   if([originalChg containsString:@"-"])
            {
              
                cell.originalPL.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
               
                cell.originalAdviceTypeButton.layer.borderColor =[[UIColor colorWithRed:(238/255.0) green:(43/255.0) blue:(53/255.0) alpha:1] CGColor];
                [cell.originalAdviceTypeButton setTitleColor:[UIColor colorWithRed:(238/255.0) green:(43/255.0) blue:(53/255.0) alpha:1] forState:UIControlStateNormal];
                //                    cell.oLbl.backgroundColor=[UIColor colorWithRed:(238/255.0) green:(43/255.0) blue:(53/255.0) alpha:1];
            }
            else{
               
                cell.originalPL.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                cell.originalAdviceTypeButton.layer.borderColor = [[UIColor colorWithRed:(87.0)/255 green:(160.0)/255 blue:(55.0)/255 alpha:1.0] CGColor];
                [cell.originalAdviceTypeButton setTitleColor:[UIColor colorWithRed:(87.0)/255 green:(160.0)/255 blue:(55.0)/255 alpha:1.0] forState:UIControlStateNormal];
                //                    cell.oLbl.backgroundColor=[UIColor colorWithRed:(86/255.0) green:(168/255.0) blue:(66/255.0) alpha:1];
            }
                cell.originalPL.text=originalChg;
                
            }
            
            NSString * originalActed=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"actedby"]];
            
            if([originalActed isEqual:[NSNull null]]||[originalActed isEqualToString:@"<null>"])
            {
                cell.originalActedBy.text=@"0.00";
            }
            else
            {
                cell.originalActedBy.text=originalActed;
                
            }
            
            NSString * originalShares=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"sharesold"]];
            
            if([originalShares isEqual:[NSNull null]]||[originalShares isEqualToString:@"<null>"])
            {
                cell.originalSharesLbl.text=@"0.00";
            }
            else
            {
                cell.originalSharesLbl.text=originalShares;
                
            }
            
           
            NSString * dateStr1=[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"createdon"];
            
            
            
            NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
            [dateFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
            [dateFormatter1 setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
            NSDate *date1 = [dateFormatter1 dateFromString:dateStr1]; // create date from string
            
            // change to a readable time format and change to local time zone
            [dateFormatter1 setDateFormat:@"dd MMM yyyy HH:mm"];
            [dateFormatter1 setTimeZone:[NSTimeZone localTimeZone]];
            NSString *finalDate1 = [dateFormatter1 stringFromDate:date1];
            cell.originalDateLabel.text=finalDate1;
            if(filterAdvice.count>4)
            {
            NSDictionary *data = [filterAdvice objectAtIndex:indexPath.row];
            BOOL lastItemReached = [data isEqual:[filterAdvice lastObject]];
            if (!lastItemReached && indexPath.row == [filterAdvice count] - 3)
            {
                if(rehitBool==true)
                {
                    [self refreshTableVeiwList];
                }
            }
            }
            return cell;
        }
        
        
        else{
            
            EquitiesCell2 * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            cell.oLabel.layer.cornerRadius = 12.5f;
            cell.oLabel.layer.masksToBounds = YES;
            cell.ltLabel.layer.cornerRadius = 12.5f;
            cell.ltLabel.layer.masksToBounds=YES;
            
            //
            
            cell.profileImage.layer.cornerRadius=cell.profileImage.frame.size.width / 2;
            cell.profileImage.clipsToBounds = YES;
            
            
            NSString * firstName = [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"firstname"];
            
            NSString * messageName = [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"messagename"];
            
            cell.adivceName.text=messageName;
            
            
            
            
            cell.profileName.text=firstName;
            
            
            NSString * dateStr=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"createdon"];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
            NSDate *date = [dateFormatter dateFromString:dateStr]; // create date from string
            
            // change to a readable time format and change to local time zone
            [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            NSString *finalDate = [dateFormatter stringFromDate:date];
            
            
            
            
            
            cell.dateAndTime.text=finalDate;
            
            
            
            
            NSString * assumedInvestment = [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"amount"];
            NSString * rupee = @"₹";
            
            NSString * finalString = [rupee stringByAppendingString:assumedInvestment];
            
            cell.assumedInvestment.text = finalString;
            
            NSString * actedBy = [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"actedby"];
            
            if([actedBy isEqual:[NSNull null]])
            {
                cell.actedByLabel.text=@"0";
            }else
            {
                cell.actedByLabel.text=actedBy;
            }
            
            NSString * sharesBought = [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"sharesold"];
            
            if([sharesBought isEqual:[NSNull null]])
            {
                cell.sharesBroughtLabel.text=@"0";
            }else
            {
                cell.sharesBroughtLabel.text=sharesBought;
            }
            
            
            cell.portfolioArray=[[NSMutableArray alloc]init];
             @try {
            
            cell.portfolioArray= [[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"portfoliojson"] objectForKey:@"stocks"];
             } @catch (NSException *exception) {
                 
                 
                 
             } @finally {
                 
             }
            
            
            
            //NSLog(@"cell portfolio array:%@",cell.portfolioArray);
            [cell.portfolioTblView reloadData];
            
            //       dispatch_async(dispatch_get_main_queue(), ^{
            //           cell.profileImage.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"logourl"]]]];
            //
            //
            //       });
            
            cell.profileImage.image=nil;
           
            NSURL *url;
            @try {
                url =  [NSURL URLWithString:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"logourl"]];
            } @catch (NSException *exception) {
                
                
                
            } @finally {
                
            }
            
            
            NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (data) {
                    UIImage *image = [UIImage imageWithData:data];
                    if (image) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            EquitiesCell2 *updateCell = (id)[tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                updateCell.profileImage.image = image;
                        });
                    }
                }
            }];
            [task resume];
            
            [cell.buySellButton addTarget:self action:@selector(portBuySellAction:) forControlEvents:UIControlEventTouchUpInside];
            
            if(filterAdvice.count>4)
            {
            NSDictionary *data1 = [filterAdvice objectAtIndex:indexPath.row];
            BOOL lastItemReached1 = [data1 isEqual:[filterAdvice lastObject]];
            if (!lastItemReached1 && indexPath.row == [filterAdvice count] - 3)
            {
                if(rehitBool==true)
                {
                [self refreshTableVeiwList];
                }
            }
            }
            return cell;
        }
        
    }
    
        
        else if(openAdvice)
        {
           
            int messageType=[[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"open_advice"]objectForKey:@"messagetypeid"] intValue];
            
            if(messageType!=4)
            {
                //        self.brokerMesssagesTableView.hidden=YES;
                //        self.notoficationstableView.hidden=NO;
                
                EquitiesCell1 * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
                cell.originalAdviceView.hidden=YES;
                if([delegate1.equities_Derivatives isEqualToString:@"DERIVATIVES"])
                {
                    cell.durationLbl.hidden=NO;
                    int duration=[[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"open_advice"] objectForKey:@"durationtype"] intValue];
                    if(duration==1)
                    {
                        cell.durationLbl.text=@"DT";
                        
                        cell.durationLbl.backgroundColor=[UIColor colorWithRed:(246/255.0) green:(195/255.0) blue:(117/255.0) alpha:1.0f];
                    }
                    else if(duration==2)
                    {
                        cell.durationLbl.text=@"ST";
                        
                        cell.durationLbl.backgroundColor=[UIColor colorWithRed:(178/255.0) green:(224/255.0) blue:(239/255.0) alpha:1.0f];
                    }
                    else if(duration==3)
                    {
                        cell.durationLbl.text=@"LT";
                        
                        cell.durationLbl.backgroundColor=[UIColor colorWithRed:(220/255.0) green:(172/255.0) blue:(241/255.0) alpha:1.0f];
                    }
                }
                else
                {
                    cell.durationLbl.hidden=YES;
                }
                cell.leaderImg.image=[UIImage imageNamed:@"default"];
                cell.equitiesImg2.layer.cornerRadius=cell.equitiesImg2.frame.size.width / 2;
                cell.equitiesImg2.clipsToBounds = YES;
                
                cell.eqitiesImg1.layer.cornerRadius=cell.eqitiesImg1.frame.size.width / 2;
                cell.eqitiesImg1.clipsToBounds = YES;
                
                cell.leaderImg.layer.cornerRadius=cell.leaderImg.frame.size.width / 2;
                cell.leaderImg.clipsToBounds = YES;
                //NSMutableAttributedString *strText = [[NSMutableAttributedString alloc] initWithString:@"SELL MUTHOOTFIN @ EP 3350.00 TP 3390.00 SL 3300.00"];
                //NSString * test = @"SELL MUTHOOTFIN @ EP 3350.00 TP 3390.00 SL 3300.00";
                // [strText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Ubuntu-Medium" size:12.5] range:NSMakeRange(0,3)];
                // [strText addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, 3)];
                
                int tickCheck = [[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"isacted"] intValue];
                if(tickCheck==0)
                {
                    cell.tickImageView.hidden=YES;
                }else if (tickCheck==1)
                {
                    cell.tickImageView.hidden=NO;
                    
                }
                
                
                
                
              
                    NSString * mainString;
                    NSNumber * buttonNumber;
                    
                
                
                        mainString=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"message"]];
                        buttonNumber = [[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"buysell"];
                
                    
                    
                    int buttonTitle = [buttonNumber intValue];
                NSString * contraBuySell;
                    if(buttonTitle==1)
                    {
                        contraBuySell=@"BUY";
                                            [cell.buySellButton setTitle:@"BUY" forState:UIControlStateNormal];
                        
                        
                        
                                            [cell.buySellButton setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1.0f]];
                        
                        
                        
                    }else if (buttonTitle==2)
                    {
                         contraBuySell=@"SELL";
                        [cell.buySellButton setTitle:@"SELL" forState:UIControlStateNormal];
                        
                        [cell.buySellButton setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1.0f]];
                        
                    }
                    if([mainString containsString:@"@"])
                    {
                        NSString * sell = [NSString stringWithFormat:@"<html><body><div style='text-align:left;display:inline-block;font-size:14px;font-family:Ubuntu;color:#4b4d52'>  %@</div> </body></html>",contraBuySell];
                        NSString * muthootfin =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"symbolname"]];
                        NSString * ep = @"<html><body><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#4b4d52'> @</div></html></body>";
                        NSString * tp = @"<html><body><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#4b4d52'> XP</div></html></body>";
                        NSString * value2 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"exitprice"]];
                        NSString * sl;
                        if([mainString containsString:@"Stop Loss Hit"]||[mainString containsString:@"Book Loss"])
                        {
                            sl = @"<html><body><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#4b4d52'> Stop Loss Hit</div></html></body>";
                        }
                        else if([mainString containsString:@"Target Achieved"]||[mainString containsString:@"Book Profit"])
                        {
                            sl = @"<html><body><div style='display:inline-block;text-align:left;font-size:14px;font-family:Ubuntu;color:#4b4d52'> Profit Target Achieved</div></html></body>";
                        }
                        
                        
                        
                        
                        NSString * string1 = [sell stringByAppendingString:muthootfin];
                        NSString *string2 = [string1 stringByAppendingString:ep];
                        NSString * string4 = [string2 stringByAppendingString:tp];
                        NSString * string5 = [string4 stringByAppendingString:value2];
                        NSString * string6 = [string5 stringByAppendingString:sl];
                        
                        NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[string6 dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                        
                        //UILabel * myLabel = [[UILabel alloc] init];
                        // myLabel.attributedText = attrStr;
                        
                        
                        cell.valueChangeLabel.attributedText=attrStr;
                        cell.valueChangeLabel.text=[mainString uppercaseString];
                        
                        //                cell.oLbl.text=@"C";
                    }
                    
                    
                    
                    else
                    {
                        
                        //                cell.oLbl.text=@"O";
                        TPString=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"targetprice"]];
                        
                        if ([mainString isEqual:[NSNull null]]||mainString==nil||[mainString isEqualToString:@"<null>"] ||  [TPString isEqual:[NSNull null]]||TPString==nil||[TPString isEqualToString:@"<null>"]   ) {
                            cell.valueChangeLabel.text =@"<Null>";
                        }
                        
                        else{
                            
                            @try {
                                
                                
                                //NSLog(@"main String %@",mainString);
                                
                                
                                
                                NSArray * mainArray=[mainString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                                
                                
                                
                                //NSLog(@"array  %@",[mainArray objectAtIndex:0]);
                                
                                //NSLog(@"array  %@",[mainArray objectAtIndex:1]);
                                
                                //                NSString * buySell=[NSString stringWithFormat:@"%@",[mainArray objectAtIndex:0]];
                                
                                if(buttonTitle==1)
                                {
                                    buySell=@"BUY";
                                    
                                }else if (buttonTitle==2)
                                {
                                    buySell=@"SELL";
                                    
                                }
                                
                                NSString * string=[NSString stringWithFormat:@"%@",[mainArray objectAtIndex:1]];
                                
                                
                                
                                NSArray * companyArray=[string componentsSeparatedByString:@";"];
                                
                                
                                
                                //NSLog(@" array1 %@",companyArray);
                                
                                
                                
                                //               for (int i=1; i<[companyArray count];i++) {
                                
                                
                                
                                //                   NSString * string11=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:0]];
                                //
                                //
                                //
                                //                   NSArray * array2=[string11 componentsSeparatedByString:@";"];
                                //
                                //                   //NSLog(@"%@",array2);
                                
                                NSString * companyName=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:0]];
                                NSString * EP1=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:1]];
                                NSString * TP=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:2]];
                                NSString * SL;
                                NSArray * SLArray;
                                if([mainString containsString:@"SL="])
                                {
                                    SL=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:3]];
                                    SLArray=[SL componentsSeparatedByString:@"="];
                                    SLString=[NSString stringWithFormat:@"%@",[SLArray objectAtIndex:1]];
                                    
                                }
                                else
                                {
                                    SL=@"";
                                    SLString=@"";
                                }
                                
                                NSArray * EPArray=[EP1 componentsSeparatedByString:@"="];
                                NSArray * TPArray=[TP componentsSeparatedByString:@"="];
                                
                                
                                EPString=[NSString stringWithFormat:@"%@",[EPArray objectAtIndex:1]];
                                TPString=[NSString stringWithFormat:@"%@",[TPArray objectAtIndex:1]];
                                
                                
                                
                                NSString * dummyfinalStr=[NSString stringWithFormat:@"%@ %@ @%@%@%@",buySell,companyName,EP1,TP,SL];
                                
                                NSString * finalStr = [dummyfinalStr stringByReplacingOccurrencesOfString:@"=" withString:@" "];
                                
                                //NSLog(@"%@",finalStr);
                                
                                //NSLog(@"COMPANY %@",companyName);
                                
                                
                                //                              if (i==1) {
                                //                                  EP=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                                //
                                //                                  //NSLog(@"EP %@",[array2 objectAtIndex:1]);
                                //
                                //                              }
                                
                                //                   if (i==2) {
                                //                       EPString=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                                //
                                //                       //NSLog(@"TP %@",[array2 objectAtIndex:1]);
                                //
                                //                   }
                                //
                                //                   if (i==3) {
                                //                       SLString=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                                //
                                //                       //NSLog(@"SL %@",[array2 objectAtIndex:1]);
                                //
                                //                   }
                                //               }
                                //
                                
                                
                                NSString * sell = [NSString stringWithFormat:@"<html><body><div style='text-align:left;display:inline-block;font-size:12px;font-family:Ubuntu;color:#4b4d52'>  %@</div> </body></html>",buySell];
                                NSString * muthootfin =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",[companyArray objectAtIndex:0]];
                                NSString * ep = @"<html><body><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#4b4d52'> @ EP</div></html></body>";
                                NSString * value1 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",EPString];
                                NSString * tp = @"<html><body><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#4b4d52'> TP</div></html></body>";
                                NSString * value2 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",TPString];
                                NSString * sl = @"<html><body><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#4b4d52'> SL</div></html></body>";
                                NSString * value3 =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",SLString] ;
                                
                                NSString * string1 = [sell stringByAppendingString:muthootfin];
                                NSString *string2 = [string1 stringByAppendingString:ep];
                                NSString * string3 = [string2 stringByAppendingString:value1];
                                NSString * string4 = [string3 stringByAppendingString:tp];
                                NSString * string5 = [string4 stringByAppendingString:value2];
                                NSString * string6 = [string5 stringByAppendingString:sl];
                                NSString * finalString;
                                if([mainString containsString:@"SL="])
                                {
                                    finalString = [string6 stringByAppendingString:value3];
                                }
                                else
                                {
                                    finalString = string5;
                                }
                                
                                
                                
                                NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[finalString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                                
                                //UILabel * myLabel = [[UILabel alloc] init];
                                // myLabel.attributedText = attrStr;
                                
                                
                                   cell.valueChangeLabel.attributedText=attrStr;
                            }
                            @catch (NSException * e) {
                                //NSLog(@"Exception: %@", e);
                            }
                            @finally {
                                //NSLog(@"finally");
                            }
                            
                        
                        
                        
                        
                    }
                    
                }
                
                
                
                cell.ltpLabel.text =[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"lasttradedprice"]];
                
                NSString * changePercentlabel = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"open_advice"]objectForKey:@"changepercent"]];
                
                NSString * percent =@"  ";
                NSString * finalChangePercentlabel = [changePercentlabel stringByAppendingString:percent];
                if([changePercentlabel isEqual:[NSNull null]]||changePercentlabel==nil||[changePercentlabel isEqualToString:@"<null>"])
                {
                    
                    
                    
                    NSString * percentage = @"";
                    NSString * zero = @"  0";
                    NSString* final = [zero stringByAppendingString:percentage];
                    cell.changePercentlabel.text=final;
                    cell.changePercentlabel.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                }
                else
                {
                    
                    if([finalChangePercentlabel containsString:@"-"])
                    {
                        cell.changePercentlabel.text=finalChangePercentlabel;
                        cell.changePercentlabel.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                        cell.adviceButton.layer.borderColor =[[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1] CGColor];
                        [cell.adviceButton setTitleColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1] forState:UIControlStateNormal];
                        [cell.adviceButton setTitle:@"Opening Advice" forState:UIControlStateNormal];
                        //                    cell.oLbl.backgroundColor=[UIColor colorWithRed:(238/255.0) green:(43/255.0) blue:(53/255.0) alpha:1];
                    }
                    else{
                        cell.changePercentlabel.text=finalChangePercentlabel;
                        cell.changePercentlabel.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                        cell.adviceButton.layer.borderColor =[[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1] CGColor];
                        [cell.adviceButton setTitleColor:[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1] forState:UIControlStateNormal];
                        [cell.adviceButton setTitle:@"Opening Advice" forState:UIControlStateNormal];
                        //                    cell.oLbl.backgroundColor=[UIColor colorWithRed:(86/255.0) green:(168/255.0) blue:(66/255.0) alpha:1];
                    }
                    
                    
                }
                
                NSString * actedBy = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"open_advice"]objectForKey:@"actedby"]];
                
                if([actedBy isEqual:[NSNull null]]||actedBy==nil||[actedBy isEqualToString:@"<null>"])
                {
                    cell.actedByLabel.text=@"0";
                }else
                {
                    cell.actedByLabel.text=actedBy;
                }
                NSString * sharesSold = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"sharesold"]];
                if([sharesSold isEqual:[NSNull null]]||sharesSold==nil||[sharesSold isEqualToString:@"<null>"])
                {
                    cell.sharesSold.text=@"0";
                }else
                {
                    cell.sharesSold.text=sharesSold;
                }
                
                
                
                
                NSString * dateStr=[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"createdon"];
                
                
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
                [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                NSDate *date = [dateFormatter dateFromString:dateStr]; // create date from string
                
                // change to a readable time format and change to local time zone
                [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
                [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                NSString *finalDate = [dateFormatter stringFromDate:date];
                
                
                
                
                
                cell.dateAndTimeLabel.text=finalDate;
                
                
                NSString * firstName=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"firstname"]];
                NSString * lastName=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"lastname"]];
                
                if([firstName isEqualToString:@"<null>"])
                {
                    firstName=@"";
                }
                
                if([lastName isEqualToString:@"<null>"])
                {
                    lastName=@"";
                }
                
                cell.profileName.text=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                
                // cell.valueChangeLabel.attributedText=strText;
                // [strText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Ubuntu-Medium" size:12.5] range:NSMakeRange(6, )];
                
                
              
                
                
                [cell.buySellButton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
                
                [cell.leaderProfileAction addTarget:self action:@selector(leaderProfileMethod:) forControlEvents:UIControlEventTouchUpInside];
                //       dispatch_async(dispatch_get_main_queue(), ^{
                //           cell.profileImg.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"logourl"]]]];
                //
                //
                //       });
                
                
                
                
                
                @try {
                    
                    NSString * actedBy =[ [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"open_advice"]objectForKey:@"actedby"];
                    
                    
                    if([actedBy isEqual:[NSNull null]])
                    {
                        cell.actedByLabel.text=@"0";
                    }else
                    {
                        int actedByInt=[actedBy intValue];
                        cell.actedByLabel.text = [NSString stringWithFormat:@"%i",actedByInt];
                    }
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                
                @try {
                    
                    NSString * sharesSold =[ [[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"sharesold"];
                    
                    
                    
                    if([sharesSold isEqual:[NSNull null]])
                    {
                        cell.sharesSold.text = @"0";
                    }else
                    {
                        int sharesSoldInt=[sharesSold intValue];
                        cell.sharesSold.text = [NSString stringWithFormat:@"%i",sharesSoldInt];
                    }
                    
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                
                @try {
                    
                    NSString * averageProfit =[ [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"open_advice"]objectForKey:@"averageprofit"];
                    
                    
                    
                    if([averageProfit isEqual:[NSNull null]])
                    {
                        
                    }else
                    {
                        int sharesSoldInt=[averageProfit intValue];
                        
                    }
                    
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                
                
                
                NSString * duration=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"durationtype"]];
                
                int durationInt=[duration intValue];
                
                if(durationInt==1)
                {
                    
                }
                
                else if(durationInt==2)
                {
                    
                }
                else if(durationInt==3)
                {
                    
                    
                    
                    
                    
                }
                
              
                
                NSString * public=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"open_advice"]objectForKey:@"ispublic"]];
                int publicInt=[public intValue];
                
                if(publicInt==1)
                {
                    cell.premiumImg.hidden=NO;
                    
                    cell.premiumImg.image=[UIImage imageNamed:@"publicnew"];
                    
                    
                    
                    NSString * source = [NSString stringWithFormat:@"%@",[[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"]objectForKey:@"otherinfo"]objectForKey:@"source"]];
                    cell.sourceLabel.hidden=NO;
                    cell.sourceDataLabel.hidden=NO;
                    cell.sourceDataLabel.text = source;
                    cell.analystName.hidden=NO;
                    cell.analystNameHeightConstant.constant=21;
                    NSString * analystName = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"]objectForKey:@"leaderorgname"]];
                    NSString * dummyAnalystName= [analystName stringByReplacingOccurrencesOfString:@" " withString:@""];
                    dummyAnalystName=[dummyAnalystName lowercaseString];
                    cell.profileName.text=analystName;
                    NSString * firstName = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"]objectForKey:@"firstname"]];
                    NSString * lastname = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"]objectForKey:@"lastname"]];
                    
                    NSString * dummyName;
                    if([lastname isEqual:[NSNull null]]||[lastname containsString:@"<null>"])
                    {
                        lastname=@"";
                        NSString * name = [NSString stringWithFormat:@"%@ %@",firstName,lastname];
                        dummyName = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
                        dummyName=[dummyName lowercaseString];
                        NSString * profileName=[cell.profileName.text stringByReplacingOccurrencesOfString:@" " withString:@""];
                        profileName=[profileName lowercaseString];
                        if(![dummyName isEqualToString:profileName])
                        {
                            cell.organizationNameLbl.text=[NSString stringWithFormat:@"%@",cell.profileName.text];
                            cell.profileName.text= [NSString stringWithFormat:@"%@ %@",firstName,lastname];
                            cell.expertXconstraint.constant=10;
                            
                        }
                        
                        else
                        {
                            cell.expertXconstraint.constant=20;
                            cell.organizationNameLbl.text=@"";
                        }
                        
                        
                    }else
                    {
                        NSString * name = [NSString stringWithFormat:@"%@ %@",firstName,lastname];
                        dummyName = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
                        dummyName=[dummyName lowercaseString];
                        NSString * profileName=[cell.profileName.text stringByReplacingOccurrencesOfString:@" " withString:@""];
                        profileName=[profileName lowercaseString];
                        if(![dummyName isEqualToString:profileName])
                        {
                             cell.organizationNameLbl.text=[NSString stringWithFormat:@"%@",cell.profileName.text];
                            cell.profileName.text= [NSString stringWithFormat:@"%@ %@",firstName,lastname];
                            cell.expertXconstraint.constant=10;
                           
                        }
                        
                        else
                        {
                            cell.expertXconstraint.constant=20;
                             cell.organizationNameLbl.text=@"";
                        }
                        
                        
                    }
                    if([dummyAnalystName containsString:dummyName])
                    {
                        cell.analystName.hidden=YES;
                        cell.analystNameHeightConstant.constant=0;
                    }else
                    {
                        cell.analystName.hidden=NO;
                        cell.analystNameHeightConstant.constant=21;
                        //                    cell.profileName.text = analystName;
                    }
                    
                    
                    
                    
                }
                
                else if(publicInt==0)
                {
                    cell.premiumImg.hidden=YES;
                    cell.expertXconstraint.constant=20;
                     cell.organizationNameLbl.text=@"";
                    cell.sourceLabel.hidden=YES;
                    cell.sourceDataLabel.hidden=YES;
                    NSString * firstName=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"firstname"]];
                    NSString * lastName=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"lastname"]];
                    
                    if([firstName isEqualToString:@"<null>"])
                    {
                        firstName=@"";
                    }
                    
                    if([lastName isEqualToString:@"<null>"])
                    {
                        lastName=@"";
                    }
                    
                    cell.profileName.text=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                    cell.analystName.hidden=YES;
                    cell.analystNameHeightConstant.constant=0;
                    
                    NSString * sub=[NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"open_advice"] objectForKey:@"subscriptiontypeid"]];
                    
                    int subInt=[sub intValue];
                    
                    if(subInt==1)
                    {
                        cell.premiumImg.hidden=YES;
                        
                    }
                    
                    else if(subInt==2)
                    {
                        cell.premiumImg.hidden=NO;
                        
                        cell.premiumImg.image=[UIImage imageNamed:@"premiumNew"];
                        
                    }
                    
                }
                
                
                cell.leaderImg.image=nil;
                
                if(publicInt==1)
                {
                    NSURL *url;
                    @try {
                        url =  [NSURL URLWithString:[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"logourl"]];
                    } @catch (NSException *exception) {
                        
                        
                        
                    } @finally {
                        
                    }
                     [cell.leaderImg sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"dpDefault"]];
                    
//                    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//                        if (data) {
//                            UIImage *image = [UIImage imageWithData:data];
//                            if (image) {
//                                dispatch_async(dispatch_get_main_queue(), ^{
//
//                                    EquitiesCell1 *cell = [self.equitiesTableView cellForRowAtIndexPath:indexPath];
//                                    if (cell)
//                                    {
//                                        cell.leaderImg.image = image;
//                                    }
//                                });
//                            }
//                        }
//                    }];
//                    [task resume];
                }else
                {
                    
                    NSURL *url;
                    @try {
                        url =  [NSURL URLWithString:[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"] objectForKey:@"logourl"]];
                    } @catch (NSException *exception) {
                        
                        
                        
                    } @finally {
                        
                    }
                    
                     [cell.leaderImg sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"dpDefault"]];
//                    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//                        if (data) {
//                            UIImage *image = [UIImage imageWithData:data];
//                            if (image) {
//                                dispatch_async(dispatch_get_main_queue(), ^{
//
//                                    EquitiesCell1 *cell = [self.equitiesTableView cellForRowAtIndexPath:indexPath];
//                                    if (cell)
//                                    {
//                                        cell.leaderImg.image = image;
//                                    }
//                                });
//                            }
//                        }
//                    }];
//                    [task resume];
                }
                
                if(filterAdvice.count>4)
                {
                NSDictionary *data = [filterAdvice objectAtIndex:indexPath.row];
                BOOL lastItemReached = [data isEqual:[filterAdvice lastObject]];
                if (!lastItemReached && indexPath.row == [filterAdvice count] - 3)
                {
                    if(rehitBool==true)
                    {
                        [self refreshTableVeiwList];
                    }
                }
                }
                return cell;
            }
            
            
            else{
                
                EquitiesCell2 * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
                cell.oLabel.layer.cornerRadius = 12.5f;
                cell.oLabel.layer.masksToBounds = YES;
                cell.ltLabel.layer.cornerRadius = 12.5f;
                cell.ltLabel.layer.masksToBounds=YES;
                
                //
                
                cell.profileImage.layer.cornerRadius=cell.profileImage.frame.size.width / 2;
                cell.profileImage.clipsToBounds = YES;
                
                
                NSString * firstName = [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"firstname"];
                
                NSString * messageName = [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"messagename"];
                
                cell.adivceName.text=messageName;
                
                
                
                
                cell.profileName.text=firstName;
                
                
                NSString * dateStr=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"createdon"];
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
                [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                NSDate *date = [dateFormatter dateFromString:dateStr]; // create date from string
                
                // change to a readable time format and change to local time zone
                [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
                [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                NSString *finalDate = [dateFormatter stringFromDate:date];
                
                
                
                
                
                cell.dateAndTime.text=finalDate;
                
                
                
                
                NSString * assumedInvestment = [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"amount"];
                NSString * rupee = @"₹";
                
                NSString * finalString = [rupee stringByAppendingString:assumedInvestment];
                
                cell.assumedInvestment.text = finalString;
                
                NSString * actedBy = [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"actedby"];
                
                if([actedBy isEqual:[NSNull null]])
                {
                    cell.actedByLabel.text=@"0";
                }else
                {
                    cell.actedByLabel.text=actedBy;
                }
                
                NSString * sharesBought = [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"sharesold"];
                
                if([sharesBought isEqual:[NSNull null]])
                {
                    cell.sharesBroughtLabel.text=@"0";
                }else
                {
                    cell.sharesBroughtLabel.text=sharesBought;
                }
                
                
                cell.portfolioArray=[[NSMutableArray alloc]init];
                @try {
                    
                    cell.portfolioArray= [[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"portfoliojson"] objectForKey:@"stocks"];
                } @catch (NSException *exception) {
                    
                    
                    
                } @finally {
                    
                }
                
                
                
                //NSLog(@"cell portfolio array:%@",cell.portfolioArray);
                [cell.portfolioTblView reloadData];
                
                //       dispatch_async(dispatch_get_main_queue(), ^{
                //           cell.profileImage.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"logourl"]]]];
                //
                //
                //       });
                
                cell.profileImage.image=nil;
                
                NSURL *url;
                @try {
                    url =  [NSURL URLWithString:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"logourl"]];
                } @catch (NSException *exception) {
                    
                    
                    
                } @finally {
                    
                }
                
                
                NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                    if (data) {
                        UIImage *image = [UIImage imageWithData:data];
                        if (image) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                EquitiesCell2 *updateCell = (id)[tableView cellForRowAtIndexPath:indexPath];
                                if (updateCell)
                                    updateCell.profileImage.image = image;
                            });
                        }
                    }
                }];
                [task resume];
                
                [cell.buySellButton addTarget:self action:@selector(portBuySellAction:) forControlEvents:UIControlEventTouchUpInside];
                if(filterAdvice.count>4)
                {
                NSDictionary *data1 = [filterAdvice objectAtIndex:indexPath.row];
                BOOL lastItemReached1 = [data1 isEqual:[filterAdvice lastObject]];
                if (!lastItemReached1 && indexPath.row == [filterAdvice count] - 3)
                {
                    if(rehitBool==true)
                    {
                        [self refreshTableVeiwList];
                    }
                }
                }
                return cell;
            }
            
        }
      
        
    }
    
    else
    {
        DerivativesCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
        
        return cell;
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    return nil;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(self.controller.isActive==YES&&self.controller.searchBar.text.length>=1)
    {
        
    }
    
    else
    {
        
        [self arrayMethod];
    }
      if(filterAdvice.count>0)
      {
          @try
        {
        
      
            
            NSDictionary * contra=[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"];
            NSDictionary * openAdvice=[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"];
            NSString * contraStr=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"]];
            NSDictionary * localDict=[[NSDictionary alloc]init];
            if(contra && openAdvice && ![contraStr isEqualToString:@"<null>"])
            {
                localDict=[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"];
            }
            else if(openAdvice)
            {
                 localDict=[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"];
            }
            if(localDict)
            {
                  delegate1.navigationCheck=@"wisdom";
            int messageType=[[localDict objectForKey:@"messagetypeid"] intValue];
            if(messageType!=4)
       
             {
            if(delegate1.leaderAdviceDetails.count>0)
            {
                [delegate1.leaderAdviceDetails removeAllObjects];
            }
            //    if(tableView==self.equitiesTableView&&segmentedControl.selectedSegmentIndex==0)
            //    {
            //
            //        if(indexPath.row==2||indexPath.row==7||indexPath.row==15||indexPath.row==12||indexPath.row==18)
            //        {
            //            StockAllocationView * allocation=[self.storyboard instantiateViewControllerWithIdentifier:@"stockallocation"];
            //
            //            [self.navigationController pushViewController:allocation animated:YES];
            //
            //        }
            
            //        else{
            
            NSString * public=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"ispublic"]];
            int publicInt=[public intValue];
            
            if(publicInt==0)
            {
            delegate1.depth=@"WISDOM";
            
            //NSLog(@"%@",delegate1.depth);
            
            StockView1 * stockDetails=[self.storyboard instantiateViewControllerWithIdentifier:@"stock1"];
            
            
            
            delegate1.symbolDepthStr=[localDict objectForKey:@"companyname"];
            
            delegate1.exchaneLblStr=[localDict objectForKey:@"segmenttype"];
            
            delegate1.instrumentDepthStr=[localDict objectForKey:@"instrumentid"];
            
//            delegate1.expirySeriesValue=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"expirydate"];
             delegate1.expirySeriesValue=@"";
                NSString * firstName=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"firstname"]];
                NSString * lastName=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"lastname"]];
                
                if([firstName isEqualToString:@"<null>"])
                {
                    firstName=@"";
                }
                
                if([lastName isEqualToString:@"<null>"])
                {
                    lastName=@"";
                }
                NSString * name=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
               
            
            [delegate1.leaderAdviceDetails addObject:name];
            [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"createdon"]];
            [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"lasttradedprice"]];
            
            [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"changepercent"]];
            
            [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"message"]];
            
            delegate1.instrumentDepthStr=[localDict objectForKey:@"instrumentid"];
            
            
            [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"leaderid"]];
            [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"messageid"]];
            [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"buysell"]];
            NSString * logo=[localDict objectForKey:@"logourl"];
            
            if([logo isEqual:[NSNull null]])
            {
                [delegate1.leaderAdviceDetails addObject:@"no"];
            }
            
            else
            {
                [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"logourl"]];
            }

            
            
            
            //NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
            
            [self.navigationController pushViewController:stockDetails animated:YES];
            }else if (publicInt ==1)
            {
                        dispatch_async(dispatch_get_main_queue(), ^{
                
                            
                
                
                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"This is a publicly available information from the above source. All users are advised, in their own interests, to seek and obtain advice that is specific or tailored to their own circumstances and that any reliance by any user on any such publicly available advice is entirely at the risk and consequence of such user, for which Zenwise Technologies Private Limited under the brand name Zambala is not responsible or liable." preferredStyle:UIAlertControllerStyleAlert];
                
                
                
                            [self presentViewController:alert animated:YES completion:^{
                
                
                
                            }];
                
                
                
                            UIAlertAction * proceedAction=[UIAlertAction actionWithTitle:@"Proceed" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                                delegate1.depth=@"WISDOM";
                                
                                //NSLog(@"%@",delegate1.depth);
                                
                                StockView1 * stockDetails=[self.storyboard instantiateViewControllerWithIdentifier:@"stock1"];
                                
                                
                                
                                delegate1.symbolDepthStr=[localDict objectForKey:@"companyname"];
                                
                                delegate1.exchaneLblStr=[localDict objectForKey:@"segmenttype"];
                                
                                delegate1.instrumentDepthStr=[localDict objectForKey:@"instrumentid"];
                                
                                //            delegate1.expirySeriesValue=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"expirydate"];
                                delegate1.expirySeriesValue=@"";
                                NSString * firstName=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"firstname"]];
                                NSString * lastName=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"lastname"]];
                                
                                if([firstName isEqualToString:@"<null>"])
                                {
                                    firstName=@"";
                                }
                                
                                if([lastName isEqualToString:@"<null>"])
                                {
                                    lastName=@"";
                                }
                                NSString * name=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                                
                                [delegate1.leaderAdviceDetails addObject:name];
                                [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"createdon"]];
                                [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"lasttradedprice"]];
                                
                                [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"changepercent"]];
                                
                                [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"message"]];
                                
                                delegate1.instrumentDepthStr=[localDict objectForKey:@"instrumentid"];
                                
                                
                                [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"leaderid"]];
                                [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"messageid"]];
                                [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"buysell"]];
                                NSString * logo=[localDict objectForKey:@"logourl"];
                                
                                if([logo isEqual:[NSNull null]])
                                {
                                    [delegate1.leaderAdviceDetails addObject:@"no"];
                                }
                                
                                else
                                {
                                    [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"logourl"]];
                                }
                                
                                
                                
                                
                                //NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
                                
                                [self.navigationController pushViewController:stockDetails animated:YES];
                
                            }];
                            
                            UIAlertAction * cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                
                                
                                
                            }];
                            
                
                
                
                            [alert addAction:proceedAction];
                            [alert addAction:cancelAction];
                
                        });
                
            }
            
        }
        
        else
        {
            
            delegate1.protFolioUserID = [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"messageid"];
            
            StockAllocationView * protfolioDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"stockallocation"];
            [self.navigationController pushViewController:protfolioDetail animated:YES];
//
            
            
            
            
            
            //        dispatch_async(dispatch_get_main_queue(), ^{
            //
            //
            //
            //
            //            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Under Development." preferredStyle:UIAlertControllerStyleAlert];
            //
            //
            //
            //            [self presentViewController:alert animated:YES completion:^{
            //
            //
            //
            //            }];
            //
            //
            //            
            //            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            //                
            //                
            //                
            //            }];
            //            
            //            
            //            
            //            [alert addAction:okAction];
            //            
            //        });
            //        
            
            
            
            
        }
        
        //            WisdomViewController * stockView=[self.storyboard instantiateViewControllerWithIdentifier:@"rohit"];
        //            
        //            [self.navigationController pushViewController:stockView animated:YES];
        //        }
        //    }
            
        
        
    }
        }@catch (NSException *exception) {
        
    } @finally {
        
    }
      }

    

    //////
    
   

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    @try
    {
    if(self.controller.isActive==YES&&self.controller.searchBar.text.length>=1)
    {
        
    }
    
    else
    {
        [self arrayMethod];
    }
        if(tableView==self.equitiesTableView)
        {
            
            if(filterAdvice.count>0)
            {
               
               
                int messageType=[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"messagetypeid"] intValue];
                
                if(messageType!=4)
                
                {
                    NSDictionary * contra=[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"];
                    NSDictionary * openAdvice=[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"];
                    NSString * contraStr=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"]];
                
                    if(contra && openAdvice && ![contraStr isEqualToString:@"<null>"])
                   
                    {
                    return 350;
                    }
                    
                    else if(openAdvice)
                    {
                         return 190;
                    }
                    else
                    {
                         return 190;
                    }
                }
                
                else
                {
                    
                    if(  [[[[filterAdvice objectAtIndex:indexPath.row ] objectForKey:@"portfoliojson"] objectForKey:@"stocks"] count]==1)
                        
                    {
                        
                        
                        return 255;
                        
                    }
                    
                    
                    
                    else if(   [[[[filterAdvice objectAtIndex:indexPath.row ] objectForKey:@"portfoliojson"] objectForKey:@"stocks"] count]==2)
                        
                    {
                        
                        
                        
                        return 285;
                        
                        //return 304;
                        
                    }
                    
                    
                    
                    else if(  [[[filterAdvice objectAtIndex:indexPath.row ] objectForKey:@"portfoliojson"] count]==3)
                        
                    {
                        
                        return 315;
                        
                    }
                    
                
                    
                    else if(   [[[[filterAdvice objectAtIndex:indexPath.row ] objectForKey:@"portfoliojson"] objectForKey:@"stocks"] count]==4)
                        
                    {
                        
                        return 335;
                        
                    }
                    
                    
                    
                    else if(   [[[[filterAdvice objectAtIndex:indexPath.row ] objectForKey:@"portfoliojson"] objectForKey:@"stocks"] count]==4)
                        
                    {
                        
                        return 365;
                        
                    }
                    
                    
                    
                    
                    
                    return 255;
                    
                    
                    
                    
                }
            }
                
            
            
            else
            {
                return 393;
            }
            
            
        }
        
    

    
       return 0;
    
} @catch (NSException *exception) {
    
} @finally {
    
}


}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (UISearchController *)controller {
//    
//    if (!_controller) {
//        
//        // instantiate search results table view
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        Equities *equities= [storyboard instantiateViewControllerWithIdentifier:@"Equities"];
//        
//        // create search controller
//        _controller = [[UISearchController alloc]initWithSearchResultsController:equities];
//        _controller.searchResultsUpdater = self;
//        
//        // optional: set the search controller delegate
//        _controller.delegate = self;
//        
//       
//        
//    }
//    return _controller;
//}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self.controller setActive:NO];
}


- (IBAction)searchAction:(id)sender {
    
    // present the search controller
    
//    self.controller.view.backgroundColor = [UIColor whiteColor];
//    
//    [self presentViewController:self.controller animated:YES completion:nil];
    
//    UISearchController *searchController = [[UISearchController alloc] initWithSearchResultsController:self];
//    // Use the current view controller to update the search results.
//    searchController.searchResultsUpdater = self;
//    // Install the search bar as the table header.
//    self.navigationItem.titleView = searchController.searchBar;
//    // It is usually good to set the presentation context.
//    self.definesPresentationContext = YES;
//    
    self.controller = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.controller.searchResultsUpdater = self;
    self.controller.hidesNavigationBarDuringPresentation = NO;
    self.controller.searchBar.placeholder = @"Search Leader";
    self.controller.dimsBackgroundDuringPresentation = false;

    self.definesPresentationContext = YES;
//    self.controller.searchBar.scopeButtonTitles = @[@"Posts", @"Users", @"Subreddits"];
    [self presentViewController:self.controller animated:YES completion:nil];
    
}

/*!
 @discussion used to update search results.
 */

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    
    
        @try {
            if(filterAdvice.count>0)
            {
                filterAdvice=[[NSMutableArray alloc]init];
                
            }
            if(adviceList.count>0)
            {
                adviceList=[[NSMutableArray alloc]init];
            }
            
            if([[self.responseArray objectForKey:@"data"] count]>0)
            {
           
            for(int i=0;i<[[self.responseArray objectForKey:@"data"] count];i++)
            {
                
                [adviceList addObject:[[[[self.responseArray objectForKey:@"data"] objectAtIndex:i]objectForKey:@"open_advice"] objectForKey:@"firstname"]];
            }
                
            
            NSPredicate * predicate=[NSPredicate predicateWithFormat:@"self beginswith[c] %@",searchController.searchBar.text];
            //NSLog(@"%@",predicate);
            if(self.controller.searchBar.text.length>0)
            {
                filterArray =[[NSArray alloc]init];
                
                
                filterArray=[adviceList  filteredArrayUsingPredicate:predicate];
                
                for(int i=0;i<filterArray.count;i++)
                {
                     detailsArray=[[NSMutableArray alloc]init];
                   
                    if(filterAdvice.count>0&&filterArray.count>0)
                    {
                        
                        filterString1=[filterArray objectAtIndex:i];
                        for(int k=0;k<filterAdvice.count;k++)
                        {
                            [detailsArray addObject:[[[filterAdvice objectAtIndex:k]objectForKey:@"open_advice"]objectForKey:@"firstname"]];
                            
                        }
                        
                    }

                    for(int j=0;j<[[self.responseArray objectForKey:@"data"]count];j++)
                    {
                        NSString * filterString=[filterArray objectAtIndex:i];
                        NSString * responseString=[[[[self.responseArray objectForKey:@"data"]  objectAtIndex:j]objectForKey:@"open_advice"]objectForKey:@"firstname"];
                        
                        
                       
                    
                      
                        if(detailsArray.count>0)
                        {
                        
                        if([detailsArray containsObject:filterString1])
                        {
                        
                          
                        }
                            
                            
                        else
                        {
                            if([filterString isEqualToString:responseString])
                            {
                                [filterAdvice addObject:[[self.responseArray objectForKey:@"data"] objectAtIndex:j]];
                            }
                        }

                            
                        }
                        
                        else
                        {
                            if([filterString isEqualToString:responseString])
                            {
                                [filterAdvice addObject:[[self.responseArray objectForKey:@"data"] objectAtIndex:j]];
                                
                            }
                        }
                        
                        
                        
                    }
                }
            }
            
                if(self.controller.searchBar.text.length>=1)
                {
                
                    [self.equitiesTableView reloadData];
                }
                
                if(self.controller.isActive==NO)
                {
                    [self.equitiesTableView reloadData];
                }
                
            
           
            
                
           
                
            }
            
            
            
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
        
        
       
    
    
}

/*!
 @discussion used to show information.
 */
- (IBAction)popUpBtn:(id)sender
{
    
    PopUpView *detailViewController = [[PopUpView alloc] initWithNibName:@"PopUpView" bundle:nil];
    [self presentPopupViewController:detailViewController animationType: MJPopupViewAnimationFade];
    
    
}
/*!
 @discussion used fro filter.
 */

-(IBAction)filterAction:(id)sender
{


}

/*!
 @discussion method will call when there is change in segment selection.
 */

-(void)segmentedControlChangedValue
{
    wisdomSymbol=@"";

    refreshButton.hidden=YES;
    
        finalResponseArray=[[NSMutableArray alloc]init];
        self.equitiesTableView.hidden=YES;
        self.activityIndicator.hidden=NO;
        [segmentedControl setEnabled:NO];
        [self.activityIndicator startAnimating];
        offset=[NSNumber numberWithInt:0];
        [self serverHit];
    
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    if([delegate1.equities_Derivatives isEqualToString:@"EQUITY"])
    {
        if(segmentedControl.selectedSegmentIndex==0)
        {
            [mixpanelMini track:@"equity_long_term_page"];
        }else if (segmentedControl.selectedSegmentIndex==1)
        {
            [mixpanelMini track:@"equity_short_term_page"];
        }else if (segmentedControl.selectedSegmentIndex==2)
        {
            [mixpanelMini track:@"equity_day_trade_page"];
        }
    }else if([delegate1.equities_Derivatives isEqualToString:@"DERIVATIVES"])
    {
        if(segmentedControl.selectedSegmentIndex==0)
        {
            [mixpanelMini track:@"derivatives_future_options_page"];
        }else if (segmentedControl.selectedSegmentIndex==1)
        {
            [mixpanelMini track:@"derivatives_currency_page"];
        }else if (segmentedControl.selectedSegmentIndex==2)
        {
            [mixpanelMini track:@"derivatives_commodities_page"];
        }
    }
    

}







/*!
 @discussion method called when we click on buy or sell.
 */

-(void)yourButtonClicked:(UIButton*)sender
{
    //    if (sender.tag == 0)
    //    {
    //
    //    }
    
    @try
    {
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.equitiesTableView];
    
    
    
    NSIndexPath *indexPath = [self.equitiesTableView indexPathForRowAtPoint:buttonPosition];
    //NSLog(@"Index path:%ld",(long)indexPath.row);
    EquitiesCell1 *cell = [self.equitiesTableView cellForRowAtIndexPath:indexPath];
    
    
    //////
    
    if(self.controller.isActive==YES&&self.controller.searchBar.text.length>=1)
    {
        
    }
    
    else
    {
        
        [self arrayMethod];
    }
    NSDictionary * contra=[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"];
    NSDictionary * openAdvice=[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"];
    NSString * contraStr=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"]];
    NSDictionary * localDict=[[NSDictionary alloc]init];
    if(contra && openAdvice && ![contraStr isEqualToString:@"<null>"])
    {
        localDict=[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"contra_advice"];
    }
    else if(openAdvice)
    {
        localDict=[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"open_advice"];
    }
    if(localDict)
    {
    delegate1.navigationCheck=@"wisdom";
    
     delegate1.wisdomCheck=true;
    
    
  
    
    
    
    
    delegate1.wisdomGardemTickIDString = [localDict objectForKey:@"messageid"];
    
        int messageType=[[localDict objectForKey:@"messagetypeid"]intValue];
        
        if(messageType!=4)
        
   
    {

        
        NSString * public=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"ispublic"]];
        int publicInt=[public intValue];
        
        if(publicInt==0)
        {
    
     @try {
          delegate1.symbolDepthStr=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"companyname"]];
         
         delegate1.tradingSecurityDes=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"symbolname"]];
         
         
     }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    @try {
        delegate1.orderSegment=[localDict objectForKey:@"segmenttype"];
        
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    
    delegate1.orderBuySell=cell.buySellButton.currentTitle;
        
        if([delegate1.brokerNameStr isEqualToString:@"Zerodha"])
        {
             delegate1.orderinstrument=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"instrumentid"]];
        }
        else
        {
            
             delegate1.orderinstrument=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"exchangetoken"]];
            
        }
   
        
    

    
        if(delegate1.leaderAdviceDetails.count>0)
        {
            [delegate1.leaderAdviceDetails removeAllObjects];
        }
        //    if(tableView==self.equitiesTableView&&segmentedControl.selectedSegmentIndex==0)
        //    {
        //
        //        if(indexPath.row==2||indexPath.row==7||indexPath.row==15||indexPath.row==12||indexPath.row==18)
        //        {
        //            StockAllocationView * allocation=[self.storyboard instantiateViewControllerWithIdentifier:@"stockallocation"];
        //
        //            [self.navigationController pushViewController:allocation animated:YES];
        //
        //        }
        
        //        else{
        delegate1.depth=@"WISDOM";
        
        //NSLog(@"%@",delegate1.depth);
        
       
        
//        delegate1.symbolDepthStr=[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"companyname"];
//        
//        delegate1.exchaneLblStr=[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"segmenttype"];
        
            NSString * firstName=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"firstname"]];
            NSString * lastName=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"lastname"]];
            
            if([firstName isEqualToString:@"<null>"])
            {
                firstName=@"";
            }
            
            if([lastName isEqualToString:@"<null>"])
            {
                lastName=@"";
            }
            NSString * name=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
        
        [delegate1.leaderAdviceDetails addObject:name];
        [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"createdon"]];
        [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"lasttradedprice"]];
        
        [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"changepercent"]];
        
        [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"message"]];
        
        NSString * logo=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"logourl"]];
        
        if([logo isEqual:[NSNull null]])
        {
            [delegate1.leaderAdviceDetails addObject:@"no"];
        }
        
        else
        {
            [delegate1.leaderAdviceDetails addObject:logo];
        }
        
        //NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
        StockOrderView * orderView=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
        
        [self.navigationController pushViewController:orderView animated:YES];
        
        
    }else if (publicInt==1)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"This is a publicly available information from the above source. All users are advised, in their own interests, to seek and obtain advice that is specific or tailored to their own circumstances and that any reliance by any user on any such publicly available advice is entirely at the risk and consequence of such user, for which Zenwise Technologies Private Limited under the brand name Zambala is not responsible or liable." preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            [self presentViewController:alert animated:YES completion:^{
                
                
                
            }];
            
            
            
            UIAlertAction * proceedAction=[UIAlertAction actionWithTitle:@"Proceed" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                @try {
                    delegate1.symbolDepthStr=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"companyname"]];
                    
                    delegate1.tradingSecurityDes=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"symbolname"]];
                    
                    
                }
                @catch (NSException * e) {
                    //NSLog(@"Exception: %@", e);
                }
                @finally {
                    //NSLog(@"finally");
                }
                
                @try {
                    delegate1.orderSegment=[localDict objectForKey:@"segmenttype"];
                    
                    
                }
                @catch (NSException * e) {
                    //NSLog(@"Exception: %@", e);
                }
                @finally {
                    //NSLog(@"finally");
                }
                
                
                delegate1.orderBuySell=cell.buySellButton.currentTitle;
                
                if([delegate1.brokerNameStr isEqualToString:@"Zerodha"])
                {
                    delegate1.orderinstrument=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"instrumentid"]];
                }
                else
                {
                    
                    delegate1.orderinstrument=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"exchangetoken"]];
                    
                }
                
                
                
                
                
                if(delegate1.leaderAdviceDetails.count>0)
                {
                    [delegate1.leaderAdviceDetails removeAllObjects];
                }
                //    if(tableView==self.equitiesTableView&&segmentedControl.selectedSegmentIndex==0)
                //    {
                //
                //        if(indexPath.row==2||indexPath.row==7||indexPath.row==15||indexPath.row==12||indexPath.row==18)
                //        {
                //            StockAllocationView * allocation=[self.storyboard instantiateViewControllerWithIdentifier:@"stockallocation"];
                //
                //            [self.navigationController pushViewController:allocation animated:YES];
                //
                //        }
                
                //        else{
                delegate1.depth=@"WISDOM";
                
                //NSLog(@"%@",delegate1.depth);
                
                
                
                //        delegate1.symbolDepthStr=[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"companyname"];
                //
                //        delegate1.exchaneLblStr=[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"segmenttype"];
                
                
                NSString * firstName=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"firstname"]];
                NSString * lastName=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"lastname"]];
                
                if([firstName isEqualToString:@"<null>"])
                {
                    firstName=@"";
                }
                
                if([lastName isEqualToString:@"<null>"])
                {
                    lastName=@"";
                }
                NSString * name=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                [delegate1.leaderAdviceDetails addObject:name];
                [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"createdon"]];
                [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"lasttradedprice"]];
                
                [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"changepercent"]];
                
                [delegate1.leaderAdviceDetails addObject:[localDict objectForKey:@"message"]];
                
                NSString * logo=[NSString stringWithFormat:@"%@",[localDict objectForKey:@"logourl"]];
                
                if([logo isEqual:[NSNull null]])
                {
                    [delegate1.leaderAdviceDetails addObject:@"no"];
                }
                
                else
                {
                    [delegate1.leaderAdviceDetails addObject:logo];
                }
                
                //NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
                StockOrderView * orderView=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
                
                [self.navigationController pushViewController:orderView animated:YES];
                
            }];
            
            UIAlertAction * cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                
                
            }];
            
            
            
            
            [alert addAction:proceedAction];
            [alert addAction:cancelAction];
            
        });
        
    }
    

   

   
        
        
    
    }
    
}
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

}

/*!
 @discussion naviagtes to leader details page according to selection.
 */
-(void)leaderProfileMethod:(UIButton *)sender
{
    
   
    @try
    {
    
        
    if(self.controller.isActive==YES&&self.controller.searchBar.text.length>=1&&self.controller.searchBar.text.length>=1)
    {
    }
    
    else
    {
        [self arrayMethod];
        
    }
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.equitiesTableView];
    
    
    
    NSIndexPath *indexPath = [self.equitiesTableView indexPathForRowAtPoint:buttonPosition];
    
    //NSLog(@"Index path:%ld",(long)indexPath.row);
    EquitiesCell1 *cell = [self.equitiesTableView cellForRowAtIndexPath:indexPath];
    
    NSString * public=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"ispublic"]];
    int publicInt=[public intValue];
    
        int messageType=[[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"messagetypeid"] intValue];
        
        if(messageType!=4)
    
    
    
    {
        
        delegate1.leaderIDWhoToFollow=[[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"open_advice"]objectForKey:@"leaderid"];
        
        
        
        
    }
    
   
        NewProfile * new=[self.storyboard instantiateViewControllerWithIdentifier:@"NewProfile"];
        
        [self.navigationController pushViewController:new animated:YES];
    
    
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

    

    
}

//rechability//

-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    
    
}

-(BOOL)networkStatus
{
    
    
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    
    if(netStatus==NotReachable)
    {
        
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    return YES;
    
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    delegate1.selectLeadersID=[[NSMutableArray alloc]init];
    delegate1.selectLeadersName=[[NSMutableArray alloc]init];
     delegate1.wisdomBool=false;
    delegate1.wisdomDuration=@"";
    delegate1.wisdomSearchSymbol=@"";
    delegate1.wisdomFromStr=@"";
    delegate1.wisdomaToStr=@"";
    delegate1.wisdomTopAdvices =false;
    if(self.controller.isActive==YES)
    {
        [self dismissViewControllerAnimated:self.controller completion:nil];

    }
    [self.client disconnectWithCompletionHandler:nil];
    
}

//port//

/*!
 @discussion method called when we click on transaction button for portfolio advice.
 */

-(void)portBuySellAction:(UIButton *)sender
{
    @try
    {
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.equitiesTableView];
    
    
    
    NSIndexPath *indexPath = [self.equitiesTableView indexPathForRowAtPoint:buttonPosition];
    
    //NSLog(@"Index path:%ld",(long)indexPath.row);
    EquitiesCell2 *cell = [self.equitiesTableView cellForRowAtIndexPath:indexPath];
    
    if(self.controller.isActive==YES&&self.controller.searchBar.text.length>=1)
    {
        int messageType=[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"messagetypeid"] intValue];
        
        if(messageType!=4)
        
      
        {
            
        }
        
        else
        {
            
             delegate1.protFolioUserID = [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"messageid"];
            delegate1.transaction=@"BUY";
            
            NewPortOrderView * protfolioDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"NewPortOrderView"];
            [self.navigationController pushViewController:protfolioDetail animated:YES];
            

            
        }

        
        
    }
    
    else
    {
        int messageType=[[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"messagetypeid"] intValue];
        
        if(messageType!=4)
            
      
        {
            
        }
        
        else
        {
            
            delegate1.protFolioUserID = [[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"messageid"];
            delegate1.transaction=@"BUY";
            
            NewPortOrderView * protfolioDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"NewPortOrderView"];
            [self.navigationController pushViewController:protfolioDetail animated:YES];
            

        }

        
    }
    
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

    
}

//pagination//
-(void)refreshTableVeiwList
{
    int limitInt=[offset intValue];
    
    int finalInt=limitInt+10;
    
    offset=[NSNumber numberWithInt:finalInt];
    
    [self serverHit];
    [refreshControl1 endRefreshing];
}
-(void)scrollViewDidScroll: (UIScrollView*)scrollView
{



//    if (scrollView.contentOffset.y + scrollView.frame.size.height == scrollView.contentSize.height)
//    {
//        [self refreshTableVeiwList];
//    }
//
//    else if (self.equitiesTableView.contentOffset.y==0)
//    {
//        [self refreshTableVeiwList1];
//
//    }
    ScrollDirection scrollDirection;
   
    if (self.lastContentOffset > scrollView.contentOffset.y) {
    scrollDirection = ScrollDirectionUp;
        rehitBool=false;
     } else if (self.lastContentOffset < scrollView.contentOffset.y) {
    scrollDirection = ScrollDirectionDown;
          rehitBool=true;
     }

    self.lastContentOffset = scrollView.contentOffset.y;
//    //NSLog(@"%f",scrollView.contentOffset.y);
}

-(void)refreshTableVeiwList1
{
    
    offset=[NSNumber numberWithInt:0];
    
    [self serverHit];
    [refreshControl1 endRefreshing];
}
-(void)tapDetected{
    //NSLog(@"single Tap on imageview");
    [myImageView removeFromSuperview];
}

-(void)refreshSocket
{
//    int j=(int)segmentedControl.selectedSegmentIndex;
    @try
    {
        dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSString * str=[NSString stringWithFormat:@"follower.%@",delegate1.userID];
            self.client= [[MQTTClient alloc] initWithClientId:str];
            //    srujansoft.com
            [self.client connectToHost:@"52.77.57.127"
                     completionHandler:^(MQTTConnectionReturnCode code) {
                         if (code == ConnectionAccepted) {
                             self.client.keepAlive=(unsigned short)3000000;
                             // when the client is connected, subscribe to the topic to receive message.
                             
                             //                     if(wisdomRefreshCheck==true)
                             //                     {
                             //                         wisdomRefreshCheck=false;
                             for(int i=0;i<followingUserId.count;i++)
                             {
                                 
                                 
                                 NSString * userID=[NSString stringWithFormat:@"%@.null",[followingUserId objectAtIndex:i]];
                                 
                                 [self.client subscribe:userID
                                  
                                  withCompletionHandler:nil];
                                 
                             }
                             
                             //                     }
                         }
                     }];
            
            [self.client setMessageHandler:^(MQTTMessage *message) {
                NSString *text =message.payloadString;
                
                
                //        int i=[text intValue];
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSString * localStr=@"";
                    if([delegate1.equities_Derivatives isEqualToString:@"EQUITY"])
                    {
                        if(segmentedControl.selectedSegmentIndex==0)
                        {
                            localStr=@"EQ.LT";
                        }
                        else if(segmentedControl.selectedSegmentIndex==1)
                        {
                            localStr=@"EQ.ST";
                        }
                        else if(segmentedControl.selectedSegmentIndex==2)
                        {
                            localStr=@"EQ.DT";
                        }
                    }
                    else if([delegate1.equities_Derivatives isEqualToString:@"DERIVATIVES"])
                    {
                        if(segmentedControl.selectedSegmentIndex==0)
                        {
                            localStr=@"DV.FO";
                        }
                        else if(segmentedControl.selectedSegmentIndex==1)
                        {
                            localStr=@"DV.CC";
                        }
                        else if(segmentedControl.selectedSegmentIndex==2)
                        {
                            localStr=@"DV.CD";
                        }
                    }
                    if([text isEqualToString:localStr])
                    {
                        AudioServicesPlaySystemSound(1007);
                        badgeValueInt=badgeValueInt+1;
                        //            badgeLbl.text=[NSString stringWithFormat:@"%i",badgeValueInt];
                        
                        [refreshButton setTitle:[NSString stringWithFormat:@"%i ↑",badgeValueInt] forState:UIControlStateNormal];
                        refreshButton.hidden=NO;
                        
                        badgeLbl.hidden=NO;
                        
                        
                        
                    }
                    
                    
                });
                
                
                
                //NSLog(@"received message %@", text);
            }];
           
        });
   
    
    [self.client disconnectWithCompletionHandler:^(NSUInteger code) {
        // The client is disconnected when this completion handler is called
        //NSLog(@"MQTT client is disconnected");
    }];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

}

-(void)arrayMethod
{
    @try
    {
    filterAdvice=[[NSMutableArray alloc]init];
    
    filterAdvice=[self.responseArray objectForKey:@"data"];
    
}
@catch (NSException * e) {
    //NSLog(@"Exception: %@", e);
}
@finally {
    //NSLog(@"finally");
}


}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
//    if ([[segue identifier] isEqualToString:@"Filter"])
//    {
        // Get reference to the destination view controller
        WGFilterViewController *vc = [segue destinationViewController];
   if([delegate1.equities_Derivatives isEqualToString:@"DERIVATIVES"])
    {
          if(segmentedControl.selectedSegmentIndex==0)
        {
            vc.title=@"Derivatives Filter";
            vc.exchangeType=@"DR";
        }
        else  if(segmentedControl.selectedSegmentIndex==1)
        {
            vc.title=@"Currency Filter";
            vc.exchangeType=@"CR";
        }
        else  if(segmentedControl.selectedSegmentIndex==2)
        {
            vc.title=@"Commodities Filter";
            vc.exchangeType=@"CO";
        }
    }
    else
    {
     
          vc.title=@"Equity Filter";
          vc.exchangeType=@"EQ";
     
        
    }
//    }
}
typedef NS_ENUM(NSInteger, ScrollDirection) {
    ScrollDirectionNone,
    ScrollDirectionRight,
    ScrollDirectionLeft,
    ScrollDirectionUp,
    ScrollDirectionDown,
    ScrollDirectionCrazy,
};

@end
