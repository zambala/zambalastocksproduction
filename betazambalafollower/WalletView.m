//
//  WalletView.m
//  NewWallet
//
//  Created by Zenwise Technologies on 27/06/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "WalletView.h"
#import "TagEncode.h"
#import <QuartzCore/QuartzCore.h>
#import "WalletHistoryCell.h"
#import "AppDelegate.h"
#import <Mixpanel/Mixpanel.h>
#import "EarnPointsCell.h"
#import "RedeemView.h"
#import "RewardsHistoryViewController.h"
#import "TabBar.h"
#import "PersonalDetailsViewController.h"
@interface WalletView ()<UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate>
{
    int hours, minutes, seconds;
    int secondsLeft;
    NSTimer * timer;
    NSString * questionId;
    NSArray * transactionalHistoryArray;
    BOOL answerBool;
    int qusType;
    AppDelegate * delegate1;
    NSString * number;
     NSString * email; NSString * name;
    NSDictionary * earnPointsDict;
    UITapGestureRecognizer *tapGestureRecognizer1;
  
    NSString * zambalaPointValue;
    NSString * rupeeValue;
    NSString * minZambalaPoints;
    NSString * emailVerify;
    NSString * oldEmail;
}

@end

@implementation WalletView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
   // self.historyDictionary = [[NSMutableArray alloc]init];
    self.ques1View.hidden=YES;
    self.recordedView.hidden=YES;
    self.resultsView.hidden=YES;
    NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
    self.qus1Fld.delegate=self;
   
     number= [loggedInUser stringForKey:@"profilemobilenumber"];
     email= [loggedInUser stringForKey:@"profileemail"];
     name= [loggedInUser stringForKey:@"profilename"];
   
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"wallet_page"];
    
    self.popUpView.hidden = YES;
    self.emailView.layer.cornerRadius = 5.0f;
    self.updateView.layer.cornerRadius = 5.0f;
    self.thankYouBGView.layer.cornerRadius = 5.0f;
    self.verifiedButton.layer.cornerRadius = 5.0f;
    self.requestModifyButton.layer.cornerRadius = 5.0f;
    self.tapToRevealLbl.layer.cornerRadius = 15;
    self.tapToRevealLbl.clipsToBounds = YES;
    self.popUpView.tag =99;
    
    self.emailView.hidden=NO;
    self.thankYouBGView.hidden=YES;
    self.updateView.hidden=YES;
    
    [self.verifiedButton addTarget:self action:@selector(onVerifiedTap) forControlEvents:UIControlEventTouchUpInside];
    [self.requestModifyButton addTarget:self action:@selector(onRequestModifyTap) forControlEvents:UIControlEventTouchUpInside];
    [self.emailModifyButton addTarget:self action:@selector(onEmailModifyTap) forControlEvents:UIControlEventTouchUpInside];
    self.updateEmailIDTF.delegate = self;
    self.ques2View.hidden=YES;
    self.yesBtn.backgroundColor=[UIColor whiteColor];
    self.noBtn.backgroundColor=[UIColor whiteColor];
    self.yesBtn.layer.cornerRadius=5.0f;
    self.noBtn.layer.cornerRadius=5.0f;
    self.referalCodeLbl.layer.borderWidth=1.0f;
    self.referalCodeLbl.layer.borderColor=[[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1.0f] CGColor];
    self.inviteView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5] CGColor];
    self.inviteView.layer.shadowOffset = CGSizeMake(2.0f,5.0f);
    self.inviteView.layer.shadowOpacity = 1.0f;
    self.inviteView.layer.shadowRadius = 5.0f;
    //self.inviteView.layer.shadowOpacity = 3.0f;
    self.inviteView.layer.masksToBounds = NO;
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    [self.walletPointsView addGestureRecognizer:tapGestureRecognizer];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = self.walletPointsView.bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:(37/255.0) green:(115/255.0) blue:(181/255.0) alpha:1.0f].CGColor, (id)[UIColor colorWithRed:(12/255.0) green:(90/255.0) blue:(168/255.0) alpha:1.0f] .CGColor, (id)[UIColor colorWithRed:(86/255.0) green:(146/255.0) blue:(206/255.0) alpha:1.0f] .CGColor];
    gradient.startPoint = CGPointMake(0.0, 0.5);
    gradient.endPoint = CGPointMake(1.0, 0.5);
    
    [self.walletPointsView.layer insertSublayer:gradient atIndex:0];
    self.walletPointsView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5] CGColor];
    self.walletPointsView.layer.shadowOffset = CGSizeMake(0,3);
    self.walletPointsView.layer.shadowOpacity = 1;
    self.walletPointsView.layer.shadowRadius = 1.0;
    self.walletPointsView.layer.shadowOpacity = 3.0f;
//    self.walletPointsView.layer.masksToBounds = NO;
//    self.walletPointsView.layer.cornerRadius=5.0f;
//    self.walletPointsView.clipsToBounds=YES;
    self.redeemLbl.layer.cornerRadius=5.0f;
    self.redeemLbl.clipsToBounds=YES;
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateCounter:) userInfo:nil repeats:YES];
    [self.historyButton addTarget:self action:@selector(onHistoryButtonTap) forControlEvents:UIControlEventTouchUpInside];
}



-(void)onVerifiedTap
{
    @try
    {
    NSDictionary *headers = @{ @"authtoken":delegate1.zenwiseToken,
                               @"deviceid":delegate1.currentDeviceId
                               };
    NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
    NSString*number= [loggedInUser stringForKey:@"profilemobilenumber"];
    NSString * url = [NSString stringWithFormat:@"%@2factor/usermailstatus?mobilenumber=%@",delegate1.baseUrl,number];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if(data!=nil)
                                                    {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==200)
                                                        {
                                                            NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            NSString * isVerified = [NSString stringWithFormat:@"%@",[json objectForKey:@"isemailverified"]];
                                                            if([isVerified isEqualToString:@"0"])
                                                            {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"We could not verify your email address. Please try again." preferredStyle:UIAlertControllerStyleAlert];
                                                                    [self presentViewController:alert animated:YES completion:^{
                                                                    }];
                                                                    UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                    }];
                                                                    [alert addAction:cancel];
                                                                });
                                                            }else if ([isVerified isEqualToString:@"1"])
                                                            {
                                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                            self.thankYouBGView.hidden=NO;
                                                            self.emailView.hidden=YES;
                                                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                                                CATransition *animation = [CATransition animation];
                                                                animation.type = kCATransitionFade;
                                                                animation.duration = 0.4;
                                                                [self.popUpView.layer addAnimation:animation forKey:nil];
                                                                self.thankYouBGView.hidden = YES;
                                                                self.popUpView.hidden=YES;
                                                                emailVerify=@"1";
                                                            });
                                                         });
                                                            }
                                                        }else
                                                        {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:@"Something went wrong. Please try again." preferredStyle:UIAlertControllerStyleAlert];
                                                                [self presentViewController:alert animated:YES completion:^{
                                                                }];
                                                                UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                }];
                                                                [alert addAction:cancel];
                                                            });
                                                        }
                                                    }
                                                    }
                                                    else
                                                    {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:@"Something went wrong. Please try again." preferredStyle:UIAlertControllerStyleAlert];
                                                            [self presentViewController:alert animated:YES completion:^{
                                                            }];
                                                            UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                            }];
                                                            [alert addAction:cancel];
                                                        });
                                                    }
                                                }];
    [dataTask resume];
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    if(touch.view.tag==99){
        TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
        [self presentViewController:tabPage animated:YES completion:nil];
    }
}

-(void)onRequestModifyTap
{
    if(self.updateEmailIDTF.text.length==0)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Email ID field should not be left blank." preferredStyle:UIAlertControllerStyleAlert];
            [self presentViewController:alert animated:YES completion:^{
            }];
            UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.updateEmailIDTF resignFirstResponder];
            }];
            [alert addAction:cancel];
        });
    }else
    {
    NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
    NSString*number= [loggedInUser stringForKey:@"profilemobilenumber"];
    NSDictionary *headers = @{ @"Content-Type": @"application/json",
                               @"authtoken":delegate1.zenwiseToken,
                               @"deviceid":delegate1.currentDeviceId};
    NSDictionary *parameters = @{ @"oldemail": oldEmail,
                                  @"newemail":self.updateEmailIDTF.text,
                                  @"mobilenumber":number
                                };
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSString * url = [NSString stringWithFormat:@"%@2factor/modifyemail",delegate1.baseUrl];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        if([httpResponse statusCode]==200)
                                                        {
                                                            NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            if(json)
                                                            {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Request received" message:@"We will update you once your email address is updated" preferredStyle:UIAlertControllerStyleAlert];
                                                                    [self presentViewController:alert animated:YES completion:^{
                                                                        
                                                                    }];
                                                                    UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                        TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
                                                                        [self presentViewController:tabPage animated:YES completion:nil];
                                                                        
                                                                    }];
                                                                    [alert addAction:cancel];
                                                                });
                                                            }
                                                        }else
                                                        {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:@"Something went wrong. Please try again." preferredStyle:UIAlertControllerStyleAlert];
                                                                [self presentViewController:alert animated:YES completion:^{
                                                                }];
                                                                UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                }];
                                                                [alert addAction:cancel];
                                                            });
                                                        }
                                                    }
                                                }];
    [dataTask resume];
    }
}

-(void)onEmailModifyTap
{
//    self.emailView.hidden=YES;
//    self.thankYouBGView.hidden=YES;
//    self.updateView.hidden=NO;
    PersonalDetailsViewController * personal = [self.storyboard instantiateViewControllerWithIdentifier:@"PersonalDetailsViewController"];
    [self presentViewController:personal animated:YES completion:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self getQuestions];
    [self getReferral];
    [self pointsList];
    self.historyTableView.delegate=self;
    self.historyTableView.dataSource=self;
}

-(void)onHistoryButtonTap
{
    if([emailVerify isEqualToString:@"1"])
    {
        RewardsHistoryViewController * rewards = [self.storyboard instantiateViewControllerWithIdentifier:@"RewardsHistoryViewController"];
        rewards.apiKey = self.apiKeyVoucher;
        [self presentViewController:rewards animated:YES completion:nil];
    }else if ([emailVerify isEqualToString:@"0"])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:@"Please verify your email to view history." preferredStyle:UIAlertControllerStyleAlert];
            [self presentViewController:alert animated:YES completion:^{
            }];
            UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            }];
            [alert addAction:cancel];
        });
    }
}
- (void)updateCounter:(NSTimer *)theTimer {
    if(secondsLeft > 0 ) {
        secondsLeft -- ;
        hours = secondsLeft / 3600;
        minutes = (secondsLeft % 3600) / 60;
        seconds = (secondsLeft %3600) % 60;
        self.timerLbl.text = [NSString stringWithFormat:@"%02d:%02d:%02d", hours, minutes, seconds];
    }
}

-(void)countdownTimer {
    @try
    {
    
    NSDate * dateGMT = [NSDate date];
    NSTimeInterval secondsFromGMT = [[NSTimeZone localTimeZone] secondsFromGMT];
    NSDate * now = [dateGMT dateByAddingTimeInterval:secondsFromGMT];
  
    NSTimeInterval interval = [self.expiryDate timeIntervalSinceDate:now];
        if( interval  < 0 ) {
            interval = [now timeIntervalSinceDate:self.expiryDate];
        }
        //NSLog(@"%f",interval);
   
    secondsLeft = hours = minutes = seconds = 0;
     secondsLeft = interval;
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}
//- (void)timerTick:(NSTimer *)timer {
//
//    NSDate *now = [NSDate date];
//    static NSDateFormatter *dateFormatter;
//    if (!dateFormatter) {
//        dateFormatter = [[NSDateFormatter alloc] init];
//        dateFormatter.dateFormat = @"h:mm:ss";  // very simple format  "8:47:22 AM"
//    }
//    self.timerLbl.text = [dateFormatter stringFromDate:self.expiryDate];
//
//    NSTimeInterval interval = [self.expiryDate timeIntervalSinceDate:now];
//    if( interval  < 0 ) {
//        interval = [now timeIntervalSinceDate:self.expiryDate];
//    }
//    //NSLog(@"%f",interval);
//}
- (void) handleTapFrom1: (UITapGestureRecognizer *)recognizer
{
//    [UIView transitionWithView:self.walletBottomView
//                      duration:10.0
//                       options:UIViewAnimationOptionTransitionFlipFromLeft
//                    animations:^{
//                       self.ques1View.hidden=NO;
//                    }
//                    completion:NULL];
    [UIView transitionWithView:self.ques1View duration:1.0 options:UIViewAnimationOptionTransitionFlipFromRight animations:^(void){
        
        [self.ques1View setHidden:NO];
        
    } completion:nil];
   
}
- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer
{
   
if(zambalaPointValue.length>0&&rupeeValue.length>0&&minZambalaPoints.length>0)
{
    RedeemView * redeemView=[self.storyboard instantiateViewControllerWithIdentifier:@"RedeemView"];
    redeemView.zambalaPointValue = zambalaPointValue;
    redeemView.rupeeString = rupeeValue;
    redeemView.minZambalaPoints = minZambalaPoints;
    [self presentViewController:redeemView animated:YES completion:nil];
}
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! Something went wrong" preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:^{
                
            }];
            
            
          
            
            UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            
            
            [alert addAction:cancel];
        });
    }
}

-(void)getQuestions
{
    @try
    {
    TagEncode * enocde=[[TagEncode alloc]init];
    
    enocde.inputRequestString=@"contestquestions";
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
  
   
    [inputArray addObject:[NSString stringWithFormat:@"%@survey/listsurvey?&userid=%@&authtoken=%@",delegate1.baseUrl,number,delegate1.zenwiseToken]];
    
    [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
        
        NSArray * keysArray=[dict allKeys];
        //NSLog(@"%@",dict);
        if(keysArray.count>0)
        {
           
           
            // change to a readable time format and change to local time zone
           
           
            int status=[[[dict objectForKey:@"data"]objectForKey:@"status"] intValue];
            self->qusType=[[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"questiontype"] intValue];
              dispatch_async(dispatch_get_main_queue(), ^{
                  if(self->qusType==1)
            {
                self.ques2View.hidden=YES;
                
            }
                  else if(self->qusType==2)
            {
                self.ques2View.hidden=NO;
                self.qus1Fld.hidden=YES;
                
            }
         });
            if(status==1)
            {
                
                
                NSString * dateStr=[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"expiry"]];
                
         
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
                [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];

                //            [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
//                            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                NSDate *dateyDate = [dateFormatter dateFromString:dateStr]; // create date from string
                NSTimeInterval timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];
                dateyDate = [dateyDate dateByAddingTimeInterval:timeZoneSeconds];

                self.expiryDate=dateyDate;
                
                [self countdownTimer];
                dispatch_async(dispatch_get_main_queue(), ^{
                   tapGestureRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom1:)];
                    [self.TimerView addGestureRecognizer:tapGestureRecognizer1];
                    NSString * todayPoints=[NSString stringWithFormat:@"Answer today’s question to win %@ points and more!",[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"rewardamount"]];
                    self.resultsView.hidden=YES;
                    self.recordedView.hidden=YES;
                    self.timerSubView.hidden=NO;
                    self.tapToRevealLbl.text=@"Tap to reveal";
                    self.timerTextview.text=todayPoints;
                    self.timerBg.image=[UIImage imageNamed:@"qusBg"];
                    self.ques1Lbl.text=[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"questionname"]];
                    
                });
     
                self->questionId=[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"questionid"]];
              
            }
            else if(status==2)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                   
                
                self.timerBg.image=[UIImage imageNamed:@"greenBg"];
                self.resultsView.hidden=YES;
                self.recordedView.hidden=YES;
                self.ques2View.hidden=YES;
                self.timerSubView.hidden=YES;
                    NSString *dateStr=[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"] objectForKey:@"data"]objectForKey:@"resultannouncetime"]];
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
                    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                    NSDate *dateyDate = [dateFormatter dateFromString:dateStr];
                    [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
                    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                    NSString * finalDate=[NSString stringWithFormat:@"Winner will be announced at %@",[dateFormatter stringFromDate:dateyDate]];
                self.tapToRevealLbl.text=finalDate;
                self.timerTextview.text=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"message"]];
                    
                int results=[[[[dict objectForKey:@"data"] objectForKey:@"data"]objectForKey:@"resultannounced"] intValue];
                    
                    if(results==1)
                    {
                         int winner=[[[[dict objectForKey:@"data"] objectForKey:@"data"]objectForKey:@"iswinner"] intValue];
                        
                        if(winner==0)
                        {
                            self.resultsView.hidden=NO;
                            self.resultsBgImg.image=[UIImage imageNamed:@"wrong"];
                            self.resultsCupImg.hidden=YES;
                            self.pointsEarnedLbl.hidden=YES;
                            self.pointsCreditedLbl.hidden=YES;
                        }
                        else  if(winner==1)
                        {
                            self.resultsView.hidden=NO;
                            self.resultsBgImg.image=[UIImage imageNamed:@"right"];
                            self.resultsCupImg.hidden=NO;
                            self.pointsEarnedLbl.hidden=NO;
                            self.pointsCreditedLbl.hidden=NO;
                            self.pointsEarnedLbl.text=[NSString stringWithFormat:@"Congratulations!You won %i today!",[[[[dict objectForKey:@"data"] objectForKey:@"data"]objectForKey:@"amount"] intValue]];
                            
                            self.pointsCreditedLbl.text=[NSString stringWithFormat:@"We have credited %i Zambala Points to your wallet",[[[[dict objectForKey:@"data"] objectForKey:@"data"]objectForKey:@"amount"] intValue]];
                        }
                    }
                });
            }
            else if(status==0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
               
                    
                    self.ques1View.hidden=YES;
                    self.resultsView.hidden=NO;
                    self.resultsBgImg.image=[UIImage imageNamed:@"Noques"];
                    self.pointsEarnedLbl.hidden=YES;
                    self.resultsCupImg.hidden=YES;
                    self.pointsCreditedLbl.hidden=YES;
                });
            }
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                
                UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self getQuestions];
                }];
                
                UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                
                
                [alert addAction:retryAction];
                [alert addAction:cancel];
            });
        }

        
    }];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onShareBtnTap:(id)sender {
    
    @try
    {
        
        if(self.referalCodeLbl.text.length!=0)
        {
            Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
            [mixpanelMini track:@"referral_code_share_initiated"];
            NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
            
//            NSString * name= [loggedInUser stringForKey:@"profilename"];
            NSString *textToShare =[NSString stringWithFormat:@"Hi!I am using Zambala Stocks app to invest in Indian and US Capital  Markets. Search your favourite stocks and see what experts are saying about it, and more! Now open your US Broking account for free in 5 minutes. No minimum funding requirement. Track and INVEST in FAANG (Facebook, Apple, Amazon, Netflix and Google) stocks! Sign up using my referral code %@ and we'll earn 10 Reward Points each which can be redeemed at various shopping outlets like BookMyShow, Big Basket etc. Happy investing!",self.referalCodeLbl.text];
            
            NSURL *myWebsite = [NSURL URLWithString:@"https://itunes.apple.com/us/app/zambala-stocks-by-zenwise/id1281356740?ls=1&mt=8"];
            // NSURL *myWebsite = [NSURL URLWithString:[NSString stringWithFormat:@"https://i06-6.tlnk.io/serve?action=click&publisher_id=358381&site_id=137664&my_publisher=%@&my_keyword=%@",delegate1.brokerNameStr,referralCode]];
            UIImage * image=[UIImage imageNamed:@"success"];
            NSArray *objectsToShare = @[textToShare,myWebsite,image];
            
            UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
            
            NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                           UIActivityTypePrint,
                                           UIActivityTypeAssignToContact,
                                           UIActivityTypeSaveToCameraRoll,
                                           UIActivityTypeAddToReadingList,
                                           UIActivityTypePostToFlickr,
                                           UIActivityTypePostToVimeo];
            
            activityVC.excludedActivityTypes = excludeActivities;
            
            [self presentViewController:activityVC animated:YES completion:nil];
            
        }
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

}
- (IBAction)subumitBtn:(id)sender {
    @try
    {
    
    TagEncode * enocde=[[TagEncode alloc]init];
    enocde.inputRequestString=@"submitAnswer";
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
//    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
//    [mixpanelMini track:@"wallet_page"];
    
    [inputArray addObject:number];
    if(qusType==1)
    {
    [inputArray addObject:self.qus1Fld.text];
    }
    else if(qusType==2)
    {
        if(answerBool==true)
        {
             [inputArray addObject:@"true"];
        }
        else if(answerBool==false)
        {
             [inputArray addObject:@"false"];
        }
   
    }
    [inputArray addObject:delegate1.currentDeviceId];
    [inputArray addObject:questionId];
    [inputArray addObject:[NSString stringWithFormat:@"%@survey/answer",delegate1.baseUrl]];
          [inputArray addObject:delegate1.zenwiseToken];
    
    [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        
        NSArray * keysArray=[dict allKeys];
        //NSLog(@"%@",dict);
        if(keysArray.count>0)
        {
            Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
            [mixpanelMini track:@"survey_answer_submitted"];
              dispatch_async(dispatch_get_main_queue(), ^{
            self.recordedView.hidden=NO;
            self.resultsView.hidden=YES;
                  self.ques2View.hidden=YES;
                  self.ques1Lbl.hidden=YES;
                  self.qus1Fld.hidden=YES;
                  self.submitBtn.hidden=YES;
            NSString * message=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"message"]];
            
            self.recordLbl.text=message;
            
            NSString * dateStr=[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"resultannouncetime"]];
            
            
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
            NSDate *dateyDate = [dateFormatter dateFromString:dateStr];
            [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            NSString * finalDate=[NSString stringWithFormat:@"Winner will be announced at %@",[dateFormatter stringFromDate:dateyDate]];
            
            self.resultsannounceTimeLbl.text=finalDate;
            
              });
        }
        
    }];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
- (IBAction)onyesBtnTap:(id)sender {
    
    answerBool=true;
    [self.yesBtn setBackgroundColor:[UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:1.0f]];
   
    self.noBtn.backgroundColor=[UIColor whiteColor];
}

- (IBAction)onNoBtnTap:(id)sender {
    answerBool=false;
    [self.noBtn setBackgroundColor:[UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:1.0f]];
    
    self.yesBtn.backgroundColor=[UIColor whiteColor];
}
- (IBAction)onRedeembtnTap:(id)sender {
}
-(void)getReferral
{
    @try
    {
    TagEncode * enocde=[[TagEncode alloc]init];

    enocde.inputRequestString=@"referral";
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];

    [inputArray addObject:[NSString stringWithFormat:@"%@coupon/userwallet?clientid=%@&mobilenumber=%@&authtoken=%@",delegate1.baseUrl,number,number,delegate1.zenwiseToken]];

    [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {

        NSArray * keysArray=[dict allKeys];
        //NSLog(@"%@",dict);
        if(keysArray.count>0)
        {
            NSString * walletPoints=[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"walletvalue"]];
            emailVerify = [NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"emailverified"]];
            oldEmail = [NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"email"]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
            self.emailLabel.text = [NSString stringWithFormat:@"We have sent a verification email to %@ \nTo continue using the Rewards feature, please verify your email and use the button below to continue.",oldEmail];
            if([emailVerify isEqualToString:@"1"])
            {
                self.popUpView.hidden=YES;
            }else if ([emailVerify isEqualToString:@"0"])
            {
                self.popUpView.hidden=NO;
            }
            });
            
            if([walletPoints isEqualToString:@"<null>"])
            {
                delegate1.rewardPoints=@"0";
                walletPoints=@"0 points";
                dispatch_async(dispatch_get_main_queue(), ^{
                self.rewardPoints.text=walletPoints;
                     });
            }
            else
            {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     delegate1.rewardPoints=[NSString stringWithFormat:@"%@ points",[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"walletvalue"]];
                  self.rewardPoints.text=[NSString stringWithFormat:@"%@ points",[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"walletvalue"]];
                     self.referalCodeLbl.text=[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"referralcode"]];
                     delegate1.referralCodeFromServer = self.referalCodeLbl.text;
//
                    
                     zambalaPointValue=[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"zambalapoint"]];
                     rupeeValue=[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"rsvalue"]];
                     minZambalaPoints = [NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"minzambalabalance"]];
                     if(delegate1.historyArray.count>0)
                     {
                         [delegate1.historyArray removeAllObjects];
                     }
                     [delegate1.historyArray addObjectsFromArray:[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"action"]];
//                     self->transactionalHistoryArray=[[NSArray alloc]init];
//                     self->transactionalHistoryArray=[[[dict objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"action"];
//                     [self.historyTableView reloadData];
                 });
            }

        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                
                UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self getReferral];
                }];
                
                UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                
                
                [alert addAction:retryAction];
                [alert addAction:cancel];
            });
        }

    }];
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
-(void)pointsList
{
    @try
    {
    TagEncode * enocde=[[TagEncode alloc]init];
    
    enocde.inputRequestString=@"pointsList";
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    
    [inputArray addObject:[NSString stringWithFormat:@"%@coupon/earnpointsway?authtoken=%@",delegate1.baseUrl,delegate1.zenwiseToken]];
    
    [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
        
        NSArray * keysArray=[dict allKeys];
        //NSLog(@"%@",dict);
        if(keysArray.count>0)
        {
            NSMutableArray * localArray=[[NSMutableArray alloc]init];
            for(int i=0;i<[[[dict objectForKey:@"data"] objectForKey:@"actions"] count];i++)
            {
                int hidden=[[[[[dict objectForKey:@"data"]objectForKey:@"actions"] objectAtIndex:i]objectForKey:@"ishidden"] intValue];
                if(hidden==0)
                {
                    [localArray addObject:[[[dict objectForKey:@"data"]objectForKey:@"actions"] objectAtIndex:i]];
                }
            }
            earnPointsDict=@{@"data":@{@"actions":localArray}};
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.earnPointsCollection.delegate=self;
                    self.earnPointsCollection.dataSource=self;
                    [self.earnPointsCollection reloadData];
                });
           
            
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                
                UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self pointsList];
                }];
                
                UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                
                
                [alert addAction:retryAction];
                [alert addAction:cancel];
            });
        }

        
    }];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   @try
    {
   if(transactionalHistoryArray.count>0)
   {
       return transactionalHistoryArray.count;
   }
    else
    {
        return 1;
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @try
    {
     WalletHistoryCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    if(transactionalHistoryArray.count>0)
    {
        cell.messageLbl.text=[NSString stringWithFormat:@"%@",[[transactionalHistoryArray objectAtIndex:indexPath.row]objectForKey:@"action"]];
        NSString * dateStr=[NSString stringWithFormat:@"%@",[[transactionalHistoryArray objectAtIndex:indexPath.row]objectForKey:@"createdon"]];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        NSDate *dateyDate = [dateFormatter dateFromString:dateStr];
        [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        NSString * finalDate=[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:dateyDate]];
        
        cell.dateTimeLbl.text=finalDate;
        cell.txnNoLbl.hidden=YES;
    }
    else
    {
        cell.txnNoLbl.hidden=YES;
        cell.dateTimeLbl.hidden=YES;
        cell.messageLbl.text=@"No Transactional history";
    }
    return cell;
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    return 71;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    @try
    {
   return [[[earnPointsDict objectForKey:@"data"]objectForKey:@"actions"] count];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
    EarnPointsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    
    cell.layer.shadowColor =[[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5] CGColor];
    cell.layer.shadowOffset = CGSizeMake(2.0f,5.0f);
    cell.layer.shadowRadius = 5.0f;
    cell.layer.shadowOpacity = 0.5f;
    cell.layer.masksToBounds = NO;
    cell.descriptionLbl.text=[NSString stringWithFormat:@"%@",[[[[earnPointsDict objectForKey:@"data"]objectForKey:@"actions"] objectAtIndex:indexPath.row]objectForKey:@"description"]];
    cell.pointsLbl.text=[NSString stringWithFormat:@"%@",[[[[earnPointsDict objectForKey:@"data"]objectForKey:@"actions"] objectAtIndex:indexPath.row]objectForKey:@"pointText"]];
   NSString * action=[NSString stringWithFormat:@"%@",[[[[earnPointsDict objectForKey:@"data"]objectForKey:@"actions"] objectAtIndex:indexPath.row]objectForKey:@"action"]];
    float one=0;
    float two=0;
    float three=0;
    float four=0;
    float five=0;
    float six=0;
    if([action isEqualToString:@"follow"])
    {
        one=254;
        two=80;
        three=48;
        four=221;
        five=37;
        six=117;
        
    }
    else if([action isEqualToString:@"referral"])
    { one=255;
        two=193;
        three=113;
        four=255;
        five=97;
        six=109;
    }
    else if([action isEqualToString:@"follow"])
    {
        one=0;
        two=0;
        three=0;
        four=0;
        five=0;
        six=0;
    }
    else if([action isEqualToString:@"subscribe"])
    {
        one=0;
        two=179;
        three=218;
        four=0;
        five=132;
        six=177;
    }
    else if([action isEqualToString:@"traded"])
    {
        one=253;
        two=198;
        three=48;
        four=243;
        five=116;
        six=53;
    }
    else if([action isEqualToString:@"open account with indian broker"])
    {
        one=79;
        two=162;
        three=176;
        four=192;
        five=222;
        six=227;
    }
    else if([action isEqualToString:@"open US trading account"])
    {
        one=248;
        two=93;
        three=127;
        four=108;
        five=129;
        six=249;
    }
    else if([action isEqualToString:@"watch zambala tv"])
    {
        one=18;
        two=155;
        three=141;
        four=55;
        five=237;
        six=125;
    }
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = cell.bgView.bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:(one/255.0) green:(two/255.0) blue:(three/255.0) alpha:1.0f].CGColor, (id)[UIColor colorWithRed:(four/255.0) green:(five/255.0) blue:(six/255.0) alpha:1.0f] .CGColor];
    gradient.startPoint = CGPointMake(0.0, 0.5);
    gradient.endPoint = CGPointMake(1.0, 0.5);
    [cell.bgView.layer insertSublayer:gradient atIndex:0];
    return cell;
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}
-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    [self.qus1Fld resignFirstResponder];
    [self.updateEmailIDTF resignFirstResponder];
    return YES;
}
    
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
   @try
    {
    delegate1.pointsNavigation=[NSString stringWithFormat:@"%@",[[[[earnPointsDict objectForKey:@"data"]objectForKey:@"actions"] objectAtIndex:indexPath.row]objectForKey:@"action"]];
    if([delegate1.pointsNavigation containsString:@"Survey participated"])
    {
        //[self handleTapFrom:tapGestureRecognizer1];
        [self handleTapFrom1:tapGestureRecognizer1];
    }else if ([delegate1.pointsNavigation containsString:@"first time login with Zambala app"])
    {
        
    }
    else if([delegate1.pointsNavigation containsString:@"referral"])
    {
        [self onShareBtnTap:self.shareBtn];
    }
    else
    {
        [self.tabBarController setSelectedIndex:3];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
- (IBAction)onBackBtnTap:(id)sender {
    if([self.navigationCheck isEqualToString:@"voucher"])
    {
        TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
        [self presentViewController:tabPage animated:YES completion:nil];
    }else
    {
    [self dismissViewControllerAnimated:YES completion:nil];
    }
}
@end
