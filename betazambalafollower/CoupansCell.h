//
//  CoupansCell.h
//  betazambalafollower
//
//  Created by Zenwise Technologies on 17/04/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CoupansCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *coupanDescription;
@property (weak, nonatomic) IBOutlet UILabel *coupanShrotDescription;
@property (weak, nonatomic) IBOutlet UIButton *applyAction;
@end
