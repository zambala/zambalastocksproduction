//
//  PortfolioView.m
//  testing
//
//  Created by zenwise technologies on 23/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "PortfolioView.h"
#import "HMSegmentedControl.h"
#import "TabBar.h"
#import "PortfolioCell.h"
#import "DummyTableCell.h"
#import "DematCell.h"
#import "AppDelegate.h"
#import "StockOrderView.h"
#import <Mixpanel/Mixpanel.h>
#import "TagEncode.h"
#import "MT.h"
#import <Foundation/Foundation.h>
#import <CoreFoundation/CoreFoundation.h>
#import "MTORDER.h"
#import "MQTTKit.h"
#import "OpenAccountView.h"
#import "BrokerViewNavigation.h"
#define buyColor [UIColor colorWithRed:37.0/255.0 green:114.0/255.0 blue:191.0/255.0 alpha:1.0]
#define sellColor [UIColor colorWithRed:206.0/255.0 green:38.0/255.0 blue:38.0/255.0 alpha:1.0]
@import SocketIO;
@interface PortfolioView ()<NSStreamDelegate,SocketEngineSpec,SocketManagerSpec,SocketEngineClient>
{
    HMSegmentedControl * segmentedControl;
    AppDelegate * delegate;
    NSDictionary * positionDictionary;
    NSDictionary * holdingDictionary;
    NSDictionary *headers;
      BOOL exchangeTokenCheck;
    NSMutableURLRequest *request1;
    
    NSDictionary *parameters;
    
    NSData *postData;
    NSMutableDictionary* dummyResponseArray;
    MT * sharedManager;
    BOOL viewCheck;
    NSMutableArray * securityTokenArray;
    MTORDER * sharedManagerOrder;
    NSMutableArray * exchangeTokenArray;
    NSMutableArray * newMtPositionsArray;
     NSMutableArray *un_array;
    NSArray * segmentsArray;
    NSMutableArray * mtExchangeArray;
    NSMutableDictionary * instrumentTokensDict;
    NSMutableArray * followingUserId;
    BOOL wisdomRefreshCheck;
    NSMutableArray * mtHoldingsArray;
    NSString *  wisdomDurationTypeString11;
    NSString * wisdomBuyString11;
    NSString * finalLederID;
    NSString * wisdomSymbol;
    NSString * number;
    NSTimer * timer;
    NSDictionary * upstoxFeed;
    NSMutableArray * upstoxFeedArray;
}

@end

@implementation PortfolioView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
    
    number= [loggedInUser stringForKey:@"profilemobilenumber"];
    delegate= (AppDelegate *)[[UIApplication sharedApplication]delegate];
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"portfolio_page"];
    [mixpanelMini track:@"dummy_positions_page"];
    sharedManager=[[MT alloc]init];
     sharedManagerOrder = [MTORDER Mt2];
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"DUMMY POSITIONS",@"POSITIONS",@"DEMAT HOLDINGS"]];
    segmentedControl.frame = CGRectMake(0,0, self.view.frame.size.width,40);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
//    segmentedControl.verticalDividerEnabled = YES;
//    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(251/255.0) green:(196/255.0) blue:(12/255.0) alpha:1];
    //    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segmentedControl];
   if(delegate.pushNotificationSection.length>0)
   {
    if([delegate.pushNotificationSection isEqualToString:@"Dummy Positions"])
    {
         [segmentedControl setSelectedSegmentIndex:0];
        [self segmentedControlChangedValue];
    }
   }
    
    positionDictionary =[[NSDictionary alloc]init];
    holdingDictionary=[[NSDictionary alloc]init];
    self.dummyBottomView.hidden=YES;
    
    self.activityInd.hidden=YES;
    self.navigationItem.rightBarButtonItem.enabled = NO;

  
   
    self.bottomInsideView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.bottomInsideView.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.bottomInsideView.layer.shadowOpacity = 10.0f;
    self.bottomInsideView.layer.shadowRadius = 4.0f;
    self.bottomInsideView.layer.cornerRadius=5.0f;
    self.bottomInsideView.layer.masksToBounds = YES;
    
   
    
//    self.dummyTableView.hidden=YES;
}

-(void)loginCheck
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
      
        
   
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Please choose an option." message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [self presentViewController:alert animated:YES completion:^{
        
    }];
    UIAlertAction * OpenAccount=[UIAlertAction actionWithTitle:@"Open Broking Account" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //Navigate to open E- Account page
        //        OpenAccountView * openAccountWebView = [self.storyboard instantiateViewControllerWithIdentifier:@"BrokersWebViewControllerUSA"];
        //        [self.navigationController pushViewController:openAccountWebView animated:YES];
        
        OpenAccountView * view=[self.storyboard instantiateViewControllerWithIdentifier:@"OpenAccountView"];
        [self.navigationController pushViewController:view animated:YES];
    }];
    UIAlertAction * Login=[UIAlertAction actionWithTitle:@"Login" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //Navigate to login page
        NSUserDefaults *loggedInUserNew  = [NSUserDefaults standardUserDefaults];
        //
        delegate.loginActivityStr=@"";
        //             delegate1.loginActivityStr=@"";
        [loggedInUserNew removeObjectForKey:@"loginActivityStr"];
        
        
        
        BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
        
        [self presentViewController:view animated:YES completion:nil];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:OpenAccount];
    [alert addAction:Login];
    [alert addAction:cancel];
        
         });
    
}






-(void)viewDidAppear:(BOOL)animated
{
    
    
    viewCheck=true;
    [self networkStatus];
    [self followerListMethod];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    
    
    
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
    
    
   
    
  
    if([delegate.brokerNameStr isEqualToString:@"Zerodha"]||[delegate.brokerNameStr isEqualToString:@"Upstox"])
    {
        
        
    }
    
    else
    {
        delegate.holdingsMt=true;
    }
    
   
    
    if([delegate.orderToPort isEqualToString:@"ordercheck"])
    {
        delegate.orderToPort=@"";
        [segmentedControl setSelectedSegmentIndex:2];
        [self segmentedControlChangedValue];
    }
    
    else if([delegate.orderToPort isEqualToString:@"position"])
    {
        delegate.orderToPort=@"";
        [segmentedControl setSelectedSegmentIndex:1];
        [self segmentedControlChangedValue];
    }
    
    else
    {
        if([delegate.brokerNameStr isEqualToString:@"Zerodha"]||[delegate.brokerNameStr isEqualToString:@"Upstox"])
        {
            if(viewCheck==true)
            {
                viewCheck=false;
                
                if(segmentedControl.selectedSegmentIndex==1)
                {
                    
            [self positionMethode];
                    
                }

                
            
            
            else  if(segmentedControl.selectedSegmentIndex==2)
            {
                [segmentedControl setSelectedSegmentIndex:2];
                [self segmentedControlChangedValue];
                
                
            }
                
            }
            
            
        }
        
        else
        {
            if(viewCheck==true)
            {
                viewCheck=false;
                if([delegate.loginActivityStr isEqualToString:@"CLIENT"])
                {
               if(segmentedControl.selectedSegmentIndex==1)
               {
                   
                [self mtPositionsRequest];
                   
               }
                
                else  if(segmentedControl.selectedSegmentIndex==2)
                {
                    [self mtHoldingsRequest];
                    
                }
                }
                
            }
            
        }
        
        
    }
    
    
    if(segmentedControl.selectedSegmentIndex==0)
    {
        [segmentedControl setSelectedSegmentIndex:0];
        [self segmentedControlChangedValue];
    }
    
    
    
  
}
-(void)followerListMethod
{
    
    @try {
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"cache-control": @"no-cache",
                                   @"authtoken":delegate.zenwiseToken,
                                   @"deviceid":delegate.currentDeviceId
                                   };
        
        NSDictionary * params;
        
        @try {
            
            params=@{@"clientid":delegate.userID,
                     @"limit":@1000,
                     @"mobilenumber":number,
                     @"brokerid":[delegate.brokerInfoDict objectForKey:@"brokerid"]
                     
                     };
            
            
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
        
        NSData * postData=[NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
        
        
        NSString * localUrl=[NSString stringWithFormat:@"%@follower/allleaders",delegate.baseUrl];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:localUrl]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(data!=nil)
                                                        {
                                                            if (error) {
                                                                //NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                //NSLog(@"%@", httpResponse);
                                                                if([httpResponse statusCode]==403)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                    
                                                                }
                                                                else if([httpResponse statusCode]==401)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    followingUserId=[[NSMutableArray alloc]init];
                                                                    NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                    
                                                                    for(int i=0;i<[[dict objectForKey:@"results"]count];i++)
                                                                    {
                                                                        int subscribed=[[[[dict objectForKey:@"results"] objectAtIndex:i] objectForKey:@"subscribed"] intValue];
                                                                        if(subscribed==1)
                                                                        {
                                                                            
                                                                            [followingUserId addObject:[[[dict objectForKey:@"results"] objectAtIndex:i] objectForKey:@"userid"]];
                                                                        }
                                                                    }
                                                                }
                                                                
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    
                                                                    wisdomRefreshCheck=true;
//                                                                    [self refreshSocket];
                                                                    
                                                                    
                                                                });
                                                                
                                                            }
                                                        }
                                                        else
                                                        {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                                
                                                                [self presentViewController:alert animated:YES completion:^{
                                                                    
                                                                }];
                                                                
                                                                
                                                                UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                    [self followerListMethod];
                                                                }];
                                                                
                                                                UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                    
                                                                }];
                                                                
                                                                
                                                                [alert addAction:retryAction];
                                                                [alert addAction:cancel];
                                                                
                                                            });
                                                            
                                                        }
                                                        
                                                    }];
        [dataTask resume];
        
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    
    
    
    
}

-(void)refreshSocket
{
    @try
    {
        dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
     __weak typeof(self) weakSelf = self;
    NSString * str=[NSString stringWithFormat:@"follower.%@",delegate.userID];
    self.client = [[MQTTClient alloc] initWithClientId:str];
        
    //    srujansoft.com
    [self.client connectToHost:@"52.77.57.127"
        completionHandler:^(MQTTConnectionReturnCode code) {
            if (code == ConnectionAccepted) {
                self.client.keepAlive=(unsigned short)3000000;
                // when the client is connected, subscribe to the topic to receive message.
                
                //                     if(wisdomRefreshCheck==true)
                //                     {
                //                         wisdomRefreshCheck=false;
                for(int i=0;i<followingUserId.count;i++)
                {
                    NSString * userID=[NSString stringWithFormat:@"%@.null",[followingUserId objectAtIndex:i]];
                    [self.client subscribe:userID
                     
                withCompletionHandler:nil];
                    
                }
                
                //                     }
            }
        }];
    
    [self.client setMessageHandler:^(MQTTMessage *message) {
        NSString *text =message.payloadString;
       dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.dummyTableView setContentOffset:CGPointZero animated:YES];
        [weakSelf segmentedControlChangedValue];
        
       });
        
        //NSLog(@"received message %@", text);
    }];
            
            
        });
    
    [self.client disconnectWithCompletionHandler:^(NSUInteger code) {
        // The client is disconnected when this completion handler is called
        //NSLog(@"MQTT client is disconnected");
    }];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)positionMethode
{
    self.dummyPotfolioView.hidden=YES;
    self.dummyBottomView.hidden=YES;
    self.postionView.hidden=NO;
    self.dematView.hidden=YES;
    
    @try {
        NSURL *url1;
        NSDictionary * headers;
        
        if([delegate.brokerNameStr isEqualToString:@"Zerodha"])
        {
            NSString * auth=[NSString stringWithFormat:@"token %@:%@",delegate.APIKey,delegate.accessToken];
            headers = @{ @"cache-control": @"no-cache",
                                       @"X-Kite-Version": delegate.kiteVersion,
                                       @"Authorization": auth
                                       };

        
        url1 = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.kite.trade/portfolio/positions/?api_key=%@&access_token=%@",delegate.APIKey,delegate.accessToken ]];
        
       
            
        }
        
        else if([delegate.brokerNameStr isEqualToString:@"Upstox"])
        {
            url1 = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.upstox.com/live/profile/positions"]];
            
             NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate.accessToken];
            
            headers  = @{ @"cache-control": @"no-cache",
                          @"Content-Type" : @"application/json",
                          @"authorization":access,
                          @"x-api-key":delegate.APIKey
                          };
            
           
        }
        
//        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url1
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:40.0];
        
        [request setHTTPMethod:@"GET"];
       
              [request setAllHTTPHeaderFields:headers];
        
        
       
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(data != nil)
                                                        {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            positionDictionary=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            NSMutableArray * positionArray=[[NSMutableArray alloc]init];
                                                            //NSLog(@"dict %@",positionDictionary);
                                                            
                                                           
                                                            
                                                            Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
                                                            
                                                            [mixpanelMini identify:delegate.userID];
                                                            
                                                            if([delegate.brokerNameStr isEqualToString:@"Zerodha"])
                                                            {
                                                           
                                                                
                                                                
                                                                NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
                                                                [dateFormatter setDateFormat:@"YYYY-MM-DD HH:MM:SS"];
                                                                NSMutableArray * positionsArray = [[NSMutableArray alloc]init];
                                                                for (int i=0; i<mtHoldingsArray.count; i++) {
                                                                    
                                                                    NSMutableDictionary * positionsDictionary = [[NSMutableDictionary alloc]init];
                                                                    NSString * buyStr=[[[[positionDictionary objectForKey:@"data"] objectForKey:@"net"] objectAtIndex:i] objectForKey:@"buy_price"];
                                                                    NSString * str;
                                                                    int butInt=[buyStr intValue];
                                                                    
                                                                    if(butInt==0)
                                                                    {
                                                                        str=@"SELL";
                                                                    }
                                                                    else
                                                                    {
                                                                        str=@"BUY";
                                                                    }
                                                                    [positionsDictionary setObject:[NSString stringWithFormat:@"%@",[[[[positionDictionary objectForKey:@"data"] objectForKey:@"net"] objectAtIndex:i] objectForKey:@"tradingsymbol"]] forKey:@"symbol_name"];
                                                                    [positionsDictionary setObject:[NSString stringWithFormat:@"%@",[[[[positionDictionary objectForKey:@"data"] objectForKey:@"net"] objectAtIndex:i] objectForKey:@"quantity"]] forKey:@"quantity"];
                                                                    [positionsDictionary setObject:[NSString stringWithFormat:@"%@",[[[[positionDictionary objectForKey:@"data"] objectForKey:@"net"] objectAtIndex:i] objectForKey:@"average_price"]] forKey:@"avg_price"];
                                                                    [positionsDictionary setObject:[NSString stringWithFormat:@"%@",str] forKey:@"transaction_type"];
                                                                    [positionsDictionary setObject:[dateFormatter stringFromDate:[NSDate date]] forKey:@"date_time"];
                                                                    [positionsArray addObject:positionsDictionary];
                                                                }
                                                                
                                                                 [mixpanelMini.people set:@{@"positionslist":positionsArray}];
                                                            
//                                                            for(int i=0;i<[[[positionDictionary objectForKey:@"data"]objectForKey:@"net"]count];i++)
//                                                            {
//                                                                NSString * symbolString=[[NSString alloc]init];
//                                                                symbolString=[NSString stringWithFormat:@"%@", [[[[positionDictionary objectForKey:@"data"]objectForKey:@"net"]objectAtIndex:i]objectForKey:@"tradingsymbol"]];
//
//                                                        float qtyFloat=[[NSString stringWithFormat:@"%@", [[[[positionDictionary objectForKey:@"data"]objectForKey:@"net"]objectAtIndex:i]objectForKey:@"quantity"]] floatValue];
//
//                                                                NSString * qtyString=[NSString stringWithFormat:@"%.2f",qtyFloat];
//
//                                                                NSString * qtyStr;
//
//                                                                if(qtyFloat>0)
//                                                                {
//                                                                    qtyStr=@"Square off";
//                                                                }
//
//                                                                else
//                                                                {
//                                                                    qtyStr=@"Flat";
//                                                                }
//
//
//                                                                NSDictionary * postionDetailsDict=@{@"symbol":symbolString,@"QTY":qtyString ,@"Position":qtyStr};
//
//                                                                [positionArray addObject:postionDetailsDict];
//
//                                                                }
                                                                
                                                                
                                                            }
                                                            
                                                            else if([delegate.brokerNameStr isEqualToString:@"Upstox"])
                                                            {
                                                                
                                                                NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
                                                                [dateFormatter setDateFormat:@"YYYY-MM-DD HH:MM:SS"];
                                                                NSMutableArray * positionsArray = [[NSMutableArray alloc]init];
                                                                for (int i=0; i<mtHoldingsArray.count; i++) {
                                                                    
                                                                     NSMutableDictionary * positionsDictionary = [[NSMutableDictionary alloc]init];
                                                                    NSString * buyStr=[[[positionDictionary objectForKey:@"data"] objectAtIndex:i] objectForKey:@"buy_amount"];
                                                                    
                                                                   
                                                                    NSString * str;
                                                                    int butInt=[buyStr intValue];
                                                                    
                                                                    if(butInt==0)
                                                                    {
                                                                        str=@"SELL";
                                                                    }
                                                                    else
                                                                    {
                                                                        str=@"BUY";
                                                                    }
                                                                    [positionsDictionary setObject:[NSString stringWithFormat:@"%@",[[[positionDictionary objectForKey:@"data"] objectAtIndex:i] objectForKey:@"symbol"]] forKey:@"symbol_name"];
                                                                    [positionsDictionary setObject:[NSString stringWithFormat:@"%@",[[[positionDictionary objectForKey:@"data"] objectAtIndex:i] objectForKey:@"net_quantity"]] forKey:@"quantity"];
                                                                    [positionsDictionary setObject:[NSString stringWithFormat:@"%@",[[[positionDictionary objectForKey:@"data"]  objectAtIndex:i] objectForKey:@"avg_buy_price"]] forKey:@"avg_price"];
                                                                    [positionsDictionary setObject:[NSString stringWithFormat:@"%@",str] forKey:@"transaction_type"];
                                                                    [positionsDictionary setObject:[dateFormatter stringFromDate:[NSDate date]] forKey:@"date_time"];
                                                                    [positionsArray addObject:positionsDictionary];
                                                                }
                                                                
                                                                [mixpanelMini.people set:@{@"positionslist":positionsArray}];
                                                                
//                                                                for(int i=0;i<[[positionDictionary objectForKey:@"data"]count];i++)
//                                                                {
//                                                                    NSString * symbolString=[[NSString alloc]init];
//                                                                    symbolString=[NSString stringWithFormat:@"%@", [[[positionDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"symbol"]];
//
//
//
//                                                                    float qtyFloat=[[NSString stringWithFormat:@"%@", [[[positionDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"quantity"]] floatValue];
//
//                                                                    NSString * qtyString=[NSString stringWithFormat:@"%.2f",qtyFloat];
//
//                                                                    NSString * qtyStr;
//
//                                                                    if(qtyFloat>0)
//                                                                    {
//                                                                        qtyStr=@"Square off";
//                                                                    }
//
//                                                                    else
//                                                                    {
//                                                                        qtyStr=@"Flat";
//                                                                    }
//
//
//                                                                    NSDictionary * postionDetailsDict=@{@"symbol":symbolString,@"QTY":qtyString ,@"Position":qtyStr};
//
//                                                                    [positionArray addObject:postionDetailsDict];
//
//                                                                }
                                                                
                                                            }
                                                            
                                                            
                                                            
                                
                                                            
                                                            
                                                            dispatch_async(dispatch_get_main_queue(),
                                                                           ^{
                                                          if([delegate.brokerNameStr isEqualToString:@"Zerodha"])
                                                          {
                                                                               
                                                                               if([[[positionDictionary objectForKey:@"data"] objectForKey:@"net"] count]>0)
                                                                               {
                                                                                   self.noPortfolioView.hidden=YES;
                                                                                   self.positionsTableView.hidden=NO;
                                                                                   self.positionsTableView.delegate=self;
                                                                                   self.positionsTableView.dataSource=self;
                                                                                   [self.
                                                                                    positionsTableView reloadData];
                                                                               }else
                                                                               {
                                                                                   self.noPortfolioView.hidden=NO;
                                                                                   self.positionsTableView.hidden=YES;
                                                                                   
                                                                               }
                                                              
                                                              
                                                          }
                                                                               
                                                                               
                                                                               else if([delegate.brokerNameStr isEqualToString:@"Upstox"])
                                                                               {
                                                                                   if([[positionDictionary objectForKey:@"data"] count]>0)
                                                                                   {
                                                                                       self.noPortfolioView.hidden=YES;
                                                                                       self.positionsTableView.hidden=NO;
                                                                                       self.positionsTableView.delegate=self;
                                                                                       self.positionsTableView.dataSource=self;
                                                                                       [self.
                                                                                        positionsTableView reloadData];
                                                                                   }else
                                                                                   {
                                                                                       self.noPortfolioView.hidden=NO;
                                                                                       self.positionsTableView.hidden=YES;
                                                                                       
                                                                                   }
                                                                                   
                                                                                   
                                                                               }
                                                                       
                                                                              
                                                                               
                                                                               
                                                                           });
                                                            
                                                            
                                                        }
                                                            
                                                        }
                                                        
                                                        else
                                                        {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                                
                                                                [self presentViewController:alert animated:YES completion:^{
                                                                    
                                                                }];
                                                                
                                                                
                                                                UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                    [self positionMethode];
                                                                }];
                                                                UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                   
                                                                }];
                                                                
                                                                [alert addAction:retryAction];
                                                                 [alert addAction:cancel];
                                                                
                                                            });
                                                            
                                                            
                                                        }
                                                    }];
        [dataTask resume];
        [self.positionsTableView reloadData];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
    

 
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtn:(id)sender {
    
//    if([delegate.holdingCheck isEqualToString:@"hold"])
//    {
    
    TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
    
    [self presentViewController:tabPage animated:YES completion:nil];
        
//    }
//
//    else
//
//    {
//        [self dismissViewControllerAnimated:YES completion:nil];
//    }
    
//
}

-(void)segmentedControlChangedValue
{
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    
    if(segmentedControl.selectedSegmentIndex==1)
    {
        self.dummyPotfolioView.hidden=YES;
        self.dummyBottomView.hidden=YES;
        
        self.postionView.hidden=NO;
        self.dematView.hidden=YES;
        [mixpanelMini track:@"positions_page"];
        self.navigationItem.rightBarButtonItem.enabled = NO;
        if([delegate.loginActivityStr isEqualToString:@"CLIENT"])
        {
        if([delegate.brokerNameStr isEqualToString:@"Zerodha"]||[delegate.brokerNameStr isEqualToString:@"Upstox"])
        {
         [self positionMethode];
        }
        else
        {
           
            [self mtPositionsRequest];
            
        }
        }
        
        if([delegate.loginActivityStr isEqualToString:@"OTP1"])
        {
            [self loginCheck];
            
        }else if ([delegate.loginActivityStr isEqualToString:@"CLIENT"])
        {
            
        }
    }
    
    else if(segmentedControl.selectedSegmentIndex==0)
    {
        
         [mixpanelMini track:@"dummy_positions_page"];
        self.navigationItem.rightBarButtonItem.enabled = YES;
        self.dummyPotfolioView.hidden=NO;
        self.dummyBottomView.hidden=NO;
        
        self.postionView.hidden=YES;
        self.dematView.hidden=YES;
        
        @try {
            
            wisdomDurationTypeString11=@"";
            wisdomBuyString11=@"";
            finalLederID=@"";
            NSString * wisdomFromDate;
            NSString  * wisdomToDate;
            
            
            /*!
             @discussion used to check filter is applied or not.
             */
            
            if(delegate.wisdomBool==true)
            {
                wisdomToDate=delegate.wisdomaToStr;
                wisdomFromDate=delegate.wisdomFromStr;
                wisdomSymbol=delegate.wisdomSearchSymbol;
                NSArray* words = [delegate.wisdomBuySell componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* wisdomBuyString = [words componentsJoinedByString:@""];
                NSString* wisdomBuyString1 = [wisdomBuyString stringByReplacingOccurrencesOfString:@"(" withString:@""];
                wisdomBuyString11 = [wisdomBuyString1 stringByReplacingOccurrencesOfString:@")" withString:@""];
                
                if(wisdomBuyString11.length==0)
                {
                    wisdomBuyString11=@"";
                }
                
                if(delegate.durationUpdateBool==true)
                {
                    NSArray* words1 = [delegate.profileDuration componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    NSString* wisdomDurationString = [words1 componentsJoinedByString:@""];
                    NSString* wisdomDurationString1 = [wisdomDurationString stringByReplacingOccurrencesOfString:@"(" withString:@""];
                    wisdomDurationTypeString11 = [wisdomDurationString1 stringByReplacingOccurrencesOfString:@")" withString:@""];
                }
                
                if(delegate.wisdomDuration.length>0)
                {
                    NSArray* words1 = [delegate.wisdomDuration componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    NSString* wisdomDurationString = [words1 componentsJoinedByString:@""];
                    NSString* wisdomDurationString1 = [wisdomDurationString stringByReplacingOccurrencesOfString:@"(" withString:@""];
                    wisdomDurationTypeString11 = [wisdomDurationString1 stringByReplacingOccurrencesOfString:@")" withString:@""];
                }
                else
                {
                    wisdomDurationTypeString11=@"";
                }
                
                
                
                if(delegate.selectLeadersID.count>0)
                {
                    
                    NSString *leaderID = [NSString stringWithFormat:@"%@",delegate.selectLeadersID];
                    NSString * leaderID1 = [leaderID stringByReplacingOccurrencesOfString:@"(" withString:@""];
                    NSString * leaderID2 = [leaderID1 stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                    finalLederID = [leaderID2 stringByReplacingOccurrencesOfString:@")" withString:@""];
                    finalLederID = [finalLederID stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    
                }else
                {
                    finalLederID=@"";
                }
                
                delegate.selectLeadersID=[[NSMutableArray alloc]init];
                delegate.selectLeadersName=[[NSMutableArray alloc]init];
                
            }else
            {
                if(delegate.durationUpdateBool==true)
                {
                    NSArray* words1 = [delegate.profileDuration componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    NSString* wisdomDurationString = [words1 componentsJoinedByString:@""];
                    NSString* wisdomDurationString1 = [wisdomDurationString stringByReplacingOccurrencesOfString:@"(" withString:@""];
                    wisdomDurationTypeString11 = [wisdomDurationString1 stringByReplacingOccurrencesOfString:@")" withString:@""];
                    wisdomBuyString11=@"";
                    finalLederID=@"";
                    wisdomFromDate=@"";
                    wisdomToDate=@"";
                    wisdomSymbol=@"";
                }
                else
                {
                    wisdomDurationTypeString11=@"";
                    wisdomBuyString11=@"";
                    finalLederID=@"";
                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                    [dateFormat setDateStyle: NSDateFormatterShortStyle];
                    [dateFormat setDateFormat:@"yyyy-MM-dd"];
                    wisdomFromDate = [dateFormat stringFromDate:[NSDate date]];
                    wisdomToDate=@"";
                    wisdomSymbol=@"";
                    
                }
                
                
            }
            
            
            segmentsArray=[[NSArray alloc]init];
            
            if([delegate.loginActivityStr isEqualToString:@"OTP1"])
            {
                
                segmentsArray=[delegate.brokerInfoDict objectForKey:@"guestsegment"];
            }
            
            else
            {
                segmentsArray=[delegate.brokerInfoDict objectForKey:@"segment"];
            }
           
            NSArray * segmentArray=@[@1,@2];
            NSString * segmentString=[NSString stringWithFormat:@"%@",segmentsArray];
            NSString * segmentF = [segmentString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"()"]];
            NSString * segment1 = [segmentF stringByReplacingOccurrencesOfString:@"(" withString:@""];
            NSString * segment2 = [segment1 stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            NSString * segment3 = [segment2 stringByReplacingOccurrencesOfString:@" " withString:@""];
            //NSLog(@"SegmentString:%@",segment3);
            
            

            headers = @{ @"cache-control": @"no-cache",
                         @"content-type": @"application/json",
                         @"authtoken":delegate.zenwiseToken,
                            @"deviceid":delegate.currentDeviceId
                         };
            
            request1 = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower/dummyposition",delegate.baseUrl]]
                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                          timeoutInterval:10.0];
            
           // NSString * dateStr=[NSString stringWithFormat:@"%@",[NSDate date]];
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateStyle: NSDateFormatterShortStyle];
            [dateFormat setDateFormat:@"yyyy-MM-dd"];
            NSString *date = [dateFormat stringFromDate:[NSDate date]];
            if(delegate.wisdomTopAdvices==true)
            {
                if([delegate.adviceType isEqualToString:@"Public"])
                {
                    parameters = @{
                                   @"clientid":delegate.userID,
                                   @"segment":segment3,
                                   @"active":@(true),
                                   @"issubscribed":@(true),
                                    @"limit":@3000,
//                                   @"offset":offset,
                                   @"orderby":@"createdon",
                                   @"ispublic":@(true),
                                   @"durationtype":wisdomDurationTypeString11,
                                   @"sortby":@"DESC",
                                   @"mobilenumber":number,
                                     @"brokerid":[delegate.brokerInfoDict objectForKey:@"brokerid"]
                                   
                                   };
                    
                }
            }
                else
                {
           
             parameters = @{
                              @"clientid":delegate.userID,
                              @"active":@(true),
                              @"issubscribed":@(true),
                              @"messagetypeid":@1,
                              @"limit":@3000,
                              @"sortby":@"DESC",
                              @"orderby":@"createdon",
                              @"closed":@(true),
                              @"segment":segment3,
                              @"durationtype":wisdomDurationTypeString11,
                              @"buysell":wisdomBuyString11,
                              @"leaderid":finalLederID,
                              @"fromdate":wisdomFromDate,
                              @"todate":wisdomToDate,
                              @"symbol":wisdomSymbol,
                              @"mobilenumber":number,
                                @"brokerid":[delegate.brokerInfoDict objectForKey:@"brokerid"]
                              
                              };
                }
            postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];

            
            [request1 setHTTPMethod:@"POST"];
            [request1 setAllHTTPHeaderFields:headers];
            [request1 setHTTPBody:postData];
            
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request1
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            if(data != nil)
                                                            {
                                                            if (error) {
                                                                //NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                //NSLog(@"%@", httpResponse);
                                                                if([httpResponse statusCode]==403)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                    
                                                                }
                                                                else if([httpResponse statusCode]==401)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    
                                                                dummyResponseArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                
                                                                //NSLog(@"Reponse array:%@",dummyResponseArray);
                                                                
                                                                
                                                                
                                                               
                                                                //                                                        if([self.advicesArray count]>0)
                                                                //                                                        {
                                                                //                                                            [self.advicesArray removeAllObjects];
                                                                //                                                        }
                                                                //                                                        for(int i=0;i<[self.responseArray count];i++)
                                                                //                                                        {
                                                                //                                                        self.test=[[self.responseArray objectAtIndex:i] objectForKey:@"isactive"];
                                                                //
                                                                //                                                        self.check=[[[self.responseArray objectAtIndex:i] objectForKey:@"isactive"] boolValue];
                                                                //                                                        if(self.check==1)
                                                                //                                                        {
                                                                //                                                            [self.advicesArray addObject:[self.responseArray objectAtIndex:i]];
                                                                //
                                                                //                                                        }
                                                                //                                                        }
                                                                //                                                        //NSLog(@"Advices:%@",self.advicesArray);
                                                                }
                                                                
                                                            }
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                
                                                                if([[dummyResponseArray objectForKey:@"data"] count]>0)
                                                                {
                                                                    self.dummyBottomView.hidden=NO;
                                                                    self.noPortfolioView.hidden=YES;
                                                                    self.dummyTableView.hidden=NO;
                                                                    
                                                                    NSString * profitAndLoss=[NSString stringWithFormat:@"%@",[dummyResponseArray  objectForKey:@"totalprofitloss"]];
                                                                    if([profitAndLoss containsString:@"-"])
                                                                    {
                                                                         self.netProfitAndLoss.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                                                                      
                                                                        
                                                                    }
                                                                    
                                                                    else
                                                                    {
                                                                         self.netProfitAndLoss.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                                                                        
                                                                    }
                                                                    self.netProfitAndLoss.text =profitAndLoss;
                                                                    self.dummyTableView.delegate=self;
                                                                    self.dummyTableView.dataSource=self;
                                                                    
                                                                    [self.dummyTableView reloadData];
                                                                }else
                                                                {
                                                                    self.noPortfolioView.hidden=NO;
                                                                    self.dummyBottomView.hidden=YES;
                                                                    self.dummyTableView.hidden=YES;
                                                                }

                                                                
                                                                
                                                                
                                                                
                                                            });
                                                            }
                                                            else
                                                            {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                                    
                                                                    [self presentViewController:alert animated:YES completion:^{
                                                                        
                                                                    }];
                                                                    
                                                                    
                                                                    UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                        [self segmentedControlChangedValue];
                                                                    }];
                                                                    UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                       
                                                                    }];
                                                                    
                                                                    [alert addAction:retryAction];
                                                                     [alert addAction:cancel];
                                                                    
                                                                });
                                                                
                                                                
                                                            }
                                                            
                                                        }];
            [dataTask resume];
            
            
            
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }

        
           }
    
    else if(segmentedControl.selectedSegmentIndex==2)
    {
        if([delegate.loginActivityStr isEqualToString:@"OTP1"])
        {
            [self loginCheck];
            
        }else if ([delegate.loginActivityStr isEqualToString:@"CLIENT"])
        {
            
        }
         [mixpanelMini track:@"demat_holdings_page"];
        self.navigationItem.rightBarButtonItem.enabled = NO;
        if([delegate.loginActivityStr isEqualToString:@"CLIENT"])
        {
        if([delegate.brokerNameStr isEqualToString:@"Zerodha"]||[delegate.brokerNameStr isEqualToString:@"Upstox"])
        {
        @try {
            self.dummyPotfolioView.hidden=YES;
            self.dummyBottomView.hidden=YES;
            self.postionView.hidden=YES;
           
            NSURL *url1;
            if([delegate.brokerNameStr isEqualToString:@"Zerodha"])
            {
                NSString * auth=[NSString stringWithFormat:@"token %@:%@",delegate.APIKey,delegate.accessToken];
                 headers = @{ @"cache-control": @"no-cache",
                                           @"X-Kite-Version": delegate.kiteVersion,
                                           @"Authorization": auth
                                           };

            url1 = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.kite.trade/portfolio/holdings/?api_key=%@&access_token=%@",delegate.APIKey,delegate.accessToken ]];
            
            //NSLog(@"url %@",url1);
                
            }
            
            else if([delegate.brokerNameStr isEqualToString:@"Upstox"])
            {
                 url1 = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.upstox.com/live/profile/holdings"]];
                 NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate.accessToken];
                headers  = @{ @"cache-control": @"no-cache",
                              @"Content-Type" : @"application/json",
                              @"authorization":access,
                              @"x-api-key":delegate.APIKey
                              };
                
            }
            
            
            
//            NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url1];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url1
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:40.0];
            
            [request setHTTPMethod:@"GET"];
            
            [request setAllHTTPHeaderFields:headers];
            
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            if(data != nil)
                                                            {
                                                                if (error) {
                                                                    //NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                //NSLog(@"%@", httpResponse);
                                                                holdingDictionary=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                if([delegate.brokerNameStr isEqualToString:@"Zerodha"])
                                                                {
                                                                NSMutableArray * totalHoldingValue=[[NSMutableArray alloc]init];
                                                                
                                                                 NSMutableArray * holdingScrips=[[NSMutableArray alloc]init];
                                                                 
                                                                    NSMutableArray * mixholdingsArray = [[NSMutableArray alloc]init];
                                                                //NSLog(@"dict %@",holdingDictionary);
                                                                
                                                                    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
                                                                    
                                                                    [mixpanelMini identify:delegate.userID];
                                                                    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
                                                                    [dateFormatter setDateFormat:@"YYYY-MM-DD HH:MM:SS"];
                                                                    for (int i=0; i<[[holdingDictionary objectForKey:@"data"] count]; i++) {
                                                                        NSMutableDictionary * mixholdingsDictionary = [[NSMutableDictionary alloc]init];
                                                                        [mixholdingsDictionary setObject:[NSString stringWithFormat:@"%@",[[[holdingDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"tradingsymbol"]] forKey:@"symbol_name"];
                                                                        [mixholdingsDictionary setObject:[NSString stringWithFormat:@"%@",[[[holdingDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"tradingsymbol"]] forKey:@"quantity"];
                                                                         [mixholdingsDictionary setObject:[NSString stringWithFormat:@"%@",[[[holdingDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"average_price"]] forKey:@"avg_price"];
                                                                        
                                                                        [mixholdingsDictionary setObject:[NSString stringWithFormat:@"%@",[[[holdingDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"average_price"]] forKey:@"avg_price"];
                                                                        [mixholdingsDictionary setObject:[dateFormatter stringFromDate:[NSDate date]] forKey:@"date_time"];
                                                                        [mixholdingsArray addObject:mixholdingsDictionary];
                                                                        
                                                                    }
                                                                
                                                                for(int i=0;i<[[holdingDictionary objectForKey:@"data"]count];i++)
                                                                                                                  {
                                                                                                                      NSString * string1=[[NSString alloc]init];
                                                                                                                       string1=[NSString stringWithFormat:@"%@", [[[holdingDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"tradingsymbol"]];
                                                                                                                      
                                                                                                                      [holdingScrips addObject:string1];
                                                                                                                      
                                                                                              float value=[[[[holdingDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"last_price"]floatValue]* [[[[holdingDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"quantity"]floatValue];
                                                                                                                      
                                                                                                                      
//                                                                                                                      NSString * stringValue=[NSString stringWithFormat:@"%.2f",value];
//
                                                                                                                      [totalHoldingValue addObject:[NSNumber  numberWithFloat:value]];
                                                                                                                                                    }
                                                                
                                                                float sum = 0;
                                                                for (NSNumber *num in totalHoldingValue)
                                                                {
                                                                    sum += [num floatValue];
                                                                }
                                                                
                                                                NSString * totalSum=[NSString stringWithFormat:@"%.2f",sum];
                                                        
                                                                
                                                                [mixpanelMini.people set:@{@"holdingslist":mixholdingsArray,@"totalholdingvalue":totalSum}];
                                                                
                                                                }else if ([delegate.brokerNameStr isEqualToString:@"Upstox"])
                                                                {
                                                                    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
                                                                    [mixpanelMini identify:delegate.userID];
                                                                    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
                                                                    [dateFormatter setDateFormat:@"YYYY-MM-DD HH:MM:SS"];
                                                                    NSMutableArray * holdingsArray = [[NSMutableArray alloc]init];
                                                                    for (int i=0; i<[[holdingDictionary objectForKey:@"data"] count]; i++) {
                                                                        NSMutableDictionary * holdingsDictionary = [[NSMutableDictionary alloc]init];
                                                                        [holdingsDictionary setObject:[NSString stringWithFormat:@"%@",[[[[[holdingDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"instrument"]objectAtIndex:0]objectForKey:@"symbol"]] forKey:@"symbol_name"];
                                                                        [holdingsDictionary setObject:[NSString stringWithFormat:@"%@",[[[holdingDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"quantity"]] forKey:@"quantity"];
                                                                        [holdingsDictionary setObject:[NSString stringWithFormat:@"%@",[[[holdingDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"avg_price"]] forKey:@"avg_price"];

                                                                        [holdingsDictionary setObject:[dateFormatter stringFromDate:[NSDate date]] forKey:@"date_time"];
                                                                        [holdingsArray addObject:@"holdingsDictionary"];
                                                                    }
                                                                    [mixpanelMini.people set:@{@"holdingslist":holdingsArray}];

                                                                    
                                                                }
                                                                dispatch_async(dispatch_get_main_queue(),
                                                                               ^{
                                                                                   
                                                                    if([[holdingDictionary objectForKey:@"data"] count]>0)
                                                                    {
                                                                        if([delegate.brokerNameStr isEqualToString:@"Upstox"])
                                                                        {
                                                                            [self upstoxHoldingFeed];
                                                                        }
                                                                        self.noPortfolioView.hidden=YES;
                                                                        self.dematTableView
                                                                        .hidden=NO;
                                                                        self.dematView.hidden=NO;
                                                                        self.dematTableView.delegate=self;
                                                                        self.dematTableView.dataSource=self;
                                                                        
                                                                        [self.dematTableView reloadData];
                                                                    }else
                                                                    {
                                                                        
                                                                        self.noPortfolioView.hidden=NO;
                                                                        self.dematTableView.hidden=YES;
                                                                    
                                                                    }
                                                                                   
                                                                               });
                                                                
                                                                
                                                            }
                                                            }
                                                            else
                                                            {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                                    
                                                                    [self presentViewController:alert animated:YES completion:^{
                                                                        
                                                                    }];
                                                                    
                                                                    
                                                                    UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                        [self segmentedControlChangedValue];
                                                                    }];
                                                                    
                                                                    [alert addAction:retryAction];
                                                                    
                                                                    UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                       
                                                                    }];
                                                                    
                                                                    [alert addAction:cancel];
                                                                    
                                                                });
                                                                
                                                                
                                                            }
                                                        }];
            [dataTask resume];
            

            
        }
        @catch (NSException * e) {
            //NSLog(@"Exception: %@", e);
        }
        @finally {
            //NSLog(@"finally");
        }
            
            
            
        }
        
        
        else
        {
            
            delegate.holdingRequestCheck=false;
            instrumentTokensDict=[[NSMutableDictionary alloc]init];
            [self mtHoldingsRequest];
            
        
        }
       
    }
        
        else
        {
            self.noPortfolioView.hidden=NO;
        }
        
    }
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(tableView==self.positionsTableView)
    {
        
        return 1;
    }
    else if(tableView==self.dummyTableView)
    {
        
        return 1;
    }
    
    else if(tableView==self.dematTableView)
    {
        
        return 1;
    }
    return 0;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    @try
    {
    if(tableView==self.positionsTableView)
    {

        if([delegate.brokerNameStr isEqualToString:@"Zerodha"])
        {
     return [[[positionDictionary objectForKey:@"data"] objectForKey:@"net"] count];
            
        }
        
        else if([delegate.brokerNameStr isEqualToString:@"Upstox"])
        {
             return [[positionDictionary objectForKey:@"data"]count];
            
        }
        
        else
        {
        
            return newMtPositionsArray.count;
        }
        
       
    }
    else if(tableView==self.dummyTableView)
    {
        
        return [[dummyResponseArray objectForKey:@"data"]count];
    }
    
    else if(tableView==self.dematTableView)
    {
        if([delegate.brokerNameStr isEqualToString:@"Zerodha"]||[delegate.brokerNameStr isEqualToString:@"Upstox"])
        {
        
        return [[holdingDictionary objectForKey:@"data"]count];
        }
        
        else
        {
//            return delegate.mtHoldingArray.count;
            if(mtHoldingsArray.count>0)
            {
            return mtHoldingsArray.count;
            }
            else
            {
                return 0;
            }
            
        }
    }
    return 0;
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @try {
        if(tableView==self.positionsTableView)
        {
            if([delegate.brokerNameStr isEqualToString:@"Zerodha"])
            {
            
            if([[[positionDictionary objectForKey:@"data"] objectForKey:@"net"]count]>0)
            {
                
                PortfolioCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
                
                cell.companyName.text=[[[[positionDictionary objectForKey:@"data"] objectForKey:@"net"] objectAtIndex:indexPath.row] objectForKey:@"tradingsymbol"];
                
                //NSLog(@"%@",cell.companyName.text);
                
                NSString * str1=[[[[positionDictionary objectForKey:@"data"] objectForKey:@"net"] objectAtIndex:indexPath.row] objectForKey:@"quantity"];
                int finalQty=[str1 intValue];
                if(finalQty==0)
                {
                    [cell.squareoffBtn setTitle:@"FLAT" forState:UIControlStateNormal];
                }else
                {
                    [cell.squareoffBtn setTitle:@"SQUARE OFF" forState:UIControlStateNormal];
                }
                
                cell.qtyTypeLbl.text=[NSString stringWithFormat:@"%i",finalQty];
                //NSLog(@"%@",cell.qtyTypeLbl.text);
                
                NSString * str=[[[[positionDictionary objectForKey:@"data"] objectForKey:@"net"] objectAtIndex:indexPath.row] objectForKey:@"average_price"];
                float finalPrice=[str floatValue];
                
                
                
                cell.priceLbl.text=[NSString stringWithFormat:@"%.2f",finalPrice];
                
                //NSLog(@"%@",cell.priceLbl.text);
                
                NSString * plValue=[[[[positionDictionary objectForKey:@"data"] objectForKey:@"net"] objectAtIndex:indexPath.row] objectForKey:@"pnl"];
                
                float finalValue=[plValue floatValue];
                
                NSString * string=[NSString stringWithFormat:@"%.2f",finalValue];
                
                if([string containsString:@"-"])
                {
                    cell.openLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                }
                
                else
                {
                    cell.openLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                }
                
                cell.openLbl.text=string;
                
                //NSLog(@"%@",cell.openLbl.text);
                cell.segmentLbl.text=[[[[positionDictionary objectForKey:@"data"] objectForKey:@"net"] objectAtIndex:indexPath.row] objectForKey:@"exchange"];
                
                //NSLog(@"%@",cell.segmentLbl.text);
                
                NSString * buyStr=[[[[positionDictionary objectForKey:@"data"] objectForKey:@"net"] objectAtIndex:indexPath.row] objectForKey:@"buy_price"];
                
                int butInt=[buyStr intValue];
                
                if(butInt==0)
                {
                    cell.transactionTypeLbl.text=@"SELL";
                }
                else
                {
                    cell.transactionTypeLbl.text=@"BUY";
                }
                if([cell.qtyTypeLbl.text isEqualToString:@"0"])
                {
                    cell.transactionTypeLbl.text=@"B/S";
                    cell.transactionTypeLbl.layer.borderColor = [UIColor colorWithRed:2.0/255.0 green:30.0/255.0 blue:41.0/255.0 alpha:1.0].CGColor;
                    cell.transactionTypeLbl.textColor =[UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0] ;
                }
                
                else  if([cell.qtyTypeLbl.text containsString:@"-"])
                {
                    cell.transactionTypeLbl.text=@"SELL";
                    cell.transactionTypeLbl.layer.borderColor = sellColor.CGColor;
                    cell.transactionTypeLbl.textColor = sellColor;
                    
                }
                
                else
                {
                    cell.transactionTypeLbl.text=@"BUY";
                    cell.transactionTypeLbl.layer.borderColor = buyColor.CGColor;
                    cell.transactionTypeLbl.textColor = buyColor;
                    
                    
                }
                
                [cell.squareoffBtn addTarget:self action:@selector(squareoffBtnAction:) forControlEvents:UIControlEventTouchUpInside];
                return cell;
                
            }
                
            }
            
            else if([delegate.brokerNameStr isEqualToString:@"Upstox"])
            {
                
                if([[positionDictionary objectForKey:@"data"]count]>0)
                {
                    
                    PortfolioCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
                    
                    cell.companyName.text=[[[positionDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"symbol"];
                    
                    //NSLog(@"%@",cell.companyName.text);
                    
                    NSString * str1=[[[positionDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"net_quantity"];
                    int finalQty=[str1 intValue];
                    if(finalQty==0)
                    {
                        [cell.squareoffBtn setTitle:@"FLAT" forState:UIControlStateNormal];
                        cell.openLbl.text=[NSString stringWithFormat:@"%.2f",[[[[positionDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"realized_profit"]floatValue]];
                    }else
                    {
                        [cell.squareoffBtn setTitle:@"SQUARE OFF" forState:UIControlStateNormal];
                        cell.openLbl.text=[NSString stringWithFormat:@"%.2f",[[[[positionDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"unrealized_profit"]floatValue]];
                    }
                    
                    cell.qtyTypeLbl.text=[NSString stringWithFormat:@"%i",finalQty];
                    
                    NSString * productStr =[NSString stringWithFormat:@"%@",[[[positionDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"product"]];
                    if([productStr isEqualToString:@"D"])
                    {
                        cell.productTypeLbl.text = @"Product: Delivery";
                    }
                    else  if([productStr isEqualToString:@"I"])
                    {
                        cell.productTypeLbl.text = @"Product: Intraday";
                    }
                    
                    
                    //NSLog(@"%@",cell.qtyTypeLbl.text);
                    
//                    NSString * str=[[[positionDictionary objectForKey:@"data"]  objectAtIndex:indexPath.row] objectForKey:@"average_price"];
//                    float finalPrice=[str floatValue];
//
//
//
//                    cell.priceLbl.text=[NSString stringWithFormat:@"%.2f",finalPrice];
//
//                    //NSLog(@"%@",cell.priceLbl.text);
//
//                    NSString * plValue=[[[[positionDictionary objectForKey:@"data"] objectForKey:@"net"] objectAtIndex:indexPath.row] objectForKey:@"pnl"];
//
//                    float finalValue=[plValue floatValue];
//
//                    cell.openLbl.text=[NSString stringWithFormat:@"%.2f",finalValue];
//
//                    //NSLog(@"%@",cell.openLbl.text);
                    cell.segmentLbl.text=[[[positionDictionary objectForKey:@"data"]objectAtIndex:indexPath.row] objectForKey:@"exchange"];
                    
                    //NSLog(@"%@",cell.segmentLbl.text);
                    
                    NSString * buyStr=[[[positionDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"buy_amount"];
                    
                    int butInt=[buyStr intValue];
                    
                    if(butInt==0)
                    {
                        cell.transactionTypeLbl.text=@"SELL";
                        NSString * str=[[[positionDictionary objectForKey:@"data"]  objectAtIndex:indexPath.row] objectForKey:@"avg_sell_price"];
                                            float finalPrice=[str floatValue];
                        
                        
                        
                                            cell.priceLbl.text=[NSString stringWithFormat:@"%.2f",finalPrice];
                        
//                        NSString * ltp=[NSString stringWithFormat:@"%@",[[[positionDictionary objectForKey:@"data"]  objectAtIndex:indexPath.row] objectForKey:@"avg_sell_price"]];
                        
                        
                    }
                    else
                    {
                        cell.transactionTypeLbl.text=@"BUY";
                        
                        NSString * str=[[[positionDictionary objectForKey:@"data"]  objectAtIndex:indexPath.row] objectForKey:@"avg_buy_price"];
                        float finalPrice=[str floatValue];
                        
                        
                        
                        cell.priceLbl.text=[NSString stringWithFormat:@"%.2f",finalPrice];
                        
                        
                    }
                    if([cell.qtyTypeLbl.text isEqualToString:@"0"])
                    {
                        cell.transactionTypeLbl.text=@"B/S";
                        cell.transactionTypeLbl.layer.borderColor = [UIColor colorWithRed:2.0/255.0 green:30.0/255.0 blue:41.0/255.0 alpha:1.0].CGColor;
                        cell.transactionTypeLbl.textColor =[UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0] ;
                    }
                    
                    else  if([cell.qtyTypeLbl.text containsString:@"-"])
                    {
                        cell.transactionTypeLbl.text=@"SELL";
                        cell.transactionTypeLbl.layer.borderColor = sellColor.CGColor;
                        cell.transactionTypeLbl.textColor = sellColor;
                        
                    }
                    
                    else
                    {
                        cell.transactionTypeLbl.text=@"BUY";
                        cell.transactionTypeLbl.layer.borderColor = buyColor.CGColor;
                        cell.transactionTypeLbl.textColor = buyColor;
                        
                        
                    }
                    
                    [cell.squareoffBtn addTarget:self action:@selector(squareoffBtnAction:) forControlEvents:UIControlEventTouchUpInside];
                    return cell;
                    
                }
                
                
            }
            
            else
            {
                
                PortfolioCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
                
//                cell.companyName.text=[[delegate.mtpositionsArray objectAtIndex:indexPath.row] objectForKey:@"stSymbol"];
               
                
                cell.companyName.text=[NSString stringWithFormat:@"%@",[[newMtPositionsArray objectAtIndex:indexPath.row] objectForKey:@"tradingsymbol"]];
                //NSLog(@"%@",cell.companyName.text);
                
               
                
//                if(buyQty==0)
//                {
//                     cell.qtyTypeLbl.text=[[delegate.mtpositionsArray objectAtIndex:indexPath.row] objectForKey:@"inSellQty"];
//
//                    cell.priceLbl.text=[[delegate.mtpositionsArray objectAtIndex:indexPath.row] objectForKey:@"dbSellAmount"];
//
//                    cell.transactionTypeLbl.text=@"SELL";
//
//
//
//                }
//
//                else if(sellQty==0)
//                {
                    cell.qtyTypeLbl.text=[NSString stringWithFormat:@"%@",[[newMtPositionsArray objectAtIndex:indexPath.row] objectForKey:@"quantity"]];
               
                
                  cell.segmentLbl.text=[[newMtPositionsArray objectAtIndex:indexPath.row] objectForKey:@"exchange"];
                if([cell.qtyTypeLbl.text containsString:@"-"])
                {
                    NSString * sellAvgStr = [NSString stringWithFormat:@"%@",[[newMtPositionsArray objectAtIndex:indexPath.row]objectForKey:@"sellaverageprice"]];
                if(![sellAvgStr isEqualToString:@"<null>"])
                {
                      cell.priceLbl.text=[NSString stringWithFormat:@"%.2f",[[[newMtPositionsArray objectAtIndex:indexPath.row]objectForKey:@"sellaverageprice"]floatValue]];
                }
                else
                {
                    cell.priceLbl.text=@"---";
                    
                }
                }
                else
                {
                    NSString * buyAvgStr = [NSString stringWithFormat:@"%@",[[newMtPositionsArray objectAtIndex:indexPath.row]objectForKey:@"buyaverageprice"]];
                    if(![buyAvgStr isEqualToString:@"<null>"])
                    {
                     cell.priceLbl.text=[NSString stringWithFormat:@"%.2f",[[[newMtPositionsArray objectAtIndex:indexPath.row]objectForKey:@"buyaverageprice"]floatValue]];
                    }
                    else
                    {
                        cell.priceLbl.text=@"---";
                        
                    }
                }
              
                
                NSString * mainSecurityid=[NSString stringWithFormat:@"%@",[[newMtPositionsArray objectAtIndex:indexPath.row] objectForKey:@"exchangetoken"]];
                cell.openLbl.text=@"0.00";
//                if([cell.segmentLbl.text isEqualToString:@"CDS"])
//                {
//                    NSString * ltp=@"65.1700";
//                    float value=[ltp floatValue] - [cell.priceLbl.text floatValue];
//                    float value1=value *[[[newMtPositionsArray objectAtIndex:indexPath.row]objectForKey:@"multiplier"] intValue];
//                    cell.openLbl.text=[NSString stringWithFormat:@"%.4f",value1];
//
//                    if([cell.openLbl.text containsString:@"-"])
//                    {
//                    cell.openLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
//                    }
//                else
//                    {
//                    cell.openLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
//                    }
//
//                }
              
                NSString * productStr =[NSString stringWithFormat:@"%@", [[newMtPositionsArray objectAtIndex:indexPath.row] objectForKey:@"product_type"]];
                if([productStr isEqualToString:@"1"])
                {
                    cell.productTypeLbl.text = @"Product: Delivery";
                }
                else  if([productStr isEqualToString:@"2"])
                {
                    cell.productTypeLbl.text = @"Product: Intraday";
                }
                
               
                for(int i=0;i<delegate.marketWatch1.count;i++)
                {
                    NSString * securityid=[NSString stringWithFormat:@"%@",[[delegate.marketWatch1 objectAtIndex:i]objectForKey:@"stSecurityID"]];

                    if([mainSecurityid isEqualToString:securityid])
                    {
                       NSString * ltp=[NSString stringWithFormat:@"%.2f",[[[delegate.marketWatch1 objectAtIndex:i]objectForKey:@"dbLTP"]floatValue]];
                        float value=[ltp floatValue] - [cell.priceLbl.text floatValue];
                        float value1=value *[cell.qtyTypeLbl.text intValue];
                        cell.openLbl.text=[NSString stringWithFormat:@"%.2f",value1];

                        if([cell.openLbl.text containsString:@"-"])
                        {
                             cell.openLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                        }
                        else
                        {
                             cell.openLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                        }



                    }
                }
                
                
//                NSString * string1=[[un_array objectAtIndex:indexPath.row] objectForKey:@"btSide"];
//
//                int orderType1=[string1 intValue];
//
//                if(orderType1==1)
//                {
//                    string1=@"BUY";
//                }
//
//                else if(orderType1==2)
//                {
//                    string1=@"SELL";
//                }
                
                if([cell.qtyTypeLbl.text isEqualToString:@"0"])
                {
                    cell.transactionTypeLbl.text=@"B/S";
                    cell.transactionTypeLbl.layer.borderColor = [UIColor colorWithRed:2.0/255.0 green:30.0/255.0 blue:41.0/255.0 alpha:1.0].CGColor;
                    cell.transactionTypeLbl.textColor =[UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0] ;
                }
                
               else  if([cell.qtyTypeLbl.text containsString:@"-"])
                {
                    cell.transactionTypeLbl.text=@"SELL";
                    cell.transactionTypeLbl.layer.borderColor = sellColor.CGColor;
                    cell.transactionTypeLbl.textColor = sellColor;
                    
                }
                
                else
                {
                    cell.transactionTypeLbl.text=@"BUY";
                    cell.transactionTypeLbl.layer.borderColor = buyColor.CGColor;
                    cell.transactionTypeLbl.textColor = buyColor;
                    
                    
                }
                    
                
//                }
                
                
                
              
                
            
               
                
           
              
                
                //NSLog(@"%@",cell.segmentLbl.text);
                
                
                [cell.squareoffBtn addTarget:self action:@selector(squareoffBtnAction:) forControlEvents:UIControlEventTouchUpInside];
             
                
                
                if([cell.qtyTypeLbl.text isEqualToString:@"0"])
                {
                    [cell.squareoffBtn setTitle:@"FLAT" forState:UIControlStateNormal];
                    float value1=[[[newMtPositionsArray objectAtIndex:indexPath.row] objectForKey:@"sellaverageprice"]floatValue]-[[[newMtPositionsArray objectAtIndex:indexPath.row] objectForKey:@"buyaverageprice"]floatValue];
                    cell.openLbl.text=[NSString stringWithFormat:@"%.2f",value1];
                     cell.priceLbl.text=[NSString stringWithFormat:@"%.2f",[[[newMtPositionsArray objectAtIndex:indexPath.row] objectForKey:@"average_price"]floatValue]];
                    if([cell.openLbl.text containsString:@"-"])
                    {
                        cell.openLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                    }
                    else
                    {
                        cell.openLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                    }
                }
                
                else
                {
                    [cell.squareoffBtn setTitle:@"SQUARE OFF" forState:UIControlStateNormal];
                    
                }
                
                

                   return cell;
            }
            
        }
        
        
        else if(tableView==self.dummyTableView)
        {
            
            DummyTableCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            
            
            cell.companyNameLabel.text = [[[[dummyResponseArray objectForKey:@"data"]  objectAtIndex:indexPath.row] objectForKey:@"contra_advice"]objectForKey:@"companyname"];
            NSString * firstName=[NSString stringWithFormat:@"%@",[[[[dummyResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"contra_advice"]objectForKey:@"firstname"]];
            NSString * lastName=[NSString stringWithFormat:@"%@",[[[[dummyResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"contra_advice"]objectForKey:@"lastname"]];
            if([lastName containsString:@"<null>"])
            {
                cell.adviceLabel.text=firstName;
            }
            else
            {
                 cell.adviceLabel.text=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
            }
          
           NSString * str = [NSString stringWithFormat:@"%@",[[[[dummyResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"contra_advice"]objectForKey:@"exitprice"]];
            if([str isEqual:[NSNull null]])
            {
                cell.LTPLabel.text = @"0";
            }else
            {
                cell.LTPLabel.text = str;
            }
            NSString * sub = [NSString stringWithFormat:@"%@",[[[[dummyResponseArray objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"contra_advice"]objectForKey:@"subscriptiontypeid"]];
            NSString * public=[NSString stringWithFormat:@"%@",[[[[dummyResponseArray objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"contra_advice"]objectForKey:@"ispublic"]];
            if([public isEqualToString:@"0"])
            {
            int subInt=[sub intValue];
            
            if(subInt==1)
            {
                cell.premiumLabel.hidden=YES;
                cell.premiumImageView.hidden=YES;
            }
            
            else if(subInt==2)
            {
                cell.premiumLabel.hidden=NO;
                cell.premiumImageView.hidden=NO;
                 cell.premiumImageView.image=[UIImage imageNamed:@"premiumNew"];
            }
            }
            else if([public isEqualToString:@"1"])
            {
                 cell.premiumImageView.hidden=NO;
                cell.premiumImageView.image=[UIImage imageNamed:@"publicnew"];
            }
            NSString * dateStr=[NSString stringWithFormat:@"%@",[[[[dummyResponseArray objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"contra_advice"]objectForKey:@"createdon"]];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
            NSDate *date = [dateFormatter dateFromString:dateStr]; // create date from string
            
            // change to a readable time format and change to local time zone
            [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            NSString *finalDate = [dateFormatter stringFromDate:date];
            cell.dateTimeLbl.text=finalDate;
            
            NSString * entryPrice = [NSString stringWithFormat:@"%@",[[[[dummyResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"contra_advice"]objectForKey:@"entryprice"]];
            
            if([entryPrice isEqual:[NSNull null]])
            {
                cell.priceLabel.text = @"0";
            }else
            {
                cell.priceLabel.text = entryPrice;
            }
            cell.NSECashLabel.text = [[[[dummyResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"segmenttype"];
            int buySell = [[[[[dummyResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"contra_advice"]objectForKey:@"buysell"] intValue];
            if(buySell==1)
            {
                cell.boughtLabel.text = @"BOUGHT";
            }else if(buySell==2)
            {
                cell.boughtLabel.text = @"SOLD";
            }
            
            NSString * profitAndLoss =[NSString stringWithFormat:@"%@",[[[[dummyResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"contra_advice"] objectForKey:@"profitloss"]];
            
            if([profitAndLoss isEqual:[NSNull null]])
            {
                cell.profitAndLossLabel.text= @"0";
            }else
            {
                
                if([profitAndLoss containsString:@"-"])
                {
                    cell.profitAndLossLabel.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                }
                
                else
                {
                    cell.profitAndLossLabel.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                    
                }
                cell.profitAndLossLabel.text=profitAndLoss;
            }
            
            
            
            
            return cell;
        }
        
        else if(tableView==self.dematTableView)
        {
            
            
            
            DematCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            if([delegate.brokerNameStr isEqualToString:@"Zerodha"])
            {
            if([[holdingDictionary objectForKey:@"data"]count]>0)
            {
                cell.avgPurchaseLbl.hidden=NO;
                cell.avgPriceLbl.hidden=NO;
                cell.companyLabel.text= [NSString stringWithFormat:@"%@",[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"tradingsymbol"]];
                cell.QTYLabel.text=[NSString stringWithFormat:@"%@",[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"quantity"]];
                cell.LPTLabel.text=[NSString stringWithFormat:@"%.2f",[[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"last_price"] floatValue]];
                
                NSString * local=[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"average_price"];
                float localFloat=[local floatValue];
                cell.avgPriceLbl.text=[NSString stringWithFormat:@"%.2f",localFloat];
                float value=[[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"last_price"]floatValue]* [[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"quantity"]floatValue];
                
                //
                cell.valueLabel.text=[NSString stringWithFormat:@"%.2f",value];
                cell.exchaneLbl.text=[NSString stringWithFormat:@"%@",[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"exchange"]];
                
                [cell.tradeBtn addTarget:self action:@selector(tradeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
                 return cell;
                
            }
                
            }else if ([delegate.brokerNameStr isEqualToString:@"Upstox"])
            {
                cell.avgPurchaseLbl.hidden=NO;
                cell.avgPriceLbl.hidden=NO;
                cell.companyLabel.text = [NSString stringWithFormat:@"%@",[[[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"instrument"]objectAtIndex:0]objectForKey:@"symbol"]];
                cell.QTYLabel.text=[NSString stringWithFormat:@"%@",[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"quantity"]];
                cell.LPTLabel.text=@"0.0";
                NSString * local=[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"avg_price"];
                float localFloat=[local floatValue];
                cell.avgPriceLbl.text=[NSString stringWithFormat:@"%.2f",localFloat];
                cell.LPTLabel.text=@"0.00";
                cell.valueLabel.text=@"0.0";
                @try {
                if(upstoxFeedArray.count>0)
                {
                    for(int i=0;i<upstoxFeedArray.count;i++)
                    {
                        NSString * symbol1=[[upstoxFeedArray objectAtIndex:i] objectForKey:@"symbol"];
                         NSString * symbol2=cell.companyLabel.text;
                     if([symbol1 isEqualToString:symbol2])
                     {
                    NSString * ltp=[[upstoxFeedArray objectAtIndex:i] objectForKey:@"ltp"];
                    cell.LPTLabel.text=ltp;
                    float ltpFloat=[ltp floatValue];
                    
                    float value=ltpFloat* [[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"quantity"]floatValue];
                     cell.valueLabel.text=[NSString stringWithFormat:@"%.2f",value];
                     }
                    }
                }
                    
                }
                @catch (NSException * e) {
                    //NSLog(@"Exception: %@", e);
                    cell.LPTLabel.text=@"0.00";
                    cell.valueLabel.text=@"0.0";
                }
                @finally {
                    //NSLog(@"finally");
                }
                
                //
                
                cell.exchaneLbl.text=[NSString stringWithFormat:@"%@",[[[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"instrument"]objectAtIndex:0]objectForKey:@"exchange"]];
                
                [cell.tradeBtn addTarget:self action:@selector(tradeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
                return cell;
                }
            
            
            else
            {
                if(mtHoldingsArray.count>0)
                {
                    if([delegate.brokerNameStr isEqualToString:@"Proficient"])
                    {
                    cell.avgPurchaseLbl.hidden=NO;
                    cell.avgPriceLbl.hidden=NO;
                        
                        cell.avgPriceLbl.text = [NSString stringWithFormat:@"%.2f",[[[mtHoldingsArray objectAtIndex:indexPath.row] objectForKey:@"avgprice"]floatValue]];
                    }
                    else
                    {
                    cell.avgPurchaseLbl.hidden=YES;
                    cell.avgPriceLbl.hidden=YES;
                        
                    }
                cell.companyLabel.text=[[mtHoldingsArray objectAtIndex:indexPath.row] objectForKey:@"tradingsymbol"];
                cell.QTYLabel.text=[NSString stringWithFormat:@"%@",[[mtHoldingsArray objectAtIndex:indexPath.row] objectForKey:@"quantity"]];
                cell.exchaneLbl.text=[[mtHoldingsArray objectAtIndex:indexPath.row] objectForKey:@"exchange"];
                cell.LPTLabel.text=@"0.00";
                cell.valueLabel.text=@"0.00";
                
                  NSString * mainSecurityid=[NSString stringWithFormat:@"%@",[[mtHoldingsArray objectAtIndex:indexPath.row] objectForKey:@"exchangetoken"]];
                    for(int i=0;i<delegate.marketWatch1.count;i++)
                    {
                        NSString * securityid=[NSString stringWithFormat:@"%@",[[delegate.marketWatch1 objectAtIndex:i]objectForKey:@"stSecurityID"]];
                        
                        if([mainSecurityid isEqualToString:securityid])
                        {
                            cell.LPTLabel.text=[NSString stringWithFormat:@"%.2f",[[[delegate.marketWatch1 objectAtIndex:i]objectForKey:@"dbLTP"]floatValue]];
                            float value=[cell.LPTLabel.text floatValue] * [cell.QTYLabel.text floatValue];
                            
                            cell.valueLabel.text=[NSString stringWithFormat:@"%.2f",value];
                            
                        }
                    }
                    
                    
                    
                [cell.tradeBtn addTarget:self action:@selector(tradeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
                return cell;
                }
                

            }

        
    }
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
            
        
    
    


    
    
   
    
}

- (void)tableView: (UITableView*)tableView willDisplayCell: (UITableViewCell*)cell forRowAtIndexPath: (NSIndexPath*)indexPath
{
    
    
//    if(indexPath.row % 2 == 0)
//        
//        cell.backgroundColor = [UIColor colorWithRed:(245/255.0) green:(245/255.0) blue:(245/255.0) alpha:1];
//    else
//    {
//        cell.backgroundColor = [UIColor whiteColor];
//        
//    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==self.positionsTableView)
    {
         return 110;
    }
    else if (tableView==self.dematTableView)
    {
         return 90;
    }
    else if(tableView==self.dummyTableView)
    {
        return 109;
    }
    return 0;
   
}

-(void)tradeBtnAction:(UIButton*)sender
{
    
    delegate.navigationCheck=@"TOP";
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.dematTableView];
    
    delegate.holdingCheck=@"hold";
    
    NSIndexPath *indexPath = [self.dematTableView indexPathForRowAtPoint:buttonPosition];
   DematCell *cell = [self.dematTableView cellForRowAtIndexPath:indexPath];
    
    @try {
        
        if([delegate.brokerNameStr isEqualToString:@"Zerodha"])
        {
        delegate.orderSegment=[NSString stringWithFormat:@"%@",[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"exchange"]];
        
        delegate.symbolDepthStr=[NSString stringWithFormat:@"%@",[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"tradingsymbol"]];
        
        delegate.holdingsQty=[NSString stringWithFormat:@"%@",[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"quantity"]];
        
        
       
            
            
        }else if ([delegate.brokerNameStr isEqualToString:@"Upstox"])
        {
            delegate.orderSegment=[NSString stringWithFormat:@"%@",cell.exchaneLbl.text];
            
            delegate.symbolDepthStr=[NSString stringWithFormat:@"%@",cell.companyLabel.text];
            
            delegate.holdingsQty=[NSString stringWithFormat:@"%@",cell.QTYLabel.text];
            
            
            delegate.orderinstrument=[NSString stringWithFormat:@"%@",[[[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"instrument"]objectAtIndex:0]objectForKey:@"token"]];
        }
        
        
        else
        {
            
            
            delegate.orderSegment=[NSString stringWithFormat:@"%@",cell.exchaneLbl.text];
            
            delegate.symbolDepthStr=[NSString stringWithFormat:@"%@",cell.companyLabel.text];
            
            delegate.holdingsQty=[NSString stringWithFormat:@"%@",cell.QTYLabel.text];
            
           
            delegate.orderinstrument=[[mtHoldingsArray objectAtIndex:indexPath.row]objectForKey:@"exchangetoken"];
            
            
        }
        
        
        if(delegate.orderSegment.length>0 && delegate.symbolDepthStr.length>0)
        {
            
            StockOrderView * stockOrder=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
            delegate.orderBool=true;
            delegate.portfolioBackBool=true;
              [self.navigationController pushViewController:stockOrder animated:YES];
//            [self presentViewController:stockOrder animated:self completion:nil];
        }
        
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
  }

-(void)squareoffBtnAction:(UIButton*)sender
{
    @try
    {
    delegate.holdingCheck=@"position";
     delegate.navigationCheck=@"TOP";
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.positionsTableView];
    
    
    
    NSIndexPath *indexPath = [self.positionsTableView indexPathForRowAtPoint:buttonPosition];
    //    FollowCell *cell = [self.followerTableView cellForRowAtIndexPath:indexPath];
    
    
   if([delegate.brokerNameStr isEqualToString:@"Zerodha"])
   {
    PortfolioCell  *cell = [self.positionsTableView cellForRowAtIndexPath:indexPath];
       delegate.orderSegment=[NSString stringWithFormat:@"%@",[[[[positionDictionary objectForKey:@"data"]objectForKey:@"net" ] objectAtIndex:indexPath.row]objectForKey:@"exchange"]];
       
       delegate.symbolDepthStr=[NSString stringWithFormat:@"%@",[[[[positionDictionary objectForKey:@"data"]objectForKey:@"net"] objectAtIndex:indexPath.row]objectForKey:@"tradingsymbol"]];
       
      NSString * string=[NSString stringWithFormat:@"%@",[[[[positionDictionary objectForKey:@"data"]objectForKey:@"net" ] objectAtIndex:indexPath.row]objectForKey:@"quantity"]];
       
       if([string containsString:@"-"])
       {
           
           delegate.holdingsQty=[string stringByReplacingOccurrencesOfString:@"-" withString:@""];
       }
       
       else
       {
           delegate.holdingsQty=string;
       }
       
       
       delegate.POSITIONtransctionType=cell.transactionTypeLbl.text;
   }else if([delegate.brokerNameStr isEqualToString:@"Upstox"])
   {
       PortfolioCell  *cell = [self.positionsTableView cellForRowAtIndexPath:indexPath];
       delegate.orderSegment=[NSString stringWithFormat:@"%@",[[[positionDictionary objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"exchange"]];
       
       delegate.symbolDepthStr=[NSString stringWithFormat:@"%@",[[[positionDictionary objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"symbol"]];
       
       NSString * string=[NSString stringWithFormat:@"%@",[[[positionDictionary objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"net_quantity"]];
       
       if([string containsString:@"-"])
       {
           
           delegate.holdingsQty=[string stringByReplacingOccurrencesOfString:@"-" withString:@""];
       }
       
       else
       {
           delegate.holdingsQty=string;
       }
       
       
       
       delegate.POSITIONtransctionType=cell.transactionTypeLbl.text;
   }
    
    else
    {
        PortfolioCell  *cell = [self.positionsTableView cellForRowAtIndexPath:indexPath];
        delegate.orderSegment=[NSString stringWithFormat:@"%@",[[newMtPositionsArray objectAtIndex:indexPath.row] objectForKey:@"exchange"]];
        
        delegate.symbolDepthStr=cell.companyName.text;
        
        NSString * string=[NSString stringWithFormat:@"%@",[[newMtPositionsArray objectAtIndex:indexPath.row] objectForKey:@"quantity"]];;
        if([string containsString:@"-"])
        {
            
            delegate.holdingsQty=[string stringByReplacingOccurrencesOfString:@"-" withString:@""];
        }
        
        else
        {
            delegate.holdingsQty=string;
        }
        
       
        
        
        delegate.orderinstrument=[NSString stringWithFormat:@"%@",[[newMtPositionsArray objectAtIndex:indexPath.row] objectForKey:@"exchangetoken"]];
        
        delegate.POSITIONtransctionType=cell.transactionTypeLbl.text;
    }
    
     @try {
   
    
    
    if(delegate.orderSegment.length>0 && delegate.symbolDepthStr.length>0)
    {
        if([delegate.holdingsQty isEqualToString:@"0"])
        {
                dispatch_async(dispatch_get_main_queue(), ^{




                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Squared off" message:@"Your position is already squared" preferredStyle:UIAlertControllerStyleAlert];



                    [self presentViewController:alert animated:YES completion:^{



                    }];



                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {



                    }];



                    [alert addAction:okAction];

                });
        }
        
        else
        {
        StockOrderView * stockOrder=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
        delegate.orderBool=true;
        delegate.portfolioBackBool=true;
            
            [self.navigationController pushViewController:stockOrder animated:YES];
//
//        [self presentViewController:stockOrder animated:self completion:nil];
            
        }
    }
         
     }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}


//reachability//

-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    
    
}

-(BOOL)networkStatus
{
    
    
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    
    if(netStatus==NotReachable)
    {
        
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    return YES;
    
    
}









-(void)mtPositionsRequest
{
    @try
    {
    securityTokenArray=[[NSMutableArray alloc]init];
    mtExchangeArray=[[NSMutableArray alloc]init];
    self.noPortfolioView.hidden=NO;
    delegate.scripsMt=[[NSMutableArray alloc]init];
    self.dummyPotfolioView.hidden=YES;
    self.dummyBottomView.hidden=YES;
 
    self.dematView.hidden=YES;
    
    delegate.mtpositionsArray=[[NSMutableArray alloc]init];
    exchangeTokenArray=[[NSMutableArray alloc]init];
   
    delegate.allTradeArray=[[NSMutableArray alloc]init];
//    TagEncode * tag = [[TagEncode alloc]init];
//    delegate.mtCheck=true;
//    [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.OrderDownloadRequest];
//    [tag GetBuffer];
//
//    [self newMessage];//     [[NSNotificationCenter defaultCenter] postNotificationName:@"ordertowatch" object:nil];
    
    
//    delegate.allTradeArray=[[NSMutableArray alloc]init];
//    tag = [[TagEncode alloc]init];
//    delegate.mtCheck=true;
//    [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.TradeDownloadRequest];
//    [tag GetBuffer];
//
//    [self newMessage];
        Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
        
        [mixpanelMini identify:delegate.userID];
        
    
    TagEncode * enocde=[[TagEncode alloc]init];
    mtHoldingsArray=[[NSMutableArray alloc]init];
    enocde.inputRequestString=@"trade";
    
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    delegate.marketWatch1=[[NSMutableArray alloc]init];
    [inputArray addObject:delegate.accessToken];
    [inputArray addObject:delegate.userID];
    [inputArray addObject:[NSString stringWithFormat:@"%@multitradeapi/position",delegate.mtBaseUrl]];
    
    newMtPositionsArray=[[NSMutableArray alloc]init];
    [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        NSArray * keysArray=[dict allKeys];
        
        
        if(keysArray.count>0)
        {
        newMtPositionsArray=[[[dict objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"positions"];
          dispatch_async(dispatch_get_main_queue(), ^{
              if(newMtPositionsArray.count>0)
              {
        [self.activityInd stopAnimating];
        self.activityInd.hidden=YES;
        self.positionsTableView.hidden=NO;
        self.noPortfolioView.hidden=YES;
        self.postionView.hidden=NO;
        self.positionsTableView.delegate=self;
        self.positionsTableView.dataSource=self;
        [self.positionsTableView reloadData];
                   }
          });
        if(newMtPositionsArray.count>0)
        {
        [self broadCast:newMtPositionsArray];
            
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"YYYY-MM-DD HH:MM:SS"];
            NSMutableArray * positionsArray = [[NSMutableArray alloc]init];
            for (int i=0; i<newMtPositionsArray.count; i++) {
                NSMutableDictionary * holdingsDictionary = [[NSMutableDictionary alloc]init];
                [holdingsDictionary setObject:[NSString stringWithFormat:@"%@",[[newMtPositionsArray objectAtIndex:i] objectForKey:@"tradingsymbol"]] forKey:@"symbol_name"];
                [holdingsDictionary setObject:[NSString stringWithFormat:@"%@",[[newMtPositionsArray objectAtIndex:i] objectForKey:@"quantity"]] forKey:@"quantity"];
                [holdingsDictionary setObject:[NSString stringWithFormat:@"%@",@"0.0"] forKey:@"avg_price"];
                [holdingsDictionary setObject:[dateFormatter stringFromDate:[NSDate date]] forKey:@"date_time"];
                [positionsArray addObject:holdingsDictionary];
            }
        
        [mixpanelMini.people set:@{@"positionslist":positionsArray}];
         }
         
        
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                
                UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self mtPositionsRequest];
                }];
                UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                
                [alert addAction:retryAction];
                [alert addAction:cancel];
                
            });
            
        }
    }];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

}

-(void)mtHoldingsRequest
{
    @try
    {
        Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
        
        [mixpanelMini identify:delegate.userID];
        
    self.noPortfolioView.hidden=NO;
    self.dummyPotfolioView.hidden=YES;
    self.dummyBottomView.hidden=YES;
    self.postionView.hidden=YES;
   
    TagEncode * enocde=[[TagEncode alloc]init];
    mtHoldingsArray=[[NSMutableArray alloc]init];
    enocde.inputRequestString=@"trade";
    
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    delegate.marketWatch1=[[NSMutableArray alloc]init];
    [inputArray addObject:delegate.accessToken];
     [inputArray addObject:delegate.userID];
    [inputArray addObject:[NSString stringWithFormat:@"%@multitradeapi/holdingrequest",delegate.mtBaseUrl]];
    
    
    [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        NSArray * keysArray=[dict allKeys];
        
        
        if(keysArray.count>0)
        {
        mtHoldingsArray=[[[dict objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"holdings"];
         dispatch_async(dispatch_get_main_queue(), ^{
             if(mtHoldingsArray.count>0)
             {
        [self.activityInd stopAnimating];
        self.activityInd.hidden=YES;
        self.dematTableView.hidden=NO;
        self.noPortfolioView.hidden=YES;
        self.dematView.hidden=NO;
        self.dematTableView.delegate=self;
        self.dematTableView.dataSource=self;
        [self.dematTableView reloadData];
             }
             });
        if(mtHoldingsArray.count>0)
        {
        [self broadCast:mtHoldingsArray];
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"YYYY-MM-DD HH:MM:SS"];
            NSMutableArray * holdingsArray = [[NSMutableArray alloc]init];
            for (int i=0; i<mtHoldingsArray.count; i++) {
                NSMutableDictionary * holdingsDictionary = [[NSMutableDictionary alloc]init];
                [holdingsDictionary setObject:[NSString stringWithFormat:@"%@",[[mtHoldingsArray objectAtIndex:i] objectForKey:@"tradingsymbol"]] forKey:@"symbol_name"];
                [holdingsDictionary setObject:[NSString stringWithFormat:@"%@",[[mtHoldingsArray objectAtIndex:i] objectForKey:@"quantity"]] forKey:@"quantity"];
                [holdingsDictionary setObject:[NSString stringWithFormat:@"%@",@"0.0"] forKey:@"avg_price"];
                [holdingsDictionary setObject:[dateFormatter stringFromDate:[NSDate date]] forKey:@"date_time"];
                [holdingsArray addObject:holdingsDictionary];
            }
        [mixpanelMini.people set:@{@"holdingslist":holdingsArray}];
        }
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                
                UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self mtHoldingsRequest];
                }];
                UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                
                [alert addAction:retryAction];
                [alert addAction:cancel];
                
            });
            
        }
        
    }];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
-(void)broadCast:(NSMutableArray *)dataArray
{
    @try {
        //            instrumentTokensDict = [[NSMutableDictionary alloc]init];
        delegate.marketWatch1=[[NSMutableArray alloc]init];
      
      //  NSURL* url = [[NSURL alloc] initWithString:@"http://13.126.147.95:8012"];
        NSURL* url = [[NSURL alloc] initWithString:delegate.ltpServerURL];
        self.manager = [[SocketManager alloc] initWithSocketURL:url config:@{@"log": @YES}];
        
        self.socketio=self.manager.defaultSocket;
        
        [self.socketio on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
            //NSLog(@"socket connected");
            
            if(segmentedControl.selectedSegmentIndex==1)
            {
                for(int i=0;i<newMtPositionsArray.count;i++)
                {
                    NSMutableArray * subscriptionList=[[NSMutableArray alloc]init];
                    NSMutableDictionary * tmpDict = [[NSMutableDictionary alloc]init];
                    
                    [tmpDict setValue:[[newMtPositionsArray objectAtIndex:i] objectForKey:@"exchangetoken"] forKey:@"securityToken"];
                    [tmpDict setValue:[[newMtPositionsArray objectAtIndex:i] objectForKey:@"exchange"] forKey:@"exchange"];
                    [subscriptionList addObject:tmpDict];
                    
                    [self.socketio emit:@"symbol" with:subscriptionList];
                       [self.socketio emit:@"initltp" with:subscriptionList];
                    
                }
                
            }
            
           else  if(segmentedControl.selectedSegmentIndex==2)
            {
                for(int i=0;i<mtHoldingsArray.count;i++)
                {
                    NSMutableArray * subscriptionList=[[NSMutableArray alloc]init];
                    NSMutableDictionary * tmpDict = [[NSMutableDictionary alloc]init];
                    
                    [tmpDict setValue:[[mtHoldingsArray objectAtIndex:i] objectForKey:@"exchangetoken"] forKey:@"securityToken"];
                    [tmpDict setValue:[[mtHoldingsArray objectAtIndex:i] objectForKey:@"exchange"] forKey:@"exchange"];
                    [subscriptionList addObject:tmpDict];
                    
                    [self.socketio emit:@"symbol" with:subscriptionList];
                    [self.socketio emit:@"initltp" with:subscriptionList];
                    
                    
                }
                
            }
           
            
        }];
        [self.socketio on:@"heartbeat" callback:^(NSArray* data, SocketAckEmitter* ack) {
            //NSLog(@"%@",data);
            
            
            
            
        }];
        [self.socketio on:@"initltp" callback:^(NSArray* data, SocketAckEmitter* ack) {
            //NSLog(@"%@",data);
            
            NSString * newString=[NSString stringWithFormat:@"%@",[[data objectAtIndex:0] objectForKey:@"stSecurityID"]];
            if(delegate.marketWatch1.count>0)
            {
                for(int i=0;i<delegate.marketWatch1.count;i++)
                {
                    NSString * localStr=[NSString stringWithFormat:@"%@",[[delegate.marketWatch1 objectAtIndex:i] objectForKey:@"stSecurityID"]];
                    
                    if([localStr containsString:newString]||[newString containsString:localStr])
                    {
                        exchangeTokenCheck=true;
                        [delegate.marketWatch1 replaceObjectAtIndex:i withObject:[data objectAtIndex:0]];
                    }
                    
                    
                    
                }
                
                if(exchangeTokenCheck==true)
                {
                    
                    exchangeTokenCheck=false;
                }
                
                else
                {
                    [delegate.marketWatch1 addObject:[data objectAtIndex:0]];
                }
                
                
                
                
                
            }
            
            else
            {
                [delegate.marketWatch1 addObject:[data objectAtIndex:0]];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(segmentedControl.selectedSegmentIndex==1)
                {
                    [self.activityInd stopAnimating];
                    self.activityInd.hidden=YES;
                    self.positionsTableView.hidden=NO;
                    self.noPortfolioView.hidden=YES;
                    self.postionView.hidden=NO;
                    self.positionsTableView.delegate=self;
                    self.positionsTableView.dataSource=self;
                    [self.positionsTableView reloadData];
                }
                
                else if(segmentedControl.selectedSegmentIndex==2)
                {
                    [self.activityInd stopAnimating];
                    self.activityInd.hidden=YES;
                    self.dematTableView.hidden=NO;
                    self.noPortfolioView.hidden=YES;
                    self.dematView.hidden=NO;
                    self.dematTableView.delegate=self;
                    self.dematTableView.dataSource=self;
                    [self.dematTableView reloadData];
                }
                
            });
            
            
        }];
        
        
        [self.socketio on:@"ltp" callback:^(NSArray* data, SocketAckEmitter* ack) {
            //NSLog(@"%@",data);
           
            NSString * newString=[NSString stringWithFormat:@"%@",[[data objectAtIndex:0] objectForKey:@"stSecurityID"]];
            if(delegate.marketWatch1.count>0)
            {
                for(int i=0;i<delegate.marketWatch1.count;i++)
                {
                    NSString * localStr=[NSString stringWithFormat:@"%@",[[delegate.marketWatch1 objectAtIndex:i] objectForKey:@"stSecurityID"]];
                    
                    if([localStr containsString:newString]||[newString containsString:localStr])
                    {
                        exchangeTokenCheck=true;
                        [delegate.marketWatch1 replaceObjectAtIndex:i withObject:[data objectAtIndex:0]];
                    }
                    
                    
                    
                }
                
                if(exchangeTokenCheck==true)
                {
                    
                    exchangeTokenCheck=false;
                }
                
                else
                {
                    [delegate.marketWatch1 addObject:[data objectAtIndex:0]];
                }
                
                
                
                
                
            }
            
            else
            {
                [delegate.marketWatch1 addObject:[data objectAtIndex:0]];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(segmentedControl.selectedSegmentIndex==1)
                {
                    [self.activityInd stopAnimating];
                    self.activityInd.hidden=YES;
                    self.positionsTableView.hidden=NO;
                    self.noPortfolioView.hidden=YES;
                    self.postionView.hidden=NO;
                    self.positionsTableView.delegate=self;
                    self.positionsTableView.dataSource=self;
                    [self.positionsTableView reloadData];
                }
                
                else if(segmentedControl.selectedSegmentIndex==2)
                {
                    [self.activityInd stopAnimating];
                    self.activityInd.hidden=YES;
                    self.dematTableView.hidden=NO;
                    self.noPortfolioView.hidden=YES;
                    self.dematView.hidden=NO;
                    self.dematTableView.delegate=self;
                    self.dematTableView.dataSource=self;
                    [self.dematTableView reloadData];
                }
               
            });
            
            
        }];
        
        [self.socketio connect];
      
        
       
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (void)viewWillDisappear:(BOOL)animated;
{
    delegate.holdingsMt=false;
    
    delegate.wisdomBool=false;
    delegate.wisdomDuration=@"";
    delegate.wisdomSearchSymbol=@"";
    delegate.wisdomFromStr=@"";
    delegate.wisdomaToStr=@"";
    [self.client disconnectWithCompletionHandler:nil];
    [self.socketio disconnect];
}

-(void)upstoxHoldingFeed
{
    upstoxFeedArray=[[NSMutableArray alloc]init];
    for(int i=0;i<[[holdingDictionary objectForKey:@"data"]count];i++)
    {
        NSString * exchange=[NSString stringWithFormat:@"%@",[[[[[holdingDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"instrument"]objectAtIndex:0]objectForKey:@"exchange"]];
        NSString * symbol=[NSString stringWithFormat:@"%@",[[[[[holdingDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"instrument"]objectAtIndex:0]objectForKey:@"symbol"]];
        NSURL *  url1 = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.upstox.com/live/feed/now/%@/%@/ltp",exchange,symbol]];
        NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate.accessToken];
        NSDictionary * headers  = @{ @"cache-control": @"no-cache",
                  @"Content-Type" : @"application/json",
                  @"authorization":access,
                  @"x-api-key":delegate.APIKey
                  };
    
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url1
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:40.0];
    
       [request setHTTPMethod:@"GET"];
    
       [request setAllHTTPHeaderFields:headers];
    
       NSURLSession *session = [NSURLSession sharedSession];
       NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if(data != nil)
                                                    {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            upstoxFeed=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            NSString * ltp=[NSString stringWithFormat:@"%.2f",[[[upstoxFeed objectForKey:@"data"]objectForKey:@"ltp"]floatValue]];
                                                            NSString * symbol=[NSString stringWithFormat:@"%@",[[upstoxFeed objectForKey:@"data"]objectForKey:@"symbol"]];
                                                            NSDictionary * local=@{@"ltp":ltp,@"symbol":symbol};
                                                            [upstoxFeedArray addObject:local];
                                                            
                                                            
                                                        }
                                                            dispatch_async(dispatch_get_main_queue(),
                                                                           ^{
                                                                               if(upstoxFeedArray.count==[[holdingDictionary objectForKey:@"data"]count])
                                                                               {
                                                                             
                                                                               [self.dematTableView reloadData];
                                                            
                                                                               }
                                                                           });
                                                        
                                                    }
                                                    else
                                                    {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                          
                                                            
                                                        });
                                                        
                                                        
                                                    }
                                                }];
    [dataTask resume];
    
                                                                           }
    
}


@end
