//
//  WalletView.h
//  NewWallet
//
//  Created by Zenwise Technologies on 27/06/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WalletView : UIViewController<UITextFieldDelegate>
- (IBAction)onBackBtnTap:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *TimerView;
@property (weak, nonatomic) IBOutlet UIButton *shareBtn;
@property (weak, nonatomic) IBOutlet UIView *recordedView;
- (IBAction)onShareBtnTap:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *referalCodeLbl;
@property (weak, nonatomic) IBOutlet UIView *ques1View;
@property (weak, nonatomic) IBOutlet UILabel *ques1Lbl;
@property (weak, nonatomic) IBOutlet UITextField *qus1Fld;
- (IBAction)subumitBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *yesBtn;
@property (weak, nonatomic) IBOutlet UIButton *noBtn;
@property (weak, nonatomic) IBOutlet UIView *ques2View;
- (IBAction)onyesBtnTap:(id)sender;
- (IBAction)onNoBtnTap:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *walletPointsView;
@property (weak, nonatomic) IBOutlet UIView *walletBottomView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *walletbottomViewHght;
@property (weak, nonatomic) IBOutlet UIButton *redeemBtn;
- (IBAction)onRedeembtnTap:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *historyTableView;
@property (weak, nonatomic) IBOutlet UILabel *timerLbl;
@property  NSDate * expiryDate;;
@property (weak, nonatomic) IBOutlet UILabel *timeMessageLbl;
@property (weak, nonatomic) IBOutlet UIView *resultsView;
@property (weak, nonatomic) IBOutlet UIImageView *resultsCupImg;
@property (weak, nonatomic) IBOutlet UILabel *pointsEarnedLbl;
@property (weak, nonatomic) IBOutlet UITextView *timerTextview;
@property (weak, nonatomic) IBOutlet UILabel *tapToRevealLbl;
@property (weak, nonatomic) IBOutlet UIView *timerView;
@property (weak, nonatomic) IBOutlet UILabel *resultsannounceTimeLbl;
@property (weak, nonatomic) IBOutlet UIView *inviteView;

@property (weak, nonatomic) IBOutlet UILabel *rewardPoints;
@property (weak, nonatomic) IBOutlet UICollectionView *earnPointsCollection;
@property (weak, nonatomic) IBOutlet UIImageView *timerBg;
@property (weak, nonatomic) IBOutlet UILabel *pointsCreditedLbl;
@property (weak, nonatomic) IBOutlet UIView *timerSubView;
@property (weak, nonatomic) IBOutlet UILabel *walletPointsLbl;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UILabel *recordLbl;
@property (weak, nonatomic) IBOutlet UIImageView *resultsBgImg;
@property (weak, nonatomic) IBOutlet UIButton *historyButton;
@property (weak, nonatomic) IBOutlet UILabel *redeemLbl;
@property NSString *apiKeyVoucher;
@property (weak, nonatomic) IBOutlet UIView *popUpView;
@property (nonatomic,retain)NSMutableArray *historyArray;
@property (weak, nonatomic) IBOutlet UIView *thankYouBGView;
@property (weak, nonatomic) IBOutlet UIView *emailView;
@property (weak, nonatomic) IBOutlet UIButton *verifiedButton;
@property (weak, nonatomic) IBOutlet UIButton *emailModifyButton;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UIView *updateView;
@property (weak, nonatomic) IBOutlet UITextField *updateEmailIDTF;
@property (weak, nonatomic) IBOutlet UIButton *requestModifyButton;
@property NSString * navigationCheck;
@end
