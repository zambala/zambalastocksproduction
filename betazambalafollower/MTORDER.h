//
//  MTORDER.h
//  
//
//  Created by zenwise technologies on 27/07/17.
//
//

#import <Foundation/Foundation.h>

@interface MTORDER : NSObject


@property int inOrderSeqNo;
@property NSString *stUserID;
@property int inATINOrderID;
@property int inOrderID;
@property int inExchangeClientOrderID;
@property NSString *stExchangeOrderID;
@property NSString *stOrderTime;
@property NSString *stExchangeOrderTime;
@property NSString *stExchange;
@property NSString *stSecurityID;
@property NSString *stSecurityType;
@property NSString *stSymbol;
@property NSString *stExpiryDate;
@property NSString *stOptionType;
@property double dbStrikePrice;
@property NSString *stOrderExpiryDateTime;
@property NSString *stSide;
@property int inQty;
@property int inPendingQty;
@property int inExeQty;
@property int inDiscloseQty;
@property double dbPrice;
@property double dbTriggerPrice;
@property NSString *stClientID;
@property NSString *stMemberID;
@property NSString *stTraderID;
@property NSString *stCTCLID;
@property NSString *stOrderType;
@property NSString *stOrderSituation;
@property NSString *stOrderStatus;
@property NSString *stTimeinForce;
@property NSString *stErrorText;
@property short shErrorCode;
@property int inOrderCount;
@property NSString *stTerminalInfo;
@property NSString *stRefText;
+ (id)Mt2;


@end
