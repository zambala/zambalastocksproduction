//
//  EarnPointsCell.m
//  betazambalafollower
//
//  Created by Zenwise Technologies on 18/07/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import "EarnPointsCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation EarnPointsCell
- (void)awakeFromNib {
    [super awakeFromNib];
self.bgView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16] CGColor];
self.bgView.layer.shadowOffset = CGSizeMake(0,2);
self.bgView.layer.shadowOpacity = 1;
self.bgView.layer.shadowRadius = 1.0;
self.bgView.layer.shadowOpacity = 3.0f;
self.bgView.layer.masksToBounds = NO;
    self.bgView.layer.cornerRadius=5.0f;
    self.bgView.clipsToBounds=YES;
}
@end
