//
//  RewardHistoryTableViewCell.h
//  betazambalafollower
//
//  Created by Zenwise Technologies Private Limited on 23/08/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RewardHistoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end
