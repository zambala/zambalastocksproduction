//
//  NewSelectBrokerCollectionViewCell.h
//  NewUI
//
//  Created by Zenwise Technologies on 22/05/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewSelectBrokerCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIButton *brokerImageButton;

@property (weak, nonatomic) IBOutlet UILabel *brokerNameLabel;

@end
