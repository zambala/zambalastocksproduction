//
//  NewProfileSettings.m
//  
//
//  Created by zenwise technologies on 21/03/17.
//
//

#import "NewProfileSettings.h"
#import "HMSegmentedControl.h"
#import "TabBar.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AudioToolbox/AudioToolbox.h>
#import "NotificationTonesPopup.h"
#import "UIViewController+MJPopupViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import <Mixpanel/Mixpanel.h>
#import "UIImageView+WebCache.h"




@interface NewProfileSettings ()<UITextFieldDelegate>
{
    HMSegmentedControl *segmentedControl;
    BOOL check,check1,check2,check3,check4,check5,check6,check7,check8,check9;
    BOOL radio1,radio2,radio3,radio4,radio5,radio6,radio7,radio8,radio9;
    NSString * fileName;
    AppDelegate * delegate;
    NSString * orderType;
    NSString * quantityType;
    NSString * tickerCheck;
    NSMutableArray * audioFileList;
    UIImagePickerController * pickerController;
    NSString * verify;
    NSMutableDictionary * dict;
    NSString * adviceNotificationStr;
}

@end

@implementation NewProfileSettings

- (void)viewDidLoad {
    [super viewDidLoad];
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"profile_settings_page"];
    
    
    
  
   
    self.userIconview.layer.borderWidth=1.0f;
    self.userIconview.layer.borderColor=[[UIColor colorWithRed:(2.0/255.0) green:(30.0/255.0) blue:(41.0/255.0) alpha:1.0] CGColor];
  
    self.emailEditView.layer.borderWidth=1.0f;
    self.emailEditView.layer.borderColor=[[UIColor colorWithRed:(2.0/255.0) green:(30.0/255.0) blue:(41.0/255.0) alpha:1.0] CGColor];
   
    self.mobileEditView.layer.borderWidth=1.0f;
    self.mobileEditView.layer.borderColor=[[UIColor colorWithRed:(2.0/255.0) green:(30.0/255.0) blue:(41.0/255.0) alpha:1.0] CGColor];
   
    self.view1.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.view1.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.view1.layer.shadowOpacity = 5.0f;
    self.view1.layer.shadowRadius = 4.0f;
    self.view1.layer.cornerRadius=5.0f;
    self.view1.layer.masksToBounds = NO;
    self.view2.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.view2.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.view2.layer.shadowOpacity = 5.0f;
    self.view2.layer.shadowRadius = 4.0f;
    self.view2.layer.cornerRadius=5.0f;
    self.view2.layer.masksToBounds = NO;
    self.nofityView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.nofityView.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.nofityView.layer.shadowOpacity = 5.0f;
    self.nofityView.layer.shadowRadius = 4.0f;
    self.nofityView.layer.cornerRadius=5.0f;
    self.nofityView.layer.masksToBounds = NO;
    self.profileUpdtBtn.layer.cornerRadius=5.0f;
    self.profileUpdtBtn.layer.masksToBounds = NO;
    self.orderPreBtn.layer.cornerRadius=5.0f;
    self.orderPreBtn.layer.masksToBounds = NO;
    check=true;
    check1=true;
    check2=true;
    check3=true;
    check4=false;
    check5=false;
    check6=false;
    check7=true;
    check8=true;
    check9=true;
    radio1=true;
    self.tradeView.hidden=YES;
    NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString * appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString * versionBuildString = [NSString stringWithFormat:@"Version: %@ (%@)", appVersionString, appBuildString];
    self.versionLbl.text=versionBuildString;
//    self.scrollView.scrollEnabled=false;
    delegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    pickerController=[[UIImagePickerController alloc]init];
    pickerController.delegate=self;
    
    self.profileImgView.layer.cornerRadius=self.profileImgView.frame.size.width / 2;
    self.profileImgView.clipsToBounds = YES;
    
    self.camBtnOtlt.layer.cornerRadius=self.camBtnOtlt.frame.size.width / 2;
    self.camBtnOtlt.clipsToBounds = YES;
    self.activityInd.hidden = YES;
   
//    if([delegate.brokerNameStr isEqualToString:@"Zerodha"]||[delegate.brokerNameStr isEqualToString:@"Upstox"])
//    {
//    self.userNameFld.text=delegate.userName;
//    }
//    else
//    {
//        self.userNameFld.text=delegate.userID;
//    }
    
    self.tradeDictionary=[[NSMutableDictionary alloc]init];
    
   
    [self newprofile];
        
   
    
    [self functionalityClass];
    [self loadAudioFileList];

     dispatch_async(dispatch_get_main_queue(), ^{
    self.profileImgView.image = [UIImage imageNamed:@"dpDefault"];
     });

    
    // Do any additional setup after loading the view.
}
-(void)newprofile
{
    self.activityInd.hidden = NO;
    [self.activityInd startAnimating];
    @try {
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"cache-control": @"no-cache",
                                   @"authtoken":delegate.zenwiseToken,
                                    @"deviceid":delegate.currentDeviceId
                                   };
        
        
        NSString * string=[NSString stringWithFormat:@"%@follower/client/%@",delegate.baseUrl,delegate.userID];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:string]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        
                                                        if(data!=nil)
                                                        {
                                                            if (error) {
                                                                //NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                //NSLog(@"%@", httpResponse);
                                                                if([httpResponse statusCode]==403)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                                                                    
                                                                }
                                                                else if([httpResponse statusCode]==401)
                                                                {
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"relogin" object:nil];
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    
                                                                dict= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                
                                                                //NSLog(@"user dict:%@",dict);
                                                                
                                                                
                                                                
                                                           
                                                                
                                                                
                                                                }
                                                                
                                                                
                                                            }
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                self.activityInd.hidden = YES;
                                                                [self.activityInd stopAnimating];
                                                                
                                                                @try {
                                                                    self.profileImage = [dict objectForKey:@"logourl"];
                                                                    
                                                                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                                                    
                                                                    // saving an NSString
                                                                    [prefs setObject:self.imageURLString forKey:@"image"];
                                                                    
                                                                    
                                                                    [prefs synchronize];
//                                                                    self.profileImgView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.profileImage]]];
                                                                    [self.profileImgView sd_setImageWithURL:[NSURL URLWithString:self.profileImage] placeholderImage:[UIImage imageNamed:@"dpDefault"]];
                                                                    
                                                                    
                                                                    
                                                                }
                                                                @catch (NSException * e) {
                                                                    //NSLog(@"Exception: %@", e);
                                                                }
                                                                @finally {
                                                                    //NSLog(@"finally");
                                                                }
                                                                
                                                
                                                                 @try {
//                                                                    NSString * name=[NSString stringWithFormat:@" %@",[dict objectForKey:@"firstname"]];
//                                                                    NSString * email=[NSString stringWithFormat:@" %@",[dict objectForKey:@"email"]];
//                                                                    if([name isEqualToString:@"<null>"])
//                                                                    {
//                                                                        self.userNameTxtFld.text=@"------";
//                                                                    }
//                                                                    else
//                                                                    {
//                                                                        self.userNameTxtFld.text=name;
//                                                                    }
//                                                                    if([email isEqualToString:@"<null>"])
//                                                                    {
//                                                                        self.emailTxtFld.text=@"------";
//                                                                    }
//                                                                    else
//                                                                    {
//                                                                        self.emailTxtFld.text=email;
//
//                                                                    }
//
                                                                   NSUserDefaults *loggedInUser = [NSUserDefaults standardUserDefaults];
                                                                    
                                                                   NSString * number= [loggedInUser stringForKey:@"profilemobilenumber"];
                                                                    
                                                                    if(number)
                                                                    {
                                                                        self.mobileNumberLbl.text=[NSString stringWithFormat:@" %@",number];
                                                                    }
                                                                     NSString * name= [loggedInUser stringForKey:@"profilename"];
                                                                     
                                                                     if(name)
                                                                     {
                                                                         self.userNameLbl.text=[NSString stringWithFormat:@" %@",name];
                                                                     }
                                                                     
                                                                     NSString * email= [loggedInUser stringForKey:@"profileemail"];
                                                                     
                                                                     if(email)
                                                                     {
                                                                         self.emailLbl.text=[NSString stringWithFormat:@" %@",email];
                                                                     }
                                                                     NSString * notStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"turnoffnotification"]];
                                                                  if([notStr isEqualToString:@"1"])
                                                                  {
                                                                      [self.adviceSwitch setOn:YES];
                                                                      adviceNotificationStr = @"1";
                                                                  }
                                                                 else if([notStr isEqualToString:@"0"])
                                                                  {
                                                                          [self.adviceSwitch setOn:NO];
                                                                      adviceNotificationStr = @"0";
                                                                  }
                                                                     
                                                                }
                                                                @catch (NSException * e) {
                                                                    //NSLog(@"Exception: %@", e);
                                                                }
                                                                @finally {
                                                                    //NSLog(@"finally");
                                                                }
                                                                
                                                                
                                                                
                                                            });
                                                        }
                                                        else
                                                            
                                                        {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                                
                                                                [self presentViewController:alert animated:YES completion:^{
                                                                    
                                                                }];
                                                                
                                                                
                                                                UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                    [self newprofile];
                                                                }];
                                                                
                                                                UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                    
                                                                }];
                                                                
                                                                
                                                                [alert addAction:retryAction];
                                                                [alert addAction:cancel];
                                                                
                                                            });
                                                            
                                                        }
                                                        
                                                    }];
        [dataTask resume];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
-(void)functionalityClass
{
    @try
    {
    self.adviceFrmLeadersBtn.enabled=YES;
    self.marketPriceAlertsBtn.enabled=YES;
    self.triggerPriceAlertBtn.enabled=YES;
    self.stopLossButton.enabled=YES;
    self.orderExecutionBtn.enabled=YES;
    
//    [self.adviceButton addTarget:self action:@selector(onToneButtontap:) forControlEvents:UIControlEventTouchUpInside];
//    [self.marketPriceButton addTarget:self action:@selector(onToneButtontap:) forControlEvents:UIControlEventTouchUpInside];
//    [self.triggerPriceButton addTarget:self action:@selector(onToneButtontap:) forControlEvents:UIControlEventTouchUpInside];
//    [self.orderExecutinButton addTarget:self action:@selector(onToneButtontap:) forControlEvents:UIControlEventTouchUpInside];
//    [self.stopLossButton addTarget:self action:@selector(onToneButtontap:) forControlEvents:UIControlEventTouchUpInside];
//    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"PERSONAL",@"TRADE"]];
//    segmentedControl.frame = CGRectMake(0,70, self.view.frame.size.width, 40);
//    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
//    //    segmentedControl.verticalDividerEnabled = YES;
//    //    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(251/255.0) green:(196/255.0) blue:(12/255.0) alpha:1];
//    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
//    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
//    [self.view addSubview:segmentedControl];
    
    //brokerName//
    [self.brokerNameTickerBtn setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    [self.brokerNameTickerBtn setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    self.brokerNameTickerBtn.selected = YES;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if(prefs !=nil)
    {
        
        // getting an NSString
        
        NSString * brokerStr=[prefs stringForKey:@"brokerNameStatus"];
        
        if([brokerStr isEqualToString:@"Selected"])
        {
            self.brokerNameTickerBtn.selected = YES;
            
        }
        
        if([brokerStr isEqualToString:@"UnSelected"])
        {
            self.brokerNameTickerBtn.selected = NO;
           
            
        }
        
    }
    

    
    //ticker//
    [self.showTickerBtn setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    [self.showTickerBtn setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    self.showTickerBtn.selected=YES;
    
    
    
    if(prefs !=nil)
    {
        
        // getting an NSString
        
        NSString * tickerStr=[prefs stringForKey:@"tickerstatus"];
        
        if([tickerStr isEqualToString:@"Selected"])
        {
            self.showTickerBtn.selected = YES;
            
        }
        
        if([tickerStr isEqualToString:@"UnSelected"])
        {
            self.showTickerBtn.selected = NO;
            
            
        }
        
        
       
        
    }
    
    [self.notificationTickerBtn setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    [self.notificationTickerBtn setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    self.notificationTickerBtn.selected = YES;
    
    
    
    
    
    
    [self.commodityBtn setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    [self.commodityBtn setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    self.commodityBtn.selected = NO;
    [self.commodityBtn addTarget:self action:@selector(commodityAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.dayTradeBtn setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    [self.dayTradeBtn setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    self.dayTradeBtn.selected = NO;
    [self.dayTradeBtn addTarget:self action:@selector(dayTradeAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.longTermBtn setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    [self.longTermBtn setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    self.longTermBtn.selected = NO;
    [self.longTermBtn addTarget:self action:@selector(longTermAction) forControlEvents:UIControlEventTouchUpInside];

    [self.shortTermBtn setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    [self.shortTermBtn setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    self.shortTermBtn.selected = NO;
    [self.shortTermBtn addTarget:self action:@selector(shortTermAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    

    
    
    [self.marketBtn setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    [self.marketBtn setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    self.marketBtn.selected = YES;
    [self.marketBtn addTarget:self action:@selector(marketAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.limitBtn setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    [self.limitBtn setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    self.limitBtn.selected = NO;
    [self.limitBtn addTarget:self action:@selector(limitAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.stopLossButton setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    [self.stopLossButton setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    self.stopLossButton.selected = NO;
    [self.stopLossButton addTarget:self action:@selector(stopLossAction1) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.quantityBtn setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    [self.quantityBtn setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    self.quantityBtn.selected = YES;
    [self.quantityBtn addTarget:self action:@selector(quantityAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.valueBtn setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    [self.valueBtn setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    self.valueBtn.selected = NO;
    [self.valueBtn addTarget:self action:@selector(valueAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.dealerModeBtn setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    [self.dealerModeBtn setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    self.dealerModeBtn.selected = NO;
    [self.dealerModeBtn addTarget:self action:@selector(dealerAction) forControlEvents:UIControlEventTouchUpInside];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

}

-(void)segmentedControlChangedValue
{
    @try
    {
    if(segmentedControl.selectedSegmentIndex==0)
    {
//        self.scrollView.scrollEnabled=false;
        self.personalView.hidden=NO;
        self.tradeView.hidden=YES;
        if(delegate.test==true)
        {
            self.showTickerBtn.selected=YES;
            delegate.test1=true;
        }else if (delegate.test==false)
        {
            self.showTickerBtn.selected=NO;
            delegate.test1=false;
        }
    }else if(segmentedControl.selectedSegmentIndex==1)
    {
        self.scrollView.scrollEnabled=true;
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        // getting an NSString
        
        
        delegate.orderStr=[prefs stringForKey:@"order"];
        
        if([delegate.orderStr isEqualToString:@"Market"])
        {
            [self marketAction];
        }
        else if([delegate.orderStr isEqualToString:@"Limit"])
        {
            [self limitAction];
        }
        else if([delegate.orderStr isEqualToString:@"StoppLoss"])
        {
            [self stopLossAction1];
        }
        
        else
        {
            [self marketAction];
        }
        
        NSString * quantityStr=[prefs stringForKey:@"quantityCheck"];
        
        if([quantityStr isEqualToString:@"quantity"])
        {
            [self quantityAction];
        }
        
        else if([quantityStr isEqualToString:@"value"])
        {
            [self valueAction];
        }
        
        else
        {
            [self quantityAction];
        }
        
        self.quantityTxt.text=[prefs stringForKey:@"quantity"];
        self.tradeView.hidden=NO;
        self.personalView.hidden=YES;
        //NSLog(@"%@",[prefs stringForKey:@"quantity"]);
    }
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

}

-(void)equityAction
{
    if(check==true)
    {
        self.equitiesBtn.selected=YES;
        check=false;
    }else
    {
        self.equitiesBtn.selected=NO;
        check=true;
    }
}
-(void)derivativesAction
{
    if(check1==true)
    {
        self.derivativesBtn.selected=YES;
        check1=false;
    }else
    {
        self.derivativesBtn.selected=NO;
        check1=true;
    }

}
-(void)currencyAction
{
    if(check2==true)
    {
        self.currencyBtn.selected=YES;
        check2=false;
    }else
    {
        self.currencyBtn.selected=NO;
        check2=true;
    }
    
}
-(void)commodityAction
{
    if(check3==true)
    {
        self.commodityBtn.selected=YES;
        check3=false;
    }else
    {
        self.commodityBtn.selected=NO;
        check3=true;
    }
}

-(void)dayTradeAction
{
    if(check4==true)
    {
        self.dayTradeBtn.selected=YES;
        check4=false;
        [self.tradeDictionary setObject:[NSNumber numberWithInt:1] forKey:@"day"];
    }else
    {
        self.dayTradeBtn.selected=NO;
        check4=true;
        [self.tradeDictionary removeObjectForKey:@"day"];
    }
    
    
    //
    
    
}
-(void)longTermAction
{
    if(check5==true)
    {
        self.longTermBtn.selected=YES;
        check5=false;
         [self.tradeDictionary setObject:[NSNumber numberWithInt:3] forKey:@"long"];
    }else
    {
        self.longTermBtn.selected=NO;
        check5=true;
         [self.tradeDictionary removeObjectForKey:@"long"];
    }
}
-(void)shortTermAction
{
    if(check6==true)
    {
        self.shortTermBtn.selected=YES;
        check6=false;
         [self.tradeDictionary setObject:[NSNumber numberWithInt:2] forKey:@"short"];
        
    }else
    {
        self.shortTermBtn.selected=NO;
        check6=true;
        [self.tradeDictionary removeObjectForKey:@"short"];
    }
}
-(void)BSEAction
{
 
     self.BSEBtn.selected=YES;
    self.NSEBtn.selected=NO;
    self.MCXBtn.selected=NO;
 
}
-(void)NSEAction
{
    
    self.BSEBtn.selected=NO;
    self.NSEBtn.selected=YES;
    self.MCXBtn.selected=NO;
    
}
-(void)MCXAction
{
    
    self.BSEBtn.selected=NO;
    self.NSEBtn.selected=NO;
    self.MCXBtn.selected=YES;
    
}
-(void)marketAction
{
    
    self.marketBtn.selected=YES;
    self.limitBtn.selected=NO;
    self.stopLossButton.selected=NO;
    
    orderType=@"Market";
    
    
}
-(void)limitAction
{
    
    self.marketBtn.selected=NO;
    self.limitBtn.selected=YES;
    self.stopLossButton.selected=NO;
    orderType=@"Limit";
    
}
-(void)stopLossAction1
{
    
    self.marketBtn.selected=NO;
    self.limitBtn.selected=NO;
    self.stopLossButton.selected=YES;
    orderType=@"StopLoss";
    
}
-(void)quantityAction
{
    self.quantityTxt.placeholder=@"";
    self.valueBtn.selected=NO;
    self.quantityBtn.selected=YES;
    quantityType=@"quantity";
    
    
}
-(void)valueAction
{
     self.quantityTxt.placeholder=@"₹";
    self.valueBtn.selected=YES;
    self.quantityBtn.selected=NO;
    quantityType=@"value";
    
}
-(void)dealerAction
{
    if(radio1==true)
    {
        self.dealerModeBtn.selected=YES;
        radio1=false;
    }else
    {
        self.dealerModeBtn.selected=NO;
        radio1=true;
    }
}







- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)brokerNameTickerTap:(id)sender {
    if(self.brokerNameTickerBtn.selected==NO)
    {
        self.brokerNameTickerBtn.selected=YES;
        
    }else if(self.brokerNameTickerBtn.selected==YES)
    {
        self.brokerNameTickerBtn.selected=NO;
        
    }
    
    
}
- (IBAction)notificationTickerTap:(id)sender {
    
    if(check9==true)
    {
        self.notificationTickerBtn.selected=YES;
        check9=false;
    }else
    {
        self.notificationTickerBtn.selected=NO;
        check9=true;
    }
    if(self.notificationTickerBtn.selected==YES)
    {
        self.adviceFrmLeadersBtn.enabled=YES;
        self.marketPriceAlertsBtn.enabled=YES;
        self.triggerPriceAlertBtn.enabled=YES;
        self.stopLossButton.enabled=YES;
        self.orderExecutionBtn.enabled=YES;

        
    }else
    {
        self.adviceFrmLeadersBtn.enabled=NO;
        self.marketPriceAlertsBtn.enabled=NO;
        self.triggerPriceAlertBtn.enabled=NO;
        self.stopLossButton.enabled=NO;
        self.orderExecutionBtn.enabled=NO;
    }
}
- (IBAction)showTickerTap:(id)sender {
    if(self.showTickerBtn.selected==NO)
    {
        self.showTickerBtn.selected=YES;
        
        
        
    }else if(self.showTickerBtn.selected==YES)
    {
        self.showTickerBtn.selected=NO;
       
    }
}
- (IBAction)backButtonTap:(id)sender {
    
//    TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
//    
//    [self presentViewController:tabPage animated:YES completion:nil];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
   

    
    
}
- (IBAction)imagePickerBtn:(id)sender {
//    dispatch_async(dispatch_get_main_queue(), ^{
//        
//        
//        
//        
//        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Under Development." preferredStyle:UIAlertControllerStyleAlert];
//        
//        
//        
//        [self presentViewController:alert animated:YES completion:^{
//            
//            
//            
//        }];
//        
//        
//        
//        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            
//            
//            
//        }];
//        
//        
//        
//        [alert addAction:okAction];
//        
//    });
//    
@try
    {
    
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Choose one" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    [self presentViewController:alert animated:YES completion:nil];
    self.camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        UIImagePickerController * cameraPicker = [[UIImagePickerController alloc]init];
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        }
        cameraPicker.delegate=self;
        [self presentViewController:cameraPicker animated:YES completion:nil];
        
        
        
        cameraPicker.mediaTypes = [NSArray arrayWithObjects:
                                   (NSString *) kUTTypeImage,
                                   nil];
        cameraPicker.allowsEditing = NO;
        // [self presentViewController:cameraPicker animated:YES completion:nil];
        self.newMedia = YES;
        
        
    }];
    
    self.gallery = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        pickerController.delegate=self;
        [self presentViewController:pickerController animated:YES completion:nil];
        
        
    }];
    
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:self.camera];
    [alert addAction:self.gallery];
    [alert addAction:cancel];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    self.activityInd.hidden = NO;
    [self.activityInd startAnimating];
    @try
    {
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    
    NSData *png_Data = UIImageJPEGRepresentation(image, 0);
    
    
    
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *str_localFilePath = [documentsDirectory stringByAppendingPathComponent:@"One.png"];
    [png_Data writeToFile:str_localFilePath atomically:YES];
    
    [self postToS3];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

    
    
   
    
}
- (NSString *)documentsPathForFileName:(NSString *)name {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
    return [documentsPath stringByAppendingPathComponent:name];
}

-(void)image:(UIImage *)image
finishedSavingWithError:(NSError *)error
 contextInfo:(void *)contextInfo
{
    if (error) {
        
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Save failed" message:@"Failed to save image" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
    }
}



- (IBAction)tradeSettingsUpdateBtn:(id)sender {
    
    @try
    {
    
      NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString * durationTypeString = [NSString stringWithFormat:@"%@",[self.tradeDictionary allValues]];
    
    if([[self.tradeDictionary allValues] count]>0)
    {
        delegate.durationUpdateBool=true;
        delegate.profileDuration = [NSString stringWithFormat:@"%@",[self.tradeDictionary allValues]];
          [prefs setObject:delegate.profileDuration forKey:@"profileDuration"];
        [prefs setObject:@"1" forKey:@"profileDurationBool"];
    }
    else
    {
        delegate.durationUpdateBool=false;
        [prefs setObject:delegate.profileDuration forKey:@"profileDuration"];
        [prefs setObject:@"0" forKey:@"profileDurationBool"];
    }
    
    
//    NSString * durationF = [durationTypeString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"()"]];
//    NSString * duration = [durationF stringByReplacingOccurrencesOfString:@"(" withString:@""];
//    NSString * duration2 = [duration stringByReplacingOccurrencesOfString:@"\n" withString:@""];
//    delegate.wisdomDuration = [duration2 stringByReplacingOccurrencesOfString:@" " withString:@""];
//    
//    //NSLog(@"DurationType:%@",delegate.wisdomDuration);

    
  
    
    // saving an NSString
    

    
    [prefs setObject:self.quantityTxt.text forKey:@"quantity"];
    
    [prefs setObject:quantityType forKey:@"quantityCheck"];
    
    
    
    [prefs setObject:orderType forKey:@"order"];
    
    [prefs synchronize];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
//        @try {
//            NSDictionary *headers = @{ @"content-type": @"application/json",
//                                       @"cache-control": @"no-cache",
//                                       @"authtoken":delegate.zenwiseToken,
//                                       @"deviceid":delegate.currentDeviceId
//                                       };
//            NSDictionary *parameters = @{ @"logourl": self.profileImage,
//                                          @"firstname":self.userNameLbl.text,
//                                          @"email":self.emailTxtFld.text
//                                          };
//
//            NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
//
//            NSString * string=[NSString stringWithFormat:@"%@follower/client/%@",delegate.baseUrl,delegate.userID];
//
//            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:string]
//                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                               timeoutInterval:10.0];
//            [request setHTTPMethod:@"POST"];
//            [request setAllHTTPHeaderFields:headers];
//            [request setHTTPBody:postData];
//
//            NSURLSession *session = [NSURLSession sharedSession];
//            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
//                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                                                            if(data !=nil)
//                                                            {
//                                                                if (error) {
//                                                                    //NSLog(@"%@", error);
//                                                                } else {
//                                                                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
//                                                                    //NSLog(@"%@", httpResponse);
//                                                                }
//
//                                                                dispatch_async(dispatch_get_main_queue(), ^{
//
//                                                                    //                                                            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//                                                                    //
//                                                                    //                                                            // saving an NSString
//                                                                    //                                                            [prefs setObject:self.imageURLString forKey:@"image"];
//                                                                    //
//                                                                    //
//                                                                    //                                                            [prefs synchronize];
//
//
//
//
//                                                                    UIAlertController * alert = [UIAlertController
//                                                                                                 alertControllerWithTitle:@""
//                                                                                                 message:@"Profile updated successfully"
//                                                                                                 preferredStyle:UIAlertControllerStyleAlert];
//
//                                                                    //Add Buttons
//
//                                                                    UIAlertAction* okButton = [UIAlertAction
//                                                                                               actionWithTitle:@"Ok"
//                                                                                               style:UIAlertActionStyleDefault
//                                                                                               handler:^(UIAlertAction * action) {
//                                                                                                   //Handle your yes please button action here
//
//                                                                                                   delegate.profileImg=@"YES";
//
//                                                                                                   [self.scrollView setContentOffset:CGPointZero animated:YES];
//
//                                                                                               }];
//                                                                    //Add your buttons to alert controller
//
//                                                                    [alert addAction:okButton];
//
//
//                                                                    [self presentViewController:alert animated:YES completion:nil];
//
//                                                                });
//
//                                                            }
//                                                            else
//                                                            {
//                                                                dispatch_async(dispatch_get_main_queue(), ^{
//                                                                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
//
//                                                                    [self presentViewController:alert animated:YES completion:^{
//
//                                                                    }];
//
//
//                                                                    UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                                                                        [self profileUpdateNew];
//                                                                    }];
//
//                                                                    UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//                                                                    }];
//
//
//                                                                    [alert addAction:retryAction];
//                                                                    [alert addAction:cancel];
//
//                                                                });
//
//                                                            }
//
//
//                                                        }];
//            [dataTask resume];
//
//        }
//        @catch (NSException * e) {
//            //NSLog(@"Exception: %@", e);
//        }
//        @finally {
//            //NSLog(@"finally");
//        }
    
   
    
    [self profileUpdateNew];
    

    
    
}




- (IBAction)profileUpdateBtn:(id)sender {
    @try
    {
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
//    // saving an NSString
//    
//    if([tickerCheck isEqualToString:@"Unselected"])
//    {
//        NSString * newTickerCheck=@"Selected";
//    }
//    
////    else if(tickerCheck isEqualToString:<#(nonnull NSString *)#>)
//    
//    [prefs setObject:self.quantityTxt.text forKey:@"quantity"];
//    
//    [prefs setObject:quantityType forKey:@"quantityCheck"];
    
    
    
    if(self.brokerNameTickerBtn.selected==YES)
    {
        NSString * newbrokerTicker=@"Selected";
        [prefs setObject:newbrokerTicker forKey:@"brokerNameStatus"];
        delegate.mainViewBrokerStr=@"Show";
    }
    
    else if(self.brokerNameTickerBtn.selected==NO)
    {
        NSString * newbrokerTicker=@"UnSelected";
        [prefs setObject:newbrokerTicker forKey:@"brokerNameStatus"];
        delegate.mainViewBrokerStr=@"DontShow";
    }
    
    if(self.showTickerBtn.selected==YES)
    {
        NSString * ticker=@"Selected";
        [prefs setObject:ticker forKey:@"tickerstatus"];
        delegate.mainTickerStr=@"Show";
    }
    
    else if(self.showTickerBtn.selected==NO)
    {
        NSString * ticker=@"UnSelected";
        [prefs setObject:ticker forKey:@"tickerstatus"];
        delegate.mainTickerStr=@"DontShow";
    }
    
    
   
    NSString * durationTypeString = [NSString stringWithFormat:@"%@",[self.tradeDictionary allValues]];
    
    if([[self.tradeDictionary allValues] count]>0)
    {
        delegate.durationUpdateBool=true;
        delegate.profileDuration = [NSString stringWithFormat:@"%@",[self.tradeDictionary allValues]];
        [prefs setObject:delegate.profileDuration forKey:@"profileDuration"];
        [prefs setObject:@"1" forKey:@"profileDurationBool"];
    }
    else
    {
        delegate.durationUpdateBool=false;
        [prefs setObject:delegate.profileDuration forKey:@"profileDuration"];
        [prefs setObject:@"0" forKey:@"profileDurationBool"];
    }
    
    
    //    NSString * durationF = [durationTypeString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"()"]];
    //    NSString * duration = [durationF stringByReplacingOccurrencesOfString:@"(" withString:@""];
    //    NSString * duration2 = [duration stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    //    delegate.wisdomDuration = [duration2 stringByReplacingOccurrencesOfString:@" " withString:@""];
    //
    //    //NSLog(@"DurationType:%@",delegate.wisdomDuration);
    
    
    
    
    // saving an NSString
    
    
    
    [prefs setObject:self.quantityTxt.text forKey:@"quantity"];
    
    [prefs setObject:quantityType forKey:@"quantityCheck"];
    
    
    
    [prefs setObject:orderType forKey:@"order"];
    
    [prefs synchronize];
    
   
        [self profileUpdateNew];
    
    
    
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    

   
    
}
-(void)profileUpdateNew
{
    @try {
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"cache-control": @"no-cache",
                                   @"authtoken":delegate.zenwiseToken,
                                    @"deviceid":delegate.currentDeviceId
                                   };
        if([self.profileImage isEqual:[NSNull null]])
        {
            self.profileImage = @"";
        }
        NSDictionary *parameters;
        if([adviceNotificationStr isEqualToString:@"1"])
        {
           parameters = @{ @"logourl": self.profileImage,
                                          @"firstname":self.userNameLbl.text,
                                          @"email":self.emailLbl.text,
                                          @"turnoffnotification": @(true)
                                          };
        }
        else
        {
            parameters = @{ @"logourl": self.profileImage,
                                          @"firstname":self.userNameLbl.text,
                                          @"email":self.emailLbl.text,
                                          @"turnoffnotification": @(false)
                                          };
        }
        
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
       
        NSString * string=[NSString stringWithFormat:@"%@follower/client/%@",delegate.baseUrl,delegate.userID];
       
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:string]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(data !=nil)
                                                        {
                                                            if (error) {
                                                                //NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                //NSLog(@"%@", httpResponse);
                                                            }
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                //                                                            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                                                //
                                                                //                                                            // saving an NSString
                                                                //                                                            [prefs setObject:self.imageURLString forKey:@"image"];
                                                                //
                                                                //
                                                                //                                                            [prefs synchronize];
                                                                
                                                                
                                                                self.activityInd.hidden = YES;
                                                                [self.activityInd stopAnimating];
                                                                
                                                                UIAlertController * alert = [UIAlertController
                                                                                             alertControllerWithTitle:@""
                                                                                             message:@"Profile updated successfully"
                                                                                             preferredStyle:UIAlertControllerStyleAlert];
                                                                
                                                                //Add Buttons
                                                                
                                                                UIAlertAction* okButton = [UIAlertAction
                                                                                           actionWithTitle:@"Ok"
                                                                                           style:UIAlertActionStyleDefault
                                                                                           handler:^(UIAlertAction * action) {
                                                                                               //Handle your yes please button action here
                                                                                               
                                                                                               delegate.profileImg=@"YES";
                                                                                               
                                                                                               [self.scrollView setContentOffset:CGPointZero animated:YES];
                                                                                               
                                                                                           }];
                                                                //Add your buttons to alert controller
                                                                
                                                                [alert addAction:okButton];
                                                                
                                                                
                                                                [self presentViewController:alert animated:YES completion:nil];
                                                                
                                                            });
                                                            
                                                        }
                                                        else
                                                        {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                                
                                                                [self presentViewController:alert animated:YES completion:^{
                                                                    
                                                                }];
                                                                
                                                                
                                                                UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                    [self profileUpdateNew];
                                                                }];
                                                                
                                                                UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                    
                                                                }];
                                                                
                                                                
                                                                [alert addAction:retryAction];
                                                                [alert addAction:cancel];
                                                                
                                                            });
                                                            
                                                        }
                                                        
                                                        
                                                    }];
        [dataTask resume];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}
//notifications//

-(void)loadAudioFileList{
    audioFileList = [[NSMutableArray alloc] init];
    
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSURL *directoryURL = [NSURL URLWithString:@"/System/Library/Audio/UISounds"];
    NSArray *keys = [NSArray arrayWithObject:NSURLIsDirectoryKey];
    
    NSDirectoryEnumerator *enumerator = [fileManager
                                         enumeratorAtURL:directoryURL
                                         includingPropertiesForKeys:keys
                                         options:0
                                         errorHandler:^(NSURL *url, NSError *error) {
                                             // Handle the error.
                                             // Return YES if the enumeration should continue after the error.
                                             return YES;
                                         }];
    
    for (NSURL *url in enumerator) {
        NSError *error;
        NSNumber *isDirectory = nil;
        if (! [url getResourceValue:&isDirectory forKey:NSURLIsDirectoryKey error:&error]) {
            // handle error
        }
        else if (! [isDirectory boolValue]) {
            [audioFileList addObject:url];
        }
    }
    
    //NSLog(@"%lu",(unsigned long)audioFileList.count);
}

//S3


-(void)postToS3
{
    @try {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *str_Filename = [documentsDirectory stringByAppendingPathComponent:@"One.png"];
        NSData *multiMedaiData = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:str_Filename]];
        
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        
        NSMutableData *body = [NSMutableData data];
        
        if (multiMedaiData) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; filename=\"%@\"\r\n",str_Filename] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:multiMedaiData];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            
        }
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        NSDictionary *headers = @{ @"content-type": contentType,@"Content-Length":postLength,@"x-access-token": @"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2UxOTAiLCJpc3MiOiJodHRwczovL3d3dy5oY3VlLmNvIiwicGVybWlzc2lvbnMiOiJhdXRoZW50aWNhdGVkIiwidXNlcmlkIjpudWxsLCJpYXQiOjE0ODYyMDczNDB9.ewu2QtoHl1kBSrM1y6yL9Jr58kiGmhCsy2YWoFrE2OU" };
        
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://zambalauploaddev.ap-southeast-1.elasticbeanstalk.com/upload"]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:body];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(data !=nil)
                                                        {
                                                        if (error) {
                                                            //NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            //NSLog(@"%@", httpResponse);
                                                            
                                                            NSMutableDictionary * responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            
                                                            //NSLog(@"Reponse Dictionary:%@",responseDictionary);
                                                            self.imageURLString = [[responseDictionary objectForKey:@"message"] objectForKey:@"Location"];
                                                            //NSLog(@"Image URl String:%@",self.imageURLString);
                                                            
                                                            
                                                            
                                                        }
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            self.profileImgView.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.imageURLString]]];
                                                            
                                                            self.profileImage=self.imageURLString;
                                                            [self profileUpdateNew];
                                                        });
                                                        }
                                                        else
                                                        {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                                
                                                                [self presentViewController:alert animated:YES completion:^{
                                                                    
                                                                }];
                                                                
                                                                
                                                                UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                    [self postToS3];
                                                                }];
                                                                
                                                                UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                    
                                                                }];
                                                                
                                                                
                                                                [alert addAction:retryAction];
                                                                [alert addAction:cancel];
                                                                
                                                            });
                                                            
                                                        }
                                                        
                                                        
                                                    }];
        [dataTask resume];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
    
}

//reachability//

-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    
    
}

-(BOOL)networkStatus
{
    
    
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    
    if(netStatus==NotReachable)
    {
        
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    return YES;
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    
    
    
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
    
    [self.orderExecutionBtn setTitle:delegate.notificationToneName forState:UIControlStateNormal];
    
 
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
     verify=[prefs stringForKey:@"profileDuration"];
    
    if([verify containsString:@"1"])
    {
        check4=true;
        [self dayTradeAction];
    }
    
    if([verify containsString:@"2"])
    {
        check6=true;
        [self shortTermAction];
    }
    
    
    if([verify containsString:@"3"])
    {
        check5=true;
        [self longTermAction];
    }
    
    
   
}







- (IBAction)orderPreBtnAction:(id)sender {
    
    
    
}
- (IBAction)publicNotifySwitch:(id)sender {
    if([sender isOn])
    {
        adviceNotificationStr = @"1";
    }
    else
    {
        adviceNotificationStr = @"0";
    }
    [self profileUpdateNew];
}
@end
