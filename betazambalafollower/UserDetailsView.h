//
//  UserDetailsView.h
//  PremiumServices
//
//  Created by zenwise mac 2 on 7/21/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserDetailsView : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *nameTxt;

@property (weak, nonatomic) IBOutlet UITextField *emailTxt;

@property (weak, nonatomic) IBOutlet UITextField *mobileTxt;


- (IBAction)continueAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *userDetailsView;

@property (weak, nonatomic) IBOutlet UIButton *continueOutlet;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeightConstraint;

@property NSString * accountIFSC;
@property NSString * accountNo;
@property NSString * segment;
@property NSString * txnAmount;
@property NSString * typeOfPayment;
@property NSString * upiAlias;
@property NSString * bankName;

@end
