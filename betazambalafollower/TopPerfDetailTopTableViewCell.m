//
//  TopPerfDetailTopTableViewCell.m
//  betazambalafollower
//
//  Created by Zenwise Technologies on 22/06/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import "TopPerfDetailTopTableViewCell.h"

@implementation TopPerfDetailTopTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.bgview.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    self.bgview.layer.shadowOffset = CGSizeMake(0,0);
    self.bgview.layer.shadowOpacity = 1.0f;
    self.bgview.layer.shadowRadius = 1.5f;
    self.bgview.layer.cornerRadius=5.0f;
    self.bgview.layer.masksToBounds = NO;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
