//
//  RedeemCodeViewController.m
//  betazambalafollower
//
//  Created by Zenwise Technologies Private Limited on 21/08/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import "RedeemCodeViewController.h"
#import "AppDelegate.h"
#import "TabBar.h"
#import "WalletView.h"
@import Mixpanel;

@interface RedeemCodeViewController ()
{
    AppDelegate * delegate;
}

@end

@implementation RedeemCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
    [mixpanelMini track:@"redeem_voucher_detail_page"];
    
    self.inviteView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5] CGColor];
    self.inviteView.layer.shadowOffset = CGSizeMake(2.0f,5.0f);
    self.inviteView.layer.shadowOpacity = 1.0f;
    self.inviteView.layer.shadowRadius = 5.0f;
    //self.inviteView.layer.shadowOpacity = 3.0f;
    self.inviteView.layer.masksToBounds = NO;
    
    self.homeButton.layer.cornerRadius =20.0f;
    self.homeButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16] CGColor];
    self.homeButton.layer.shadowOffset = CGSizeMake(0.0f,3.0f);
    self.homeButton.layer.shadowOpacity = 1.0f;
    self.homeButton.layer.shadowRadius = 5.0f;
    //self.inviteView.layer.shadowOpacity = 3.0f;
    self.homeButton.layer.masksToBounds = NO;
    
    self.referralLabel.text = delegate.referralCodeFromServer;
         self.referralLabel.layer.borderWidth=1.0f;
         self.referralLabel.layer.borderColor=[[UIColor colorWithRed:(23/255.0) green:(102/255.0) blue:(160/255.0) alpha:1.0f] CGColor];
    CAShapeLayer *yourViewBorder = [CAShapeLayer layer];
    yourViewBorder.strokeColor = [UIColor whiteColor].CGColor;
    yourViewBorder.fillColor = nil;
    yourViewBorder.lineDashPattern = @[@2, @2];
    yourViewBorder.frame = self.codeButton.bounds;
    yourViewBorder.path = [UIBezierPath bezierPathWithRect:self.codeButton.bounds].CGPath;
    [self.codeButton.layer addSublayer:yourViewBorder];
    
    int remaniningBalance = [delegate.rewardPoints intValue]-[self.pointsRedeemed intValue];
    NSString * remainingBalanceString = [NSString stringWithFormat:@"%d",remaniningBalance];
    NSString * string = [NSString stringWithFormat:@"You have succesfully redeemed %@ Points from your account. Remaining balance: %@ Zambala Points",self.pointsRedeemed,remainingBalanceString];
    
//    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string attributes:@{
//                                                                                                                                                                                                                      NSFontAttributeName: [UIFont fontWithName:@"Ubuntu-Regular" size: 12.0f],
//                                                                                                                                                                                                                      NSForegroundColorAttributeName: [UIColor colorWithRed:25.0f / 255.0f green:165.0f / 255.0f blue:25.0f / 255.0f alpha:1.0f]
//                                                                                                                                                                                                                      }];
//    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Ubuntu-Bold" size: 12.0f] range:NSMakeRange(30, 10)];
//    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Ubuntu-Bold" size: 12.0f] range:NSMakeRange(79, 18)];
    
    self.pointsLabel.text = string;
    self.companyLabel.text = [NSString stringWithFormat:@"Here’s your %@ Voucher of ₹%@",self.voucherCompany,self.voucherValue];
    [self.codeButton setTitle:[NSString stringWithFormat:@"%@",self.voucherCode] forState:UIControlStateNormal];
    self.expiryLabel.text = [NSString stringWithFormat:@"Voucher expiry date: %@",self.expiryDate];
    [self.homeButton setTitle:@"BACK TO REWARDS" forState:UIControlStateNormal];
    [self.inviteButton addTarget:self action:@selector(onInviteTap) forControlEvents:UIControlEventTouchUpInside];
    [self.homeButton addTarget:self action:@selector(onHomeTap) forControlEvents:UIControlEventTouchUpInside];
    [self.codeButton addTarget:self action:@selector(onCodeButtonTap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

-(void)onCodeButtonTap
{
    [[UIPasteboard generalPasteboard] setString:self.codeButton.titleLabel.text];
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Code copied successfully." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        
        [alert addAction:okAction];
    });
}

-(void)onInviteTap
{
    @try
    {
        if(self.referralLabel.text.length!=0)
        {
            
            
            //            NSString * name= [loggedInUser stringForKey:@"profilename"];
            NSString *textToShare =[NSString stringWithFormat:@"I won %@ Coupon worth ₹ %@  as a reward in Zambala Stocks, a stock trading platform to create wealth. Start investing now and get rewarded. Use my refferal %@",self.voucherCompany,self.voucherValue,self.referralLabel.text];
            NSURL *myWebsite = [NSURL URLWithString:@"https://itunes.apple.com/us/app/zambala-stocks-by-zenwise/id1281356740?ls=1&mt=8"];
            // NSURL *myWebsite = [NSURL URLWithString:[NSString stringWithFormat:@"https://i06-6.tlnk.io/serve?action=click&publisher_id=358381&site_id=137664&my_publisher=%@&my_keyword=%@",delegate1.brokerNameStr,referralCode]];
            UIImage * image=[UIImage imageNamed:@"success"];
            NSArray *objectsToShare = @[textToShare,myWebsite,image];
            
            UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
            
            NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                           UIActivityTypePrint,
                                           UIActivityTypeAssignToContact,
                                           UIActivityTypeSaveToCameraRoll,
                                           UIActivityTypeAddToReadingList,
                                           UIActivityTypePostToFlickr,
                                           UIActivityTypePostToVimeo];
            
            activityVC.excludedActivityTypes = excludeActivities;
            
            [self presentViewController:activityVC animated:YES completion:nil];
        }
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)onHomeTap
{
    WalletView * wallet=[self.storyboard instantiateViewControllerWithIdentifier:@"WalletView"];
    wallet.navigationCheck = @"voucher";
    [self presentViewController:wallet animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
