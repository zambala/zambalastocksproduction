//
//  StockOrderView.h
//  testing
//
//  Created by zenwise technologies on 21/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSWebSocket.h"
#import <QuartzCore/QuartzCore.h>
@import SocketIO;
@interface StockOrderView : UIViewController<UITextFieldDelegate,NSStreamDelegate>


@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityInd;

@property (nonatomic, strong)  SocketManager* manager;
@property (nonatomic, strong)  SocketIOClient* socketio;
@property (weak, nonatomic) IBOutlet UISegmentedControl *buySellSegment;

@property (weak, nonatomic) IBOutlet UILabel *orderTypeLblStr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHgt;

@property (strong, nonatomic) IBOutlet UIImageView *dropDownImgView;

@property (strong, nonatomic) IBOutlet UIView *marketView;
@property (strong, nonatomic) IBOutlet UITextField *limitTxtFld;
@property (strong, nonatomic) IBOutlet UIView *limitTxtView;
@property (strong, nonatomic) IBOutlet UILabel *limitLbl;
@property (strong, nonatomic) IBOutlet UIView *stopLossView;
@property (strong, nonatomic) IBOutlet UIButton *marketBtn;
@property (strong, nonatomic) IBOutlet UIButton *limitBtn;
@property (strong, nonatomic) IBOutlet UIButton *stopLossBtn;
@property (strong, nonatomic) IBOutlet UILabel *companyLbl;
@property (strong, nonatomic) IBOutlet UILabel *bidPriceLbl;
@property (strong, nonatomic) IBOutlet UILabel *askPriceLbl;
@property UIWebView * kiteOrderWebview;
@property (strong, nonatomic) IBOutlet UILabel *bidQtyLbl;
@property (weak, nonatomic) IBOutlet UIView *orderTypeViewStr;

@property (strong, nonatomic) IBOutlet UILabel *askSizeLbl;
@property (strong, nonatomic) IBOutlet UILabel *ltpLbl;

@property (strong, nonatomic) IBOutlet UILabel *chngLbl;

@property (strong, nonatomic) IBOutlet UITextField *qtyTxt;
@property (strong, nonatomic) IBOutlet UILabel *chngPerLbl;


@property (strong, nonatomic) IBOutlet UITextField *trigerPriceTxt;


@property (strong, nonatomic) IBOutlet UITextField *stopLossPriceTxt;
- (IBAction)submitBtn:(id)sender;
@property PSWebSocket * socket;
@property NSString * message;

@property (strong, nonatomic) IBOutlet UILabel *segment;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *headerViewHgt;
- (IBAction)backBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *port;
@property (strong, nonatomic) IBOutlet UIButton *back;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *view2_Hgt;
@property (strong, nonatomic) IBOutlet UIView *view2;
@property (strong, nonatomic) IBOutlet UIView *view1;
@property (strong, nonatomic) IBOutlet UIView *futureView;
@property (strong, nonatomic) IBOutlet UIButton *decrementBtn;

@property (strong, nonatomic) IBOutlet UITextField *futQtyTxt;
@property (strong, nonatomic) IBOutlet UIButton *incrementBtn;

@property (strong, nonatomic) IBOutlet UILabel *bidQty1;
@property (strong, nonatomic) IBOutlet UILabel *bidQty2;

@property (strong, nonatomic) IBOutlet UILabel *bidQty3;
@property (strong, nonatomic) IBOutlet UILabel *bidQty4;
@property (strong, nonatomic) IBOutlet UILabel *bidQty5;
@property (strong, nonatomic) IBOutlet UILabel *bidPrice1;
@property (strong, nonatomic) IBOutlet UILabel *bidPrice2;
@property (strong, nonatomic) IBOutlet UILabel *bidPrice3;
@property (strong, nonatomic) IBOutlet UILabel *bidprice4;
@property (strong, nonatomic) IBOutlet UILabel *bidPrice5;
@property (strong, nonatomic) IBOutlet UILabel *askQty2;
@property (strong, nonatomic) IBOutlet UILabel *askQty3;
@property (strong, nonatomic) IBOutlet UILabel *askQty4;
@property (strong, nonatomic) IBOutlet UILabel *askQty5;
@property (strong, nonatomic) IBOutlet UILabel *askPrice1;
@property (strong, nonatomic) IBOutlet UILabel *askPrice3;
@property (strong, nonatomic) IBOutlet UILabel *askPrice4;
@property (strong, nonatomic) IBOutlet UILabel *askPrice5;

@property (strong, nonatomic) IBOutlet UILabel *askPrice2;
@property (strong, nonatomic) IBOutlet UILabel *expandLbl;

@property (strong, nonatomic) IBOutlet UILabel *askQty1;
@property (strong, nonatomic) IBOutlet UIImageView *expandImgView;

@property (strong, nonatomic) IBOutlet UILabel *totalBid;
@property (strong, nonatomic) IBOutlet UILabel *totalAsk;
@property (strong, nonatomic) IBOutlet UIButton *buyBtn;
- (IBAction)sellBtn:(id)sender;
- (IBAction)butBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *buyBtnOutlet;

@property (strong, nonatomic) IBOutlet UIButton *sellBtnOutlet;

- (IBAction)incrementBtn:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *orderTypeLbl;

- (IBAction)decrementBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *futQtyTextField;

@property (strong, nonatomic) IBOutlet UILabel *headingLbl;

@property (strong, nonatomic) IBOutlet UIButton *submitButton;

@property (strong, nonatomic) IBOutlet UIView *optView;

@property (strong, nonatomic) IBOutlet UILabel *optCompany;

@property (strong, nonatomic) IBOutlet UIView *adviceView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *adviceViewHgt;

@property (weak, nonatomic) IBOutlet UISwitch *deliverySwitch;

@property (strong, nonatomic) IBOutlet UIImageView *profileImg;

@property (strong, nonatomic) IBOutlet UILabel *profileName;

@property (strong, nonatomic) IBOutlet UILabel *date;

@property (strong, nonatomic) IBOutlet UILabel *companyName;
@property (strong, nonatomic) IBOutlet UILabel *detailAdvice;
-(void)navigationMethod;
@property NSString *localWisdomIDString;
@property NSString * upstoxCheckString;
//-(void)orderStatusAlertMethod;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property NSTimer * timer;
@property (weak, nonatomic) IBOutlet UIView *view3;
//@property (weak, nonatomic) IBOutlet UIView *deliverySwitch;
- (IBAction)deliverySwitchAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *deliveryDescLbl;
@property (weak, nonatomic) IBOutlet UIScrollView *subScrollView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *deliverySegment;
- (IBAction)deliverySegmentAction:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *orderViewHgt;

@end
