//
//  PersonalDetailsViewController.m
//  NewUI
//
//  Created by Zenwise Technologies on 22/05/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "PersonalDetailsViewController.h"
#import "NewSelectBrokerViewController.h"
#import "TagEncode.h"
#import "AppDelegate.h"
#import "BrokerViewNavigation.h"
#import "Reachability.h"
#import "NewOTPViewController.h"
#import <QuartzCore/QuartzCore.h>
@import Mixpanel;
@interface PersonalDetailsViewController ()<UITextFieldDelegate>
{
    NSDictionary * clientCreationDict;
    AppDelegate *delegate2;
    BOOL resendOtpBool;
    NSString * emailVerify;
}

@end

@implementation PersonalDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
     delegate2=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.fullNameTF.layer.cornerRadius=5.0f;
     self.fullNameTF.layer.borderWidth = 1.0f;
     self.fullNameTF.layer.borderColor = [[UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0] CGColor];
    self.fullNameTF.autocorrectionType = UITextAutocorrectionTypeNo;
    self.activityInd.hidden=YES;
    [self.activityInd stopAnimating];
    self.countryCodeTF.layer.cornerRadius=5.0f;
    self.countryCodeTF.layer.borderWidth = 1.0f;
    self.countryCodeTF.layer.borderColor = [[UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0] CGColor];
    self.countryCodeTF.autocorrectionType = UITextAutocorrectionTypeNo;
    self.mobileNumberTF.layer.cornerRadius=5.0f;
    self.mobileNumberTF.layer.borderWidth = 1.0f;
    self.mobileNumberTF.layer.borderColor = [[UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0] CGColor];
    self.mobileNumberTF.autocorrectionType = UITextAutocorrectionTypeNo;
    self.mobileNumberTF.delegate=self;
    
    self.emailIDTF.layer.cornerRadius=5.0f;
    self.emailIDTF.layer.borderWidth = 1.0f;
    self.emailIDTF.layer.borderColor = [[UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0] CGColor];
 self.emailIDTF.autocorrectionType = UITextAutocorrectionTypeNo;
    self.referralCodeTF.layer.cornerRadius=5.0f;
    self.referralCodeTF.layer.borderWidth = 1.0f;
    self.referralCodeTF.layer.borderColor = [[UIColor colorWithRed:(2.0)/255 green:(30.0)/255 blue:(41.0)/255 alpha:1.0] CGColor];
     self.referralCodeTF.autocorrectionType = UITextAutocorrectionTypeNo;
  
    [self.continueButton addTarget:self action:@selector(onContinueButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    UITapGestureRecognizer *terms = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTermsTap)];
    terms.numberOfTapsRequired = 1;
    terms.numberOfTouchesRequired = 1;
    self.termsAndConditionsLabel.userInteractionEnabled = YES;
    [self.termsAndConditionsLabel addGestureRecognizer:terms];
    
    UITapGestureRecognizer *bgTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onBgTap)];
    bgTap.numberOfTapsRequired = 1;
    bgTap.numberOfTouchesRequired = 1;
    self.bgView.userInteractionEnabled = YES;
    [self.bgView addGestureRecognizer:bgTap];
    
    self.bgView.hidden=YES;
    self.bgSubView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    self.bgSubView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.bgSubView.layer.shadowOpacity = 1.0f;
    self.bgSubView.layer.shadowRadius = 1.0f;
    self.bgSubView.layer.cornerRadius=5.0f;
    self.bgSubView.layer.masksToBounds = NO;
    
    self.succesView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    self.succesView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.succesView.layer.shadowOpacity = 1.0f;
    self.succesView.layer.shadowRadius = 1.0f;
    self.succesView.layer.cornerRadius=5.0f;
    self.succesView.layer.masksToBounds = NO;
    
    self.verifiedBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    self.verifiedBtn.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.verifiedBtn.layer.shadowOpacity = 1.0f;
    self.verifiedBtn.layer.shadowRadius = 1.0f;
    self.verifiedBtn.layer.cornerRadius=5.0f;
    self.verifiedBtn.layer.masksToBounds = NO;
    
    self.bgView.tag = 99;
    
    self.succesView.hidden=YES;
    self.activityIndicator.hidden=YES;
//    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
//    self.fullNameTF.leftView = paddingView;
//    self.fullNameTF.leftViewMode = UITextFieldViewModeAlways;
//
//    self.countryCodeTF.leftView = paddingView;
//    self.countryCodeTF.leftViewMode = UITextFieldViewModeAlways;
//
//    self.mobileNumberTF.leftView = paddingView;
//    self.mobileNumberTF.leftViewMode = UITextFieldViewModeAlways;
//
//    self.emailIDTF.leftView = paddingView;
//    self.emailIDTF.leftViewMode = UITextFieldViewModeAlways;
//
//    self.referralCodeTF.leftView = paddingView;
//    self.referralCodeTF.leftViewMode = UITextFieldViewModeAlways;
//     Do any additional setup after loading the view.
}

-(void)onBgTap
{
    self.activityInd.hidden=YES;                                                                                                                                                                                                                                                                                                                                                                                                           
    [self.activityInd stopAnimating];
    self.bgView.hidden=YES;
}

-(void)onTermsTap
{
    NSString *termsLink = @"https://s3-ap-southeast-1.amazonaws.com/elasticbeanstalk-ap-southeast-1-507357262371/docs/Zenwise+-+Privacy+Policy.pdf";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:termsLink]];
}
-(void)viewDidAppear:(BOOL)animated
{
    [self networkStatus];
     self.bgView.hidden=YES;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
}



-(void)viewWillAppear:(BOOL)animated
{
     [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
//    [self.navigationController setNavigationBarHidden:YES animated:YES];
    self.countryCodeTF.text=@"+91";
   
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.activityInd.hidden=YES;
        [self.activityInd stopAnimating];
     });
}

-(void)onContinueButtonTap
{
    [self.view endEditing:YES];
    @try
    {
    if(self.fullNameTF.text.length>0&&self.mobileNumberTF.text.length>0&&self.countryCodeTF.text.length>0&&[self.emailIDTF.text containsString:@"@"])
    {
        self.activityInd.hidden=NO;
        [self.activityInd startAnimating];
        if([self.mobileNumberTF.text isEqualToString:@"5454545454"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.activityInd.hidden=YES;
                [self.activityInd stopAnimating];
                NewOTPViewController * otp = [self.storyboard instantiateViewControllerWithIdentifier:@"NewOTPViewController"];
                otp.referralCode = self.referralCodeTF.text;
                otp.fullName = self.fullNameTF.text;
                otp.phoneNumber = self.mobileNumberTF.text;
                otp.countryCode = self.countryCodeTF.text;
                otp.email=self.emailIDTF.text;
               // otp.checkString=@"Validate";
//                NSUserDefaults *loggedInUserNew = [NSUserDefaults standardUserDefaults];
//                [loggedInUserNew setObject:@"clientcreated" forKey:@"clientstatus"];
//                delegate2.zenwiseToken=[NSString stringWithFormat:@"%@",@"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2UxOTAiLCJpc3MiOiJodHRwczovL3d3dy5oY3VlLmNvIiwicGVybWlzc2lvbnMiOiJhdXRoZW50aWNhdGVkIiwidXNlcmlkIjpudWxsLCJpYXQiOjE0ODYyMDczNDB9.ewu2QtoHl1kBSrM1y6yL9Jr58kiGmhCsy2YWoFrE2OU"];
//                delegate2.userID=@"5454545454";
//                [loggedInUserNew setObject:delegate2.zenwiseToken forKey:@"guestaccess"];
//                [loggedInUserNew setObject:delegate2.userID forKey:@"userid"];
//                [loggedInUserNew setObject:@"5454545454" forKey:@"profilemobilenumber"];
//                [loggedInUserNew setObject:@"xxx@gmail.com" forKey:@"profileemail"];
//                [loggedInUserNew setObject:self.fullNameTF.text forKey:@"profilename"];
//                [loggedInUserNew synchronize];
                [self presentViewController:otp animated:YES completion:nil];
                //            [BDPassCodeView setBDPasscodeDelegate:(id)self];
                //            [BDPassCodeView showPasscodeViewWithType:PassCodeTypeSixDigitOTP];
            });
        }
        else
        {
       [self generateEmail];
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Please fill all missing fields." preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            [self presentViewController:alert animated:YES completion:^{
                
                
                
            }];
            
            
            
            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                self.activityInd.hidden=YES;
                [self.activityInd stopAnimating];
                
            }];
            
            
            
            [alert addAction:okAction];
            
        });
        
    }
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}
-(void)generateEmail
{
    @try
    {
    TagEncode * enocde=[[TagEncode alloc]init];
    enocde.inputRequestString=@"verifyemail";
        NSString * number = [[NSString alloc]initWithString:self.countryCodeTF.text];
        number = [number stringByAppendingString:self.mobileNumberTF.text];
        NSArray* words = [number componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString* nospacestring = [words componentsJoinedByString:@""];
        delegate2.voucherMobileNumber = nospacestring;
        
        NSString * mobileNumber = [NSString stringWithFormat:@"%@",self.mobileNumberTF.text];
        NSArray* words1 = [mobileNumber componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString* nospacestring1 = [words1 componentsJoinedByString:@""];
        
        NSString * countryCode = [NSString stringWithFormat:@"%@",self.countryCodeTF.text];
        NSArray* words2 = [countryCode componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString* nospacestring2 = [words2 componentsJoinedByString:@""];
        
        NSString * email = [NSString stringWithFormat:@"%@",self.emailIDTF.text];
        NSArray* words3 = [email componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString* nospacestring3 = [words3 componentsJoinedByString:@""];
        
        NSString * referal = [NSString stringWithFormat:@"%@",self.referralCodeTF.text];
        NSArray* words4 = [referal componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString* nospacestring4 = [words4 componentsJoinedByString:@""];
        
        delegate2.referralCode=nospacestring4;
        
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    [inputArray addObject:nospacestring1];
    [inputArray addObject:nospacestring4];
    [inputArray addObject:@""];
    [inputArray addObject:nospacestring3];
    [inputArray addObject:self.fullNameTF.text];
    [inputArray addObject:nospacestring2];
    [inputArray addObject:[NSString stringWithFormat:@"%@2factor/verifyemail",delegate2.baseUrl]];
    
    [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        NSArray * keysArray=[[dict objectForKey:@"data"]  allKeys];
        if(keysArray.count>0)
        {
//            if([keysArray containsObject:@"authtoken"])
//            {
//                NSUserDefaults *loggedInUserNew = [NSUserDefaults standardUserDefaults];
//                [loggedInUserNew setObject:@"clientcreated" forKey:@"clientstatus"];
//
//                if([[[dict objectForKey:@"data"] objectForKey:@"message"] isEqualToString:@"client info created"])
//                {
//                    delegate2.marketmonksHint=true;
//                    delegate2.wisdomHint=true;
//                    delegate2.feedsHint=true;
//                    delegate2.marketwatchHinT=true;
//                    delegate2.stockHint=true;
//                    [loggedInUserNew setObject:@"showlearn" forKey:@"visibility"];
//                    delegate2.zenwiseToken=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"] objectForKey:@"authtoken"]];
//                    delegate2.userID=nospacestring1;
//                    [loggedInUserNew setObject:delegate2.zenwiseToken forKey:@"guestaccess"];
//                    [loggedInUserNew setObject:delegate2.userID forKey:@"userid"];
//                    [loggedInUserNew setObject:nospacestring1 forKey:@"profilemobilenumber"];
//                    [loggedInUserNew setObject:nospacestring3 forKey:@"profileemail"];
//                    [loggedInUserNew setObject:self.fullNameTF.text forKey:@"profilename"];
//                    [loggedInUserNew synchronize];
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        self.activityInd.hidden=YES;
//                        [self.activityInd stopAnimating];
//
//                        BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
//
//                        [self presentViewController:view animated:YES completion:nil];
//                    });
//                }else if ([[[dict objectForKey:@"data"] objectForKey:@"message"] isEqualToString:@"client info updated"])
//                {
//                    emailVerify = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"] objectForKey:@"emailverified"]];
//                    if([emailVerify isEqualToString:@"1"])
//                    {
//                        [self sendOTP];
//                    }else if ([emailVerify isEqualToString:@"0"])
//                    {
//                        [self sendOTP];
//                        [loggedInUserNew setObject:@"showlearn" forKey:@"visibility"];
//                        delegate2.zenwiseToken=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"] objectForKey:@"authtoken"]];
//                        delegate2.userID=nospacestring1;
//                        [loggedInUserNew setObject:delegate2.zenwiseToken forKey:@"guestaccess"];
//                        [loggedInUserNew setObject:delegate2.userID forKey:@"userid"];
//                        [loggedInUserNew setObject:nospacestring1 forKey:@"profilemobilenumber"];
//                        [loggedInUserNew setObject:nospacestring3 forKey:@"profileemail"];
//                        [loggedInUserNew setObject:self.fullNameTF.text forKey:@"profilename"];
//                        [loggedInUserNew synchronize];
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            self.activityInd.hidden=YES;
//                            [self.activityInd stopAnimating];
//
////                            BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
////
////                            [self presentViewController:view animated:YES completion:nil];
//                        });
//                    }
////                    }else if([emailVerify isEqualToString:@"0"])
////                    {
////                        dispatch_async(dispatch_get_main_queue(), ^{
////                            self.bgView.hidden=NO;
////                        });
////                    }
//                }
//
//                //mixpanel//
//                NSString * referalCode;
//                if(nospacestring4.length>0)
//                {
//                    referalCode=nospacestring4;
//                }
//                else
//                {
//                    referalCode=@"";
//                }
//                if(delegate2.subLocality.length>0)
//                {
//
//                }
//                else
//                {
//                    delegate2.subLocality=@"";
//                }
//                if(delegate2.postalCode.length>0)
//                {
//
//                }
//                else
//                {
//                    delegate2.postalCode=@"";
//                }
//                if(delegate2.locality.length>0)
//                {
//
//                }
//                else
//                {
//                    delegate2.locality=@"";
//                }
//
//            }
//            }else if ([[dict objectForKey:@"data"] objectForKey:@"message"])
//            {
//                dispatch_async(dispatch_get_main_queue(), ^{
//            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Something went wrong." preferredStyle:UIAlertControllerStyleAlert];
//
//                    [self presentViewController:alert animated:YES completion:^{
//                    }];
//            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//                    self.activityInd.hidden=YES;
//                    [self.activityInd stopAnimating];
//
//                    }];
//                    [alert addAction:okAction];
//
//                                              });
//            }
//            else
//            {
            if([[[dict objectForKey:@"data"] objectForKey:@"message"]isEqualToString:@"mail sent on given mail id"])
            {
                 dispatch_async(dispatch_get_main_queue(), ^{
                 self.bgView.hidden=NO;
                self.verifiedBtn.hidden = NO;
                self.activityIndicator.hidden = YES;
                     NSString * emailStr = [NSString stringWithFormat:@"We have sent a verification link to %@. Please use the button below once you have verified using the link.",self.emailIDTF.text];
                     self.mailVerificationLbl.text = emailStr;
                 });
            }
            else
                
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:[[dict objectForKey:@"data"] objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
                    
                                [self presentViewController:alert animated:YES completion:^{
                    
                                }];
                    
                                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                                    self.activityInd.hidden=YES;
                                    [self.activityInd stopAnimating];
                                }];
                    
                                [alert addAction:okAction];
                            });
                    //
            }
//            }
//        if([[[dict objectForKey:@"data"] objectForKey:@"message"]isEqualToString:@"Success"])
//        {
////            delegate2.phoneNumberSessionID=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"] objectForKey:@"Details"]];
//             dispatch_async(dispatch_get_main_queue(), ^{
//                 self.activityInd.hidden=YES;
//                 [self.activityInd stopAnimating];
//                 NewOTPViewController * otp = [self.storyboard instantiateViewControllerWithIdentifier:@"NewOTPViewController"];
//                 otp.referralCode = self.referralCodeTF.text;
//                 otp.fullName = self.fullNameTF.text;
//                 otp.phoneNumber = nospacestring1;
//                 otp.countryCode = self.countryCodeTF.text;
//                 otp.email=self.emailIDTF.text;
//                 [self presentViewController:otp animated:YES completion:nil];
////            [BDPassCodeView setBDPasscodeDelegate:(id)self];
////            [BDPassCodeView showPasscodeViewWithType:PassCodeTypeSixDigitOTP];
//             });
//
//        }
        
//        else
//
//        {
//                     dispatch_async(dispatch_get_main_queue(), ^{
//            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Something went wrong." preferredStyle:UIAlertControllerStyleAlert];
//
//
//
//            [self presentViewController:alert animated:YES completion:^{
//
//
//
//            }];
//
//
//
//            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//                self.activityInd.hidden=YES;
//                [self.activityInd stopAnimating];
//
//            }];
//
//
//
//            [alert addAction:okAction];
//
//                          });
//
//        }
        
        
        //        dispatch_async(dispatch_get_main_queue(), ^{
        //            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:msg preferredStyle:UIAlertControllerStyleAlert];
        //
        //            [self presentViewController:alert animated:YES completion:^{
        //
        //            }];
        //
        //            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //
        //            }];
        //
        //            [alert addAction:okAction];
        //        });
        //
        //
        //
        
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                
                UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    self.activityInd.hidden=YES;
                    [self.activityInd stopAnimating];
                    [self generateEmail];
                }];
                
                UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    self.activityInd.hidden=YES;
                    [self.activityInd stopAnimating];
                }];
                
                
                [alert addAction:retryAction];
                [alert addAction:cancel];
                
            });
            
        }
    }];
        
    }

@catch (NSException * e) {
    //NSLog(@"Exception: %@", e);
}
@finally {
    //NSLog(@"finally");
}

   
}

-(void)sendOTP
{
    @try
    {
        TagEncode * enocde=[[TagEncode alloc]init];
        enocde.inputRequestString=@"generateOtp";
        NSString * number = [self.countryCodeTF.text stringByAppendingString:self.mobileNumberTF.text];
        NSArray* words = [number componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString* nospacestring = [words componentsJoinedByString:@""];
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray  addObject:nospacestring];
        [inputArray addObject:[NSString stringWithFormat:@"%@2factor/generateotp",delegate2.baseUrl]];
        
        [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            NSArray * keysArray=[dict allKeys];
            
            
            if(keysArray.count>0)
            {
                if([[[dict objectForKey:@"data"] objectForKey:@"message"]isEqualToString:@"Success"])
                {
                    
                    //                    delegate.phoneNumberSessionID=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"] objectForKey:@"Details"]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.activityIndicator.hidden=YES;
                        [self.activityIndicator stopAnimating];
                    });
                    NSString * mobileNumber = [NSString stringWithFormat:@"%@",self.mobileNumberTF.text];
                    NSArray* words1 = [mobileNumber componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    NSString* nospacestring1 = [words1 componentsJoinedByString:@""];
                    NewOTPViewController * otp = [self.storyboard instantiateViewControllerWithIdentifier:@"NewOTPViewController"];
                    otp.referralCode = self.referralCodeTF.text;
                    otp.fullName = self.fullNameTF.text;
                    otp.phoneNumber = nospacestring1;
                    otp.countryCode = self.countryCodeTF.text;
                    otp.email=self.emailIDTF.text;
                    if ([emailVerify isEqualToString:@"0"])
                    {
                    otp.checkString = @"Validate";
                    }
                    [self presentViewController:otp animated:YES completion:nil];
                    
                }
                
                else
                    
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Something went wrong." preferredStyle:UIAlertControllerStyleAlert];
                        
                        
                        
                        [self presentViewController:alert animated:YES completion:^{
                            
                            
                            
                        }];
                        
                        
                        
                        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            
                            self.activityIndicator.hidden=YES;
                            [self.activityIndicator stopAnimating];
                            
                        }];
                        
                        
                        
                        [alert addAction:okAction];
                        
                    });
                    
                }
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    
                    UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        self.activityIndicator.hidden=YES;
                        [self.activityIndicator stopAnimating];
                        [self sendOTP];
                    }];
                    
                    UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        self.activityIndicator.hidden=YES;
                        [self.activityIndicator stopAnimating];
                    }];
                    
                    
                    [alert addAction:retryAction];
                    [alert addAction:cancel];
                    
                });
                
            }
        }];
        
        
    }
    
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}



-(void)clientCreation
{
    @try
    {
    TagEncode * enocde=[[TagEncode alloc]init];
    clientCreationDict=[[NSDictionary alloc]init];
    enocde.inputRequestString=@"clientcreation";
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    if(self.referralCodeTF.text.length==0)
    {
        self.referralCodeTF.text=@"";
    }
    [inputArray addObject:self.mobileNumberTF.text];
    [inputArray addObject:self.referralCodeTF.text];
    [inputArray addObject:@""];
    [inputArray addObject:self.emailIDTF.text];
    [inputArray addObject:self.fullNameTF.text];
    [inputArray addObject:self.countryCodeTF.text];
    [inputArray addObject:[NSString stringWithFormat:@"%@follower/client",delegate2.baseUrl]];
    [inputArray addObject:self.mobileNumberTF.text];
  
    [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        NSArray * keysArray=[dict allKeys];
        
        
        if(keysArray.count>0)
        {
        clientCreationDict=dict;
      if(clientCreationDict)
      {
          NSUserDefaults *loggedInUserNew = [NSUserDefaults standardUserDefaults];
         [loggedInUserNew setObject:@"clientcreated" forKey:@"clientstatus"];
          if([[[clientCreationDict objectForKey:@"data"] objectForKey:@"message"] isEqualToString:@"client info created"])
          {
         [loggedInUserNew setObject:@"showlearn" forKey:@"visibility"];
          }
         delegate2.zenwiseToken=[NSString stringWithFormat:@"%@",[[clientCreationDict objectForKey:@"data"] objectForKey:@"authtoken"]];
         delegate2.userID=self.mobileNumberTF.text;
         [loggedInUserNew setObject:delegate2.zenwiseToken forKey:@"guestaccess"];
         [loggedInUserNew setObject:delegate2.userID forKey:@"userid"];
         [loggedInUserNew setObject:self.mobileNumberTF.text forKey:@"profilemobilenumber"];
         [loggedInUserNew setObject:self.emailIDTF.text forKey:@"profileemail"];
         [loggedInUserNew setObject:self.fullNameTF.text forKey:@"profilename"];
         [loggedInUserNew synchronize];
          //mixpanel//
          NSString * referalCode;
          if(self.referralCodeTF.text.length>0)
          {
              referalCode=self.referralCodeTF.text;
          }
          else
          {
              referalCode=@"";
          }
          Mixpanel *mixpanelMini = [Mixpanel sharedInstance];
          
          [mixpanelMini identify:self.mobileNumberTF.text];
          
          [mixpanelMini.people set:@{@"name":self.fullNameTF.text,@"mobileno":self.mobileNumberTF.text,@"email":self.emailIDTF.text,@"logintime":[NSDate date],@"sublocality":delegate2.subLocality,@"location":delegate2.locality,@"postalcode":delegate2.postalCode,@"referralcode":referalCode,@"typeofuser":@"GUEST",@"userid":self.mobileNumberTF.text,@"deviceid":delegate2.currentDeviceId}];
          if(delegate2.mixpanelDeviceToken.length>0)
                      {
                          [mixpanelMini.people addPushDeviceToken:delegate2.mixpanelDeviceToken];
              
                      }
          
         dispatch_async(dispatch_get_main_queue(), ^{
         BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
             
         [self presentViewController:view animated:YES completion:nil];
         });
      }
        else
        {
            [self clientCreation];
        }
        }
        else
        {
            [self clientCreation];
        }
        
    }];
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

}

-(void)otpVerify:(NSString *)otpStr
{
    @try
    {
    if(otpStr.length>0)
    {
    TagEncode * enocde=[[TagEncode alloc]init];
   
    enocde.inputRequestString=@"otpVerify";
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    [inputArray addObject:[NSString stringWithFormat:@"http://2factor.in/API/V1/13ffd06a-2fe3-11e7-8473-00163ef91450/SMS/VERIFY/%@/%@",delegate2.phoneNumberSessionID,otpStr]];
    
    [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
        
        NSArray * keysArray=[dict allKeys];
        
        
        if(keysArray.count>0)
        {
       
        if([[[dict objectForKey:@"data"] objectForKey:@"Details"]isEqualToString:@"OTP Matched"])
            
        {
            
         
            [self clientCreation];
         
            
        }
        
        else
            
        {
            
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Invalid OTP. Try Again." preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            [self presentViewController:alert animated:YES completion:^{
                
                
                
            }];
            
            
            
            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                
                
            }];
            
            
            
            [alert addAction:okAction];
            
        }
        
        
        }
        
    }];
        
    }
        
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }

    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    
    
}
-(BOOL)networkStatus
{
    
    
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    
    if(netStatus==NotReachable)
    {
        
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    return YES;
    
    
}
- (IBAction)verifiedAction:(id)sender {
    self.verifiedBtn.hidden=YES;
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    
    TagEncode * enocde=[[TagEncode alloc]init];
    
    enocde.inputRequestString=@"emailstatus";
    NSString * number = [[NSString alloc]initWithString:self.countryCodeTF.text];
    number = [number stringByAppendingString:self.mobileNumberTF.text];
    NSArray* words = [number componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString* nospacestring = [words componentsJoinedByString:@""];
    delegate2.voucherMobileNumber = nospacestring;
    
    NSString * mobileNumber = [NSString stringWithFormat:@"%@",self.mobileNumberTF.text];
    NSArray* words1 = [mobileNumber componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString* nospacestring1 = [words1 componentsJoinedByString:@""];
    
    NSString * countryCode = [NSString stringWithFormat:@"%@",self.countryCodeTF.text];
    NSArray* words2 = [countryCode componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString* nospacestring2 = [words2 componentsJoinedByString:@""];
    CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef) nospacestring2, NULL, CFSTR("!*'();:@&=+@/?#[]"), kCFStringEncodingUTF8);
    
    NSString * email = [NSString stringWithFormat:@"%@",self.emailIDTF.text];
    NSArray* words3 = [email componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString* nospacestring3 = [words3 componentsJoinedByString:@""];
    
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    [inputArray addObject:[NSString stringWithFormat:@"%@2factor/emailstatus?mobilenumber=%@&email=%@&countrycode=%@",delegate2.baseUrl,nospacestring1,nospacestring3,newString]];
    
    [enocde inputRequestMethod1:inputArray withCompletionHandler:^(NSDictionary *dict) {
        NSArray * keysArray=[dict allKeys];
        if(keysArray.count>0)
        {
            if([[[dict objectForKey:@"data"] objectForKey:@"isemailverified"] intValue] == 0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:@"Please verify email to proceed." preferredStyle:UIAlertControllerStyleAlert];
                    
                    
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                        self.verifiedBtn.hidden=NO;
                        
                    }];
                    
                    
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                        self.activityIndicator.hidden=YES;
                        [self.activityIndicator stopAnimating];
                        
                    }];
                    
                    
                    
                    [alert addAction:okAction];
                    
                });
            }
            else if([[[dict objectForKey:@"data"] objectForKey:@"isemailverified"] intValue] == 1)
            {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    CATransition *animation = [CATransition animation];
                    animation.type = kCATransitionFade;
                    animation.duration = 0.4;
                    [self.bgView.layer addAnimation:animation forKey:nil];
                    self.succesView.hidden = YES;
                    self.bgSubView.hidden=NO;
               
                    NewOTPViewController * otp = [self.storyboard instantiateViewControllerWithIdentifier:@"NewOTPViewController"];
                                     otp.referralCode = self.referralCodeTF.text;
                                     otp.fullName = self.fullNameTF.text;
                                     otp.phoneNumber = nospacestring1;
                                     otp.countryCode = self.countryCodeTF.text;
                                     otp.email=self.emailIDTF.text;
                                     [self presentViewController:otp animated:YES completion:nil];
                  });
                
            }
        }
        else
        {
            
        }
    }];
}

- (IBAction)resendEmailAction:(id)sender {
    [self generateEmail];
}

@end
