//
//  PortfolioCell.h
//  testing
//
//  Created by zenwise technologies on 23/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PortfolioCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *companyName;
@property (strong, nonatomic) IBOutlet UILabel *transactionTypeLbl;
@property (strong, nonatomic) IBOutlet UILabel *qtyTypeLbl;
@property (strong, nonatomic) IBOutlet UILabel *priceLbl;
@property (strong, nonatomic) IBOutlet UILabel *openLbl;
@property (strong, nonatomic) IBOutlet UILabel *segmentLbl;
@property (strong, nonatomic) IBOutlet UIButton *squareoffBtn;
@property (weak, nonatomic) IBOutlet UILabel *productTypeLbl;

@end
