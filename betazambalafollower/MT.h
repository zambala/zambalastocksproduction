//
//  MT.h
//  
//
//  Created by zenwise technologies on 27/05/17.
//
//

#import <Foundation/Foundation.h>

@interface MT : NSObject


 @property short shPacketSize  ;
 @property short shHeaderSize  ;

 @property short shHash ;

// Data Length
 @property short shCharSize ;
 @property short shBoolSize ;
 @property short shShortSize ;
 
 @property short shIntSize  ;
 @property short shLongSize  ;
 @property short shDoubleSize  ;
 @property short shFloatSize ;
 @property short shDecimalSize ;

// Compress Type
 @property short shCompressNone  ;
 @property short shCompressZLib  ;
 @property short shCompressCrypt ;


    // Tag Code 1 - 5000 - Byte
     @property short btDataType  ;
     @property short btWalletType  ;
    
     @property short btSide  ;
     @property short btOrderType ;
    
     @property short btTimeinForce ;
    
     @property short btTerminal  ;
     @property short btOrderSituation  ;
     @property short btOrderStatus ;
    
    // Tag Code 5001 - 10000 - Bool
     @property short blLoginCkeck  ;
     @property short blSuspend  ;
     @property short blCancelOnLogout  ;
    //         @property short blValidate  5003;
    
    // Tag Code 10001 - 20000 -  @property short
     @property short shDataSize  ;
     @property short shMsgCode  ;
     @property short shPos  ;
     @property short shPosCurrent  ;
     @property short shNoofRows  ;
     @property short shMasterType  ;
     @property short shErrorCode  ;
     @property short shRecordNo  ;
    
    // Tag Code 20001 - 30000 - Int
     @property short inMasterID  ;
     @property short inATINTradeNo  ;
     @property short inATINOrderNo  ;
     @property short inExchangeClientOrderNo  ;
 @property short inOrderNo  ;
     @property short inOrderQty  ;
     @property short inProductType ;
     @property short inPendingQty ;
     @property short inExecuteQty  ;
     @property short inDiscloseQry ;
     @property short inID  ;
     @property short inBuyQty  ;
     @property short inSellQty  ;
     @property short inNetQty  ;
     @property short inLTQ  ;
    
    
    
    // Tag Code 30001 - 35000 - Long
     @property short lnWalletID  ;
     @property short lnPayeeID  ;
     @property short lnTransactionID  ;
     @property short lnAmount  ;
     @property short lnBalance  ;
     @property short lnExpense  ;
     @property short lnWalletBufferKey  ;
    
    
    // Tag Code 50001 - 55000 - DateTime
     @property short dtSystemTime  ;
     @property short dtTransactionTime  ;
     @property short dtExpireTime  ;
     @property short dtExpiryDate ;
     @property short dtModifiedDate  ;
     @property short dtDateTime  ;
     @property short dtLicenseEndDate  ;
     @property short dtLicenseStartDate  ;
    
    // Tag Code 40001 - 45000 - Double
    
     @property short dbTickSize  ;
     @property short dbPrice ;
     @property short dbTriggerPrice  ;
     @property short dbStrikePrice ;
     @property short dbUpperband ;
     @property short dbLowerband ;
     @property short dbDivider  ;
     @property short dbMultiplier  ;
     @property short dbBuyAmount  ;
     @property short dbSellAmount  ;
     @property short dbBuyAvg  ;
     @property short dbSellAvg  ;
     @property short dbNetAmount ;
     @property short dbNetAvg  ;
     @property short dbMTM  ;
     @property short dbLTP ;
     @property short dbPrevClose ;
    
    
    // Tag Code 55001 - 60000 - String
     @property short stUserID ;
     @property short stPassword ;
     @property short stNewPassword  ;
     @property short stHostAddress  ;
     @property short stSenderCompID  ;
     @property short stSenderSubID  ;
     @property short stTargetComID  ;
     @property short stTargetSubID  ;
     @property short stDataServerHost  ;
     @property short stExchange  ;
     @property short stDealerID  ;
     @property short stCtclID  ;
     @property short stManagerID  ;
     @property short stTradeNo  ;
     @property short stExchangeOrderNo  ;
     @property short stClientID  ;
     @property short stText  ;
     @property short stSymbol ;
     @property short stSecurityID  ;
     @property short stSecurityType  ;
     @property short stOptionType  ;
     @property short stSecurityDesc  ;
     @property short stRefText  ;
    
     @property short stErrorText  ;
     @property short stFileDescription  ;
     @property short stOrderTime  ;
     @property short stExchangeOrderTime  ;
     @property short stTradeTime  ;
     @property short stTerminalInfo  ;
    
     @property short stCurrency  ;
     @property short stMemberID  ;
     @property short stSettleCurrency  ;
     @property short stTraderID ;
    
     @property short stExpiryDate  ;
     @property short stParameter  ;
     @property short stApplicationName  ;
     @property short stSeries  ;
     @property short stDigit  ;
    
    // Tag Code 60001 - 65000 - Binary & Other
     @property short obBinaryData  ;
    
     @property short nlTagFooter  ;
    
    



    
     @property short PreLogin  ;
     @property short Login  ;
     @property short Logout;
    
     @property short Gateway  ;
     @property short GatewayRequest  ;
     @property short GatewayResponse  ;
    
     @property short GatewayStart  ;
     @property short GatewayStop  ;
     @property short GatewayStatus  ;
    
     @property short GatewayParameter  ;
    
     @property short UserMapRequest  ;
     @property short UserMap  ;
     @property short UserMapResponse  ;
    
     @property short UserMaster  ;
     @property short UserMasterRequest  ;
     @property short UserMasterResponse  ;
    
     @property short UserOrderID  ;
    
     @property short ServerMasterRequest  ;
     @property short ServerMaster  ;
     @property short ServerMasterResponse  ;
    
    // File Details Message Code //
    
     @property short Application_Update_Request  ;
     @property short Application_Update_Response  ;
     @property short Application_Delete_Request  ;
    
    // BroadCast Code //
    
     @property short Broadcast  ;
     @property short Watch  ;
     @property short BroadCastPrice  ;
     @property short IndexWatch  ;
     @property short SecurityRequest  ;
     @property short SecurityResponse  ;
     @property short broadCastId;

    // Application //
    
     @property short Application_List  ;
     @property short Application_Desc_Start  ;
     @property short Application_Desc  ;
     @property short Application_Desc_End  ;
     @property short Application_Desc_Request  ;
     @property short Application_Data_Start  ;
     @property short Application_Data  ;
     @property short Application_Data_End  ;
     @property short Application_Error  ;
    
    // Master //
    
     @property short MasterRequest  ;
     @property short Master  ;
     @property short MasterResponse  ;
    
    //Order //
     @property short OrderDownloadRequest  ;
     @property short OrderDownload  ;
     @property short OrderDownloadResponce  ;
     @property short TradeDownloadRequest  ;
     @property short TradeDownloadResponce  ;
     @property short TradeDownload  ;
     @property short Order  ;
     @property short Trade  ;
     @property short NetPositionHistry  ;
     @property short NetPositionHistryRequest  ;
     @property short NetPositionHistryResponce  ;
     @property short modifyOrder;
@property short cancelOrder;

    
     @property short SocketNotify;
    @property short inRefID;


 @property short inBroadCastID;
//security
@property short stSingleSecurityAnswer;
@property short SecurityQueAndAns;
@property short bl2FA;
@property short blEnable;
@property short blLock;
@property short inSecurityQuestionNo;
@property short inVersion;

//holdings//

@property short holdingRequest;
@property short blDelete;
@property short inRecordNo;
@property  short stClientIDHolding;
@property short stSymbolHolding;
@property short stSeriesHolding;
@property short inNetQtyHolding;
@property short blIsCollateral;
@property short InEvoluationMethod;
@property short inCollateralQty;
@property short InHairCut;
@property short inCollateralUpdateQty;
@property short InHoldingUpdateQty;
@property short stProduct;
@property short stSecurityIDHolding;
@property short dbClosingPrice;
@property short inWithheldHoldingQty;
@property short intWithheldCollateralQty;
@property short holdingsResponse;
@property short holdingsDownloadResponse;


//IndexWatch




+ (id)Mt1;

@end
