//
//  NewMessages.h
//  zambala leader
//
//  Created by zenwise mac 2 on 3/15/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewMessages : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *messagesTbl;
- (IBAction)backAction:(id)sender;

- (IBAction)readAllBtnAction:(id)sender;

- (IBAction)filterAction:(id)sender;


@end
