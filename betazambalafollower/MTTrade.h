//
//  MTTrade.h
//  betazambalafollower
//
//  Created by zenwise mac 2 on 8/31/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTTrade : NSObject

@property  NSString*    stUserID; //
@property  NSString*    stManagerID; //
@property  int       inATINTradeID; //
@property  int       inATINOrderID; //
@property  NSString*    stTradeID; //
@property  NSString*    stTradeTime; //
@property  int       inOrderID; //
@property  NSString*    stOrderTime; //
@property  NSString*    stExchangeOrderID; //
@property  NSString*    stExchangeOrderTime; //
@property  NSString*    stExchange; //
@property  NSString*    stSecurityID; //
@property  NSString*    stSecurityType; //
@property  NSString*    stSymbol; //
@property  NSString*    stExpiryDate; // Series Expiry
@property  NSString*    stOptionType; // not this time use
@property  double    dbStrikePrice; //
@property  NSString*    stSide; //
@property  NSString*    stOrderType; //
@property  int       inQty; // Execute Qty
@property  int       inPendingQty; //
@property  double    dbPrice; //
@property  NSString*    stClientID; //
@property  NSString*    stRefText; //
@property  int       inTradeSeqNo;





+ (id)Mt5;

@end
